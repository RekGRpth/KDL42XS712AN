.class public Lcom/konka/lockscreen/DateWeather;
.super Landroid/widget/LinearLayout;
.source "DateWeather.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/lockscreen/DateWeather$FormatChangeObserver;,
        Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver;
    }
.end annotation


# static fields
.field private static final M24:Ljava/lang/String; = "kk:mm"


# instance fields
.field private final MSG:I

.field private final NET_MSG:I

.field private final TAG:Ljava/lang/String;

.field private mCalendar:Ljava/util/Calendar;

.field private mContext:Landroid/content/Context;

.field private mDate:Landroid/widget/TextView;

.field private mDateFormatString:Ljava/lang/String;

.field private mFormatChangeObserver:Landroid/database/ContentObserver;

.field private mHandler:Landroid/os/Handler;

.field private mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mNetThread:Ljava/lang/Thread;

.field private mTempInfo:Ljava/lang/String;

.field private mTime:Landroid/widget/TextView;

.field private mWeather:Landroid/widget/TextView;

.field private mWeatherInfo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "DateWeather"

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->TAG:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/lockscreen/DateWeather;->MSG:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/lockscreen/DateWeather;->NET_MSG:I

    new-instance v0, Lcom/konka/lockscreen/DateWeather$1;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/DateWeather$1;-><init>(Lcom/konka/lockscreen/DateWeather;)V

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/lockscreen/DateWeather$2;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/DateWeather$2;-><init>(Lcom/konka/lockscreen/DateWeather;)V

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    iput-object p1, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/DateWeather;->setVisibility(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/konka/lockscreen/DateWeather;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/DateWeather;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mWeatherInfo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/konka/lockscreen/DateWeather;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/konka/lockscreen/DateWeather;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/lockscreen/DateWeather;->mWeatherInfo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/konka/lockscreen/DateWeather;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/DateWeather;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mTempInfo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/konka/lockscreen/DateWeather;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/konka/lockscreen/DateWeather;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/lockscreen/DateWeather;->mTempInfo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/konka/lockscreen/DateWeather;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/DateWeather;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/konka/lockscreen/DateWeather;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/DateWeather;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mWeather:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/konka/lockscreen/DateWeather;)V
    .locals 0
    .param p0    # Lcom/konka/lockscreen/DateWeather;

    invoke-direct {p0}, Lcom/konka/lockscreen/DateWeather;->updateTime()V

    return-void
.end method

.method static synthetic access$500(Lcom/konka/lockscreen/DateWeather;)V
    .locals 0
    .param p0    # Lcom/konka/lockscreen/DateWeather;

    invoke-direct {p0}, Lcom/konka/lockscreen/DateWeather;->refresh()V

    return-void
.end method

.method static synthetic access$602(Lcom/konka/lockscreen/DateWeather;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0    # Lcom/konka/lockscreen/DateWeather;
    .param p1    # Ljava/util/Calendar;

    iput-object p1, p0, Lcom/konka/lockscreen/DateWeather;->mCalendar:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$700(Lcom/konka/lockscreen/DateWeather;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/DateWeather;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/konka/lockscreen/DateWeather;)Z
    .locals 1
    .param p0    # Lcom/konka/lockscreen/DateWeather;

    invoke-direct {p0}, Lcom/konka/lockscreen/DateWeather;->isNetworkAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$902(Lcom/konka/lockscreen/DateWeather;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0    # Lcom/konka/lockscreen/DateWeather;
    .param p1    # Ljava/lang/Thread;

    iput-object p1, p0, Lcom/konka/lockscreen/DateWeather;->mNetThread:Ljava/lang/Thread;

    return-object p1
.end method

.method private checkNetwork()V
    .locals 1

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mNetThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/lockscreen/DateWeather$4;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/DateWeather$4;-><init>(Lcom/konka/lockscreen/DateWeather;)V

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mNetThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mNetThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method private initUI()V
    .locals 3

    const v0, 0x1020414    # android.R.id.konka_lockscreen_time

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/DateWeather;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mTime:Landroid/widget/TextView;

    const-string v1, "00:00"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    const v0, 0x1020415    # android.R.id.konka_lockscreen_weather

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/DateWeather;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mWeather:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mWeather:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const v0, 0x1020416    # android.R.id.konka_lockscreen_date

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/DateWeather;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mDate:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mDateFormatString:Ljava/lang/String;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-static {v1, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method private isNetworkAvailable()Z
    .locals 7

    :try_start_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v4

    const-string v5, "http.connection.timeout"

    const/16 v6, 0x7d0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    const-string v4, "http://www.baidu.com"

    invoke-direct {v2, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-interface {v0, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_1

    :cond_0
    new-instance v4, Ljava/io/IOException;

    const-string v5, "StatusCode!=200"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    :goto_0
    const/4 v4, 0x0

    :goto_1
    return v4

    :cond_1
    const/4 v4, 0x1

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private refresh()V
    .locals 3

    invoke-direct {p0}, Lcom/konka/lockscreen/DateWeather;->updateTime()V

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mDateFormatString:Ljava/lang/String;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-static {v1, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTime()V
    .locals 4

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mCalendar:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v1, "kk:mm"

    iget-object v2, p0, Lcom/konka/lockscreen/DateWeather;->mCalendar:Ljava/util/Calendar;

    invoke-static {v1, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mTime:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver;-><init>(Lcom/konka/lockscreen/DateWeather;)V

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.TIME_TICK"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    :cond_0
    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mFormatChangeObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_1

    new-instance v0, Lcom/konka/lockscreen/DateWeather$FormatChangeObserver;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/DateWeather$FormatChangeObserver;-><init>(Lcom/konka/lockscreen/DateWeather;)V

    iput-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mFormatChangeObserver:Landroid/database/ContentObserver;

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/konka/lockscreen/DateWeather;->mFormatChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_1
    invoke-direct {p0}, Lcom/konka/lockscreen/DateWeather;->updateTime()V

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    invoke-direct {p0}, Lcom/konka/lockscreen/DateWeather;->checkNetwork()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mFormatChangeObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mFormatChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    iput-object v2, p0, Lcom/konka/lockscreen/DateWeather;->mFormatChangeObserver:Landroid/database/ContentObserver;

    iput-object v2, p0, Lcom/konka/lockscreen/DateWeather;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1040064    # android.R.string.abbrev_wday_month_day_no_year

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mDateFormatString:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x109007d    # android.R.layout.konka_lockscreen_date_weather

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-direct {p0}, Lcom/konka/lockscreen/DateWeather;->initUI()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mCalendar:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/konka/lockscreen/DateWeather$3;

    invoke-direct {v2, p0}, Lcom/konka/lockscreen/DateWeather$3;-><init>(Lcom/konka/lockscreen/DateWeather;)V

    invoke-static {v1, v2}, Lcom/konka/lockscreen/service/WeatherService;->getSinaWeather(Landroid/content/Context;Lcom/konka/lockscreen/service/IWeatherCallback;)V

    return-void
.end method
