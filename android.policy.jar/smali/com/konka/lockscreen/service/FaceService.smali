.class public Lcom/konka/lockscreen/service/FaceService;
.super Ljava/lang/Object;
.source "FaceService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FaceService"

.field private static mFaceService:Lcom/konka/lockscreen/service/FaceService;


# instance fields
.field private final ACTION:Ljava/lang/String;

.field private final FACE_CAMERA_NOT_EXIST:I

.field private final FACE_REC_INVALID:I

.field private final FACE_SUCCESS:I

.field private mContext:Landroid/content/Context;

.field private mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;

.field private mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/lockscreen/service/FaceService;->mFaceService:Lcom/konka/lockscreen/service/FaceService;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "com.konka.FACE_RECOGNITION"

    iput-object v0, p0, Lcom/konka/lockscreen/service/FaceService;->ACTION:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/lockscreen/service/FaceService;->FACE_SUCCESS:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/lockscreen/service/FaceService;->FACE_CAMERA_NOT_EXIST:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/lockscreen/service/FaceService;->FACE_REC_INVALID:I

    new-instance v0, Lcom/konka/lockscreen/service/FaceService$1;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/service/FaceService$1;-><init>(Lcom/konka/lockscreen/service/FaceService;)V

    iput-object v0, p0, Lcom/konka/lockscreen/service/FaceService;->mServiceConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/konka/lockscreen/service/FaceService;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$002(Lcom/konka/lockscreen/service/FaceService;Lcom/konka/facerecognition/IFaceRecognitionService;)Lcom/konka/facerecognition/IFaceRecognitionService;
    .locals 0
    .param p0    # Lcom/konka/lockscreen/service/FaceService;
    .param p1    # Lcom/konka/facerecognition/IFaceRecognitionService;

    iput-object p1, p0, Lcom/konka/lockscreen/service/FaceService;->mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;

    return-object p1
.end method

.method static synthetic access$102(Lcom/konka/lockscreen/service/FaceService;Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;
    .locals 0
    .param p0    # Lcom/konka/lockscreen/service/FaceService;
    .param p1    # Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/konka/lockscreen/service/FaceService;->mServiceConnection:Landroid/content/ServiceConnection;

    return-object p1
.end method

.method static synthetic access$202(Lcom/konka/lockscreen/service/FaceService;)Lcom/konka/lockscreen/service/FaceService;
    .locals 0
    .param p0    # Lcom/konka/lockscreen/service/FaceService;

    sput-object p0, Lcom/konka/lockscreen/service/FaceService;->mFaceService:Lcom/konka/lockscreen/service/FaceService;

    return-object p0
.end method

.method public static getFaceService(Landroid/content/Context;)Lcom/konka/lockscreen/service/FaceService;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-string v0, "FaceService"

    const-string v1, "getFaceService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/lockscreen/service/FaceService;->mFaceService:Lcom/konka/lockscreen/service/FaceService;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/lockscreen/service/FaceService;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/service/FaceService;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/lockscreen/service/FaceService;->mFaceService:Lcom/konka/lockscreen/service/FaceService;

    :cond_0
    sget-object v0, Lcom/konka/lockscreen/service/FaceService;->mFaceService:Lcom/konka/lockscreen/service/FaceService;

    return-object v0
.end method


# virtual methods
.method public bindService()V
    .locals 6

    const-string v2, "FaceService"

    const-string v3, "bindService"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.FACE_RECOGNITION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/lockscreen/service/FaceService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/konka/lockscreen/service/FaceService;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getIFaceRecognitionService()Lcom/konka/facerecognition/IFaceRecognitionService;
    .locals 1

    iget-object v0, p0, Lcom/konka/lockscreen/service/FaceService;->mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;

    return-object v0
.end method

.method public isCameraExist()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/konka/lockscreen/service/FaceService;->mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v3, p0, Lcom/konka/lockscreen/service/FaceService;->mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;

    invoke-interface {v3}, Lcom/konka/facerecognition/IFaceRecognitionService;->initFaceRec()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eq v0, v2, :cond_0

    move v1, v2

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public isFaceRec()Z
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/lockscreen/service/FaceService;->mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/konka/lockscreen/service/FaceService;->mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;

    invoke-interface {v2}, Lcom/konka/facerecognition/IFaceRecognitionService;->initFaceRec()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public unbindService()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/service/FaceService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/lockscreen/service/FaceService;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const-string v0, "FaceService"

    const-string v1, "unbind face rec service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
