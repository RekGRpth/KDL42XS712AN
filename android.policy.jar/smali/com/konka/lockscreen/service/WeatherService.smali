.class public Lcom/konka/lockscreen/service/WeatherService;
.super Ljava/lang/Object;
.source "WeatherService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "WeatherService"

.field private static final URL:Ljava/lang/String; = "http://php.weather.sina.com.cn/iframe/public/weather.php?day=0&city=\u6df1\u5733"

.field private static bDebug:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/lockscreen/service/WeatherService;->bDebug:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Lcom/konka/lockscreen/service/IWeatherCallback;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/konka/lockscreen/service/IWeatherCallback;

    invoke-static {p0, p1}, Lcom/konka/lockscreen/service/WeatherService;->getWeatherInfo(Landroid/content/Context;Lcom/konka/lockscreen/service/IWeatherCallback;)Z

    move-result v0

    return v0
.end method

.method public static getSinaWeather(Landroid/content/Context;Lcom/konka/lockscreen/service/IWeatherCallback;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/konka/lockscreen/service/IWeatherCallback;

    new-instance v0, Lcom/konka/lockscreen/service/WeatherService$1;

    invoke-direct {v0, p0, p1}, Lcom/konka/lockscreen/service/WeatherService$1;-><init>(Landroid/content/Context;Lcom/konka/lockscreen/service/IWeatherCallback;)V

    invoke-virtual {v0}, Lcom/konka/lockscreen/service/WeatherService$1;->start()V

    return-void
.end method

.method private static getWeatherInfo(Landroid/content/Context;Lcom/konka/lockscreen/service/IWeatherCallback;)Z
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/konka/lockscreen/service/IWeatherCallback;

    :try_start_0
    sget-boolean v8, Lcom/konka/lockscreen/service/WeatherService;->bDebug:Z

    if-eqz v8, :cond_0

    const-string v8, "WeatherService"

    const-string v9, "begin get SZ weather"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v8

    const-string v9, "http.connection.timeout"

    const/16 v10, 0x7d0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v8, v9, v10}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    const-string v8, "http://php.weather.sina.com.cn/iframe/public/weather.php?day=0&city=\u6df1\u5733"

    invoke-direct {v3, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-interface {v0, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    const/16 v9, 0xc8

    if-eq v8, v9, :cond_3

    :cond_1
    sget-boolean v8, Lcom/konka/lockscreen/service/WeatherService;->bDebug:Z

    if-eqz v8, :cond_2

    const-string v8, "WeatherService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "error : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v8, Ljava/io/IOException;

    const-string v9, "StatusCode!=200"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    :goto_0
    const/4 v8, 0x0

    :goto_1
    return v8

    :cond_3
    :try_start_1
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v5

    invoke-virtual {v5}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v7

    new-instance v8, Lcom/konka/lockscreen/service/WeatherHandler;

    invoke-direct {v8, p0, p1}, Lcom/konka/lockscreen/service/WeatherHandler;-><init>(Landroid/content/Context;Lcom/konka/lockscreen/service/IWeatherCallback;)V

    invoke-interface {v7, v8}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    new-instance v4, Lorg/xml/sax/InputSource;

    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v4, v8}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v7, v4}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    const/4 v8, 0x1

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
