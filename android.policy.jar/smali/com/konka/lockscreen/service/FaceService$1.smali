.class Lcom/konka/lockscreen/service/FaceService$1;
.super Ljava/lang/Object;
.source "FaceService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/lockscreen/service/FaceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/service/FaceService;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/service/FaceService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/service/FaceService$1;->this$0:Lcom/konka/lockscreen/service/FaceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    :try_start_0
    iget-object v1, p0, Lcom/konka/lockscreen/service/FaceService$1;->this$0:Lcom/konka/lockscreen/service/FaceService;

    invoke-static {p2}, Lcom/konka/facerecognition/IFaceRecognitionService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/konka/facerecognition/IFaceRecognitionService;

    move-result-object v2

    # setter for: Lcom/konka/lockscreen/service/FaceService;->mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;
    invoke-static {v1, v2}, Lcom/konka/lockscreen/service/FaceService;->access$002(Lcom/konka/lockscreen/service/FaceService;Lcom/konka/facerecognition/IFaceRecognitionService;)Lcom/konka/facerecognition/IFaceRecognitionService;

    const-string v1, "FaceService"

    const-string v2, "get face interface"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/lockscreen/service/FaceService$1;->this$0:Lcom/konka/lockscreen/service/FaceService;

    # setter for: Lcom/konka/lockscreen/service/FaceService;->mServiceConnection:Landroid/content/ServiceConnection;
    invoke-static {v0, v1}, Lcom/konka/lockscreen/service/FaceService;->access$102(Lcom/konka/lockscreen/service/FaceService;Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;

    iget-object v0, p0, Lcom/konka/lockscreen/service/FaceService$1;->this$0:Lcom/konka/lockscreen/service/FaceService;

    # setter for: Lcom/konka/lockscreen/service/FaceService;->mFaceRecognitionService:Lcom/konka/facerecognition/IFaceRecognitionService;
    invoke-static {v0, v1}, Lcom/konka/lockscreen/service/FaceService;->access$002(Lcom/konka/lockscreen/service/FaceService;Lcom/konka/facerecognition/IFaceRecognitionService;)Lcom/konka/facerecognition/IFaceRecognitionService;

    # setter for: Lcom/konka/lockscreen/service/FaceService;->mFaceService:Lcom/konka/lockscreen/service/FaceService;
    invoke-static {v1}, Lcom/konka/lockscreen/service/FaceService;->access$202(Lcom/konka/lockscreen/service/FaceService;)Lcom/konka/lockscreen/service/FaceService;

    return-void
.end method
