.class public Lcom/konka/lockscreen/service/MessageService;
.super Ljava/lang/Object;
.source "MessageService.java"


# static fields
.field private static mUpdate:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static destroyUpdatesService(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;

    :try_start_0
    sget-object v0, Lcom/konka/lockscreen/service/MessageService;->mUpdate:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getSystemMsgCount(Landroid/content/Context;)I
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x0

    const/4 v2, 0x0

    const-string v0, "content://com.konka.message.contentprovider/MessageTable"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, "BusinessId=0 and ReadedFlag=0"

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    :goto_0
    return v6

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getUpdatesService(Landroid/content/Context;Lcom/konka/lockscreen/service/IMessageCallback;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/konka/lockscreen/service/IMessageCallback;

    :try_start_0
    new-instance v1, Lcom/konka/lockscreen/service/MessageService$1;

    invoke-direct {v1, p1}, Lcom/konka/lockscreen/service/MessageService$1;-><init>(Lcom/konka/lockscreen/service/IMessageCallback;)V

    sput-object v1, Lcom/konka/lockscreen/service/MessageService;->mUpdate:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.market.main"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v1, Lcom/konka/lockscreen/service/MessageService;->mUpdate:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method
