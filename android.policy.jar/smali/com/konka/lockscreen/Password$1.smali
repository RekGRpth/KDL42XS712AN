.class Lcom/konka/lockscreen/Password$1;
.super Ljava/lang/Object;
.source "Password.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/Password;->initUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/Password;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/Password;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/Password$1;->this$0:Lcom/konka/lockscreen/Password;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/konka/lockscreen/Password$1;->this$0:Lcom/konka/lockscreen/Password;

    # getter for: Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/lockscreen/Password;->access$000(Lcom/konka/lockscreen/Password;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    iget-object v1, p0, Lcom/konka/lockscreen/Password$1;->this$0:Lcom/konka/lockscreen/Password;

    sget-object v2, Lcom/konka/lockscreen/PasswordMode;->LOCK:Lcom/konka/lockscreen/PasswordMode;

    invoke-virtual {v1, v2}, Lcom/konka/lockscreen/Password;->setPasswordMode(Lcom/konka/lockscreen/PasswordMode;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
