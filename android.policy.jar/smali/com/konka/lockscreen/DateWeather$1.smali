.class Lcom/konka/lockscreen/DateWeather$1;
.super Landroid/os/Handler;
.source "DateWeather.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/lockscreen/DateWeather;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/DateWeather;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/DateWeather;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/DateWeather$1;->this$0:Lcom/konka/lockscreen/DateWeather;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$1;->this$0:Lcom/konka/lockscreen/DateWeather;

    # getter for: Lcom/konka/lockscreen/DateWeather;->mWeather:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/lockscreen/DateWeather;->access$300(Lcom/konka/lockscreen/DateWeather;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/konka/lockscreen/DateWeather$1;->this$0:Lcom/konka/lockscreen/DateWeather;

    # getter for: Lcom/konka/lockscreen/DateWeather;->mWeatherInfo:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/lockscreen/DateWeather;->access$000(Lcom/konka/lockscreen/DateWeather;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/lockscreen/DateWeather$1;->this$0:Lcom/konka/lockscreen/DateWeather;

    # getter for: Lcom/konka/lockscreen/DateWeather;->mTempInfo:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/lockscreen/DateWeather;->access$100(Lcom/konka/lockscreen/DateWeather;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/lockscreen/DateWeather$1;->this$0:Lcom/konka/lockscreen/DateWeather;

    # getter for: Lcom/konka/lockscreen/DateWeather;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/konka/lockscreen/DateWeather;->access$200(Lcom/konka/lockscreen/DateWeather;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x10400c8    # android.R.string.konka_lockscreen_degree

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$1;->this$0:Lcom/konka/lockscreen/DateWeather;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/DateWeather;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$1;->this$0:Lcom/konka/lockscreen/DateWeather;

    # invokes: Lcom/konka/lockscreen/DateWeather;->updateTime()V
    invoke-static {v0}, Lcom/konka/lockscreen/DateWeather;->access$400(Lcom/konka/lockscreen/DateWeather;)V

    const-string v0, "DateWeather"

    const-string v1, "show date info"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
