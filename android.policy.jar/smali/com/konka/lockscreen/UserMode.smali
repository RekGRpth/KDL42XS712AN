.class public final enum Lcom/konka/lockscreen/UserMode;
.super Ljava/lang/Enum;
.source "UserMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/lockscreen/UserMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/lockscreen/UserMode;

.field public static final enum ACTIVE_MODE:Lcom/konka/lockscreen/UserMode;

.field public static final enum LEFT_MODE:Lcom/konka/lockscreen/UserMode;

.field public static final enum RIGHT_MODE:Lcom/konka/lockscreen/UserMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/lockscreen/UserMode;

    const-string v1, "LEFT_MODE"

    invoke-direct {v0, v1, v2}, Lcom/konka/lockscreen/UserMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/lockscreen/UserMode;->LEFT_MODE:Lcom/konka/lockscreen/UserMode;

    new-instance v0, Lcom/konka/lockscreen/UserMode;

    const-string v1, "ACTIVE_MODE"

    invoke-direct {v0, v1, v3}, Lcom/konka/lockscreen/UserMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/lockscreen/UserMode;->ACTIVE_MODE:Lcom/konka/lockscreen/UserMode;

    new-instance v0, Lcom/konka/lockscreen/UserMode;

    const-string v1, "RIGHT_MODE"

    invoke-direct {v0, v1, v4}, Lcom/konka/lockscreen/UserMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/lockscreen/UserMode;->RIGHT_MODE:Lcom/konka/lockscreen/UserMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/lockscreen/UserMode;

    sget-object v1, Lcom/konka/lockscreen/UserMode;->LEFT_MODE:Lcom/konka/lockscreen/UserMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/lockscreen/UserMode;->ACTIVE_MODE:Lcom/konka/lockscreen/UserMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/lockscreen/UserMode;->RIGHT_MODE:Lcom/konka/lockscreen/UserMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/lockscreen/UserMode;->$VALUES:[Lcom/konka/lockscreen/UserMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/lockscreen/UserMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/konka/lockscreen/UserMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/lockscreen/UserMode;

    return-object v0
.end method

.method public static values()[Lcom/konka/lockscreen/UserMode;
    .locals 1

    sget-object v0, Lcom/konka/lockscreen/UserMode;->$VALUES:[Lcom/konka/lockscreen/UserMode;

    invoke-virtual {v0}, [Lcom/konka/lockscreen/UserMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/lockscreen/UserMode;

    return-object v0
.end method
