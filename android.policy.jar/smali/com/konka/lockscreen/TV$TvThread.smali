.class Lcom/konka/lockscreen/TV$TvThread;
.super Ljava/lang/Thread;
.source "TV.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/lockscreen/TV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TvThread"
.end annotation


# instance fields
.field private h:I

.field private holder:Landroid/view/SurfaceHolder;

.field final synthetic this$0:Lcom/konka/lockscreen/TV;

.field private w:I

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>(Lcom/konka/lockscreen/TV;Landroid/view/SurfaceHolder;IIII)V
    .locals 0
    .param p2    # Landroid/view/SurfaceHolder;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    iput-object p1, p0, Lcom/konka/lockscreen/TV$TvThread;->this$0:Lcom/konka/lockscreen/TV;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/konka/lockscreen/TV$TvThread;->holder:Landroid/view/SurfaceHolder;

    iput p3, p0, Lcom/konka/lockscreen/TV$TvThread;->x:I

    iput p4, p0, Lcom/konka/lockscreen/TV$TvThread;->y:I

    iput p5, p0, Lcom/konka/lockscreen/TV$TvThread;->w:I

    iput p6, p0, Lcom/konka/lockscreen/TV$TvThread;->h:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    invoke-super {p0}, Ljava/lang/Thread;->run()V

    iget-object v3, p0, Lcom/konka/lockscreen/TV$TvThread;->this$0:Lcom/konka/lockscreen/TV;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/konka/lockscreen/TV$TvThread;->this$0:Lcom/konka/lockscreen/TV;

    # getter for: Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/lockscreen/TV;->access$000(Lcom/konka/lockscreen/TV;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TvThread Surface ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/lockscreen/TV$TvThread;->holder:Landroid/view/SurfaceHolder;

    invoke-interface {v5}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iget v2, p0, Lcom/konka/lockscreen/TV$TvThread;->x:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iget v2, p0, Lcom/konka/lockscreen/TV$TvThread;->y:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iget v2, p0, Lcom/konka/lockscreen/TV$TvThread;->w:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iget v2, p0, Lcom/konka/lockscreen/TV$TvThread;->h:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iget-object v2, p0, Lcom/konka/lockscreen/TV$TvThread;->this$0:Lcom/konka/lockscreen/TV;

    # getter for: Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/lockscreen/TV;->access$000(Lcom/konka/lockscreen/TV;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TvThread TV Window(x="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/konka/lockscreen/TV$TvThread;->x:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",y="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/konka/lockscreen/TV$TvThread;->y:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",w="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/konka/lockscreen/TV$TvThread;->w:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",h="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/konka/lockscreen/TV$TvThread;->h:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z

    iget-object v2, p0, Lcom/konka/lockscreen/TV$TvThread;->this$0:Lcom/konka/lockscreen/TV;

    # getter for: Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/lockscreen/TV;->access$000(Lcom/konka/lockscreen/TV;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "TvThread Success"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v3

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
