.class public Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;
.super Ljava/lang/Object;
.source "LockScreenPasswordTransformationMethod.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/text/method/TransformationMethod;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$ViewReference;,
        Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;,
        Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$PasswordCharSequence;
    }
.end annotation


# static fields
.field private static DOT:C

.field private static final TextKeyListener_ACTIVE:Ljava/lang/Object;

.field private static sInstance:Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    sput-object v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->TextKeyListener_ACTIVE:Ljava/lang/Object;

    const/16 v0, 0x2022

    sput-char v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->DOT:C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->TextKeyListener_ACTIVE:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200()C
    .locals 1

    sget-char v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->DOT:C

    return v0
.end method

.method public static getInstance()Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;
    .locals 1

    sget-object v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->sInstance:Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->sInstance:Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    invoke-direct {v0}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;-><init>()V

    sput-object v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->sInstance:Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    sget-object v0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->sInstance:Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    goto :goto_0
.end method

.method private static removeVisibleSpans(Landroid/text/Spannable;)V
    .locals 5
    .param p0    # Landroid/text/Spannable;

    const/4 v2, 0x0

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v3

    const-class v4, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;

    invoke-interface {p0, v2, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    invoke-interface {p0, v2}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/view/View;

    const/4 v5, 0x0

    instance-of v3, p1, Landroid/text/Spannable;

    if-eqz v3, :cond_1

    move-object v1, p1

    check-cast v1, Landroid/text/Spannable;

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v3

    const-class v4, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$ViewReference;

    invoke-interface {v1, v5, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$ViewReference;

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    invoke-interface {v1, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->removeVisibleSpans(Landroid/text/Spannable;)V

    new-instance v3, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$ViewReference;

    invoke-direct {v3, p2}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$ViewReference;-><init>(Landroid/view/View;)V

    const/16 v4, 0x22

    invoke-interface {v1, v3, v5, v5, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    new-instance v3, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$PasswordCharSequence;

    invoke-direct {v3, p1}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$PasswordCharSequence;-><init>(Ljava/lang/CharSequence;)V

    return-object v3
.end method

.method public onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Z
    .param p4    # I
    .param p5    # Landroid/graphics/Rect;

    if-nez p3, :cond_0

    instance-of v1, p2, Landroid/text/Spannable;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->removeVisibleSpans(Landroid/text/Spannable;)V

    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    instance-of v4, p1, Landroid/text/Spannable;

    if-eqz v4, :cond_0

    move-object v1, p1

    check-cast v1, Landroid/text/Spannable;

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    const-class v6, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$ViewReference;

    invoke-interface {v1, v4, v5, v6}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$ViewReference;

    array-length v4, v3

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_1
    if-nez v2, :cond_2

    array-length v4, v3

    if-ge v0, v4, :cond_2

    aget-object v4, v3, v0

    invoke-virtual {v4}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$ViewReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_0

    if-lez p4, :cond_0

    invoke-static {v1}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->removeVisibleSpans(Landroid/text/Spannable;)V

    const/4 v4, 0x1

    if-ne p4, v4, :cond_0

    new-instance v4, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;

    invoke-direct {v4, v1, p0}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;-><init>(Landroid/text/Spannable;Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;)V

    add-int v5, p2, p4

    const/16 v6, 0x21

    invoke-interface {v1, v4, p2, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method
