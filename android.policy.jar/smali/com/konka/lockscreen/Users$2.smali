.class Lcom/konka/lockscreen/Users$2;
.super Landroid/app/IUserSwitchObserver$Stub;
.source "Users.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/Users;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/Users;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/Users;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    invoke-direct {p0}, Landroid/app/IUserSwitchObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onUserSwitchComplete(I)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    # getter for: Lcom/konka/lockscreen/Users;->bDone:Z
    invoke-static {v1}, Lcom/konka/lockscreen/Users;->access$100(Lcom/konka/lockscreen/Users;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    # setter for: Lcom/konka/lockscreen/Users;->bDone:Z
    invoke-static {v1, v2}, Lcom/konka/lockscreen/Users;->access$102(Lcom/konka/lockscreen/Users;Z)Z

    iget-object v1, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    # invokes: Lcom/konka/lockscreen/Users;->done_real()V
    invoke-static {v1}, Lcom/konka/lockscreen/Users;->access$200(Lcom/konka/lockscreen/Users;)V

    :cond_0
    iget-object v1, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    # getter for: Lcom/konka/lockscreen/Users;->bInitSwitch:Z
    invoke-static {v1}, Lcom/konka/lockscreen/Users;->access$300(Lcom/konka/lockscreen/Users;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    # setter for: Lcom/konka/lockscreen/Users;->bInitSwitch:Z
    invoke-static {v1, v2}, Lcom/konka/lockscreen/Users;->access$302(Lcom/konka/lockscreen/Users;Z)Z

    iget-object v1, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    # getter for: Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;
    invoke-static {v1}, Lcom/konka/lockscreen/Users;->access$500(Lcom/konka/lockscreen/Users;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    # getter for: Lcom/konka/lockscreen/Users;->mCurrent:I
    invoke-static {v2}, Lcom/konka/lockscreen/Users;->access$400(Lcom/konka/lockscreen/Users;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/UserInfo;

    iget v0, v1, Landroid/content/pm/UserInfo;->id:I

    iget-object v1, p0, Lcom/konka/lockscreen/Users$2;->this$0:Lcom/konka/lockscreen/Users;

    # getter for: Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;
    invoke-static {v1}, Lcom/konka/lockscreen/Users;->access$600(Lcom/konka/lockscreen/Users;)Lcom/konka/lockscreen/LockScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/lockscreen/LockScreen;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/internal/widget/LockPatternUtils;->setCurrentUser(I)V

    if-eqz v0, :cond_1

    const-string v1, "Users"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopUser ID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/app/IActivityManager;->stopUser(ILandroid/app/IStopUserCallback;)I

    :cond_1
    return-void
.end method

.method public onUserSwitching(ILandroid/os/IRemoteCallback;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/IRemoteCallback;

    return-void
.end method
