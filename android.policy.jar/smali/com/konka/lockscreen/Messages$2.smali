.class Lcom/konka/lockscreen/Messages$2;
.super Landroid/os/Handler;
.source "Messages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/Messages;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/Messages;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/Messages;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/konka/lockscreen/Messages$2;->this$0:Lcom/konka/lockscreen/Messages;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Messages$2;->this$0:Lcom/konka/lockscreen/Messages;

    # getter for: Lcom/konka/lockscreen/Messages;->mUpdateNum:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/lockscreen/Messages;->access$300(Lcom/konka/lockscreen/Messages;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/lockscreen/Messages$2;->this$0:Lcom/konka/lockscreen/Messages;

    # getter for: Lcom/konka/lockscreen/Messages;->mUpdateInfo:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/lockscreen/Messages;->access$000(Lcom/konka/lockscreen/Messages;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
