.class Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver$1;
.super Ljava/lang/Object;
.source "DateWeather.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver;

.field final synthetic val$clock:Lcom/konka/lockscreen/DateWeather;

.field final synthetic val$timezoneChanged:Z


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver;ZLcom/konka/lockscreen/DateWeather;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver$1;->this$0:Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver;

    iput-boolean p2, p0, Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver$1;->val$timezoneChanged:Z

    iput-object p3, p0, Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver$1;->val$clock:Lcom/konka/lockscreen/DateWeather;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver$1;->val$timezoneChanged:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver$1;->val$clock:Lcom/konka/lockscreen/DateWeather;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    # setter for: Lcom/konka/lockscreen/DateWeather;->mCalendar:Ljava/util/Calendar;
    invoke-static {v0, v1}, Lcom/konka/lockscreen/DateWeather;->access$602(Lcom/konka/lockscreen/DateWeather;Ljava/util/Calendar;)Ljava/util/Calendar;

    :cond_0
    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$TimeChangedReceiver$1;->val$clock:Lcom/konka/lockscreen/DateWeather;

    # invokes: Lcom/konka/lockscreen/DateWeather;->updateTime()V
    invoke-static {v0}, Lcom/konka/lockscreen/DateWeather;->access$400(Lcom/konka/lockscreen/DateWeather;)V

    return-void
.end method
