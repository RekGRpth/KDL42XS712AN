.class Lcom/android/wallpaper/grass/GrassView;
.super Landroid/renderscript/RSSurfaceView;
.source "GrassView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Landroid/renderscript/RSSurfaceView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/android/wallpaper/grass/GrassView;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Lcom/android/wallpaper/grass/GrassView;->setFocusableInTouchMode(Z)V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 6
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v5, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/renderscript/RSSurfaceView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    new-instance v2, Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    invoke-direct {v2}, Landroid/renderscript/RenderScriptGL$SurfaceConfig;-><init>()V

    invoke-virtual {p0, v2}, Lcom/android/wallpaper/grass/GrassView;->createRenderScriptGL(Landroid/renderscript/RenderScriptGL$SurfaceConfig;)Landroid/renderscript/RenderScriptGL;

    move-result-object v0

    new-instance v1, Lcom/android/wallpaper/grass/GrassRS;

    invoke-virtual {p0}, Lcom/android/wallpaper/grass/GrassView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, p3, p4}, Lcom/android/wallpaper/grass/GrassRS;-><init>(Landroid/content/Context;II)V

    invoke-virtual {p0}, Lcom/android/wallpaper/grass/GrassView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1, v0, v3, v5}, Lcom/android/wallpaper/grass/GrassRS;->init(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;Z)V

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v5, v5}, Lcom/android/wallpaper/grass/GrassRS;->setOffset(FFII)V

    invoke-virtual {v1}, Lcom/android/wallpaper/grass/GrassRS;->start()V

    return-void
.end method
