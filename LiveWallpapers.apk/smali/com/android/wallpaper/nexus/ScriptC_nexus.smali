.class public Lcom/android/wallpaper/nexus/ScriptC_nexus;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_nexus.java"


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private mExportVar_gIsPreview:I

.field private mExportVar_gMode:I

.field private mExportVar_gPFTexture:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFTexture565:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPSBlend:Landroid/renderscript/ProgramStore;

.field private mExportVar_gTBackground:Landroid/renderscript/Allocation;

.field private mExportVar_gTGlow:Landroid/renderscript/Allocation;

.field private mExportVar_gTPulse:Landroid/renderscript/Allocation;

.field private mExportVar_gWorldScaleX:F

.field private mExportVar_gWorldScaleY:F

.field private mExportVar_gXOffset:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->__ALLOCATION:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public invoke_addTap(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/renderscript/FieldPacker;->addI32(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addI32(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->invoke(ILandroid/renderscript/FieldPacker;)V

    return-void
.end method

.method public invoke_initPulses()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->invoke(I)V

    return-void
.end method

.method public declared-synchronized set_gIsPreview(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(II)V

    iput p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gIsPreview:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gMode(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(II)V

    iput p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gMode:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPFTexture(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPFTexture:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPFTexture565(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/4 v0, 0x7

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPFTexture565:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPSBlend(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    monitor-enter p0

    const/4 v0, 0x6

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gPSBlend:Landroid/renderscript/ProgramStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTBackground(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x8

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTBackground:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTGlow(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xa

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTGlow:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTPulse(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x9

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gTPulse:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gWorldScaleX(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gWorldScaleX:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gWorldScaleY(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gWorldScaleY:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gXOffset(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/nexus/ScriptC_nexus;->mExportVar_gXOffset:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
