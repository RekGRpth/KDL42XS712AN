.class public Lcom/android/wallpaper/galaxy/ScriptC_galaxy;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_galaxy.java"


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_Particles:Lcom/android/wallpaper/galaxy/ScriptField_Particle;

.field private mExportVar_gIsPreview:I

.field private mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFStars:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPSLights:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPVBkProj:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPVStars:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gParticlesMesh:Landroid/renderscript/Mesh;

.field private mExportVar_gTFlares:Landroid/renderscript/Allocation;

.field private mExportVar_gTLight1:Landroid/renderscript/Allocation;

.field private mExportVar_gTSpace:Landroid/renderscript/Allocation;

.field private mExportVar_gXOffset:F

.field private mExportVar_vpConstants:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->__MESH:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_Particles(Lcom/android/wallpaper/galaxy/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/galaxy/ScriptField_Particle;

    const/16 v1, 0xb

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_Particles:Lcom/android/wallpaper/galaxy/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/wallpaper/galaxy/ScriptField_Particle;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_vpConstants(Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    const/16 v1, 0xc

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_vpConstants:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public declared-synchronized set_gIsPreview(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(II)V

    iput p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gIsPreview:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPFBackground(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPFStars(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPFStars:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPSLights(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    monitor-enter p0

    const/4 v0, 0x6

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPSLights:Landroid/renderscript/ProgramStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPVBkProj(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPVBkProj:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPVStars(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gPVStars:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gParticlesMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/16 v0, 0xa

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gParticlesMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTFlares(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x8

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTFlares:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTLight1(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x9

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTLight1:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTSpace(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/4 v0, 0x7

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gTSpace:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gXOffset(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->mExportVar_gXOffset:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
