.class public final Ljax;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:Ljbh;

.field public c:[Ljbd;

.field public d:Ljbb;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Ljax;->a:[I

    iput-object v1, p0, Ljax;->b:Ljbh;

    invoke-static {}, Ljbd;->c()[Ljbd;

    move-result-object v0

    iput-object v0, p0, Ljax;->c:[Ljbd;

    iput-object v1, p0, Ljax;->d:Ljbb;

    const/4 v0, -0x1

    iput v0, p0, Ljax;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v3

    iget-object v0, p0, Ljax;->a:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljax;->a:[I

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v4, p0, Ljax;->a:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Ljax;->a:[I

    aget v4, v4, v0

    invoke-static {v4}, Lizn;->a(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int v0, v3, v2

    iget-object v2, p0, Ljax;->a:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :goto_1
    iget-object v2, p0, Ljax;->b:Ljbh;

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Ljax;->b:Ljbh;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Ljax;->c:[Ljbd;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljax;->c:[Ljbd;

    array-length v2, v2

    if-lez v2, :cond_3

    :goto_2
    iget-object v2, p0, Ljax;->c:[Ljbd;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    iget-object v2, p0, Ljax;->c:[Ljbd;

    aget-object v2, v2, v1

    if-eqz v2, :cond_2

    const/4 v3, 0x5

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Ljax;->d:Ljbb;

    if-eqz v1, :cond_4

    const/16 v1, 0x8

    iget-object v2, p0, Ljax;->d:Ljbb;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Ljax;->C:I

    return v0

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 7

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lizm;->a()I

    :cond_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljax;->a:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Ljax;->a:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljax;->a:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Ljax;->a:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljax;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lizm;->e(I)V

    iget-object v1, p0, Ljax;->a:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Ljax;->a:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lizm;->k()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lizm;->g()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Ljax;->a:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Ljax;->a:[I

    :cond_a
    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Ljax;->b:Ljbh;

    if-nez v0, :cond_b

    new-instance v0, Ljbh;

    invoke-direct {v0}, Ljbh;-><init>()V

    iput-object v0, p0, Ljax;->b:Ljbh;

    :cond_b
    iget-object v0, p0, Ljax;->b:Ljbh;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v1

    iget-object v0, p0, Ljax;->c:[Ljbd;

    if-nez v0, :cond_d

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Ljbd;

    if-eqz v0, :cond_c

    iget-object v3, p0, Ljax;->c:[Ljbd;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_e

    new-instance v3, Ljbd;

    invoke-direct {v3}, Ljbd;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Ljax;->c:[Ljbd;

    array-length v0, v0

    goto :goto_7

    :cond_e
    new-instance v3, Ljbd;

    invoke-direct {v3}, Ljbd;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v1, p0, Ljax;->c:[Ljbd;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ljax;->d:Ljbb;

    if-nez v0, :cond_f

    new-instance v0, Ljbb;

    invoke-direct {v0}, Ljbb;-><init>()V

    iput-object v0, p0, Ljax;->d:Ljbb;

    :cond_f
    iget-object v0, p0, Ljax;->d:Ljbb;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x2a -> :sswitch_4
        0x42 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Ljax;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljax;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljax;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Ljax;->a:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lizn;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ljax;->b:Ljbh;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v2, p0, Ljax;->b:Ljbh;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Ljax;->c:[Ljbd;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljax;->c:[Ljbd;

    array-length v0, v0

    if-lez v0, :cond_3

    :goto_1
    iget-object v0, p0, Ljax;->c:[Ljbd;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Ljax;->c:[Ljbd;

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ljax;->d:Ljbb;

    if-eqz v0, :cond_4

    const/16 v0, 0x8

    iget-object v1, p0, Ljax;->d:Ljbb;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_4
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljax;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljax;

    iget-object v2, p0, Ljax;->a:[I

    iget-object v3, p1, Ljax;->a:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljax;->b:Ljbh;

    if-nez v2, :cond_4

    iget-object v2, p1, Ljax;->b:Ljbh;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljax;->b:Ljbh;

    iget-object v3, p1, Ljax;->b:Ljbh;

    invoke-virtual {v2, v3}, Ljbh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljax;->c:[Ljbd;

    iget-object v3, p1, Ljax;->c:[Ljbd;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljax;->d:Ljbb;

    if-nez v2, :cond_7

    iget-object v2, p1, Ljax;->d:Ljbb;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljax;->d:Ljbb;

    iget-object v3, p1, Ljax;->d:Ljbb;

    invoke-virtual {v2, v3}, Ljbb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ljax;->a:[I

    invoke-static {v0}, Lizq;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljax;->b:Ljbh;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljax;->c:[Ljbd;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljax;->d:Ljbb;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljax;->b:Ljbh;

    invoke-virtual {v0}, Ljbh;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ljax;->d:Ljbb;

    invoke-virtual {v1}, Ljbb;->hashCode()I

    move-result v1

    goto :goto_1
.end method
