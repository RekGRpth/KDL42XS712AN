.class public final Lecm;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Lbel;
.implements Ldle;


# instance fields
.field private ad:Lebj;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxe;-><init>()V

    return-void
.end method

.method private V()V
    .locals 3

    invoke-virtual {p0}, Lecm;->J()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ReqSummListFragment"

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lecm;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->b()V

    sget-object v0, Lcte;->o:Ldlf;

    invoke-virtual {p0}, Lecm;->N()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ldlf;->c(Lbdu;Ljava/lang/String;)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method


# virtual methods
.method public final F_()V
    .locals 0

    invoke-direct {p0}, Lecm;->V()V

    return-void
.end method

.method public final G_()V
    .locals 0

    invoke-direct {p0}, Lecm;->V()V

    return-void
.end method

.method public final M_()V
    .locals 3

    invoke-super {p0}, Ldxe;->M_()V

    invoke-virtual {p0}, Lecm;->K()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ReqSummListFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lecm;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->o:Ldlf;

    invoke-virtual {p0}, Lecm;->M()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ldlf;->b(Lbdu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final R()V
    .locals 0

    invoke-direct {p0}, Lecm;->V()V

    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040066    # com.google.android.gms.R.layout.games_inbox_list_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;)V
    .locals 4

    invoke-virtual {p0}, Lecm;->M()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lecm;->ad:Lebj;

    invoke-virtual {p0}, Lecm;->N()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lecm;->O()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lebj;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcte;->o:Ldlf;

    invoke-virtual {p0}, Lecm;->N()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ldlf;->c(Lbdu;Ljava/lang/String;)Lbeh;

    move-result-object v1

    invoke-interface {v1, p0}, Lbeh;->a(Lbel;)V

    sget-object v1, Lcte;->n:Lctp;

    const/4 v2, 0x4

    invoke-interface {v1, p1, v0, v2}, Lctp;->a(Lbdu;Ljava/lang/String;I)V

    return-void
.end method

.method public final synthetic a(Lbek;)V
    .locals 5

    check-cast p1, Ldlg;

    invoke-interface {p1}, Ldlg;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-interface {p1}, Ldlg;->g()Ldlc;

    move-result-object v2

    :try_start_0
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {v2}, Ldlc;->b()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lecm;->Y:Ldvn;

    invoke-virtual {v0, v1}, Ldvn;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, Ldlc;->b()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v0, p0, Lecm;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    iget-object v0, p0, Lecm;->ad:Lebj;

    invoke-virtual {v0, v2}, Lebj;->a(Lbgo;)V

    invoke-virtual {v2}, Ldlc;->a()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lecm;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    invoke-virtual {p0}, Lecm;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcte;->o:Ldlf;

    invoke-virtual {p0}, Lecm;->M()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, p0, v2}, Ldlf;->a(Lbdu;Ldle;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    :try_start_3
    iget-object v0, p0, Lecm;->Z:Leds;

    const v3, 0x7f0b02c8    # com.google.android.gms.R.string.games_match_inbox_list_generic_error

    const v4, 0x7f0b02c7    # com.google.android.gms.R.string.games_match_inbox_list_null_state

    invoke-virtual {v0, v1, v3, v4}, Leds;->a(III)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ldlc;->b()V

    throw v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lecm;->Y:Ldvn;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessInboxListActivity;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecm;->Y:Ldvn;

    check-cast v0, Ldzy;

    invoke-interface {v0}, Ldzy;->D_()Ldzx;

    move-result-object v0

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v1, Lebj;

    iget-object v2, p0, Lecm;->Y:Ldvn;

    invoke-direct {v1, v2, v0}, Lebj;-><init>(Ldvn;Lebk;)V

    iput-object v1, p0, Lecm;->ad:Lebj;

    iget-object v0, p0, Lecm;->ad:Lebj;

    invoke-virtual {p0, v0}, Lecm;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lecm;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lecm;->ad:Lebj;

    invoke-virtual {v0}, Lebj;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method
