.class public final Lble;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:Ljava/util/List;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lizk;-><init>()V

    iput v1, p0, Lble;->b:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lble;->c:Ljava/util/List;

    iput-boolean v1, p0, Lble;->e:Z

    iput-boolean v1, p0, Lble;->g:Z

    const/4 v0, -0x1

    iput v0, p0, Lble;->h:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lble;->h:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lble;->b()I

    :cond_0
    iget v0, p0, Lble;->h:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iget-object v1, p0, Lble;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lble;->c:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lble;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v2, p0, Lble;->d:Z

    iput-boolean v0, p0, Lble;->e:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v2, p0, Lble;->f:Z

    iput-boolean v0, p0, Lble;->g:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->c()I

    move-result v0

    iput-boolean v2, p0, Lble;->a:Z

    iput v0, p0, Lble;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-object v0, p0, Lble;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lizh;->a(II)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lble;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lble;->e:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_1
    iget-boolean v0, p0, Lble;->f:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lble;->g:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_2
    iget-boolean v0, p0, Lble;->a:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lble;->b:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_3
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lble;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lizh;->a(I)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x0

    iget-object v1, p0, Lble;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget-boolean v1, p0, Lble;->d:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lble;->e:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lble;->f:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Lble;->g:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lble;->a:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Lble;->b:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lble;->h:I

    return v0
.end method
