.class public final Leez;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field protected a:I

.field protected b:I

.field protected c:Ljava/lang/String;

.field d:Z

.field private e:Leeu;

.field private f:Landroid/content/Intent;

.field private g:Line;

.field private h:J


# direct methods
.method public constructor <init>(Leeu;Landroid/content/Intent;Line;J)V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Leez;->d:Z

    iput-object p1, p0, Leez;->e:Leeu;

    iput-object p2, p0, Leez;->f:Landroid/content/Intent;

    iput-object p3, p0, Leez;->g:Line;

    iput-wide p4, p0, Leez;->h:J

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Leez;->d:Z

    if-nez v0, :cond_0

    const-string v0, "GCM/DMM"

    const-string v1, "Force release of GOOGLE_C2DM lock"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Leez;->e:Leeu;

    invoke-virtual {v0}, Leeu;->b()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leez;->d:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Leez;->h:J

    sub-long v1, v0, v2

    invoke-virtual {p0}, Leez;->getResultCode()I

    move-result v0

    iput v0, p0, Leez;->a:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Received "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Leez;->g:Line;

    iget-object v3, v3, Line;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, p0, Leez;->a:I

    if-nez v0, :cond_1

    const-string v0, " CANCELED "

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " time="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    iget-object v0, p0, Leez;->e:Leeu;

    iget-object v1, p0, Leez;->f:Landroid/content/Intent;

    iget-object v2, p0, Leez;->g:Line;

    iget v3, p0, Leez;->a:I

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Leez;->getResultExtras(Z)Landroid/os/Bundle;

    iget v4, p0, Leez;->b:I

    iget-object v5, p0, Leez;->c:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Leeu;->a(Landroid/content/Intent;Line;IILjava/lang/String;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Leez;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Leez;->e:Leeu;

    invoke-virtual {v0}, Leeu;->b()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leez;->d:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_1
    const-string v0, ""

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
