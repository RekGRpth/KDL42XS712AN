.class final Lcxe;
.super Lcwk;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcwm;

.field private final b:Ldfz;


# direct methods
.method constructor <init>(Lcwm;Ldfz;)V
    .locals 0

    iput-object p1, p0, Lcxe;->a:Lcwm;

    invoke-direct {p0}, Lcwk;-><init>()V

    iput-object p2, p0, Lcxe;->b:Ldfz;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcxe;->a:Lcwm;

    new-instance v1, Lcxg;

    iget-object v2, p0, Lcxe;->a:Lcwm;

    iget-object v3, p0, Lcxe;->b:Ldfz;

    invoke-direct {v1, v2, v3, p1}, Lcxg;-><init>(Lcwm;Ldfz;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final k(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    new-instance v1, Ldfu;

    invoke-direct {v1, p1}, Ldfu;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Ldfu;->a()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ldfu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Ldfu;->b()V

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcxe;->a:Lcwm;

    new-instance v2, Lcxf;

    iget-object v3, p0, Lcxe;->a:Lcwm;

    iget-object v4, p0, Lcxe;->b:Ldfz;

    invoke-direct {v2, v3, v4, v0}, Lcxf;-><init>(Lcwm;Ldfz;Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-virtual {v1, v2}, Lcwm;->a(Lbjg;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldfu;->b()V

    throw v0
.end method
