.class public abstract Lbol;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/view/LayoutInflater;

.field protected final c:I

.field d:Lbon;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lbol;->e:I

    invoke-virtual {p0}, Lbol;->a()I

    move-result v0

    iput v0, p0, Lbol;->c:I

    new-instance v0, Lboq;

    invoke-direct {v0, p0}, Lboq;-><init>(Lbol;)V

    iput-object v0, p0, Lbol;->d:Lbon;

    iput-object p1, p0, Lbol;->a:Landroid/content/Context;

    iget-object v0, p0, Lbol;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lbol;->b:Landroid/view/LayoutInflater;

    return-void
.end method

.method static synthetic a(Lbol;I)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbol;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a()I
    .locals 2

    iget v0, p0, Lbol;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lbol;->e:I

    return v0
.end method

.method protected final a(Landroid/view/View;I)Landroid/view/View;
    .locals 2

    if-eqz p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    if-lez p2, :cond_1

    iget-object v0, p0, Lbol;->b:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No usable view found and no layout provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a(Lbon;)V
    .locals 0

    iput-object p1, p0, Lbol;->d:Lbon;

    invoke-virtual {p0}, Lbol;->b()V

    invoke-virtual {p0}, Lbol;->notifyDataSetChanged()V

    return-void
.end method

.method protected b()V
    .locals 0

    return-void
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lbol;->d:Lbon;

    invoke-virtual {v0}, Lbon;->a()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbol;->d:Lbon;

    invoke-virtual {v0, p1}, Lbon;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    iget-object v0, p0, Lbol;->d:Lbon;

    invoke-virtual {v0, p1}, Lbon;->a(I)Ljava/lang/Object;

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lbol;->d:Lbon;

    invoke-virtual {v0, p1}, Lbon;->b(I)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    iget-object v1, p0, Lbol;->d:Lbon;

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, p2, p3, v0}, Lbon;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    iget v0, p0, Lbol;->e:I

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
