.class public final Liam;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Liab;
.implements Lifq;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final A:Lhpd;

.field private final b:Lcom/google/android/location/internal/server/GoogleLocationService;

.field private final c:Landroid/location/LocationManager;

.field private d:Z

.field private e:Z

.field private f:Z

.field private final g:Ljava/lang/Object;

.field private h:Licp;

.field private i:Ljava/lang/Boolean;

.field private final j:Ljava/util/LinkedHashMap;

.field private k:Z

.field private l:Lifo;

.field private m:Lhoc;

.field private n:Lhod;

.field private o:I

.field private p:I

.field private q:Lilx;

.field private final r:Liai;

.field private s:Lifn;

.field private final t:Ljava/util/Map;

.field private final u:Ljava/util/Map;

.field private v:J

.field private final w:I

.field private x:Landroid/database/ContentObserver;

.field private y:Lhjy;

.field private final z:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v0, Licj;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "gms"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NlpServiceThread"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Liam;->a:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, "gmm"

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/location/internal/server/GoogleLocationService;Landroid/os/Looper;Lhon;)V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0xa

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-boolean v1, p0, Liam;->d:Z

    iput-boolean v1, p0, Liam;->e:Z

    iput-boolean v1, p0, Liam;->f:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Liam;->g:Ljava/lang/Object;

    new-instance v0, Lian;

    invoke-direct {v0, p0}, Lian;-><init>(Liam;)V

    iput-object v0, p0, Liam;->j:Ljava/util/LinkedHashMap;

    iput-boolean v1, p0, Liam;->k:Z

    iput v2, p0, Liam;->o:I

    iput v2, p0, Liam;->p:I

    iput-object v4, p0, Liam;->q:Lilx;

    iput-object v4, p0, Liam;->s:Lifn;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Liam;->t:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Liam;->u:Ljava/util/Map;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Liam;->v:J

    iput-object p1, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Lcom/google/android/location/internal/server/GoogleLocationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Liam;->c:Landroid/location/LocationManager;

    invoke-static {p1}, Liam;->a(Landroid/content/Context;)Liad;

    move-result-object v0

    iget v0, v0, Liad;->d:I

    iput v0, p0, Liam;->w:I

    new-instance v0, Liai;

    iget v1, p0, Liam;->w:I

    invoke-direct {v0, v1, p3}, Liai;-><init>(ILhon;)V

    iput-object v0, p0, Liam;->r:Liai;

    new-instance v0, Liao;

    invoke-direct {v0, p0, p0}, Liao;-><init>(Liam;Landroid/os/Handler;)V

    iput-object v0, p0, Liam;->x:Landroid/database/ContentObserver;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Liam;->z:Ljava/util/Random;

    invoke-static {p1}, Lhpd;->a(Landroid/content/Context;)Lhpd;

    move-result-object v0

    iput-object v0, p0, Liam;->A:Lhpd;

    return-void
.end method

.method private a(Landroid/location/Location;Lhud;Lhup;Z)Landroid/os/Bundle;
    .locals 4

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p2, Lhud;->a:Lhtw;

    const-string v2, "nlpVersion"

    iget v3, p0, Liam;->w:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p2, Lhud;->d:Lhtj;

    if-eq v0, v2, :cond_1

    if-eqz p4, :cond_0

    invoke-static {p1, p2}, Liam;->a(Landroid/location/Location;Ljava/lang/Object;)[B

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "dbgProtoBuf"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_0
    iget-object v2, p2, Lhud;->c:Lhtd;

    if-ne v0, v2, :cond_3

    const-string v0, "networkLocationType"

    const-string v2, "cell"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    if-eqz p3, :cond_2

    sget-object v0, Lhup;->c:Lhup;

    if-eq p3, v0, :cond_2

    const-string v0, "travelState"

    invoke-virtual {p3}, Lhup;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-object v1

    :cond_3
    iget-object v2, p2, Lhud;->b:Lhuu;

    if-ne v0, v2, :cond_1

    const-string v0, "networkLocationType"

    const-string v2, "wifi"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p2, Lhud;->b:Lhuu;

    iget-object v0, v0, Lhuu;->c:Lhug;

    if-eqz v0, :cond_1

    instance-of v2, v0, Lhtl;

    if-eqz v2, :cond_1

    check-cast v0, Lhtl;

    iget-object v2, v0, Lhtl;->b:Ljava/lang/String;

    if-eqz v2, :cond_4

    const-string v2, "levelId"

    iget-object v3, v0, Lhtl;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget v2, v0, Lhtl;->c:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_1

    const-string v2, "levelNumberE3"

    iget v0, v0, Lhtl;->c:I

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Liad;
    .locals 1

    sget-boolean v0, Licj;->f:Z

    if-eqz v0, :cond_0

    sget-object v0, Liae;->e:Liae;

    :goto_0
    invoke-static {v0, p0}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Liae;->b:Liae;

    goto :goto_0
.end method

.method static synthetic a(Liam;)V
    .locals 0

    invoke-direct {p0}, Liam;->h()V

    return-void
.end method

.method private a(Ljava/io/PrintWriter;Ljava/util/Map;JLjava/lang/String;)V
    .locals 8

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    int-to-long v4, v2

    cmp-long v4, p3, v4

    if-nez v4, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Liam;->v:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " interval "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7fffffff

    if-ne v2, v5, :cond_1

    const-string v2, "<no-client>"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", duration was "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1

    :cond_2
    return-void
.end method

.method private static a(Ljava/util/Map;IJ)V
    .locals 4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-nez v0, :cond_1

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 6

    iget-object v0, p0, Liam;->l:Lifo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Liam;->l:Lifo;

    iget-object v1, p0, Liam;->r:Liai;

    invoke-virtual {v1}, Liai;->h()I

    move-result v1

    iget-object v2, p0, Liam;->r:Liai;

    invoke-virtual {v2}, Liai;->f()I

    move-result v2

    iget-object v3, p0, Liam;->r:Liai;

    invoke-virtual {v3}, Liai;->g()Z

    move-result v4

    iget-object v3, p0, Liam;->r:Liai;

    invoke-virtual {v3}, Liai;->e()Lilx;

    move-result-object v5

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lifo;->a(IIZZLilx;)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/location/Location;Ljava/lang/Object;)[B
    .locals 12

    const-wide v10, 0x416312d000000000L    # 1.0E7

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x6

    new-instance v0, Livi;

    sget-object v1, Lihj;->F:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    new-instance v1, Livi;

    sget-object v2, Lihj;->c:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    new-instance v2, Livi;

    sget-object v3, Lihj;->r:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    invoke-virtual {v0, v9, v2}, Livi;->b(ILivi;)Livi;

    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v9, v3}, Livi;->e(II)Livi;

    :cond_0
    new-instance v3, Livi;

    sget-object v4, Lihj;->i:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    mul-double/2addr v4, v10

    double-to-int v4, v4

    invoke-virtual {v3, v8, v4}, Livi;->e(II)Livi;

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    mul-double/2addr v4, v10

    double-to-int v4, v4

    invoke-virtual {v3, v7, v4}, Livi;->e(II)Livi;

    invoke-virtual {v2, v8, v3}, Livi;->b(ILivi;)Livi;

    if-nez p1, :cond_2

    const/4 v2, 0x0

    invoke-virtual {v1, v6, v2}, Livi;->e(II)Livi;

    :cond_1
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Livi;->f()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Livi;->a([B)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->a(I)V

    invoke-virtual {v0}, Livi;->f()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    instance-of v2, p1, Lhud;

    if-eqz v2, :cond_6

    check-cast p1, Lhud;

    iget-object v2, p1, Lhud;->a:Lhtw;

    iget-object v3, p1, Lhud;->c:Lhtd;

    if-ne v2, v3, :cond_5

    invoke-virtual {v1, v6, v8}, Livi;->e(II)Livi;

    :cond_3
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object v4, p1, Lhud;->c:Lhtd;

    if-eqz v4, :cond_4

    iget-object v4, p1, Lhud;->c:Lhtd;

    iget-object v4, v4, Lhtd;->a:Lhtf;

    if-eqz v4, :cond_4

    invoke-virtual {v4, v0, v2, v3}, Lhtf;->a(Livi;J)V

    :cond_4
    iget-object v4, p1, Lhud;->b:Lhuu;

    if-eqz v4, :cond_1

    iget-object v4, p1, Lhud;->b:Lhuu;

    iget-object v4, v4, Lhuu;->a:Lhuv;

    if-eqz v4, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v3, v5}, Lhuv;->a(JZ)Livi;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Livi;->b(ILivi;)Livi;

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lhud;->a:Lhtw;

    iget-object v3, p1, Lhud;->b:Lhuu;

    if-ne v2, v3, :cond_3

    invoke-virtual {v1, v6, v7}, Livi;->e(II)Livi;

    goto :goto_2

    :cond_6
    instance-of v2, p1, Lhtw;

    if-eqz v2, :cond_7

    invoke-virtual {v1, v6, v9}, Livi;->e(II)Livi;

    goto :goto_0

    :cond_7
    instance-of v2, p1, Landroid/location/Location;

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    invoke-virtual {v1, v6, v2}, Livi;->e(II)Livi;

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_8

    sget-object v1, Liam;->a:Ljava/lang/String;

    const-string v2, "getLocationDebugInfo"

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Z)V
    .locals 10

    iget-object v0, p0, Liam;->r:Liai;

    invoke-virtual {v0}, Liai;->a()I

    move-result v1

    iget-object v0, p0, Liam;->r:Liai;

    invoke-virtual {v0}, Liai;->b()I

    move-result v2

    iget-object v0, p0, Liam;->r:Liai;

    invoke-virtual {v0}, Liai;->c()I

    move-result v3

    iget-object v0, p0, Liam;->r:Liai;

    invoke-virtual {v0}, Liai;->d()Lilx;

    move-result-object v5

    iget-object v0, p0, Liam;->l:Lifo;

    if-eqz v0, :cond_1

    if-eqz v5, :cond_2

    iget-object v0, p0, Liam;->q:Lilx;

    invoke-virtual {v5, v0}, Lilx;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    if-nez p1, :cond_0

    iget v4, p0, Liam;->o:I

    if-ne v1, v4, :cond_0

    iget v4, p0, Liam;->p:I

    if-ne v2, v4, :cond_0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Liam;->l:Lifo;

    move v4, p1

    invoke-virtual/range {v0 .. v5}, Lifo;->a(IIIZLilx;)V

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-wide v6, p0, Liam;->v:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_4

    iput-wide v3, p0, Liam;->v:J

    :goto_1
    iput v1, p0, Liam;->o:I

    iput v2, p0, Liam;->p:I

    iput-object v5, p0, Liam;->q:Lilx;

    return-void

    :cond_2
    iget-object v0, p0, Liam;->q:Lilx;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    iget-wide v6, p0, Liam;->v:J

    sub-long v6, v3, v6

    iput-wide v3, p0, Liam;->v:J

    iget-object v0, p0, Liam;->t:Ljava/util/Map;

    iget v3, p0, Liam;->o:I

    invoke-static {v0, v3, v6, v7}, Liam;->a(Ljava/util/Map;IJ)V

    iget-object v0, p0, Liam;->u:Ljava/util/Map;

    iget v3, p0, Liam;->p:I

    invoke-static {v0, v3, v6, v7}, Liam;->a(Ljava/util/Map;IJ)V

    goto :goto_1
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    sget-object v0, Liam;->a:Ljava/lang/String;

    return-object v0
.end method

.method private final f()Z
    .locals 2

    iget-object v0, p0, Liam;->c:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private g()V
    .locals 4

    sget-object v0, Liae;->a:Liae;

    iget-object v1, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-static {v0, v1}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v0

    iget-object v1, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-static {v1}, Liam;->a(Landroid/content/Context;)Liad;

    move-result-object v1

    iget v2, v0, Liad;->d:I

    iget v3, v1, Liad;->d:I

    if-lt v2, v3, :cond_1

    :goto_0
    iget-object v2, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-direct {p0}, Liam;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    if-ne v0, v1, :cond_2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Liam;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "This NLP should run continuously. intent is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v1, Liad;->e:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    iget-object v1, v1, Liad;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/server/GoogleLocationService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/GoogleLocationService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lhhv;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Liam;->x:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    sget-object v0, Liam;->a:Ljava/lang/String;

    const-string v1, "This NLP should stop in the absence of clients."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/GoogleLocationService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Liam;->x:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/GoogleLocationService;->a()V

    goto :goto_1
.end method

.method private h()V
    .locals 26

    move-object/from16 v0, p0

    iget-boolean v2, v0, Liam;->k:Z

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-direct/range {p0 .. p0}, Liam;->f()Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    move/from16 v22, v2

    :goto_0
    if-eqz v22, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    const/4 v3, 0x1

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "network_location_opt_in"

    const/4 v5, -0x1

    invoke-static {v2, v4, v5}, Lhhv;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v3, v2, :cond_f

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_10

    const/4 v2, 0x1

    move/from16 v20, v2

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Liam;->g:Ljava/lang/Object;

    monitor-enter v3

    if-nez v20, :cond_1

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Liam;->d:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    sget-object v2, Liam;->a:Ljava/lang/String;

    const-string v4, "Sending NLP deactivated msg"

    invoke-static {v2, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Liam;->d:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->r:Liai;

    move-object/from16 v0, p0

    iget-object v4, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Liai;->a(Landroid/content/Context;Z)V

    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v20, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->l:Lifo;

    if-nez v2, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Liam;->g:Ljava/lang/Object;

    move-object/from16 v23, v0

    monitor-enter v23

    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Liam;->k:Z

    if-eqz v2, :cond_6

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_2

    sget-object v2, Liam;->a:Ljava/lang/String;

    const-string v3, "Creating RealOs"

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-boolean v4, Licj;->f:Z

    sget-object v2, Liae;->a:Liae;

    move-object/from16 v0, p0

    iget-object v3, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-static {v2, v3}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v3

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v2, v5, :cond_11

    const/4 v2, 0x1

    :goto_3
    if-nez v2, :cond_3

    iget-object v2, v3, Liad;->a:Liae;

    sget-object v3, Liae;->f:Liae;

    if-eq v2, v3, :cond_12

    :cond_3
    const/4 v2, 0x1

    move v3, v2

    :goto_4
    if-eqz v4, :cond_15

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_4

    sget-object v2, Liam;->a:Ljava/lang/String;

    const-string v4, "NLP Running in GCore, testing to see if Android Maps is running."

    invoke-static {v2, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    sget-object v2, Liae;->c:Liae;

    move-object/from16 v0, p0

    iget-object v4, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-static {v2, v4}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v2

    sget-object v4, Liae;->d:Liae;

    move-object/from16 v0, p0

    iget-object v5, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-static {v4, v5}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v4

    iget-object v5, v2, Liad;->a:Liae;

    sget-object v6, Liae;->f:Liae;

    if-ne v5, v6, :cond_13

    iget-object v4, v4, Liad;->a:Liae;

    sget-object v5, Liae;->f:Liae;

    if-ne v4, v5, :cond_13

    const/4 v2, 0x1

    :goto_5
    move/from16 v21, v2

    :goto_6
    sget-object v2, Lhjv;->b:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v24

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_5

    sget-object v2, Liam;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fullCollection: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", collectionEnabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", gServices collection enabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    new-instance v4, Lifo;

    move-object/from16 v0, p0

    iget-object v5, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    move-object/from16 v0, p0

    iget-object v6, v0, Liam;->h:Licp;

    if-eqz v3, :cond_18

    if-eqz v24, :cond_18

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v4, v5, v6, v0, v2}, Lifo;-><init>(Landroid/content/Context;Licp;Lifq;Z)V

    move-object/from16 v0, p0

    iput-object v4, v0, Liam;->l:Lifo;

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->l:Lifo;

    invoke-virtual {v2}, Lifo;->o()Ljava/util/concurrent/ExecutorService;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->l:Lifo;

    invoke-virtual {v2}, Lifo;->a()Ljava/io/File;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->l:Lifo;

    invoke-virtual {v2}, Lifo;->i()[B

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Liam;->l:Lifo;

    new-instance v2, Ljava/io/File;

    const-string v3, "macs"

    invoke-direct {v2, v11, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v25, Lhnz;

    sget-object v3, Lhoy;->a:Lhpc;

    move-object/from16 v0, v25

    invoke-direct {v0, v3, v2, v9, v10}, Lhnz;-><init>(Lhpc;Ljava/io/File;[BLidq;)V

    new-instance v8, Ljava/io/File;

    const-string v2, "selectors"

    invoke-direct {v8, v11, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    invoke-interface {v10, v8}, Lidq;->a(Ljava/io/File;)V

    new-instance v2, Lhnt;

    const/4 v3, 0x2

    const/16 v4, 0x14

    new-instance v6, Lhtt;

    invoke-direct {v6}, Lhtt;-><init>()V

    sget-object v7, Lhoy;->b:Lhpc;

    invoke-direct/range {v2 .. v10}, Lhnt;-><init>(IILjava/util/concurrent/ExecutorService;Lhuj;Lhpc;Ljava/io/File;[BLidq;)V

    new-instance v17, Ljava/io/File;

    const-string v3, "models"

    move-object/from16 v0, v17

    invoke-direct {v0, v11, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->mkdirs()Z

    move-object/from16 v0, v17

    invoke-interface {v10, v0}, Lidq;->a(Ljava/io/File;)V

    new-instance v11, Lhnt;

    const/4 v12, 0x4

    const/16 v13, 0xa

    new-instance v15, Lhtq;

    invoke-direct {v15}, Lhtq;-><init>()V

    sget-object v16, Lhoy;->b:Lhpc;

    move-object v14, v5

    move-object/from16 v18, v9

    move-object/from16 v19, v10

    invoke-direct/range {v11 .. v19}, Lhnt;-><init>(IILjava/util/concurrent/ExecutorService;Lhuj;Lhpc;Ljava/io/File;[BLidq;)V

    new-instance v3, Lhoc;

    move-object/from16 v0, v25

    invoke-direct {v3, v0, v2, v11}, Lhoc;-><init>(Lhnz;Lhnt;Lhnt;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Liam;->m:Lhoc;

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->l:Lifo;

    invoke-virtual {v2}, Lifo;->a()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_19

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_19

    new-instance v3, Lhoi;

    move-object/from16 v0, p0

    iget-object v4, v0, Liam;->l:Lifo;

    move-object/from16 v0, p0

    iget-object v5, v0, Liam;->l:Lifo;

    invoke-virtual {v5}, Lifo;->i()[B

    move-result-object v5

    invoke-direct {v3, v4, v2, v5}, Lhoi;-><init>(Lidq;Ljava/io/File;[B)V

    move-object/from16 v0, p0

    iput-object v3, v0, Liam;->n:Lhod;

    :goto_8
    new-instance v2, Lhjy;

    move-object/from16 v0, p0

    iget-object v3, v0, Liam;->l:Lifo;

    move-object/from16 v0, p0

    iget-object v4, v0, Liam;->l:Lifo;

    invoke-virtual {v4}, Lifo;->F()Lids;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Liam;->l:Lifo;

    invoke-virtual {v5}, Lifo;->E()Liha;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Liam;->m:Lhoc;

    move-object/from16 v0, p0

    iget-object v7, v0, Liam;->n:Lhod;

    if-eqz v21, :cond_1b

    if-eqz v24, :cond_1b

    const/4 v8, 0x1

    :goto_9
    invoke-direct/range {v2 .. v8}, Lhjy;-><init>(Lidu;Lids;Liha;Lhoc;Lhod;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Liam;->y:Lhjy;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Liam;->b(Z)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Liam;->a(Z)V

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Liam;->i:Ljava/lang/Boolean;

    :cond_6
    monitor-exit v23
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_7
    :goto_a
    move-object/from16 v0, p0

    iget-object v3, v0, Liam;->g:Ljava/lang/Object;

    monitor-enter v3

    if-eqz v20, :cond_9

    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Liam;->d:Z

    move/from16 v0, v20

    if-eq v2, v0, :cond_9

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_8

    sget-object v2, Liam;->a:Ljava/lang/String;

    const-string v4, "Sending NLP activated msg"

    invoke-static {v2, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Liam;->d:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->r:Liai;

    move-object/from16 v0, p0

    iget-object v4, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    move/from16 v0, v20

    invoke-virtual {v2, v4, v0}, Liai;->a(Landroid/content/Context;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.location.internal.server.ACTION_RESTARTED"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/google/android/location/internal/server/GoogleLocationService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_9
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-object/from16 v0, p0

    iget-boolean v2, v0, Liam;->d:Z

    if-nez v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-static {v2}, Lifo;->a(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->m:Lhoc;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->m:Lhoc;

    invoke-virtual {v2}, Lhoc;->b()V

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->n:Lhod;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->n:Lhod;

    invoke-interface {v2}, Lhod;->c()V

    :cond_b
    if-nez v22, :cond_d

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_c

    sget-object v2, Liam;->a:Ljava/lang/String;

    const-string v3, "Destroying our service"

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-virtual {v2}, Lcom/google/android/location/internal/server/GoogleLocationService;->a()V

    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    :cond_d
    return-void

    :cond_e
    const/4 v2, 0x0

    move/from16 v22, v2

    goto/16 :goto_0

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_10
    const/4 v2, 0x0

    move/from16 v20, v2

    goto/16 :goto_2

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_12
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_4

    :cond_13
    :try_start_3
    iget v2, v2, Liad;->d:I

    const/16 v4, 0x45f

    if-lt v2, v4, :cond_14

    const/4 v2, 0x1

    goto/16 :goto_5

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_15
    sget-object v2, Liae;->e:Liae;

    move-object/from16 v0, p0

    iget-object v4, v0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-static {v2, v4}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v2

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_16

    sget-object v4, Liam;->a:Ljava/lang/String;

    const-string v5, "NLP Running in Maps"

    invoke-static {v4, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    iget-object v2, v2, Liad;->a:Liae;

    sget-object v4, Liae;->f:Liae;

    if-ne v2, v4, :cond_17

    const/4 v2, 0x1

    :goto_b
    move/from16 v21, v2

    goto/16 :goto_6

    :cond_17
    const/4 v2, 0x0

    goto :goto_b

    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_19
    sget-boolean v2, Licj;->d:Z

    if-eqz v2, :cond_1a

    sget-object v2, Liam;->a:Ljava/lang/String;

    const-string v3, "No persistence dir, unable to record NlpStats"

    invoke-static {v2, v3}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a
    new-instance v2, Lhoe;

    invoke-direct {v2}, Lhoe;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Liam;->n:Lhod;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_8

    :catchall_1
    move-exception v2

    monitor-exit v23

    throw v2

    :cond_1b
    const/4 v8, 0x0

    goto/16 :goto_9

    :cond_1c
    if-nez v20, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->l:Lifo;

    if-eqz v2, :cond_7

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1d

    sget-object v2, Liam;->a:Ljava/lang/String;

    const-string v3, "Destroying RealOs"

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->l:Lifo;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lifo;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Liam;->l:Lifo;

    invoke-virtual {v2}, Lifo;->G()V

    move-object/from16 v0, p0

    iget-object v3, v0, Liam;->g:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x0

    :try_start_4
    move-object/from16 v0, p0

    iput-object v2, v0, Liam;->l:Lifo;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Liam;->s:Lifn;

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Liam;->o:I

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto/16 :goto_a

    :catchall_2
    move-exception v2

    monitor-exit v3

    throw v2

    :catchall_3
    move-exception v2

    monitor-exit v3

    throw v2
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x2

    :try_start_0
    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(II)V
    .locals 3

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Liam;->r:Liai;

    iget-object v2, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-virtual {v0, v2, p1, p2}, Liai;->a(Landroid/content/Context;II)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 4

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Liam;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removing location pending intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Liam;->r:Liai;

    invoke-virtual {v0, p1}, Liai;->a(Landroid/app/PendingIntent;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Liam;->b(Z)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/app/PendingIntent;IIZZZLilx;)V
    .locals 9

    iget-object v8, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Liam;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "adding pendingIntent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with period "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and trigger "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x5

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v0, p0, Liam;->r:Liai;

    iget-object v1, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    move-object v2, p1

    move v4, p3

    move v5, p4

    move v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Liai;->a(Landroid/content/Context;Landroid/app/PendingIntent;IIZZLilx;)V

    invoke-direct {p0, p5}, Liam;->b(Z)V

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method public final a(Landroid/app/PendingIntent;IZZLilx;)V
    .locals 7

    iget-object v6, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Liam;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "adding activity pendingIntent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Liam;->r:Liai;

    iget-object v1, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    const/4 v2, 0x0

    invoke-static {p2, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object v2, p1

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Liai;->a(Landroid/content/Context;Landroid/app/PendingIntent;IZLilx;)V

    invoke-direct {p0, p3}, Liam;->a(Z)V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Liam;->r:Liai;

    iget-object v2, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-virtual {v0, v2, p1}, Liai;->a(Landroid/content/Context;Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Liam;->a(Z)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lhud;Lhup;)V
    .locals 12

    const-wide v5, 0x416312d000000000L    # 1.0E7

    const/4 v7, 0x0

    iget-object v0, p1, Lhud;->a:Lhtw;

    new-instance v4, Landroid/location/Location;

    const-string v1, "network"

    invoke-direct {v4, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-object v1, v0, Lhtw;->c:Lhug;

    iget v2, v1, Lhug;->d:I

    int-to-double v2, v2

    div-double/2addr v2, v5

    invoke-virtual {v4, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    iget v2, v1, Lhug;->e:I

    int-to-double v2, v2

    div-double/2addr v2, v5

    invoke-virtual {v4, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    iget v1, v1, Lhug;->f:I

    int-to-float v1, v1

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {v4, v1}, Landroid/location/Location;->setAccuracy(F)V

    iget-wide v0, v0, Lhtw;->e:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v2, v5

    add-long/2addr v0, v2

    invoke-virtual {v4, v0, v1}, Landroid/location/Location;->setTime(J)V

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    invoke-virtual {v0, v4}, Lifs;->b(Landroid/location/Location;)V

    new-instance v5, Landroid/location/Location;

    invoke-direct {v5, v4}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    const/4 v0, 0x1

    invoke-direct {p0, v4, p1, p2, v0}, Liam;->a(Landroid/location/Location;Lhud;Lhup;Z)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v4, p1, p2, v7}, Liam;->a(Landroid/location/Location;Lhud;Lhup;Z)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    invoke-virtual {v5, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    iget-object v7, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Liam;->r:Liai;

    iget-object v1, p1, Lhud;->a:Lhtw;

    iget-wide v1, v1, Lhtw;->e:J

    iget-object v3, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    iget-boolean v6, p1, Lhud;->e:Z

    invoke-virtual/range {v0 .. v6}, Liai;->a(JLandroid/content/Context;Landroid/location/Location;Landroid/location/Location;Z)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lifn;

    invoke-virtual {v4}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    sub-long/2addr v2, v8

    const/4 v6, 0x0

    invoke-direct {v1, v5, v2, v3, v6}, Lifn;-><init>(Landroid/location/Location;JI)V

    iput-object v1, p0, Liam;->s:Lifn;

    iget-object v1, p0, Liam;->j:Ljava/util/LinkedHashMap;

    new-instance v2, Ljava/lang/Long;

    iget-object v3, p0, Liam;->s:Lifn;

    iget-object v3, v3, Lifn;->a:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    invoke-direct {v2, v5, v6}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    sget-object v1, Liam;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reporting "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Liam;->b(Z)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Liam;->l:Lifo;

    if-eqz v2, :cond_1

    iget-object v2, p0, Liam;->l:Lifo;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lifo;->a(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Liam;->z:Ljava/util/Random;

    sget-object v1, Lhjv;->c:Lbfy;

    invoke-static {v0, v1}, Lhjv;->a(Ljava/util/Random;Lbfy;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Liam;->r:Liai;

    invoke-virtual {v0}, Liai;->i()Ljava/util/Map;

    move-result-object v2

    iget-object v0, p0, Liam;->A:Lhpd;

    invoke-virtual {v0}, Lhpd;->a()V

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Liam;->A:Lhpd;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v4, v0, v1}, Lhpd;->a(Ljava/lang/String;I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Liam;->A:Lhpd;

    invoke-virtual {v0}, Lhpd;->b()V

    :cond_4
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Licp;)V
    .locals 2

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Liam;->h:Licp;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 7

    iget-object v6, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NLP-Period is currently "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Liam;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v2, p0, Liam;->t:Ljava/util/Map;

    iget v0, p0, Liam;->o:I

    int-to-long v3, v0

    const-string v5, "NLP-Period"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Liam;->a(Ljava/io/PrintWriter;Ljava/util/Map;JLjava/lang/String;)V

    iget-object v2, p0, Liam;->u:Ljava/util/Map;

    iget v0, p0, Liam;->p:I

    int-to-long v3, v0

    const-string v5, "NLP-Low-Power-Period"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Liam;->a(Ljava/io/PrintWriter;Ljava/util/Map;JLjava/lang/String;)V

    iget-object v0, p0, Liam;->r:Liai;

    invoke-virtual {v0, p1}, Liai;->a(Ljava/io/PrintWriter;)V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a(Ljava/text/Format;Ljava/io/PrintWriter;)V
    .locals 5

    iget-object v2, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v0, Licj;->f:Z

    if-eqz v0, :cond_1

    const-string v0, "GMS"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " collection is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Liam;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    const-string v0, "not yet set."

    move-object v1, p2

    :goto_1
    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Liam;->l:Lifo;

    if-eqz v0, :cond_0

    const-string v0, "RealOs stats:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Liam;->l:Lifo;

    invoke-virtual {v0, p1, p2}, Lifo;->a(Ljava/text/Format;Ljava/io/PrintWriter;)V

    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    :cond_0
    invoke-static {}, Lhsp;->a()Lhsp;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-virtual {v0, p2, v3, v4}, Lhsp;->a(Ljava/io/PrintWriter;J)V

    monitor-exit v2

    return-void

    :cond_1
    const-string v0, "GMM"

    goto :goto_0

    :cond_2
    iget-object v0, p0, Liam;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "enabled"

    move-object v1, p2

    goto :goto_1

    :cond_3
    const-string v0, "disabled"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, p2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b()V
    .locals 2

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Liaa;->a(Liab;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Liam;->k:Z

    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    invoke-direct {p0}, Liam;->g()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Landroid/app/PendingIntent;)V
    .locals 4

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Liam;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removing activity pendingIntent "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Liam;->r:Liai;

    invoke-virtual {v0, p1}, Liai;->b(Landroid/app/PendingIntent;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Liam;->a(Z)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/io/PrintWriter;)V
    .locals 6

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Liam;->y:Lhjy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Liam;->y:Lhjy;

    iget-object v0, v0, Lhjy;->n:Lhlu;

    const-string v2, "####VehicleExitDetectorStats Start"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v2, v0, Lhlu;->c:Lhmb;

    const-string v3, "\n"

    iget-object v0, v0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v4

    invoke-virtual {v2, p1, v3, v4, v5}, Lhmb;->a(Ljava/io/PrintWriter;Ljava/lang/String;J)V

    const-string v0, "####VehicleExitDetectorStats End"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 3

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Liaa;->b(Liab;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Liam;->k:Z

    iget-object v0, p0, Liam;->l:Lifo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Liam;->l:Lifo;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lifo;->a(Z)V

    :cond_0
    iget-boolean v0, p0, Liam;->f:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    invoke-direct {p0}, Liam;->g()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Lidr;
    .locals 2

    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Liam;->s:Lifn;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Liam;->e:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Liam;->e:Z

    iget-object v0, p0, Liam;->b:Lcom/google/android/location/internal/server/GoogleLocationService;

    invoke-static {v0}, Lifo;->b(Landroid/content/Context;)V

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Liam;->h()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Liam;->g()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_2
    iget-object v1, p0, Liam;->g:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Liam;->f:Z

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, Liam;->a:Ljava/lang/String;

    const-string v2, "Looper quitting."

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
