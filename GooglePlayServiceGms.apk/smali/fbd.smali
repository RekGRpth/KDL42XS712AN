.class public final Lfbd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lbfy;

.field public static final B:Lbfy;

.field public static final C:Lbfy;

.field public static final D:Lbfy;

.field public static final E:Lbfy;

.field public static final F:Lbfy;

.field public static final G:Lbfy;

.field public static final H:Lbfy;

.field public static final I:Lbfy;

.field public static final J:Lbfy;

.field public static final K:Lbfy;

.field public static final L:Lbfy;

.field public static final M:Lbfy;

.field public static final N:Lbfy;

.field public static final O:Lbfy;

.field public static final P:Lbfy;

.field public static final Q:Lbfy;

.field public static final R:Lbfy;

.field public static final S:Lbfy;

.field public static final T:Lbfy;

.field public static final U:Lbfy;

.field public static final V:Lbfy;

.field public static final W:Lbfy;

.field public static final X:Lbfy;

.field public static final Y:Lbfy;

.field public static final Z:Lbfy;

.field public static final a:Lbfy;

.field public static final aa:Lbfy;

.field public static final ab:Lbfy;

.field public static final ac:Lbfy;

.field public static final ad:Lbfy;

.field public static final ae:Lbfy;

.field public static final af:Lbfy;

.field private static final ag:Lbfy;

.field private static final ah:Lbfy;

.field private static final ai:Lbfy;

.field private static final aj:Lbfy;

.field private static final ak:Lbfy;

.field private static final al:Lbfy;

.field private static final am:Lbfy;

.field public static final b:Lbfy;

.field public static final c:Lbfy;

.field public static final d:Lbfy;

.field public static final e:Lbfy;

.field public static final f:Lbfy;

.field public static final g:Lbfy;

.field public static final h:Lbfy;

.field public static final i:Lbfy;

.field public static final j:Lbfy;

.field public static final k:Lbfy;

.field public static final l:Lbfy;

.field public static final m:Lbfy;

.field public static final n:Lbfy;

.field public static final o:Lbfy;

.field public static final p:Lbfy;

.field public static final q:Lbfy;

.field public static final r:Lbfy;

.field public static final s:Lbfy;

.field public static final t:Lbfy;

.field public static final u:Lbfy;

.field public static final v:Lbfy;

.field public static final w:Lbfy;

.field public static final x:Lbfy;

.field public static final y:Lbfy;

.field public static final z:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v7, 0x258

    const/16 v6, 0xc8

    const/16 v5, 0x64

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "people.cache_enabled"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->a:Lbfy;

    const-string v0, "people.avatar_cache_enabled"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->b:Lbfy;

    const-string v0, "people.verbose_logging"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->c:Lbfy;

    const-string v0, "people.apiary_trace"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->d:Lbfy;

    const-string v0, "people.num_logs_to_keep"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->e:Lbfy;

    const-string v0, "people.allow_unknown_client"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->f:Lbfy;

    const-string v0, "people.ignore_gplus_app_metadata"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->g:Lbfy;

    const-string v0, "people.num_prp_threads"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->h:Lbfy;

    const-string v0, "people.num_days_to_keep_logs_ez"

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->i:Lbfy;

    const-string v0, "people.use_contactables_api"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->j:Lbfy;

    const-string v0, "people.periodic_sync_interval_sec"

    const v1, 0x15180

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->k:Lbfy;

    const-string v0, "people.default_avatar_expiration_interval_sec"

    const-wide/32 v1, 0x127500

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->l:Lbfy;

    const-string v0, "people.is_periodic_full_people_sync_enabled"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->m:Lbfy;

    const-string v0, "people.periodic_full_people_sync_interval_sec"

    const-wide/32 v1, 0x127500

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->n:Lbfy;

    const-string v0, "people.minimum_width_cover_photo_dp"

    const/16 v1, 0x140

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->o:Lbfy;

    const-string v0, "people.manual_sync_min_allowance_sec"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->p:Lbfy;

    const-string v0, "people.skip_periodic_sync_sec"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->q:Lbfy;

    const-string v0, "people.tickle_sync_delay_sec"

    const-wide/16 v1, 0xa

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->r:Lbfy;

    const-string v0, "people.tickle_sync_plusadmin_delay_sec"

    const-wide/16 v1, 0xa

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->s:Lbfy;

    const-string v0, "people.periodic_page_sync_max_pages"

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->t:Lbfy;

    const-string v0, "people.sync_facl"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->u:Lbfy;

    const-string v0, "people.should_have_evergreen_people"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->v:Lbfy;

    const-string v0, "people.allow_including_evergreen_people_in_aggregation"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->w:Lbfy;

    const-string v0, "people.aggregation_evergreen_override"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->x:Lbfy;

    const-string v0, "people.plus_url"

    const-string v1, "https://plus.google.com/%s"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->y:Lbfy;

    const-string v0, "people.sync_general_error_back_off_sec"

    const/16 v1, 0xe10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->z:Lbfy;

    const-string v0, "people.sync_general_error_back_off_exponential"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->A:Lbfy;

    const-string v0, "people.sync_max_back_off_sec"

    const v1, 0x2a300

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->B:Lbfy;

    const-string v0, "people.fetch_avatar_error_back_off_sec"

    const/16 v1, 0x12c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->C:Lbfy;

    const-string v0, "people.fetch_avatar_error_back_off_exponential"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->D:Lbfy;

    const-string v0, "people.fetch_avatar_max_back_off_sec"

    const v1, 0x2a300

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->E:Lbfy;

    const-string v0, "people.sync_failure_count_too_many_retries"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->F:Lbfy;

    const-string v0, "people.sync_failure_count_to_disable_sync"

    const v1, 0x5f5e0ff

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->G:Lbfy;

    const-string v0, "people.expedite_new_sync"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->H:Lbfy;

    const-string v0, "people.v1whitelisted_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->I:Lbfy;

    const-string v0, "people.v1whitelisted_server_api_path"

    const-string v1, "/plus/v1whitelisted/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->J:Lbfy;

    const-string v0, "people.whitelisted_server_upload_api_path"

    const-string v1, "/upload/plus/v1whitelisted/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->K:Lbfy;

    const-string v0, "people.whitelisted_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->L:Lbfy;

    const-string v0, "people.profile_image_max_dimension"

    const/16 v1, 0x280

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->M:Lbfy;

    const-string v0, "people.profile_image_compress_quality"

    const/16 v1, 0x50

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->N:Lbfy;

    const-string v0, "people.v2whitelisted_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->O:Lbfy;

    const-string v0, "people.v2whitelisted_server_api_path"

    const-string v1, "/plus/v2whitelisted/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->P:Lbfy;

    const-string v0, "people.v2whitelisted_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->Q:Lbfy;

    const-string v0, "people.sync_page_large_memory_class_threshold"

    const/16 v1, 0x60

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->ag:Lbfy;

    const-string v0, "people.sync_page_size_v1_small"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->ah:Lbfy;

    const-string v0, "people.sync_page_size_v1_large"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->ai:Lbfy;

    const-string v0, "people.sync_page_size_v2_circled_small"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->aj:Lbfy;

    const-string v0, "people.sync_page_size_v2_circled_large"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->ak:Lbfy;

    const-string v0, "people.sync_page_size_v2_all_small"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->al:Lbfy;

    const-string v0, "people.sync_page_size_v2_all_large"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->am:Lbfy;

    const-string v0, "people.sync_sanity_check_interval_seconds"

    const-wide/32 v1, 0x15180

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->R:Lbfy;

    const-string v0, "people.sync_sanity_check_run_ratio"

    const v1, 0x3a03126f    # 5.0E-4f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->S:Lbfy;

    const-string v0, "people.crash_on_json_parser_errors"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->T:Lbfy;

    const-string v0, "people.reenable_people_sync"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->U:Lbfy;

    const-string v0, "people.recover_sync_failure"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->V:Lbfy;

    const-string v0, "people.cp2_join_by_phone"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->W:Lbfy;

    const-string v0, "people.whitelist_cp2sync_settings"

    const-string v1, "com.google.android.apps.plus"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->X:Lbfy;

    const-string v0, "people.whitelist_request_sync"

    const-string v1, "com.google.android.apps.plus,com.google.android.talk,com.google.android.apps.babel,com.google.android.play.games"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->Y:Lbfy;

    const-string v0, "people.default_account_avatar_url"

    const-string v1, "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/dS9ff5TYSlA/c/photo.jpg"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->Z:Lbfy;

    const-string v0, "people.default_page_avatar_url"

    const-string v1, "http://lh3.googleusercontent.com/-q1Smh9d8d0g/AAAAAAAAAAM/AAAAAAAAAAA/q6bRUp5qvvk/c/photo.jpg"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->aa:Lbfy;

    const-string v0, "people.enable_cp2_sync"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->ab:Lbfy;

    const-string v0, "people.sync_request_log_ratio"

    const v1, 0x3c23d70a    # 0.01f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->ac:Lbfy;

    const-string v0, "people.sync_result_log_ratio"

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->ad:Lbfy;

    const-string v0, "people.show_sync_notification"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->ae:Lbfy;

    const-string v0, "people.playlog_stacktrace_max_len"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfbd;->af:Lbfy;

    return-void
.end method

.method public static final a(I)I
    .locals 4

    const/16 v2, 0x12c

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "people.http_back_off_sec_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    :goto_0
    return v0

    :cond_0
    sparse-switch p0, :sswitch_data_0

    move v0, v2

    :goto_1
    const-string v1, "people.http_back_off_sec"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :sswitch_0
    move v0, v1

    goto :goto_1

    :sswitch_1
    move v0, v2

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x194 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Landroid/content/Context;)I
    .locals 1

    invoke-static {p0}, Lfbd;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfbd;->ai:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lfbd;->ah:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)I
    .locals 1

    invoke-static {p0}, Lfbd;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfbd;->ak:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lfbd;->aj:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method

.method public static final b(I)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "people.http_back_off_exponential_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_2

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    sparse-switch p0, :sswitch_data_0

    move v0, v1

    :goto_1
    const-string v3, "people.http_back_off_exponential"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v0}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v1, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_0
    const/4 v0, 0x2

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12e -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(Landroid/content/Context;)I
    .locals 1

    invoke-static {p0}, Lfbd;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfbd;->am:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lfbd;->al:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method

.method private static d(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    sget-object v0, Lfbd;->ag:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
