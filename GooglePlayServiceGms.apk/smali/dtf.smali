.class public final Ldtf;
.super Ldrw;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1}, Ldrw;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldtf;->b:Ldad;

    iput-object p3, p0, Ldtf;->c:Ljava/lang/String;

    iput-boolean p4, p0, Ldtf;->d:Z

    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 3

    iget-object v0, p0, Ldtf;->b:Ldad;

    iget-object v1, p0, Ldtf;->c:Ljava/lang/String;

    iget-boolean v2, p0, Ldtf;->d:Z

    invoke-interface {v0, p1, v1, v2}, Ldad;->a(ILjava/lang/String;Z)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)I
    .locals 3

    iget-object v0, p0, Ldtf;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Ldtf;->c:Ljava/lang/String;

    iget-boolean v2, p0, Ldtf;->d:Z

    invoke-virtual {p2, p1, v0, v1, v2}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method
