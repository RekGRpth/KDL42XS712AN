.class public final Liun;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static a:Liun;

.field protected static final d:Ljava/lang/Object;


# instance fields
.field protected final b:Liuw;

.field protected final c:Liva;

.field protected final e:Liuo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Liun;->d:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Liun;->c:Liva;

    iput-object v0, p0, Liun;->b:Liuw;

    new-instance v0, Liup;

    invoke-direct {v0}, Liup;-><init>()V

    iput-object v0, p0, Liun;->e:Liuo;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Liva;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Liun;->d:Ljava/lang/Object;

    monitor-enter v1

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Liux;

    invoke-direct {v0}, Liux;-><init>()V

    :goto_0
    iput-object v0, p0, Liun;->c:Liva;

    new-instance v0, Liup;

    invoke-direct {v0}, Liup;-><init>()V

    iput-object v0, p0, Liun;->e:Liuo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sput-object p0, Liun;->a:Liun;

    new-instance v0, Livf;

    invoke-direct {v0, p1}, Livf;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Liun;->b:Liuw;

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Livh;

    invoke-direct {v0, p1}, Livh;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()Liun;
    .locals 1

    sget-object v0, Liun;->a:Liun;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Liun;
    .locals 3

    sget-object v1, Liun;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Liun;->a:Liun;

    if-nez v0, :cond_0

    new-instance v0, Liun;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Liun;-><init>(Landroid/content/Context;Liva;)V

    sput-object v0, Liun;->a:Liun;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Liun;->a:Liun;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()Liuo;
    .locals 1

    iget-object v0, p0, Liun;->e:Liuo;

    return-object v0
.end method

.method public final c()Liva;
    .locals 1

    iget-object v0, p0, Liun;->c:Liva;

    return-object v0
.end method

.method public final d()Liuw;
    .locals 1

    iget-object v0, p0, Liun;->b:Liuw;

    return-object v0
.end method
