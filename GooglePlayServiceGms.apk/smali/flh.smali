.class public final Lflh;
.super Lizp;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Lflc;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lizp;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lflh;->a:J

    iput-object v2, p0, Lflh;->b:Lflc;

    iput-object v2, p0, Lflh;->q:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lflh;->C:I

    return-void
.end method

.method public static a([B)Lflh;
    .locals 1

    new-instance v0, Lflh;

    invoke-direct {v0}, Lflh;-><init>()V

    invoke-static {v0, p0}, Lizs;->a(Lizs;[B)Lizs;

    move-result-object v0

    check-cast v0, Lflh;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizp;->a()I

    move-result v0

    iget-wide v1, p0, Lflh;->a:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-wide v2, p0, Lflh;->a:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lflh;->b:Lflc;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lflh;->b:Lflc;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lflh;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lflh;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lflh;->a:J

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lflh;->b:Lflc;

    if-nez v0, :cond_1

    new-instance v0, Lflc;

    invoke-direct {v0}, Lflc;-><init>()V

    iput-object v0, p0, Lflh;->b:Lflc;

    :cond_1
    iget-object v0, p0, Lflh;->b:Lflc;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    iget-wide v0, p0, Lflh;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Lflh;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_0
    iget-object v0, p0, Lflh;->b:Lflc;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lflh;->b:Lflc;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    invoke-super {p0, p1}, Lizp;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lflh;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lflh;

    iget-wide v2, p0, Lflh;->a:J

    iget-wide v4, p1, Lflh;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lflh;->b:Lflc;

    if-nez v2, :cond_4

    iget-object v2, p1, Lflh;->b:Lflc;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lflh;->b:Lflc;

    iget-object v3, p1, Lflh;->b:Lflc;

    invoke-virtual {v2, v3}, Lflc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lflh;->q:Ljava/util/List;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lflh;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    iget-object v2, p1, Lflh;->q:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lflh;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lflh;->q:Ljava/util/List;

    iget-object v1, p1, Lflh;->q:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-wide v2, p0, Lflh;->a:J

    iget-wide v4, p0, Lflh;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lflh;->b:Lflc;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lflh;->q:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lflh;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lflh;->b:Lflc;

    invoke-virtual {v0}, Lflc;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lflh;->q:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method
