.class public final Lfch;
.super Lbje;
.source "SourceFile"


# static fields
.field private static volatile i:Landroid/os/Bundle;

.field private static volatile j:Landroid/os/Bundle;


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/HashMap;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbje;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;[Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    iput-object p5, p0, Lfch;->f:Ljava/lang/String;

    iput-object p6, p0, Lfch;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lbjh;

    invoke-direct {v3, p2}, Lbjh;-><init>(Lbbr;)V

    new-instance v4, Lbjl;

    invoke-direct {v4, p3}, Lbjl;-><init>(Lbbs;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lfch;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(ILandroid/os/Bundle;)Lbbo;
    .locals 2

    new-instance v1, Lbbo;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, p0, v0}, Lbbo;-><init>(ILandroid/app/PendingIntent;)V

    return-object v1

    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method static synthetic a(Lfch;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method private b(Lfar;)Lfct;
    .locals 3

    iget-object v1, p0, Lfch;->h:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfct;

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lfct;

    invoke-direct {v0, p0, p1}, Lfct;-><init>(Lfch;Lfar;)V

    iget-object v2, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lfcd;->a(Landroid/os/IBinder;)Lfcc;

    move-result-object v0

    return-object v0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "post_init_configuration"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "use_contactables_api"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Lfdy;->a(Z)V

    sget-object v1, Lfcg;->a:Lfcg;

    invoke-virtual {v1, v0}, Lfcg;->a(Landroid/os/Bundle;)V

    const-string v1, "config.email_type_map"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sput-object v1, Lfch;->i:Landroid/os/Bundle;

    const-string v1, "config.phone_type_map"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lfch;->j:Landroid/os/Bundle;

    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, p1, p2, v0}, Lbje;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_1
    const-string v0, "post_init_resolution"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Lbjy;Lbjj;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "social_client_application_id"

    iget-object v2, p0, Lfch;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "real_client_package_name"

    iget-object v2, p0, Lfch;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x41fe88

    iget-object v2, p0, Lbje;->b_:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lbjy;->c(Lbjv;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lfam;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfck;

    invoke-direct {v1, p0, p1}, Lfck;-><init>(Lfch;Lfam;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    const/4 v5, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lfcc;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lfck;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfan;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfcm;

    invoke-direct {v1, p0, p1}, Lfcm;-><init>(Lfch;Lfan;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lfcc;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lfcm;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfao;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfco;

    invoke-direct {v1, p0, p1}, Lfco;-><init>(Lfch;Lfao;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    invoke-interface {v0, v1, p2, p3}, Lfcc;->b(Lfbz;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfco;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfap;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfcq;

    invoke-direct {v1, p0, p1}, Lfcq;-><init>(Lfch;Lfap;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lfcc;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lfcq;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfaq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 8

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfcs;

    invoke-direct {v1, p0, p1}, Lfcs;-><init>(Lfch;Lfaq;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    const/4 v4, 0x0

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lfcc;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lfcs;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public final a(Lfar;)V
    .locals 7

    iget-object v6, p0, Lfch;->h:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    invoke-super {p0}, Lbje;->h()V

    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfct;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lfcc;->a(Lfbz;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_1
    :try_start_4
    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :catch_0
    move-exception v0

    :try_start_5
    const-string v1, "PeopleClient"

    const-string v2, "Failed to unregister listener"

    invoke-static {v1, v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_6
    iget-object v1, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public final a(Lfas;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfdb;

    invoke-direct {v1, p0, p1}, Lfdb;-><init>(Lfch;Lfas;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    invoke-interface {v0, v1, p2}, Lfcc;->a(Lfbz;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfdb;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public final a(Lfas;Ljava/lang/String;II)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfdb;

    invoke-direct {v1, p0, p1}, Lfdb;-><init>(Lfch;Lfas;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    invoke-interface {v0, v1, p2, p3, p4}, Lfcc;->a(Lfbz;Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfdb;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public final a(Lfat;Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfcu;

    invoke-direct {v1, p0, p1}, Lfcu;-><init>(Lfch;Lfat;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    invoke-interface {v0, v1, p2}, Lfcc;->b(Lfbz;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfcu;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfau;Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfcw;

    invoke-direct {v1, p0, p1}, Lfcw;-><init>(Lfch;Lfau;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfcc;->a(Lfbz;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v3, v3}, Lfcw;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfav;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfcy;

    invoke-direct {v1, p0, p1}, Lfcy;-><init>(Lfch;Lfav;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    invoke-interface {v0, v1, p2, p3}, Lfcc;->c(Lfbz;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfcy;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfaw;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfda;

    invoke-direct {v1, p0, p1}, Lfda;-><init>(Lfch;Lfaw;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    const/4 v6, 0x0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v6}, Lfcc;->a(Lfbz;ZZLjava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lfda;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public final a(Lfax;Ljava/lang/String;Ljava/lang/String;Lfal;)V
    .locals 12

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfdc;

    invoke-direct {v1, p0, p1}, Lfdc;-><init>(Lfch;Lfax;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    invoke-virtual/range {p4 .. p4}, Lfal;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, Lfal;->b()Ljava/util/Collection;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v5, 0x0

    :goto_0
    invoke-virtual/range {p4 .. p4}, Lfal;->c()I

    move-result v6

    invoke-virtual/range {p4 .. p4}, Lfal;->d()Z

    move-result v7

    invoke-virtual/range {p4 .. p4}, Lfal;->e()J

    move-result-wide v8

    invoke-virtual/range {p4 .. p4}, Lfal;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lfal;->g()I

    move-result v11

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v11}, Lfcc;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V

    :goto_1
    return-void

    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lfdc;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_1
.end method

.method public final a(Lfay;Ljava/lang/String;Ljava/lang/String;Lfak;)V
    .locals 8

    const/4 v7, 0x0

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfdd;

    invoke-direct {v1, p0, p1}, Lfdd;-><init>(Lfch;Lfay;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    invoke-virtual {p4}, Lfak;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p4}, Lfak;->b()I

    move-result v5

    invoke-virtual {p4}, Lfak;->c()Ljava/lang/String;

    move-result-object v6

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v6}, Lfcc;->b(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lfdd;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public final a(Lfaz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 8

    invoke-super {p0}, Lbje;->h()V

    new-instance v1, Lfde;

    invoke-direct {v1, p0, p1}, Lfde;-><init>(Lfch;Lfaz;)V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Lfcc;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lfde;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfar;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 8

    const/4 v6, 0x1

    invoke-super {p0}, Lbje;->h()V

    iget-object v7, p0, Lfch;->h:Ljava/util/HashMap;

    monitor-enter v7

    :try_start_0
    invoke-direct {p0, p1}, Lfch;->b(Lfar;)Lfct;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    const/4 v2, 0x1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lfcc;->a(Lfbz;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v7

    move v0, v6

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "PeopleClient"

    const-string v2, "Failed to register listener"

    invoke-static {v1, v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 8

    const/4 v7, 0x0

    invoke-super {p0}, Lbje;->h()V

    :try_start_0
    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-interface/range {v0 .. v6}, Lfcc;->a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "PeopleClient"

    const-string v2, "Service call failed."

    invoke-static {v1, v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v7

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    iget-object v6, p0, Lfch;->h:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    invoke-virtual {p0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfct;

    invoke-super {p0}, Lbje;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfcc;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lfcc;->a(Lfbz;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "PeopleClient"

    const-string v2, "Failed to unregister listener"

    invoke-static {v1, v2, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lfch;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Lbje;->b()V

    return-void

    :catch_1
    move-exception v0

    :try_start_2
    const-string v1, "PeopleClient"

    const-string v2, "PeopleService is in unexpected state"

    invoke-static {v1, v2, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final b(Lbjg;)V
    .locals 0

    invoke-super {p0, p1}, Lbje;->a(Lbjg;)V

    return-void
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.people.service.START"

    return-object v0
.end method

.method protected final c_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.people.internal.IPeopleService"

    return-object v0
.end method
