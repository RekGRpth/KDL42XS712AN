.class final Lbdz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbdu;


# instance fields
.field final a:Ljava/util/concurrent/locks/Lock;

.field final b:Ljava/util/Queue;

.field c:Lbbo;

.field d:I

.field e:I

.field f:I

.field g:J

.field final h:Landroid/os/Handler;

.field final i:Landroid/os/Bundle;

.field j:Z

.field final k:Ljava/util/Set;

.field final l:Lbdx;

.field private final m:Ljava/util/concurrent/locks/Condition;

.field private final n:Lbjn;

.field private o:Z

.field private p:I

.field private final q:Ljava/util/Map;

.field private final r:Lbee;

.field private final s:Lbjp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)V
    .locals 9

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lbdz;->m:Ljava/util/concurrent/locks/Condition;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbdz;->b:Ljava/util/Queue;

    const/4 v0, 0x4

    iput v0, p0, Lbdz;->e:I

    iput v1, p0, Lbdz;->f:I

    iput-boolean v1, p0, Lbdz;->o:Z

    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lbdz;->g:J

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbdz;->i:Landroid/os/Bundle;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbdz;->q:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbdz;->k:Ljava/util/Set;

    new-instance v0, Lbea;

    invoke-direct {v0, p0}, Lbea;-><init>(Lbdz;)V

    iput-object v0, p0, Lbdz;->r:Lbee;

    new-instance v0, Lbeb;

    invoke-direct {v0, p0}, Lbeb;-><init>(Lbdz;)V

    iput-object v0, p0, Lbdz;->l:Lbdx;

    new-instance v0, Lbec;

    invoke-direct {v0, p0}, Lbec;-><init>(Lbdz;)V

    iput-object v0, p0, Lbdz;->s:Lbjp;

    new-instance v0, Lbjn;

    iget-object v1, p0, Lbdz;->s:Lbjp;

    invoke-direct {v0, p2, v1}, Lbjn;-><init>(Landroid/os/Looper;Lbjp;)V

    iput-object v0, p0, Lbdz;->n:Lbjn;

    new-instance v0, Lbef;

    invoke-direct {v0, p0, p2}, Lbef;-><init>(Lbdz;Landroid/os/Looper;)V

    iput-object v0, p0, Lbdz;->h:Landroid/os/Handler;

    invoke-interface {p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdx;

    iget-object v2, p0, Lbdz;->n:Lbjn;

    invoke-virtual {v2, v0}, Lbjn;->a(Lbdx;)V

    goto :goto_0

    :cond_0
    invoke-interface {p6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdy;

    iget-object v2, p0, Lbdz;->n:Lbjn;

    invoke-virtual {v2, v0}, Lbjn;->a(Lbbs;)V

    goto :goto_1

    :cond_1
    invoke-interface {p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbdm;

    iget-object v0, v1, Lbdm;->a:Lbdo;

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbdv;

    iget-object v8, p0, Lbdz;->q:Ljava/util/Map;

    iget-object v5, p0, Lbdz;->l:Lbdx;

    new-instance v6, Lbed;

    invoke-direct {v6, p0, v0}, Lbed;-><init>(Lbdz;Lbdo;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v6}, Lbdo;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Lbdv;Lbdx;Lbdy;)Lbdn;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    return-void
.end method

.method static synthetic a(Lbdz;)V
    .locals 4

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lbdz;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbdz;->p:I

    iget v0, p0, Lbdz;->p:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lbdz;->c:Lbbo;

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbdz;->o:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbdz;->a(I)V

    invoke-virtual {p0}, Lbdz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lbdz;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbdz;->f:I

    :cond_0
    invoke-virtual {p0}, Lbdz;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbdz;->h:Landroid/os/Handler;

    iget-object v1, p0, Lbdz;->h:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lbdz;->g:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbdz;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lbdz;->n:Lbjn;

    iget-object v1, p0, Lbdz;->c:Lbbo;

    invoke-virtual {v0, v1}, Lbjn;->a(Lbbo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_3
    const/4 v0, 0x2

    :try_start_2
    iput v0, p0, Lbdz;->e:I

    invoke-direct {p0}, Lbdz;->h()V

    iget-object v0, p0, Lbdz;->m:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    invoke-direct {p0}, Lbdz;->g()V

    iget-boolean v0, p0, Lbdz;->o:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbdz;->o:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbdz;->a(I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lbdz;->i:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lbdz;->n:Lbjn;

    invoke-virtual {v1, v0}, Lbjn;->a(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lbdz;->i:Landroid/os/Bundle;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private a(Lbeg;)V
    .locals 2

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lbdz;->d()Z

    move-result v0

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-interface {p1}, Lbeg;->a()Lbdo;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed or enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    instance-of v0, p1, Lbej;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbdz;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lbdz;->r:Lbee;

    invoke-interface {p1, v0}, Lbeg;->a(Lbee;)V

    :cond_0
    invoke-interface {p1}, Lbeg;->a()Lbdo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbdz;->a(Lbdo;)Lbdn;

    move-result-object v0

    invoke-interface {p1, v0}, Lbeg;->b(Lbdn;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private g()V
    .locals 3

    invoke-virtual {p0}, Lbdz;->d()Z

    move-result v0

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :goto_0
    :try_start_0
    iget-object v0, p0, Lbdz;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lbdz;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbeg;

    invoke-direct {p0, v0}, Lbdz;->a(Lbeg;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "GoogleApiClientImpl"

    const-string v2, "Service died while flushing queue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lbdz;->f:I

    iget-object v0, p0, Lbdz;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a(Lbdo;)Lbdn;
    .locals 2

    iget-object v0, p0, Lbdz;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdn;

    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final a(Lbdq;)Lbdq;
    .locals 2

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lbdz;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbdz;->b(Lbdq;)Lbdq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbdz;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lbdz;->o:Z

    invoke-virtual {p0}, Lbdz;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbdz;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lbdz;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lbdz;->c:Lbbo;

    const/4 v0, 0x1

    iput v0, p0, Lbdz;->e:I

    iget-object v0, p0, Lbdz;->i:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lbdz;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lbdz;->p:I

    iget-object v0, p0, Lbdz;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdn;

    invoke-interface {v0}, Lbdn;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method final a(I)V
    .locals 5

    const/4 v1, 0x3

    const/4 v4, -0x1

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lbdz;->e:I

    if-eq v0, v1, :cond_9

    if-ne p1, v4, :cond_2

    invoke-virtual {p0}, Lbdz;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbdz;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbdz;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    :cond_1
    iget-object v0, p0, Lbdz;->c:Lbbo;

    if-nez v0, :cond_2

    iget-object v0, p0, Lbdz;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbdz;->o:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-void

    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lbdz;->e()Z

    move-result v0

    invoke-virtual {p0}, Lbdz;->d()Z

    move-result v1

    const/4 v2, 0x3

    iput v2, p0, Lbdz;->e:I

    if-eqz v0, :cond_4

    if-ne p1, v4, :cond_3

    const/4 v0, 0x0

    iput-object v0, p0, Lbdz;->c:Lbbo;

    :cond_3
    iget-object v0, p0, Lbdz;->m:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    :cond_4
    iget-object v0, p0, Lbdz;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbeg;

    invoke-interface {v0}, Lbeg;->d()V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lbdz;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbdz;->j:Z

    iget-object v0, p0, Lbdz;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdn;

    invoke-interface {v0}, Lbdn;->d_()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Lbdn;->b()V

    goto :goto_3

    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbdz;->j:Z

    const/4 v0, 0x4

    iput v0, p0, Lbdz;->e:I

    if-eqz v1, :cond_9

    if-eq p1, v4, :cond_8

    iget-object v0, p0, Lbdz;->n:Lbjn;

    invoke-virtual {v0, p1}, Lbjn;->a(I)V

    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbdz;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_9
    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1
.end method

.method public final a(Lbdx;)V
    .locals 1

    iget-object v0, p0, Lbdz;->n:Lbjn;

    invoke-virtual {v0, p1}, Lbjn;->a(Lbdx;)V

    return-void
.end method

.method public final a(Lbdy;)V
    .locals 1

    iget-object v0, p0, Lbdz;->n:Lbjn;

    invoke-virtual {v0, p1}, Lbjn;->a(Lbbs;)V

    return-void
.end method

.method public final b(Lbdq;)Lbdq;
    .locals 2

    invoke-virtual {p0}, Lbdz;->d()Z

    move-result v0

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lbdz;->g()V

    :try_start_0
    invoke-direct {p0, p1}, Lbdz;->a(Lbeg;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p1

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbdz;->a(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lbdz;->h()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbdz;->a(I)V

    return-void
.end method

.method public final b(Lbdx;)V
    .locals 1

    iget-object v0, p0, Lbdz;->n:Lbjn;

    invoke-virtual {v0, p1}, Lbjn;->c(Lbdx;)V

    return-void
.end method

.method public final b(Lbdy;)V
    .locals 4

    iget-object v0, p0, Lbdz;->n:Lbjn;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lbjn;->d:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lbjn;->d:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lbjn;->e:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lbjn;->d:Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lbjn;->d:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, v0, Lbjn;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionFailedListener(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 0

    invoke-virtual {p0}, Lbdz;->b()V

    invoke-virtual {p0}, Lbdz;->a()V

    return-void
.end method

.method public final d()Z
    .locals 2

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lbdz;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v1, p0, Lbdz;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method final f()Z
    .locals 2

    iget-object v0, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lbdz;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbdz;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
