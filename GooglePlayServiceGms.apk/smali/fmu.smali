.class public final Lfmu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:Landroid/content/Intent;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfmu;->d:Landroid/content/Context;

    iput-object p2, p0, Lfmu;->e:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfmu;->a:Z

    const/4 v0, 0x2

    iput v0, p0, Lfmu;->b:I

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 10

    const/4 v7, 0x0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lfmu;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfmu;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->i()Ljava/lang/String;

    move-result-object v5

    :goto_0
    iget-boolean v1, p0, Lfmu;->a:Z

    if-eqz v1, :cond_1

    move-object v1, v7

    :goto_1
    iget-object v2, p0, Lfmu;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfmu;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lfmu;->b:I

    iget-object v6, p0, Lfmu;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->j()[Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lfmu;->d:Landroid/content/Context;

    iget-object v9, p0, Lfmu;->c:Landroid/content/Intent;

    if-nez v9, :cond_2

    :goto_2
    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V

    iget-object v1, p0, Lfmu;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->k()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->d()V

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lfmu;->d:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v1

    :cond_0
    move-object v5, v7

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lfmu;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    const/high16 v7, 0x8000000

    invoke-static {v8, v9, v7}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    goto :goto_2
.end method
