.class abstract Lbjc;
.super Lbja;
.source "SourceFile"


# instance fields
.field final d:Ljava/lang/CharSequence;

.field final e:Z

.field f:I


# direct methods
.method protected constructor <init>(Lbiw;Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbja;-><init>(B)V

    iput v0, p0, Lbjc;->f:I

    iget-boolean v0, p1, Lbiw;->a:Z

    iput-boolean v0, p0, Lbjc;->e:Z

    iput-object p2, p0, Lbjc;->d:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method abstract a(I)I
.end method

.method protected final synthetic a()Ljava/lang/Object;
    .locals 4

    const/4 v3, -0x1

    :cond_0
    iget v0, p0, Lbjc;->f:I

    if-eq v0, v3, :cond_3

    iget v1, p0, Lbjc;->f:I

    iget v0, p0, Lbjc;->f:I

    invoke-virtual {p0, v0}, Lbjc;->a(I)I

    move-result v0

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lbjc;->d:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iput v3, p0, Lbjc;->f:I

    :goto_0
    iget-boolean v2, p0, Lbjc;->e:Z

    if-eqz v2, :cond_1

    if-eq v1, v0, :cond_0

    :cond_1
    iget-object v2, p0, Lbjc;->d:Ljava/lang/CharSequence;

    invoke-interface {v2, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {p0, v0}, Lbjc;->b(I)I

    move-result v2

    iput v2, p0, Lbjc;->f:I

    goto :goto_0

    :cond_3
    sget-object v0, Lbjb;->c:Lbjb;

    iput-object v0, p0, Lbja;->b:Lbjb;

    const/4 v0, 0x0

    goto :goto_1
.end method

.method abstract b(I)I
.end method
