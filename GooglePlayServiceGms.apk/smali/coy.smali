.class public Lcoy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcoy;

.field private static b:Lcoy;


# instance fields
.field private final c:Lbuh;

.field private final d:Lcon;

.field private final e:Lcos;

.field private final f:Landroid/content/Context;

.field private final g:Lcdu;

.field private final h:Lbve;

.field private final i:Lcfz;

.field private final j:Lbrl;

.field private final k:Lcmn;

.field private final l:Lcby;

.field private final m:Lcll;

.field private final n:Lbwd;

.field private final o:Lcon;

.field private final p:Lbtd;

.field private final q:Lbst;

.field private final r:Lbwq;

.field private final s:Lchd;

.field private final t:Lbra;

.field private final u:Lbwo;

.field private final v:Lcnf;

.field private final w:Lbsr;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcoy;->f:Landroid/content/Context;

    new-instance v0, Lbtd;

    invoke-direct {v0}, Lbtd;-><init>()V

    iput-object v0, p0, Lcoy;->p:Lbtd;

    new-instance v0, Lcbi;

    invoke-direct {v0, p1}, Lcbi;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcoy;->e:Lcos;

    new-instance v0, Lccd;

    invoke-direct {v0, p1}, Lccd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcoy;->l:Lcby;

    new-instance v0, Lcdu;

    iget-object v1, p0, Lcoy;->l:Lcby;

    iget-object v2, p0, Lcoy;->p:Lbtd;

    invoke-direct {v0, p1, v1, v2}, Lcdu;-><init>(Landroid/content/Context;Lcby;Lbtd;)V

    iput-object v0, p0, Lcoy;->g:Lcdu;

    new-instance v0, Lclp;

    invoke-direct {v0, p1}, Lclp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcoy;->m:Lcll;

    sget-object v0, Lcoo;->a:Lcoo;

    iput-object v0, p0, Lcoy;->d:Lcon;

    sget-object v0, Lcoo;->b:Lcoo;

    iput-object v0, p0, Lcoy;->o:Lcon;

    new-instance v0, Lcfi;

    iget-object v1, p0, Lcoy;->g:Lcdu;

    invoke-direct {v0, v1}, Lcfi;-><init>(Lcdu;)V

    iput-object v0, p0, Lcoy;->i:Lcfz;

    new-instance v0, Lcmn;

    iget-object v1, p0, Lcoy;->i:Lcfz;

    iget-object v2, p0, Lcoy;->g:Lcdu;

    iget-object v3, p0, Lcoy;->e:Lcos;

    invoke-direct {v0, p0, v1, v2, v3}, Lcmn;-><init>(Lcoy;Lcfz;Lcdu;Lcos;)V

    iput-object v0, p0, Lcoy;->k:Lcmn;

    new-instance v0, Lbst;

    iget-object v1, p0, Lcoy;->g:Lcdu;

    iget-object v2, p0, Lcoy;->i:Lcfz;

    iget-object v3, p0, Lcoy;->d:Lcon;

    iget-object v4, p0, Lcoy;->l:Lcby;

    invoke-direct {v0, v1, v2, v3, v4}, Lbst;-><init>(Lcdu;Lcfz;Lcon;Lcby;)V

    iput-object v0, p0, Lcoy;->q:Lbst;

    new-instance v0, Lbve;

    iget-object v1, p0, Lcoy;->i:Lcfz;

    iget-object v2, p0, Lcoy;->l:Lcby;

    iget-object v3, p0, Lcoy;->k:Lcmn;

    iget-object v4, p0, Lcoy;->q:Lbst;

    iget-object v5, p0, Lcoy;->g:Lcdu;

    iget-object v6, p0, Lcoy;->d:Lcon;

    invoke-direct/range {v0 .. v6}, Lbve;-><init>(Lcfz;Lcby;Lcmn;Lbst;Lcdu;Lcon;)V

    iput-object v0, p0, Lcoy;->h:Lbve;

    new-instance v0, Lbuh;

    new-instance v1, Lbui;

    invoke-direct {v1, p1}, Lbui;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p1, v1}, Lbuh;-><init>(Landroid/content/Context;Lbum;)V

    iput-object v0, p0, Lcoy;->c:Lbuh;

    new-instance v0, Lbwq;

    iget-object v1, p0, Lcoy;->q:Lbst;

    invoke-direct {v0, v1}, Lbwq;-><init>(Lbst;)V

    iput-object v0, p0, Lcoy;->r:Lbwq;

    new-instance v0, Lchd;

    iget-object v1, p0, Lcoy;->g:Lcdu;

    iget-object v2, p0, Lcoy;->i:Lcfz;

    invoke-direct {v0, p1, v1, v2}, Lchd;-><init>(Landroid/content/Context;Lcdu;Lcfz;)V

    iput-object v0, p0, Lcoy;->s:Lchd;

    new-instance v0, Lbra;

    iget-object v1, p0, Lcoy;->s:Lchd;

    invoke-direct {v0, v1}, Lbra;-><init>(Lchd;)V

    iput-object v0, p0, Lcoy;->t:Lbra;

    new-instance v0, Lbsr;

    iget-object v1, p0, Lcoy;->i:Lcfz;

    iget-object v2, p0, Lcoy;->l:Lcby;

    iget-object v3, p0, Lcoy;->q:Lbst;

    invoke-direct {v0, v1, v2, v3}, Lbsr;-><init>(Lcfz;Lcby;Lbst;)V

    iput-object v0, p0, Lcoy;->w:Lbsr;

    new-instance v0, Lbrg;

    iget-object v1, p0, Lcoy;->k:Lcmn;

    iget-object v2, p0, Lcoy;->q:Lbst;

    iget-object v3, p0, Lcoy;->g:Lcdu;

    iget-object v4, p0, Lcoy;->d:Lcon;

    iget-object v5, p0, Lcoy;->w:Lbsr;

    invoke-direct/range {v0 .. v5}, Lbrg;-><init>(Lcmn;Lbst;Lcdu;Lcon;Lbsr;)V

    iput-object v0, p0, Lcoy;->j:Lbrl;

    new-instance v0, Lbwo;

    iget-object v1, p0, Lcoy;->c:Lbuh;

    iget-object v2, p0, Lcoy;->r:Lbwq;

    iget-object v3, p0, Lcoy;->q:Lbst;

    iget-object v4, p0, Lcoy;->i:Lcfz;

    iget-object v5, p0, Lcoy;->w:Lbsr;

    invoke-direct/range {v0 .. v5}, Lbwo;-><init>(Lbuh;Lbwq;Lbst;Lcfz;Lbsr;)V

    iput-object v0, p0, Lcoy;->u:Lbwo;

    new-instance v0, Lcnf;

    iget-object v1, p0, Lcoy;->e:Lcos;

    iget-object v2, p0, Lcoy;->i:Lcfz;

    iget-object v3, p0, Lcoy;->l:Lcby;

    iget-object v4, p0, Lcoy;->u:Lbwo;

    new-instance v5, Lbwb;

    iget-object v6, p0, Lcoy;->m:Lcll;

    iget-object v7, p0, Lcoy;->i:Lcfz;

    iget-object v8, p0, Lcoy;->d:Lcon;

    invoke-direct {v5, v6, v7, v8}, Lbwb;-><init>(Lcll;Lcfz;Lcon;)V

    invoke-direct/range {v0 .. v5}, Lcnf;-><init>(Lcos;Lcfz;Lcby;Lbwo;Lbwb;)V

    iput-object v0, p0, Lcoy;->v:Lcnf;

    new-instance v0, Lbwd;

    iget-object v2, p0, Lcoy;->e:Lcos;

    iget-object v3, p0, Lcoy;->d:Lcon;

    iget-object v4, p0, Lcoy;->l:Lcby;

    iget-object v5, p0, Lcoy;->v:Lcnf;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lbwd;-><init>(Landroid/content/Context;Lcos;Lcon;Lcby;Lcnf;)V

    iput-object v0, p0, Lcoy;->n:Lbwd;

    iget-object v0, p0, Lcoy;->g:Lcdu;

    iget-object v1, p0, Lcoy;->t:Lbra;

    iput-object v1, v0, Lcdu;->b:Lchb;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcoy;
    .locals 3

    sget-object v0, Lcoy;->a:Lcoy;

    if-eqz v0, :cond_0

    sget-object v0, Lcoy;->a:Lcoy;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcoy;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcoy;->b:Lcoy;

    if-eqz v2, :cond_1

    sget-object v2, Lcoy;->b:Lcoy;

    iget-object v2, v2, Lcoy;->f:Landroid/content/Context;

    if-eq v2, v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "attempted to initialize Singletons multiple times with different application context instances."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    new-instance v2, Lcoy;

    invoke-direct {v2, v0}, Lcoy;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcoy;->b:Lcoy;

    :cond_2
    sget-object v0, Lcoy;->b:Lcoy;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcon;
    .locals 1

    iget-object v0, p0, Lcoy;->d:Lcon;

    return-object v0
.end method

.method public final b()Lcos;
    .locals 1

    iget-object v0, p0, Lcoy;->e:Lcos;

    return-object v0
.end method

.method public final c()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcoy;->f:Landroid/content/Context;

    return-object v0
.end method

.method public final d()Lcdu;
    .locals 1

    iget-object v0, p0, Lcoy;->g:Lcdu;

    return-object v0
.end method

.method public final e()Lbve;
    .locals 1

    iget-object v0, p0, Lcoy;->h:Lbve;

    return-object v0
.end method

.method public final f()Lcfz;
    .locals 1

    iget-object v0, p0, Lcoy;->i:Lcfz;

    return-object v0
.end method

.method public final g()Lbrl;
    .locals 1

    iget-object v0, p0, Lcoy;->j:Lbrl;

    return-object v0
.end method

.method public final h()Lcmn;
    .locals 1

    iget-object v0, p0, Lcoy;->k:Lcmn;

    return-object v0
.end method

.method public final i()Lcby;
    .locals 1

    iget-object v0, p0, Lcoy;->l:Lcby;

    return-object v0
.end method

.method public final j()Lcll;
    .locals 1

    iget-object v0, p0, Lcoy;->m:Lcll;

    return-object v0
.end method

.method public final k()Lbwd;
    .locals 1

    iget-object v0, p0, Lcoy;->n:Lbwd;

    return-object v0
.end method

.method public final l()Lcon;
    .locals 1

    iget-object v0, p0, Lcoy;->o:Lcon;

    return-object v0
.end method

.method public final m()Lbtd;
    .locals 1

    iget-object v0, p0, Lcoy;->p:Lbtd;

    return-object v0
.end method

.method public final n()Lbst;
    .locals 1

    iget-object v0, p0, Lcoy;->q:Lbst;

    return-object v0
.end method

.method public final o()Lchd;
    .locals 1

    iget-object v0, p0, Lcoy;->s:Lchd;

    return-object v0
.end method

.method public final p()Lbra;
    .locals 1

    iget-object v0, p0, Lcoy;->t:Lbra;

    return-object v0
.end method

.method public final q()Lbwo;
    .locals 1

    iget-object v0, p0, Lcoy;->u:Lbwo;

    return-object v0
.end method

.method public final r()Lcnf;
    .locals 1

    iget-object v0, p0, Lcoy;->v:Lcnf;

    return-object v0
.end method

.method public final s()Lbsr;
    .locals 1

    iget-object v0, p0, Lcoy;->w:Lbsr;

    return-object v0
.end method
