.class public final Leda;
.super Lecz;
.source "SourceFile"

# interfaces
.implements Lbel;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lecz;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public final synthetic a(Lbek;)V
    .locals 6

    const/4 v5, 0x7

    const/4 v4, 0x4

    const/16 v3, 0x2712

    check-cast p1, Lctx;

    invoke-virtual {p0}, Leda;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lctx;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Lctx;->a()Lcts;

    move-result-object v1

    if-ne v0, v4, :cond_1

    invoke-virtual {p0, v3}, Leda;->e(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v2, 0x3eb

    if-ne v0, v2, :cond_2

    const/16 v0, 0x4e20

    invoke-virtual {p0, v0}, Leda;->e(I)V

    goto :goto_0

    :cond_2
    const/16 v2, 0x3e9

    if-ne v0, v2, :cond_3

    invoke-virtual {p0, v4}, Leda;->c(I)V

    goto :goto_0

    :cond_3
    const/16 v2, 0x3e8

    if-ne v0, v2, :cond_4

    invoke-virtual {p0, v3}, Leda;->e(I)V

    goto :goto_0

    :cond_4
    const/16 v2, 0x5dc

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Leda;->c(I)V

    goto :goto_0

    :cond_5
    const-string v0, "LoadSelfFragment"

    const-string v1, "Too many out-of-box exceptions - bailing out of sign-in."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Leda;->e(I)V

    goto :goto_0

    :cond_6
    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Leda;->c(I)V

    goto :goto_0

    :cond_7
    if-ne v0, v5, :cond_8

    const/16 v0, 0x2713

    invoke-virtual {p0, v0}, Leda;->e(I)V

    goto :goto_0

    :cond_8
    const/16 v2, 0x3ea

    if-ne v0, v2, :cond_9

    const-string v0, "LoadSelfFragment"

    const-string v1, "Unable to sign in - application does not have a registered client ID"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x2714

    invoke-virtual {p0, v0}, Leda;->e(I)V

    goto :goto_0

    :cond_9
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Lcts;->a()I

    move-result v2

    if-lez v2, :cond_a

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcts;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_a
    invoke-virtual {v1}, Lcts;->b()V

    if-eqz v0, :cond_b

    iget-object v1, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Leda;->c(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcts;->b()V

    throw v0

    :cond_b
    const-string v0, "LoadSelfFragment"

    const-string v1, "No player found when signing in"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Leda;->e(I)V

    goto/16 :goto_0
.end method

.method public final a(Lduk;)V
    .locals 5

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lduk;->c()V

    :try_start_0
    iget-object v1, p1, Lduk;->f:Ldam;

    new-instance v2, Lduz;

    invoke-direct {v2, p1, p0}, Lduz;-><init>(Lduk;Lbel;)V

    iget-object v3, p1, Lduk;->d:Ljava/lang/String;

    iget-object v4, p1, Lduk;->e:[Ljava/lang/String;

    invoke-interface {v1, v2, v0, v3, v4}, Ldam;->b(Ldaj;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method
