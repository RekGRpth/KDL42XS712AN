.class public final Lezd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field private final b:Lezf;

.field private c:Z

.field private d:F

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/view/Display;Lezf;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lezd;->c:Z

    iput-boolean v1, p0, Lezd;->e:Z

    iput-object p2, p0, Lezd;->b:Lezf;

    iput-boolean v1, p0, Lezd;->e:Z

    invoke-virtual {p1}, Landroid/view/Display;->getRotation()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    iput v1, p0, Lezd;->a:I

    :goto_0
    iget v2, p0, Lezd;->a:I

    int-to-float v2, v2

    iput v2, p0, Lezd;->d:F

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-lt v2, v3, :cond_0

    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {v1, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v2, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    if-nez v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lezd;->f:Z

    :cond_0
    invoke-direct {p0}, Lezd;->b()V

    return-void

    :pswitch_0
    iput v1, p0, Lezd;->a:I

    goto :goto_0

    :pswitch_1
    const/16 v2, 0x5a

    iput v2, p0, Lezd;->a:I

    goto :goto_0

    :pswitch_2
    const/16 v2, 0xb4

    iput v2, p0, Lezd;->a:I

    goto :goto_0

    :pswitch_3
    const/16 v2, -0x5a

    iput v2, p0, Lezd;->a:I

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b()V
    .locals 8

    const/high16 v5, 0x3fc00000    # 1.5f

    const/high16 v7, -0x3d4c0000    # -90.0f

    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lezd;->b:Lezf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lezd;->b:Lezf;

    iget-boolean v0, v0, Lezf;->d:Z

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "LightCycle"

    const-string v1, "Sensor reader is not initialized."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lezd;->b:Lezf;

    iget-object v3, v0, Lezf;->c:Leyk;

    iget-boolean v0, p0, Lezd;->e:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lezd;->f:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iget v4, v3, Leyk;->a:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, v3, Leyk;->b:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_3

    move v2, v1

    :cond_3
    xor-int/2addr v0, v2

    iput-boolean v0, p0, Lezd;->c:Z

    iput-boolean v1, p0, Lezd;->e:Z

    :goto_2
    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lezd;->c:Z

    iget-boolean v1, p0, Lezd;->f:Z

    xor-int/2addr v0, v1

    if-eqz v0, :cond_c

    iget v0, v3, Leyk;->a:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_b

    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lezd;->d:F

    :goto_3
    iget v0, p0, Lezd;->d:F

    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_e

    const/high16 v1, 0x43b40000    # 360.0f

    sub-float/2addr v0, v1

    :cond_4
    :goto_4
    iput v0, p0, Lezd;->d:F

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iget-boolean v0, p0, Lezd;->f:Z

    iget-boolean v4, p0, Lezd;->c:Z

    xor-int/2addr v0, v4

    if-eqz v0, :cond_9

    iget v0, v3, Leyk;->b:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, v3, Leyk;->a:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float/2addr v4, v5

    cmpl-float v0, v0, v4

    if-lez v0, :cond_8

    move v0, v1

    :goto_5
    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lezd;->c:Z

    if-nez v0, :cond_7

    move v2, v1

    :cond_7
    iput-boolean v2, p0, Lezd;->c:Z

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_5

    :cond_9
    iget v0, v3, Leyk;->a:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, v3, Leyk;->b:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float/2addr v4, v5

    cmpl-float v0, v0, v4

    if-lez v0, :cond_a

    move v0, v1

    goto :goto_5

    :cond_a
    move v0, v2

    goto :goto_5

    :cond_b
    iput v7, p0, Lezd;->d:F

    goto :goto_3

    :cond_c
    iget v0, v3, Leyk;->b:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_d

    iput v6, p0, Lezd;->d:F

    goto :goto_3

    :cond_d
    const/high16 v0, 0x43340000    # 180.0f

    iput v0, p0, Lezd;->d:F

    goto :goto_3

    :cond_e
    cmpg-float v1, v0, v7

    if-gez v1, :cond_4

    const/high16 v1, 0x43b40000    # 360.0f

    add-float/2addr v0, v1

    goto :goto_4

    :cond_f
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-direct {p0}, Lezd;->b()V

    iget v0, p0, Lezd;->d:F

    float-to-int v0, v0

    iput v0, p0, Lezd;->a:I

    return-void
.end method
