.class public final Lboe;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/gms/common/ui/ErrorDialogActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lboe;->a:Landroid/content/Intent;

    iget-object v0, p0, Lboe;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_TITLE"

    const v2, 0x7f0b046f    # com.google.android.gms.R.string.common_google_play_services_error_dialog_title

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lboe;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_MESSAGE"

    const v2, 0x7f0b0470    # com.google.android.gms.R.string.common_google_play_services_no_restricted_profiles

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lboe;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_RESULT_CODE"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lboe;->a:Landroid/content/Intent;

    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public final a()Lboe;
    .locals 3

    iget-object v0, p0, Lboe;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_RESULT_CODE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object p0
.end method
