.class public final Lfob;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lal;
.implements Lfpf;


# instance fields
.field private Y:Lfnz;

.field private final Z:Ljava/util/Map;

.field a:Landroid/accounts/Account;

.field b:I

.field final c:Ljava/util/ArrayList;

.field final d:Ljava/util/HashSet;

.field private e:Ljava/lang/String;

.field private f:I

.field private final g:Ljava/util/ArrayList;

.field private h:Ljava/lang/String;

.field private i:Lfpd;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lfob;->b:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfob;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfob;->g:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfob;->d:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfob;->Z:Ljava/util/Map;

    return-void
.end method

.method private K()V
    .locals 3

    const/4 v1, 0x1

    invoke-static {}, Lfot;->a()Lfot;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, v1}, Lfot;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lfob;->L()V

    invoke-direct {p0, v1}, Lfob;->e(I)V

    invoke-virtual {p0}, Lfob;->s()Lak;

    move-result-object v0

    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    :cond_0
    return-void
.end method

.method private L()V
    .locals 1

    iget-object v0, p0, Lfob;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfob;->h:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lfob;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lfob;

    invoke-direct {v1}, Lfob;-><init>()V

    invoke-virtual {v1, v0}, Lfob;->g(Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;
    .locals 6

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "service_googleme"

    aput-object v1, v5, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lfob;->a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;[Ljava/lang/String;)Lftx;

    move-result-object v0

    return-object v0
.end method

.method private static final a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;[Ljava/lang/String;)Lftx;
    .locals 4

    new-instance v0, Lfwa;

    invoke-direct {v0, p1}, Lfwa;-><init>(Landroid/content/Context;)V

    iput-object p4, v0, Lfwa;->a:Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "https://www.googleapis.com/auth/plus.applications.manage"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "https://www.googleapis.com/auth/grants.audit"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "https://www.googleapis.com/auth/plus.me"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lfwa;->a([Ljava/lang/String;)Lfwa;

    move-result-object v0

    iput-object p5, v0, Lfwa;->d:[Ljava/lang/String;

    const-string v1, "81"

    iput-object v1, v0, Lfwa;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    invoke-interface {p0, p1, v0, p2, p3}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;)[Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbov;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;
    .locals 6

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lfob;->a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;[Ljava/lang/String;)Lftx;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0}, Lfob;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v3

    array-length v2, v3

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v2, "com.google.android.gms.plus.apps.AppsUtilFragment"

    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v4, "prefs_account_name"

    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    aget-object v0, v3, v1

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_3

    aget-object v4, v3, v0

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    aget-object v0, v3, v1

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const-string v1, "com.google.android.gms.plus.apps.AppsUtilFragment"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lo;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "prefs_account_name"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-object p1
.end method

.method private static d(I)Z
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private e(I)V
    .locals 2

    iput p1, p0, Lfob;->b:I

    iget-object v0, p0, Lfob;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoc;

    invoke-interface {v0}, Lfoc;->c()V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method final J()Lfnz;
    .locals 1

    iget-object v0, p0, Lfob;->Y:Lfnz;

    return-object v0
.end method

.method public final M_()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    iget-object v0, p0, Lfob;->i:Lfpd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfob;->i:Lfpd;

    invoke-virtual {v0, p0}, Lfpd;->b(Lfpf;)V

    :cond_0
    invoke-virtual {p0}, Lfob;->s()Lak;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lak;->a(I)V

    return-void
.end method

.method public final N_()V
    .locals 0

    return-void
.end method

.method public final synthetic a(ILandroid/os/Bundle;)Lcb;
    .locals 5

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    new-instance v1, Lfpg;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, p0, Lfob;->a:Landroid/accounts/Account;

    sget-object v0, Lfsr;->E:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lfob;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, v4}, Lfpg;-><init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;)V

    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Lcb;Ljava/lang/Object;)V
    .locals 6

    const/4 v1, 0x0

    const/16 v5, 0xa

    check-cast p2, Lfxi;

    iget v0, p1, Lcb;->m:I

    if-ne v0, v5, :cond_1

    check-cast p1, Lfpg;

    invoke-virtual {p1}, Lfpg;->b()Lbbo;

    move-result-object v0

    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbbo;->b()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    invoke-direct {p0}, Lfob;->L()V

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lfob;->e(I)V

    invoke-virtual {p0}, Lfob;->s()Lak;

    move-result-object v1

    invoke-virtual {v1, v5}, Lak;->a(I)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbbo;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lfxi;->a()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_4

    iget-object v3, p0, Lfob;->c:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Lfxi;->b(I)Lfxh;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lfxh;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lfpg;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfob;->h:Ljava/lang/String;

    iget-object v0, p0, Lfob;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lfob;->e(I)V

    invoke-virtual {p0}, Lfob;->s()Lak;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, p0}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    goto :goto_0

    :cond_5
    invoke-direct {p0, v1}, Lfob;->e(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfob;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lfob;->e:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, v2}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method final a(Lfod;Lfxh;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lfob;->i:Lfpd;

    invoke-virtual {v0, p2, p3}, Lfpd;->a(Lfxh;Ljava/lang/String;)V

    iget-object v0, p0, Lfob;->Z:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lfob;->Z:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lfxh;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iget-object v0, p0, Lfob;->Y:Lfnz;

    invoke-virtual {v0, p1, p2}, Lfnz;->a(Lfxh;Landroid/graphics/drawable/Drawable;)Lfoa;

    iget-object v0, p0, Lfob;->Z:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfod;

    invoke-interface {v0}, Lfod;->a()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lfob;->Z:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Lfob;->c(Ljava/lang/String;)Ljava/lang/String;

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lfob;->a:Landroid/accounts/Account;

    invoke-direct {p0}, Lfob;->K()V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "account_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v0}, Lfob;->c(Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    new-instance v2, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v2, v0, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lfob;->a:Landroid/accounts/Account;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "calling_package_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfob;->e:Ljava/lang/String;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lfob;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lfob;->g:Ljava/util/ArrayList;

    const-string v2, "disconnected_apps"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iget-object v0, p0, Lfob;->a:Landroid/accounts/Account;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    const-string v0, "has_error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-static {}, Lfot;->a()Lfot;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, v1}, Lfot;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p0, Lfob;->a:Landroid/accounts/Account;

    if-nez v0, :cond_7

    const/4 v0, 0x2

    :goto_1
    invoke-direct {p0, v0}, Lfob;->e(I)V

    :cond_4
    :goto_2
    return-void

    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lfob;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    :cond_7
    const/4 v0, 0x3

    goto :goto_1
.end method

.method final b()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lfob;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method final b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfob;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lfob;->e:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, v2}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lfob;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final c(I)V
    .locals 0

    iput p1, p0, Lfob;->f:I

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lfpd;->a(Landroid/content/Context;)Lfpd;

    move-result-object v1

    iput-object v1, p0, Lfob;->i:Lfpd;

    iget-object v1, p0, Lfob;->i:Lfpd;

    invoke-virtual {v1, p0}, Lfpd;->a(Lfpf;)V

    invoke-static {v0}, Lfnz;->a(Landroid/content/Context;)Lfnz;

    move-result-object v0

    iput-object v0, p0, Lfob;->Y:Lfnz;

    iget v0, p0, Lfob;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lfob;->s()Lak;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p0}, Lak;->a(ILal;)Lcb;

    :cond_0
    const/4 v0, -0x2

    iput v0, p0, Lfob;->f:I

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "has_error"

    iget v1, p0, Lfob;->b:I

    invoke-static {v1}, Lfob;->d(I)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "disconnected_apps"

    iget-object v1, p0, Lfob;->g:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public final w()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget v0, p0, Lfob;->b:I

    invoke-static {v0}, Lfob;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lfob;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lfob;->K()V

    :cond_0
    const/4 v0, -0x2

    iput v0, p0, Lfob;->f:I

    return-void
.end method
