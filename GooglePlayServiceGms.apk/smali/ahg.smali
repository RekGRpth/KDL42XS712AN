.class public final Lahg;
.super Lenu;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)V
    .locals 1

    iput-object p1, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-direct {p0, p1}, Lenu;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lahg;->c:Z

    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lenv;

    iget-object v0, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->b(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    iget-wide v2, p1, Lenv;->c:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->c(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->c(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    if-nez p1, :cond_1

    const-wide/16 v0, 0x0

    :goto_0
    invoke-static {v3, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-wide v0, p1, Lenv;->d:J

    goto :goto_0
.end method

.method protected final onPreExecute()V
    .locals 2

    iget-object v0, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->b(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v1}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->a(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->c(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->c(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lahg;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v1}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->a(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
