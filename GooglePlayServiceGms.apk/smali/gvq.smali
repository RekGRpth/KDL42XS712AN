.class public final Lgvq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)V
    .locals 0

    iput-object p1, p0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgvq;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)V

    return-void
.end method


# virtual methods
.method public final a()Lgvq;
    .locals 2

    iget-object v0, p0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    const v1, 0x7f0b0151    # com.google.android.gms.R.string.wallet_card_holder_name

    iput v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lgvq;
    .locals 2

    iget-object v0, p0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-static {p1}, Lhgq;->b(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    return-object p0
.end method

.method public final a(Ljava/util/ArrayList;)Lgvq;
    .locals 1

    iget-object v0, p0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lgvq;
    .locals 2

    iget-object v0, p0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-static {p1}, Lgty;->a(Ljava/util/List;)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    return-object p0
.end method

.method public final a(Z)Lgvq;
    .locals 1

    iget-object v0, p0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    return-object p0
.end method
