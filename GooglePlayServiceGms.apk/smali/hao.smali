.class public final Lhao;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lgwv;
.implements Lgyq;


# instance fields
.field Y:Lgwr;

.field Z:I

.field a:Landroid/view/View;

.field protected final aa:Lhcb;

.field private ab:Lgwn;

.field private ac:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private ad:Z

.field private ae:Landroid/accounts/Account;

.field private af:Lioj;

.field private ag:Ljau;

.field private ah:Lhgu;

.field private ai:Z

.field b:Landroid/widget/ProgressBar;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/ImageView;

.field e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field f:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

.field g:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field h:Lhca;

.field i:Lgwr;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhao;->ad:Z

    const/4 v0, -0x1

    iput v0, p0, Lhao;->Z:I

    new-instance v0, Lhap;

    invoke-direct {v0, p0}, Lhap;-><init>(Lhao;)V

    iput-object v0, p0, Lhao;->aa:Lhcb;

    return-void
.end method

.method private J()V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhao;->a(Z)V

    iget-object v0, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljao;

    invoke-direct {v1}, Ljao;-><init>()V

    iget-object v2, p0, Lhao;->af:Lioj;

    iget-object v2, v2, Lioj;->a:Ljava/lang/String;

    iput-object v2, v1, Ljao;->a:Ljava/lang/String;

    iget-object v2, p0, Lhao;->h:Lhca;

    invoke-interface {v2, v0, v1}, Lhca;->a(Ljava/lang/String;Ljao;)V

    return-void
.end method

.method static synthetic a(Lhao;Ljav;)Lcom/google/android/gms/wallet/FullWallet;
    .locals 8

    iget-object v0, p0, Lhao;->af:Lioj;

    iget-object v0, v0, Lioj;->e:Lipv;

    invoke-static {v0}, Lhfx;->a(Lipv;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    iget-object v0, p0, Lhao;->af:Lioj;

    iget-object v0, v0, Lioj;->e:Lipv;

    iget-object v0, v0, Lipv;->a:Lixo;

    iget-object v2, v0, Lixo;->t:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, v0, Lixo;->t:Ljava/lang/String;

    :goto_0
    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lgrc;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/wallet/ProxyCard;

    iget-object v4, p1, Ljav;->e:Ljava/lang/String;

    iget-object v5, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget v6, p1, Ljav;->c:I

    iget v7, p1, Ljav;->d:I

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/wallet/ProxyCard;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v2, v3}, Lgrc;->a(Lcom/google/android/gms/wallet/ProxyCard;)Lgrc;

    move-result-object v2

    iget-object v3, p0, Lhao;->ag:Ljau;

    iget-object v3, v3, Ljau;->f:Ljava/lang/String;

    invoke-static {v3}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lgrc;->a(Ljava/lang/String;)Lgrc;

    move-result-object v2

    iget-object v3, p0, Lhao;->ag:Ljau;

    iget-object v3, v3, Ljau;->g:Ljava/lang/String;

    invoke-static {v3}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lgrc;->b(Ljava/lang/String;)Lgrc;

    move-result-object v2

    invoke-virtual {v2, v1}, Lgrc;->a(Lcom/google/android/gms/wallet/Address;)Lgrc;

    move-result-object v1

    iget-object v2, p1, Ljav;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lgrc;->a([Ljava/lang/String;)Lgrc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lgrc;->c(Ljava/lang/String;)Lgrc;

    move-result-object v0

    iget-object v1, p1, Ljav;->h:Lipv;

    if-eqz v1, :cond_0

    iget-object v1, p1, Ljav;->h:Lipv;

    invoke-static {v1}, Lhfx;->a(Lipv;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgrc;->b(Lcom/google/android/gms/wallet/Address;)Lgrc;

    :cond_0
    iget-object v0, v0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;Ljau;ZLjava/lang/String;)Lhao;
    .locals 3

    new-instance v0, Lhao;

    invoke-direct {v0}, Lhao;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "instrument"

    invoke-static {v1, v2, p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    const-string v2, "protoRequest"

    invoke-static {v1, v2, p3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    const-string v2, "localMode"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "sessionId"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhao;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lhao;)Ljau;
    .locals 1

    iget-object v0, p0, Lhao;->ag:Ljau;

    return-object v0
.end method

.method private a()V
    .locals 2

    iget v0, p0, Lhao;->Z:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lhao;->h:Lhca;

    iget-object v1, p0, Lhao;->aa:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lhao;->Z:I

    :cond_0
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0, p1, p2}, Lo;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->finish()V

    return-void
.end method

.method static synthetic a(Lhao;I)V
    .locals 0

    invoke-direct {p0, p1}, Lhao;->c(I)V

    return-void
.end method

.method static synthetic a(Lhao;ILandroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lhao;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lhao;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lhao;->Y:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhao;->Y:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {p1, p2, p3}, Lgwr;->a(Ljava/lang/String;Ljava/lang/String;I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lhao;->Y:Lgwr;

    iget-object v0, p0, Lhao;->Y:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lhao;->Y:Lgwr;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "AuthenticateInstrumentF.OwErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhao;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lhao;->g:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-object v0, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    iput-boolean p1, p0, Lhao;->ai:Z

    return-void

    :cond_0
    iget-object v0, p0, Lhao;->b:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lhao;->ab:Lgwn;

    invoke-virtual {v0}, Lgwn;->R_()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhao;->i()Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lhao;->J()V

    goto :goto_0
.end method

.method static synthetic b(Lhao;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhao;->a(Z)V

    return-void
.end method

.method private c(I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lgrc;

    move-result-object v1

    iget-object v2, p0, Lhao;->ag:Ljau;

    iget-object v2, v2, Ljau;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lgrc;->a(Ljava/lang/String;)Lgrc;

    move-result-object v1

    iget-object v2, p0, Lhao;->ag:Ljau;

    iget-object v2, v2, Ljau;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lgrc;->b(Ljava/lang/String;)Lgrc;

    move-result-object v1

    iget-object v1, v1, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    const-string v2, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lhao;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic c(Lhao;)Z
    .locals 1

    iget-boolean v0, p0, Lhao;->ad:Z

    return v0
.end method

.method static synthetic d(Lhao;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lhao;->ae:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic e(Lhao;)V
    .locals 3

    iget-object v0, p0, Lhao;->i:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhao;->i:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lhao;->i:Lgwr;

    iget-object v0, p0, Lhao;->i:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lhao;->i:Lgwr;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "AuthenticateInstrumentF.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    iget-object v0, p0, Lhao;->ab:Lgwn;

    invoke-virtual {v0}, Lgwn;->R_()Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    iget-object v0, p0, Lhao;->ab:Lgwn;

    invoke-virtual {v0}, Lgwn;->S_()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const v0, 0x7f04012d    # com.google.android.gms.R.layout.wallet_fragment_authenticate_instrument

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a031a    # com.google.android.gms.R.id.authenticate_instrument_content

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhao;->a:Landroid/view/View;

    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lhao;->b:Landroid/widget/ProgressBar;

    const v0, 0x7f0a031c    # com.google.android.gms.R.id.card_description

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhao;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lhao;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lhao;->af:Lioj;

    iget-object v2, v2, Lioj;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a031b    # com.google.android.gms.R.id.card_image

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lhao;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lhao;->d:Landroid/widget/ImageView;

    iget-object v2, p0, Lhao;->af:Lioj;

    iget-object v3, p0, Lhao;->ah:Lhgu;

    invoke-static {v0, v2, v3}, Lgth;->a(Landroid/widget/ImageView;Lioj;Lhgu;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhao;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    const v0, 0x7f0a031d    # com.google.android.gms.R.id.cvc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, p0, Lhao;->af:Lioj;

    iget v2, v0, Lioj;->c:I

    new-instance v0, Lgwn;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v4, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v3, v4, v2}, Lgwn;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;I)V

    iput-object v0, p0, Lhao;->ab:Lgwn;

    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-static {v2}, Lgth;->d(I)I

    move-result v3

    invoke-direct {v0, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iget-object v3, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-array v4, v5, [Landroid/text/InputFilter;

    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v3, p0, Lhao;->ab:Lgwn;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v3, p0, Lhao;->ab:Lgwn;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v0, 0x7f0a031e    # com.google.android.gms.R.id.cvc_hint

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    iput-object v0, p0, Lhao;->f:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    iget-object v0, p0, Lhao;->f:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a(I)V

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lhao;->g:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lhao;->g:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lhao;->g:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-boolean v0, p0, Lhao;->ai:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v5}, Lhao;->a(Z)V

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, Lhao;->d:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, v1}, Lhao;->a(Z)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lhao;->J()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lhao;->a(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2}, Lhao;->c(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lhao;->ac:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lhao;->ae:Landroid/accounts/Account;

    const-string v0, "instrument"

    const-class v2, Lioj;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, p0, Lhao;->af:Lioj;

    const-string v0, "protoRequest"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    const-string v0, "protoRequest"

    const-class v2, Ljau;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljau;

    iput-object v0, p0, Lhao;->ag:Ljau;

    const-string v0, "localMode"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lhao;->ad:Z

    iget-object v0, p0, Lhao;->h:Lhca;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhao;->ad:Z

    if-eqz v0, :cond_1

    const-string v0, "sessionId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lhbt;

    iget-object v2, p0, Lhao;->ac:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v3, p0, Lhao;->ae:Landroid/accounts/Account;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v4}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v0}, Lhbt;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lhao;->h:Lhca;

    :goto_0
    iget-object v0, p0, Lhao;->h:Lhca;

    invoke-interface {v0}, Lhca;->a()V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lhbu;

    const/4 v1, 0x2

    iget-object v2, p0, Lhao;->ac:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v3, p0, Lhao;->ae:Landroid/accounts/Account;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v4}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lhbu;-><init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput-object v0, p0, Lhao;->h:Lhca;

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lhao;->q()V

    new-instance v0, Lhgu;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v1}, Lhgu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhao;->ah:Lhgu;

    iget-object v0, p0, Lhao;->ah:Lhgu;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Lhgs;->a(Landroid/content/Context;)Lhgs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhgu;->a(Lhgs;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhao;->ac:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhao;->ac:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "authenticate_instrument"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lhao;->Z:I

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lhao;->a()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lhao;->Z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lhao;->ab:Lgwn;

    invoke-virtual {v0}, Lgwn;->S_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lhao;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a036c
        :pswitch_0    # com.google.android.gms.R.id.continue_btn
    .end packed-switch
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-ne p1, v1, :cond_0

    if-nez p3, :cond_1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :pswitch_0
    invoke-direct {p0}, Lhao;->b()V

    goto :goto_1

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lhao;->b()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x42
        :pswitch_2
    .end packed-switch
.end method

.method public final w()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "AuthenticateInstrumentF.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lhao;->i:Lgwr;

    iget-object v0, p0, Lhao;->i:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhao;->i:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "AuthenticateInstrumentF.OwErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lhao;->Y:Lgwr;

    iget-object v0, p0, Lhao;->Y:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhao;->Y:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    iget-object v0, p0, Lhao;->h:Lhca;

    iget-object v1, p0, Lhao;->aa:Lhcb;

    iget v2, p0, Lhao;->Z:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    const/4 v0, -0x1

    iput v0, p0, Lhao;->Z:I

    return-void
.end method

.method public final x()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->x()V

    invoke-direct {p0}, Lhao;->a()V

    return-void
.end method
