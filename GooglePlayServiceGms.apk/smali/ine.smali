.class public final Line;
.super Lizk;
.source "SourceFile"


# instance fields
.field private A:Z

.field private B:Z

.field private C:Ljava/lang/String;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:I

.field private I:Z

.field private J:J

.field private K:Z

.field private L:I

.field private M:Z

.field private N:I

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/List;

.field public g:Ljava/lang/String;

.field public h:I

.field public i:J

.field public j:I

.field public k:J

.field public l:Z

.field public m:Lizf;

.field public n:Z

.field public o:I

.field public p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Line;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Line;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Line;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Line;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Line;->e:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Line;->f:Ljava/util/List;

    iput-boolean v1, p0, Line;->w:Z

    const-string v0, ""

    iput-object v0, p0, Line;->g:Ljava/lang/String;

    iput v1, p0, Line;->z:I

    iput v1, p0, Line;->h:I

    const-string v0, ""

    iput-object v0, p0, Line;->C:Ljava/lang/String;

    iput-wide v2, p0, Line;->i:J

    iput v1, p0, Line;->j:I

    iput-wide v2, p0, Line;->k:J

    iput v1, p0, Line;->H:I

    iput-wide v2, p0, Line;->J:J

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Line;->m:Lizf;

    iput v1, p0, Line;->o:I

    iput v1, p0, Line;->L:I

    iput-boolean v1, p0, Line;->p:Z

    const/4 v0, -0x1

    iput v0, p0, Line;->N:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Line;->N:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Line;->b()I

    :cond_0
    iget v0, p0, Line;->N:I

    return v0
.end method

.method public final a(I)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->A:Z

    iput p1, p0, Line;->h:I

    return-object p0
.end method

.method public final a(J)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->D:Z

    iput-wide p1, p0, Line;->i:J

    return-object p0
.end method

.method public final a(Linc;)Line;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Line;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Line;->f:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Line;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->q:Z

    iput-object p1, p0, Line;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Line;->a(Ljava/lang/String;)Line;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Line;->r:Z

    iput-object v0, p0, Line;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Line;->b(Ljava/lang/String;)Line;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Line;->c(Ljava/lang/String;)Line;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Line;->d(Ljava/lang/String;)Line;

    goto :goto_0

    :sswitch_6
    new-instance v0, Linc;

    invoke-direct {v0}, Linc;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Line;->a(Linc;)Line;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v2, p0, Line;->v:Z

    iput-boolean v0, p0, Line;->w:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Line;->e(Ljava/lang/String;)Line;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v2, p0, Line;->y:Z

    iput v0, p0, Line;->z:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Line;->a(I)Line;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Line;->f(Ljava/lang/String;)Line;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Line;->a(J)Line;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Line;->b(I)Line;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Line;->b(J)Line;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Line;->c(I)Line;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Line;->c(J)Line;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    iput-boolean v2, p0, Line;->l:Z

    iput-object v0, p0, Line;->m:Lizf;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Line;->d(I)Line;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Line;->e(I)Line;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v2, p0, Line;->M:Z

    iput-boolean v0, p0, Line;->p:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x6a -> :sswitch_b
        0x80 -> :sswitch_c
        0x88 -> :sswitch_d
        0x90 -> :sswitch_e
        0x98 -> :sswitch_f
        0xa0 -> :sswitch_10
        0xaa -> :sswitch_11
        0xb0 -> :sswitch_12
        0xb8 -> :sswitch_13
        0xc0 -> :sswitch_14
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Line;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Line;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Line;->r:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget-object v1, p0, Line;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Line;->s:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    iget-object v1, p0, Line;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Line;->t:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Line;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Line;->u:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Line;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p0, Line;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linc;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILizk;)V

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Line;->v:Z

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    iget-boolean v1, p0, Line;->w:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_6
    iget-boolean v0, p0, Line;->x:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Line;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_7
    iget-boolean v0, p0, Line;->y:Z

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget v1, p0, Line;->z:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_8
    iget-boolean v0, p0, Line;->A:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    iget v1, p0, Line;->h:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_9
    iget-boolean v0, p0, Line;->B:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xd

    iget-object v1, p0, Line;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_a
    iget-boolean v0, p0, Line;->D:Z

    if-eqz v0, :cond_b

    const/16 v0, 0x10

    iget-wide v1, p0, Line;->i:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_b
    iget-boolean v0, p0, Line;->E:Z

    if-eqz v0, :cond_c

    const/16 v0, 0x11

    iget v1, p0, Line;->j:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_c
    iget-boolean v0, p0, Line;->F:Z

    if-eqz v0, :cond_d

    const/16 v0, 0x12

    iget-wide v1, p0, Line;->k:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_d
    iget-boolean v0, p0, Line;->G:Z

    if-eqz v0, :cond_e

    const/16 v0, 0x13

    iget v1, p0, Line;->H:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_e
    iget-boolean v0, p0, Line;->I:Z

    if-eqz v0, :cond_f

    const/16 v0, 0x14

    iget-wide v1, p0, Line;->J:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_f
    iget-boolean v0, p0, Line;->l:Z

    if-eqz v0, :cond_10

    const/16 v0, 0x15

    iget-object v1, p0, Line;->m:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_10
    iget-boolean v0, p0, Line;->n:Z

    if-eqz v0, :cond_11

    const/16 v0, 0x16

    iget v1, p0, Line;->o:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_11
    iget-boolean v0, p0, Line;->K:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x17

    iget v1, p0, Line;->L:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_12
    iget-boolean v0, p0, Line;->M:Z

    if-eqz v0, :cond_13

    const/16 v0, 0x18

    iget-boolean v1, p0, Line;->p:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_13
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Line;->q:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Line;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Line;->r:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Line;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Line;->s:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    iget-object v2, p0, Line;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Line;->t:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Line;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Line;->u:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Line;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Line;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linc;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Line;->v:Z

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    iget-boolean v2, p0, Line;->w:Z

    invoke-static {v0}, Lizh;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    :cond_6
    iget-boolean v0, p0, Line;->x:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget-object v2, p0, Line;->g:Ljava/lang/String;

    invoke-static {v0, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_7
    iget-boolean v0, p0, Line;->y:Z

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget v2, p0, Line;->z:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_8
    iget-boolean v0, p0, Line;->A:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    iget v2, p0, Line;->h:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_9
    iget-boolean v0, p0, Line;->B:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xd

    iget-object v2, p0, Line;->C:Ljava/lang/String;

    invoke-static {v0, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_a
    iget-boolean v0, p0, Line;->D:Z

    if-eqz v0, :cond_b

    const/16 v0, 0x10

    iget-wide v2, p0, Line;->i:J

    invoke-static {v0, v2, v3}, Lizh;->c(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_b
    iget-boolean v0, p0, Line;->E:Z

    if-eqz v0, :cond_c

    const/16 v0, 0x11

    iget v2, p0, Line;->j:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_c
    iget-boolean v0, p0, Line;->F:Z

    if-eqz v0, :cond_d

    const/16 v0, 0x12

    iget-wide v2, p0, Line;->k:J

    invoke-static {v0, v2, v3}, Lizh;->c(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_d
    iget-boolean v0, p0, Line;->G:Z

    if-eqz v0, :cond_e

    const/16 v0, 0x13

    iget v2, p0, Line;->H:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_e
    iget-boolean v0, p0, Line;->I:Z

    if-eqz v0, :cond_f

    const/16 v0, 0x14

    iget-wide v2, p0, Line;->J:J

    invoke-static {v0, v2, v3}, Lizh;->c(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_f
    iget-boolean v0, p0, Line;->l:Z

    if-eqz v0, :cond_10

    const/16 v0, 0x15

    iget-object v2, p0, Line;->m:Lizf;

    invoke-static {v0, v2}, Lizh;->b(ILizf;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_10
    iget-boolean v0, p0, Line;->n:Z

    if-eqz v0, :cond_11

    const/16 v0, 0x16

    iget v2, p0, Line;->o:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_11
    iget-boolean v0, p0, Line;->K:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x17

    iget v2, p0, Line;->L:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_12
    iget-boolean v0, p0, Line;->M:Z

    if-eqz v0, :cond_13

    const/16 v0, 0x18

    iget-boolean v2, p0, Line;->p:Z

    invoke-static {v0}, Lizh;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    :cond_13
    iput v1, p0, Line;->N:I

    return v1
.end method

.method public final b(I)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->E:Z

    iput p1, p0, Line;->j:I

    return-object p0
.end method

.method public final b(J)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->F:Z

    iput-wide p1, p0, Line;->k:J

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->s:Z

    iput-object p1, p0, Line;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final c(I)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->G:Z

    iput p1, p0, Line;->H:I

    return-object p0
.end method

.method public final c(J)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->I:Z

    iput-wide p1, p0, Line;->J:J

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->t:Z

    iput-object p1, p0, Line;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final d(I)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->n:Z

    iput p1, p0, Line;->o:I

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->u:Z

    iput-object p1, p0, Line;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final e(I)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->K:Z

    iput p1, p0, Line;->L:I

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->x:Z

    iput-object p1, p0, Line;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Line;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Line;->B:Z

    iput-object p1, p0, Line;->C:Ljava/lang/String;

    return-object p0
.end method
