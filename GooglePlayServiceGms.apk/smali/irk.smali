.class final enum Lirk;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lirk;

.field public static final enum b:Lirk;

.field public static final enum c:Lirk;

.field public static final enum d:Lirk;

.field private static final synthetic e:[Lirk;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lirk;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lirk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->a:Lirk;

    new-instance v0, Lirk;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, Lirk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->b:Lirk;

    new-instance v0, Lirk;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lirk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->c:Lirk;

    new-instance v0, Lirk;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lirk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->d:Lirk;

    const/4 v0, 0x4

    new-array v0, v0, [Lirk;

    sget-object v1, Lirk;->a:Lirk;

    aput-object v1, v0, v2

    sget-object v1, Lirk;->b:Lirk;

    aput-object v1, v0, v3

    sget-object v1, Lirk;->c:Lirk;

    aput-object v1, v0, v4

    sget-object v1, Lirk;->d:Lirk;

    aput-object v1, v0, v5

    sput-object v0, Lirk;->e:[Lirk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lirk;
    .locals 1

    const-class v0, Lirk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lirk;

    return-object v0
.end method

.method public static values()[Lirk;
    .locals 1

    sget-object v0, Lirk;->e:[Lirk;

    invoke-virtual {v0}, [Lirk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lirk;

    return-object v0
.end method
