.class public final Lecj;
.super Lebd;
.source "SourceFile"

# interfaces
.implements Leca;


# instance fields
.field private final a:Ldwr;

.field private final b:Ljava/lang/String;

.field private final c:Leci;


# direct methods
.method public constructor <init>(Ldwr;)V
    .locals 1

    invoke-direct {p0}, Lebd;-><init>()V

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lecj;->a:Ldwr;

    invoke-virtual {p1}, Ldwr;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lecj;->b:Ljava/lang/String;

    new-instance v0, Leci;

    invoke-direct {v0, p1}, Leci;-><init>(Ldwr;)V

    iput-object v0, p0, Lecj;->c:Leci;

    return-void
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->d()Lcom/google/android/gms/games/Game;

    move-result-object v2

    iget-object v3, p0, Lecj;->a:Ldwr;

    iget-object v4, p0, Lecj;->b:Ljava/lang/String;

    invoke-static {v4}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-static {v2}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->f()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "requests"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-static {v0, v4}, Ldab;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v3, v2, v0}, Leee;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    iget-object v0, p0, Lecj;->a:Ldwr;

    const/16 v1, 0x4e21

    invoke-virtual {v0, v1}, Ldwr;->setResult(I)V

    iget-object v0, p0, Lecj;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->finish()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->b()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-virtual {p0, v0}, Lecj;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lecj;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessReqInboxHelper"

    const-string v1, "onRequestClusterSeeMoreClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->o:Ldlf;

    invoke-interface {v1, v0, p1, p2}, Ldlf;->a(Lbdu;Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lecj;->a:Ldwr;

    const/16 v2, 0x384

    invoke-virtual {v1, v0, v2}, Ldwr;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 5

    iget-object v0, p0, Lecj;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessReqInboxHelper"

    const-string v1, "onRequestDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->d()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcte;->o:Ldlf;

    iget-object v3, p0, Lecj;->a:Ldwr;

    invoke-virtual {v3}, Ldwr;->o()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v0, v1, v3, v4}, Ldlf;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbeh;

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 3

    iget-object v0, p0, Lecj;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "HeadlessReqInboxHelper"

    const-string v1, "switchAccountForRequest: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->d()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lecj;->b:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcte;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lecj;->b(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 3

    iget-object v0, p0, Lecj;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "HeadlessReqInboxHelper"

    const-string v1, "onRequestClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lbha;->a([Lbgz;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->d()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcte;->a(Lbdu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lecj;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lecj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lecj;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lebz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Lebz;

    move-result-object v0

    iget-object v1, p0, Lecj;->a:Ldwr;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lecj;->b(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final b_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lecj;->c:Leci;

    invoke-virtual {v0, p1}, Leci;->a(Lcom/google/android/gms/games/Game;)V

    return-void
.end method
