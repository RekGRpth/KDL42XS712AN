.class public final Ledc;
.super Lecz;
.source "SourceFile"

# interfaces
.implements Lduy;


# instance fields
.field private b:Lduk;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lecz;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public final a(Lduk;)V
    .locals 6

    iput-object p1, p0, Ledc;->b:Lduk;

    iget-object v4, p0, Ledc;->b:Lduk;

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lduk;->c()V

    :try_start_0
    iget-object v0, v4, Lduk;->f:Ldam;

    new-instance v1, Ldvb;

    invoke-direct {v1, v4, p0}, Ldvb;-><init>(Lduk;Lduy;)V

    iget-object v3, v4, Lduk;->d:Ljava/lang/String;

    iget-object v4, v4, Lduk;->e:[Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Ldam;->a(Ldaj;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0xc

    return v0
.end method

.method public final b_(I)V
    .locals 3

    invoke-virtual {p0}, Ledc;->J()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "RecordSignInFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error found when recording sign in attempt: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Ledc;->d(I)I

    move-result v0

    invoke-virtual {p0, v0}, Ledc;->e(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ledc;->b:Lduk;

    invoke-virtual {v0}, Lduk;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ledc;->b:Lduk;

    iget-object v1, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lduk;->c()V

    :try_start_0
    iget-object v0, v0, Lduk;->f:Ldam;

    invoke-interface {v0, v1, v2}, Ldam;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ledc;->c(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
