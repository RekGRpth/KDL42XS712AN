.class public final Lezf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Leyk;

.field c:Leyk;

.field d:Z

.field e:F

.field f:[F

.field g:J

.field h:[F

.field i:[F

.field j:I

.field public k:Leze;

.field public l:[F

.field public m:F

.field public n:Lezl;

.field o:F

.field public p:[D

.field private q:Landroid/hardware/SensorManager;

.field private r:Z

.field private final s:Landroid/hardware/SensorEventListener;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v5, 0x0

    const/16 v4, 0x10

    const/4 v3, 0x3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lezf;->a:Z

    iput-object v5, p0, Lezf;->q:Landroid/hardware/SensorManager;

    new-instance v0, Leyk;

    invoke-direct {v0}, Leyk;-><init>()V

    iput-object v0, p0, Lezf;->b:Leyk;

    new-instance v0, Leyk;

    invoke-direct {v0}, Leyk;-><init>()V

    iput-object v0, p0, Lezf;->c:Leyk;

    iput-boolean v2, p0, Lezf;->d:Z

    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lezf;->e:F

    new-array v0, v3, [F

    iput-object v0, p0, Lezf;->f:[F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lezf;->g:J

    new-array v0, v3, [F

    iput-object v0, p0, Lezf;->h:[F

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lezf;->i:[F

    iput v2, p0, Lezf;->j:I

    new-instance v0, Leze;

    invoke-direct {v0}, Leze;-><init>()V

    iput-object v0, p0, Lezf;->k:Leze;

    new-array v0, v4, [F

    iput-object v0, p0, Lezf;->l:[F

    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lezf;->m:F

    iput-object v5, p0, Lezf;->n:Lezl;

    const/4 v0, 0x0

    iput v0, p0, Lezf;->o:F

    iput-boolean v2, p0, Lezf;->r:Z

    new-array v0, v4, [D

    iput-object v0, p0, Lezf;->p:[D

    new-instance v0, Lezg;

    invoke-direct {v0, p0}, Lezg;-><init>(Lezf;)V

    iput-object v0, p0, Lezf;->s:Landroid/hardware/SensorEventListener;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lezf;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-boolean v0, p0, Lezf;->r:Z

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iput-boolean v4, p0, Lezf;->r:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {v5, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    int-to-float v0, v0

    iput v0, p0, Lezf;->m:F

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Model is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Nexus 7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lezf;->m:F

    :cond_1
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lezf;->q:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lezf;->q:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lezf;->s:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lezf;->q:Landroid/hardware/SensorManager;

    invoke-virtual {v2, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    iget-object v0, p0, Lezf;->q:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lezf;->s:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lezf;->q:Landroid/hardware/SensorManager;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    iget-object v0, p0, Lezf;->q:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lezf;->s:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lezf;->q:Landroid/hardware/SensorManager;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    iput-boolean v5, p0, Lezf;->d:Z

    iget-object v0, p0, Lezf;->i:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    iget-object v0, p0, Lezf;->k:Leze;

    invoke-virtual {v0}, Leze;->a()V

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lezf;->r:Z

    iget-object v0, p0, Lezf;->q:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lezf;->q:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lezf;->s:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    :cond_0
    return-void
.end method
