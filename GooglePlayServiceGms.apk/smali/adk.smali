.class public final Ladk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsj;
.implements Lsk;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Landroid/content/SharedPreferences;


# instance fields
.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Landroid/content/Context;

.field private final e:Lsf;

.field private final f:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ladk;->a:Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "ads_prefs_state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/app/GmsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Ladk;->b:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ladk;->f:Ljava/util/Set;

    iput-object p1, p0, Ladk;->d:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lsf;

    move-result-object v0

    iput-object v0, p0, Ladk;->e:Lsf;

    new-instance v0, Ladl;

    invoke-direct {v0, p0}, Ladl;-><init>(Ladk;)V

    iput-object v0, p0, Ladk;->c:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(I)V
    .locals 2

    iget-object v0, p0, Ladk;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladm;

    invoke-interface {v0, p1}, Ladm;->a(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static b(Z)V
    .locals 2

    sget-object v0, Ladk;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "stop_scheduled"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public static c()Z
    .locals 3

    sget-object v0, Ladk;->b:Landroid/content/SharedPreferences;

    const-string v1, "ads_opt_in_in_progress"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 3

    sget-object v0, Ladk;->b:Landroid/content/SharedPreferences;

    const-string v1, "ads_opt_out_in_progress"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic f()Ljava/lang/Object;
    .locals 1

    sget-object v0, Ladk;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private g()V
    .locals 2

    const/4 v0, 0x0

    invoke-static {v0}, Ladk;->b(Z)V

    :try_start_0
    iget-object v0, p0, Ladk;->d:Landroid/content/Context;

    iget-object v1, p0, Ladk;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Ladk;->d:Landroid/content/Context;

    iget-object v2, p0, Ladk;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public final a(Ladm;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Ladk;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Ljava/lang/Boolean;

    const-string v0, "AdsPrefsState"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AdsPrefsState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successful response of server value of personalized ads. (response="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Ladk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ladk;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "ads_opt_in_in_progress"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    :goto_0
    sget-object v0, Ladk;->b:Landroid/content/SharedPreferences;

    const-string v2, "stop_scheduled"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Ladk;->g()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    :goto_1
    invoke-direct {p0, v0}, Ladk;->a(I)V

    return-void

    :cond_2
    :try_start_1
    sget-object v0, Ladk;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "ads_opt_out_in_progress"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    const/4 v0, 0x4

    goto :goto_1
.end method

.method public final a(Lsp;)V
    .locals 3

    const/4 v2, 0x2

    const-string v0, "AdsPrefsState"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AdsPrefsState"

    const-string v1, "Failed to retrieve server value of personalized ads."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0, v2}, Ladk;->a(I)V

    return-void
.end method

.method public final a(Z)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ladk;->a(I)V

    sget-object v1, Ladk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v2, Ladk;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ads_opt_in_in_progress"

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    sget-object v2, Ladk;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ads_opt_out_in_progress"

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v1, Ladj;

    iget-object v2, p0, Ladk;->d:Landroid/content/Context;

    if-eqz p1, :cond_1

    const-string v0, "mobile_optin"

    :goto_0
    invoke-direct {v1, v2, v0, p0, p0}, Ladj;-><init>(Landroid/content/Context;Ljava/lang/String;Lsk;Lsj;)V

    iget-object v0, p0, Ladk;->e:Lsf;

    invoke-virtual {v0, v1}, Lsf;->a(Lsc;)Lsc;

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    const-string v0, "mobile_optout"

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    sget-object v1, Ladk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ladk;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ladk;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ladk;->b(Z)V

    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    invoke-direct {p0}, Ladk;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ladm;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Ladk;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Ladk;->d:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Ladk;->a(I)V

    new-instance v0, Ladj;

    iget-object v1, p0, Ladk;->d:Landroid/content/Context;

    const-string v3, "mobile_view"

    invoke-direct {v0, v1, v3, p0, p0}, Ladj;-><init>(Landroid/content/Context;Ljava/lang/String;Lsk;Lsj;)V

    invoke-virtual {v0, v2}, Ladj;->a(Z)Lsc;

    iget-object v1, p0, Ladk;->e:Lsf;

    invoke-virtual {v1, v0}, Lsf;->a(Lsc;)Lsc;

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Ladk;->a(I)V

    goto :goto_1
.end method
