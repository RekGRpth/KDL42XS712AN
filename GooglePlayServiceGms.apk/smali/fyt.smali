.class public final Lfyt;
.super Lfyw;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private e:Lfyu;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfyw;-><init>()V

    return-void
.end method

.method public static a(ZLcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;Ljava/lang/String;)Lfyt;
    .locals 3

    new-instance v0, Lfyt;

    invoke-direct {v0}, Lfyt;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "is_setup_wizard_theme"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "upgrade_account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "error_id"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lfyt;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lfyw;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lfyt;->d:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lfyt;->d:Landroid/widget/Button;

    const v2, 0x7f0b03e2    # com.google.android.gms.R.string.plus_bad_name_decline_button_label

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "error_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfyt;->f:Ljava/lang/String;

    const-string v1, "invalidNameHardFail"

    iget-object v2, p0, Lfyt;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfyt;->c:Landroid/widget/Button;

    const v2, 0x7f0b03e4    # com.google.android.gms.R.string.plus_bad_name_skip_button_label

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lfyt;->c:Landroid/widget/Button;

    const v2, 0x7f0b03e3    # com.google.android.gms.R.string.plus_bad_name_agree_button_label

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method protected final a()V
    .locals 0

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Lfyw;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfyu;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lfyu;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lfyu;

    iput-object p1, p0, Lfyt;->e:Lfyu;

    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 3

    const v1, 0x7f0400df    # com.google.android.gms.R.layout.plus_oob_buttons

    const v0, 0x7f0a02b1    # com.google.android.gms.R.id.buttons_layout

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 4

    invoke-super {p0, p1}, Lfyw;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    iget-object v0, p0, Lfyt;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lfyt;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Lfyj;

    if-eqz v3, :cond_0

    check-cast v0, Lfyj;

    invoke-virtual {v0}, Lfyj;->m()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lfyj;->k()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lfyt;->c:Landroid/widget/Button;

    invoke-virtual {v0}, Lfyj;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lfyj;->l()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lfyt;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Lfyj;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method protected final b()V
    .locals 0

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lfyt;->a:Lfyf;

    sget-object v1, Lbcr;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v2, p0, Lfyt;->a:Lfyf;

    invoke-interface {v2}, Lfyf;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lfyf;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget-object v0, p0, Lfyt;->e:Lfyu;

    invoke-interface {v0}, Lfyu;->f()V

    goto :goto_0

    :sswitch_1
    const-string v0, "invalidNameHardFail"

    iget-object v1, p0, Lfyt;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyt;->e:Lfyu;

    invoke-interface {v0}, Lfyu;->e()V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lfyw;->onClick(Landroid/view/View;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a00a5 -> :sswitch_1    # com.google.android.gms.R.id.next_button
        0x7f0a00aa -> :sswitch_0    # com.google.android.gms.R.id.back_button
    .end sparse-switch
.end method
