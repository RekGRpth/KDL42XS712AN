.class public final Lguh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lguq;


# instance fields
.field a:Ljava/util/ArrayList;

.field private final b:Landroid/content/Context;

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Lgur;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lguh;->c:Z

    iput-boolean v0, p0, Lguh;->d:Z

    iput-object p1, p0, Lguh;->b:Landroid/content/Context;

    return-void
.end method

.method private static a(II)I
    .locals 2

    sub-int v0, p0, p1

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Device data exceeds allowed storage for source"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return v0
.end method

.method private static a(Ljava/lang/String;)I
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x2d

    div-int/lit8 v0, v0, 0x8

    mul-int/lit8 v0, v0, 0x8

    goto :goto_0
.end method

.method private declared-synchronized c()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lguh;->a:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lguh;->d()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lguh;->a:Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "DevicePhoneNumberSource"

    const-string v2, "Could not retrieve contacts"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lguh;->a:Ljava/util/ArrayList;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d()Ljava/util/ArrayList;
    .locals 14

    const/4 v12, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Lguh;->f()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lguh;->b:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    div-int/lit8 v0, v0, 0x10

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_8

    const/high16 v0, 0x100000

    move v6, v0

    :goto_1
    iget-object v0, p0, Lguh;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v11, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v2, v7

    const-string v1, "data1"

    aput-object v1, v2, v10

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "in_visible_group=1 AND mimetype=?"

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "vnd.android.cursor.item/name"

    aput-object v5, v4, v7

    const-string v5, "contact_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v8, Landroid/util/SparseArray;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v8, v2}, Landroid/util/SparseArray;-><init>(I)V

    const-string v2, "contact_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    const-string v3, "data1"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    :cond_2
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    invoke-static {v5}, Lguh;->a(Ljava/lang/String;)I

    move-result v9

    invoke-static {v6, v9}, Lguh;->a(II)I

    move-result v6

    new-instance v9, Lgui;

    invoke-direct {v9, v5}, Lgui;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4, v9}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v2, v7

    const-string v1, "data1"

    aput-object v1, v2, v10

    const-string v1, "data2"

    aput-object v1, v2, v11

    const/4 v1, 0x3

    const-string v3, "data3"

    aput-object v3, v2, v1

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "in_visible_group=1 AND data1 IS NOT NULL"

    move-object v4, v12

    move-object v5, v12

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-static {v2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    const-string v0, "contact_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    const-string v0, "data1"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    const-string v0, "data2"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    const-string v0, "data3"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    move v1, v6

    :cond_4
    :goto_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v10, 0x0

    invoke-static {v6, v10}, Lhgr;->a(Ljava/lang/CharSequence;Z)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    invoke-virtual {v8, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgui;

    if-eqz v0, :cond_4

    invoke-static {v6}, Lguh;->a(Ljava/lang/String;)I

    move-result v12

    add-int/lit8 v12, v12, 0x4

    invoke-static {v11}, Lguh;->a(Ljava/lang/String;)I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v1, v12}, Lguh;->a(II)I

    move-result v1

    new-instance v12, Lguj;

    invoke-direct {v12, v6, v10, v11}, Lguj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    iget-object v0, v0, Lgui;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v2, v7

    :goto_4
    if-ge v2, v3, :cond_7

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgui;

    iget-object v4, v0, Lgui;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto/16 :goto_0

    :cond_8
    move v6, v0

    goto/16 :goto_1
.end method

.method private e()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lguh;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lguh;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lguh;->e:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lguh;->e:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized f()Z
    .locals 3

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lguh;->d:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lguh;->d:Z

    iget-object v1, p0, Lguh;->b:Landroid/content/Context;

    const-string v2, "android.permission.READ_CONTACTS"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lguh;->c:Z

    :cond_0
    iget-boolean v0, p0, Lguh;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "DevicePhoneNumberSource"

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 18

    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-direct/range {p0 .. p0}, Lguh;->c()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lhgr;->a(Ljava/lang/CharSequence;Z)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Lhgr;->a(Ljava/lang/CharSequence;)Ljava/util/regex/Pattern;

    move-result-object v13

    invoke-direct/range {p0 .. p0}, Lguh;->e()Ljava/lang/String;

    move-result-object v14

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lguh;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v15

    move v10, v1

    :goto_1
    if-ge v10, v15, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lguh;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgui;

    iget-object v2, v1, Lgui;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    iget-object v3, v1, Lgui;->a:Ljava/lang/String;

    invoke-virtual {v13, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v2, 0x2

    move v3, v2

    :goto_2
    const/4 v2, 0x0

    iget-object v4, v1, Lgui;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v16

    move v9, v2

    :goto_3
    move/from16 v0, v16

    if-ge v9, v0, :cond_4

    iget-object v2, v1, Lgui;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lguj;

    iget-object v4, v2, Lguj;->a:Ljava/lang/String;

    invoke-static {v12, v4}, Lhgr;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    or-int/lit8 v5, v3, 0x1

    :goto_4
    if-eqz v5, :cond_1

    iget-object v4, v2, Lguj;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    and-int/lit8 v4, v5, 0x1

    if-eqz v4, :cond_2

    invoke-static {v8, v12}, Lhgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    move-object v7, v4

    :goto_5
    and-int/lit8 v4, v5, 0x2

    if-eqz v4, :cond_3

    iget-object v4, v1, Lgui;->a:Ljava/lang/String;

    invoke-static {v4, v13}, Lhgr;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Landroid/text/SpannableString;

    move-result-object v4

    :goto_6
    iget-object v2, v2, Lguj;->a:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    or-int/lit8 v2, v5, 0x4

    :goto_7
    new-instance v5, Lgur;

    const-string v17, "DevicePhoneNumberSource"

    move-object/from16 v0, v17

    invoke-direct {v5, v7, v4, v2, v0}, Lgur;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v11, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_3

    :cond_2
    move-object v7, v8

    goto :goto_5

    :cond_3
    iget-object v4, v1, Lgui;->a:Ljava/lang/String;

    goto :goto_6

    :cond_4
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto/16 :goto_1

    :cond_5
    if-eqz v14, :cond_6

    invoke-virtual {v14, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v14}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {v1, v12}, Lhgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    new-instance v3, Lgur;

    const/4 v4, 0x0

    const/4 v5, 0x5

    const-string v7, "DevicePhoneNumberSource"

    invoke-direct {v3, v2, v4, v5, v7}, Lgur;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v11, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_6
    sget-object v1, Lgur;->a:Ljava/util/Comparator;

    invoke-static {v6, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object v1, v6

    goto/16 :goto_0

    :cond_7
    move v2, v5

    goto :goto_7

    :cond_8
    move v5, v3

    goto :goto_4

    :cond_9
    move v3, v2

    goto/16 :goto_2
.end method

.method public final b()Lgur;
    .locals 5

    iget-object v0, p0, Lguh;->f:Lgur;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lguh;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lgur;

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-string v4, "DevicePhoneNumberSource"

    invoke-direct {v1, v0, v2, v3, v4}, Lgur;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;)V

    iput-object v1, p0, Lguh;->f:Lgur;

    :cond_0
    iget-object v0, p0, Lguh;->f:Lgur;

    return-object v0
.end method
