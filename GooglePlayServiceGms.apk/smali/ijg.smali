.class public final Lijg;
.super Liix;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/reporting/service/PreferenceService;


# direct methods
.method private constructor <init>(Lcom/google/android/location/reporting/service/PreferenceService;)V
    .locals 0

    iput-object p1, p0, Lijg;->a:Lcom/google/android/location/reporting/service/PreferenceService;

    invoke-direct {p0}, Liix;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/location/reporting/service/PreferenceService;B)V
    .locals 0

    invoke-direct {p0, p1}, Lijg;-><init>(Lcom/google/android/location/reporting/service/PreferenceService;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;
    .locals 1

    iget-object v0, p0, Lijg;->a:Lcom/google/android/location/reporting/service/PreferenceService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Lcom/google/android/location/reporting/service/PreferenceService;)Lijt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lijt;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/location/reporting/service/ReportingConfig;
    .locals 1

    iget-object v0, p0, Lijg;->a:Lcom/google/android/location/reporting/service/PreferenceService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Lcom/google/android/location/reporting/service/PreferenceService;)Lijt;

    move-result-object v0

    invoke-virtual {v0}, Lijt;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;Z)V
    .locals 4

    invoke-static {p1}, Liir;->a(Landroid/accounts/Account;)Liis;

    move-result-object v0

    invoke-virtual {v0}, Liis;->b()Liis;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Liis;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Liis;->a()Liir;

    move-result-object v0

    iget-object v1, p0, Lijg;->a:Lcom/google/android/location/reporting/service/PreferenceService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Lcom/google/android/location/reporting/service/PreferenceService;)Lijt;

    move-result-object v1

    const-string v2, "PrefService.setReportingEnabled"

    const-string v3, "ui_update"

    invoke-virtual {v1, v2, v0, v3}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    return-void
.end method

.method public final b(Landroid/accounts/Account;Z)V
    .locals 4

    invoke-static {p1}, Liir;->a(Landroid/accounts/Account;)Liis;

    move-result-object v0

    invoke-virtual {v0}, Liis;->b()Liis;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Liis;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Liis;->a()Liir;

    move-result-object v0

    iget-object v1, p0, Lijg;->a:Lcom/google/android/location/reporting/service/PreferenceService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Lcom/google/android/location/reporting/service/PreferenceService;)Lijt;

    move-result-object v1

    const-string v2, "PrefService.setHistoryEnabled"

    const-string v3, "ui_update"

    invoke-virtual {v1, v2, v0, v3}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    return-void
.end method
