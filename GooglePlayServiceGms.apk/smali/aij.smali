.class public final Laij;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:I

.field public d:Z

.field public e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laij;->f:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Laij;->c:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laij;->g:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/appdatasearch/Feature;)Laij;
    .locals 1

    iget-object v0, p0, Laij;->g:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/util/List;Lcom/google/android/gms/appdatasearch/Feature;)V

    return-object p0
.end method

.method public final a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;
    .locals 9

    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v1, p0, Laij;->f:Ljava/lang/String;

    iget-object v2, p0, Laij;->a:Ljava/lang/String;

    iget-boolean v3, p0, Laij;->b:Z

    iget v4, p0, Laij;->c:I

    iget-boolean v5, p0, Laij;->d:Z

    iget-object v6, p0, Laij;->e:Ljava/lang/String;

    iget-object v7, p0, Laij;->g:Ljava/util/List;

    iget-object v8, p0, Laij;->g:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-interface {v7, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V

    return-object v0
.end method
