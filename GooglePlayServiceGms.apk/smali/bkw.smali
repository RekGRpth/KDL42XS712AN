.class public final Lbkw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/people/data/Audience;


# instance fields
.field private b:Ljava/util/List;

.field private c:I

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbkw;

    invoke-direct {v0}, Lbkw;-><init>()V

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    sput-object v0, Lbkw;->a:Lcom/google/android/gms/common/people/data/Audience;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbkw;->b:Ljava/util/List;

    iput v1, p0, Lbkw;->c:I

    iput-boolean v1, p0, Lbkw;->d:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Audience must not be null."

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbkw;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v0

    iput v0, p0, Lbkw;->c:I

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->d()Z

    move-result v0

    iput-boolean v0, p0, Lbkw;->d:Z

    return-void
.end method


# virtual methods
.method public final a(I)Lbkw;
    .locals 3

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown domain restriction setting: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iput p1, p0, Lbkw;->c:I

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/util/Collection;)Lbkw;
    .locals 2

    new-instance v1, Ljava/util/ArrayList;

    const-string v0, "Audience members must not be null."

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbkw;->b:Ljava/util/List;

    return-object p0
.end method

.method public final a(Z)Lbkw;
    .locals 0

    iput-boolean p1, p0, Lbkw;->d:Z

    return-object p0
.end method

.method public final a()Lcom/google/android/gms/common/people/data/Audience;
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/people/data/Audience;

    iget-object v1, p0, Lbkw;->b:Ljava/util/List;

    iget v2, p0, Lbkw;->c:I

    iget-boolean v3, p0, Lbkw;->d:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/people/data/Audience;-><init>(Ljava/util/List;IZ)V

    return-object v0
.end method
