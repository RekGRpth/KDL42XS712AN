.class public final Ldzv;
.super Ldvj;
.source "SourceFile"


# static fields
.field private static final m:Landroid/widget/LinearLayout$LayoutParams;


# instance fields
.field a:Ldff;

.field b:Ldff;

.field c:Ldff;

.field e:I

.field f:Landroid/widget/LinearLayout;

.field g:Landroid/widget/LinearLayout;

.field h:Landroid/view/View;

.field i:Landroid/view/View;

.field j:Landroid/view/View;

.field k:Landroid/view/View;

.field l:Landroid/view/View;

.field private final n:Landroid/content/Context;

.field private final o:Landroid/view/LayoutInflater;

.field private final p:Landroid/view/View$OnClickListener;

.field private q:Ljava/lang/String;

.field private r:J

.field private final s:Z

.field private final t:Z

.field private final u:I

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    sput-object v0, Ldzv;->m:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;ZZ)V
    .locals 2

    invoke-direct {p0}, Ldvj;-><init>()V

    iput-object p1, p0, Ldzv;->n:Landroid/content/Context;

    iput-object p2, p0, Ldzv;->p:Landroid/view/View$OnClickListener;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldzv;->o:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lww;->c:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Ldzv;->t:Z

    iput-boolean p3, p0, Ldzv;->s:Z

    if-eqz p3, :cond_0

    sget v1, Lwx;->u:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Ldzv;->u:I

    sget v1, Lwz;->p:I

    iput v1, p0, Ldzv;->x:I

    :goto_0
    sget v1, Lwx;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Ldzv;->v:I

    sget v1, Lwx;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Ldzv;->w:I

    iput-boolean p4, p0, Ldzv;->y:Z

    return-void

    :cond_0
    sget v1, Lwx;->t:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Ldzv;->u:I

    sget v1, Lwz;->q:I

    iput v1, p0, Ldzv;->x:I

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Ldff;)V
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzw;

    if-nez p1, :cond_1

    iget-object v0, v0, Ldzw;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, v0, Ldzw;->i:Ldzv;

    iget-object v1, v1, Ldzv;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {p1}, Ldff;->m()Lcom/google/android/gms/games/Player;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-interface {v6}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v4, v0, Ldzw;->i:Ldzv;

    iget-object v4, v4, Ldzv;->q:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    iget-object v4, v0, Ldzw;->i:Ldzv;

    iget-boolean v4, v4, Ldvj;->d:Z

    if-eqz v4, :cond_4

    invoke-interface {p1}, Ldff;->k()Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-interface {p1}, Ldff;->i()Landroid/net/Uri;

    move-result-object v4

    :cond_2
    iget-object v7, v0, Ldzw;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v8, Lwz;->f:I

    invoke-virtual {v7, v4, v8}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_2
    if-eqz v1, :cond_5

    sget v4, Lxf;->aN:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v7, v0, Ldzw;->c:Landroid/database/CharArrayBuffer;

    invoke-static {v4, v7}, Lbpf;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    :goto_3
    iget-object v4, v0, Ldzw;->b:Landroid/widget/TextView;

    iget-object v7, v0, Ldzw;->c:Landroid/database/CharArrayBuffer;

    iget-object v7, v7, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v8, v0, Ldzw;->c:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v4, v7, v3, v8}, Landroid/widget/TextView;->setText([CII)V

    iget-object v4, v0, Ldzw;->e:Landroid/database/CharArrayBuffer;

    invoke-interface {p1, v4}, Ldff;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v4, v0, Ldzw;->d:Landroid/widget/TextView;

    iget-object v7, v0, Ldzw;->e:Landroid/database/CharArrayBuffer;

    iget-object v7, v7, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v8, v0, Ldzw;->e:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v4, v7, v3, v8}, Landroid/widget/TextView;->setText([CII)V

    iget-object v4, v0, Ldzw;->f:Landroid/widget/ImageView;

    iget-object v7, v0, Ldzw;->i:Ldzv;

    iget v7, v7, Ldzv;->x:I

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    if-eqz v1, :cond_6

    iget-object v1, v0, Ldzw;->d:Landroid/widget/TextView;

    iget-object v4, v0, Ldzw;->i:Ldzv;

    iget v4, v4, Ldzv;->v:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Ldzw;->g:Landroid/widget/TextView;

    iget-object v4, v0, Ldzw;->i:Ldzv;

    iget v4, v4, Ldzv;->u:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setBackgroundColor(I)V

    iget-object v1, v0, Ldzw;->g:Landroid/widget/TextView;

    const/4 v4, -0x1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_4
    invoke-interface {p1}, Ldff;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ledm;->a(J)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Ldzw;->g:Landroid/widget/TextView;

    invoke-interface {p1}, Ldff;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    iget-object v1, v0, Ldzw;->i:Ldzv;

    iget-boolean v1, v1, Ldzv;->s:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldzw;->h:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, v0, Ldzw;->h:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    move v1, v3

    goto/16 :goto_1

    :cond_4
    iget-object v4, v0, Ldzw;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_2

    :cond_5
    iget-object v4, v0, Ldzw;->c:Landroid/database/CharArrayBuffer;

    invoke-interface {p1, v4}, Ldff;->b(Landroid/database/CharArrayBuffer;)V

    goto :goto_3

    :cond_6
    iget-object v1, v0, Ldzw;->d:Landroid/widget/TextView;

    iget-object v4, v0, Ldzw;->i:Ldzv;

    iget v4, v4, Ldzv;->w:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Ldzw;->g:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, v0, Ldzw;->g:Landroid/widget/TextView;

    iget-object v4, v0, Ldzw;->i:Ldzv;

    iget v4, v4, Ldzv;->u:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4

    :cond_7
    const-wide/16 v9, 0x64

    mul-long/2addr v7, v9

    iget-object v1, v0, Ldzw;->i:Ldzv;

    iget-wide v9, v1, Ldzv;->r:J

    div-long/2addr v7, v9

    long-to-int v1, v7

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sget v4, Lxf;->aK:I

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v3

    invoke-virtual {v5, v4, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Ldzw;->g:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method

.method private a(Ldff;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Ldzv;->q:Ljava/lang/String;

    invoke-static {v1}, Lbiq;->a(Ljava/lang/Object;)V

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Ldff;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Ldzv;->q:Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Ldzv;)Z
    .locals 1

    iget-boolean v0, p0, Ldzv;->s:Z

    return v0
.end method

.method static synthetic b(Ldzv;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Ldzv;->p:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private b()V
    .locals 5

    const/4 v2, 0x4

    const/16 v3, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Ldzv;->a:Ldff;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    iget-object v4, p0, Ldzv;->b:Ldff;

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    iget-object v4, p0, Ldzv;->c:Ldff;

    if-eqz v4, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "It is not possible to have more than 3 scores in a podium"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Ldzv;->j:Landroid/view/View;

    iget-boolean v0, p0, Ldzv;->t:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void

    :cond_2
    move v0, v3

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    iget-object v2, p0, Ldzv;->a:Ldff;

    invoke-static {v0, v2}, Ldzv;->a(Landroid/view/View;Ldff;)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Ldzv;->k:Landroid/view/View;

    iget-boolean v0, p0, Ldzv;->y:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    iget-boolean v2, p0, Ldzv;->y:Z

    if-eqz v2, :cond_4

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_3

    :cond_4
    move v1, v3

    goto :goto_4

    :pswitch_2
    iget-boolean v0, p0, Ldzv;->t:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    iget-object v4, p0, Ldzv;->a:Ldff;

    invoke-static {v0, v4}, Ldzv;->a(Landroid/view/View;Ldff;)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    iget-object v1, p0, Ldzv;->b:Ldff;

    invoke-static {v0, v1}, Ldzv;->a(Landroid/view/View;Ldff;)V

    iget-object v0, p0, Ldzv;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    iget-object v0, p0, Ldzv;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    iget-object v2, p0, Ldzv;->a:Ldff;

    invoke-static {v0, v2}, Ldzv;->a(Landroid/view/View;Ldff;)V

    iget-object v0, p0, Ldzv;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->j:Landroid/view/View;

    iget-object v1, p0, Ldzv;->b:Ldff;

    invoke-static {v0, v1}, Ldzv;->a(Landroid/view/View;Ldff;)V

    goto :goto_5

    :pswitch_3
    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    iget-object v2, p0, Ldzv;->a:Ldff;

    invoke-static {v0, v2}, Ldzv;->a(Landroid/view/View;Ldff;)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    iget-object v2, p0, Ldzv;->b:Ldff;

    invoke-static {v0, v2}, Ldzv;->a(Landroid/view/View;Ldff;)V

    iget-object v0, p0, Ldzv;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->j:Landroid/view/View;

    iget-object v1, p0, Ldzv;->c:Ldff;

    invoke-static {v0, v1}, Ldzv;->a(Landroid/view/View;Ldff;)V

    iget-object v0, p0, Ldzv;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0}, Ldzv;->a(Ldff;Ldff;Ldff;)V

    return-void
.end method

.method public final a(J)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iput-wide p1, p0, Ldzv;->r:J

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldff;Ldff;Ldff;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Ldzv;->q:Ljava/lang/String;

    const-string v3, "Player Id needs to be set before the podium scores"

    invoke-static {v0, v3}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    if-nez p1, :cond_0

    if-nez p2, :cond_3

    if-nez p3, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    :cond_0
    if-nez p2, :cond_1

    if-nez p3, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbkm;->a(Z)V

    :cond_1
    iput-object p1, p0, Ldzv;->a:Ldff;

    iput-object p2, p0, Ldzv;->b:Ldff;

    iput-object p3, p0, Ldzv;->c:Ldff;

    iget-object v0, p0, Ldzv;->a:Ldff;

    if-nez v0, :cond_6

    iget-boolean v0, p0, Ldzv;->y:Z

    if-eqz v0, :cond_5

    :goto_2
    iput v1, p0, Ldzv;->e:I

    :goto_3
    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Ldzv;->b()V

    :cond_2
    invoke-virtual {p0}, Ldzv;->notifyDataSetChanged()V

    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    iget-object v0, p0, Ldzv;->b:Ldff;

    if-nez v0, :cond_a

    iget-object v0, p0, Ldzv;->c:Ldff;

    if-nez v0, :cond_a

    iget-boolean v0, p0, Ldzv;->t:Z

    if-eqz v0, :cond_7

    move-object v0, p0

    :goto_4
    iput v1, v0, Ldzv;->e:I

    goto :goto_3

    :cond_7
    iget-boolean v0, p0, Ldzv;->y:Z

    if-eqz v0, :cond_9

    invoke-direct {p0, p1}, Ldzv;->a(Ldff;)Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v0, p0

    :goto_5
    const/4 v1, 0x2

    goto :goto_4

    :cond_8
    move-object v0, p0

    goto :goto_4

    :cond_9
    move-object v0, p0

    goto :goto_4

    :cond_a
    iget-boolean v0, p0, Ldzv;->t:Z

    if-eqz v0, :cond_b

    move-object v0, p0

    goto :goto_4

    :cond_b
    move-object v0, p0

    goto :goto_5
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldzv;->q:Ljava/lang/String;

    return-void
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Ldzv;->e:I

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget v0, p0, Ldzv;->e:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    iget v0, p0, Ldzv;->e:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    int-to-long v0, p1

    return-wide v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const/4 v7, -0x1

    const/4 v6, -0x2

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Ldzv;->e:I

    if-ge p1, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    if-nez v0, :cond_3

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v4, Landroid/widget/LinearLayout;

    invoke-direct {v4, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    iget-object v4, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v4, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v4, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    sget v5, Lxa;->W:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setId(I)V

    iget-object v4, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v4, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    new-instance v5, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v5, v7, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    sget v5, Lwy;->l:I

    invoke-static {v4, v5}, Leee;->a(Landroid/view/View;I)V

    new-instance v4, Landroid/widget/LinearLayout;

    invoke-direct {v4, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    sget v4, Lxa;->X:I

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setId(I)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    new-instance v4, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v4, v7, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    sget v4, Lwy;->l:I

    invoke-static {v0, v4}, Leee;->a(Landroid/view/View;I)V

    iget-object v0, p0, Ldzv;->o:Landroid/view/LayoutInflater;

    sget v4, Lxc;->m:I

    iget-object v5, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzv;->h:Landroid/view/View;

    new-instance v0, Ldzw;

    iget-object v4, p0, Ldzv;->h:Landroid/view/View;

    invoke-direct {v0, p0, v4}, Ldzw;-><init>(Ldzv;Landroid/view/View;)V

    iget-object v4, p0, Ldzv;->h:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Ldzv;->o:Landroid/view/LayoutInflater;

    sget v4, Lxc;->m:I

    invoke-virtual {v0, v4, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzv;->i:Landroid/view/View;

    new-instance v0, Ldzw;

    iget-object v4, p0, Ldzv;->i:Landroid/view/View;

    invoke-direct {v0, p0, v4}, Ldzw;-><init>(Ldzv;Landroid/view/View;)V

    iget-object v4, p0, Ldzv;->i:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Ldzv;->o:Landroid/view/LayoutInflater;

    sget v4, Lxc;->m:I

    invoke-virtual {v0, v4, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzv;->j:Landroid/view/View;

    new-instance v0, Ldzw;

    iget-object v4, p0, Ldzv;->j:Landroid/view/View;

    invoke-direct {v0, p0, v4}, Ldzw;-><init>(Ldzv;Landroid/view/View;)V

    iget-object v4, p0, Ldzv;->j:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Ldzv;->o:Landroid/view/LayoutInflater;

    sget v4, Lxc;->n:I

    invoke-virtual {v0, v4, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzv;->k:Landroid/view/View;

    iget-object v0, p0, Ldzv;->k:Landroid/view/View;

    sget v4, Lxa;->aT:I

    invoke-virtual {v0, v4}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Ldzv;->k:Landroid/view/View;

    sget v4, Lxa;->Q:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v4, Lwz;->F:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    sget v4, Lwz;->r:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Ldzv;->k:Landroid/view/View;

    sget v4, Lxa;->V:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v4, Lxf;->af:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Ldzv;->k:Landroid/view/View;

    sget v4, Lxa;->aq:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v4, p0, Ldzv;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    const-string v4, "ShareView"

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Ldzv;->o:Landroid/view/LayoutInflater;

    sget v4, Lxc;->n:I

    invoke-virtual {v0, v4, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzv;->l:Landroid/view/View;

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    sget v2, Lxa;->x:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    sget v2, Lxa;->Q:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v2, Lwz;->E:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    sget v2, Lwz;->k:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    sget v2, Lxa;->V:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lxf;->ad:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    sget v2, Lxa;->aq:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Ldzv;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    const-string v2, "FindPeople"

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-boolean v0, p0, Ldzv;->t:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_1
    iget-object v0, p0, Ldzv;->h:Landroid/view/View;

    sget-object v2, Ldzv;->m:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Ldzv;->i:Landroid/view/View;

    sget-object v2, Ldzv;->m:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Ldzv;->j:Landroid/view/View;

    sget-object v2, Ldzv;->m:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Ldzv;->k:Landroid/view/View;

    sget-object v2, Ldzv;->m:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Ldzv;->l:Landroid/view/View;

    sget-object v2, Ldzv;->m:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Ldzv;->b()V

    :goto_2
    if-nez p1, :cond_5

    iget v0, p0, Ldzv;->e:I

    if-ne v0, v1, :cond_4

    iget-boolean v0, p0, Ldzv;->y:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Ldzv;->t:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Ldzv;->a:Ldff;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldzv;->a:Ldff;

    invoke-direct {p0, v0}, Ldzv;->a(Ldff;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    :goto_3
    return-object v0

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Ldzv;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Ldzv;->b()V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldzv;->f:Landroid/widget/LinearLayout;

    goto :goto_3

    :cond_5
    if-ne p1, v1, :cond_6

    iget-object v0, p0, Ldzv;->g:Landroid/widget/LinearLayout;

    goto :goto_3

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid podium adapter position: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    move-object v0, v3

    goto :goto_3
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
