.class public final Leck;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Ldle;
.implements Ldws;


# instance fields
.field private ad:Lebc;

.field private ae:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxe;-><init>()V

    return-void
.end method

.method private V()V
    .locals 5

    invoke-virtual {p0}, Leck;->J()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ReqInboxListFrag"

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Leck;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Leck;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->b()V

    iget-object v0, p0, Leck;->ad:Lebc;

    iget-object v2, p0, Leck;->ae:Ljava/lang/String;

    invoke-virtual {p0}, Leck;->N()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Leck;->O()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lebc;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Leck;
    .locals 3

    invoke-static {p0}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v0, Leck;

    invoke-direct {v0}, Leck;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "external_game_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Leck;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final F_()V
    .locals 0

    invoke-direct {p0}, Leck;->V()V

    return-void
.end method

.method public final G_()V
    .locals 0

    invoke-direct {p0}, Leck;->V()V

    return-void
.end method

.method public final M_()V
    .locals 3

    invoke-super {p0}, Ldxe;->M_()V

    invoke-virtual {p0}, Leck;->K()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ReqInboxListFrag"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Leck;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->o:Ldlf;

    iget-object v2, p0, Leck;->ae:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ldlf;->b(Lbdu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final R()V
    .locals 0

    invoke-direct {p0}, Leck;->V()V

    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040066    # com.google.android.gms.R.layout.games_inbox_list_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;)V
    .locals 4

    iget-object v0, p0, Leck;->ad:Lebc;

    iget-object v1, p0, Leck;->ae:Ljava/lang/String;

    invoke-virtual {p0}, Leck;->N()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Leck;->O()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lebc;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Leck;->Y:Ldvn;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Leck;->Y:Ldvn;

    instance-of v0, v0, Lebe;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Leck;->Y:Ldvn;

    check-cast v0, Lebe;

    invoke-interface {v0}, Lebe;->v()Lebd;

    move-result-object v1

    invoke-static {v1}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "external_game_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "Must specify an external game id!"

    invoke-static {v2, v3}, Lbiq;->a(ZLjava/lang/Object;)V

    const-string v2, "external_game_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leck;->ae:Ljava/lang/String;

    new-instance v2, Lebc;

    iget-object v3, p0, Leck;->Y:Ldvn;

    iget-object v0, p0, Leck;->Y:Ldvn;

    check-cast v0, Ldwt;

    invoke-direct {v2, v3, v1, v0, p0}, Lebc;-><init>(Ldvn;Lebh;Ldwt;Ldws;)V

    iput-object v2, p0, Leck;->ad:Lebc;

    iget-object v0, p0, Leck;->ad:Lebc;

    invoke-virtual {p0, v0}, Leck;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Leck;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Leck;->ad:Lebc;

    invoke-virtual {v0}, Lebc;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method

.method public final y_()V
    .locals 3

    iget-object v0, p0, Leck;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Leck;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    iget-object v0, p0, Leck;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    invoke-virtual {p0}, Leck;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->o:Ldlf;

    iget-object v2, p0, Leck;->ae:Ljava/lang/String;

    invoke-interface {v1, v0, p0, v2}, Ldlf;->a(Lbdu;Ldle;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final z_()V
    .locals 2

    iget-object v0, p0, Leck;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Leck;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    iget-object v0, p0, Leck;->Z:Leds;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Leds;->a(I)V

    return-void
.end method
