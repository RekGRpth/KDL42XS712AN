.class public final Lfow;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfup;


# instance fields
.field private Y:Z

.field private final a:Lftz;

.field private b:Lftx;

.field private c:Landroid/accounts/Account;

.field private d:Z

.field private e:Lfox;

.field private f:Z

.field private g:Lfxh;

.field private h:Z

.field private i:Lbbo;


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lfow;-><init>(Lftz;)V

    return-void
.end method

.method private constructor <init>(Lftz;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-object p1, p0, Lfow;->a:Lftz;

    return-void
.end method

.method public static a(Landroid/accounts/Account;)Lfow;
    .locals 3

    sget-object v0, Lftx;->a:Lftz;

    new-instance v1, Lfow;

    invoke-direct {v1, v0}, Lfow;-><init>(Lftz;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lfow;->g(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfow;->e:Lfox;

    return-void
.end method

.method public final P_()V
    .locals 1

    iget-boolean v0, p0, Lfow;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfow;->b:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfow;->d:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfow;->d:Z

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    iget-boolean v0, p0, Lfow;->Y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfow;->i:Lbbo;

    invoke-virtual {p0, v0}, Lfow;->b(Lbbo;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfox;

    if-eqz v0, :cond_0

    check-cast p1, Lfox;

    iput-object p1, p0, Lfow;->e:Lfox;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DisconnectSourceFragment must be hosted by an activity that implements DisconnectSourceCallbacks."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lbbo;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lfow;->d:Z

    iput-object p1, p0, Lfow;->i:Lbbo;

    iput-boolean v0, p0, Lfow;->Y:Z

    iget-boolean v2, p0, Lfow;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfow;->e:Lfox;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfow;->e:Lfox;

    iget-object v3, p0, Lfow;->g:Lfxh;

    invoke-interface {v2, p1, v3}, Lfox;->a(Lbbo;Lfxh;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_0
    iput-boolean v0, p0, Lfow;->Y:Z

    :cond_0
    iget-boolean v0, p0, Lfow;->Y:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lfow;->f:Z

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lfxh;Z)Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lfow;->f:Z

    if-eqz v1, :cond_2

    const-string v0, "DisconnectSource"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DisconnectSource"

    const-string v1, "Can only disconnect one app at a time."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :cond_2
    iput-boolean v0, p0, Lfow;->f:Z

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lfxh;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v1

    iput-object v1, p0, Lfow;->g:Lfxh;

    iput-boolean p2, p0, Lfow;->h:Z

    iget-object v1, p0, Lfow;->b:Lftx;

    invoke-interface {v1}, Lftx;->d_()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v1}, Lfow;->b_(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lfow;->d:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lfow;->b:Lftx;

    invoke-interface {v1}, Lftx;->a()V

    iput-boolean v0, p0, Lfow;->d:Z

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lfow;->q()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lfow;->c:Landroid/accounts/Account;

    iget-object v0, p0, Lfow;->a:Lftz;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lfow;->c:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1, p0, p0, v2}, Lfob;->a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lfow;->b:Lftx;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfow;->Y:Z

    return-void
.end method

.method public final b(Lbbo;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lfow;->i:Lbbo;

    iput-boolean v0, p0, Lfow;->Y:Z

    iget-boolean v2, p0, Lfow;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfow;->e:Lfox;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfow;->e:Lfox;

    iget-object v3, p0, Lfow;->g:Lfxh;

    invoke-interface {v2, p1, v3}, Lfox;->a(Lbbo;Lfxh;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_0
    iput-boolean v0, p0, Lfow;->Y:Z

    :cond_0
    iget-boolean v0, p0, Lfow;->Y:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lfow;->f:Z

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 6

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfow;->d:Z

    iget-boolean v0, p0, Lfow;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfow;->b:Lftx;

    iget-object v1, p0, Lfow;->g:Lfxh;

    invoke-interface {v1}, Lfxh;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lfow;->g:Lfxh;

    invoke-interface {v1}, Lfxh;->d()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lfow;->h:Z

    iget-object v1, p0, Lfow;->g:Lfxh;

    invoke-interface {v1}, Lfxh;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfow;->g:Lfxh;

    invoke-interface {v1}, Lfxh;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v5, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    :goto_0
    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lftx;->a(Lfup;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final y()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    iget-object v0, p0, Lfow;->b:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfow;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfow;->b:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    iput-object v2, p0, Lfow;->b:Lftx;

    iput-boolean v1, p0, Lfow;->d:Z

    iput-boolean v1, p0, Lfow;->f:Z

    iput-object v2, p0, Lfow;->g:Lfxh;

    return-void
.end method
