.class public final Lsf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Ljava/util/Set;

.field final c:Ljava/util/concurrent/PriorityBlockingQueue;

.field private d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final e:Ljava/util/concurrent/PriorityBlockingQueue;

.field private final f:Lro;

.field private final g:Lrw;

.field private final h:Lsl;

.field private i:[Lrx;

.field private j:Lrq;


# direct methods
.method private constructor <init>(Lro;Lrw;)V
    .locals 4

    const/4 v0, 0x4

    new-instance v1, Lrt;

    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v1, v2}, Lrt;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lsf;-><init>(Lro;Lrw;ILsl;)V

    return-void
.end method

.method public constructor <init>(Lro;Lrw;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lsf;-><init>(Lro;Lrw;)V

    return-void
.end method

.method private constructor <init>(Lro;Lrw;ILsl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lsf;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsf;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsf;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lsf;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lsf;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iput-object p1, p0, Lsf;->f:Lro;

    iput-object p2, p0, Lsf;->g:Lrw;

    const/4 v0, 0x4

    new-array v0, v0, [Lrx;

    iput-object v0, p0, Lsf;->i:[Lrx;

    iput-object p4, p0, Lsf;->h:Lsl;

    return-void
.end method


# virtual methods
.method public final a(Lsc;)Lsc;
    .locals 5

    invoke-virtual {p1, p0}, Lsc;->a(Lsf;)Lsc;

    iget-object v1, p0, Lsf;->b:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lsf;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lsf;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-virtual {p1, v0}, Lsc;->a(I)Lsc;

    const-string v0, "add-to-queue"

    invoke-virtual {p1, v0}, Lsc;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Lsc;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lsf;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object p1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    iget-object v1, p0, Lsf;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_1
    invoke-virtual {p1}, Lsc;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lsf;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsf;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    :cond_1
    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lsf;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v0, Lsq;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "Request for cacheKey=%s is in flight, putting on hold."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v3}, Lsq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_2
    iget-object v0, p0, Lsf;->a:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lsf;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1
.end method

.method public final a()V
    .locals 6

    invoke-virtual {p0}, Lsf;->b()V

    new-instance v0, Lrq;

    iget-object v1, p0, Lsf;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v2, p0, Lsf;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lsf;->f:Lro;

    iget-object v4, p0, Lsf;->h:Lsl;

    invoke-direct {v0, v1, v2, v3, v4}, Lrq;-><init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lro;Lsl;)V

    iput-object v0, p0, Lsf;->j:Lrq;

    iget-object v0, p0, Lsf;->j:Lrq;

    invoke-virtual {v0}, Lrq;->start()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lsf;->i:[Lrx;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    new-instance v1, Lrx;

    iget-object v2, p0, Lsf;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lsf;->g:Lrw;

    iget-object v4, p0, Lsf;->f:Lro;

    iget-object v5, p0, Lsf;->h:Lsl;

    invoke-direct {v1, v2, v3, v4, v5}, Lrx;-><init>(Ljava/util/concurrent/BlockingQueue;Lrw;Lro;Lsl;)V

    iget-object v2, p0, Lsf;->i:[Lrx;

    aput-object v1, v2, v0

    invoke-virtual {v1}, Lrx;->start()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lsh;)V
    .locals 4

    iget-object v1, p0, Lsf;->b:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lsf;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsc;

    invoke-interface {p1, v0}, Lsh;->a(Lsc;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lsc;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lsf;->j:Lrq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsf;->j:Lrq;

    invoke-virtual {v0}, Lrq;->a()V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lsf;->i:[Lrx;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lsf;->i:[Lrx;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lsf;->i:[Lrx;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lrx;->a()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
