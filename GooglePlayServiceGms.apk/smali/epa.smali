.class public final Lepa;
.super Lbip;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lepb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lepb;)V
    .locals 0

    invoke-direct {p0}, Lbip;-><init>()V

    iput-object p1, p0, Lepa;->a:Landroid/content/Context;

    iput-object p2, p0, Lepa;->b:Lepb;

    return-void
.end method


# virtual methods
.method public final l(Lbjv;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    const-string v0, "Callbacks must not be null."

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-lez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Client version must be set to a positive value."

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "Calling package must be set."

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lepa;->a:Landroid/content/Context;

    invoke-static {v0, p3}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lepa;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lbbv;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    :cond_0
    const-string v0, "Arguments must not be null."

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lepa;->b:Lepb;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p1, v1, v0, v2}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
