.class public final Lblq;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/people/data/AudienceMember;)Lblr;
    .locals 5

    const v1, 0x7f02008c    # com.google.android.gms.R.drawable.common_ic_acl_circles

    const v2, 0x7f020086    # com.google.android.gms.R.drawable.common_acl_chip_green

    const v3, 0x7f020083    # com.google.android.gms.R.drawable.common_acl_chip_blue

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown audience member type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown circle type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    const v0, 0x7f0b045d    # com.google.android.gms.R.string.common_chips_label_public

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020091    # com.google.android.gms.R.drawable.common_ic_acl_public

    :goto_0
    new-instance v3, Lblr;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v0, v1, v4}, Lblr;-><init>(ILjava/lang/String;IB)V

    return-object v3

    :pswitch_3
    const v0, 0x7f0b045f    # com.google.android.gms.R.string.common_chips_label_extended_circles

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02008e    # com.google.android.gms.R.drawable.common_ic_acl_extended

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0b045e    # com.google.android.gms.R.string.common_chips_label_your_circles

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto :goto_0

    :pswitch_5
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02008d    # com.google.android.gms.R.drawable.common_ic_acl_domain

    goto :goto_0

    :pswitch_6
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto :goto_0

    :pswitch_7
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v2, 0x7f020089    # com.google.android.gms.R.drawable.common_acl_chip_grey

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020094    # com.google.android.gms.R.drawable.common_ic_email_black_24

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020090    # com.google.android.gms.R.drawable.common_ic_acl_person

    move v2, v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
