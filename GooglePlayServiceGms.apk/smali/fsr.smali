.class public final Lfsr;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static A:Lbfy;

.field public static B:Lbfy;

.field public static C:Lbfy;

.field public static D:Lbfy;

.field public static E:Lbfy;

.field public static F:Lbfy;

.field public static G:Lbfy;

.field public static H:Lbfy;

.field public static I:Lbfy;

.field public static J:Lbfy;

.field public static K:Lbfy;

.field public static L:Lbfy;

.field public static M:Lbfy;

.field public static N:Lbfy;

.field public static O:Lbfy;

.field public static P:Lbfy;

.field public static Q:Lbfy;

.field public static R:Lbfy;

.field public static S:Lbfy;

.field public static T:Lbfy;

.field public static U:Lbfy;

.field public static V:Lbfy;

.field public static W:Lbfy;

.field public static X:Lbfy;

.field public static Y:Lbfy;

.field public static Z:Lbfy;

.field public static a:Lbfy;

.field public static aa:Lbfy;

.field public static ab:Lbfy;

.field public static ac:Lbfy;

.field public static ad:Lbfy;

.field public static ae:Lbfy;

.field public static af:Lbfy;

.field public static ag:Lbfy;

.field public static b:Lbfy;

.field public static c:Lbfy;

.field public static d:Lbfy;

.field public static e:Lbfy;

.field public static f:Lbfy;

.field public static g:Lbfy;

.field public static h:Lbfy;

.field public static i:Lbfy;

.field public static j:Lbfy;

.field public static k:Lbfy;

.field public static l:Lbfy;

.field public static m:Lbfy;

.field public static n:Lbfy;

.field public static o:Lbfy;

.field public static p:Lbfy;

.field public static q:Lbfy;

.field public static r:Lbfy;

.field public static s:Lbfy;

.field public static t:Lbfy;

.field public static u:Lbfy;

.field public static v:Lbfy;

.field public static w:Lbfy;

.field public static x:Lbfy;

.field public static y:Lbfy;

.field public static z:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x64

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v0, "plus.facl.max_age"

    const v1, 0xa4cb800

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->a:Lbfy;

    const-string v0, "plus.cache_enabled"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->b:Lbfy;

    const-string v0, "plus.verbose_logging"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->c:Lbfy;

    const-string v0, "plus.apiary_trace"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->d:Lbfy;

    const-string v0, "plus.pos_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->e:Lbfy;

    const-string v0, "plus.pos_server_api_path"

    const-string v1, "/pos/v1/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->f:Lbfy;

    const-string v0, "plus.pos_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->g:Lbfy;

    const-string v0, "plus.pos_anonymous_api_key"

    const-string v1, "AIzaSyBa9bgzwtnGchlkux96-c5Q_fi19fE1pEA"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->h:Lbfy;

    const-string v0, "plus.whitelisted_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->i:Lbfy;

    const-string v0, "plus.whitelisted_server_api_path"

    const-string v1, "/plus/v1whitelisted/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->j:Lbfy;

    const-string v0, "plus.whitelisted_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->k:Lbfy;

    const-string v0, "plus.v1_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->l:Lbfy;

    const-string v0, "plus.v1_server_api_path"

    const-string v1, "/plus/v1/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->m:Lbfy;

    const-string v0, "plus.v1_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->n:Lbfy;

    const-string v0, "plus.oauth_server_url"

    const-string v1, "https://accounts.google.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->o:Lbfy;

    const-string v0, "plus.oauth_server_api_path"

    const-string v1, "/o/oauth2"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->p:Lbfy;

    const-string v0, "plus.oauth_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->q:Lbfy;

    const-string v0, "plus.connect_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->r:Lbfy;

    const-string v0, "plus.connect_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->s:Lbfy;

    const-string v0, "plus.connect_server_api_path"

    const-string v1, "/plus/v1whitelisted/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->t:Lbfy;

    const-string v0, "plus.connect_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->u:Lbfy;

    const-string v0, "plus.lso_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->v:Lbfy;

    const-string v0, "plus.lso_server_api_path"

    const-string v1, "/oauth2/v3/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->w:Lbfy;

    const-string v0, "plus.lso_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->x:Lbfy;

    const-string v0, "plus.gcm.sender_id"

    const-string v1, "745476177629"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->y:Lbfy;

    const-string v0, "plus.list_apps.learn_more_url"

    const-string v1, "https://support.google.com/plus/?p=plus_sign_in"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->z:Lbfy;

    const-string v0, "plus.list_apps.help_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->A:Lbfy;

    const-string v0, "plus.list_apps_lso._help_url"

    const-string v1, "https://support.google.com/accounts/answer/3290768"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->B:Lbfy;

    const-string v0, "plus.list_moments.help_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->C:Lbfy;

    const-string v0, "plus.moments.manage_web_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->D:Lbfy;

    const-string v0, "plus.list_apps.max_items"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->E:Lbfy;

    const-string v0, "plus.list_moments.max_items"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->F:Lbfy;

    const-string v0, "plus.post_max_individual_acls"

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->G:Lbfy;

    const-string v0, "plus.lso_aspen_scope_ids"

    const-string v1, "9335"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->H:Lbfy;

    const-string v0, "plus.list_apps.disabled_versions"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->I:Lbfy;

    const-string v0, "plus.list_apps.disabled_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->J:Lbfy;

    const-string v0, "plus.list_moments.disabled_versions"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->K:Lbfy;

    const-string v0, "plus.list_moments.disabled_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->L:Lbfy;

    const-string v0, "plus.manage_app.disabled_versions"

    const-string v1, ""

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->M:Lbfy;

    const-string v0, "plus.manage_app.disabled_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->N:Lbfy;

    const-string v0, "plus.oob_debugging"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->O:Lbfy;

    const-string v0, "plus.oob_default_birthday"

    const-string v1, "1940-01-01"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->P:Lbfy;

    const-string v0, "plus.oob_last_name_first_countries"

    const-string v1, "*ja*ko*hu*zh*"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->Q:Lbfy;

    const-string v0, "plus.share_box.max_people"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->R:Lbfy;

    const-string v0, "plus.share_box.add_to_circle_enabled"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->S:Lbfy;

    const-string v0, "plus.share_box.add_to_circle_default"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->T:Lbfy;

    const-string v0, "plus.share_box.show_acl_picker_first"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->U:Lbfy;

    const-string v0, "plus.share_box.showcased_suggestion_count"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->V:Lbfy;

    const-string v0, "plus.share_box.suggestion_count"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->W:Lbfy;

    const-string v0, "plus.share_box.client_suggestion_count"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->X:Lbfy;

    const-string v0, "plus.share_box.show_suggested_default"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->Y:Lbfy;

    const-string v0, "plus.share_box.include_suggestions_default"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->Z:Lbfy;

    const-string v0, "plus.share_box.show_add_to_circle_default"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->aa:Lbfy;

    const-string v0, "plus.share_box.add_to_circle_max_name_length"

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->ab:Lbfy;

    const-string v0, "plus.share_box.people_sync_allowance_seconds"

    const-wide/16 v1, 0x7080

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->ac:Lbfy;

    const-string v0, "plus.share_box.people_sync_allowance_seconds_sign_in"

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->ad:Lbfy;

    const-string v0, "plus.server.timeout_ms.default"

    const/16 v1, 0x9c4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->ae:Lbfy;

    const-string v0, "plus.write_moments.online_disabled"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->af:Lbfy;

    const-string v0, "location.help_url"

    const-string v1, "http://www.google.com/support/mobile/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfsr;->ag:Lbfy;

    return-void
.end method

.method public static a(ILjava/lang/String;)I
    .locals 5

    const/4 v1, -0x1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v0, v3

    invoke-static {v2, p0, v3, v0, v1}, Lfsr;->a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;II)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_0

    array-length v0, v3

    invoke-static {v2, p0, v3, v0}, Lfsr;->a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;I)I

    move-result v0

    if-gtz v0, :cond_0

    array-length v1, v3

    :goto_0
    if-lez v1, :cond_0

    add-int/lit8 v0, v1, -0x1

    const-string v4, "*"

    aput-object v4, v3, v0

    invoke-static {v2, p0, v3, v1}, Lfsr;->a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;I)I

    move-result v0

    if-gtz v0, :cond_0

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    :goto_1
    return v0

    :cond_1
    sget-object v0, Lfsr;->ae:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1
.end method

.method private static a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;I)I
    .locals 4

    const/4 v2, -0x1

    add-int/lit8 v0, p3, -0x1

    move v1, v0

    move v0, v2

    :goto_0
    if-ltz v1, :cond_0

    invoke-static {p0, p1, p2, p3, v1}, Lfsr;->a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;II)V

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_0

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private static a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;II)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v1, "plus.server.timeout_ms."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    packed-switch p1, :pswitch_data_0

    :goto_0
    if-ge v0, p3, :cond_1

    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    if-ne v0, p4, :cond_0

    const/16 v1, 0x2a

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    const-string v1, "DEPRECATED_GET_OR_POST."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const-string v1, "GET."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_2
    const-string v1, "POST."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_3
    const-string v1, "PUT."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_4
    const-string v1, "DELETE."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    aget-object v1, p2, v0

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
