.class final Lfdd;
.super Lfbx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lfch;

.field private final b:Lfay;


# direct methods
.method public constructor <init>(Lfch;Lfay;)V
    .locals 0

    iput-object p1, p0, Lfdd;->a:Lfch;

    invoke-direct {p0}, Lfbx;-><init>()V

    iput-object p2, p0, Lfdd;->b:Lfay;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 7

    const/4 v5, 0x0

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "People callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1, p2}, Lfch;->a(ILandroid/os/Bundle;)Lbbo;

    move-result-object v3

    if-nez p3, :cond_1

    move-object v4, v5

    :goto_0
    if-nez p3, :cond_2

    :goto_1
    iget-object v6, p0, Lfdd;->a:Lfch;

    new-instance v0, Lfdj;

    iget-object v1, p0, Lfdd;->a:Lfch;

    iget-object v2, p0, Lfdd;->b:Lfay;

    invoke-direct/range {v0 .. v5}, Lfdj;-><init>(Lfch;Lfay;Lbbo;Lfef;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Lfch;->b(Lbjg;)V

    return-void

    :cond_1
    new-instance v4, Lfef;

    invoke-direct {v4, p3}, Lfef;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p3}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pageToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method
