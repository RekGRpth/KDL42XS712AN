.class public final Leqi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lbdm;

.field public static b:Lepy;

.field public static c:Leqb;

.field private static final d:Lbdo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Leqj;

    invoke-direct {v0}, Leqj;-><init>()V

    sput-object v0, Leqi;->d:Lbdo;

    new-instance v0, Lbdm;

    sget-object v1, Leqi;->d:Lbdo;

    const/4 v2, 0x0

    new-array v2, v2, [Lbem;

    invoke-direct {v0, v1, v2}, Lbdm;-><init>(Lbdo;[Lbem;)V

    sput-object v0, Leqi;->a:Lbdm;

    new-instance v0, Leqm;

    invoke-direct {v0}, Leqm;-><init>()V

    sput-object v0, Leqi;->b:Lepy;

    new-instance v0, Leqq;

    invoke-direct {v0}, Leqq;-><init>()V

    sput-object v0, Leqi;->c:Leqb;

    return-void
.end method

.method static synthetic a()Lbdo;
    .locals 1

    sget-object v0, Leqi;->d:Lbdo;

    return-object v0
.end method

.method public static a(Lbdu;)Lera;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "GoogleApiClient parameter is required."

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    sget-object v0, Leqi;->d:Lbdo;

    invoke-interface {p0, v0}, Lbdu;->a(Lbdo;)Lbdn;

    move-result-object v0

    check-cast v0, Lera;

    if-eqz v0, :cond_1

    :goto_1
    const-string v2, "GoogleApiClient is not configured to use the LocationServices.API Api. Pass thisinto GoogleApiClient.Builder#addApi() to use this feature."

    invoke-static {v1, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
