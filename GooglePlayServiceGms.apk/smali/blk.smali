.class public final Lblk;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Lbln;

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:I

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizk;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lblk;->a:Lbln;

    const-string v0, ""

    iput-object v0, p0, Lblk;->c:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lblk;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lblk;->g:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lblk;->g:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lblk;->b()I

    :cond_0
    iget v0, p0, Lblk;->g:I

    return v0
.end method

.method public final a(I)Lblk;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lblk;->d:Z

    iput p1, p0, Lblk;->e:I

    return-object p0
.end method

.method public final a(Lbln;)Lblk;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lblk;->f:Z

    iput-object p1, p0, Lblk;->a:Lbln;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lblk;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lblk;->b:Z

    iput-object p1, p0, Lblk;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lbln;

    invoke-direct {v0}, Lbln;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lblk;->a(Lbln;)Lblk;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lblk;->a(Ljava/lang/String;)Lblk;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lblk;->a(I)Lblk;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Lblk;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lblk;->a:Lbln;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-boolean v0, p0, Lblk;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lblk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lblk;->d:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lblk;->e:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lblk;->f:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lblk;->a:Lbln;

    invoke-static {v0, v1}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lblk;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lblk;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lblk;->d:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lblk;->e:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lblk;->g:I

    return v0
.end method
