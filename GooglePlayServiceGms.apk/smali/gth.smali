.class public final Lgth;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "([\\w]{1,4})?([\\w]{1,6})?([\\w]{1,5})?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgth;->a:Ljava/util/regex/Pattern;

    const-string v0, "((?:[\\w]{4})|(?:[\\w]{1,4}$))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgth;->b:Ljava/util/regex/Pattern;

    const-string v0, "[^\\d]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgth;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static a(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgth;->e(I)I

    move-result v0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x4

    invoke-static {v0}, Lgth;->e(I)I

    move-result v0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    invoke-static {v0}, Lgth;->e(I)I

    move-result v0

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    invoke-static {v0}, Lgth;->e(I)I

    move-result v0

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x1b

    invoke-static {v0}, Lgth;->e(I)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public static a(III)I
    .locals 6

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    if-lez p0, :cond_0

    const/16 v4, 0xc

    if-le p0, v4, :cond_2

    :cond_0
    const/4 v0, 0x2

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {v3, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    if-ge p1, v4, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/16 v4, 0xb

    if-le p0, v4, :cond_4

    add-int/lit8 p1, p1, 0x1

    move p0, v0

    :cond_4
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, p1, p0, v2}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4, v3}, Ljava/util/GregorianCalendar;->compareTo(Ljava/util/Calendar;)I

    move-result v5

    if-gtz v5, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    if-lez p2, :cond_1

    invoke-virtual {v3, v2, p2}, Ljava/util/GregorianCalendar;->roll(II)V

    invoke-virtual {v4, v3}, Ljava/util/GregorianCalendar;->compareTo(Ljava/util/Calendar;)I

    move-result v1

    if-lez v1, :cond_1

    move v0, v2

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 5

    const/4 v1, 0x3

    const/4 v0, 0x0

    const/4 v2, 0x4

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v3, "4"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "51"

    const-string v4, "55"

    invoke-static {p0, v3, v4}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const-string v3, "34"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const-string v3, "37"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    const-string v1, "3528"

    const-string v3, "358"

    invoke-static {p0, v1, v3}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x5

    goto :goto_0

    :cond_6
    const-string v1, "60110"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v0, v2

    goto :goto_0

    :cond_7
    const-string v1, "60112"

    const-string v3, "60114"

    invoke-static {p0, v1, v3}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    move v0, v2

    goto :goto_0

    :cond_8
    const-string v1, "601174"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    move v0, v2

    goto :goto_0

    :cond_9
    const-string v1, "601177"

    const-string v3, "601179"

    invoke-static {p0, v1, v3}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    move v0, v2

    goto :goto_0

    :cond_a
    const-string v1, "601186"

    const-string v3, "60119"

    invoke-static {p0, v1, v3}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    move v0, v2

    goto :goto_0

    :cond_b
    const-string v1, "644"

    const-string v3, "65"

    invoke-static {p0, v1, v3}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lioh;
    .locals 3

    :try_start_0
    new-instance v0, Lioh;

    invoke-direct {v0}, Lioh;-><init>()V

    invoke-static {p1}, Lgth;->e(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lioh;->a:J

    iput-object p0, v0, Lioh;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Liot;)Lioj;
    .locals 2

    iget-object v0, p0, Liot;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Liot;->a:[Lioj;

    iget-object v1, p0, Liot;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lgth;->a([Lioj;Ljava/lang/String;)Lioj;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lgth;->d(Lioj;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Liot;->a:[Lioj;

    invoke-static {v0}, Lgth;->a([Lioj;)Lioj;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Lioj;)Lioj;
    .locals 7

    const/4 v2, 0x0

    array-length v4, p0

    const/4 v0, 0x0

    move v3, v0

    move-object v0, v2

    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, p0, v3

    invoke-static {v1}, Lgth;->d(Lioj;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-nez v0, :cond_0

    move-object v0, v1

    :cond_0
    iget-boolean v5, v1, Lioj;->f:Z

    if-eqz v5, :cond_1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    return-object v0

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v1, v0

    move-object v0, v2

    goto :goto_1
.end method

.method public static a([Lioj;Lioj;)Lioj;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lioj;->a:Ljava/lang/String;

    invoke-static {p0, v0}, Lgth;->a([Lioj;Ljava/lang/String;)Lioj;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Lioj;Lioj;Lbpj;)Lioj;
    .locals 9

    const/4 v4, 0x0

    const/4 v3, 0x0

    if-nez p1, :cond_1

    :cond_0
    return-object v3

    :cond_1
    invoke-static {p1}, Lgti;->a(Lioj;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v6, v3

    :goto_0
    if-eqz v6, :cond_0

    const/high16 v1, -0x80000000

    array-length v7, p0

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_0

    aget-object v2, p0, v5

    invoke-static {v2}, Lgti;->a(Lioj;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, v6, Lgti;->b:I

    iget v8, v2, Lioj;->c:I

    if-ne v0, v8, :cond_3

    iget-object v0, v6, Lgti;->a:Ljava/lang/String;

    iget-object v8, v2, Lioj;->m:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    invoke-interface {p2, v2}, Lbpj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v0, v1, :cond_4

    move-object v1, v2

    :goto_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v3, v1

    move v1, v0

    goto :goto_1

    :cond_2
    new-instance v0, Lgti;

    iget-object v1, p1, Lioj;->m:Ljava/lang/String;

    iget v2, p1, Lioj;->c:I

    invoke-direct {v0, v1, v2}, Lgti;-><init>(Ljava/lang/String;I)V

    move-object v6, v0

    goto :goto_0

    :cond_3
    move v0, v4

    goto :goto_2

    :cond_4
    move v0, v1

    move-object v1, v3

    goto :goto_3
.end method

.method public static a([Lioj;Ljava/lang/String;)Lioj;
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    array-length v3, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, p0, v2

    iget-object v4, v1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static a([Lion;Ljava/lang/String;)Lion;
    .locals 5

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    array-length v3, p0

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v0, p0, v2

    iget-object v4, v0, Lion;->c:Liob;

    iget-object v4, v4, Liob;->a:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static a([Lipv;)Lipv;
    .locals 7

    const/4 v2, 0x0

    array-length v4, p0

    const/4 v0, 0x0

    move v3, v0

    move-object v0, v2

    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, p0, v3

    iget-boolean v5, v1, Lipv;->g:Z

    if-eqz v5, :cond_1

    if-nez v0, :cond_0

    move-object v0, v1

    :cond_0
    iget-boolean v5, v1, Lipv;->h:Z

    if-eqz v5, :cond_1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    return-object v0

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v1, v0

    move-object v0, v2

    goto :goto_1
.end method

.method public static a([Lipv;Lipv;)Lipv;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lipv;->b:Ljava/lang/String;

    invoke-static {p0, v0}, Lgth;->a([Lipv;Ljava/lang/String;)Lipv;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Lipv;Ljava/lang/String;)Lipv;
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    array-length v3, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, p0, v2

    iget-object v4, v1, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lioj;ZZZ[I[I)Ljava/lang/String;
    .locals 6

    const v5, 0x7f0b016d    # com.google.android.gms.R.string.wallet_invalid

    const/4 v4, 0x2

    iget v0, p1, Lioj;->l:I

    iget v1, p1, Lioj;->c:I

    invoke-static {p6, v0}, Lboz;->a([II)Z

    move-result v2

    if-eqz v2, :cond_0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected instrument category: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const v0, 0x7f0b00f4    # com.google.android.gms.R.string.wallet_cannot_use_prepaid

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    const v0, 0x7f0b00f5    # com.google.android.gms.R.string.wallet_cannot_use_debit

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {p5, v1}, Lboz;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    packed-switch v1, :pswitch_data_1

    const v0, 0x7f0b00f3    # com.google.android.gms.R.string.wallet_cannot_use_card_default

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b00f2    # com.google.android.gms.R.string.wallet_cannot_use_card_amex

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v0, p1, Lioj;->h:I

    if-ne v0, v4, :cond_2

    const v0, 0x7f0b016e    # com.google.android.gms.R.string.wallet_declined

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lioj;->g:[I

    array-length v0, v0

    if-eqz v0, :cond_6

    iget-object v1, p1, Lioj;->g:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget v3, v1, v0

    if-eq v3, v4, :cond_3

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    if-eqz p2, :cond_5

    const v0, 0x7f0b016b    # com.google.android.gms.R.string.wallet_expired_editable

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const v0, 0x7f0b016c    # com.google.android.gms.R.string.wallet_expired

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    if-eqz p3, :cond_7

    iget-object v0, p1, Lioj;->e:Lipv;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lioj;->e:Lipv;

    iget-boolean v0, v0, Lipv;->f:Z

    if-eqz v0, :cond_7

    const v0, 0x7f0b0173    # com.google.android.gms.R.string.wallet_min_address_editable

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    if-eqz p4, :cond_8

    iget-object v0, p1, Lioj;->e:Lipv;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lioj;->e:Lipv;

    invoke-static {v0}, Lgth;->a(Lipv;)Z

    move-result v0

    if-nez v0, :cond_8

    const v0, 0x7f0b0171    # com.google.android.gms.R.string.wallet_missing_phone_editable

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_8
    iget v0, p1, Lioj;->h:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_9

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    const-string v0, ""

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/res/Resources;I[I)Ljava/lang/String;
    .locals 1

    invoke-static {p2, p1}, Lboz;->a([II)Z

    move-result v0

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f0b00f4    # com.google.android.gms.R.string.wallet_cannot_use_prepaid

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b00f5    # com.google.android.gms.R.string.wallet_cannot_use_debit

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lioh;)Ljava/lang/String;
    .locals 4

    invoke-static {}, Lgtd;->a()Lgte;

    move-result-object v0

    invoke-virtual {v0}, Lgte;->a()Lgtd;

    move-result-object v0

    iget-wide v1, p0, Lioh;->a:J

    const/4 v3, 0x6

    invoke-static {v1, v2, v3}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v1

    iget-object v2, p0, Lioh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgtd;->a(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lioj;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lgth;->a(Lioj;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lioj;Z)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lioj;->b:Ljava/lang/String;

    iget v1, p0, Lioj;->d:I

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0, p1}, Lgth;->b(Lioj;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lioj;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static a(J)Ljava/math/BigDecimal;
    .locals 1

    const/4 v0, 0x6

    invoke-static {p0, p1, v0}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public static a([Lipj;)Ljava/util/ArrayList;
    .locals 8

    const/4 v1, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v4, p0

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v5, p0, v3

    if-nez v5, :cond_1

    move-object v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    iget-object v6, v5, Lipj;->a:Ljava/lang/String;

    iget-object v7, v5, Lipj;->b:Ljava/lang/String;

    iget-object v5, v5, Lipj;->c:[Ljava/lang/String;

    invoke-direct {v0, v6, v7, v5}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method

.method public static a(Liob;)V
    .locals 11

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    iget-object v4, p0, Liob;->b:[Liod;

    array-length v5, v4

    const/4 v0, 0x0

    move v8, v0

    move-object v0, v1

    move-wide v9, v2

    move-wide v1, v9

    move v3, v8

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    iget-object v7, v6, Liod;->d:Lioh;

    if-eqz v7, :cond_1

    iget-object v0, v6, Liod;->d:Lioh;

    iget-object v0, v0, Lioh;->b:Ljava/lang/String;

    iget-object v6, v6, Liod;->d:Lioh;

    iget-wide v6, v6, Lioh;->a:J

    add-long/2addr v1, v6

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v7, v6, Liod;->c:Lioh;

    if-eqz v7, :cond_0

    iget-object v0, v6, Liod;->c:Lioh;

    iget-object v0, v0, Lioh;->b:Ljava/lang/String;

    iget-object v6, v6, Liod;->c:Lioh;

    iget-wide v6, v6, Lioh;->a:J

    add-long/2addr v1, v6

    goto :goto_1

    :cond_2
    iget-object v3, p0, Liob;->g:Lipr;

    if-eqz v3, :cond_3

    iget-object v3, p0, Liob;->g:Lipr;

    iget-object v3, v3, Lipr;->a:Lioh;

    if-eqz v3, :cond_3

    iget-object v0, p0, Liob;->g:Lipr;

    iget-object v0, v0, Lipr;->a:Lioh;

    iget-wide v3, v0, Lioh;->a:J

    add-long/2addr v1, v3

    iget-object v0, p0, Liob;->g:Lipr;

    iget-object v0, v0, Lipr;->a:Lioh;

    iget-object v0, v0, Lioh;->b:Ljava/lang/String;

    :cond_3
    iget-object v3, p0, Liob;->f:Lipt;

    if-eqz v3, :cond_4

    iget-object v3, p0, Liob;->f:Lipt;

    iget-object v3, v3, Lipt;->a:Lioh;

    if-eqz v3, :cond_4

    iget-object v0, p0, Liob;->f:Lipt;

    iget-object v0, v0, Lipt;->a:Lioh;

    iget-wide v3, v0, Lioh;->a:J

    add-long/2addr v1, v3

    iget-object v0, p0, Liob;->f:Lipt;

    iget-object v0, v0, Lipt;->a:Lioh;

    iget-object v0, v0, Lioh;->b:Ljava/lang/String;

    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v3, Lioh;

    invoke-direct {v3}, Lioh;-><init>()V

    iput-object v3, p0, Liob;->e:Lioh;

    iget-object v3, p0, Liob;->e:Lioh;

    iput-wide v1, v3, Lioh;->a:J

    iget-object v1, p0, Liob;->e:Lioh;

    iput-object v0, v1, Lioh;->b:Ljava/lang/String;

    :cond_5
    return-void
.end method

.method public static a(Landroid/widget/ImageView;Lioj;Lhgu;)Z
    .locals 7

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v0, "imageView is required"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_1

    iget v0, p1, Lioj;->c:I

    invoke-static {v0}, Lgth;->e(I)I

    move-result v1

    :goto_0
    if-eqz p1, :cond_2

    iget-object v0, p1, Lioj;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lioj;->i:Ljava/lang/String;

    move-object v4, v0

    :goto_1
    if-eqz v4, :cond_4

    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v5

    if-eqz v1, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "=w"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-h"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "-n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lhgw;

    invoke-direct {v5}, Lhgw;-><init>()V

    new-instance v6, Lhgx;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v6, v5, v3}, Lhgx;-><init>(Lhgw;B)V

    iget-object v3, v6, Lhgx;->a:Lhgw;

    iput-object p0, v3, Lhgw;->c:Landroid/widget/ImageView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v6, Lhgx;->a:Lhgw;

    iput-object v0, v3, Lhgw;->a:Ljava/lang/String;

    iget-object v0, v6, Lhgx;->a:Lhgw;

    iput-boolean v2, v0, Lhgw;->b:Z

    iget-object v0, v6, Lhgx;->a:Lhgw;

    iput v1, v0, Lhgw;->d:I

    iget-object v0, v6, Lhgx;->a:Lhgw;

    iget-object v0, v0, Lhgw;->a:Ljava/lang/String;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v6, Lhgx;->a:Lhgw;

    iget-object v0, v0, Lhgw;->c:Landroid/widget/ImageView;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v6, Lhgx;->a:Lhgw;

    if-nez p2, :cond_0

    new-instance p2, Lhgu;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Lhgu;-><init>(Landroid/content/Context;)V

    :cond_0
    invoke-virtual {p2, v0}, Lhgu;->a(Lhgw;)V

    move v0, v2

    :goto_3
    return v0

    :cond_1
    move v1, v3

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    move-object v4, v0

    goto/16 :goto_1

    :cond_3
    const v0, 0x7f020251    # com.google.android.gms.R.drawable.wallet_card_full_visa

    goto/16 :goto_2

    :cond_4
    invoke-static {p0}, Lhha;->a(Landroid/widget/ImageView;)Lhhc;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v2}, Lhhc;->cancel(Z)Z

    :cond_5
    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    if-eqz v1, :cond_6

    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v3

    goto :goto_3
.end method

.method public static a(Lioj;[I[I)Z
    .locals 3

    const/4 v2, 0x3

    const/4 v0, 0x1

    iget v1, p0, Lioj;->l:I

    invoke-static {p2, v1}, Lboz;->a([II)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lioj;->c:I

    if-ne v1, v2, :cond_2

    invoke-static {p1, v2}, Lboz;->a([II)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lipv;)Z
    .locals 1

    if-eqz p0, :cond_0

    iget-object v0, p0, Lipv;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v2, :cond_1

    if-ge v1, v3, :cond_1

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a([Landroid/accounts/Account;)[Lguu;
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    array-length v3, p0

    new-array v1, v3, [Lguu;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    new-instance v4, Lguu;

    aget-object v5, p0, v2

    invoke-direct {v4, v5, v0}, Lguu;-><init>(Landroid/accounts/Account;Ljava/lang/String;)V

    aput-object v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a([Ljbe;)[Ljava/lang/String;
    .locals 5

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, p0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, p0, v0

    iget-object v4, v3, Ljbe;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v3, v3, Ljbe;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/16 v0, 0xc

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0xf

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x10

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lioh;)Ljava/lang/String;
    .locals 6

    invoke-static {p0}, Lgth;->d(Lioh;)I

    move-result v0

    if-gtz v0, :cond_0

    const-string v0, "\\d*"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/text/DecimalFormatSymbols;->getInstance()Ljava/text/DecimalFormatSymbols;

    move-result-object v1

    invoke-virtual {v1}, Ljava/text/DecimalFormatSymbols;->getMonetaryDecimalSeparator()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "\\d*(%s\\d{0,%d})?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lioj;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lgth;->a(Lioj;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lioj;Z)Ljava/lang/String;
    .locals 1

    iget v0, p0, Lioj;->c:I

    sparse-switch v0, :sswitch_data_0

    const-string v0, "XXX"

    :goto_0
    return-object v0

    :sswitch_0
    if-eqz p1, :cond_0

    const-string v0, "VISA"

    goto :goto_0

    :cond_0
    const-string v0, "Visa"

    goto :goto_0

    :sswitch_1
    if-eqz p1, :cond_1

    const-string v0, "AMEX"

    goto :goto_0

    :cond_1
    const-string v0, "Amex"

    goto :goto_0

    :sswitch_2
    if-eqz p1, :cond_2

    const-string v0, "MASTERCARD"

    goto :goto_0

    :cond_2
    const-string v0, "MasterCard"

    goto :goto_0

    :sswitch_3
    if-eqz p1, :cond_3

    const-string v0, "DISCOVER"

    goto :goto_0

    :cond_3
    const-string v0, "Discover"

    goto :goto_0

    :sswitch_4
    const-string v0, "JCB"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_3
        0x1b -> :sswitch_4
    .end sparse-switch
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    sget-object v0, Lgzp;->f:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<a href=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lipv;)Z
    .locals 1

    iget-boolean v0, p0, Lipv;->g:Z

    return v0
.end method

.method public static c(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/16 v0, 0x13

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0xf

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x10

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static c(Lioj;)Ljah;
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lioj;->d:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    new-instance v0, Ljah;

    invoke-direct {v0}, Ljah;-><init>()V

    invoke-static {p0, v1}, Lgth;->b(Lioj;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ljah;->a:Ljava/lang/String;

    iget-object v1, p0, Lioj;->m:Ljava/lang/String;

    iput-object v1, v0, Ljah;->b:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lioh;)Ljava/lang/String;
    .locals 6

    invoke-static {p0}, Lgth;->d(Lioh;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/text/DecimalFormatSymbols;->getInstance()Ljava/text/DecimalFormatSymbols;

    move-result-object v1

    invoke-virtual {v1}, Ljava/text/DecimalFormatSymbols;->getMonetaryDecimalSeparator()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "\\d*%s\\d{%d}"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lgth;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lipv;)Z
    .locals 1

    iget-boolean v0, p0, Lipv;->f:Z

    return v0
.end method

.method public static d(I)I
    .locals 1

    const/4 v0, 0x4

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static d(Lioh;)I
    .locals 3

    const/4 v0, 0x0

    iget-wide v1, p0, Lioh;->a:J

    long-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->log10(D)D

    move-result-wide v1

    double-to-int v1, v1

    rsub-int/lit8 v1, v1, 0x6

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/16 v4, 0xf

    const/4 v1, 0x0

    invoke-static {p0}, Lgth;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->a(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_0

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lgth;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_3
    invoke-static {v0}, Lgth;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static d(Lioj;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lioj;->g:[I

    array-length v1, v1

    if-nez v1, :cond_0

    iget v1, p0, Lioj;->h:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(I)I
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const v0, 0x7f020251    # com.google.android.gms.R.drawable.wallet_card_full_visa

    goto :goto_0

    :sswitch_1
    const v0, 0x7f02024d    # com.google.android.gms.R.drawable.wallet_card_full_discover

    goto :goto_0

    :sswitch_2
    const v0, 0x7f02024c    # com.google.android.gms.R.drawable.wallet_card_full_amex

    goto :goto_0

    :sswitch_3
    const v0, 0x7f02024f    # com.google.android.gms.R.drawable.wallet_card_full_mastercard

    goto :goto_0

    :sswitch_4
    const v0, 0x7f02024e    # com.google.android.gms.R.drawable.wallet_card_full_jcb

    goto :goto_0

    :sswitch_5
    const v0, 0x7f020250    # com.google.android.gms.R.drawable.wallet_card_full_play

    goto :goto_0

    :sswitch_6
    const v0, 0x7f020252    # com.google.android.gms.R.drawable.wallet_card_full_wallet

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0x4 -> :sswitch_1
        0x1b -> :sswitch_4
        0x20 -> :sswitch_6
        0x21 -> :sswitch_5
    .end sparse-switch
.end method

.method public static e(Ljava/lang/String;)J
    .locals 4

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v1

    instance-of v0, v1, Ljava/text/DecimalFormat;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/text/DecimalFormat;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setParseBigDecimal(Z)V

    :cond_0
    invoke-virtual {v1, p0}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    instance-of v1, v0, Ljava/math/BigDecimal;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/math/BigDecimal;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->scaleByPowerOfTen(I)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static e(Lioj;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v0, 0x0

    invoke-static {p0}, Lgth;->d(Lioj;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lioj;->h:I

    if-eq v1, v5, :cond_0

    iget-object v1, p0, Lioj;->g:[I

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v2, p0, Lioj;->g:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget v4, v2, v1

    if-ne v4, v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static f(Lioj;)Z
    .locals 2

    iget-object v0, p0, Lioj;->g:[I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lboz;->a([II)Z

    move-result v0

    return v0
.end method

.method public static f(Ljava/lang/String;)Z
    .locals 3

    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lgtd;->a()Lgte;

    move-result-object v1

    invoke-virtual {v1}, Lgte;->a()Lgtd;

    move-result-object v1

    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2, p0}, Lgtd;->a(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lioj;)Lioj;
    .locals 2

    const/4 v1, 0x2

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget v0, p0, Lioj;->h:I

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput v1, v0, Lioj;->h:I

    move-object p0, v0

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/16 v1, 0x10

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lgth;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
