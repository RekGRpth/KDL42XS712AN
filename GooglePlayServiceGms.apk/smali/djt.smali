.class public final Ldjt;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field private final a:Ldjs;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ldjs;Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x0

    if-nez p3, :cond_0

    move-object v0, v1

    :goto_0
    const/16 v2, 0x1fa

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object p1, p0, Ldjt;->a:Ldjs;

    iput-object p2, p0, Ldjt;->b:Landroid/content/Context;

    return-void

    :cond_0
    const-string v0, "games_%s.db"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DROP TABLE IF EXISTS achievement_definitions;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS achievement_instances;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS achievement_pending_ops;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS client_contexts;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS games;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS game_badges;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS game_instances;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS images;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS invitations;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS leaderboards;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS leaderboard_instances;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS leaderboard_pending_scores;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS leaderboard_scores;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS matches;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS matches_pending_ops;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS notifications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS players;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS participants;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS requests;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS request_recipients;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS featured_games;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS achievement_definitions_game_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS achievement_instances_definition_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS achievement_instances_player_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS achievement_pending_op_client_contexts_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS games_external_game_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS game_instances_game_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS invitations_external_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS leaderboards_game_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS leaderboard_instances_leaderboard_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS leaderboard_scores_instance_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS matches_external_match_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS matches_pending_op_client_contexts_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS participants_invitation_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS participants_match_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS participants_player_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS players_external_player_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS requests_sender_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS requests_participants_request_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE matches (_id INTEGER PRIMARY KEY AUTOINCREMENT,game_id INTEGER REFERENCES games(_id) NOT NULL,external_match_id TEXT NOT NULL,creator_external TEXT NOT NULL,creation_timestamp INTEGER NOT NULL,last_updater_external TEXT,last_updated_timestamp INTEGER,pending_participant_external TEXT,data BLOB,status INTEGER NOT NULL,description TEXT,version INTEGER NOT NULL,variant INTEGER NOT NULL DEFAULT -1,notification_text TEXT,user_match_status INTEGER NOT NULL,has_automatch_criteria INTEGER NOT NULL,automatch_min_players INTEGER,automatch_max_players INTEGER,automatch_bit_mask INTEGER,automatch_wait_estimate_sec INTEGER NOT NULL DEFAULT -1,rematch_id TEXT,match_number INTEGER NOT NULL DEFAULT 1,previous_match_data BLOB,upsync_required INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX matches_external_match_id_index ON matches (external_match_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE matches_pending_ops (_id INTEGER PRIMARY KEY AUTOINCREMENT,client_context_id INTEGER REFERENCES client_contexts(_id) NOT NULL,type INTEGER NOT NULL,external_game_id TEXT NOT NULL,external_match_id TEXT,pending_participant_id TEXT,version INTEGER,is_turn INTEGER,results TEXT);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX matches_pending_op_client_contexts_id_index ON matches_pending_ops (client_context_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private static d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE participants (_id INTEGER PRIMARY KEY AUTOINCREMENT,match_id INTEGER REFERENCES matches(_id),invitation_id INTEGER REFERENCES invitations(_id),external_participant_id TEXT NOT NULL,player_id INTEGER REFERENCES players(_id),default_display_image_id INTEGER REFERENCES images(_id),default_display_name TEXT,player_status INTEGER NOT NULL,client_address TEXT,result_type INTEGER,placing INTEGER,connected INTEGER,capabilities INTEGER);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX participants_match_id_index ON participants (match_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX participants_invitation_id_index ON participants (invitation_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX participants_player_id_index ON participants (player_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private static e(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE requests (_id INTEGER PRIMARY KEY AUTOINCREMENT,external_request_id TEXT NOT NULL,game_id INTEGER REFERENCES games(_id) NOT NULL,sender_id INTEGER REFERENCES players(_id) NOT NULL,data BLOB,type INTEGER NOT NULL DEFAULT -1,creation_timestamp INTEGER NOT NULL DEFAULT 0,expiration_timestamp INTEGER NOT NULL DEFAULT 0,status INTEGER NOT NULL DEFAULT -1);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX requests_sender_id_index ON requests (sender_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE request_recipients (_id INTEGER PRIMARY KEY AUTOINCREMENT,request_id INTEGER REFERENCES requests(_id) NOT NULL,player_id INTEGER REFERENCES players(_id) NOT NULL,recipient_status INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX requests_participants_request_id_index ON request_recipients (request_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private f(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "ALTER TABLE matches ADD description_participant_id TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "matches"

    invoke-virtual {p1, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Ldjt;->a:Ldjs;

    iget-object v1, p0, Ldjt;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ldjs;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "match_sync_token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    const-string v0, "GamesDatabaseHelper"

    const-string v1, "Bootstrapping database version: 506"

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CREATE TABLE images (_id INTEGER PRIMARY KEY AUTOINCREMENT,url TEXT,local INTEGER NOT NULL,filesize INTEGER,download_timestamp INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE players (_id INTEGER PRIMARY KEY AUTOINCREMENT,external_player_id TEXT NOT NULL,profile_name TEXT NOT NULL,profile_icon_image_id INTEGER REFERENCES images(_id),profile_hi_res_image_id INTEGER REFERENCES images(_id),last_updated INTEGER NOT NULL,most_recent_game_id INTEGER REFERENCES games(_id),most_recent_activity_timestamp INTEGER NOT NULL DEFAULT -1,is_in_circles INTEGER NOT NULL DEFAULT -1);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX players_external_player_id_index ON players (external_player_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE games (_id INTEGER PRIMARY KEY AUTOINCREMENT,external_game_id TEXT NOT NULL,display_name TEXT NOT NULL,primary_category TEXT,secondary_category TEXT,developer_name TEXT,game_description TEXT,game_icon_image_id INTEGER REFERENCES images(_id),game_hi_res_image_id INTEGER REFERENCES images(_id),featured_image_id INTEGER REFERENCES images(_id),play_enabled_game INTEGER NOT NULL DEFAULT 0,last_played_server_time INTEGER,last_connection_local_time INTEGER NOT NULL DEFAULT 0,last_synced_local_time INTEGER NOT NULL DEFAULT 0,metadata_version INTEGER NOT NULL DEFAULT 0,metadata_sync_requested INTEGER NOT NULL DEFAULT 0,target_instance INTEGER REFERENCES game_instances(_id),gameplay_acl_status INTEGER NOT NULL DEFAULT 1,availability INTEGER NOT NULL DEFAULT 1,owned INTEGER NOT NULL DEFAULT 0,achievement_total_count INTEGER NOT NULL DEFAULT 0,leaderboard_count INTEGER NOT NULL DEFAULT 0,price_micros INTEGER,formatted_price TEXT,full_price_micros INTEGER,formatted_full_price TEXT,muted INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX games_external_game_id_index ON games (external_game_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE game_badges (_id INTEGER PRIMARY KEY AUTOINCREMENT,badge_game_id INTEGER REFERENCES games(_id) NOT NULL,badge_type INTEGER NOT NULL,badge_title STRING NOT NULL,badge_description STRING,badge_icon_image_id INTEGER REFERENCES images(_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX game_badges_game_id_index ON game_badges (badge_game_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE game_instances (_id INTEGER PRIMARY KEY AUTOINCREMENT,instance_game_id INTEGER REFERENCES games(_id) NOT NULL,real_time_support INTEGER NOT NULL DEFAULT 0,turn_based_support INTEGER NOT NULL DEFAULT 0,platform_type INTEGER NOT NULL,instance_display_name TEXT NOT NULL,package_name TEXT,piracy_check INTEGER NOT NULL DEFAULT 0,installed INTEGER NOT NULL DEFAULT 0,preferred INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX game_instances_game_id_index ON game_instances (instance_game_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE client_contexts (_id INTEGER PRIMARY KEY AUTOINCREMENT,package_name TEXT NOT NULL,package_uid INTEGER NOT NULL,account_name TEXT NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE achievement_definitions (_id INTEGER PRIMARY KEY AUTOINCREMENT,game_id INTEGER REFERENCES games(_id) NOT NULL,external_achievement_id TEXT NOT NULL,type INTEGER NOT NULL,name TEXT NOT NULL,description TEXT,unlocked_icon_image_id INTEGER REFERENCES images(_id),revealed_icon_image_id INTEGER REFERENCES images(_id),total_steps INTEGER,formatted_total_steps TEXT,initial_state INTEGER NOT NULL,sorting_rank INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX achievement_definitions_game_id_index ON achievement_definitions (game_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE achievement_pending_ops (_id INTEGER PRIMARY KEY AUTOINCREMENT,client_context_id INTEGER REFERENCES client_contexts(_id) NOT NULL,external_achievement_id TEXT NOT NULL,achievement_type INTEGER NOT NULL,new_state INTEGER,steps_to_increment INTEGER,min_steps_to_set INTEGER,external_game_id TEXT,external_player_id TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX achievement_pending_op_client_contexts_id_index ON achievement_pending_ops (client_context_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE achievement_instances (_id INTEGER PRIMARY KEY AUTOINCREMENT,definition_id INTEGER REFERENCES achievement_definitions(_id) NOT NULL,player_id INTEGER REFERENCES players(_id) NOT NULL,state INTEGER NOT NULL,current_steps INTEGER,formatted_current_steps TEXT,last_updated_timestamp INTEGER NOT NULL DEFAULT -1);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX achievement_instances_definition_id_index ON achievement_instances (definition_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX achievement_instances_player_id_index ON achievement_instances (player_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE invitations (_id INTEGER PRIMARY KEY AUTOINCREMENT,game_id INTEGER REFERENCES games(_id) NOT NULL,external_invitation_id TEXT NOT NULL,external_inviter_id TEXT NOT NULL,creation_timestamp INTEGER NOT NULL,last_modified_timestamp INTEGER NOT NULL,description TEXT,type INTEGER NOT NULL,variant INTEGER NOT NULL DEFAULT -1,has_automatch_criteria INTEGER NOT NULL DEFAULT 0,automatch_min_players INTEGER,automatch_max_players INTEGER,inviter_in_circles INTEGER NOT NULL DEFAULT 1);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX invitations_external_id_index ON invitations (external_invitation_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE leaderboards (_id INTEGER PRIMARY KEY AUTOINCREMENT,game_id INTEGER REFERENCES games(_id) NOT NULL,external_leaderboard_id TEXT NOT NULL,name TEXT NOT NULL,board_icon_image_id INTEGER REFERENCES images(_id),sorting_rank INTEGER,score_order INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX leaderboards_game_id_index ON leaderboards (game_id,external_leaderboard_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE leaderboard_instances (_id INTEGER PRIMARY KEY AUTOINCREMENT,leaderboard_id INTEGER REFERENCES leaderboards(_id) NOT NULL,timespan INTEGER NOT NULL,collection INTEGER NOT NULL,player_raw_score INTEGER,player_display_score TEXT,player_rank INTEGER,player_display_rank TEXT,player_score_tag TEXT,total_scores INTEGER,top_page_token_next STRING,window_page_token_prev STRING,window_page_token_next STRING);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX leaderboard_instances_leaderboard_id_index ON leaderboard_instances (leaderboard_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE leaderboard_scores (_id INTEGER PRIMARY KEY AUTOINCREMENT,instance_id INTEGER REFERENCES leaderboard_instances(_id) NOT NULL,page_type INTEGER NOT NULL DEFAULT 0,player_id INTEGER REFERENCES players(_id),default_display_name TEXT,default_display_image_id INTEGER REFERENCES images(_id),rank INTEGER NOT NULL,display_rank TEXT NOT NULL,raw_score INTEGER NOT NULL,display_score TEXT NOT NULL,achieved_timestamp INTEGER NOT NULL,score_tag TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX leaderboard_scores_instance_id_index ON leaderboard_scores (instance_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE leaderboard_pending_scores (_id INTEGER PRIMARY KEY AUTOINCREMENT,client_context_id INTEGER REFERENCES client_contexts(_id) NOT NULL,external_game_id TEXT NOT NULL,external_leaderboard_id TEXT NOT NULL,external_player_id TEXT NOT NULL,raw_score INTEGER NOT NULL,achieved_timestamp INTEGER NOT NULL,score_tag TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-direct {p0, p1}, Ldjt;->f(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p1}, Ldjt;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    const-string v0, "CREATE TABLE notifications (_id INTEGER PRIMARY KEY AUTOINCREMENT,notification_id TEXT,game_id INTEGER REFERENCES games(_id) NOT NULL,external_sub_id TEXT NOT NULL,type INTEGER NOT NULL,image_id INTEGER REFERENCES images(_id),ticker TEXT,title TEXT,text TEXT,coalesced_text TEXT,timestamp INTEGER NOT NULL DEFAULT -1,acknowledged INTEGER NOT NULL DEFAULT 0,alert_level INTEGER NOT NULL DEFAULT 1);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    const-string v0, "ALTER TABLE participants ADD default_display_hi_res_image_id INTEGER REFERENCES images(_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    const-string v0, "GamesDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Downgrading from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0, p1}, Ldjt;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8

    const/16 v0, 0xc7

    const/16 v1, 0xc6

    const/16 v2, 0xc5

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v3, 0xc4

    if-lt p2, v3, :cond_0

    const/16 v3, 0x258

    if-lt p3, v3, :cond_2

    :cond_0
    const-string v0, "GamesDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrading from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", all data will be wiped!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0, p1}, Ldjt;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v3, "GamesDatabaseHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Upgrading from version "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-ge p2, v2, :cond_1a

    const-string v3, "ALTER TABLE invitations ADD variant INTEGER NOT NULL DEFAULT -1;"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :goto_1
    if-ge v2, v1, :cond_19

    const-string v2, "ALTER TABLE game_instances ADD preferred INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :goto_2
    if-ge v1, v0, :cond_18

    const-string v1, "ALTER TABLE participants ADD capabilities INTEGER;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :goto_3
    const/16 v1, 0xc8

    if-ge v0, v1, :cond_3

    const-string v0, "ALTER TABLE notifications ADD notification_id TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0xc8

    :cond_3
    const/16 v1, 0xc9

    if-ge v0, v1, :cond_4

    const-string v0, "ALTER TABLE leaderboard_scores ADD score_tag TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE leaderboard_instances ADD player_score_tag TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE leaderboard_pending_scores ADD score_tag TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0xc9

    :cond_4
    const/16 v1, 0xca

    if-ge v0, v1, :cond_5

    const-string v0, "ALTER TABLE achievement_pending_ops ADD min_steps_to_set INTEGER;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0xca

    :cond_5
    const/16 v1, 0xcb

    if-ge v0, v1, :cond_6

    const-string v0, "DROP TABLE IF EXISTS featured_games"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0xcb

    :cond_6
    const/16 v1, 0x12c

    if-ge v0, v1, :cond_7

    const-string v0, "ALTER TABLE matches ADD automatch_wait_estimate_sec INTEGER NOT NULL DEFAULT -1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x12c

    :cond_7
    const/16 v1, 0x12d

    if-ge v0, v1, :cond_8

    const-string v0, "ALTER TABLE matches ADD rematch_id TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE matches ADD match_number INTEGER NOT NULL DEFAULT 1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE matches ADD previous_match_data BLOB;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x12d

    :cond_8
    const/16 v1, 0x12e

    if-ge v0, v1, :cond_9

    const/16 v0, 0x12e

    :cond_9
    const/16 v1, 0x12f

    if-ge v0, v1, :cond_a

    const-string v0, "DROP TABLE IF EXISTS matches;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS participants;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p1}, Ldjt;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x12f

    :cond_a
    const/16 v1, 0x130

    if-ge v0, v1, :cond_b

    const-string v0, "DROP TABLE IF EXISTS matches_pending_ops;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x130

    :cond_b
    const/16 v1, 0x131

    if-ge v0, v1, :cond_c

    const-string v0, "DROP TABLE IF EXISTS matches_pending_ops;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS matches_pending_op_client_contexts_id_index;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    const-string v0, "DROP TABLE IF EXISTS matches;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS participants;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Ldjt;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p1}, Ldjt;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x131

    :cond_c
    const/16 v1, 0x132

    if-ge v0, v1, :cond_d

    const-string v0, "ALTER TABLE achievement_pending_ops ADD %s TEXT;"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "external_game_id"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "external_player_id"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x132

    :cond_d
    const/16 v1, 0x133

    if-ge v0, v1, :cond_e

    const-string v0, "ALTER TABLE notifications ADD coalesced_text TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE notifications ADD timestamp INTEGER NOT NULL DEFAULT -1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x133

    :cond_e
    const/16 v1, 0x190

    if-ge v0, v1, :cond_f

    const-string v0, "ALTER TABLE participants ADD default_display_hi_res_image_id INTEGER REFERENCES images(_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x190

    :cond_f
    const/16 v1, 0x193

    if-ge v0, v1, :cond_10

    const-string v0, "ALTER TABLE players ADD is_in_circles INTEGER NOT NULL DEFAULT -1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x193

    :cond_10
    const/16 v1, 0x196

    if-ge v0, v1, :cond_11

    const-string v0, "ALTER TABLE invitations ADD has_automatch_criteria INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE invitations ADD automatch_min_players INTEGER;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE invitations ADD automatch_max_players INTEGER;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x196

    :cond_11
    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_12

    invoke-static {p1}, Ldjt;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x1f4

    :cond_12
    const/16 v1, 0x1f5

    if-ge v0, v1, :cond_13

    const-string v0, "ALTER TABLE invitations ADD inviter_in_circles INTEGER NOT NULL DEFAULT 1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x1f5

    :cond_13
    const/16 v1, 0x1f6

    if-ge v0, v1, :cond_14

    const-string v0, "ALTER TABLE notifications ADD alert_level INTEGER NOT NULL DEFAULT 1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x1f6

    :cond_14
    const/16 v1, 0x1f7

    if-ge v0, v1, :cond_15

    const-string v0, "DROP TABLE IF EXISTS notifications;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE notifications (_id INTEGER PRIMARY KEY AUTOINCREMENT,notification_id TEXT,game_id INTEGER REFERENCES games(_id) NOT NULL,external_sub_id TEXT NOT NULL,type INTEGER NOT NULL,image_id INTEGER REFERENCES images(_id),ticker TEXT,title TEXT,text TEXT,coalesced_text TEXT,timestamp INTEGER NOT NULL DEFAULT -1,acknowledged INTEGER NOT NULL DEFAULT 0,alert_level INTEGER NOT NULL DEFAULT 1);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x1f7

    :cond_15
    const/16 v1, 0x1f8

    if-ge v0, v1, :cond_16

    invoke-direct {p0, p1}, Ldjt;->f(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x1f8

    :cond_16
    const/16 v1, 0x1f9

    if-ge v0, v1, :cond_17

    const-string v0, "ALTER TABLE games ADD muted INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/16 v0, 0x1f9

    :cond_17
    const/16 v1, 0x1fa

    if-ge v0, v1, :cond_1

    const-string v0, "requests"

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Ldjt;->a:Ldjs;

    iget-object v1, p0, Ldjt;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ldjs;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "request_sync_token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0

    :cond_18
    move v0, v1

    goto/16 :goto_3

    :cond_19
    move v1, v2

    goto/16 :goto_2

    :cond_1a
    move v2, p2

    goto/16 :goto_1
.end method
