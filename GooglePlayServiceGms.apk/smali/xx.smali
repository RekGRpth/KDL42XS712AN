.class public final Lxx;
.super Lyh;
.source "SourceFile"

# interfaces
.implements Laaf;
.implements Laak;
.implements Laar;
.implements Lxw;
.implements Lyq;
.implements Lzi;


# instance fields
.field private final a:Lzp;

.field private final b:Lxy;

.field private final c:Lxz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lzp;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 2

    invoke-direct {p0}, Lyh;-><init>()V

    new-instance v0, Lxy;

    invoke-direct {v0, p1, p2, p3, p5}, Lxy;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    iput-object v0, p0, Lxx;->b:Lxy;

    iput-object p4, p0, Lxx;->a:Lzp;

    new-instance v0, Lxz;

    invoke-direct {v0, p0}, Lxz;-><init>(Lxx;)V

    iput-object v0, p0, Lxx;->c:Lxz;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Use AdRequest.Builder.addTestDevice(\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Laci;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\") to get test ads on this device."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lacd;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to load ad: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    invoke-interface {v0, p1}, Lyd;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdFailedToLoad()."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    const/4 v1, -0x2

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1, p1, v0}, Landroid/widget/ViewSwitcher;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    const-wide/16 v4, -0x1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-nez v0, :cond_1

    const-string v0, "Ad state was null when trying to ping impression URLs."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Pinging Impression URLs."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->j:Labs;

    iget-object v1, v0, Labs;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, v0, Labs;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-wide v2, v0, Labs;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Labs;->d:J

    invoke-static {}, Labu;->b()Labv;

    move-result-object v2

    invoke-virtual {v2}, Labv;->a()V

    invoke-static {v0}, Labu;->a(Labs;)V

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->e:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v2, v2, Lxy;->i:Labr;

    iget-object v2, v2, Labr;->e:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lacd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    :cond_3
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->n:Lzh;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->n:Lzh;

    iget-object v0, v0, Lzh;->d:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v2, v2, Lxy;->i:Labr;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v3, v3, Lxy;->b:Ljava/lang/String;

    iget-object v4, p0, Lxx;->b:Lxy;

    iget-object v4, v4, Lxy;->i:Labr;

    iget-object v4, v4, Labr;->n:Lzh;

    iget-object v5, v4, Lzh;->d:Ljava/util/List;

    move v4, p1

    invoke-static/range {v0 .. v5}, Lzo;->a(Landroid/content/Context;Ljava/lang/String;Labr;Ljava/lang/String;ZLjava/util/List;)V

    :cond_4
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->k:Lzg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->k:Lzg;

    iget-object v0, v0, Lzg;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v2, v2, Lxy;->i:Labr;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v3, v3, Lxy;->b:Ljava/lang/String;

    iget-object v4, p0, Lxx;->b:Lxy;

    iget-object v4, v4, Lxy;->i:Labr;

    iget-object v4, v4, Labr;->k:Lzg;

    iget-object v5, v4, Lzg;->e:Ljava/util/List;

    move v4, p1

    invoke-static/range {v0 .. v5}, Lzo;->a(Landroid/content/Context;Ljava/lang/String;Labr;Ljava/lang/String;ZLjava/util/List;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Labr;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p1, Labr;->j:Z

    if-eqz v0, :cond_5

    :try_start_0
    iget-object v0, p1, Labr;->l:Lzs;

    invoke-interface {v0}, Lzs;->a()Lcrv;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v3, v3, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, Lxx;->b:Lxy;

    iget-object v4, v4, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4, v3}, Landroid/widget/ViewSwitcher;->removeView(Landroid/view/View;)V

    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lxx;->a(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    :cond_2
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Lacl;

    if-eqz v3, :cond_6

    check-cast v0, Lacl;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v3, v3, Lxy;->c:Landroid/content/Context;

    iget-object v4, p0, Lxx;->b:Lxy;

    iget-object v4, v4, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-virtual {v0, v3, v4}, Lacl;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->l:Lzs;

    if-eqz v0, :cond_4

    :try_start_2
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->l:Lzs;

    invoke-interface {v0}, Lzs;->c()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_4
    :goto_2
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setVisibility(I)V

    move v0, v2

    :goto_3
    return v0

    :catch_0
    move-exception v0

    const-string v2, "Could not get View from mediation adapter."

    invoke-static {v2, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v2, "Could not add mediation view to view hierarchy."

    invoke-static {v2, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_3

    :cond_5
    iget-object v0, p1, Labr;->q:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    if-eqz v0, :cond_1

    iget-object v0, p1, Labr;->b:Lacl;

    iget-object v3, p1, Labr;->q:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-virtual {v0, v3}, Lacl;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->removeAllViews()V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget-object v3, p1, Labr;->q:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v3, v3, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    invoke-virtual {v0, v3}, Landroid/widget/ViewSwitcher;->setMinimumWidth(I)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget-object v3, p1, Labr;->q:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v3, v3, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    invoke-virtual {v0, v3}, Landroid/widget/ViewSwitcher;->setMinimumHeight(I)V

    iget-object v0, p1, Labr;->b:Lacl;

    invoke-direct {p0, v0}, Lxx;->a(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_6
    if-eqz v0, :cond_3

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v3, v3, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3, v0}, Landroid/widget/ViewSwitcher;->removeView(Landroid/view/View;)V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v0, "Could not destroy previous mediation adapter."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private c(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)Labd;
    .locals 14

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    :try_start_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_0
    const/4 v1, 0x0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1, v0}, Landroid/widget/ViewSwitcher;->getLocationOnScreen([I)V

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v1, 0x1

    aget v3, v0, v1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getWidth()I

    move-result v4

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getHeight()I

    move-result v7

    const/4 v0, 0x0

    iget-object v8, p0, Lxx;->b:Lxy;

    iget-object v8, v8, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v8}, Landroid/widget/ViewSwitcher;->isShown()Z

    move-result v8

    if-eqz v8, :cond_0

    add-int v8, v2, v4

    if-lez v8, :cond_0

    add-int v8, v3, v7

    if-lez v8, :cond_0

    iget v8, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-gt v2, v8, :cond_0

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-gt v3, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    const/4 v8, 0x5

    invoke-direct {v1, v8}, Landroid/os/Bundle;-><init>(I)V

    const-string v8, "x"

    invoke-virtual {v1, v8, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "y"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "width"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "height"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "visible"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    invoke-static {}, Labu;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lxx;->b:Lxy;

    new-instance v2, Labs;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v3, v3, Lxy;->b:Ljava/lang/String;

    invoke-direct {v2, v7, v3}, Labs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, v0, Lxy;->j:Labs;

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->j:Labs;

    iget-object v2, v0, Labs;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, v0, Labs;->h:J

    invoke-static {}, Labu;->b()Labv;

    move-result-object v3

    iget-wide v8, v0, Labs;->h:J

    iget-object v4, v3, Labv;->a:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-wide v10, v3, Labv;->d:J

    const-wide/16 v12, -0x1

    cmp-long v0, v10, v12

    if-nez v0, :cond_2

    iput-wide v8, v3, Labv;->d:J

    iget-wide v8, v3, Labv;->d:J

    iput-wide v8, v3, Labv;->c:J

    :goto_1
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    const-string v8, "gw"

    const/4 v9, 0x2

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v8, 0x1

    if-ne v0, v8, :cond_3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v2, v2, Lxy;->c:Landroid/content/Context;

    invoke-static {v0, v7, v2}, Labu;->a(Lxy;Ljava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v10

    new-instance v0, Labd;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v3, v2, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v4, v2, Lxy;->b:Ljava/lang/String;

    sget-object v8, Labu;->a:Ljava/lang/String;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v9, v2, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-object v2, p1

    invoke-direct/range {v0 .. v10}, Labd;-><init>(Landroid/os/Bundle;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Landroid/os/Bundle;)V

    return-object v0

    :catch_0
    move-exception v0

    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_2
    :try_start_4
    iput-wide v8, v3, Labv;->c:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v4

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_3
    :try_start_6
    iget v0, v3, Labv;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Labv;->e:I

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method

.method private s()V
    .locals 2

    const-string v0, "Ad finished loading."

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    invoke-interface {v0}, Lyd;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdLoaded()."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    invoke-virtual {v0}, Lacl;->destroy()V

    iget-object v0, p0, Lxx;->b:Lxy;

    const/4 v1, 0x0

    iput-object v1, v0, Lxy;->i:Labr;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-nez v0, :cond_1

    const-string v0, "Ad state was null when trying to ping click URLs."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Pinging click URLs."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->j:Labs;

    iget-object v1, v0, Labs;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, v0, Labs;->i:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    new-instance v2, Labt;

    invoke-direct {v2}, Labt;-><init>()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, v2, Labt;->a:J

    iget-object v3, v0, Labs;->a:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-wide v2, v0, Labs;->g:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Labs;->g:J

    invoke-static {}, Labu;->b()Labv;

    move-result-object v2

    iget-object v3, v2, Labv;->a:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v4, v2, Labv;->b:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Labv;->b:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v0}, Labu;->a(Labs;)V

    :cond_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->c:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v2, v2, Lxy;->i:Labr;

    iget-object v2, v2, Labr;->c:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lacd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    :cond_3
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->n:Lzh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->n:Lzh;

    iget-object v0, v0, Lzh;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v2, v2, Lxy;->i:Labr;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v3, v3, Lxy;->b:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lxx;->b:Lxy;

    iget-object v5, v5, Lxy;->i:Labr;

    iget-object v5, v5, Labr;->n:Lzh;

    iget-object v5, v5, Lzh;->c:Ljava/util/List;

    invoke-static/range {v0 .. v5}, Lzo;->a(Landroid/content/Context;Ljava/lang/String;Labr;Ljava/lang/String;ZLjava/util/List;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Labr;)V
    .locals 10

    const/4 v9, 0x0

    const/4 v5, 0x3

    const/4 v6, -0x2

    const-wide/16 v7, -0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lxx;->b:Lxy;

    iput-object v9, v0, Lxy;->g:Laby;

    iget v0, p1, Labr;->d:I

    if-eq v0, v6, :cond_0

    iget v0, p1, Labr;->d:I

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    invoke-static {v0}, Labu;->a(Lxy;)V

    :cond_0
    iget v0, p1, Labr;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Labr;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    iget-object v0, p1, Labr;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    const-string v1, "_noRefresh"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_1
    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v1, :cond_5

    iget-object v0, p1, Labr;->b:Lacl;

    invoke-static {v0}, Lacd;->a(Landroid/webkit/WebView;)V

    :cond_2
    :goto_2
    iget v0, p1, Labr;->d:I

    if-ne v0, v5, :cond_3

    iget-object v0, p1, Labr;->n:Lzh;

    if-eqz v0, :cond_3

    iget-object v0, p1, Labr;->n:Lzh;

    iget-object v0, v0, Lzh;->e:Ljava/util/List;

    if-eqz v0, :cond_3

    const-string v0, "Pinging no fill URLs."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v3, v2, Lxy;->b:Ljava/lang/String;

    iget-object v2, p1, Labr;->n:Lzh;

    iget-object v5, v2, Lzh;->e:Ljava/util/List;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lzo;->a(Landroid/content/Context;Ljava/lang/String;Labr;Ljava/lang/String;ZLjava/util/List;)V

    :cond_3
    iget v0, p1, Labr;->d:I

    if-eq v0, v6, :cond_8

    iget v0, p1, Labr;->d:I

    invoke-direct {p0, v0}, Lxx;->a(I)V

    goto :goto_0

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    if-nez v0, :cond_2

    iget-wide v0, p1, Labr;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_6

    iget-object v0, p0, Lxx;->c:Lxz;

    iget-object v1, p1, Labr;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-wide v2, p1, Labr;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lxz;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V

    goto :goto_2

    :cond_6
    iget-object v0, p1, Labr;->n:Lzh;

    if-eqz v0, :cond_7

    iget-object v0, p1, Labr;->n:Lzh;

    iget-wide v0, v0, Lzh;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    iget-object v0, p0, Lxx;->c:Lxz;

    iget-object v1, p1, Labr;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v2, p1, Labr;->n:Lzh;

    iget-wide v2, v2, Lzh;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lxz;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V

    goto :goto_2

    :cond_7
    iget-boolean v0, p1, Labr;->j:Z

    if-nez v0, :cond_2

    iget v0, p1, Labr;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lxx;->c:Lxz;

    iget-object v1, p1, Labr;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    invoke-virtual {v0, v1}, Lxz;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_9

    invoke-direct {p0, p1}, Lxx;->b(Labr;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-direct {p0, v4}, Lxx;->a(I)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->o:Lzj;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->o:Lzj;

    invoke-virtual {v0, v9}, Lzj;->a(Lzi;)V

    :cond_a
    iget-object v0, p1, Labr;->o:Lzj;

    if-eqz v0, :cond_b

    iget-object v0, p1, Labr;->o:Lzj;

    invoke-virtual {v0, p0}, Lzj;->a(Lzi;)V

    :cond_b
    iget-object v0, p0, Lxx;->b:Lxy;

    iput-object p1, v0, Lxy;->i:Labr;

    iget-object v0, p1, Labr;->q:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v1, p1, Labr;->q:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iput-object v1, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    :cond_c
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->j:Labs;

    iget-wide v1, p1, Labr;->s:J

    iget-object v3, v0, Labs;->b:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-wide v1, v0, Labs;->i:J

    iget-wide v1, v0, Labs;->i:J

    cmp-long v1, v1, v7

    if-eqz v1, :cond_d

    invoke-static {v0}, Labu;->a(Labs;)V

    :cond_d
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->j:Labs;

    iget-wide v1, p1, Labr;->t:J

    iget-object v3, v0, Labs;->b:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-wide v5, v0, Labs;->i:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_e

    iput-wide v1, v0, Labs;->c:J

    invoke-static {v0}, Labu;->a(Labs;)V

    :cond_e
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->j:Labs;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    iget-object v2, v0, Labs;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-wide v5, v0, Labs;->i:J

    cmp-long v3, v5, v7

    if-eqz v3, :cond_f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iput-wide v5, v0, Labs;->f:J

    if-nez v1, :cond_f

    iget-wide v5, v0, Labs;->f:J

    iput-wide v5, v0, Labs;->d:J

    invoke-static {v0}, Labu;->a(Labs;)V

    :cond_f
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->j:Labs;

    iget-boolean v1, p1, Labr;->j:Z

    iget-object v2, v0, Labs;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-wide v5, v0, Labs;->i:J

    cmp-long v3, v5, v7

    if-eqz v3, :cond_10

    iput-boolean v1, v0, Labs;->e:Z

    invoke-static {v0}, Labu;->a(Labs;)V

    :cond_10
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_11

    invoke-direct {p0, v4}, Lxx;->a(Z)V

    :cond_11
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->l:Labw;

    if-nez v0, :cond_12

    iget-object v0, p0, Lxx;->b:Lxy;

    new-instance v1, Labw;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v2, v2, Lxy;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Labw;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lxy;->l:Labw;

    :cond_12
    iget-object v0, p1, Labr;->n:Lzh;

    if-eqz v0, :cond_13

    iget-object v0, p1, Labr;->n:Lzh;

    iget v0, v0, Lzh;->h:I

    iget-object v1, p1, Labr;->n:Lzh;

    iget v4, v1, Lzh;->i:I

    :goto_3
    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->l:Labw;

    iget-object v2, v1, Labw;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_4
    iput v0, v1, Labw;->b:I

    iput v4, v1, Labw;->c:I

    iget-object v0, v1, Labw;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Labu;->a(Ljava/lang/String;Labw;)V

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    invoke-direct {p0}, Lxx;->s()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_3
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_4
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_13
    move v0, v4

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V
    .locals 2

    const-string v0, "setAdSize must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iput-object p1, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    invoke-virtual {v0, p1}, Lacl;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    :cond_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget v1, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setMinimumWidth(I)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget v1, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setMinimumHeight(I)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->requestLayout()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->k:Lyl;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->k:Lyl;

    invoke-interface {v0, p1, p2}, Lyl;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call the AppEventListener."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lyd;)V
    .locals 1

    const-string v0, "setAdListener must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iput-object p1, v0, Lxy;->f:Lyd;

    return-void
.end method

.method public final a(Lyl;)V
    .locals 1

    const-string v0, "setAppEventListener must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iput-object p1, v0, Lxy;->k:Lyl;

    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)Z
    .locals 11

    const/4 v9, 0x1

    const/4 v2, 0x0

    const-string v0, "loadAd must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->g:Laby;

    if-eqz v0, :cond_1

    const-string v0, "An ad request is already in progress. Aborting."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_2

    const-string v0, "An interstitial is already loading. Aborting."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "android.permission.INTERNET"

    invoke-static {v0, v1, v3}, Lacd;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    const-string v3, "Missing internet permission in AndroidManifest.xml."

    const-string v4, "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />"

    invoke-static {v0, v1, v3, v4}, Laci;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v0, v2

    :goto_1
    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->c:Landroid/content/Context;

    invoke-static {v1}, Lacd;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    const-string v3, "Missing AdActivity with android:configChanges in AndroidManifest.xml."

    const-string v4, "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />"

    invoke-static {v0, v1, v3, v4}, Laci;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v0, v2

    :cond_5
    if-nez v0, :cond_6

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1, v2}, Landroid/widget/ViewSwitcher;->setVisibility(I)V

    :cond_6
    if-eqz v0, :cond_0

    const-string v0, "Starting ad request."

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->c:Lxz;

    invoke-virtual {v0}, Lxz;->a()V

    invoke-direct {p0, p1}, Lxx;->c(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)Labd;

    move-result-object v10

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v4, v3, Lxy;->d:Luf;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v5, v3, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lacl;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLuf;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lacl;

    move-result-object v6

    invoke-virtual {v6}, Lacl;->e()Lacn;

    move-result-object v0

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p0

    move-object v4, p0

    move v5, v9

    invoke-virtual/range {v0 .. v5}, Lacn;->a(Lxw;Laaf;Lyq;Laak;Z)V

    move-object v4, v6

    :goto_2
    iget-object v7, p0, Lxx;->b:Lxy;

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v1, v0, Lxy;->c:Landroid/content/Context;

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v3, v0, Lxy;->d:Luf;

    iget-object v5, p0, Lxx;->a:Lzp;

    new-instance v0, Laas;

    move-object v2, v10

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Laas;-><init>(Landroid/content/Context;Labd;Luf;Lacl;Lzp;Laar;)V

    iget-object v1, v0, Laby;->e:Ljava/lang/Runnable;

    invoke-static {v1}, Laca;->a(Ljava/lang/Runnable;)V

    iput-object v0, v7, Lxy;->g:Laby;

    move v2, v9

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lacl;

    if-eqz v1, :cond_9

    check-cast v0, Lacl;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->c:Landroid/content/Context;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v3, v3, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-virtual {v0, v1, v3}, Lacl;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    :cond_8
    :goto_3
    invoke-virtual {v0}, Lacl;->e()Lacn;

    move-result-object v3

    move-object v4, p0

    move-object v5, p0

    move-object v6, p0

    move-object v7, p0

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lacn;->a(Lxw;Laaf;Lyq;Laak;Z)V

    move-object v4, v0

    goto :goto_2

    :cond_9
    if-eqz v0, :cond_a

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1, v0}, Landroid/widget/ViewSwitcher;->removeView(Landroid/view/View;)V

    :cond_a
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v4, v3, Lxy;->d:Luf;

    iget-object v3, p0, Lxx;->b:Lxy;

    iget-object v5, v3, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lacl;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLuf;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lacl;

    move-result-object v0

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    if-nez v1, :cond_8

    invoke-direct {p0, v0}, Lxx;->a(Landroid/view/View;)V

    goto :goto_3

    :cond_b
    move v0, v9

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "destroy must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iput-object v1, v0, Lxy;->f:Lyd;

    iget-object v0, p0, Lxx;->b:Lxy;

    iput-object v1, v0, Lxy;->k:Lyl;

    iget-object v0, p0, Lxx;->c:Lxz;

    invoke-virtual {v0}, Lxz;->a()V

    invoke-virtual {p0}, Lxx;->r()V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->removeAllViews()V

    :cond_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    invoke-virtual {v0}, Lacl;->destroy()V

    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
    .locals 2

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lacd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lxx;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "Ad is not visible. Not refreshing ad."

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->c:Lxz;

    invoke-virtual {v0, p1}, Lxz;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V

    goto :goto_0
.end method

.method public final c()Lcrv;
    .locals 1

    const-string v0, "getAdFrame must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->a:Landroid/widget/ViewSwitcher;

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    .locals 1

    const-string v0, "getAdSize must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    const-string v0, "isLoaded must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->g:Laby;

    if-nez v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    const-string v0, "Ad leaving application."

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    invoke-interface {v0}, Lyd;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdLeftApplication()."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 7

    const-wide/16 v5, -0x1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lxx;->t()V

    :cond_0
    const-string v0, "Ad closing."

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    invoke-interface {v0}, Lyd;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v1, v0, Lxy;->j:Labs;

    iget-object v2, v1, Labs;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-wide v3, v1, Labs;->i:J

    cmp-long v0, v3, v5

    if-eqz v0, :cond_2

    iget-object v0, v1, Labs;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v1, Labs;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labt;

    iget-wide v3, v0, Labt;->b:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, v0, Labt;->b:J

    invoke-static {v1}, Labu;->a(Labs;)V

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdClosed()."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lxx;->a(Z)V

    :cond_0
    const-string v0, "Ad opening."

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->f:Lyd;

    invoke-interface {v0}, Lyd;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdOpened()."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final i()V
    .locals 0

    invoke-virtual {p0}, Lxx;->a()V

    return-void
.end method

.method public final j()V
    .locals 0

    invoke-virtual {p0}, Lxx;->g()V

    return-void
.end method

.method public final k()V
    .locals 0

    invoke-virtual {p0}, Lxx;->f()V

    return-void
.end method

.method public final l()V
    .locals 0

    invoke-virtual {p0}, Lxx;->h()V

    return-void
.end method

.method public final m()V
    .locals 2

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Mediation adapter "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->i:Labr;

    iget-object v1, v1, Labr;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " refreshed, but mediation adapters should never refresh."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lxx;->a(Z)V

    invoke-direct {p0}, Lxx;->s()V

    return-void
.end method

.method public final n()V
    .locals 1

    const-string v0, "pause must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    invoke-static {v0}, Lacd;->a(Landroid/webkit/WebView;)V

    :cond_0
    return-void
.end method

.method public final o()V
    .locals 3

    const-string v0, "recordManualImpression must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-nez v0, :cond_1

    const-string v0, "Ad state was null when trying to ping manual tracking URLs."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Pinging manual tracking URLs."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->c:Landroid/content/Context;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lxx;->b:Lxy;

    iget-object v2, v2, Lxy;->i:Labr;

    iget-object v2, v2, Labr;->f:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lacd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final p()V
    .locals 1

    const-string v0, "resume must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    invoke-static {v0}, Lacd;->b(Landroid/webkit/WebView;)V

    :cond_0
    return-void
.end method

.method public final q()V
    .locals 7

    const-string v0, "showInterstitial must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_0

    const-string v0, "Cannot call showInterstitial on a banner ad."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-nez v0, :cond_1

    const-string v0, "The interstitial has not loaded."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    invoke-virtual {v0}, Lacl;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "The interstitial is already showing."

    invoke-static {v0}, Lacj;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lacl;->a(Z)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-boolean v0, v0, Labr;->j:Z

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->l:Lzs;

    invoke-interface {v0}, Lzs;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not show interstitial."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lxx;->t()V

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->i:Labr;

    iget-object v4, v1, Labr;->b:Lacl;

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->i:Labr;

    iget v5, v1, Labr;->g:I

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v6, v1, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;-><init>(Lxw;Laaf;Laak;Lacl;ILcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    iget-object v1, p0, Lxx;->b:Lxy;

    iget-object v1, v1, Lxy;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Laaa;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;)V

    goto :goto_0
.end method

.method public final r()V
    .locals 2

    const-string v0, "stopLoading must be called on the main UI thread."

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->i:Labr;

    iget-object v0, v0, Labr;->b:Lacl;

    invoke-virtual {v0}, Lacl;->stopLoading()V

    iget-object v0, p0, Lxx;->b:Lxy;

    const/4 v1, 0x0

    iput-object v1, v0, Lxy;->i:Labr;

    :cond_0
    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->g:Laby;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lxx;->b:Lxy;

    iget-object v0, v0, Lxy;->g:Laby;

    invoke-virtual {v0}, Laby;->f()V

    :cond_1
    return-void
.end method
