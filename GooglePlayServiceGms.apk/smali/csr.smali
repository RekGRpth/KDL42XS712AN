.class public final Lcsr;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/feedback/SendService;

.field private b:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/SendService;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcsr;->a:Lcom/google/android/gms/feedback/SendService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcsr;->b:Ljava/io/File;

    return-void
.end method

.method private a(ZZ)V
    .locals 2

    iget-object v0, p0, Lcsr;->a:Lcom/google/android/gms/feedback/SendService;

    invoke-static {v0}, Lcom/google/android/gms/feedback/SendService;->b(Lcom/google/android/gms/feedback/SendService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcss;

    invoke-direct {v1, p0, p1, p2}, Lcss;-><init>(Lcsr;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Ljava/io/File;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    :try_start_0
    iget-object v0, p0, Lcsr;->a:Lcom/google/android/gms/feedback/SendService;

    invoke-static {v0}, Lcom/google/android/gms/feedback/SendService;->a(Lcom/google/android/gms/feedback/SendService;)Lcsn;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Lcsn;->a(Ljava/io/File;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const-string v2, "GoogleFeedbackSendService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "network error "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    throw v0
.end method


# virtual methods
.method public final run()V
    .locals 10

    const/4 v3, 0x1

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcsr;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    if-nez v4, :cond_0

    invoke-direct {p0, v1, v3}, Lcsr;->a(ZZ)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    array-length v5, v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v1

    move v0, v3

    :goto_1
    if-ge v2, v5, :cond_3

    :try_start_2
    aget-object v6, v4, v2

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".proto.gz"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-direct {p0, v6}, Lcsr;->a(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    invoke-direct {p0, v1, v0}, Lcsr;->a(ZZ)V

    goto :goto_0

    :catch_0
    move-exception v0

    move v2, v3

    :goto_3
    :try_start_3
    const-string v4, "GoogleFeedbackSendService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "3 failed attempts, giving up for now"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-direct {p0, v3, v1}, Lcsr;->a(ZZ)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move v2, v3

    :goto_4
    invoke-direct {p0, v1, v2}, Lcsr;->a(ZZ)V

    throw v0

    :catchall_1
    move-exception v2

    move-object v9, v2

    move v2, v0

    move-object v0, v9

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v2

    move-object v9, v2

    move v2, v0

    move-object v0, v9

    goto :goto_3
.end method
