.class public final Layl;
.super Lnz;
.source "SourceFile"


# static fields
.field public static final i:Lbfy;

.field public static final j:Lbfy;

.field public static final k:Lbfy;

.field private static final l:Laye;

.field private static m:Layl;

.field private static final n:[Ljava/lang/String;

.field private static final x:Lawh;

.field private static final z:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final o:Laxb;

.field private final p:Laxi;

.field private final q:Ljava/util/Map;

.field private final r:Ljava/util/Map;

.field private s:Lazg;

.field private t:Z

.field private final u:Lazj;

.field private final v:Ljava/util/Set;

.field private final w:Ljava/util/List;

.field private final y:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v0, Lbbj;

    const-string v1, "CastMediaRouteProvider"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    sput-object v0, Layl;->l:Laye;

    const-string v0, "gms:cast:media:generic_player_app_id"

    const-string v1, "CC1AD845"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Layl;->i:Lbfy;

    const-string v0, "gms:cast:media:use_tdls"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Layl;->j:Lbfy;

    const-string v0, "gms:cast:media:stats_flags"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Layl;->k:Lbfy;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.media.intent.action.PAUSE"

    aput-object v1, v0, v3

    const-string v1, "android.media.intent.action.RESUME"

    aput-object v1, v0, v2

    const/4 v1, 0x2

    const-string v2, "android.media.intent.action.STOP"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "android.media.intent.action.SEEK"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "android.media.intent.action.GET_STATUS"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "android.media.intent.action.START_SESSION"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "android.media.intent.action.GET_SESSION_STATUS"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "android.media.intent.action.END_SESSION"

    aput-object v2, v0, v1

    sput-object v0, Layl;->n:[Ljava/lang/String;

    const-string v0, "674A0243"

    const-string v1, "com.google.android.gms.cast.CATEGORY_CAST"

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "[A-F0-9]+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid appliation ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lawh;->a(Ljava/lang/String;)Lawh;

    move-result-object v0

    sput-object v0, Layl;->x:Lawh;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Layl;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x2

    invoke-direct {p0, p1}, Lnz;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Layl;->y:Ljava/util/Map;

    iget-object v0, p0, Layl;->y:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b030c    # com.google.android.gms.R.string.error_request_failed

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Layl;->y:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b030d    # com.google.android.gms.R.string.error_session_start_failed

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Layl;->y:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b030e    # com.google.android.gms.R.string.error_unknown_session

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Layl;->y:Ljava/util/Map;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b030f    # com.google.android.gms.R.string.error_temporarily_disconnected

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Layl;->q:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Layl;->v:Ljava/util/Set;

    new-instance v0, Laym;

    iget-object v1, p0, Layl;->v:Ljava/util/Set;

    const-string v2, "gms_cast_mrp"

    invoke-direct {v0, p0, p1, v1, v2}, Laym;-><init>(Layl;Landroid/content/Context;Ljava/util/Set;Ljava/lang/String;)V

    iput-object v0, p0, Layl;->u:Lazj;

    new-instance v0, Layn;

    invoke-direct {v0, p0}, Layn;-><init>(Layl;)V

    iput-object v0, p0, Layl;->p:Laxi;

    new-instance v0, Laxo;

    invoke-direct {v0, p1}, Laxo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Layl;->o:Laxb;

    iget-object v0, p0, Layl;->o:Laxb;

    iget-object v1, p0, Layl;->p:Laxi;

    invoke-virtual {v0, v1}, Laxb;->a(Laxi;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Layl;->r:Ljava/util/Map;

    invoke-direct {p0}, Layl;->e()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Layl;->w:Ljava/util/List;

    invoke-direct {p0}, Layl;->c()V

    return-void
.end method

.method static synthetic a()Laye;
    .locals 1

    sget-object v0, Layl;->l:Laye;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Layl;
    .locals 3

    const-class v1, Layl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Layl;->m:Layl;

    if-nez v0, :cond_0

    new-instance v0, Layl;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Layl;-><init>(Landroid/content/Context;)V

    sput-object v0, Layl;->m:Layl;

    :cond_0
    sget-object v0, Layl;->m:Layl;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lazf;)Lnw;
    .locals 14

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v7, p1, Lazf;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v8, p1, Lazf;->b:Ljava/util/Set;

    iget-object v0, p0, Layl;->r:Ljava/util/Map;

    invoke-virtual {v7}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laze;

    if-eqz v0, :cond_6

    iget-boolean v2, v0, Laze;->c:Z

    iget-object v0, v0, Laze;->a:Lbac;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lbac;->d()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v0}, Lbac;->f()D

    move-result-wide v5

    const-wide/high16 v9, 0x4034000000000000L    # 20.0

    mul-double/2addr v5, v9

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-int v1, v5

    invoke-virtual {v0}, Lbac;->g()Ljava/lang/String;

    move-result-object v0

    move v5, v1

    move v1, v2

    move v2, v4

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v7}, Lcom/google/android/gms/cast/CastDevice;->e()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    :goto_1
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v7, v9}, Lcom/google/android/gms/cast/CastDevice;->a(Landroid/os/Bundle;)V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawh;

    new-instance v11, Landroid/content/IntentFilter;

    invoke-direct {v11}, Landroid/content/IntentFilter;-><init>()V

    iget-object v0, v0, Lawh;->a:Ljava/lang/String;

    invoke-virtual {v11, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    const-string v12, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-static {v0, v12}, Layl;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_0

    const-string v12, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK"

    invoke-static {v0, v12}, Layl;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Layl;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_1
    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    sget-object v0, Layl;->l:Laye;

    const-string v8, "buildRouteDescriptorForDevice: id=%s, description=%s, connecting=%b, volume=%d"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v3

    invoke-virtual {v7}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v4

    const/4 v12, 0x2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v0, v8, v11}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lnx;

    invoke-virtual {v7}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v8, v7}, Lnx;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, v0, Lnx;->a:Landroid/os/Bundle;

    const-string v8, "status"

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v0, Lnx;->a:Landroid/os/Bundle;

    const-string v7, "connecting"

    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, v0, Lnx;->a:Landroid/os/Bundle;

    const-string v6, "volumeHandling"

    invoke-virtual {v1, v6, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, v0, Lnx;->a:Landroid/os/Bundle;

    const-string v2, "volume"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, v0, Lnx;->a:Landroid/os/Bundle;

    const-string v2, "volumeMax"

    const/16 v5, 0x14

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, v0, Lnx;->a:Landroid/os/Bundle;

    const-string v2, "playbackType"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v10}, Lnx;->a(Ljava/util/Collection;)Lnx;

    move-result-object v0

    iget-object v1, v0, Lnx;->a:Landroid/os/Bundle;

    const-string v2, "extras"

    invoke-virtual {v1, v2, v9}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v1, v0, Lnx;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lnx;->a:Landroid/os/Bundle;

    const-string v2, "controlFilters"

    iget-object v4, v0, Lnx;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_3
    new-instance v1, Lnw;

    iget-object v2, v0, Lnx;->a:Landroid/os/Bundle;

    iget-object v0, v0, Lnx;->b:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v0, v3}, Lnw;-><init>(Landroid/os/Bundle;Ljava/util/List;B)V

    return-object v1

    :cond_4
    move-object v6, v0

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    move v5, v3

    move v1, v2

    move v2, v3

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    move v2, v3

    move v5, v3

    move v1, v3

    goto/16 :goto_0
.end method

.method static synthetic a(Layl;)V
    .locals 0

    invoke-direct {p0}, Layl;->c()V

    return-void
.end method

.method static synthetic a(Layl;Layx;)V
    .locals 7

    iget-object v1, p1, Layx;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Layl;->r:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laze;

    if-eqz v0, :cond_3

    iget-object v3, v0, Laze;->e:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Laze;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Layl;->l:Laye;

    const-string v4, "disposing CastDeviceController for %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, v0, Laze;->a:Lbac;

    invoke-virtual {v3}, Lbac;->i()V

    iget-boolean v3, v0, Laze;->d:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Layl;->o:Laxb;

    invoke-virtual {v3, v2}, Laxb;->a(Ljava/lang/String;)V

    :cond_0
    iget-boolean v3, v0, Laze;->b:Z

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Laze;->d:Z

    if-eqz v3, :cond_2

    :cond_1
    invoke-direct {p0, v1}, Layl;->b(Lcom/google/android/gms/cast/CastDevice;)V

    invoke-direct {p0}, Layl;->c()V

    :cond_2
    iget-object v1, p0, Layl;->r:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Laze;->a:Lbac;

    invoke-virtual {v0}, Lbac;->q()V

    :cond_3
    return-void
.end method

.method static synthetic a(Layl;Lcom/google/android/gms/cast/CastDevice;)V
    .locals 0

    invoke-direct {p0, p1}, Layl;->b(Lcom/google/android/gms/cast/CastDevice;)V

    return-void
.end method

.method static synthetic a(Layl;Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;)V
    .locals 6

    iget-object v0, p0, Layl;->q:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazf;

    if-eqz v0, :cond_1

    sget-object v1, Layl;->l:Laye;

    const-string v2, "merging in criteria for existing device %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lawh;

    iget-object v3, v0, Lazf;->b:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lazf;->b:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Layl;->q:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lazf;

    invoke-direct {v2, p1, p2}, Lazf;-><init>(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v0, p0, Layl;->s:Lazg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Layl;->s:Lazg;

    invoke-interface {v0, p1}, Lazg;->a(Lcom/google/android/gms/cast/CastDevice;)V

    :cond_3
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Layl;)Laxb;
    .locals 1

    iget-object v0, p0, Layl;->o:Laxb;

    return-object v0
.end method

.method static synthetic b(Layl;Layx;)Lbac;
    .locals 8

    const/4 v7, 0x1

    iget-object v1, p1, Layx;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Layl;->r:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laze;

    if-nez v0, :cond_0

    new-instance v0, Laze;

    invoke-direct {v0, p0}, Laze;-><init>(Layl;)V

    sget-object v3, Layl;->l:Laye;

    const-string v4, "creating CastDeviceController for %s"

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v3, Layo;

    invoke-direct {v3, p0, v0}, Layo;-><init>(Layl;Laze;)V

    iget-object v4, p0, Lnz;->a:Landroid/content/Context;

    iget-object v5, p0, Lnz;->c:Lob;

    const-string v6, "gms_cast_mrp"

    invoke-static {v4, v5, v6, v1, v3}, Lbac;->a(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/gms/cast/CastDevice;Lbai;)Lbac;

    move-result-object v1

    iput-object v1, v0, Laze;->a:Lbac;

    iget-object v1, v0, Laze;->a:Lbac;

    const-string v3, "MRP"

    invoke-virtual {v1, v3}, Lbac;->a(Ljava/lang/String;)V

    iput-boolean v7, v0, Laze;->c:Z

    iget-object v1, p0, Layl;->r:Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, v0, Laze;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Laze;->a:Lbac;

    return-object v0
.end method

.method static synthetic b()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    sget-object v0, Layl;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private b(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 2

    iget-object v0, p0, Layl;->q:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Layl;->s:Lazg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Layl;->s:Lazg;

    invoke-interface {v0, p1}, Lazg;->b(Lcom/google/android/gms/cast/CastDevice;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Layl;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Layl;->r:Ljava/util/Map;

    return-object v0
.end method

.method private c()V
    .locals 5

    const/4 v3, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Layl;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazf;

    invoke-direct {p0, v0}, Layl;->a(Lazf;)Lnw;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lof;

    invoke-direct {v0}, Lof;-><init>()V

    invoke-virtual {v0, v1}, Lof;->a(Ljava/util/Collection;)Lof;

    move-result-object v0

    invoke-virtual {v0}, Lof;->a()Loe;

    move-result-object v0

    invoke-static {}, Lop;->a()V

    iget-object v2, p0, Lnz;->g:Loe;

    if-eq v2, v0, :cond_1

    iput-object v0, p0, Lnz;->g:Loe;

    iget-boolean v0, p0, Lnz;->h:Z

    if-nez v0, :cond_1

    iput-boolean v3, p0, Lnz;->h:Z

    iget-object v0, p0, Lnz;->c:Lob;

    invoke-virtual {v0, v3}, Lob;->sendEmptyMessage(I)Z

    :cond_1
    sget-object v0, Layl;->l:Laye;

    const-string v2, "published %d routes"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic d(Layl;)Lazj;
    .locals 1

    iget-object v0, p0, Layl;->u:Lazj;

    return-object v0
.end method

.method private d()V
    .locals 9

    const/4 v1, 0x1

    const/4 v3, 0x0

    new-instance v5, Ljava/util/HashSet;

    iget-object v0, p0, Layl;->v:Ljava/util/Set;

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Layl;->v:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lnz;->e:Lny;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lny;->a()Lon;

    move-result-object v0

    invoke-virtual {v0}, Lon;->a()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v4, v3

    move v2, v3

    :goto_0
    if-ge v4, v7, :cond_3

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v8, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    :try_start_0
    sget-object v0, Layl;->i:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Lawh;

    invoke-direct {v2}, Lawh;-><init>()V

    const-string v8, "android.media.intent.category.REMOTE_PLAYBACK"

    iput-object v8, v2, Lawh;->a:Ljava/lang/String;

    iput-object v0, v2, Lawh;->b:Ljava/lang/String;

    iget-object v0, p0, Layl;->v:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1

    :cond_0
    const-string v8, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK/"

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "com.google.android.gms.cast.CATEGORY_CAST"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "com.google.android.gms.cast.CATEGORY_CAST/"

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    :cond_1
    :try_start_1
    invoke-static {v0}, Lawh;->a(Ljava/lang/String;)Lawh;

    move-result-object v0

    iget-object v2, p0, Layl;->v:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_1

    :cond_2
    move v2, v3

    :cond_3
    iget-boolean v0, p0, Layl;->t:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Layl;->v:Ljava/util/Set;

    sget-object v2, Layl;->x:Lawh;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Layl;->v:Ljava/util/Set;

    sget-object v2, Layl;->x:Lawh;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    iget-object v0, p0, Layl;->v:Ljava/util/Set;

    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_3
    return-void

    :cond_5
    iget-object v0, p0, Layl;->v:Ljava/util/Set;

    sget-object v1, Layl;->x:Lawh;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move v1, v2

    goto :goto_2

    :cond_6
    iget-object v0, p0, Layl;->u:Lazj;

    iget-object v2, p0, Layl;->v:Ljava/util/Set;

    invoke-virtual {v0, v2}, Lazj;->a(Ljava/util/Set;)V

    if-eqz v1, :cond_7

    sget-object v0, Layl;->l:Laye;

    const-string v1, "starting the scan"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lnz;->a:Landroid/content/Context;

    iget-object v1, p0, Layl;->o:Laxb;

    invoke-static {v0, v1}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->b(Landroid/content/Context;Laxb;)V

    iget-object v0, p0, Lnz;->a:Landroid/content/Context;

    iget-object v1, p0, Layl;->o:Laxb;

    invoke-static {v0, v1}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a(Landroid/content/Context;Laxb;)V

    goto :goto_3

    :cond_7
    sget-object v0, Layl;->l:Laye;

    const-string v1, "stopping the scan"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lnz;->a:Landroid/content/Context;

    iget-object v1, p0, Layl;->o:Laxb;

    invoke-static {v0, v1}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->b(Landroid/content/Context;Laxb;)V

    goto :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_1
.end method

.method private e()Ljava/util/List;
    .locals 7

    const/4 v0, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v3, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    const-string v1, "android.media.intent.action.PLAY"

    invoke-virtual {v3, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "http"

    invoke-virtual {v3, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    const-string v1, "https"

    invoke-virtual {v3, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lnz;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0e0002    # com.google.android.gms.R.array.cast_mime_types

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    :try_start_0
    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Layl;->n:[Ljava/lang/String;

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    const-string v6, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v5, v6}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.cast.ACTION_SYNC_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Layl;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Layl;->q:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Layl;)Lazg;
    .locals 1

    iget-object v0, p0, Layl;->s:Lazg;

    return-object v0
.end method

.method static synthetic g(Layl;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Layl;->y:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lod;
    .locals 2

    iget-object v0, p0, Layl;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Layx;

    iget-object v0, v0, Lazf;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-direct {v1, p0, v0}, Layx;-><init>(Layl;Lcom/google/android/gms/cast/CastDevice;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lazg;)V
    .locals 0

    iput-object p1, p0, Layl;->s:Lazg;

    return-void
.end method

.method public final a(Lny;)V
    .locals 4

    sget-object v0, Layl;->l:Laye;

    const-string v1, "in onDiscoveryRequestChanged: request=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Layl;->d()V

    return-void
.end method

.method public final a(Z)V
    .locals 5

    sget-object v0, Layl;->l:Laye;

    const-string v1, "setDiscoverRemoteDisplays() enable=%b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v0, p0, Layl;->t:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Layl;->t:Z

    invoke-direct {p0}, Layl;->d()V

    :cond_0
    return-void
.end method
