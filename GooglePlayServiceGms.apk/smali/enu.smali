.class public Lenu;
.super Leog;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Leog;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lahj;)Ljava/lang/Object;
    .locals 15

    invoke-virtual/range {p1 .. p1}, Lahj;->e()Lcom/google/android/gms/appdatasearch/StorageStats;

    move-result-object v9

    new-instance v10, Lenv;

    invoke-direct {v10}, Lenv;-><init>()V

    if-eqz v9, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, v9, Lcom/google/android/gms/appdatasearch/StorageStats;->b:[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, v10, Lenv;->a:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, v10, Lenv;->b:J

    iget-object v11, v9, Lcom/google/android/gms/appdatasearch/StorageStats;->b:[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    array-length v12, v11

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v12, :cond_2

    aget-object v13, v11, v8

    iget-object v14, v10, Lenv;->a:Ljava/util/List;

    iget-object v0, p0, Lenu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v0, Leoh;

    iget-object v1, v13, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->b:Ljava/lang/String;

    iget-object v2, v13, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->b:Ljava/lang/String;

    invoke-static {v3, v2}, Leoh;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    iget-object v4, v13, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Leoh;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-wide v4, v13, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->c:J

    iget-wide v6, v13, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->e:J

    invoke-direct/range {v0 .. v7}, Leoh;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;JJ)V

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-wide v0, v10, Lenv;->b:J

    iget-wide v2, v13, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->e:J

    add-long/2addr v0, v2

    iput-wide v0, v10, Lenv;->b:J

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    iget-wide v0, v10, Lenv;->b:J

    iget-wide v2, v9, Lcom/google/android/gms/appdatasearch/StorageStats;->c:J

    add-long/2addr v0, v2

    iput-wide v0, v10, Lenv;->b:J

    iget-wide v0, v9, Lcom/google/android/gms/appdatasearch/StorageStats;->d:J

    iput-wide v0, v10, Lenv;->c:J

    iget-wide v0, v9, Lcom/google/android/gms/appdatasearch/StorageStats;->e:J

    iput-wide v0, v10, Lenv;->d:J

    :goto_2
    return-object v10

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v10, Lenv;->a:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, v10, Lenv;->c:J

    const-wide/16 v0, 0x0

    iput-wide v0, v10, Lenv;->b:J

    const-wide/16 v0, 0x0

    iput-wide v0, v10, Lenv;->d:J

    goto :goto_2
.end method
