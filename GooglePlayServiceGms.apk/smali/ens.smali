.class public final Lens;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/Map;


# instance fields
.field final a:Ljava/lang/Object;

.field b:Lejm;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/Context;

.field private final f:Lemz;

.field private final g:Leji;

.field private final h:Leiv;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lens;->c:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 6

    new-instance v3, Lenr;

    const-class v0, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-direct {v3, p2, p1, v0}, Lenr;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)V

    new-instance v4, Lemo;

    invoke-direct {v4, p2}, Lemo;-><init>(Landroid/content/Context;)V

    new-instance v5, Lenk;

    invoke-direct {v5, p2}, Lenk;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lens;-><init>(Ljava/lang/String;Landroid/content/Context;Lemz;Leji;Leiv;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;Lemz;Leji;Leiv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lens;->a:Ljava/lang/Object;

    iput-object p1, p0, Lens;->d:Ljava/lang/String;

    iput-object p2, p0, Lens;->e:Landroid/content/Context;

    iput-object p3, p0, Lens;->f:Lemz;

    iput-object p4, p0, Lens;->g:Leji;

    iput-object p5, p0, Lens;->h:Leiv;

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/app/Service;)Lens;
    .locals 5

    sget-object v2, Lens;->c:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lens;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lens;

    if-nez v0, :cond_1

    new-instance v0, Lens;

    invoke-virtual {p1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lens;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    sget-object v1, Lens;->c:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    :goto_0
    invoke-static {}, Lens;->d()V

    iget v0, v1, Lens;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Lens;->i:I

    const-string v0, "onCreate count=%d"

    iget v3, v1, Lens;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v0, 0x7

    :goto_1
    const/4 v3, 0x2

    if-le v0, v3, :cond_0

    const-string v3, "Icing"

    add-int/lit8 v4, v0, -0x1

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(I)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private static d()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-static {}, Lens;->d()V

    iget v0, p0, Lens;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lens;->i:I

    iget v0, p0, Lens;->i:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "More calls to onDestroy than onCreate"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    const-string v0, "onDestroy count=%d"

    iget v1, p0, Lens;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget v0, p0, Lens;->i:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lens;->g:Leji;

    invoke-interface {v0}, Leji;->a()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lemz;
    .locals 1

    iget-object v0, p0, Lens;->f:Lemz;

    return-object v0
.end method

.method public final c()Lejm;
    .locals 8

    iget-object v7, p0, Lens;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lens;->b:Lejm;

    if-nez v0, :cond_0

    new-instance v4, Lelk;

    iget-object v0, p0, Lens;->d:Ljava/lang/String;

    iget-object v1, p0, Lens;->e:Landroid/content/Context;

    iget-object v2, p0, Lens;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Lelk;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/io/File;)V

    new-instance v0, Lejm;

    iget-object v1, p0, Lens;->e:Landroid/content/Context;

    iget-object v2, p0, Lens;->f:Lemz;

    iget-object v3, p0, Lens;->d:Ljava/lang/String;

    iget-object v5, p0, Lens;->g:Leji;

    iget-object v6, p0, Lens;->h:Leiv;

    invoke-direct/range {v0 .. v6}, Lejm;-><init>(Landroid/content/Context;Lemz;Ljava/lang/String;Lemp;Leji;Leiv;)V

    const-string v1, "%s: Starting asynchronous initialization"

    iget-object v2, p0, Lens;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lejm;->a(Z)Lenh;

    iput-object v0, p0, Lens;->b:Lejm;

    new-instance v1, Lent;

    invoke-direct {v1, p0, v0}, Lent;-><init>(Lens;Lejm;)V

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "index-service-init-watch-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lens;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    :goto_0
    monitor-exit v7

    return-object v0

    :cond_0
    const-string v1, "%s: Re-using cached"

    iget-object v2, p0, Lens;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method
