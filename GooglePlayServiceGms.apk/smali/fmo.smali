.class final Lfmo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbdo;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final synthetic a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Lbdv;Lbdx;Lbdy;)Lbdn;
    .locals 10

    new-instance v0, Lfmp;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lfmp;-><init>(B)V

    if-eqz p4, :cond_0

    instance-of v0, p4, Lfmp;

    const-string v1, "Must provide valid PlusOptions!"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    check-cast p4, Lfmp;

    :goto_0
    new-instance v9, Lfto;

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusSession;

    iget-object v1, p3, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/gms/common/internal/ClientSettings;->a()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p4, Lfmp;->b:Ljava/util/Set;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v8, Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-direct {v8}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;-><init>()V

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/PlusSession;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    move-object v1, v9

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    move-object/from16 v5, p6

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lfto;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;Lcom/google/android/gms/plus/internal/PlusSession;)V

    return-object v9

    :cond_0
    move-object p4, v0

    goto :goto_0
.end method
