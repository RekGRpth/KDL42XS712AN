.class public abstract Lbdq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbds;
.implements Lbeg;
.implements Lbeh;


# instance fields
.field private final a:Lbdo;

.field private final b:Ljava/lang/Object;

.field private c:Lbdr;

.field private final d:Ljava/util/concurrent/CountDownLatch;

.field private final e:Ljava/util/ArrayList;

.field private f:Lbel;

.field private volatile g:Lbek;

.field private volatile h:Z

.field private i:Z

.field private j:Z

.field private k:Lbee;


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbdq;-><init>(Lbdo;)V

    return-void
.end method

.method public constructor <init>(Lbdo;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbdq;->b:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbdq;->d:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbdq;->e:Ljava/util/ArrayList;

    iput-object p1, p0, Lbdq;->a:Lbdo;

    return-void
.end method

.method protected constructor <init>(Lbdr;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbdq;->b:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbdq;->d:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbdq;->e:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lbdq;->a:Lbdo;

    iput-object p1, p0, Lbdq;->c:Lbdr;

    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lbdq;->a(Lcom/google/android/gms/common/api/Status;)Lbek;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbdq;->a(Lbek;)V

    return-void
.end method

.method private e()Lbek;
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lbdq;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Lbdq;->h:Z

    if-nez v2, :cond_1

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lbdq;->b()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbdq;->g:Lbek;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lbdq;->h:Z

    const/4 v2, 0x0

    iput-object v2, p0, Lbdq;->g:Lbek;

    iget-object v2, p0, Lbdq;->k:Lbee;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbdq;->k:Lbee;

    invoke-interface {v2, p0}, Lbee;->a(Lbeg;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private f()V
    .locals 5

    iget-object v1, p0, Lbdq;->g:Lbek;

    if-eqz v1, :cond_0

    instance-of v1, p0, Lbej;

    if-eqz v1, :cond_0

    :try_start_0
    move-object v0, p0

    check-cast v0, Lbej;

    move-object v1, v0

    invoke-interface {v1}, Lbej;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "GoogleApi"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to release "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a()Lbdo;
    .locals 1

    iget-object v0, p0, Lbdq;->a:Lbdo;

    return-object v0
.end method

.method protected abstract a(Lcom/google/android/gms/common/api/Status;)Lbek;
.end method

.method protected abstract a(Lbdn;)V
.end method

.method public final a(Lbee;)V
    .locals 0

    iput-object p1, p0, Lbdq;->k:Lbee;

    return-void
.end method

.method public final a(Lbei;)V
    .locals 2

    iget-boolean v0, p0, Lbdq;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lbdq;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lbdq;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbdq;->g:Lbek;

    invoke-interface {v0}, Lbek;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {p1, v0}, Lbei;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_1
    monitor-exit v1

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbdq;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lbek;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lbdq;->b:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lbdq;->j:Z

    if-eqz v2, :cond_1

    instance-of v0, p1, Lbej;

    if-eqz v0, :cond_0

    check-cast p1, Lbej;

    invoke-interface {p1}, Lbej;->c()V

    :cond_0
    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbdq;->b()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-boolean v2, p0, Lbdq;->h:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lbdq;->g:Lbek;

    iget-boolean v0, p0, Lbdq;->i:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lbdq;->f()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    :try_start_1
    iget-object v0, p0, Lbdq;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lbdq;->g:Lbek;

    invoke-interface {v0}, Lbek;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    iget-object v0, p0, Lbdq;->f:Lbel;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbdq;->c:Lbdr;

    invoke-virtual {v0}, Lbdr;->a()V

    iget-object v0, p0, Lbdq;->c:Lbdr;

    iget-object v2, p0, Lbdq;->f:Lbel;

    invoke-direct {p0}, Lbdq;->e()Lbek;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lbdr;->a(Lbel;Lbek;)V

    :cond_5
    iget-object v0, p0, Lbdq;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbei;

    invoke-interface {v0, v1}, Lbei;->b(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lbdq;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Lbel;)V
    .locals 3

    iget-boolean v0, p0, Lbdq;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lbdq;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lbdq;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbdq;->c:Lbdr;

    invoke-direct {p0}, Lbdq;->e()Lbek;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lbdr;->a(Lbel;Lbek;)V

    :goto_1
    monitor-exit v1

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lbdq;->f:Lbel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lbek;

    invoke-virtual {p0, p1}, Lbdq;->a(Lbek;)V

    return-void
.end method

.method public final b(Lbdn;)V
    .locals 2

    new-instance v0, Lbdr;

    invoke-interface {p1}, Lbdn;->f()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lbdr;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbdq;->c:Lbdr;

    :try_start_0
    invoke-virtual {p0, p1}, Lbdq;->a(Lbdn;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lbdq;->a(Landroid/os/RemoteException;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lbdq;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    iget-object v0, p0, Lbdq;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lbek;
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lbdq;->h:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Results has already been consumed"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lbdq;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "await must not be called on the UI thread"

    invoke-static {v2, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lbdq;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0}, Lbdq;->b()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lbdq;->e()Lbek;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbdq;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    sget-object v0, Lcom/google/android/gms/common/api/Status;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lbdq;->a(Lcom/google/android/gms/common/api/Status;)Lbek;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbdq;->a(Lbek;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbdq;->j:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 1

    invoke-direct {p0}, Lbdq;->f()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbdq;->i:Z

    return-void
.end method
