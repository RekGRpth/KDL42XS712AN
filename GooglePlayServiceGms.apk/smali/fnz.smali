.class public final Lfnz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lfnz;


# instance fields
.field private final b:Ldl;

.field private final c:Landroid/content/pm/PackageManager;

.field private final d:Landroid/content/res/Resources;

.field private e:Landroid/graphics/drawable/Drawable;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lfnz;->c:Landroid/content/pm/PackageManager;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lfnz;->d:Landroid/content/res/Resources;

    new-instance v0, Ldl;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ldl;-><init>(I)V

    iput-object v0, p0, Lfnz;->b:Ldl;

    return-void
.end method

.method private a()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lfnz;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnz;->d:Landroid/content/res/Resources;

    const v1, 0x7f0201ff    # com.google.android.gms.R.drawable.plus_ic_apps_color_24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfnz;->e:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Lfnz;->e:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lfnz;
    .locals 1

    sget-object v0, Lfnz;->a:Lfnz;

    if-nez v0, :cond_0

    new-instance v0, Lfnz;

    invoke-direct {v0, p0}, Lfnz;-><init>(Landroid/content/Context;)V

    sput-object v0, Lfnz;->a:Lfnz;

    :cond_0
    sget-object v0, Lfnz;->a:Lfnz;

    return-object v0
.end method


# virtual methods
.method public final a(Lfxh;)Lfoa;
    .locals 6

    const/4 v2, 0x0

    invoke-interface {p1}, Lfxh;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lfnz;->b:Ldl;

    invoke-virtual {v0, v4}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoa;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lfxh;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lfnz;->c:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_1

    invoke-direct {p0}, Lfnz;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_1
    new-instance v3, Lfoa;

    invoke-interface {p1}, Lfxh;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lfnz;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-ne v0, v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-direct {v3, v5, v0, v1, v2}, Lfoa;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;ZB)V

    iget-object v0, p0, Lfnz;->b:Ldl;

    invoke-virtual {v0, v4, v3}, Ldl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final a(Lfxh;Landroid/graphics/drawable/Drawable;)Lfoa;
    .locals 2

    invoke-virtual {p0, p1}, Lfnz;->a(Lfxh;)Lfoa;

    move-result-object v0

    if-eqz p2, :cond_0

    iput-object p2, v0, Lfoa;->b:Landroid/graphics/drawable/Drawable;

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, Lfoa;->c:Z

    return-object v0
.end method
