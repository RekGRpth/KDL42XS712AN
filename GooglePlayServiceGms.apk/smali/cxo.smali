.class final Lcxo;
.super Lcwk;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcwm;

.field private final b:Ldgp;


# direct methods
.method constructor <init>(Lcwm;Ldgp;)V
    .locals 0

    iput-object p1, p0, Lcxo;->a:Lcwm;

    invoke-direct {p0}, Lcwk;-><init>()V

    iput-object p2, p0, Lcxo;->b:Ldgp;

    return-void
.end method


# virtual methods
.method public final c(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcxo;->a:Lcwm;

    new-instance v1, Lcxn;

    iget-object v2, p0, Lcxo;->a:Lcwm;

    iget-object v3, p0, Lcxo;->b:Ldgp;

    invoke-direct {v1, v2, v3, p1}, Lcxn;-><init>(Lcwm;Ldgp;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final q(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    new-instance v1, Ldgq;

    invoke-direct {v1, p1}, Ldgq;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Ldgq;->a()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ldgq;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Ldgq;->b()V

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcxo;->a:Lcwm;

    new-instance v2, Lcxp;

    iget-object v3, p0, Lcxo;->a:Lcwm;

    iget-object v4, p0, Lcxo;->b:Ldgp;

    invoke-direct {v2, v3, v4, v0}, Lcxp;-><init>(Lcwm;Ldgp;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    invoke-virtual {v1, v2}, Lcwm;->a(Lbjg;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldgq;->b()V

    throw v0
.end method
