.class public final Lenx;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lenx;->a:Ljava/util/List;

    iget-object v0, p0, Lenx;->a:Ljava/util/List;

    new-instance v1, Leny;

    invoke-direct {v1, p0}, Leny;-><init>(Lenx;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lenx;->b:Landroid/view/LayoutInflater;

    iput-object p1, p0, Lenx;->c:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(I)Leoh;
    .locals 1

    iget-object v0, p0, Lenx;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leoh;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lenx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lenx;->a(I)Leoh;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const/4 v1, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lenx;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f0400ac    # com.google.android.gms.R.layout.manage_applications_item

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lenz;

    iget-object v2, p0, Lenx;->c:Landroid/content/Context;

    invoke-direct {v0, v2, p2}, Lenz;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lenz;

    invoke-virtual {p0, p1}, Lenx;->a(I)Leoh;

    move-result-object v2

    iget-object v3, v0, Lenz;->a:Landroid/widget/TextView;

    iget-object v4, v2, Leoh;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v0, Lenz;->b:Landroid/widget/TextView;

    iget-wide v4, v2, Leoh;->d:J

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_1

    iget-object v1, v0, Lenz;->d:Landroid/content/Context;

    invoke-static {v1, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lenz;->c:Landroid/widget/ImageView;

    iget-object v1, v2, Leoh;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object p2
.end method
