.class public final Lgpb;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Lgpd;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ScrollView;

.field private g:Landroid/widget/Button;

.field private h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lgpb;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lgpb;->e:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;
    .locals 1

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic c(Lgpb;)Landroid/widget/ScrollView;
    .locals 1

    iget-object v0, p0, Lgpb;->f:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic d(Lgpb;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpb;->c:Z

    return v0
.end method

.method static synthetic e(Lgpb;)Z
    .locals 1

    iget-boolean v0, p0, Lgpb;->b:Z

    return v0
.end method

.method static synthetic f(Lgpb;)Lgpd;
    .locals 1

    iget-object v0, p0, Lgpb;->d:Lgpd;

    return-object v0
.end method

.method static synthetic g(Lgpb;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpb;->b:Z

    return v0
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->f()Lgpe;

    move-result-object v0

    invoke-virtual {v0}, Lgpe;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgpb;->K()V

    :cond_0
    return-void
.end method

.method public final J()Z
    .locals 1

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final K()V
    .locals 7

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->e()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->e()Lgpx;

    move-result-object v0

    iget-object v4, v0, Lgpx;->m:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {p0}, Lgpb;->s()Lak;

    move-result-object v1

    iget-object v2, p0, Lgpb;->d:Lgpd;

    invoke-interface {v2}, Lgpd;->f()Lgpe;

    move-result-object v2

    invoke-virtual {v2}, Lgpe;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpb;->d:Lgpd;

    invoke-interface {v3}, Lgpd;->e()Lgpx;

    move-result-object v3

    invoke-virtual {v3}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lgpb;->d:Lgpd;

    invoke-interface {v5}, Lgpd;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lgpb;->d:Lgpd;

    invoke-interface {v6}, Lgpd;->h()Lfrb;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Lak;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfrb;)V

    iget-boolean v0, p0, Lgpb;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->f()Lgpe;

    move-result-object v0

    sget-object v1, Lbcz;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpe;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpb;->a:Z

    :cond_0
    return-void

    :cond_1
    sget-object v4, Lbch;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const/4 v1, 0x0

    const v0, 0x7f040102    # com.google.android.gms.R.layout.plus_replybox_fragment

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgpb;->e:Landroid/view/View;

    iget-object v0, p0, Lgpb;->e:Landroid/view/View;

    const v2, 0x7f0a02b6    # com.google.android.gms.R.id.mention_scroll_view

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lgpb;->f:Landroid/widget/ScrollView;

    iget-object v0, p0, Lgpb;->e:Landroid/view/View;

    const v2, 0x7f0a02b9    # com.google.android.gms.R.id.reply_button

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgpb;->g:Landroid/widget/Button;

    iget-object v0, p0, Lgpb;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lgpb;->g:Landroid/widget/Button;

    if-eqz p3, :cond_0

    const-string v0, "button_enabled"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lgpb;->e:Landroid/view/View;

    const v1, 0x7f0a02b8    # com.google.android.gms.R.id.compose_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    new-instance v1, Lgpc;

    invoke-virtual {p0}, Lgpb;->j()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lgpc;-><init>(Lgpb;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lgpb;->e:Landroid/view/View;

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lgpd;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lgpd;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lgpd;

    iput-object p1, p0, Lgpb;->d:Lgpd;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lgpb;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lgpb;->c:Z

    return v0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "logged_expand_replybox"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpb;->a:Z

    const-string v0, "logged_comment_added"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpb;->b:Z

    const-string v0, "user_edited"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpb;->c:Z

    :cond_0
    return-void
.end method

.method public final b()Lcom/google/android/gms/plus/model/posts/Comment;
    .locals 5

    invoke-virtual {p0}, Lgpb;->J()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->f()Lgpe;

    move-result-object v0

    sget-object v1, Lbcz;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpe;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v0, v1}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lgpb;->d:Lgpd;

    invoke-interface {v1}, Lgpd;->e()Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lgpq;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->f()Lgpe;

    move-result-object v0

    sget-object v1, Lbcz;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpe;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_1
    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->e()Lgpx;

    move-result-object v0

    iget-object v1, v0, Lgpx;->g:Ljava/lang/String;

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lgpq;->a(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->e()Lgpx;

    move-result-object v0

    iget-object v3, v0, Lgpx;->f:Ljava/lang/String;

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->e()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/model/posts/Comment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget-object v2, p0, Lgpb;->d:Lgpd;

    invoke-interface {v2}, Lgpd;->e()Lgpx;

    move-result-object v2

    iget-object v2, v2, Lgpx;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    iget-object v2, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v3, p0, Lgpb;->d:Lgpd;

    invoke-interface {v3}, Lgpd;->e()Lgpx;

    move-result-object v3

    iget-object v3, v3, Lgpx;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    :cond_0
    if-nez p1, :cond_1

    iget-object v2, p0, Lgpb;->d:Lgpd;

    invoke-interface {v2}, Lgpd;->e()Lgpx;

    move-result-object v2

    iget-object v2, v2, Lgpx;->r:Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v2, :cond_4

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->e()Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->r:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    const-string v3, "+"

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    iget-object v3, p0, Lgpb;->d:Lgpd;

    invoke-interface {v3}, Lgpd;->e()Lgpx;

    move-result-object v3

    iget-object v3, v3, Lgpx;->r:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-direct {v3, v0}, Lcom/google/android/gms/plus/sharebox/MentionSpan;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/16 v4, 0x21

    invoke-interface {v2, v3, v1, v0, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->requestFocus()Z

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setSelection(I)V

    :cond_1
    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lgpb;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setSelection(I)V

    :cond_2
    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->i()V

    return-void

    :cond_3
    move v2, v1

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto/16 :goto_1
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "logged_expand_replybox"

    iget-boolean v1, p0, Lgpb;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "logged_comment_added"

    iget-boolean v1, p0, Lgpb;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "user_edited"

    iget-boolean v1, p0, Lgpb;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "button_enabled"

    iget-object v1, p0, Lgpb;->g:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a02b9    # com.google.android.gms.R.id.reply_button

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgpb;->a(Z)V

    iget-object v0, p0, Lgpb;->d:Lgpd;

    invoke-interface {v0}, Lgpd;->j()V

    :cond_0
    return-void
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    const v1, 0x7f0a02b8    # com.google.android.gms.R.id.compose_text

    if-ne v0, v1, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, p1}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
