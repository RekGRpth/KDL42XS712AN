.class public Lboy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsj;
.implements Lsk;


# instance fields
.field public final a:Ljava/util/ArrayList;

.field private b:Lsp;

.field private final c:Lboy;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lboy;-><init>(Lboy;)V

    return-void
.end method

.method private constructor <init>(Lboy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lboy;->a:Ljava/util/ArrayList;

    iput-object p1, p0, Lboy;->c:Lboy;

    return-void
.end method


# virtual methods
.method public final a()Lboy;
    .locals 1

    new-instance v0, Lboy;

    invoke-direct {v0, p0}, Lboy;-><init>(Lboy;)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 1

    iget-object v0, p0, Lboy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    iget-object v0, p0, Lboy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    invoke-virtual {p0, p1}, Lboy;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method

.method public final a(Lsp;)V
    .locals 1

    iget-object v0, p0, Lboy;->c:Lboy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lboy;->c:Lboy;

    invoke-virtual {v0, p1}, Lboy;->a(Lsp;)V

    :cond_0
    iput-object p1, p0, Lboy;->b:Lsp;

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lboy;->b:Lsp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lboy;->b:Lsp;

    throw v0

    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lboy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
