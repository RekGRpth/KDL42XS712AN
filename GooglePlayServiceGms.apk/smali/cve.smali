.class Lcve;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/concurrent/locks/ReentrantLock;

.field private final c:Lcve;

.field private final d:Ljava/util/HashSet;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcve;->a:Ljava/lang/String;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/locks/ReentrantLock;

    iput-object v0, p0, Lcve;->b:Ljava/util/concurrent/locks/ReentrantLock;

    iput-object p3, p0, Lcve;->c:Lcve;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcve;->d:Ljava/util/HashSet;

    iget-object v0, p0, Lcve;->c:Lcve;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcve;->c:Lcve;

    invoke-direct {v0}, Lcve;->a()Z

    move-result v0

    const-string v1, "Only one level of nesting supported."

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcve;->c:Lcve;

    invoke-direct {v0}, Lcve;->a()Z

    move-result v1

    const-string v2, "Can only register children on the root"

    invoke-static {v1, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-virtual {v0}, Lcve;->e()Z

    move-result v1

    invoke-static {v1}, Lbiq;->a(Z)V

    iget-object v0, v0, Lcve;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private a()Z
    .locals 1

    iget-object v0, p0, Lcve;->c:Lcve;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcve;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcve;->c:Lcve;

    invoke-virtual {v0}, Lcve;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcve;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v2}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcve;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lcve;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcve;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void
.end method


# virtual methods
.method public varargs a([Lcve;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lcve;->a()Z

    move-result v0

    const-string v2, "Can only release locks from the root"

    invoke-static {v0, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    :goto_1
    array-length v0, p1

    if-ge v1, v0, :cond_1

    aget-object v0, p1, v1

    invoke-direct {v0}, Lcve;->h()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public varargs b([Lcve;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcve;->a()Z

    move-result v0

    const-string v3, "Can only acquire locks from the root"

    invoke-static {v0, v3}, Lbiq;->a(ZLjava/lang/Object;)V

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-direct {p0}, Lcve;->b()V

    move v3, v2

    :goto_1
    :try_start_0
    array-length v0, p1

    if-ge v3, v0, :cond_2

    aget-object v0, p1, v3

    iget-object v0, v0, Lcve;->c:Lcve;

    if-ne p0, v0, :cond_1

    move v0, v1

    :goto_2
    const-string v4, "Lockables must be children of this root."

    invoke-static {v0, v4}, Lbiq;->a(ZLjava/lang/Object;)V

    aget-object v0, p1, v3

    invoke-direct {v0}, Lcve;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcve;->h()V

    return-void

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcve;->h()V

    throw v0
.end method

.method public c()V
    .locals 5

    invoke-direct {p0}, Lcve;->a()Z

    move-result v0

    const-string v1, "Can only check for locks held by current thread on the root"

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lcve;->b()V

    :try_start_0
    iget-object v0, p0, Lcve;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcve;

    invoke-virtual {v0}, Lcve;->e()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Lock still held: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcve;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbiq;->a(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcve;->h()V

    throw v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcve;->h()V

    return-void
.end method

.method public d()V
    .locals 3

    invoke-virtual {p0}, Lcve;->e()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Lock "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcve;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not held by thread!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    return-void
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcve;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcve;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcve;

    invoke-direct {v0}, Lcve;->h()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    invoke-direct {p0}, Lcve;->a()Z

    move-result v0

    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-direct {p0}, Lcve;->b()V

    iget-object v0, p0, Lcve;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcve;

    invoke-direct {v0}, Lcve;->b()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcve;->h()V

    return-void
.end method
