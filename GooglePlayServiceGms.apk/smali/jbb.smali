.class public final Ljbb;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:Ljava/lang/String;

.field public c:[Ljbi;

.field public d:Ljava/lang/String;

.field public e:[Lipv;

.field public f:Ljava/lang/String;

.field public g:[Ljbe;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:[Ljai;

.field public m:[Ljaj;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Ljbb;->a:[I

    const-string v0, ""

    iput-object v0, p0, Ljbb;->b:Ljava/lang/String;

    invoke-static {}, Ljbi;->c()[Ljbi;

    move-result-object v0

    iput-object v0, p0, Ljbb;->c:[Ljbi;

    const-string v0, ""

    iput-object v0, p0, Ljbb;->d:Ljava/lang/String;

    invoke-static {}, Lipv;->c()[Lipv;

    move-result-object v0

    iput-object v0, p0, Ljbb;->e:[Lipv;

    const-string v0, ""

    iput-object v0, p0, Ljbb;->f:Ljava/lang/String;

    invoke-static {}, Ljbe;->c()[Ljbe;

    move-result-object v0

    iput-object v0, p0, Ljbb;->g:[Ljbe;

    const-string v0, ""

    iput-object v0, p0, Ljbb;->h:Ljava/lang/String;

    iput-boolean v1, p0, Ljbb;->i:Z

    iput-boolean v1, p0, Ljbb;->j:Z

    iput-boolean v1, p0, Ljbb;->k:Z

    invoke-static {}, Ljai;->c()[Ljai;

    move-result-object v0

    iput-object v0, p0, Ljbb;->l:[Ljai;

    invoke-static {}, Ljaj;->c()[Ljaj;

    move-result-object v0

    iput-object v0, p0, Ljbb;->m:[Ljaj;

    const/4 v0, -0x1

    iput v0, p0, Ljbb;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v3

    iget-object v0, p0, Ljbb;->a:[I

    if-eqz v0, :cond_16

    iget-object v0, p0, Ljbb;->a:[I

    array-length v0, v0

    if-lez v0, :cond_16

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v4, p0, Ljbb;->a:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Ljbb;->a:[I

    aget v4, v4, v0

    invoke-static {v4}, Lizn;->a(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int v0, v3, v2

    iget-object v2, p0, Ljbb;->a:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :goto_1
    iget-object v2, p0, Ljbb;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Ljbb;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Ljbb;->c:[Ljbi;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljbb;->c:[Ljbi;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Ljbb;->c:[Ljbi;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Ljbb;->c:[Ljbi;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v0, v2

    :cond_4
    iget-object v2, p0, Ljbb;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x4

    iget-object v3, p0, Ljbb;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Ljbb;->e:[Lipv;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljbb;->e:[Lipv;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    :goto_3
    iget-object v3, p0, Ljbb;->e:[Lipv;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    iget-object v3, p0, Ljbb;->e:[Lipv;

    aget-object v3, v3, v0

    if-eqz v3, :cond_6

    const/4 v4, 0x5

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    move v0, v2

    :cond_8
    iget-object v2, p0, Ljbb;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x6

    iget-object v3, p0, Ljbb;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Ljbb;->g:[Ljbe;

    if-eqz v2, :cond_c

    iget-object v2, p0, Ljbb;->g:[Ljbe;

    array-length v2, v2

    if-lez v2, :cond_c

    move v2, v0

    move v0, v1

    :goto_4
    iget-object v3, p0, Ljbb;->g:[Ljbe;

    array-length v3, v3

    if-ge v0, v3, :cond_b

    iget-object v3, p0, Ljbb;->g:[Ljbe;

    aget-object v3, v3, v0

    if-eqz v3, :cond_a

    const/4 v4, 0x7

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    move v0, v2

    :cond_c
    iget-object v2, p0, Ljbb;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const/16 v2, 0x9

    iget-object v3, p0, Ljbb;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-boolean v2, p0, Ljbb;->i:Z

    if-eqz v2, :cond_e

    const/16 v2, 0xb

    iget-boolean v3, p0, Ljbb;->i:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_e
    iget-boolean v2, p0, Ljbb;->j:Z

    if-eqz v2, :cond_f

    const/16 v2, 0xc

    iget-boolean v3, p0, Ljbb;->j:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_f
    iget-boolean v2, p0, Ljbb;->k:Z

    if-eqz v2, :cond_10

    const/16 v2, 0xd

    iget-boolean v3, p0, Ljbb;->k:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_10
    iget-object v2, p0, Ljbb;->l:[Ljai;

    if-eqz v2, :cond_13

    iget-object v2, p0, Ljbb;->l:[Ljai;

    array-length v2, v2

    if-lez v2, :cond_13

    move v2, v0

    move v0, v1

    :goto_5
    iget-object v3, p0, Ljbb;->l:[Ljai;

    array-length v3, v3

    if-ge v0, v3, :cond_12

    iget-object v3, p0, Ljbb;->l:[Ljai;

    aget-object v3, v3, v0

    if-eqz v3, :cond_11

    const/16 v4, 0xe

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_12
    move v0, v2

    :cond_13
    iget-object v2, p0, Ljbb;->m:[Ljaj;

    if-eqz v2, :cond_15

    iget-object v2, p0, Ljbb;->m:[Ljaj;

    array-length v2, v2

    if-lez v2, :cond_15

    :goto_6
    iget-object v2, p0, Ljbb;->m:[Ljaj;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    iget-object v2, p0, Ljbb;->m:[Ljaj;

    aget-object v2, v2, v1

    if-eqz v2, :cond_14

    const/16 v3, 0xf

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_15
    iput v0, p0, Ljbb;->C:I

    return v0

    :cond_16
    move v0, v3

    goto/16 :goto_1
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 7

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lizm;->a()I

    :cond_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljbb;->a:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Ljbb;->a:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljbb;->a:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Ljbb;->a:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljbb;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lizm;->e(I)V

    iget-object v1, p0, Ljbb;->a:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Ljbb;->a:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lizm;->k()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lizm;->g()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Ljbb;->a:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Ljbb;->a:[I

    :cond_a
    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbb;->b:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v1

    iget-object v0, p0, Ljbb;->c:[Ljbi;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Ljbi;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljbb;->c:[Ljbi;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Ljbi;

    invoke-direct {v3}, Ljbi;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Ljbb;->c:[Ljbi;

    array-length v0, v0

    goto :goto_7

    :cond_d
    new-instance v3, Ljbi;

    invoke-direct {v3}, Ljbi;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v1, p0, Ljbb;->c:[Ljbi;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbb;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v1

    iget-object v0, p0, Ljbb;->e:[Lipv;

    if-nez v0, :cond_f

    move v0, v2

    :goto_9
    add-int/2addr v1, v0

    new-array v1, v1, [Lipv;

    if-eqz v0, :cond_e

    iget-object v3, p0, Ljbb;->e:[Lipv;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    :goto_a
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_10

    new-instance v3, Lipv;

    invoke-direct {v3}, Lipv;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_f
    iget-object v0, p0, Ljbb;->e:[Lipv;

    array-length v0, v0

    goto :goto_9

    :cond_10
    new-instance v3, Lipv;

    invoke-direct {v3}, Lipv;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v1, p0, Ljbb;->e:[Lipv;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbb;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v1

    iget-object v0, p0, Ljbb;->g:[Ljbe;

    if-nez v0, :cond_12

    move v0, v2

    :goto_b
    add-int/2addr v1, v0

    new-array v1, v1, [Ljbe;

    if-eqz v0, :cond_11

    iget-object v3, p0, Ljbb;->g:[Ljbe;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    :goto_c
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_13

    new-instance v3, Ljbe;

    invoke-direct {v3}, Ljbe;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_12
    iget-object v0, p0, Ljbb;->g:[Ljbe;

    array-length v0, v0

    goto :goto_b

    :cond_13
    new-instance v3, Ljbe;

    invoke-direct {v3}, Ljbe;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v1, p0, Ljbb;->g:[Ljbe;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbb;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljbb;->i:Z

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljbb;->j:Z

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljbb;->k:Z

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x72

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v1

    iget-object v0, p0, Ljbb;->l:[Ljai;

    if-nez v0, :cond_15

    move v0, v2

    :goto_d
    add-int/2addr v1, v0

    new-array v1, v1, [Ljai;

    if-eqz v0, :cond_14

    iget-object v3, p0, Ljbb;->l:[Ljai;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_14
    :goto_e
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_16

    new-instance v3, Ljai;

    invoke-direct {v3}, Ljai;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_15
    iget-object v0, p0, Ljbb;->l:[Ljai;

    array-length v0, v0

    goto :goto_d

    :cond_16
    new-instance v3, Ljai;

    invoke-direct {v3}, Ljai;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v1, p0, Ljbb;->l:[Ljai;

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v1

    iget-object v0, p0, Ljbb;->m:[Ljaj;

    if-nez v0, :cond_18

    move v0, v2

    :goto_f
    add-int/2addr v1, v0

    new-array v1, v1, [Ljaj;

    if-eqz v0, :cond_17

    iget-object v3, p0, Ljbb;->m:[Ljaj;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_17
    :goto_10
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_19

    new-instance v3, Ljaj;

    invoke-direct {v3}, Ljaj;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_18
    iget-object v0, p0, Ljbb;->m:[Ljaj;

    array-length v0, v0

    goto :goto_f

    :cond_19
    new-instance v3, Ljaj;

    invoke-direct {v3}, Ljaj;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v1, p0, Ljbb;->m:[Ljaj;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x4a -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Ljbb;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljbb;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljbb;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Ljbb;->a:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lizn;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ljbb;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v2, p0, Ljbb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljbb;->c:[Ljbi;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljbb;->c:[Ljbi;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v2, p0, Ljbb;->c:[Ljbi;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ljbb;->c:[Ljbi;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ljbb;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x4

    iget-object v2, p0, Ljbb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p0, Ljbb;->e:[Lipv;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljbb;->e:[Lipv;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    :goto_2
    iget-object v2, p0, Ljbb;->e:[Lipv;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ljbb;->e:[Lipv;

    aget-object v2, v2, v0

    if-eqz v2, :cond_5

    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Ljbb;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x6

    iget-object v2, p0, Ljbb;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_7
    iget-object v0, p0, Ljbb;->g:[Ljbe;

    if-eqz v0, :cond_9

    iget-object v0, p0, Ljbb;->g:[Ljbe;

    array-length v0, v0

    if-lez v0, :cond_9

    move v0, v1

    :goto_3
    iget-object v2, p0, Ljbb;->g:[Ljbe;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ljbb;->g:[Ljbe;

    aget-object v2, v2, v0

    if-eqz v2, :cond_8

    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    iget-object v0, p0, Ljbb;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const/16 v0, 0x9

    iget-object v2, p0, Ljbb;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_a
    iget-boolean v0, p0, Ljbb;->i:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xb

    iget-boolean v2, p0, Ljbb;->i:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_b
    iget-boolean v0, p0, Ljbb;->j:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xc

    iget-boolean v2, p0, Ljbb;->j:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_c
    iget-boolean v0, p0, Ljbb;->k:Z

    if-eqz v0, :cond_d

    const/16 v0, 0xd

    iget-boolean v2, p0, Ljbb;->k:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_d
    iget-object v0, p0, Ljbb;->l:[Ljai;

    if-eqz v0, :cond_f

    iget-object v0, p0, Ljbb;->l:[Ljai;

    array-length v0, v0

    if-lez v0, :cond_f

    move v0, v1

    :goto_4
    iget-object v2, p0, Ljbb;->l:[Ljai;

    array-length v2, v2

    if-ge v0, v2, :cond_f

    iget-object v2, p0, Ljbb;->l:[Ljai;

    aget-object v2, v2, v0

    if-eqz v2, :cond_e

    const/16 v3, 0xe

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_f
    iget-object v0, p0, Ljbb;->m:[Ljaj;

    if-eqz v0, :cond_11

    iget-object v0, p0, Ljbb;->m:[Ljaj;

    array-length v0, v0

    if-lez v0, :cond_11

    :goto_5
    iget-object v0, p0, Ljbb;->m:[Ljaj;

    array-length v0, v0

    if-ge v1, v0, :cond_11

    iget-object v0, p0, Ljbb;->m:[Ljaj;

    aget-object v0, v0, v1

    if-eqz v0, :cond_10

    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_11
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljbb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljbb;

    iget-object v2, p0, Ljbb;->a:[I

    iget-object v3, p1, Ljbb;->a:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljbb;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Ljbb;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljbb;->b:Ljava/lang/String;

    iget-object v3, p1, Ljbb;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljbb;->c:[Ljbi;

    iget-object v3, p1, Ljbb;->c:[Ljbi;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljbb;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Ljbb;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljbb;->d:Ljava/lang/String;

    iget-object v3, p1, Ljbb;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljbb;->e:[Lipv;

    iget-object v3, p1, Ljbb;->e:[Lipv;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljbb;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Ljbb;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljbb;->f:Ljava/lang/String;

    iget-object v3, p1, Ljbb;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Ljbb;->g:[Ljbe;

    iget-object v3, p1, Ljbb;->g:[Ljbe;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Ljbb;->h:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Ljbb;->h:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_d
    iget-object v2, p0, Ljbb;->h:Ljava/lang/String;

    iget-object v3, p1, Ljbb;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-boolean v2, p0, Ljbb;->i:Z

    iget-boolean v3, p1, Ljbb;->i:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget-boolean v2, p0, Ljbb;->j:Z

    iget-boolean v3, p1, Ljbb;->j:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-boolean v2, p0, Ljbb;->k:Z

    iget-boolean v3, p1, Ljbb;->k:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Ljbb;->l:[Ljai;

    iget-object v3, p1, Ljbb;->l:[Ljai;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-object v2, p0, Ljbb;->m:[Ljaj;

    iget-object v3, p1, Ljbb;->m:[Ljaj;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget-object v0, p0, Ljbb;->a:[I

    invoke-static {v0}, Lizq;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljbb;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljbb;->c:[Ljbi;

    invoke-static {v4}, Lizq;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljbb;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljbb;->e:[Lipv;

    invoke-static {v4}, Lizq;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljbb;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljbb;->g:[Ljbe;

    invoke-static {v4}, Lizq;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljbb;->h:Ljava/lang/String;

    if-nez v4, :cond_3

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Ljbb;->i:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Ljbb;->j:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Ljbb;->k:Z

    if-eqz v1, :cond_6

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ljbb;->l:[Ljai;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ljbb;->m:[Ljaj;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljbb;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljbb;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ljbb;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Ljbb;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v0, v3

    goto :goto_4

    :cond_5
    move v0, v3

    goto :goto_5

    :cond_6
    move v2, v3

    goto :goto_6
.end method
