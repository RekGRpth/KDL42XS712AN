.class public final Ljby;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Z

.field public c:Ljbx;

.field public d:Ljava/lang/String;

.field public e:Ljbo;

.field public f:Z

.field public g:Ljbm;

.field private h:Z

.field private i:Lizf;

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Ljbp;

.field private p:Z

.field private q:Z

.field private r:Ljbr;

.field private s:Z

.field private t:I

.field private u:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljby;->i:Lizf;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljby;->a:J

    iput-object v2, p0, Ljby;->c:Ljbx;

    const-string v0, ""

    iput-object v0, p0, Ljby;->l:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljby;->d:Ljava/lang/String;

    iput-object v2, p0, Ljby;->o:Ljbp;

    iput-object v2, p0, Ljby;->e:Ljbo;

    iput-object v2, p0, Ljby;->r:Ljbr;

    iput-object v2, p0, Ljby;->g:Ljbm;

    const/4 v0, 0x0

    iput v0, p0, Ljby;->t:I

    const/4 v0, -0x1

    iput v0, p0, Ljby;->u:I

    return-void
.end method

.method public static a([B)Ljby;
    .locals 2

    new-instance v0, Ljby;

    invoke-direct {v0}, Ljby;-><init>()V

    array-length v1, p0

    invoke-super {v0, p0, v1}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Ljby;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljby;->u:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljby;->b()I

    :cond_0
    iget v0, p0, Ljby;->u:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    iput-boolean v2, p0, Ljby;->h:Z

    iput-object v0, p0, Ljby;->i:Lizf;

    goto :goto_0

    :sswitch_2
    new-instance v0, Ljbx;

    invoke-direct {v0}, Ljbx;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljby;->b:Z

    iput-object v0, p0, Ljby;->c:Ljbx;

    goto :goto_0

    :sswitch_3
    new-instance v0, Ljbo;

    invoke-direct {v0}, Ljbo;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljby;->p:Z

    iput-object v0, p0, Ljby;->e:Ljbo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Ljby;->k:Z

    iput-object v0, p0, Ljby;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Ljby;->m:Z

    iput-object v0, p0, Ljby;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    new-instance v0, Ljbr;

    invoke-direct {v0}, Ljbr;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljby;->q:Z

    iput-object v0, p0, Ljby;->r:Ljbr;

    goto :goto_0

    :sswitch_7
    new-instance v0, Ljbm;

    invoke-direct {v0}, Ljbm;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljby;->f:Z

    iput-object v0, p0, Ljby;->g:Ljbm;

    goto :goto_0

    :sswitch_8
    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljby;->n:Z

    iput-object v0, p0, Ljby;->o:Ljbp;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    iput-boolean v2, p0, Ljby;->j:Z

    iput-wide v0, p0, Ljby;->a:J

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v2, p0, Ljby;->s:Z

    iput v0, p0, Ljby;->t:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x318 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Ljby;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljby;->i:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_0
    iget-boolean v0, p0, Ljby;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljby;->c:Ljbx;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_1
    iget-boolean v0, p0, Ljby;->p:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljby;->e:Ljbo;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    iget-boolean v0, p0, Ljby;->k:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljby;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Ljby;->m:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljby;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Ljby;->q:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljby;->r:Ljbr;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_5
    iget-boolean v0, p0, Ljby;->f:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Ljby;->g:Ljbm;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_6
    iget-boolean v0, p0, Ljby;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Ljby;->o:Ljbp;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_7
    iget-boolean v0, p0, Ljby;->j:Z

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget-wide v1, p0, Ljby;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_8
    iget-boolean v0, p0, Ljby;->s:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x63

    iget v1, p0, Ljby;->t:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_9
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljby;->h:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljby;->i:Lizf;

    invoke-static {v0, v1}, Lizh;->b(ILizf;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljby;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljby;->c:Ljbx;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljby;->p:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljby;->e:Ljbo;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljby;->k:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljby;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Ljby;->m:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljby;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Ljby;->q:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ljby;->r:Ljbr;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Ljby;->f:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Ljby;->g:Ljbm;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Ljby;->n:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget-object v2, p0, Ljby;->o:Ljbp;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Ljby;->j:Z

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    iget-wide v2, p0, Ljby;->a:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Ljby;->s:Z

    if-eqz v1, :cond_9

    const/16 v1, 0x63

    iget v2, p0, Ljby;->t:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Ljby;->u:I

    return v0
.end method
