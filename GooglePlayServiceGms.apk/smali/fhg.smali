.class public final Lfhg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfhf;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lfhg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 12

    const-wide/16 v10, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->n()Z

    invoke-virtual {v0}, Lfbo;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->e(Landroid/content/Context;)V

    invoke-static {p1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->m()Lfhp;

    move-result-object v4

    iget-object v0, v4, Lfhp;->b:Lfjl;

    invoke-virtual {v0}, Lfjl;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjm;

    iget-object v1, v0, Lfjm;->a:Ljava/lang/String;

    iget-object v6, v4, Lfhp;->a:Landroid/content/Context;

    invoke-static {v6}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v6

    invoke-virtual {v6}, Lfbc;->a()Lfbe;

    move-result-object v6

    invoke-virtual {v6, v3}, Lfbe;->c(Z)J

    move-result-wide v7

    invoke-static {v7, v8, v3}, Lfhp;->a(JZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v2}, Lfbe;->c(Z)J

    move-result-wide v8

    invoke-static {v8, v9, v2}, Lfhp;->a(JZ)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v7, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    move v1, v3

    :goto_1
    if-nez v1, :cond_0

    iget-object v1, v4, Lfhp;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v1

    invoke-virtual {v1}, Lfbo;->e()Lfbk;

    move-result-object v1

    iget-object v6, v0, Lfjm;->a:Ljava/lang/String;

    iget-object v7, v0, Lfjm;->b:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v1, v10, v6

    if-nez v1, :cond_0

    iget-object v1, v4, Lfhp;->b:Lfjl;

    iget-object v6, v0, Lfjm;->a:Ljava/lang/String;

    iget-object v0, v0, Lfjm;->b:Ljava/lang/String;

    invoke-virtual {v1, v6, v0}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    invoke-static {p1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->n()Lfhq;

    move-result-object v1

    iget-object v0, v1, Lfhq;->b:Lfjn;

    invoke-virtual {v0}, Lfjn;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjo;

    iget-object v3, v1, Lfhq;->a:Landroid/content/Context;

    invoke-static {v3}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v3

    invoke-virtual {v3}, Lfbo;->e()Lfbk;

    move-result-object v3

    iget-object v4, v0, Lfjo;->a:Ljava/lang/String;

    iget-object v5, v0, Lfjo;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v3

    cmp-long v3, v10, v3

    if-nez v3, :cond_4

    iget-object v3, v1, Lfhq;->b:Lfjn;

    iget-object v4, v0, Lfjo;->a:Ljava/lang/String;

    iget-object v0, v0, Lfjo;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lfjn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    return-void
.end method
