.class public abstract Lbod;
.super Lcb;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;


# instance fields
.field public a:Lbbq;

.field private b:Z

.field private c:Lbbo;

.field private d:Lbgo;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcb;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbod;->e:Ljava/util/ArrayList;

    return-void
.end method

.method private a(Lbgo;Z)V
    .locals 2

    iget-boolean v0, p0, Lcb;->r:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lbgo;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbod;->d:Lbgo;

    iput-object p1, p0, Lbod;->d:Lbgo;

    iget-boolean v1, p0, Lcb;->p:Z

    if-eqz v1, :cond_2

    invoke-super {p0, p1}, Lcb;->b(Ljava/lang/Object;)V

    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    iget-object v1, p0, Lbod;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lbod;->d()V

    goto :goto_0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lbod;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lbod;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgo;

    invoke-virtual {v0}, Lbgo;->b()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbod;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method


# virtual methods
.method public P_()V
    .locals 1

    iget-boolean v0, p0, Lcb;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbod;->m_()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbod;->b:Z

    return-void
.end method

.method protected abstract a(Landroid/content/Context;Lbbr;Lbbs;)Lbbq;
.end method

.method public a()V
    .locals 1

    invoke-super {p0}, Lcb;->a()V

    iget-object v0, p0, Lbod;->a:Lbbq;

    invoke-interface {v0}, Lbbq;->d_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbod;->a:Lbbq;

    invoke-virtual {p0, v0}, Lbod;->a(Lbbq;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lbod;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbod;->a:Lbbq;

    invoke-interface {v0}, Lbbq;->a()V

    goto :goto_0
.end method

.method public final a(Lbbo;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbod;->b:Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbod;->a(Lbbo;Lbgo;)V

    return-void
.end method

.method protected final a(Lbbo;Lbgo;)V
    .locals 1

    iput-object p1, p0, Lbod;->c:Lbbo;

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lbod;->a(Lbgo;Z)V

    return-void
.end method

.method protected final a(Lbbo;Lbgo;Z)V
    .locals 1

    iput-object p1, p0, Lbod;->c:Lbbo;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lbod;->a(Lbgo;Z)V

    return-void
.end method

.method protected abstract a(Lbbq;)V
.end method

.method public final b()Lbbo;
    .locals 1

    iget-object v0, p0, Lbod;->c:Lbbo;

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lbgo;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lbod;->a(Lbgo;Z)V

    return-void
.end method

.method public b_(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbod;->b:Z

    iget-object v0, p0, Lbod;->a:Lbbq;

    invoke-virtual {p0, v0}, Lbod;->a(Lbbq;)V

    return-void
.end method

.method protected final c()Z
    .locals 1

    iget-object v0, p0, Lbod;->a:Lbbq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbod;->a:Lbbq;

    invoke-interface {v0}, Lbbq;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lbod;->a:Lbbq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbod;->a:Lbbq;

    invoke-interface {v0}, Lbbq;->d_()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lbod;->b:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lbod;->a:Lbbq;

    invoke-interface {v0}, Lbbq;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbod;->b:Z

    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 1

    invoke-super {p0}, Lcb;->g()V

    invoke-virtual {p0}, Lbod;->f()V

    iget-object v0, p0, Lbod;->d:Lbgo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbod;->d:Lbgo;

    invoke-virtual {v0}, Lbgo;->b()V

    invoke-direct {p0}, Lbod;->d()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbod;->d:Lbgo;

    return-void
.end method

.method public m_()V
    .locals 2

    iget-object v0, p0, Lbod;->a:Lbbq;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcb;->o:Landroid/content/Context;

    invoke-virtual {p0, v0, p0, p0}, Lbod;->a(Landroid/content/Context;Lbbr;Lbbs;)Lbbq;

    move-result-object v0

    iput-object v0, p0, Lbod;->a:Lbbq;

    :cond_0
    iget-object v0, p0, Lbod;->d:Lbgo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbod;->d:Lbgo;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lbod;->a(Lbgo;Z)V

    :cond_1
    invoke-virtual {p0}, Lbod;->j()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbod;->d:Lbgo;

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcb;->a()V

    :cond_3
    return-void
.end method
