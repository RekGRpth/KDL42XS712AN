.class public abstract Ldvp;
.super Lah;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lbdx;
.implements Lbdy;


# instance fields
.field protected Y:Ldvn;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lah;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldvp;->i:Z

    return-void
.end method


# virtual methods
.method public E_()V
    .locals 1

    invoke-super {p0}, Lah;->E_()V

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0, p0}, Ldvn;->a(Lbdx;)V

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0, p0}, Ldvn;->a(Lbdy;)V

    return-void
.end method

.method protected final J()Lbdu;
    .locals 1

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->j()Lbdu;

    move-result-object v0

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    return-object v0
.end method

.method public final K()Z
    .locals 1

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->k()Z

    move-result v0

    return v0
.end method

.method protected final L()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final M()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->m()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method should only be called in a headless context"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final N()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->m()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method should only be called in a headless context"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final O()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->m()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method should only be called in a headless context"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbbo;)V
    .locals 0

    return-void
.end method

.method public abstract a(Lbdu;)V
.end method

.method public a(Lcom/google/android/gms/games/Game;)V
    .locals 0

    return-void
.end method

.method public final c(I)V
    .locals 2

    const-string v0, "GamesListFragment"

    const-string v1, "Unexpected call to onConnectionSuspended - subclasses should unregister as a listener in onStop() and clear data in onDestroyView()"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lah;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Ldvn;

    iput-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {p0}, Ldvp;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-virtual {p0}, Ldvp;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method public final e(Z)V
    .locals 2

    invoke-super {p0, p1}, Lah;->e(Z)V

    invoke-virtual {p0}, Ldvp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v1, v0, Ldvj;

    if-eqz v1, :cond_0

    check-cast v0, Ldvj;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Ldvj;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public g_()V
    .locals 1

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0, p0}, Ldvn;->b(Lbdx;)V

    iget-object v0, p0, Ldvp;->Y:Ldvn;

    invoke-virtual {v0, p0}, Ldvn;->b(Lbdy;)V

    invoke-super {p0}, Lah;->g_()V

    return-void
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Ldvp;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    invoke-static {v1}, Lbiq;->a(Z)V

    invoke-virtual {p0, v0}, Ldvp;->a(Lbdu;)V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    const/4 v2, 0x2

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    iget-boolean v1, p0, Ldvp;->i:Z

    if-nez v1, :cond_1

    if-ne p2, v2, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Ldvp;->i:Z

    instance-of v1, v0, Ldvh;

    if-eqz v1, :cond_0

    check-cast v0, Ldvh;

    invoke-interface {v0}, Ldvh;->i()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Ldvp;->i:Z

    if-eqz v1, :cond_0

    if-eq p2, v2, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Ldvp;->i:Z

    instance-of v1, v0, Ldvh;

    if-eqz v1, :cond_0

    check-cast v0, Ldvh;

    invoke-interface {v0}, Ldvh;->j()V

    goto :goto_0
.end method
