.class public final Lgsn;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/ApplicationParameters;
    .locals 5

    const/4 v1, 0x0

    invoke-static {p0}, Lgsn;->b(Landroid/os/Bundle;)I

    move-result v4

    if-eqz p0, :cond_0

    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    move-object v3, v0

    :goto_0
    if-eqz p0, :cond_1

    const-string v0, "com.google.android.gms.wallet.EXTRA_ALLOW_ACCOUNT_SELECTION"

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    move v2, v0

    :goto_1
    if-eqz p0, :cond_2

    const-string v0, "com.google.android.gms.wallet.EXTRA_THEME"

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_2
    invoke-static {}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->a()Lhgd;

    move-result-object v1

    invoke-virtual {v1, v4}, Lhgd;->a(I)Lhgd;

    move-result-object v1

    invoke-virtual {v1, v3}, Lhgd;->a(Landroid/accounts/Account;)Lhgd;

    move-result-object v1

    invoke-virtual {v1, p0}, Lhgd;->a(Landroid/os/Bundle;)Lhgd;

    move-result-object v1

    invoke-virtual {v1, v2}, Lhgd;->a(Z)Lhgd;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhgd;->b(I)Lhgd;

    move-result-object v0

    iget-object v0, v0, Lhgd;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static b(Landroid/os/Bundle;)I
    .locals 2

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    const-string v1, "com.google.android.gms.wallet.EXTRA_ENVIRONMENT"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :cond_0
    return v0
.end method
