.class public final Lgse;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "com.google.android.gms.wallet.cache.MerchantCache"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgse;->a:Landroid/content/SharedPreferences;

    return-void
.end method

.method public static b(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {}, Lgtq;->a()Lgts;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgts;->a(Ljava/lang/String;)Lgts;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgts;->a(I)Lgts;

    move-result-object v0

    invoke-virtual {v0}, Lgts;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)Lgsf;
    .locals 8

    const/4 v3, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lgsf;

    invoke-direct {v0}, Lgsf;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lgse;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lgse;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgse;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v3, p0, Lgse;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, p2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    new-instance v3, Lgsf;

    invoke-direct {v3}, Lgsf;-><init>()V

    if-eqz v0, :cond_2

    invoke-static {v0}, Lgtq;->a(Ljava/lang/String;)Lgtr;

    move-result-object v5

    invoke-virtual {v5}, Lgtr;->a()I

    move-result v0

    iput v0, v3, Lgsf;->a:I

    iget v0, v3, Lgsf;->a:I

    if-le v0, v6, :cond_4

    invoke-virtual {v5, v2}, Lgtr;->a(Z)Z

    move-result v0

    iput-boolean v0, v3, Lgsf;->b:Z

    :goto_1
    invoke-virtual {v5}, Lgtr;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lgsf;->c:Ljava/lang/String;

    iget v0, v3, Lgsf;->a:I

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    invoke-virtual {v5, v1}, Lgtr;->a(Z)Z

    move-result v0

    iput-boolean v0, v3, Lgsf;->d:Z

    :cond_2
    iget v0, v3, Lgsf;->a:I

    if-ge v0, v7, :cond_3

    iput v7, v3, Lgsf;->a:I

    iget-object v0, p0, Lgse;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v3}, Lgsf;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_3
    move-object v0, v3

    goto :goto_0

    :cond_4
    iget v0, v3, Lgsf;->a:I

    if-ne v0, v6, :cond_6

    invoke-virtual {v5}, Lgtr;->a()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Lgsf;->b:Z

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    iget v0, v3, Lgsf;->a:I

    if-ne v0, v1, :cond_7

    move v2, v1

    :cond_7
    iput-boolean v2, v3, Lgsf;->b:Z

    iput v1, v3, Lgsf;->a:I

    goto :goto_1
.end method
