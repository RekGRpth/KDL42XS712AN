.class public final Ljbx;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:I

.field public c:Z

.field public d:Ljbn;

.field public e:Z

.field public f:Ljbl;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Ljbq;

.field private o:Z

.field private p:I

.field private q:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljbx;->a:J

    const/4 v0, 0x1

    iput v0, p0, Ljbx;->b:I

    const-string v0, ""

    iput-object v0, p0, Ljbx;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbx;->l:Ljava/lang/String;

    iput-object v2, p0, Ljbx;->d:Ljbn;

    iput-object v2, p0, Ljbx;->n:Ljbq;

    const/4 v0, 0x0

    iput v0, p0, Ljbx;->p:I

    iput-object v2, p0, Ljbx;->f:Ljbl;

    const/4 v0, -0x1

    iput v0, p0, Ljbx;->q:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljbx;->q:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljbx;->b()I

    :cond_0
    iget v0, p0, Ljbx;->q:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->j()J

    move-result-wide v0

    iput-boolean v2, p0, Ljbx;->g:Z

    iput-wide v0, p0, Ljbx;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v2, p0, Ljbx;->h:Z

    iput v0, p0, Ljbx;->b:I

    goto :goto_0

    :sswitch_3
    new-instance v0, Ljbn;

    invoke-direct {v0}, Ljbn;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbx;->c:Z

    iput-object v0, p0, Ljbx;->d:Ljbn;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Ljbx;->i:Z

    iput-object v0, p0, Ljbx;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Ljbx;->k:Z

    iput-object v0, p0, Ljbx;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    new-instance v0, Ljbq;

    invoke-direct {v0}, Ljbq;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbx;->m:Z

    iput-object v0, p0, Ljbx;->n:Ljbq;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v2, p0, Ljbx;->o:Z

    iput v0, p0, Ljbx;->p:I

    goto :goto_0

    :sswitch_8
    new-instance v0, Ljbl;

    invoke-direct {v0}, Ljbl;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbx;->e:Z

    iput-object v0, p0, Ljbx;->f:Ljbl;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Ljbx;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Ljbx;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->b(IJ)V

    :cond_0
    iget-boolean v0, p0, Ljbx;->h:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljbx;->b:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_1
    iget-boolean v0, p0, Ljbx;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljbx;->d:Ljbn;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    iget-boolean v0, p0, Ljbx;->i:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljbx;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Ljbx;->k:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljbx;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Ljbx;->m:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljbx;->n:Ljbq;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_5
    iget-boolean v0, p0, Ljbx;->o:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Ljbx;->p:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_6
    iget-boolean v0, p0, Ljbx;->e:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Ljbx;->f:Ljbl;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_7
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljbx;->g:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Ljbx;->a:J

    invoke-static {v0}, Lizh;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljbx;->h:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljbx;->b:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljbx;->c:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljbx;->d:Ljbn;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljbx;->i:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljbx;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Ljbx;->k:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljbx;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Ljbx;->m:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ljbx;->n:Ljbq;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Ljbx;->o:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Ljbx;->p:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Ljbx;->e:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Ljbx;->f:Ljbl;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Ljbx;->q:I

    return v0
.end method
