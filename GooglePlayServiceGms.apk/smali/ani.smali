.class public final Lani;
.super Lanm;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/authzen/keyservice/AuthZenSecretProviderService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/auth/authzen/keyservice/AuthZenSecretProviderService;)V
    .locals 0

    iput-object p1, p0, Lani;->a:Lcom/google/android/gms/auth/authzen/keyservice/AuthZenSecretProviderService;

    invoke-direct {p0}, Lanm;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/auth/authzen/keyservice/AuthZenSecretProviderService;B)V
    .locals 0

    invoke-direct {p0, p1}, Lani;-><init>(Lcom/google/android/gms/auth/authzen/keyservice/AuthZenSecretProviderService;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lanx;->a()Lanx;

    move-result-object v1

    sget-object v2, Lany;->a:Lany;

    iget-object v3, v1, Lanx;->a:Landroid/content/Context;

    invoke-static {v3}, Lanh;->c(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v1, "AuthZen"

    const-string v2, "Authzen is disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    new-instance v1, Lano;

    iget-object v2, p0, Lani;->a:Lcom/google/android/gms/auth/authzen/keyservice/AuthZenSecretProviderService;

    invoke-direct {v1, v2}, Lano;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Lano;->b(Ljava/lang/String;)Lanj;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v1, "AuthZenSecretProviderService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get key for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v0

    :cond_1
    iget-object v3, v1, Lanx;->a:Landroid/content/Context;

    invoke-static {v3}, Lbov;->b(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Lanx;->a:Landroid/content/Context;

    invoke-static {v3, p1}, Lbov;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "AuthZen"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot initiate enrollment because account does not exist: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :try_start_1
    const-string v3, "AuthZen"

    const-string v4, "Fetching signing key..."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, v1, Lanx;->b:Lano;

    const-string v4, "device_key"

    invoke-virtual {v3, v4}, Lano;->a(Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v3

    const-string v4, "AuthZen"

    const-string v5, "Signing key fetched successfuly!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Landroid/accounts/Account;

    const-string v5, "com.google"

    invoke-direct {v4, p1, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v4, v3}, Lanx;->a(Lany;Landroid/accounts/Account;Ljava/security/KeyPair;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lanp; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "AuthZen"

    const-string v3, "Error while fetching key."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "AuthZenSecretProviderService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected exception for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_2
    move-exception v1

    :try_start_3
    const-string v2, "AuthZen"

    const-string v3, "Error while creating key."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :cond_3
    iget-object v2, v1, Lanj;->a:Lank;

    iget-object v2, v2, Lank;->b:Ljavax/crypto/SecretKey;

    invoke-static {v2}, Lcom/google/android/gms/auth/authzen/keyservice/AuthZenSecretProviderService;->a(Ljavax/crypto/SecretKey;)Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v2

    if-nez v2, :cond_4

    const-string v1, "AuthZenSecretProviderService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to derive secret for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    const-string v3, "otp_secret"

    invoke-virtual {v2}, Ljavax/crypto/spec/SecretKeySpec;->getEncoded()[B

    move-result-object v2

    invoke-virtual {p2, v3, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "creation_time_millis"

    iget-object v1, v1, Lanj;->a:Lank;

    iget-wide v3, v1, Lank;->c:J

    invoke-virtual {p2, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    const/4 v0, 0x1

    goto/16 :goto_1
.end method
