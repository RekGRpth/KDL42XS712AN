.class public final Lbng;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lbnl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    sput-object v0, Lbng;->a:Lbnl;

    return-void
.end method

.method public static a(Lsp;Ljava/lang/String;)Lbnf;
    .locals 6

    const/4 v1, 0x0

    invoke-static {p0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lsp;->a:Lrz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsp;->a:Lrz;

    iget-object v0, v0, Lrz;->b:[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsp;->a:Lrz;

    iget-object v0, v0, Lrz;->b:[B

    :try_start_0
    invoke-static {v0}, Lbpm;->a([B)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/util/zip/GZIPInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbnu; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Lbnh;

    invoke-direct {v0}, Lbnh;-><init>()V

    sget-object v3, Lbng;->a:Lbnl;

    invoke-virtual {v3, v2, v0}, Lbnl;->a(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    invoke-virtual {v0}, Lbnh;->b()Lbnf;

    move-result-object v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbnf;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lbnu; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    return-object v0

    :cond_1
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Error in GZIP header, bad magic code"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lbnu; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_0
    move-exception v0

    move-object v2, v1

    :goto_1
    :try_start_4
    const-string v3, "ErrorUtils"

    const-string v4, "Unable to read error response"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v2, :cond_2

    :try_start_5
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_2
    :goto_2
    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "ErrorUtils"

    const-string v3, "Failed to close input stream"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v2, "ErrorUtils"

    const-string v3, "Failed to close input stream"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v2, v1

    :goto_3
    :try_start_6
    iget-object v0, p0, Lsp;->a:Lrz;

    iget v0, v0, Lrz;->a:I

    const-string v3, "ErrorUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Received generic error from server: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v2, :cond_2

    :try_start_7
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    :catch_4
    move-exception v0

    const-string v2, "ErrorUtils"

    const-string v3, "Failed to close input stream"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_4
    if-eqz v2, :cond_3

    :try_start_8
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_3
    :goto_5
    throw v0

    :catch_5
    move-exception v1

    const-string v2, "ErrorUtils"

    const-string v3, "Failed to close input stream"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v0

    goto :goto_3

    :catch_7
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lsp;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lsp;->a:Lrz;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lsp;->a:Lrz;

    iget v1, v1, Lrz;->a:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x190
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lsp;I)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lsp;->a:Lrz;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lsp;->a:Lrz;

    iget v1, v1, Lrz;->a:I

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lsp;Ljava/lang/String;)Lbne;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p0, p1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lbnf;->getErrors()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbne;

    goto :goto_0
.end method
