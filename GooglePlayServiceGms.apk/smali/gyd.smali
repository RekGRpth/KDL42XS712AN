.class public final Lgyd;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgwv;


# instance fields
.field a:Landroid/accounts/Account;

.field b:Lgye;

.field c:[Ljava/lang/String;

.field d:I

.field e:Lgyf;

.field f:Lgwr;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lgyd;->d:I

    return-void
.end method

.method private J()Z
    .locals 2

    iget v0, p0, Lgyd;->d:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lgyd;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private K()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lgyd;->c:[Ljava/lang/String;

    iget v1, p0, Lgyd;->d:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method private L()V
    .locals 4

    new-instance v0, Lgye;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lgye;-><init>(Lgyd;Landroid/content/Context;)V

    iput-object v0, p0, Lgyd;->b:Lgye;

    iget-object v0, p0, Lgyd;->b:Lgye;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Lgyd;->K()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lgye;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public static a(Landroid/accounts/Account;[Ljava/lang/String;)Lgyd;
    .locals 3

    new-instance v0, Lgyd;

    invoke-direct {v0}, Lgyd;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "tokenTypes"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lgyd;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lgyd;->c:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgyd;->c(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lgyd;->J()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "No TokenTypes remain"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lgyd;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgyd;->d:I

    invoke-direct {p0}, Lgyd;->L()V

    goto :goto_0
.end method


# virtual methods
.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lgyd;->e:Lgyf;

    return-void
.end method

.method final a()V
    .locals 3

    iget-object v0, p0, Lgyd;->f:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgyd;->f:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lgyd;->f:Lgwr;

    iget-object v0, p0, Lgyd;->f:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lgyd;->f:Lgwr;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "RetrieveAuthTokensFragment.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public final a(II)V
    .locals 3

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lgyd;->e:Lgyf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyd;->e:Lgyf;

    invoke-interface {v0}, Lgyf;->f()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lgyd;->L()V

    goto :goto_0

    :cond_1
    const-string v0, "RetrieveAuthTokensFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    const/16 v1, 0x1f5

    if-ne p1, v1, :cond_0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgyd;->c(I)V

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_1

    const-string v0, "authtoken"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, v0}, Lgyd;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lgyd;->e:Lgyf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyd;->e:Lgyf;

    invoke-interface {v0}, Lgyf;->f()V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lgyd;->c(I)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lgyd;->c(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    :try_start_0
    move-object v0, p1

    check-cast v0, Lgyf;

    move-object v1, v0

    iput-object v1, p0, Lgyd;->e:Lgyf;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement OnAuthTokensRetrievedListener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method final a(Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lgyd;->L()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lgry;->a()Lgry;

    move-result-object v0

    iget-object v1, p0, Lgyd;->a:Landroid/accounts/Account;

    invoke-direct {p0}, Lgyd;->K()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lgry;->b:Ljava/util/HashSet;

    monitor-enter v3

    :try_start_0
    iget-object v4, v0, Lgry;->b:Ljava/util/HashSet;

    invoke-static {v1, v2}, Lgry;->b(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, v0, Lgry;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lgyd;->J()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lgyd;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    iget-object v0, p0, Lgyd;->e:Lgyf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyd;->e:Lgyf;

    invoke-interface {v0}, Lgyf;->e()V

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lgyd;->q()V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "Fragment requires arguments!"

    invoke-static {v0, v2}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Fragment requires account extra!"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lgyd;->a:Landroid/accounts/Account;

    const-string v0, "tokenTypes"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Fragment requires tokenTypes extra!"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Ljava/util/HashSet;

    const-string v2, "tokenTypes"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lgyd;->c:[Ljava/lang/String;

    invoke-static {}, Lgry;->a()Lgry;

    move-result-object v0

    iget-object v1, p0, Lgyd;->a:Landroid/accounts/Account;

    iget-object v2, p0, Lgyd;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgry;->a(Landroid/accounts/Account;[Ljava/lang/String;)V

    return-void
.end method

.method final c(I)V
    .locals 1

    iget-object v0, p0, Lgyd;->e:Lgyf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyd;->e:Lgyf;

    invoke-interface {v0, p1}, Lgyf;->a(I)V

    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget v0, p0, Lgyd;->d:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    invoke-direct {p0}, Lgyd;->b()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "RetrieveAuthTokensFragment.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lgyd;->f:Lgwr;

    iget-object v0, p0, Lgyd;->f:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyd;->f:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    return-void
.end method
