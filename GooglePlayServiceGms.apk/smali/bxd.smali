.class public abstract Lbxd;
.super Lbxe;
.source "SourceFile"

# interfaces
.implements Lcbc;


# instance fields
.field private p:Z

.field private final q:Lcaz;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lbxe;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxd;->p:Z

    new-instance v0, Lcaz;

    new-instance v1, Lcbd;

    invoke-direct {v1}, Lcbd;-><init>()V

    invoke-direct {v0, p0, v1}, Lcaz;-><init>(Lcbc;Lcbe;)V

    iput-object v0, p0, Lbxd;->q:Lcaz;

    return-void
.end method


# virtual methods
.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lbxd;->q:Lcaz;

    iget-object v0, v0, Lcaz;->e:Lcce;

    invoke-interface {v0}, Lcce;->b()V

    invoke-super {p0}, Lbxe;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxd;->p:Z

    iget-object v0, p0, Lbxd;->q:Lcaz;

    iget-object v1, v0, Lcaz;->b:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcaz;->d:Lcbe;

    iget-object v2, v0, Lcaz;->b:Landroid/database/ContentObserver;

    invoke-interface {v1, p0, v2}, Lcbe;->b(Landroid/content/Context;Landroid/database/ContentObserver;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcaz;->b:Landroid/database/ContentObserver;

    :cond_0
    invoke-super {p0}, Lbxe;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Lbxe;->onResume()V

    iget-object v0, p0, Lbxd;->q:Lcaz;

    iget-boolean v1, p0, Lbxd;->p:Z

    iget-object v2, v0, Lcaz;->b:Landroid/database/ContentObserver;

    if-nez v2, :cond_0

    new-instance v2, Lcbb;

    iget-object v3, v0, Lcaz;->a:Landroid/os/Handler;

    invoke-direct {v2, v0, v3}, Lcbb;-><init>(Lcaz;Landroid/os/Handler;)V

    iput-object v2, v0, Lcaz;->b:Landroid/database/ContentObserver;

    iget-object v2, v0, Lcaz;->d:Lcbe;

    iget-object v3, v0, Lcaz;->b:Landroid/database/ContentObserver;

    invoke-interface {v2, p0, v3}, Lcbe;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcaz;->e:Lcce;

    invoke-interface {v0}, Lcce;->a()V

    :cond_0
    return-void
.end method
