.class public final Lcpr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcpr;->a:Ljava/lang/String;

    iput-object p2, p0, Lcpr;->b:Landroid/content/Context;

    iput-object p3, p0, Lcpr;->c:Ljava/lang/Throwable;

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;[B)[B
    .locals 16

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v4, v2

    if-ge v1, v4, :cond_3

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "em.V"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "j"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "d.g"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    const-string v5, "a"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "r.e"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/16 v5, 0x1e

    if-ne v4, v5, :cond_2

    :cond_1
    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v2, v1

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v2, v1

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcpy;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "$"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcpy;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x66

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    const/16 v6, 0x4d

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2, v3}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v10, Lcpy;

    invoke-direct {v10, v7}, Lcpy;-><init>(Ljava/lang/String;)V

    new-instance v11, Lcpy;

    const-class v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v11, v1}, Lcpy;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcps;

    invoke-direct {v1, v10}, Lcps;-><init>(Lcpy;)V

    invoke-virtual {v1}, Lcps;->a()Ljava/lang/String;

    move-result-object v12

    const-string v6, ""

    const-wide/16 v4, 0x0

    const-string v3, ""

    const-string v2, ""

    const-string v1, ""

    if-nez p2, :cond_4

    new-instance v1, Lcpu;

    invoke-direct {v1}, Lcpu;-><init>()V

    invoke-static {v10}, Lcpu;->a(Lcpy;)Ljava/lang/String;

    move-result-object v6

    new-instance v1, Lcpz;

    invoke-direct {v1}, Lcpz;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long v4, v1, v3

    new-instance v1, Lcpm;

    invoke-direct {v1, v11}, Lcpm;-><init>(Lcpy;)V

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcpm;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    new-instance v1, Lcpn;

    invoke-direct {v1}, Lcpn;-><init>()V

    invoke-static {}, Lcpn;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lcpo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcpr;->b:Landroid/content/Context;

    invoke-direct {v1, v13}, Lcpo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcpo;->a()Ljava/lang/String;

    move-result-object v1

    :cond_4
    const/4 v13, 0x7

    new-array v13, v13, [I

    fill-array-data v13, :array_1

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v11, v13}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v7, v13}, Lbpv;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v13, Lcpq;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcpr;->b:Landroid/content/Context;

    invoke-direct {v13, v10, v14}, Lcpq;-><init>(Lcpy;Landroid/content/Context;)V

    if-nez p2, :cond_5

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v10, v14

    const/4 v14, 0x1

    aput-object v6, v10, v14

    const/4 v14, 0x2

    aput-object v1, v10, v14

    const/4 v14, 0x3

    aput-object v3, v10, v14

    invoke-virtual {v13, v7, v10}, Lcpq;->a(Ljava/lang/String;[Ljava/lang/String;)Lcpp;

    move-result-object v7

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v15, "5="

    invoke-direct {v10, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "\n7="

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v12, v7, Lcpp;->a:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "\n8="

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v12, v7, Lcpp;->b:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "\n9="

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v7, v7, Lcpp;->c:Ljava/lang/String;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "\n"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    if-nez p2, :cond_8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcpr;->c:Ljava/lang/Throwable;

    invoke-virtual {v10}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v10

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, "0="

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcpr;->a:Ljava/lang/String;

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, "\n1="

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n2="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n3="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n4="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n6="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n10="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sub-long v2, v13, v8

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n11="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcpr;->c:Ljava/lang/Throwable;

    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n12="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v10, :cond_6

    const-string v1, ""

    :goto_2
    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    if-eqz p2, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    :goto_4
    return-object v1

    :cond_5
    move-object/from16 v0, p2

    invoke-virtual {v13, v7, v0}, Lcpq;->a(Ljava/lang/String;[B)Lcpp;

    move-result-object v7

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v10}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_7
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lbpv;->a([B)[B

    move-result-object v2

    const/16 v1, 0x43

    new-array v1, v1, [I

    fill-array-data v1, :array_2

    invoke-virtual {v11, v1}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    :try_start_0
    new-instance v3, Lcpv;

    invoke-direct {v3, v11}, Lcpv;-><init>(Lcpy;)V

    invoke-virtual {v3, v2}, Lcpv;->a([B)[B
    :try_end_0
    .catch Lcpw; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_4

    :catch_0
    move-exception v2

    goto :goto_4

    :cond_8
    move-object v1, v7

    goto :goto_3

    nop

    :array_0
    .array-data 4
        0x49
        0xc4
        0xaa
        0x9e
        0xb4
        0xb6
        0xc5
        0xaa
        0x98
        0x25
        0xb0
        0xa1
        0x8f
        0xcb
        0xcc
        0xc5
        0x98
        0xc0
        0xb1
        0x6b
        0xcd
        0xb9
        0xce
        0xb2
        0xc1
        0xb1
        0x25
        0x84
        0x84
        0xca
        0xa9
        0xb2
        0xb0
        0x98
        0xba
        0x72
        0x7d
        0xa9
        0xc7
        0xb1
        0xd1
        0xb9
        0x25
        0xc3
        0xb9
        0xc1
        0xb0
        0xca
        0xc7
        0x6b
        0xbe
        0x84
        0x7f
        0xbe
        0xa5
        0xf
        0x25
        0xc0
        0xb2
        0xca
        0xc3
        0xc5
        0xd0
        0xb4
        0xcc
        0xb2
        0xb5
        0xaa
        0x55
        0x8f
        0xa9
        0xc1
        0xc5
        0xd0
        0xc0
        0xc3
        0xc2
        0x78
        0xbf
        0xc2
        0x8b
        0xc9
        0xd0
        0xcb
        0xaa
        0xc5
        0xb9
        0x4
        0xc0
        0xb0
        0xb9
        0xa9
        0x84
        0x98
        0xb1
        0xaa
        0xc2
        0xbc
        0xcd
        0x8f
        0xd1
        0x8f
    .end array-data

    :array_1
    .array-data 4
        0x32
        0x1b
        0x2d
        0x30
        0x5f
        0x74
        0x56
    .end array-data

    :array_2
    .array-data 4
        0x1d
        0x9
        0x6c
        0x9
        0x73
        0x6f
        0x6e
        0x68
        0x55
        0x9
        0x5d
        0xe
        0x4a
        0x49
        0x70
        0x75
        0x59
        0x39
        0x24
        0x4d
        0x67
        0x3e
        0x4c
        0x68
        0x70
        0x24
        0x6b
        0x5b
        0x39
        0x4a
        0x66
        0x62
        0x39
        0x5e
        0x4c
        0x45
        0x3e
        0x1e
        0x71
        0x75
        0x6c
        0x48
        0x6c
        0x66
        0x71
        0x71
        0x6c
        0x60
        0x63
        0x65
        0x73
        0x4c
        0x66
        0x35
        0x5e
        0x4a
        0x60
        0x7
        0x65
        0x69
        0xf
        0x74
        0x69
        0x48
        0x4b
        0x39
        0x1e
    .end array-data
.end method
