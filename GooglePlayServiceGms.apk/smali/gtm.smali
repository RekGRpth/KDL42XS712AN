.class public final Lgtm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/content/IntentFilter;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lgto;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgtm;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lgto;

    invoke-direct {v0}, Lgto;-><init>()V

    invoke-direct {p0, p1, v0}, Lgtm;-><init>(Landroid/content/Context;Lgto;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lgto;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgtm;->b:Landroid/content/Context;

    iput-object p2, p0, Lgtm;->c:Lgto;

    return-void
.end method

.method private static a(Leqf;Lgtn;)Ljee;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Leqf;->a()V

    :try_start_0
    invoke-virtual {p1}, Lgtn;->a()V

    iget-object v1, p0, Leqf;->a:Lera;

    invoke-virtual {v1}, Lera;->d_()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "RiskAdvisoryDataProvider"

    const-string v2, "failed to connect to location client"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Leqf;->b()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Leqf;->a:Lera;

    invoke-virtual {v1}, Lera;->e()Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v0, Ljee;

    invoke-direct {v0}, Ljee;-><init>()V

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v0, Ljee;->b:D

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iput-wide v2, v0, Ljee;->c:D

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    long-to-double v2, v2

    iput-wide v2, v0, Ljee;->e:D

    invoke-virtual {v1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    iput-wide v2, v0, Ljee;->a:D

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    iput v1, v0, Ljee;->d:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    invoke-virtual {p0}, Leqf;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Leqf;->b()V

    throw v0
.end method

.method private a(Ljed;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lgtm;->c:Lgto;

    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p1, Ljed;->i:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p1, Ljed;->i:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "RiskAdvisoryDataProvider"

    const-string v2, "Unable to retrieve network interfaces"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-void
.end method

.method private static a(Ljed;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 5

    if-eqz p2, :cond_2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Ljed;->a:[Ljef;

    new-instance v2, Ljef;

    invoke-direct {v2}, Ljef;-><init>()V

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v3, v2, Ljef;->a:Ljava/lang/String;

    :cond_0
    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Ljef;->b:Ljava/lang/String;

    iget-wide v3, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iput-wide v3, v2, Ljef;->d:J

    iget-wide v3, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    iput-wide v3, v2, Ljef;->c:J

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_1

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v0, v2, Ljef;->e:Ljava/lang/String;

    :cond_1
    invoke-static {v1, v2}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljef;

    iput-object v0, p0, Ljed;->a:[Ljef;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "RiskAdvisoryDataProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package info not found for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    const/4 v10, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Lgtn;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    invoke-direct {v3, v0}, Lgtn;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    new-instance v4, Leqf;

    iget-object v0, p0, Lgtm;->b:Landroid/content/Context;

    invoke-direct {v4, v0, v3, v3}, Leqf;-><init>(Landroid/content/Context;Lbbr;Lbbs;)V

    new-instance v5, Ljeb;

    invoke-direct {v5}, Ljeb;-><init>()V

    new-instance v6, Ljec;

    invoke-direct {v6}, Ljec;-><init>()V

    iput v2, v6, Ljec;->a:I

    iget-object v0, p0, Lgtm;->b:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    iput-object v0, v6, Ljec;->e:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lgtm;->b:Landroid/content/Context;

    invoke-static {}, Lbox;->a()J

    move-result-wide v7

    iput-wide v7, v6, Ljec;->f:J

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, v6, Ljec;->g:Ljava/lang/String;

    :cond_2
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v0, v6, Ljec;->h:Ljava/lang/String;

    :cond_3
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, v6, Ljec;->i:Ljava/lang/String;

    :cond_4
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, v6, Ljec;->j:Ljava/lang/String;

    :cond_5
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v0, v6, Ljec;->k:Ljava/lang/String;

    :cond_6
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, v6, Ljec;->l:Ljava/lang/String;

    :cond_7
    iput-object v6, v5, Ljeb;->a:Ljec;

    new-instance v6, Ljed;

    invoke-direct {v6}, Ljed;-><init>()V

    iget-object v0, p0, Lgtm;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v7, p0, Lgtm;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v0, v7}, Lgtm;->a(Ljed;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    if-eqz p2, :cond_8

    invoke-static {v6, v0, p2}, Lgtm;->a(Ljed;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    :cond_8
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    iput-object v0, v6, Ljed;->b:[Ljava/lang/String;

    invoke-static {v4, v3}, Lgtm;->a(Leqf;Lgtn;)Ljee;

    move-result-object v0

    if-eqz v0, :cond_9

    iput-object v0, v6, Ljed;->e:Ljee;

    :cond_9
    iget-object v0, p0, Lgtm;->b:Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v4, Lgtm;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_a

    const-string v3, "level"

    invoke-virtual {v0, v3, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "scale"

    invoke-virtual {v0, v4, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_a

    mul-int/lit8 v3, v3, 0x64

    div-int v0, v3, v0

    iput v0, v6, Ljed;->c:I

    :cond_a
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    int-to-long v3, v0

    iput-wide v3, v6, Ljed;->d:J

    iget-object v0, p0, Lgtm;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v0, v4, :cond_e

    const-string v0, "adb_enabled"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_c

    move v0, v1

    :goto_1
    iput-boolean v0, v6, Ljed;->f:Z

    const-string v0, "install_non_market_apps"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_d

    :goto_2
    iput-boolean v1, v6, Ljed;->g:Z

    :goto_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Ljed;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Ljed;->j:Ljava/lang/String;

    invoke-direct {p0, v6}, Lgtm;->a(Ljed;)V

    iput-object v6, v5, Ljeb;->b:Ljed;

    new-instance v0, Ljea;

    invoke-direct {v0}, Ljea;-><init>()V

    iput-object v5, v0, Ljea;->a:Ljeb;

    new-instance v1, Ljeg;

    invoke-direct {v1}, Ljeg;-><init>()V

    iput-object v0, v1, Ljeg;->a:Ljea;

    invoke-static {v1}, Lizs;->a(Lizs;)[B

    move-result-object v0

    invoke-static {v0}, Lbpd;->b([B)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    const/16 v9, 0x8

    if-gt v8, v9, :cond_b

    iput-object v7, v6, Ljec;->d:Ljava/lang/String;

    goto/16 :goto_0

    :cond_b
    iput-object v7, v6, Ljec;->c:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1
    iput-object v7, v6, Ljec;->b:Ljava/lang/String;

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_1

    :cond_d
    move v1, v2

    goto :goto_2

    :cond_e
    const-string v0, "adb_enabled"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_f

    move v0, v1

    :goto_4
    iput-boolean v0, v6, Ljed;->f:Z

    const-string v0, "install_non_market_apps"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_10

    :goto_5
    iput-boolean v1, v6, Ljed;->g:Z

    goto :goto_3

    :cond_f
    move v0, v2

    goto :goto_4

    :cond_10
    move v1, v2

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
