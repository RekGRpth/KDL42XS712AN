.class public final Lbxr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lbxr;->a:Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lbxr;->a:Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->a(Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    iget-object v0, p0, Lbxr;->a:Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->b(Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;)Lcoy;

    move-result-object v0

    invoke-virtual {v0}, Lcoy;->i()Lcby;

    move-result-object v0

    iget-object v1, p0, Lbxr;->a:Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->a(Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-interface {v0, v1}, Lcby;->a(Z)V

    return-void
.end method
