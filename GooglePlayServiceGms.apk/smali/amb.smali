.class public final Lamb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lalr;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Lbjv;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.google"

    aput-object v2, v0, v1

    sput-object v0, Lamb;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lamb;->b:Lbjv;

    iput p2, p0, Lamb;->c:I

    iput-object p3, p0, Lamb;->d:Ljava/lang/String;

    iput-object p4, p0, Lamb;->e:Ljava/lang/String;

    iput-object p5, p0, Lamb;->f:[Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p4, :cond_0

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, p4, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "pendingIntent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lamb;->b:Lbjv;

    invoke-interface {v1, p2, p3, v0}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lakk;)V
    .locals 10

    const/4 v9, 0x4

    const/16 v8, 0xa

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-static {p1}, Lbov;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lboe;

    invoke-direct {v1, p1}, Lboe;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lboe;->a()Lboe;

    move-result-object v1

    iget-object v1, v1, Lboe;->a:Landroid/content/Intent;

    const/4 v2, 0x6

    invoke-direct {p0, p1, v2, v0, v1}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget v6, p0, Lamb;->c:I

    iget-object v1, p0, Lamb;->d:Ljava/lang/String;

    const-string v2, "com.google.android.gms.appstate.APP_ID"

    invoke-static {p1, v1, v2}, Lbox;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, ""

    move-object v2, v1

    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lamb;->c:I

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    if-eq v1, v5, :cond_2

    const-string v1, "ValidateServiceOp"

    const-string v2, "Missing metadata tag with the name \"com.google.android.gms.appstate.APP_ID\" in the application tag of your manifest."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, v8, v0, v0}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v1

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v2, "ValidateServiceOp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Application ID ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") must be a numeric value. Please verify that your manifest refers to the correct project ID."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v8, v0, v0}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lamb;->e:Ljava/lang/String;

    const-string v5, "<<default account>>"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v1, p0, Lamb;->d:Ljava/lang/String;

    invoke-static {p1, v1}, Lbov;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_3
    iget-object v5, p0, Lamb;->d:Ljava/lang/String;

    invoke-static {p1, v5}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    if-nez v1, :cond_6

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v5, v1

    :goto_2
    if-nez v5, :cond_7

    iget-object v2, p0, Lamb;->d:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p1, v2}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sget-object v2, Lamb;->a:[Ljava/lang/String;

    move-object v4, v0

    move-object v5, v0

    move-object v6, v0

    move-object v7, v0

    move v8, v3

    invoke-static/range {v0 .. v8}, Lbbm;->a(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Z)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-direct {p0, p1, v9, v0, v1}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    if-eqz v1, :cond_3

    iget-object v5, p0, Lamb;->d:Ljava/lang/String;

    invoke-static {p1, v1, v5}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    move-object v5, v0

    goto :goto_2

    :cond_5
    move-object v5, v0

    goto :goto_2

    :cond_6
    move-object v5, v1

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lamb;->d:Ljava/lang/String;

    invoke-static {p1, v6, v5, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    if-eqz v3, :cond_8

    move v1, v4

    :goto_3
    iget-object v7, p0, Lamb;->f:[Ljava/lang/String;

    array-length v7, v7

    if-ge v1, v7, :cond_8

    iget-object v7, p0, Lamb;->f:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-virtual {v3, v7}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_a

    move-object v3, v0

    :cond_8
    if-nez v3, :cond_e

    :try_start_1
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v7, p0, Lamb;->d:Ljava/lang/String;

    invoke-direct {v1, v6, v5, v5, v7}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v3, p0, Lamb;->f:[Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    new-instance v3, Lbmx;

    const/4 v5, 0x1

    invoke-direct {v3, v1, v5}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    invoke-interface {v3, p1}, Lbmz;->b(Landroid/content/Context;)Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/server/ClientContext;->a(Landroid/content/Context;)V
    :try_end_2
    .catch Lamq; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_4
    invoke-static {v1}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a(Lcom/google/android/gms/common/server/ClientContext;)Lalp;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lalp;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    :cond_9
    new-instance v3, Lalp;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5, v1, v2}, Lalp;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    invoke-static {v1, v3}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a(Lcom/google/android/gms/common/server/ClientContext;Lalp;)V

    move-object v2, v3

    :goto_5
    if-nez v2, :cond_c

    invoke-direct {p0, p1, v8, v0, v0}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :catch_1
    move-exception v1

    :goto_6
    invoke-virtual {v3, p1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    instance-of v2, v1, Lane;

    if-eqz v2, :cond_b

    check-cast v1, Lane;

    invoke-virtual {v1}, Lane;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, p1, v9, v0, v1}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_b
    const/16 v1, 0x8

    invoke-direct {p0, p1, v1, v0, v0}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    :catch_2
    move-exception v1

    const/4 v1, 0x7

    invoke-direct {p0, p1, v1, v0, v0}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    invoke-static {p1, v1}, Lbov;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-direct {p0, p1, v4, v2, v0}, Lamb;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    :catch_3
    move-exception v2

    move-object v3, v1

    move-object v1, v2

    goto :goto_6

    :cond_d
    move-object v2, v3

    goto :goto_5

    :cond_e
    move-object v1, v3

    goto :goto_4
.end method
