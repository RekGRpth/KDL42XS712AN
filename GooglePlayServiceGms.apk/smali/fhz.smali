.class public final Lfhz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Lfbe;

.field private final c:Ljava/lang/Object;

.field private final d:Lbpe;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfhz;->c:Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfhz;->a:Landroid/content/Context;

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfdl;->a(Landroid/content/Context;)Lbpe;

    move-result-object v0

    iput-object v0, p0, Lfhz;->d:Lbpe;

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v0

    iput-object v0, p0, Lfhz;->b:Lfbe;

    return-void
.end method

.method static a(Ljava/lang/Exception;)I
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p0, Lsp;

    if-eqz v1, :cond_0

    check-cast p0, Lsp;

    iget-object v1, p0, Lsp;->a:Lrz;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lsp;->a:Lrz;

    iget v0, v0, Lrz;->a:I

    :cond_0
    return v0
.end method

.method public static a(ZZ)Landroid/os/Bundle;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "ignore_settings"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    if-eqz p0, :cond_0

    const-string v1, "ignore_backoff"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    if-eqz p1, :cond_1

    const-string v1, "expedited"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_0

    const-string v0, "feed"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/os/Bundle;)I
    .locals 2

    invoke-static {p0}, Lfhz;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "plusfeed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v1, "pluspageadmin"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {p1, p2}, Lfhn;->a(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/os/Bundle;)I
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    invoke-static {p0}, Lfhz;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "periodic_sync"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)J
    .locals 4

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->e()Lfbk;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->c()Lfbn;

    move-result-object v2

    :try_start_0
    const-string v3, "SELECT last_successful_sync_time FROM owners WHERE _id=?"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lfbn;->b(Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)Ljava/util/List;
    .locals 7

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->c()Lfbn;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "SELECT page_gaia_id FROM owners WHERE account_name=? AND page_gaia_id IS NOT NULL  AND last_successful_sync_time<? ORDER BY last_successful_sync_time"

    iget-object v3, p0, Lfhz;->d:Lbpe;

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long/2addr v5, p2

    sub-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-object v1
.end method

.method public final a()V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfgy;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    const-string v5, "com.google.android.gms.people"

    invoke-static {v1, v1}, Lfhz;->a(ZZ)Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {p0, v4, v5, v6}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 7

    const/4 v6, 0x1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfbd;->k:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "com.google.android.gms.people"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iget-object v3, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v3}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {p1, v1, v2}, Lfhn;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "com.google.android.gms.people"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "periodic_sync"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    int-to-long v3, v0

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {p1, v1, v2, v3, v4}, Lfhn;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.google.android.gms.people"

    const-string v2, "plusupdates"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "plusfeed"

    aput-object v5, v3, v4

    const-string v4, "pluspageadmin"

    aput-object v4, v3, v6

    iget-object v4, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v4}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {v0, p1, v1, v2, v3}, Lfhn;->a(Landroid/content/ContentResolver;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    const-string v1, "PeopleSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting subscription: result="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    if-nez v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "Unable to subscribe to feed."

    invoke-virtual {p0, v0, v1}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLfeu;)V
    .locals 18

    const/4 v1, 0x1

    move/from16 v0, p4

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v1

    invoke-virtual {v1}, Lfbc;->a()Lfbe;

    move-result-object v1

    invoke-virtual {v1}, Lfbe;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhz;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhz;->a:Landroid/content/Context;

    const/4 v3, 0x1

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v2, Lax;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfhz;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lax;-><init>(Landroid/content/Context;)V

    const v3, 0x1080078    # android.R.drawable.stat_notify_error

    invoke-virtual {v2, v3}, Lax;->a(I)Lax;

    move-result-object v2

    const-string v3, "People Details sync failed"

    iput-object v3, v2, Lax;->b:Ljava/lang/CharSequence;

    const-string v3, "[%d] %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lax;->c:Ljava/lang/CharSequence;

    iput-object v1, v2, Lax;->d:Landroid/app/PendingIntent;

    invoke-virtual {v2}, Lax;->d()Landroid/app/Notification;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lfhz;->a:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v1

    invoke-virtual {v1}, Lfbc;->k()Lfjj;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhz;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static/range {p3 .. p3}, Lfhz;->c(Landroid/os/Bundle;)I

    move-result v5

    invoke-static/range {p3 .. p3}, Lfhz;->b(Landroid/os/Bundle;)I

    move-result v6

    invoke-static/range {p3 .. p3}, Lfie;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {p5 .. p5}, Lfhz;->a(Ljava/lang/Exception;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhz;->b:Lfbe;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lfbe;->b(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhz;->b:Lfbe;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lfbe;->a(Ljava/lang/String;)I

    move-result v4

    int-to-long v12, v4

    move/from16 v4, p2

    move/from16 v8, p4

    move-object/from16 v10, p5

    move-object/from16 v14, p6

    move-wide/from16 v15, p7

    move-object/from16 v17, p9

    invoke-virtual/range {v1 .. v17}, Lfjj;->a(Landroid/content/Context;Ljava/lang/String;IIILjava/lang/String;IILjava/lang/Throwable;IJLjava/util/List;JLfeu;)V

    return-void

    :pswitch_0
    const/4 v1, 0x0

    goto/16 :goto_0

    :pswitch_1
    sget-object v1, Lfbd;->ae:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0, p2}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method final a(Landroid/accounts/Account;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {p1, p2, p3}, Lfhn;->a(Landroid/accounts/Account;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {p1, p2, p3}, Lfhn;->b(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method final a(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {p1, p2, p3}, Lfhn;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    const-string v1, "PeopleSync"

    invoke-static {v0, v1, p1, p2}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 5

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->c()Lfbn;

    move-result-object v0

    const-string v1, "SELECT count(1) FROM owners WHERE account_name=? AND page_gaia_id IS NOT NULL"

    invoke-static {p1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v1

    sget-object v0, Lfbd;->t:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v3, v0

    cmp-long v0, v1, v3

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 7

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Lfhz;->d(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_1

    const-string v1, "PeopleSync"

    const-string v2, "requestSync: Owner doesn\'t exist"

    invoke-static {v1, v2}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v3, p0, Lfhz;->d:Lbpe;

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long/2addr v5, p3

    sub-long/2addr v3, v5

    cmp-long v3, v1, v3

    if-ltz v3, :cond_2

    :goto_1
    const-string v3, "PeopleService"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "PeopleSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "areDataFresh, last successful sync="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fresh="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " account="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " page="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " allowance="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZZZ)Z
    .locals 9

    const/4 v1, 0x0

    if-eqz p8, :cond_e

    invoke-virtual {p0, p1, p2}, Lfhz;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v1, "PeopleService"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleSync"

    const-string v2, "Not performing requestSync since background sync is enabled."

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x1

    move v3, v1

    :goto_0
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    cmp-long v2, p3, v4

    if-eqz v2, :cond_d

    invoke-virtual {p0, p1, p2, p3, p4}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    iget-object v4, p0, Lfhz;->a:Landroid/content/Context;

    const-string v5, "PeopleSync"

    const-string v6, "Sync requested: allowance=%d, needFreshData=%d, ignoreBackoff=%d, skipBecauseOfBackgroundSync=%d, isDisabledByBackgroundSync=%d"

    const/4 v1, 0x5

    new-array v7, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v1

    const/4 v8, 0x1

    if-eqz v2, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v8

    const/4 v8, 0x2

    if-eqz p6, :cond_4

    const/4 v1, 0x1

    :goto_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v8

    const/4 v8, 0x3

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v8

    const/4 v8, 0x4

    if-eqz p8, :cond_6

    const/4 v1, 0x1

    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, p1, p2, v1}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v1

    invoke-virtual {v1}, Lfbc;->k()Lfjj;

    move-result-object v4

    iget-object v5, p0, Lfhz;->a:Landroid/content/Context;

    sget-object v1, Lfdl;->f:Ljava/security/SecureRandom;

    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v6

    sget-object v1, Lfbd;->ac:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v6, v1

    if-gez v1, :cond_1

    new-instance v1, Lfes;

    invoke-direct {v1}, Lfes;-><init>()V

    invoke-static {v5, p1, p2}, Lfjj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lfew;

    move-result-object v5

    iput-object v5, v1, Lfes;->a:Lfew;

    invoke-static {p5}, Lfjj;->a(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lfes;->b:I

    iput-wide p3, v1, Lfes;->c:J

    iput-boolean p6, v1, Lfes;->d:Z

    move/from16 v0, p7

    iput-boolean v0, v1, Lfes;->e:Z

    move/from16 v0, p8

    iput-boolean v0, v1, Lfes;->f:Z

    iput-boolean v2, v1, Lfes;->g:Z

    iput-boolean v3, v1, Lfes;->h:Z

    new-instance v5, Lfeq;

    invoke-direct {v5}, Lfeq;-><init>()V

    invoke-static {}, Lfjj;->a()Lfex;

    move-result-object v6

    iput-object v6, v5, Lfeq;->a:Lfex;

    iput-object v1, v5, Lfeq;->b:Lfes;

    const-string v1, "sync_request"

    invoke-virtual {v4, v1, v5}, Lfjj;->a(Ljava/lang/String;Lfeq;)V

    :cond_1
    if-eqz v2, :cond_2

    if-eqz v3, :cond_7

    :cond_2
    const/4 v1, 0x0

    :goto_6
    return v1

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    :cond_7
    iget-object v1, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v1

    invoke-virtual {v1}, Lfbo;->d()Lfbn;

    move-result-object v2

    invoke-virtual {v2}, Lfbn;->b()V

    :try_start_0
    iget-object v1, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v1

    invoke-virtual {v1}, Lfbo;->e()Lfbk;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "SELECT COUNT(*)  FROM owner_sync_requests JOIN owners ON owner_sync_requests.account_name=owners.account_name AND ((owner_sync_requests.page_gaia_id IS NULL AND owners.page_gaia_id IS NULL ) OR owner_sync_requests.page_gaia_id=owners.page_gaia_id) WHERE owners._id=? AND owner_sync_requests.sync_requested_time>owners.last_sync_start_time"

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x0

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_a

    invoke-virtual {v2}, Lfbn;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v2}, Lfbn;->e()V

    :goto_7
    invoke-static/range {p6 .. p7}, Lfhz;->a(ZZ)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "gms.people.request_app_id"

    invoke-virtual {v2, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const-string v3, "page_only"

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    const/4 v1, 0x1

    :goto_8
    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v1

    invoke-virtual {v1}, Lfbo;->e()Lfbk;

    move-result-object v1

    invoke-virtual {v1, p1}, Lfbk;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    const-string v3, "com.google.android.gms.people"

    invoke-virtual {p0, v1, v3, v2}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "PeopleService"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "PeopleSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sync requested for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " page="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ignoreBackoff="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const/4 v1, 0x1

    goto/16 :goto_6

    :cond_a
    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_b
    iget-object v1, p0, Lfhz;->d:Lbpe;

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v3

    const-string v1, "REPLACE INTO owner_sync_requests (account_name ,page_gaia_id ,sync_requested_time) VALUES (?,?,?)"

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, p2, v3}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lfbn;->d()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v2}, Lfbn;->e()V

    goto :goto_7

    :catchall_0
    move-exception v1

    :try_start_4
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v1

    invoke-virtual {v2}, Lfbn;->e()V

    throw v1

    :cond_c
    const/4 v1, 0x0

    goto :goto_8

    :cond_d
    move v2, v1

    goto/16 :goto_1

    :cond_e
    move v3, v1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    const/4 v6, 0x0

    const-wide/16 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move v7, v6

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZZZ)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 9

    const/4 v6, 0x1

    const-wide/16 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move v7, v6

    move v8, p4

    invoke-virtual/range {v0 .. v8}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZZZ)Z

    move-result v0

    return v0
.end method

.method public final a(Z)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lfhz;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lfhz;->b:Lfbe;

    iget-object v3, v3, Lfbe;->a:Landroid/content/SharedPreferences;

    const-string v4, "is_first_sync"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    monitor-exit v2

    :goto_0
    return v0

    :cond_0
    const-string v3, "First sync"

    const/4 v4, 0x0

    invoke-virtual {p0, v4, v3}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v3}, Lfgy;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    const-string v6, "com.google.android.gms.people"

    const/4 v7, 0x1

    invoke-virtual {p0, v5, v6, v7}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v6, "com.android.contacts"

    invoke-direct {p0, v5, v6}, Lfhz;->b(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v6

    invoke-static {}, Lfdk;->a()Z

    const-string v7, "com.google.android.gms.people"

    invoke-virtual {p0, v5, v7, v6}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    if-eqz v6, :cond_1

    invoke-virtual {p0, v5}, Lfhz;->a(Landroid/accounts/Account;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lfhz;->b:Lfbe;

    const/4 v3, 0x0

    invoke-static {v3}, Lbkm;->c(Ljava/lang/String;)V

    iget-object v0, v0, Lfbe;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "is_first_sync"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lfhz;->a()V

    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->c()Lfbn;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "SELECT owner_sync_requests.page_gaia_id,last_sync_start_time FROM owner_sync_requests JOIN owners ON owner_sync_requests.account_name=owners.account_name AND ((owner_sync_requests.page_gaia_id IS NULL AND owners.page_gaia_id IS NULL ) OR owner_sync_requests.page_gaia_id=owners.page_gaia_id) WHERE owner_sync_requests.account_name=? AND owner_sync_requests.page_gaia_id IS NOT NULL AND owner_sync_requests.sync_requested_time>last_sync_start_time ORDER BY owner_sync_requests.sync_requested_time"

    invoke-static {p1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-object v1
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {}, Lfhn;->b()Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lfhz;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lfhz;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->e()Lfbk;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->c()Lfbn;

    move-result-object v2

    const-string v3, "SELECT last_sync_status FROM owners WHERE _id=?"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v0, v4, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "PeopleSync"

    const-string v1, "Contacts sync requested"

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v3, v3}, Lfhz;->a(ZZ)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "gms.people.contacts_sync"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v1

    invoke-virtual {v1}, Lfbo;->e()Lfbk;

    move-result-object v1

    invoke-virtual {v1, p1}, Lfbk;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "com.google.android.gms.people"

    invoke-virtual {p0, v1, v2, v0}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfhz;->b:Lfbe;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lfbe;->b(Ljava/lang/String;I)V

    return-void
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->e()Lfbk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfbk;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "com.google.android.gms.people"

    invoke-direct {p0, v0, v1}, Lfhz;->b(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
