.class public final Lfmz;
.super Lfmx;
.source "SourceFile"

# interfaces
.implements Lfnn;


# instance fields
.field final synthetic b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfmx;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lfmz;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public final a(Lbbo;)V
    .locals 3

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->k(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lbbo;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {p1}, Lbbo;->d()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const v2, 0x7f0b0352    # com.google.android.gms.R.string.plus_invalid_account

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const v2, 0x7f0b0495    # com.google.android.gms.R.string.auth_network_unreliable

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const v2, 0x7f0b0353    # com.google.android.gms.R.string.plus_internal_error

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Landroid/app/PendingIntent;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 6

    const/4 v1, -0x1

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v2, 0x80

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->j(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    :goto_0
    if-eq v3, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lfni;

    move-result-object v0

    iget-object v1, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->j(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lfmz;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v4

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lfni;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lfnn;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v3, v1

    goto :goto_0
.end method
