.class public final Lfie;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lglz;

.field private static final b:Lgma;

.field private static final c:Lgma;


# instance fields
.field private final A:Lfhx;

.field private final B:Lfii;

.field private final C:Lfin;

.field private final D:Lfio;

.field private final d:Landroid/content/Context;

.field private final e:Lbpe;

.field private final f:Landroid/content/SyncResult;

.field private final g:Lfia;

.field private final h:Lfid;

.field private final i:I

.field private final j:Z

.field private final k:Z

.field private final l:Landroid/os/Bundle;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private final o:Lffe;

.field private final p:Lffa;

.field private final q:Lffg;

.field private final r:Lffg;

.field private final s:Lfip;

.field private final t:Lffh;

.field private final u:Lgly;

.field private final v:Lglv;

.field private final w:Lgex;

.field private final x:Lgel;

.field private final y:Lgej;

.field private final z:Lgeg;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lglz;

    invoke-direct {v0}, Lglz;-><init>()V

    const-string v1, "etag"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "names"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "emails"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "phoneNumbers"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "addresses"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "metadata/ownerId"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "metadata/ownerUserTypes"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    const-string v1, "coverPhotos"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    sput-object v0, Lfie;->a:Lglz;

    new-instance v0, Lgma;

    invoke-direct {v0}, Lgma;-><init>()V

    const-string v1, "items(etag,id,names,images,urls,sortKeys,taglines,emails,phoneNumbers,addresses,metadata)"

    invoke-virtual {v0, v1}, Lgma;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lgma;

    const-string v1, "nextPageToken"

    invoke-virtual {v0, v1}, Lgma;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lgma;

    const-string v1, "nextSyncToken"

    invoke-virtual {v0, v1}, Lgma;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lgma;

    sput-object v0, Lfie;->b:Lgma;

    new-instance v0, Lgma;

    invoke-direct {v0}, Lgma;-><init>()V

    const-string v1, "items(id,metadata)"

    invoke-virtual {v0, v1}, Lgma;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lgma;

    const-string v1, "nextPageToken"

    invoke-virtual {v0, v1}, Lgma;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lgma;

    sput-object v0, Lfie;->c:Lgma;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lfia;Lfhx;Lfid;Landroid/os/Bundle;ZZ)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfii;

    invoke-direct {v0, p0, v1}, Lfii;-><init>(Lfie;B)V

    iput-object v0, p0, Lfie;->B:Lfii;

    new-instance v0, Lfin;

    invoke-direct {v0, p0, v1}, Lfin;-><init>(Lfie;B)V

    iput-object v0, p0, Lfie;->C:Lfin;

    new-instance v0, Lfio;

    invoke-direct {v0, p0, v1}, Lfio;-><init>(Lfie;B)V

    iput-object v0, p0, Lfie;->D:Lfio;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfie;->d:Landroid/content/Context;

    iget-object v0, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v0}, Lfdl;->a(Landroid/content/Context;)Lbpe;

    move-result-object v0

    iput-object v0, p0, Lfie;->e:Lbpe;

    iput-object p5, p0, Lfie;->f:Landroid/content/SyncResult;

    iput-object p6, p0, Lfie;->g:Lfia;

    iput-object p8, p0, Lfie;->h:Lfid;

    iget-object v0, p0, Lfie;->h:Lfid;

    iput-object p2, v0, Lfid;->a:Ljava/lang/String;

    iget-object v0, p0, Lfie;->h:Lfid;

    iput-object p3, v0, Lfid;->b:Ljava/lang/String;

    iput p4, p0, Lfie;->i:I

    iput-boolean p10, p0, Lfie;->j:Z

    iput-boolean p11, p0, Lfie;->k:Z

    iput-object p9, p0, Lfie;->l:Landroid/os/Bundle;

    iput-object p2, p0, Lfie;->m:Ljava/lang/String;

    iput-object p3, p0, Lfie;->n:Ljava/lang/String;

    iput-object p7, p0, Lfie;->A:Lfhx;

    iget-object v0, p0, Lfie;->d:Landroid/content/Context;

    iget-object v1, p0, Lfie;->l:Landroid/os/Bundle;

    invoke-static {v1}, Lfie;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p2, p3, v1}, Lffe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lffa;

    move-result-object v0

    iput-object v0, p0, Lfie;->p:Lffa;

    iget-object v0, p0, Lfie;->p:Lffa;

    iget-object v1, v0, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    packed-switch p4, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lffg;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    new-instance v0, Lfip;

    iget-object v1, p0, Lfie;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p3}, Lfip;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lfie;->s:Lfip;

    iget-object v0, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v0}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v0

    iput-object v0, p0, Lfie;->t:Lffh;

    iget-object v0, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v0}, Lffe;->a(Landroid/content/Context;)Lffe;

    move-result-object v0

    iput-object v0, p0, Lfie;->o:Lffe;

    iget-object v0, p0, Lfie;->o:Lffe;

    invoke-virtual {v0}, Lffe;->a()Lffg;

    move-result-object v0

    iput-object v0, p0, Lfie;->q:Lffg;

    iget-object v0, p0, Lfie;->o:Lffe;

    invoke-virtual {v0}, Lffe;->b()Lffg;

    move-result-object v0

    iput-object v0, p0, Lfie;->r:Lffg;

    new-instance v0, Lgly;

    iget-object v1, p0, Lfie;->r:Lffg;

    invoke-direct {v0, v1}, Lgly;-><init>(Lbmi;)V

    iput-object v0, p0, Lfie;->u:Lgly;

    new-instance v0, Lgex;

    iget-object v1, p0, Lfie;->q:Lffg;

    invoke-direct {v0, v1}, Lgex;-><init>(Lbmi;)V

    iput-object v0, p0, Lfie;->w:Lgex;

    new-instance v0, Lgel;

    iget-object v1, p0, Lfie;->q:Lffg;

    invoke-direct {v0, v1}, Lgel;-><init>(Lbmi;)V

    iput-object v0, p0, Lfie;->x:Lgel;

    new-instance v0, Lglv;

    iget-object v1, p0, Lfie;->r:Lffg;

    invoke-direct {v0, v1}, Lglv;-><init>(Lbmi;)V

    iput-object v0, p0, Lfie;->v:Lglv;

    new-instance v0, Lgej;

    iget-object v1, p0, Lfie;->q:Lffg;

    invoke-direct {v0, v1}, Lgej;-><init>(Lbmi;)V

    iput-object v0, p0, Lfie;->y:Lgej;

    new-instance v0, Lgeg;

    iget-object v1, p0, Lfie;->q:Lffg;

    invoke-direct {v0, v1}, Lgeg;-><init>(Lbmi;)V

    iput-object v0, p0, Lfie;->z:Lgeg;

    return-void

    :pswitch_0
    const-string v0, "m"

    goto :goto_0

    :pswitch_1
    const-string v0, "p"

    goto :goto_0

    :pswitch_2
    const-string v0, "t"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lfie;)Lfip;
    .locals 1

    iget-object v0, p0, Lfie;->s:Lfip;

    return-object v0
.end method

.method static final a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lbch;->b:Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    const-string v0, "gms.people.request_app_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lfia;ZLfhx;Lfid;Landroid/os/Bundle;Z)V
    .locals 15

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v13

    new-instance v1, Lfie;

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p6

    invoke-direct/range {v1 .. v12}, Lfie;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lfia;Lfhx;Lfid;Landroid/os/Bundle;ZZ)V

    iget-boolean v2, v1, Lfie;->j:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, Lfie;->n:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const-string v3, "isPageExistenceCheckOnly=true is not used with pages"

    invoke-static {v2, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    :cond_0
    const-string v2, "PeopleSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sync start: cannotHavePeople="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, v1, Lfie;->k:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isPageExistenceCheckOnly="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, v1, Lfie;->j:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "PeopleSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sync for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lfie;->m:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " page="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lfie;->n:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v2, v1, Lfie;->e:Lbpe;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v4

    const/4 v8, 0x0

    :try_start_0
    iget-boolean v2, v1, Lfie;->j:Z

    if-nez v2, :cond_2

    iget-object v2, v1, Lfie;->s:Lfip;

    const/4 v3, 0x0

    const/4 v6, 0x0

    iget-boolean v7, v1, Lfie;->k:Z

    invoke-virtual/range {v2 .. v7}, Lfip;->a(ZJLjava/lang/Long;Z)V

    iget-object v2, v1, Lfie;->t:Lffh;

    iget-object v3, v1, Lfie;->m:Ljava/lang/String;

    iget-object v6, v1, Lfie;->n:Ljava/lang/String;

    const/16 v7, 0x10

    invoke-virtual {v2, v3, v6, v7}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v2, v1, Lfie;->t:Lffh;

    invoke-virtual {v2}, Lffh;->b()V

    :cond_2
    iget-object v2, v1, Lfie;->g:Lfia;

    invoke-virtual {v2}, Lfia;->c()V

    iget-object v2, v1, Lfie;->g:Lfia;

    invoke-virtual {v2}, Lfia;->c()V

    const/4 v2, 0x1

    iget-object v3, v1, Lfie;->n:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-boolean v3, v1, Lfie;->j:Z

    if-eqz v3, :cond_3

    iget-object v3, v1, Lfie;->d:Landroid/content/Context;

    invoke-static {v3}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v3

    invoke-virtual {v3}, Lfbo;->e()Lfbk;

    move-result-object v3

    iget-object v6, v1, Lfie;->m:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lfbk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    invoke-direct {v1}, Lfie;->e()Z

    move-result v2

    :cond_4
    invoke-direct {v1}, Lfie;->a()Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "PeopleSync"

    const-string v6, "Sync inconsistency.  Resyncing pages."

    invoke-static {v3, v6}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v1, Lfie;->s:Lfip;

    const-string v6, "pages"

    invoke-virtual {v3, v6}, Lfip;->b(Ljava/lang/String;)V

    invoke-direct {v1}, Lfie;->a()Z

    :cond_5
    iget-boolean v3, v1, Lfie;->j:Z

    if-nez v3, :cond_a

    iget-boolean v3, v1, Lfie;->k:Z

    if-nez v3, :cond_e

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_7

    invoke-direct {v1}, Lfie;->b()Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "PeopleSync"

    const-string v6, "Sync inconsistency.  Resyncing groups."

    invoke-static {v3, v6}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v1, Lfie;->s:Lfip;

    const-string v6, "groups"

    invoke-virtual {v3, v6}, Lfip;->b(Ljava/lang/String;)V

    invoke-direct {v1}, Lfie;->b()Z

    :cond_6
    invoke-direct {v1}, Lfie;->c()Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "PeopleSync"

    const-string v6, "Sync inconsistency.  Resyncing circles."

    invoke-static {v3, v6}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v1, Lfie;->s:Lfip;

    const-string v6, "circles"

    invoke-virtual {v3, v6}, Lfip;->b(Ljava/lang/String;)V

    invoke-direct {v1}, Lfie;->c()Z

    :cond_7
    invoke-direct {v1, v2}, Lfie;->a(Z)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v1, Lfie;->e:Lbpe;

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v6

    iget-object v3, v1, Lfie;->s:Lfip;

    invoke-virtual {v3, v6, v7}, Lfip;->a(J)V

    :cond_8
    if-eqz v2, :cond_9

    iget-object v2, v1, Lfie;->n:Ljava/lang/String;

    if-nez v2, :cond_9

    invoke-direct {v1}, Lfie;->d()V

    :cond_9
    iget-object v2, v1, Lfie;->d:Landroid/content/Context;

    invoke-static {v2}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v2

    invoke-virtual {v2}, Lfbc;->a()Lfbe;

    move-result-object v3

    iget-object v2, v1, Lfie;->m:Ljava/lang/String;

    iget-object v6, v1, Lfie;->n:Ljava/lang/String;

    iget-object v7, v3, Lfbe;->a:Landroid/content/SharedPreferences;

    invoke-static {v2, v6}, Lfbe;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v9, 0x0

    invoke-interface {v7, v2, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    sget-object v2, Lfbd;->R:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    add-long/2addr v6, v9

    iget-object v2, v1, Lfie;->e:Lbpe;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v9

    cmp-long v2, v9, v6

    if-gez v2, :cond_f

    const-string v2, "PeopleSync"

    const-string v3, "Sync sanity check not necessary."

    invoke-static {v2, v3}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_1
    const-string v2, "PeopleService"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, v1, Lfie;->s:Lfip;

    invoke-virtual {v2}, Lfip;->a()V

    :cond_b
    iget-object v2, v1, Lfie;->A:Lfhx;

    if-eqz v2, :cond_c

    iget-object v2, v1, Lfie;->A:Lfhx;
    :try_end_0
    .catch Lfic; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_c
    iget-object v2, v1, Lfie;->e:Lbpe;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v8

    iget-boolean v2, v1, Lfie;->j:Z

    if-nez v2, :cond_d

    iget-object v2, v1, Lfie;->s:Lfip;

    const/4 v3, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-boolean v7, v1, Lfie;->k:Z

    invoke-virtual/range {v2 .. v7}, Lfip;->a(ZJLjava/lang/Long;Z)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lfie;->a(ZZ)V

    :cond_d
    const-string v1, "PeopleSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sync finished, successful=true"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ", took "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v8, v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    sub-long/2addr v1, v13

    move-object/from16 v0, p8

    iput-wide v1, v0, Lfid;->D:J

    return-void

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_f
    :try_start_1
    iget-object v2, v1, Lfie;->m:Ljava/lang/String;

    iget-object v6, v1, Lfie;->n:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v7}, Lbkm;->c(Ljava/lang/String;)V

    iget-object v3, v3, Lfbe;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-static {v2, v6}, Lfbe;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2, v9, v10}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v2, Lfdl;->f:Ljava/security/SecureRandom;

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v3

    sget-object v2, Lfbd;->S:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v2, v3, v2

    if-ltz v2, :cond_11

    const-string v2, "PeopleSync"

    const-string v3, "Skipping sanity check."

    invoke-static {v2, v3}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lfic; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_1

    :catch_0
    move-exception v2

    const/4 v3, 0x1

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    move-object v8, v2

    move v9, v3

    :goto_2
    iget-object v2, v1, Lfie;->e:Lbpe;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v10

    iget-boolean v2, v1, Lfie;->j:Z

    if-nez v2, :cond_10

    iget-object v2, v1, Lfie;->s:Lfip;

    const/4 v3, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-boolean v7, v1, Lfie;->k:Z

    invoke-virtual/range {v2 .. v7}, Lfip;->a(ZJLjava/lang/Long;Z)V

    const/4 v2, 0x0

    invoke-direct {v1, v2, v9}, Lfie;->a(ZZ)V

    :cond_10
    const-string v1, "PeopleSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sync finished, successful=false"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ", took "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v10, v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    :cond_11
    :try_start_3
    invoke-direct {v1}, Lfie;->h()V
    :try_end_3
    .catch Lfic; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_1

    :catchall_1
    move-exception v2

    move v9, v8

    move-object v8, v2

    goto :goto_2
.end method

.method private a(Lfim;Lfil;Ljava/lang/String;)V
    .locals 10

    const/4 v7, 0x0

    :cond_0
    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    iget-object v0, p0, Lfie;->u:Lgly;

    iget-object v1, p0, Lfie;->p:Lffa;

    iget-object v1, v1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "me"

    const-string v3, "all"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v5}, Lfbd;->c(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lfie;->n:Ljava/lang/String;

    sget-object v9, Lfie;->b:Lgma;

    move-object v8, p3

    invoke-virtual/range {v0 .. v9}, Lgly;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgma;)Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;

    move-result-object v3

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    iget-boolean v0, p1, Lfim;->b:Z

    if-eqz v0, :cond_e

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->e()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lfhr;->b(Ljava/util/List;)I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_c

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->i()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lfhr;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lfhr;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_a

    invoke-static {v0}, Lfip;->d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v1

    if-nez v1, :cond_7

    sget-object v1, Lfbd;->v:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->j()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lfhr;->a(Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->j()Ljava/util/List;

    move-result-object v1

    const-string v6, "myContacts"

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_4
    if-nez v1, :cond_7

    invoke-static {v0}, Lfhr;->f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p1, Lfim;->c:Lfie;

    iget-object v1, v1, Lfie;->s:Lfip;

    invoke-virtual {v1, v0}, Lfip;->i(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lfim;->c:Lfie;

    iget-object v1, v1, Lfie;->s:Lfip;

    invoke-virtual {v1, v0}, Lfip;->l(Ljava/lang/String;)V

    :goto_5
    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v0, Landroid/content/SyncStats;->numDeletes:J

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->h:Lfid;

    iget v1, v0, Lfid;->q:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfid;->q:I

    :cond_2
    :goto_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    goto :goto_4

    :cond_7
    invoke-static {v0}, Lfhr;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    iget-object v6, p1, Lfim;->a:Ljava/util/Set;

    if-eqz v6, :cond_8

    iget-object v6, p1, Lfim;->a:Ljava/util/Set;

    invoke-interface {v6, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_8
    iget-object v6, p1, Lfim;->c:Lfie;

    iget-object v6, v6, Lfie;->s:Lfip;

    invoke-virtual {v6, v1}, Lfip;->i(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p1, Lfim;->c:Lfie;

    iget-object v1, v1, Lfie;->s:Lfip;

    invoke-virtual {v1, v0}, Lfip;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v0, Landroid/content/SyncStats;->numUpdates:J

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->h:Lfid;

    iget v1, v0, Lfid;->p:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfid;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_6

    :catchall_0
    move-exception v0

    iget-object v1, p1, Lfim;->c:Lfie;

    iget-object v1, v1, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_9
    :try_start_1
    iget-object v1, p1, Lfim;->c:Lfie;

    iget-object v1, v1, Lfie;->s:Lfip;

    invoke-virtual {v1, v0}, Lfip;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v0, Landroid/content/SyncStats;->numInserts:J

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->h:Lfid;

    iget v1, v0, Lfid;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfid;->o:I

    goto :goto_6

    :cond_a
    invoke-static {v0}, Lfhr;->f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->s:Lfip;

    invoke-virtual {v0, v1}, Lfip;->l(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_b
    iget-object v1, p1, Lfim;->c:Lfie;

    iget-object v1, v1, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfip;->m(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_c
    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->p()V

    if-lez v5, :cond_d

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->t:Lffh;

    iget-object v1, p1, Lfim;->c:Lfie;

    iget-object v1, v1, Lfie;->m:Ljava/lang/String;

    iget-object v2, p1, Lfim;->c:Lfie;

    iget-object v2, v2, Lfie;->n:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-virtual {v0, v1, v2, v4}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_d
    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    :cond_e
    invoke-virtual {p2, v3}, Lfil;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;)V

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->f()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    iget-boolean v0, p1, Lfim;->b:Z

    if-eqz v0, :cond_11

    iget-object v0, p1, Lfim;->a:Ljava/util/Set;

    if-eqz v0, :cond_11

    iget-object v3, p1, Lfim;->c:Lfie;

    iget-object v1, p1, Lfim;->a:Ljava/util/Set;

    const-string v0, "PeopleSync"

    const-string v4, "Orphaned People"

    invoke-static {v0, v4}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v3, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, v3, Lfie;->s:Lfip;

    invoke-virtual {v5, v0}, Lfip;->l(Ljava/lang/String;)V

    iget-object v0, v3, Lfie;->t:Lffh;

    iget-object v5, v3, Lfie;->m:Ljava/lang/String;

    iget-object v6, v3, Lfie;->n:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-virtual {v0, v5, v6, v7}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, v3, Lfie;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v0, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    iput-wide v5, v0, Landroid/content/SyncStats;->numDeletes:J

    iget-object v0, v3, Lfie;->h:Lfid;

    iget v5, v0, Lfid;->q:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Lfid;->q:I

    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x32

    if-lt v0, v1, :cond_f

    iget-object v0, v3, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->p()V

    iget-object v0, v3, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->r()V

    const/4 v0, 0x0

    iget-object v1, v3, Lfie;->g:Lfia;

    invoke-virtual {v1}, Lfia;->c()V

    :cond_f
    move v1, v0

    goto :goto_7

    :cond_10
    iget-object v0, v3, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->p()V

    iget-object v0, v3, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v0, v3, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, v3, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    :cond_11
    invoke-virtual {p2}, Lfil;->d()V

    iget-boolean v0, p1, Lfim;->b:Z

    if-eqz v0, :cond_12

    iget-object v0, p1, Lfim;->c:Lfie;

    iget-object v0, v0, Lfie;->s:Lfip;

    const-string v1, "people"

    invoke-virtual {v0, v1, v2}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    iget-object v0, p2, Lfil;->d:Lfie;

    invoke-direct {v0}, Lfie;->g()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p2, Lfil;->d:Lfie;

    iget-object v0, v0, Lfie;->s:Lfip;

    const-string v1, "gaiamap"

    invoke-virtual {v0, v1, v2}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    return-void

    :catchall_1
    move-exception v0

    iget-object v1, v3, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Lfim;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, p0, v0, p2}, Lfim;-><init>(Lfie;ZZ)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PeopleSync"

    const-string v2, "Gaia-map/people sync (full)"

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->h:Lfid;

    invoke-direct {p0}, Lfie;->g()Z

    move-result v2

    iput-boolean v2, v0, Lfid;->w:Z

    iget-object v0, p0, Lfie;->h:Lfid;

    iput-boolean p2, v0, Lfid;->r:Z

    iget-object v0, p0, Lfie;->s:Lfip;

    const-string v2, "people"

    invoke-virtual {v0, v2, v3}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->s:Lfip;

    const-string v2, "gaiamap"

    invoke-virtual {v0, v2, v3}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lfik;

    invoke-direct {v0, p0}, Lfik;-><init>(Lfie;)V

    invoke-direct {p0, v1, v0, p1}, Lfie;->a(Lfim;Lfil;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "PeopleSync"

    const-string v2, "Gaia-map/people sync (delta)"

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lfij;

    invoke-direct {v0, p0}, Lfij;-><init>(Lfie;)V

    invoke-direct {p0, v1, v0, p1}, Lfie;->a(Lfim;Lfil;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Ljava/util/Set;)V
    .locals 6

    const/4 v0, 0x1

    iget-object v1, p0, Lfie;->n:Ljava/lang/String;

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    const-string v0, "PeopleSync"

    const-string v1, "Orphaned Pages"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lfie;->s:Lfip;

    invoke-virtual {v2, v0}, Lfip;->d(Ljava/lang/String;)V

    iget-object v2, p0, Lfie;->t:Lffh;

    iget-object v3, p0, Lfie;->m:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numDeletes:J

    iget-object v0, p0, Lfie;->h:Lfid;

    iget v2, v0, Lfid;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lfid;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    return-void
.end method

.method private a(Ljava/util/Set;Z)V
    .locals 7

    const-string v0, "PeopleSync"

    const-string v1, "Orphaned Groups and Circles"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lfie;->s:Lfip;

    invoke-virtual {v2, v0}, Lfip;->n(Ljava/lang/String;)V

    const/4 v0, 0x1

    iget-object v2, p0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numDeletes:J

    if-eqz p2, :cond_0

    iget-object v2, p0, Lfie;->h:Lfid;

    iget v3, v2, Lfid;->m:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lfid;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_0
    :try_start_1
    iget-object v2, p0, Lfie;->h:Lfid;

    iget v3, v2, Lfid;->i:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lfid;->i:I

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lfie;->t:Lffh;

    iget-object v1, p0, Lfie;->m:Ljava/lang/String;

    iget-object v2, p0, Lfie;->n:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    return-void
.end method

.method private a(ZZ)V
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lfie;->t:Lffh;

    iget-object v1, p0, Lfie;->m:Ljava/lang/String;

    iget-object v2, p0, Lfie;->n:Ljava/lang/String;

    const/16 v3, 0x20

    invoke-virtual {v0, v1, v2, v3}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    iget-object v0, p0, Lfie;->t:Lffh;

    invoke-virtual {v0}, Lffh;->b()V

    return-void

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lfie;->t:Lffh;

    iget-object v1, p0, Lfie;->m:Ljava/lang/String;

    iget-object v2, p0, Lfie;->n:Ljava/lang/String;

    const/16 v3, 0x80

    invoke-virtual {v0, v1, v2, v3}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfie;->t:Lffh;

    iget-object v1, p0, Lfie;->m:Ljava/lang/String;

    iget-object v2, p0, Lfie;->n:Ljava/lang/String;

    const/16 v3, 0x40

    invoke-virtual {v0, v1, v2, v3}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private a()Z
    .locals 12

    iget-object v0, p0, Lfie;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    const/4 v1, 0x0

    const/4 v7, 0x0

    const-string v0, "PeopleSync"

    const-string v2, "Pages"

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lfie;->h:Lfid;

    iget-object v0, p0, Lfie;->s:Lfip;

    const-string v3, "pages"

    invoke-virtual {v0, v3}, Lfip;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v2, Lfid;->f:Z

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->e()Ljava/util/Set;

    move-result-object v9

    move v8, v1

    :goto_2
    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    iget-object v0, p0, Lfie;->w:Lgex;

    iget-object v1, p0, Lfie;->p:Lffa;

    iget-object v1, v1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "me"

    const-string v3, "pages"

    const/4 v4, 0x0

    iget-object v5, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v5}, Lfbd;->a(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v7}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v2

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->e()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lfhr;->b(Ljava/util/List;)I

    move-result v4

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    if-lez v4, :cond_5

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pages."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lfie;->s:Lfip;

    invoke-virtual {v5, v0}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "PeopleSync"

    const-string v1, "    Up to date"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkp;

    invoke-interface {v0}, Lgkp;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v5, p0, Lfie;->s:Lfip;

    invoke-virtual {v5, v0}, Lfip;->e(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    const/4 v0, 0x0

    :goto_4
    return v0

    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    :try_start_1
    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v0, v5}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_5
    if-ge v1, v4, :cond_5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkp;

    invoke-interface {v0}, Lgkp;->g()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v9, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v6, p0, Lfie;->s:Lfip;

    invoke-virtual {v6, v5}, Lfip;->e(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lfie;->s:Lfip;

    invoke-virtual {v6, v0}, Lfip;->b(Lgkp;)V

    iget-object v0, p0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    iput-wide v6, v0, Landroid/content/SyncStats;->numUpdates:J

    iget-object v0, p0, Lfie;->h:Lfid;

    iget v6, v0, Lfid;->d:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v0, Lfid;->d:I

    :goto_6
    iget-object v0, p0, Lfie;->t:Lffh;

    iget-object v6, p0, Lfie;->m:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v5, v7}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_4
    iget-object v6, p0, Lfie;->s:Lfip;

    invoke-virtual {v6, v0}, Lfip;->a(Lgkp;)V

    iget-object v0, p0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    iput-wide v6, v0, Landroid/content/SyncStats;->numInserts:J

    iget-object v0, p0, Lfie;->h:Lfid;

    iget v6, v0, Lfid;->c:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v0, Lfid;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_5
    :try_start_2
    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v4, :cond_8

    iget-object v0, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->m()Lfhp;

    move-result-object v5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkp;

    iget-object v6, p0, Lfie;->m:Ljava/lang/String;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v6}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Lgkp;->g()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Lgkp;->h()Lgks;

    move-result-object v10

    const/4 v0, 0x0

    if-eqz v10, :cond_6

    invoke-interface {v10}, Lgks;->c()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v10}, Lgks;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v7, v0}, Lfhp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :cond_6
    if-nez v0, :cond_7

    iget-object v0, v5, Lfhp;->b:Lfjl;

    invoke-virtual {v0, v6, v7}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_8
    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->g()Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v0, v8, 0x1

    if-nez v7, :cond_9

    invoke-direct {p0, v9}, Lfie;->a(Ljava/util/Set;)V

    invoke-direct {p0}, Lfie;->f()V

    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_9
    move v8, v0

    goto/16 :goto_2
.end method

.method private a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Z
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->b(Ljava/util/List;)I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;

    const-string v4, "android"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lfie;->d:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lbox;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a(Z)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    if-nez p1, :cond_1

    invoke-direct {p0}, Lfie;->g()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "PeopleSync"

    const-string v2, "Both gaia-map sync and people sync are disabled."

    invoke-static {v1, v2}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget-object v2, p0, Lfie;->s:Lfip;

    invoke-virtual {v2}, Lfip;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v2, v3

    :goto_1
    invoke-direct {p0, v2, p1}, Lfie;->a(Ljava/lang/String;Z)V

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lfie;->g()Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz p1, :cond_4

    iget-object v2, p0, Lfie;->s:Lfip;

    const-string v4, "people"

    invoke-virtual {v2, v4}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lfie;->s:Lfip;

    const-string v5, "gaiamap"

    invoke-virtual {v4, v5}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lirf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lfie;->s:Lfip;

    const-string v4, "gaiamap"

    invoke-virtual {v2, v4}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    const-string v2, "PeopleSync"

    const-string v4, "Gaia-map and people sync tokens unexpectedly different."

    invoke-static {v2, v4}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lfie;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lfie;->s:Lfip;

    const-string v4, "gaiamap"

    invoke-virtual {v2, v4}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lfie;->s:Lfip;

    const-string v4, "people"

    invoke-virtual {v2, v4}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v2, v0, Lsp;->a:Lrz;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lsp;->a:Lrz;

    iget v2, v2, Lrz;->a:I

    const/16 v4, 0x19a

    if-eq v2, v4, :cond_7

    :cond_6
    throw v0

    :cond_7
    const-string v0, "PeopleSync"

    const-string v2, "Sync Token out of date, syncing all people/gaia-map"

    invoke-static {v0, v2}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3, p1}, Lfie;->a(Ljava/lang/String;Z)V

    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lfie;)Lfid;
    .locals 1

    iget-object v0, p0, Lfie;->h:Lfid;

    return-object v0
.end method

.method private b()Z
    .locals 17

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const-string v1, "PeopleSync"

    const-string v2, "Groups"

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfie;->h:Lfid;

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->s:Lfip;

    const-string v5, "groups"

    invoke-virtual {v1, v5}, Lfip;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, v2, Lfid;->j:Z

    const/4 v2, 0x0

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lfie;->s:Lfip;

    invoke-virtual {v5}, Lfip;->c()Ljava/util/Set;

    move-result-object v11

    move-object v7, v1

    move v8, v2

    move v9, v3

    move v10, v4

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->g:Lfia;

    invoke-virtual {v1}, Lfia;->c()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->x:Lgel;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfie;->p:Lffa;

    iget-object v2, v2, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfie;->p:Lffa;

    invoke-virtual {v3}, Lffa;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lfie;->d:Landroid/content/Context;

    invoke-static {v5}, Lfbd;->a(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual/range {v1 .. v6}, Lgel;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->g:Lfia;

    invoke-virtual {v1}, Lfia;->c()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->q()V

    :try_start_0
    invoke-virtual {v12}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->e()Ljava/util/List;

    move-result-object v13

    invoke-static {v13}, Lfhr;->b(Ljava/util/List;)I

    move-result v14

    if-lez v14, :cond_e

    const/4 v2, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "groups."

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->d()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lfie;->s:Lfip;

    invoke-virtual {v4, v1}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v1, "PeopleSync"

    const-string v3, "    Up to date"

    invoke-static {v1, v3}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v14, :cond_3

    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;

    const-string v4, "circle"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v5

    invoke-interface {v5}, Lgld;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v1

    invoke-interface {v1}, Lgld;->d()Ljava/lang/String;

    move-result-object v1

    const-string v4, "PeopleService"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "PeopleSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Group: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface {v11, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lfie;->s:Lfip;

    invoke-virtual {v4, v1}, Lfip;->h(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    const/4 v1, 0x0

    :goto_3
    return v1

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_3
    add-int v1, v10, v14

    move v3, v1

    :goto_4
    const/4 v1, 0x0

    move v6, v1

    move-object v4, v7

    move v5, v8

    :goto_5
    if-ge v6, v14, :cond_8

    :try_start_1
    invoke-interface {v13, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;

    const-string v7, "circle"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v8

    invoke-interface {v8}, Lgld;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v1

    const-string v7, "domain"

    invoke-interface {v1}, Lgld;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    const/4 v4, 0x1

    invoke-interface {v1}, Lgld;->b()Ljava/lang/String;

    move-result-object v1

    :goto_6
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    move-object v4, v1

    goto :goto_5

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lfie;->s:Lfip;

    invoke-virtual {v12}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v3, v1

    move v1, v10

    :goto_7
    if-ge v3, v14, :cond_d

    add-int/lit8 v10, v1, 0x1

    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;

    const-string v4, "circle"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v5

    invoke-interface {v5}, Lgld;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v4

    invoke-interface {v4}, Lgld;->d()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PeopleService"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "PeopleSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v15, "Group: "

    invoke-direct {v6, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-interface {v11, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lfie;->s:Lfip;

    invoke-virtual {v5, v4}, Lfip;->h(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lfie;->s:Lfip;

    invoke-virtual {v4, v1, v10}, Lfip;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v15, 0x1

    add-long/2addr v4, v15

    iput-wide v4, v1, Landroid/content/SyncStats;->numUpdates:J

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->h:Lfid;

    iget v4, v1, Lfid;->h:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lfid;->h:I

    :cond_6
    :goto_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v10

    goto :goto_7

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lfie;->s:Lfip;

    invoke-virtual {v4, v1, v10}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v15, 0x1

    add-long/2addr v4, v15

    iput-wide v4, v1, Landroid/content/SyncStats;->numInserts:J

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->h:Lfid;

    iget v4, v1, Lfid;->g:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lfid;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_8

    :catchall_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lfie;->s:Lfip;

    invoke-virtual {v2}, Lfip;->s()V

    throw v1

    :cond_8
    if-eqz v2, :cond_9

    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->t:Lffh;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfie;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lfie;->n:Ljava/lang/String;

    const/4 v7, 0x2

    invoke-virtual {v1, v2, v6, v7}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_9
    move-object v1, v4

    move v2, v5

    move v4, v3

    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lfie;->s:Lfip;

    invoke-virtual {v3}, Lfip;->t()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lfie;->s:Lfip;

    invoke-virtual {v3}, Lfip;->s()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lfie;->g:Lfia;

    invoke-virtual {v3}, Lfia;->c()V

    invoke-virtual {v12}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->f()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v3, v9, 0x1

    if-nez v6, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lfie;->n:Ljava/lang/String;

    if-nez v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lfie;->s:Lfip;

    invoke-virtual {v3}, Lfip;->q()V

    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lfie;->s:Lfip;

    invoke-virtual {v3, v2, v1}, Lfip;->a(ZLjava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->t()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    :cond_a
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v1}, Lfie;->a(Ljava/util/Set;Z)V

    const/4 v1, 0x1

    goto/16 :goto_3

    :catchall_1
    move-exception v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lfie;->s:Lfip;

    invoke-virtual {v2}, Lfip;->s()V

    throw v1

    :cond_b
    move-object v7, v1

    move v8, v2

    move v9, v3

    move v10, v4

    goto/16 :goto_1

    :cond_c
    move-object v1, v4

    move v4, v5

    goto/16 :goto_6

    :cond_d
    move v3, v1

    goto/16 :goto_4

    :cond_e
    move-object v1, v7

    move v2, v8

    move v4, v10

    goto :goto_9
.end method

.method static synthetic c(Lfie;)Landroid/content/SyncResult;
    .locals 1

    iget-object v0, p0, Lfie;->f:Landroid/content/SyncResult;

    return-object v0
.end method

.method private c()Z
    .locals 14

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const-string v0, "PeopleSync"

    const-string v4, "Circles"

    invoke-static {v0, v4}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lfie;->h:Lfid;

    iget-object v0, p0, Lfie;->s:Lfip;

    const-string v5, "circles"

    invoke-virtual {v0, v5}, Lfip;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v4, Lfid;->n:Z

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->d()Ljava/util/Set;

    move-result-object v8

    move-object v0, v1

    move v6, v2

    move v7, v3

    :goto_1
    iget-object v1, p0, Lfie;->g:Lfia;

    invoke-virtual {v1}, Lfia;->c()V

    iget-object v2, p0, Lfie;->v:Lglv;

    iget-object v1, p0, Lfie;->p:Lffa;

    iget-object v1, v1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lfie;->p:Lffa;

    invoke-virtual {v3}, Lffa;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v4}, Lfbd;->a(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5, v3, v4, v0}, Lglv;->a(Lglw;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lglv;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;

    iget-object v1, p0, Lfie;->g:Lfia;

    invoke-virtual {v1}, Lfia;->c()V

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->q()V

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->e()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lfhr;->b(Ljava/util/List;)I

    move-result v5

    if-lez v5, :cond_8

    const/4 v2, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "circles."

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lfie;->s:Lfip;

    invoke-virtual {v9, v1}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v1, "PeopleSync"

    const-string v3, "    Up to date"

    invoke-static {v1, v3}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgmb;

    invoke-interface {v1}, Lgmb;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v9, p0, Lfie;->s:Lfip;

    invoke-virtual {v9, v1}, Lfip;->h(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_2
    add-int v1, v7, v5

    move v13, v2

    move v2, v1

    move v1, v13

    :goto_4
    :try_start_1
    iget-object v3, p0, Lfie;->s:Lfip;

    invoke-virtual {v3}, Lfip;->p()V

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfie;->t:Lffh;

    iget-object v3, p0, Lfie;->m:Ljava/lang/String;

    iget-object v4, p0, Lfie;->n:Ljava/lang/String;

    const/4 v5, 0x2

    invoke-virtual {v1, v3, v4, v5}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_3
    :goto_5
    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->t()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    iget-object v1, p0, Lfie;->g:Lfia;

    invoke-virtual {v1}, Lfia;->c()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->h()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v6, 0x1

    if-nez v0, :cond_6

    const/4 v0, 0x1

    invoke-direct {p0, v8, v0}, Lfie;->a(Ljava/util/Set;Z)V

    const/4 v0, 0x1

    goto :goto_3

    :cond_4
    :try_start_2
    iget-object v2, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v3, v1

    move v1, v7

    :goto_6
    if-ge v3, v5, :cond_7

    add-int/lit8 v7, v1, 0x1

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgmb;

    invoke-interface {v1}, Lgmb;->c()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v10, p0, Lfie;->s:Lfip;

    invoke-virtual {v10, v9}, Lfip;->h(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, p0, Lfie;->s:Lfip;

    invoke-virtual {v9, v1, v7}, Lfip;->b(Lgmb;I)V

    iget-object v1, p0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v1, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v1, Landroid/content/SyncStats;->numUpdates:J

    iget-object v1, p0, Lfie;->h:Lfid;

    iget v9, v1, Lfid;->l:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v1, Lfid;->l:I

    :goto_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v7

    goto :goto_6

    :cond_5
    iget-object v9, p0, Lfie;->s:Lfip;

    invoke-virtual {v9, v1, v7}, Lfip;->a(Lgmb;I)V

    iget-object v1, p0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v1, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v1, Landroid/content/SyncStats;->numInserts:J

    iget-object v1, p0, Lfie;->h:Lfid;

    iget v9, v1, Lfid;->k:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v1, Lfid;->k:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_7

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_6
    move v6, v1

    move v7, v2

    goto/16 :goto_1

    :cond_7
    move v13, v2

    move v2, v1

    move v1, v13

    goto/16 :goto_4

    :cond_8
    move v2, v7

    goto/16 :goto_5
.end method

.method static synthetic d(Lfie;)Lffh;
    .locals 1

    iget-object v0, p0, Lfie;->t:Lffh;

    return-object v0
.end method

.method private d()V
    .locals 11

    const/4 v4, 0x0

    const/4 v7, 0x0

    sget-object v0, Lfbd;->u:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "PeopleSync"

    const-string v1, "Applications (Skipped)"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "PeopleSync"

    const-string v1, "Applications"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->k()Ljava/util/Set;

    move-result-object v8

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    move-object v6, v4

    :cond_2
    const-string v0, "PeopleSync"

    const-string v1, "  Getting Connected Apps"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->y:Lgej;

    iget-object v1, p0, Lfie;->p:Lffa;

    iget-object v1, v1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "me"

    const-string v3, "connected"

    iget-object v5, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v5}, Lfbd;->a(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lgej;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->b(Ljava/util/List;)I

    move-result v3

    move v1, v7

    :goto_0
    if-ge v1, v3, :cond_4

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-direct {p0, v0}, Lfie;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->e()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    const/4 v0, 0x1

    new-array v10, v0, [Lsp;

    new-instance v6, Lfif;

    invoke-direct {v6, p0, v10}, Lfif;-><init>(Lfie;[Lsp;)V

    iget-object v0, p0, Lfie;->q:Lffg;

    invoke-virtual {v0}, Lffg;->c()V

    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    new-instance v5, Lfig;

    invoke-direct {v5, p0, v2, v8}, Lfig;-><init>(Lfie;Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/util/Set;)V

    iget-object v0, p0, Lfie;->z:Lgeg;

    iget-object v1, p0, Lfie;->p:Lffa;

    iget-object v1, v1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, "visible"

    invoke-virtual/range {v0 .. v6}, Lgeg;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    goto :goto_1

    :cond_5
    :try_start_0
    iget-object v0, p0, Lfie;->q:Lffg;

    invoke-virtual {v0}, Lffg;->d()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    :try_start_1
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lfie;->s:Lfip;

    invoke-virtual {v2, v0}, Lfip;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->h:Lfid;

    iget v2, v0, Lfid;->z:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lfid;->z:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :catch_0
    move-exception v0

    new-instance v1, Lfic;

    invoke-direct {v1, v0}, Lfic;-><init>(Ljava/lang/Exception;)V

    throw v1

    :cond_6
    :try_start_2
    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    aget-object v0, v10, v7

    if-eqz v0, :cond_0

    aget-object v0, v10, v7

    throw v0
.end method

.method static synthetic e(Lfie;)V
    .locals 1

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    return-void
.end method

.method private e()Z
    .locals 10

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lfie;->n:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v6

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    const-string v0, "PeopleSync"

    const-string v1, "Me"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    iget-object v0, p0, Lfie;->u:Lgly;

    iget-object v1, p0, Lfie;->p:Lffa;

    iget-object v1, v1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "me"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "disabled"

    aput-object v5, v4, v7

    const-string v5, "blocked"

    aput-object v5, v4, v6

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    sget-object v5, Lfie;->a:Lglz;

    invoke-virtual/range {v0 .. v5}, Lgly;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lglz;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    move-result-object v8

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    :try_start_0
    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfie;->s:Lfip;

    const-string v2, "me"

    invoke-virtual {v1, v2}, Lfip;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PeopleSync"

    const-string v1, "    Up to date"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    iget-boolean v0, p0, Lfie;->j:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->m()Lfhp;

    move-result-object v1

    iget-object v2, p0, Lfie;->m:Ljava/lang/String;

    invoke-static {v8}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v8}, Lfhr;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->g()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->d()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v9, v0}, Lfhp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :goto_2
    if-nez v0, :cond_0

    iget-object v0, v1, Lfhp;->b:Lfjl;

    invoke-virtual {v0, v2, v9}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->n()Lfhq;

    move-result-object v2

    iget-object v1, p0, Lfie;->m:Ljava/lang/String;

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lfip;

    iget-object v3, v2, Lfhq;->a:Landroid/content/Context;

    invoke-direct {v0, v3, v1, v9}, Lfip;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lfip;->j()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {v0}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lfbd;->o:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lfhq;->a(I)I

    move-result v5

    iget-object v0, v2, Lfhq;->b:Lfjn;

    invoke-static {v3}, Lfjp;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v9, v4, v5}, Lfjn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v3, v5}, Lfjp;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v2, Lfhq;->a:Landroid/content/Context;

    invoke-static {v4}, Lfft;->a(Landroid/content/Context;)Lfft;

    move-result-object v4

    invoke-virtual {v4, v0}, Lfft;->a(Ljava/lang/String;)[B

    move-result-object v4

    if-eqz v4, :cond_1

    sget-object v0, Lffl;->a:[B

    if-eq v4, v0, :cond_1

    sget-object v0, Lffl;->b:[B

    if-eq v4, v0, :cond_1

    iget-object v0, v2, Lfhq;->b:Lfjn;

    invoke-static {v3}, Lfjp;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v9

    invoke-virtual/range {v0 .. v5}, Lfjn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BI)Z

    :cond_1
    :goto_4
    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->o()Ljava/util/List;

    move-result-object v0

    const-string v1, "googlePlus"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->o()Ljava/util/List;

    move-result-object v0

    const-string v1, "googlePlusDisabledByAdmin"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_5
    return v6

    :cond_2
    move v0, v7

    goto/16 :goto_0

    :cond_3
    :try_start_1
    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0, v8}, Lfip;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    iget-object v0, p0, Lfie;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numUpdates:J

    iget-object v0, p0, Lfie;->t:Lffh;

    iget-object v1, p0, Lfie;->m:Ljava/lang/String;

    iget-object v2, p0, Lfie;->n:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lfie;->s:Lfip;

    const-string v1, "me"

    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_4
    invoke-virtual {v0, v9}, Lfip;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_5
    iget-object v0, v2, Lfhq;->b:Lfjn;

    invoke-virtual {v0, v1, v9}, Lfjn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    move v6, v7

    goto :goto_5

    :cond_7
    move v0, v7

    goto/16 :goto_2
.end method

.method private f()V
    .locals 5

    iget-object v0, p0, Lfie;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    const-string v0, "PeopleSync"

    const-string v1, "Orphaned pages sync requests"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    :try_start_0
    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lfie;->t:Lffh;

    iget-object v3, p0, Lfie;->m:Ljava/lang/String;

    const/16 v4, 0x40

    invoke-virtual {v2, v3, v0, v4}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->g()V

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    return-void
.end method

.method static synthetic f(Lfie;)Z
    .locals 1

    invoke-direct {p0}, Lfie;->g()Z

    move-result v0

    return v0
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lfie;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 13

    const/4 v7, 0x0

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-string v0, "PeopleSync"

    const-string v1, "Sync sanity check."

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->q()V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    iget-object v0, p0, Lfie;->u:Lgly;

    iget-object v1, p0, Lfie;->p:Lffa;

    iget-object v1, v1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "me"

    const-string v3, "all"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lfie;->d:Landroid/content/Context;

    invoke-static {v5}, Lfbd;->b(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lfie;->p:Lffa;

    iget-object v6, v6, Lffa;->b:Ljava/lang/String;

    const/4 v8, 0x0

    sget-object v9, Lfie;->c:Lgma;

    invoke-virtual/range {v0 .. v9}, Lgly;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgma;)Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;

    move-result-object v2

    iget-object v0, p0, Lfie;->g:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->e()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lfhr;->b(Ljava/util/List;)I

    move-result v4

    move v1, v10

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v5, p0, Lfie;->s:Lfip;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    iget-object v6, p0, Lfie;->h:Lfid;

    invoke-virtual {v5, v0, v6}, Lfip;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Lfid;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->f()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    iget-object v0, p0, Lfie;->h:Lfid;

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->m()I

    move-result v1

    iput v1, v0, Lfid;->F:I

    iget-object v0, p0, Lfie;->h:Lfid;

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->n()I

    move-result v1

    iput v1, v0, Lfid;->G:I

    iget-object v0, p0, Lfie;->h:Lfid;

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->o()I

    move-result v1

    iput v1, v0, Lfid;->H:I

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lfie;->s:Lfip;

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, p0, Lfie;->h:Lfid;

    iget v1, v0, Lfid;->E:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfid;->E:I

    const-string v0, "PeopleService"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v1, "PeopleSync"

    const-string v2, "Sanity-check: [%s] DB people count=%d/%d  Actual=%d  inconsistent-members=%d"

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lfie;->h:Lfid;

    invoke-virtual {v0}, Lfid;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "OK"

    :goto_1
    aput-object v0, v3, v10

    iget-object v0, p0, Lfie;->h:Lfid;

    iget v0, v0, Lfid;->G:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v11

    const/4 v0, 0x2

    iget-object v4, p0, Lfie;->h:Lfid;

    iget v4, v4, Lfid;->F:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    iget-object v0, p0, Lfie;->h:Lfid;

    iget v0, v0, Lfid;->I:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v12

    const/4 v0, 0x4

    iget-object v4, p0, Lfie;->h:Lfid;

    iget v4, v4, Lfid;->K:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lfie;->h:Lfid;

    new-instance v1, Lbdj;

    invoke-direct {v1}, Lbdj;-><init>()V

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lbdj;->a:Ljava/lang/Boolean;

    iget v2, p0, Lfie;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Lfid;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lbdj;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lfid;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lbdj;->d:Ljava/lang/Boolean;

    iget v2, v0, Lfid;->F:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->e:Ljava/lang/Integer;

    iget v2, v0, Lfid;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->f:Ljava/lang/Integer;

    iget v2, v0, Lfid;->H:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->g:Ljava/lang/Integer;

    iget v2, v0, Lfid;->I:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->h:Ljava/lang/Integer;

    iget v2, v0, Lfid;->J:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->i:Ljava/lang/Integer;

    iget v2, v0, Lfid;->G:I

    iget v3, v0, Lfid;->I:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->j:Ljava/lang/Integer;

    iget v2, v0, Lfid;->H:I

    iget v3, v0, Lfid;->J:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->k:Ljava/lang/Integer;

    iget v2, v0, Lfid;->K:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->l:Ljava/lang/Integer;

    iget v2, v0, Lfid;->L:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lbdj;->m:Ljava/lang/Integer;

    iget-boolean v0, v0, Lfid;->r:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lbdj;->n:Ljava/lang/Boolean;

    invoke-static {v1}, Lbcm;->a(Lbdj;)V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfie;->s:Lfip;

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_3
    const-string v0, "FAIL"

    goto/16 :goto_1
.end method


# virtual methods
.method final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Set;Lfdx;)V
    .locals 5

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->b(Ljava/util/List;)I

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lfie;->B:Lfii;

    invoke-virtual {v1, p1, v0, p3}, Lfii;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/lang/String;Lfdx;)V

    iget-object v1, p0, Lfie;->C:Lfin;

    invoke-virtual {v1, p1, v0, p3}, Lfin;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/lang/String;Lfdx;)V

    iget-object v1, p0, Lfie;->D:Lfio;

    invoke-virtual {v1, p1, v0, p3}, Lfio;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/lang/String;Lfdx;)V

    goto :goto_0
.end method
