.class public final Lamo;
.super Lva;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/GetToken;

.field private volatile b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/GetToken;)V
    .locals 2

    iput-object p1, p0, Lamo;->a:Lcom/google/android/gms/auth/GetToken;

    invoke-direct {p0}, Lva;-><init>()V

    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lamo;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    new-instance v0, Laoy;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Laoy;-><init>(Landroid/content/Context;)V

    new-instance v1, Laqw;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Laqw;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;

    move-result-object v0

    invoke-virtual {v1, v0}, Laqw;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;->a()Laso;

    move-result-object v2

    sget-object v3, Laso;->a:Laso;

    invoke-virtual {v2, v3}, Laso;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Laso;->M:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;->a()Laso;

    move-result-object v3

    invoke-virtual {v3}, Laso;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v2, "booleanResult"

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;->a()Laso;

    move-result-object v0

    sget-object v3, Laso;->a:Laso;

    invoke-virtual {v0, v3}, Laso;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 11

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Latp;

    iget-object v2, p0, Lamo;->a:Lcom/google/android/gms/auth/GetToken;

    invoke-direct {v3, v2}, Latp;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    new-instance v6, Laoy;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v6, v2}, Laoy;-><init>(Landroid/content/Context;)V

    if-nez p3, :cond_0

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    :cond_0
    new-instance v7, Land;

    invoke-direct {v7, v6}, Land;-><init>(Laoy;)V

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    sget-object v2, Lamr;->a:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {p3, v2, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sget-object v9, Lamr;->b:Ljava/lang/String;

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v2, :cond_1

    invoke-virtual {v6, v9}, Laoy;->a(Ljava/lang/String;)I

    move-result v2

    :cond_1
    invoke-static {v8, v9, v2, v6}, Laow;->a(ILjava/lang/String;ILaoy;)Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v2

    invoke-virtual {v7, v2, p1, p2, p3}, Land;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v7

    if-nez v7, :cond_2

    const-string v2, "token result is null"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    iget-boolean v9, p0, Lamo;->b:Z

    if-eqz v9, :cond_2

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v8, "GLSActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/auth/GetToken;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget-object v2, Lamr;->b:Ljava/lang/String;

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v4, v9, v4

    iput-wide v4, v3, Latp;->e:J

    const-string v2, "handle_notification"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v3, Latp;->f:Z

    const-string v2, "callback_intent"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_3

    move v2, v0

    :goto_0
    iput-boolean v2, v3, Latp;->g:Z

    const-string v2, "sync_extras"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_4

    :goto_1
    iput-boolean v0, v3, Latp;->h:Z

    const-string v0, "TokenCache"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v3, Latp;->i:Z

    invoke-virtual {v3, v8}, Latp;->a(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Latp;->b(Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Laoy;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Latp;->c(Ljava/lang/String;)V

    invoke-virtual {v3}, Latp;->a()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v7

    :cond_3
    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/auth/GetToken;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - getToken exception!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method
