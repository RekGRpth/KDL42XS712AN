.class public abstract Lbje;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbq;
.implements Lbdn;
.implements Lbjp;


# static fields
.field public static final e:[Ljava/lang/String;


# instance fields
.field final b:Landroid/os/Handler;

.field public final b_:Landroid/content/Context;

.field public final c:[Ljava/lang/String;

.field d:Z

.field private final f:Landroid/os/Looper;

.field private g:Landroid/os/IInterface;

.field private final h:Ljava/util/ArrayList;

.field private i:Lbjk;

.field private volatile j:I

.field private final k:Lbjn;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lbje;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbje;->h:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, Lbje;->j:I

    iput-boolean v2, p0, Lbje;->d:Z

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbje;->b_:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lbje;->f:Landroid/os/Looper;

    new-instance v0, Lbjn;

    iget-object v1, p0, Lbje;->f:Landroid/os/Looper;

    invoke-direct {v0, v1, p0}, Lbjn;-><init>(Landroid/os/Looper;Lbjp;)V

    iput-object v0, p0, Lbje;->k:Lbjn;

    new-instance v0, Lbjf;

    iget-object v1, p0, Lbje;->f:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lbjf;-><init>(Lbje;Landroid/os/Looper;)V

    iput-object v0, p0, Lbje;->b:Landroid/os/Handler;

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lbje;->c:[Ljava/lang/String;

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbje;->h:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, Lbje;->j:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbje;->d:Z

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbje;->b_:Landroid/content/Context;

    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lbje;->f:Landroid/os/Looper;

    new-instance v0, Lbjn;

    invoke-direct {v0, p2, p0}, Lbjn;-><init>(Landroid/os/Looper;Lbjp;)V

    iput-object v0, p0, Lbje;->k:Lbjn;

    new-instance v0, Lbjf;

    invoke-direct {v0, p0, p2}, Lbjf;-><init>(Lbje;Landroid/os/Looper;)V

    iput-object v0, p0, Lbje;->b:Landroid/os/Handler;

    invoke-virtual {p0, p5}, Lbje;->a([Ljava/lang/String;)V

    iput-object p5, p0, Lbje;->c:[Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdx;

    invoke-virtual {p0, v0}, Lbje;->a(Lbdx;)V

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdy;

    invoke-virtual {p0, v0}, Lbje;->a(Lbdy;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lbbr;Lbbs;[Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lbjh;

    invoke-direct {v3, p2}, Lbjh;-><init>(Lbbr;)V

    new-instance v4, Lbjl;

    invoke-direct {v4, p3}, Lbjl;-><init>(Lbbs;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbje;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lbje;I)I
    .locals 0

    iput p1, p0, Lbje;->j:I

    return p1
.end method

.method static synthetic a(Lbje;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, Lbje;->g:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(Lbje;)Lbjn;
    .locals 1

    iget-object v0, p0, Lbje;->k:Lbjn;

    return-object v0
.end method

.method static synthetic b(Lbje;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lbje;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lbje;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lbje;->g:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic d(Lbje;)Lbjk;
    .locals 1

    iget-object v0, p0, Lbje;->i:Lbjk;

    return-object v0
.end method

.method static synthetic e(Lbje;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lbje;->b_:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lbje;)Lbjk;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbje;->i:Lbjk;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
.end method

.method public a()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lbje;->d:Z

    const/4 v0, 0x2

    iput v0, p0, Lbje;->j:I

    iget-object v0, p0, Lbje;->b_:Landroid/content/Context;

    invoke-static {v0}, Lbbv;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    iput v1, p0, Lbje;->j:I

    iget-object v1, p0, Lbje;->b:Landroid/os/Handler;

    iget-object v2, p0, Lbje;->b:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbje;->i:Lbjk;

    if-eqz v0, :cond_2

    const-string v0, "GmsClient"

    const-string v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lbje;->g:Landroid/os/IInterface;

    iget-object v0, p0, Lbje;->b_:Landroid/content/Context;

    invoke-static {v0}, Lbjq;->a(Landroid/content/Context;)Lbjq;

    move-result-object v0

    invoke-virtual {p0}, Lbje;->b_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbje;->i:Lbjk;

    invoke-virtual {v0, v1, v2}, Lbjq;->b(Ljava/lang/String;Lbjk;)V

    :cond_2
    new-instance v0, Lbjk;

    invoke-direct {v0, p0}, Lbjk;-><init>(Lbje;)V

    iput-object v0, p0, Lbje;->i:Lbjk;

    iget-object v0, p0, Lbje;->b_:Landroid/content/Context;

    invoke-static {v0}, Lbjq;->a(Landroid/content/Context;)Lbjq;

    move-result-object v0

    invoke-virtual {p0}, Lbje;->b_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbje;->i:Lbjk;

    invoke-virtual {v0, v1, v2}, Lbjq;->a(Ljava/lang/String;Lbjk;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbje;->b_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lbje;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbje;->b:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lbje;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbje;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lbjm;

    invoke-direct {v3, p0, p1, p2, p3}, Lbjm;-><init>(Lbje;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Lbbr;)V
    .locals 2

    iget-object v0, p0, Lbje;->k:Lbjn;

    new-instance v1, Lbjh;

    invoke-direct {v1, p1}, Lbjh;-><init>(Lbbr;)V

    invoke-virtual {v0, v1}, Lbjn;->a(Lbdx;)V

    return-void
.end method

.method public final a(Lbbs;)V
    .locals 1

    iget-object v0, p0, Lbje;->k:Lbjn;

    invoke-virtual {v0, p1}, Lbjn;->a(Lbbs;)V

    return-void
.end method

.method public final a(Lbdx;)V
    .locals 1

    iget-object v0, p0, Lbje;->k:Lbjn;

    invoke-virtual {v0, p1}, Lbjn;->a(Lbdx;)V

    return-void
.end method

.method public final a(Lbdy;)V
    .locals 1

    iget-object v0, p0, Lbje;->k:Lbjn;

    invoke-virtual {v0, p1}, Lbjn;->a(Lbbs;)V

    return-void
.end method

.method public final a(Lbjg;)V
    .locals 3

    iget-object v1, p0, Lbje;->h:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbje;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbje;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbje;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract a(Lbjy;Lbjj;)V
.end method

.method protected varargs a([Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbje;->d:Z

    iget-object v2, p0, Lbje;->h:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lbje;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lbje;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjg;

    invoke-virtual {v0}, Lbjg;->f()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbje;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iput v0, p0, Lbje;->j:I

    iput-object v4, p0, Lbje;->g:Landroid/os/IInterface;

    iget-object v0, p0, Lbje;->i:Lbjk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbje;->b_:Landroid/content/Context;

    invoke-static {v0}, Lbjq;->a(Landroid/content/Context;)Lbjq;

    move-result-object v0

    invoke-virtual {p0}, Lbje;->b_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbje;->i:Lbjk;

    invoke-virtual {v0, v1, v2}, Lbjq;->b(Ljava/lang/String;Lbjk;)V

    iput-object v4, p0, Lbje;->i:Lbjk;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method protected final b(Landroid/os/IBinder;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lbjz;->a(Landroid/os/IBinder;)Lbjy;

    move-result-object v0

    new-instance v1, Lbjj;

    invoke-direct {v1, p0}, Lbjj;-><init>(Lbje;)V

    invoke-virtual {p0, v0, v1}, Lbje;->a(Lbjy;Lbjj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GmsClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b(Lbbr;)Z
    .locals 2

    iget-object v0, p0, Lbje;->k:Lbjn;

    new-instance v1, Lbjh;

    invoke-direct {v1, p1}, Lbjh;-><init>(Lbbr;)V

    invoke-virtual {v0, v1}, Lbjn;->b(Lbdx;)Z

    move-result v0

    return v0
.end method

.method protected abstract b_()Ljava/lang/String;
.end method

.method public final c(Lbbr;)V
    .locals 2

    iget-object v0, p0, Lbje;->k:Lbjn;

    new-instance v1, Lbjh;

    invoke-direct {v1, p1}, Lbjh;-><init>(Lbbr;)V

    invoke-virtual {v0, v1}, Lbjn;->c(Lbdx;)V

    return-void
.end method

.method protected abstract c_()Ljava/lang/String;
.end method

.method public final d()Z
    .locals 2

    iget v0, p0, Lbje;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d_()Z
    .locals 2

    iget v0, p0, Lbje;->j:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e_()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lbje;->f:Landroid/os/Looper;

    return-object v0
.end method

.method public final f_()Z
    .locals 1

    iget-boolean v0, p0, Lbje;->d:Z

    return v0
.end method

.method public final g()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lbje;->b_:Landroid/content/Context;

    return-object v0
.end method

.method public final h()V
    .locals 2

    invoke-virtual {p0}, Lbje;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final i()Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0}, Lbje;->h()V

    iget-object v0, p0, Lbje;->g:Landroid/os/IInterface;

    return-object v0
.end method
