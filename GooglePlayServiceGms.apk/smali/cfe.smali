.class public final Lcfe;
.super Lcfl;
.source "SourceFile"


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field private b:Ljava/util/Date;

.field private c:J

.field private d:J

.field private g:Ljava/util/Date;

.field private h:Ljava/util/Date;

.field private i:J

.field private final j:Ljava/lang/String;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lcom/google/android/gms/drive/data/sync/syncadapter/DatabaseNotificationContentProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/accounts"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcfe;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcdu;Ljava/lang/String;)V
    .locals 6

    const-wide v4, 0x7fffffffffffffffL

    const-wide/16 v2, 0x0

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v0

    sget-object v1, Lcfe;->a:Landroid/net/Uri;

    invoke-direct {p0, p1, v0, v1}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfe;->b:Ljava/util/Date;

    iput-wide v2, p0, Lcfe;->c:J

    iput-wide v2, p0, Lcfe;->d:J

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfe;->g:Ljava/util/Date;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfe;->h:Ljava/util/Date;

    iput-wide v2, p0, Lcfe;->i:J

    invoke-virtual {p2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfe;->j:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcdu;Landroid/database/Cursor;)Lcfe;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Lcfe;

    sget-object v0, Lcdd;->a:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v0}, Lcfe;-><init>(Lcdu;Ljava/lang/String;)V

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v0

    invoke-virtual {v0}, Lcdc;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcfe;->d(J)V

    sget-object v0, Lcdd;->b:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->e(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, v2, Lcfe;->k:Z

    sget-object v0, Lcdd;->c:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iput-wide v3, v2, Lcfe;->c:J

    sget-object v0, Lcdd;->d:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v0}, Lcfe;->a(Ljava/util/Date;)V

    sget-object v0, Lcdd;->h:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcfe;->b(J)V

    sget-object v0, Lcdd;->e:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v3

    if-nez v3, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, v2, Lcfe;->g:Ljava/util/Date;

    sget-object v0, Lcdd;->f:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    iput-object v1, v2, Lcfe;->h:Ljava/util/Date;

    sget-object v0, Lcdd;->g:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, v2, Lcfe;->i:J

    return-object v2

    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    goto :goto_1
.end method


# virtual methods
.method public final a(J)V
    .locals 0

    iput-wide p1, p0, Lcfe;->c:J

    return-void
.end method

.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lcdd;->a:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfe;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcdd;->b:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcfe;->k:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    sget-object v0, Lcdd;->c:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcfe;->c:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcdd;->d:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfe;->b:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcdd;->h:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcfe;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcfe;->g:Ljava/util/Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcdd;->e:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfe;->g:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_0
    iget-object v0, p0, Lcfe;->h:Ljava/util/Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcdd;->f:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfe;->h:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_1
    sget-object v0, Lcdd;->g:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcfe;->i:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-void

    :cond_0
    sget-object v0, Lcdd;->e:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcdd;->f:Lcdd;

    invoke-virtual {v0}, Lcdd;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/util/Date;)V
    .locals 0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcfe;->b:Ljava/util/Date;

    return-void
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcfe;->k:Z

    return v0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcfe;->k:Z

    return-void
.end method

.method public final b(J)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    iput-wide p1, p0, Lcfe;->d:J

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcfe;->g:Ljava/util/Date;

    return-void
.end method

.method public final c(J)V
    .locals 0

    iput-wide p1, p0, Lcfe;->i:J

    return-void
.end method

.method public final c(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcfe;->h:Ljava/util/Date;

    return-void
.end method

.method public final c()Z
    .locals 4

    iget-wide v0, p0, Lcfe;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    iget-wide v0, p0, Lcfe;->c:J

    return-wide v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lcfe;->d:J

    return-wide v0
.end method

.method public final f()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcfe;->g:Ljava/util/Date;

    return-object v0
.end method

.method public final g()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcfe;->h:Ljava/util/Date;

    return-object v0
.end method

.method public final h()J
    .locals 2

    iget-wide v0, p0, Lcfe;->i:J

    return-wide v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfe;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lcfc;
    .locals 4

    new-instance v0, Lcfc;

    iget-object v1, p0, Lcfe;->j:Ljava/lang/String;

    iget-wide v2, p0, Lcfl;->f:J

    invoke-direct {v0, v1, v2, v3}, Lcfc;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Account[%s, sqlId=%d%s]"

    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lcfe;->j:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-wide v4, p0, Lcfl;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    iget-object v0, p0, Lcfe;->h:Ljava/util/Date;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ", clipped"

    goto :goto_0
.end method
