.class public final Lhnj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Ljava/lang/String;

.field private static c:Lbfy;

.field private static final d:Ljava/util/Collection;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    sput-object v0, Lhnj;->a:Landroid/content/Context;

    sput-object v0, Lhnj;->b:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.google.android.location"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lhnj;->d:Ljava/util/Collection;

    return-void
.end method

.method private static a()Lafu;
    .locals 4

    const/4 v1, 0x0

    sget-object v0, Lhnj;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v0, "LocationAnalytics"

    const-string v2, "empty AnalyticsContext, must call init() first"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhnj;->c:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lhnj;->a:Landroid/content/Context;

    invoke-static {v0}, Lafh;->a(Landroid/content/Context;)Lafh;

    move-result-object v0

    const-string v2, "UA-44492294-1"

    invoke-virtual {v0, v2}, Lafh;->a(Ljava/lang/String;)Lafu;

    move-result-object v0

    invoke-virtual {v0}, Lafu;->a()V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v2

    sget-object v3, Lafg;->m:Lafg;

    invoke-virtual {v2, v3}, Laff;->a(Lafg;)V

    iget-object v2, v0, Lafu;->a:Lafv;

    const-string v3, "sessionControl"

    invoke-virtual {v2, v3, v1}, Lafv;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lhnj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lafu;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "utm_source="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lhnj;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lafu;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Lbfy;)V
    .locals 2

    const-class v1, Lhnj;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lhnj;->a:Landroid/content/Context;

    sput-object p1, Lhnj;->b:Ljava/lang/String;

    sput-object p2, Lhnj;->c:Lbfy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 4

    const/4 v2, 0x3

    invoke-static {}, Lhnj;->a()Lafu;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "LocationAnalytics"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LocationAnalytics"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "location tracking disabled, can\'t send "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "LocationAnalytics"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "LocationAnalytics"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Event "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {v0, p0, p1, p2, p3}, Lafu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Throwable;Z)V
    .locals 8

    const/4 v2, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {}, Lhnj;->a()Lafu;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "LocationAnalytics"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LocationAnalytics"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "location tracking disabled, can\'t send "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "LocationAnalytics"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "LocationAnalytics"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", fatal: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v0, Laft;

    sget-object v2, Lhnj;->a:Landroid/content/Context;

    sget-object v3, Lhnj;->d:Ljava/util/Collection;

    invoke-direct {v0, v2, v3}, Laft;-><init>(Landroid/content/Context;Ljava/util/Collection;)V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v2

    sget-object v3, Lafg;->E:Lafg;

    invoke-virtual {v2, v3}, Laff;->a(Lafg;)V

    iput-object v0, v1, Lafu;->b:Laes;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lafu;->a()V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v2

    sget-object v3, Lafg;->f:Lafg;

    invoke-virtual {v2, v3}, Laff;->a(Lafg;)V

    iget-object v2, v1, Lafu;->b:Laes;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lafu;->b:Laes;

    invoke-interface {v2, v0, p0}, Laes;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {}, Laff;->a()Laff;

    move-result-object v2

    invoke-virtual {v2, v7}, Laff;->a(Z)V

    invoke-virtual {v1}, Lafu;->a()V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v2

    sget-object v3, Lafg;->e:Lafg;

    invoke-virtual {v2, v3}, Laff;->a(Lafg;)V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v2

    invoke-virtual {v2, v7}, Laff;->a(Z)V

    const-string v2, "exception"

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v4, "exDescription"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "exFatal"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    sget-object v4, Lafg;->H:Lafg;

    invoke-virtual {v0, v4}, Laff;->a(Lafg;)V

    invoke-virtual {v1, v2, v3}, Lafu;->a(Ljava/lang/String;Ljava/util/Map;)V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    invoke-virtual {v0, v6}, Laff;->a(Z)V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    invoke-virtual {v0, v6}, Laff;->a(Z)V

    goto/16 :goto_0

    :cond_3
    :try_start_0
    invoke-static {}, Laff;->a()Laff;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Laff;->a(Z)V

    const-string v2, "exception"

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v5, Ljava/io/ObjectOutputStream;

    invoke-direct {v5, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v5, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v5}, Ljava/io/ObjectOutputStream;->close()V

    const-string v5, "rawException"

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-static {v4}, Lafx;->a([B)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_4

    const-string v4, "exceptionThreadName"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    const-string v0, "exFatal"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    sget-object v4, Lafg;->I:Lafg;

    invoke-virtual {v0, v4}, Laff;->a(Lafg;)V

    invoke-virtual {v1, v2, v3}, Lafu;->a(Ljava/lang/String;Ljava/util/Map;)V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Laff;->a(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v0, "trackException: couldn\'t serialize, sending \"Unknown Exception\""

    invoke-static {v0}, Lafl;->e(Ljava/lang/String;)I

    const-string v0, "Unknown Exception"

    goto/16 :goto_1
.end method
