.class public final Lapo;
.super Lark;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/be/GoogleAccountDataService;

.field private final b:Lapp;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/be/GoogleAccountDataService;Lapp;)V
    .locals 0

    iput-object p1, p0, Lapo;->a:Lcom/google/android/gms/auth/be/GoogleAccountDataService;

    invoke-direct {p0}, Lark;-><init>()V

    iput-object p2, p0, Lapo;->b:Lapp;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckResponse;
    .locals 3

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;->d()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;->e()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-static {}, Lapp;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-static {p1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidanceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-static {p1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidanceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateResult;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-static {p1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {v0, p1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameResponse;
    .locals 3

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameRequest;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {v0, p1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {v0, p1}, Lapp;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoResponse;
    .locals 3

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoRequest;->b()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lapp;->a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckResponse;
    .locals 2

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;->e()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 5

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->a()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->b()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->c()Z

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->d()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 3

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;->a()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;->b()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 4

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->n()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->o()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->p()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v3

    invoke-virtual {v0, v1, v2, p1, v3}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 5

    const-string v0, "GLSUser"

    const-string v1, "GoogleAccountDataService.getToken()"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TokenRequest cannot be null!"

    invoke-static {v0, p1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->i()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->j()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    sget-object v0, Laqc;->e:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "auth.getToken.response.status."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v4

    invoke-virtual {v4}, Laso;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/status/StatusService;->a(Ljava/util/List;)V

    :cond_0
    return-object v1
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 3

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;->a()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;->b()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lapp;->b(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfigRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfig;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {v0, p1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfigRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfig;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {v0, p1, p2}, Lapp;->a(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {v0, p1}, Lapp;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 2

    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->n()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->o()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->p()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    invoke-virtual {v0, v1, p1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method
