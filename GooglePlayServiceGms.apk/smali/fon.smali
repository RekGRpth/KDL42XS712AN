.class public final Lfon;
.super Lk;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private Y:Lfoo;

.field private Z:Landroid/accounts/Account;

.field private aa:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

.field private ab:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lk;-><init>()V

    return-void
.end method

.method public static a(Landroid/accounts/Account;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;Ljava/lang/String;)Lfon;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "moment"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lfon;

    invoke-direct {v1}, Lfon;-><init>()V

    invoke-virtual {v1, v0}, Lfon;->g(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final M_()V
    .locals 1

    invoke-super {p0}, Lk;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfon;->Y:Lfoo;

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Lk;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfoo;

    if-eqz v0, :cond_0

    check-cast p1, Lfoo;

    iput-object p1, p0, Lfon;->Y:Lfoo;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DeleteMomentDialog has to be hosted by an Activity that implements OnDeleteMomentAcceptedListener."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lk;->a_(Landroid/os/Bundle;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lfon;->Z:Landroid/accounts/Account;

    const-string v0, "moment"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    iput-object v0, p0, Lfon;->aa:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    const-string v0, "calling_package_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfon;->ab:Ljava/lang/String;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b03a3    # com.google.android.gms.R.string.plus_manage_moment_delete_confirm

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b037e    # com.google.android.gms.R.string.plus_disconnect_source_dialog_negative_button

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0400d2    # com.google.android.gms.R.layout.plus_delete_moment_dialog_contents

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a0071    # com.google.android.gms.R.id.message

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lfon;->aa:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 5

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfon;->Z:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lbcj;->w:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbck;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfon;->ab:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lfon;->Y:Lfoo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lfon;->Y:Lfoo;

    iget-object v1, p0, Lfon;->Z:Landroid/accounts/Account;

    iget-object v1, p0, Lfon;->aa:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lfoo;->f_(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfon;->Z:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lbcj;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbck;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfon;->ab:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :goto_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lk;->a(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfon;->Z:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lbcj;->w:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbck;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfon;->ab:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1
.end method
