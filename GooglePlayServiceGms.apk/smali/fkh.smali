.class public final Lfkh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Landroid/os/Handler;

.field private static final c:Landroid/content/ComponentName;


# instance fields
.field final a:Lfkn;

.field private final d:Landroid/content/Context;

.field private final e:Lfkl;

.field private final f:Ljava/util/Queue;

.field private g:Luw;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lfkh;->b:Landroid/os/Handler;

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.apps.plus"

    const-string v2, "com.google.android.apps.photos.service.PhotosService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lfkh;->c:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfki;

    sget-object v1, Lfkh;->c:Landroid/content/ComponentName;

    invoke-direct {v0, p0, v1}, Lfki;-><init>(Lfkh;Landroid/content/ComponentName;)V

    iput-object v0, p0, Lfkh;->a:Lfkn;

    iput-object p1, p0, Lfkh;->d:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, Lfkh;->e:Lfkl;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfkh;->f:Ljava/util/Queue;

    invoke-direct {p0}, Lfkh;->e()Z

    return-void
.end method

.method static synthetic a(Lfkh;)Lfkl;
    .locals 1

    iget-object v0, p0, Lfkh;->e:Lfkl;

    return-object v0
.end method

.method static synthetic a(Lfkh;Luw;)Luw;
    .locals 0

    iput-object p1, p0, Lfkh;->g:Luw;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    sget-object v2, Lfkh;->c:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic b(Lfkh;)V
    .locals 1

    :goto_0
    iget-object v0, p0, Lfkh;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfkh;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkm;

    invoke-interface {v0}, Lfkm;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lfkh;)V
    .locals 1

    :goto_0
    iget-object v0, p0, Lfkh;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfkh;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkm;

    invoke-interface {v0}, Lfkm;->b()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic d(Lfkh;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lfkh;->d:Landroid/content/Context;

    return-object v0
.end method

.method private e()Z
    .locals 3

    iget-object v0, p0, Lfkh;->a:Lfkn;

    invoke-virtual {v0}, Lfkn;->d()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v1, Lfkh;->b:Landroid/os/Handler;

    new-instance v2, Lfkj;

    invoke-direct {v2, p0}, Lfkj;-><init>(Lfkh;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lfkh;->a:Lfkn;

    invoke-virtual {v1}, Lfkn;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "PhotosClient"

    const-string v2, "PhotosService is not connected, returning default, null."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lfkh;->g:Luw;

    invoke-interface {v1, p1}, Luw;->a(Ljava/lang/String;)Landroid/app/PendingIntent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Lfkm;)V
    .locals 1

    iget-object v0, p0, Lfkh;->a:Lfkn;

    invoke-virtual {v0}, Lfkn;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lfkm;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lfkh;->f:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lfkh;->e()Z

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lfkh;->a:Lfkn;

    invoke-virtual {v1}, Lfkn;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "PhotosClient"

    const-string v2, "PhotosService is not connected, returning default, disabled."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lfkh;->g:Luw;

    invoke-interface {v1}, Luw;->a()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lfkh;->a:Lfkn;

    invoke-virtual {v1}, Lfkn;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "PhotosClient"

    const-string v2, "PhotosService is not connected, returning default, null."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lfkh;->g:Luw;

    invoke-interface {v1}, Luw;->b()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final c()Landroid/app/PendingIntent;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lfkh;->a:Lfkn;

    invoke-virtual {v1}, Lfkn;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "PhotosClient"

    const-string v2, "PhotosService is not connected, returning default, null."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lfkh;->g:Luw;

    invoke-interface {v1}, Luw;->c()Landroid/app/PendingIntent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lfkh;->a:Lfkn;

    invoke-virtual {v0}, Lfkn;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfkh;->b:Landroid/os/Handler;

    new-instance v1, Lfkk;

    invoke-direct {v1, p0}, Lfkk;-><init>(Lfkh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
