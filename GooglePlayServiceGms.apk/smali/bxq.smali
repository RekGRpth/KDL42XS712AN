.class public final Lbxq;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)V
    .locals 0

    iput-object p1, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lbxq;-><init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    new-instance v0, Lcbm;

    iget-object v1, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbm;-><init>(Lcoy;)V

    invoke-virtual {v0}, Lcbm;->a()Lcbn;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 5

    const/4 v4, 0x1

    check-cast p1, Lcbn;

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->e(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->g(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    iget-wide v2, p1, Lcbn;->a:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->h(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    iget-wide v2, p1, Lcbn;->b:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->i(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    iget-wide v2, p1, Lcbn;->c:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->j(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    iget-wide v2, p1, Lcbn;->d:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->c(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0b006a    # com.google.android.gms.R.string.drive_storage_management_reclaim_button_label

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->c(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->f(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->e(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->c(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lbxq;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->f(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
