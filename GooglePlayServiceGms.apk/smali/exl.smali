.class public final Lexl;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lbgm;

.field public static final b:Lbgm;

.field public static final c:Lbgm;

.field public static final d:Lbgm;

.field public static final e:Lbgm;

.field public static final f:Lbgm;

.field public static final g:Lbgm;

.field private static final h:Lbgh;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lbgh;

    const-string v1, "mdm"

    invoke-direct {v0, v1}, Lbgh;-><init>(Ljava/lang/String;)V

    sput-object v0, Lexl;->h:Lbgh;

    const-string v1, "sitrepGmsCoreVersion"

    invoke-virtual {v0, v1}, Lbgh;->b(Ljava/lang/String;)Lbgm;

    move-result-object v0

    sput-object v0, Lexl;->a:Lbgm;

    sget-object v0, Lexl;->h:Lbgh;

    const-string v1, "sitrepGcmRegistrationId"

    invoke-virtual {v0, v1, v2}, Lbgh;->a(Ljava/lang/String;Ljava/lang/String;)Lbgm;

    move-result-object v0

    sput-object v0, Lexl;->b:Lbgm;

    sget-object v0, Lexl;->h:Lbgh;

    const-string v1, "sitrepIsDeviceAdmin"

    invoke-virtual {v0, v1, v2}, Lbgh;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lbgm;

    move-result-object v0

    sput-object v0, Lexl;->c:Lbgm;

    sget-object v1, Lexl;->h:Lbgh;

    const-string v2, "locationEnabled"

    sget-object v0, Lexh;->d:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v0}, Lbgh;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lbgm;

    move-result-object v0

    sput-object v0, Lexl;->d:Lbgm;

    sget-object v0, Lexl;->h:Lbgh;

    const-string v1, "lockMessage"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lbgh;->a(Ljava/lang/String;Ljava/lang/String;)Lbgm;

    move-result-object v0

    sput-object v0, Lexl;->e:Lbgm;

    sget-object v0, Lexl;->h:Lbgh;

    const-string v1, "lastSitrepReason"

    invoke-virtual {v0, v1}, Lbgh;->b(Ljava/lang/String;)Lbgm;

    move-result-object v0

    sput-object v0, Lexl;->f:Lbgm;

    sget-object v0, Lexl;->h:Lbgh;

    const-string v1, "sitrepRetryEpochTimeMs"

    invoke-virtual {v0, v1}, Lbgh;->a(Ljava/lang/String;)Lbgm;

    move-result-object v0

    sput-object v0, Lexl;->g:Lbgm;

    return-void
.end method
