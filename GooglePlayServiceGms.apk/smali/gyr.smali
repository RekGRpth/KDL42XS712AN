.class public final Lgyr;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lgyu;

.field public static final b:Lgyu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgys;

    invoke-direct {v0}, Lgys;-><init>()V

    sput-object v0, Lgyr;->a:Lgyu;

    new-instance v0, Lgyt;

    invoke-direct {v0}, Lgyt;-><init>()V

    sput-object v0, Lgyr;->b:Lgyu;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->e()I

    move-result v0

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_1

    invoke-interface {p2, v0}, Lgyu;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTheme(I)V

    :cond_1
    return-void
.end method
