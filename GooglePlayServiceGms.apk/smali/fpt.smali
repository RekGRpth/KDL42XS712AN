.class public final Lfpt;
.super Lfqq;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lbfw;
.implements Lfrc;


# instance fields
.field private ac:I

.field private ad:Landroid/widget/LinearLayout;

.field private ae:Lcom/google/android/gms/common/audience/widgets/AudienceView;

.field private af:Landroid/widget/CompoundButton;

.field private ag:Landroid/view/View;

.field private ah:Lfaj;

.field private ai:Ljava/lang/String;

.field private aj:I

.field private ak:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfqq;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIIILjava/util/List;)Lfpt;
    .locals 18

    new-instance v17, Lfpt;

    invoke-direct/range {v17 .. v17}, Lfpt;-><init>()V

    const/4 v7, 0x0

    const/4 v11, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v14, p12

    move/from16 v15, p13

    move-object/from16 v16, p14

    invoke-static/range {v1 .. v16}, Lfqq;->a(Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "domainRestricted"

    move/from16 v0, p9

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lfpt;->g(Landroid/os/Bundle;)V

    return-object v17
.end method

.method private a(Ljava/lang/String;I)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lfpt;->ag:Landroid/view/View;

    const v1, 0x7f0a0256    # com.google.android.gms.R.id.plus_domain_title

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lfpt;->ag:Landroid/view/View;

    const v2, 0x7f0a0257    # com.google.android.gms.R.id.plus_domain_body

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lfpt;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03ad    # com.google.android.gms.R.string.plus_domain_restricted_choice_title

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lfpt;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b03ae    # com.google.android.gms.R.string.plus_domain_restricted_choice_body

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfpt;->ag:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Lfqq;->E_()V

    iget-object v0, p0, Lfpt;->ah:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    invoke-virtual {p0}, Lfpt;->P()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->a(Lfrc;)V

    return-void
.end method

.method public final J()Landroid/view/View;
    .locals 9

    const/4 v8, 0x0

    const v7, 0x7f0a026c    # com.google.android.gms.R.id.search_icon

    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/common/audience/widgets/AudienceView;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v4}, Lcom/google/android/gms/common/audience/widgets/AudienceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfpt;->ae:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    iget-object v0, p0, Lfpt;->ae:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    new-instance v4, Lbfs;

    iget-object v5, p0, Lfpt;->ah:Lfaj;

    invoke-direct {v4, v5}, Lbfs;-><init>(Lfaj;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lbfp;)V

    iget-object v0, p0, Lfpt;->ae:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lbfw;)V

    iget-object v0, p0, Lfpt;->ae:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Z)V

    iget-object v0, p0, Lfpt;->ae:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {p0}, Lfpt;->P()Lfrb;

    move-result-object v4

    iget-object v4, v4, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    const v0, 0x7f0400c8    # com.google.android.gms.R.layout.plus_audience_selection_pacl_header

    invoke-virtual {v3, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lfpt;->ad:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lfpt;->ad:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lfpt;->ae:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lfpt;->ad:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfpt;->ad:Landroid/widget/LinearLayout;

    const v4, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Lfqq;->i:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget v0, p0, Lfpt;->ac:I

    if-eqz v0, :cond_0

    iget v0, p0, Lfpt;->ac:I

    if-ne v0, v1, :cond_3

    :goto_1
    const v0, 0x7f0400bf    # com.google.android.gms.R.layout.plus_audience_selection_acl_domain_restricted_header

    invoke-virtual {v3, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfpt;->ag:Landroid/view/View;

    iget-object v0, p0, Lfpt;->ag:Landroid/view/View;

    const v2, 0x7f0a0258    # com.google.android.gms.R.id.plus_switch

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lfpt;->af:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lfpt;->ai:Ljava/lang/String;

    iget v2, p0, Lfpt;->aj:I

    invoke-direct {p0, v0, v2}, Lfpt;->a(Ljava/lang/String;I)V

    iget-object v0, p0, Lfpt;->af:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lfpt;->af:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lfpt;->ad:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lfpt;->ag:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_0
    iget-boolean v0, p0, Lfpt;->ak:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfpt;->ad:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lfpt;->ad:Landroid/widget/LinearLayout;

    return-object v0

    :cond_2
    iget-object v4, p0, Lfqq;->i:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final K()I
    .locals 1

    iget v0, p0, Lfpt;->ac:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 2

    invoke-virtual {p0}, Lfpt;->P()Lfrb;

    move-result-object v0

    invoke-virtual {p0}, Lfpt;->P()Lfrb;

    move-result-object v1

    iget-object v1, v1, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, p1}, Lblc;->b(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lfec;)V
    .locals 2

    invoke-interface {p1}, Lfec;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfpt;->ai:Ljava/lang/String;

    iget-object v0, p0, Lfpt;->ai:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfpt;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03af    # com.google.android.gms.R.string.plus_domain_default

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfpt;->ai:Ljava/lang/String;

    :cond_0
    iget v0, p0, Lfpt;->ac:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfpt;->ai:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lfpt;->a(Ljava/lang/String;I)V

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lfpt;->ae:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {p0}, Lfpt;->P()Lfrb;

    move-result-object v1

    iget-object v1, v1, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lfpt;->ak:Z

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x4

    invoke-super {p0, p1}, Lfqq;->a_(Landroid/os/Bundle;)V

    new-instance v0, Lfaj;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const-class v2, Lbbr;

    invoke-static {v2}, Lbpw;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbbr;

    const-class v3, Lbbs;

    invoke-static {v3}, Lbpw;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbbs;

    iget-object v4, p0, Lfqy;->Z:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/plus/service/PlusService;->a(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lfqy;->aa:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Lfpt;->ah:Lfaj;

    if-eqz p1, :cond_1

    const-string v0, "domainRestricted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfpt;->ac:I

    const-string v0, "domain"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfpt;->ai:Ljava/lang/String;

    const-string v0, "domainRestrictedVisibility"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lfpt;->aj:I

    const-string v0, "hideSearchIcon"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfpt;->ak:Z

    :goto_0
    if-nez p1, :cond_0

    iget v0, p0, Lfpt;->ac:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfqy;->Y:Ljava/lang/String;

    sget-object v2, Lbcz;->B:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfqy;->aa:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "domainRestricted"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfpt;->ac:I

    const-string v0, ""

    iput-object v0, p0, Lfpt;->ai:Ljava/lang/String;

    iput v6, p0, Lfpt;->aj:I

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lfqq;->e(Landroid/os/Bundle;)V

    const-string v0, "domainRestricted"

    iget v1, p0, Lfpt;->ac:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "domain"

    iget-object v1, p0, Lfpt;->ai:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfpt;->ag:Landroid/view/View;

    if-eqz v0, :cond_0

    const-string v0, "domainRestrictedVisibility"

    iget-object v1, p0, Lfpt;->ag:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "hideSearchIcon"

    iget-boolean v1, p0, Lfpt;->ak:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final g_()V
    .locals 1

    invoke-virtual {p0}, Lfpt;->P()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->b(Lfrc;)V

    iget-object v0, p0, Lfpt;->ah:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    invoke-super {p0}, Lfqq;->g_()V

    return-void
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 7

    const/4 v2, 0x2

    const/4 v1, 0x1

    iget-object v0, p0, Lfpt;->af:Landroid/widget/CompoundButton;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lfpt;->ac:I

    if-eqz p2, :cond_2

    sget-object v0, Lbcz;->C:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v4, p0, Lfqy;->Y:Ljava/lang/String;

    sget-object v5, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfqy;->aa:Ljava/lang/String;

    invoke-static {v3, v4, v0, v5, v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    invoke-virtual {p0}, Lfpt;->P()Lfrb;

    move-result-object v0

    iget-object v0, v0, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    new-instance v3, Lbkw;

    invoke-direct {v3, v0}, Lbkw;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    if-eqz p2, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Lbkw;->a(I)Lbkw;

    move-result-object v0

    invoke-virtual {p0}, Lfpt;->P()Lfrb;

    move-result-object v1

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    sget-object v0, Lbcz;->D:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a026c    # com.google.android.gms.R.id.search_icon

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lfpt;->N()Lfqr;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->onSearchRequested()Z

    :cond_0
    return-void
.end method
