.class public final Lhfx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Landroid/content/Intent;

.field private static final b:Ljava/text/DecimalFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    sput-object v0, Lhfx;->b:Ljava/text/DecimalFormat;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.ENABLE_WALLET_OPTIMIZATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhfx;->a:Landroid/content/Intent;

    return-void
.end method

.method public static a(Lipy;)I
    .locals 7

    const/16 v0, 0x194

    const/16 v1, 0x8

    iget-object v2, p0, Lipy;->c:Lipx;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lipy;->c:Lipx;

    iget-object v3, v2, Lipx;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "OwServiceUtils"

    iget-object v4, v2, Lipx;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, v2, Lipx;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "OwServiceUtils"

    iget-object v2, v2, Lipx;->b:Ljava/lang/String;

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lipy;->d:Ljak;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lipy;->d:Ljak;

    const-string v3, "OwServiceUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Wallet error code: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v2, Ljak;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, v2, Ljak;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "OwServiceUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Wallet error detail: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Ljak;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v3, v2, Ljak;->c:Ljal;

    if-eqz v3, :cond_3

    iget-object v3, v2, Ljak;->c:Ljal;

    iget-object v4, v3, Ljal;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "OwServiceUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Wallet error user message title: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Ljal;->a:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget v2, v2, Ljak;->a:I

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    const/16 v0, 0x199

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x198

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x19c

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x195

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x192

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x196

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x1f -> :sswitch_0
        0x20 -> :sswitch_6
        0x29 -> :sswitch_0
        0x2a -> :sswitch_0
        0x2b -> :sswitch_3
        0x33 -> :sswitch_4
        0x3d -> :sswitch_5
    .end sparse-switch
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_GET_MASKED_WALLET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    const-class v2, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_GET_MASKED_WALLET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "com.google.android.gms.wallet.EXTRA_REQUEST"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.google.android.gms.wallet.EXTRA_SELECTED_ADDRESS_ID"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string v1, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lioj;Ljau;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_AUTHENTICATE_INSTRUMENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    const-class v2, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lizs;)V

    const-string v1, "com.google.android.gms.wallet.FULL_WALLET_REQUEST"

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lizs;)V

    const-string v1, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljbg;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;
    .locals 8

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->a()Lgrn;

    move-result-object v4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move-object v2, p1

    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v4, p2}, Lgrn;->a(Ljava/lang/String;)Lgrn;

    :cond_0
    :goto_1
    iget-object v0, p3, Ljbg;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p3, Ljbg;->e:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lgrn;->b(Ljava/lang/String;)Lgrn;

    :cond_1
    iget-boolean v0, p3, Ljbg;->j:Z

    invoke-virtual {v4, v0}, Lgrn;->e(Z)Lgrn;

    iget-object v0, p3, Ljbg;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p3, Ljbg;->f:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lgrn;->c(Ljava/lang/String;)Lgrn;

    :cond_2
    iget-object v0, p3, Ljbg;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p3, Ljbg;->d:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lgrn;->d(Ljava/lang/String;)Lgrn;

    :cond_3
    iget-object v0, p3, Ljbg;->n:[Ljbj;

    array-length v5, v0

    if-lez v5, :cond_6

    move v0, v3

    :goto_2
    if-ge v0, v5, :cond_6

    iget-object v6, p3, Ljbg;->n:[Ljbj;

    aget-object v6, v6, v0

    iget-object v6, v6, Ljbj;->a:Ljava/lang/String;

    new-instance v7, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-direct {v7, v6}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Lgrn;->a(Lcom/google/android/gms/identity/intents/model/CountrySpecification;)Lgrn;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v2, p3, Ljbg;->b:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-object v0, p3, Ljbg;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p3, Ljbg;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lgrn;->a(Ljava/lang/String;)Lgrn;

    goto :goto_1

    :cond_6
    iget-boolean v0, p3, Ljbg;->i:Z

    invoke-virtual {v4, v0}, Lgrn;->a(Z)Lgrn;

    iget-boolean v0, p3, Ljbg;->g:Z

    invoke-virtual {v4, v0}, Lgrn;->b(Z)Lgrn;

    iget-boolean v0, p3, Ljbg;->h:Z

    invoke-virtual {v4, v0}, Lgrn;->c(Z)Lgrn;

    iget-boolean v0, p3, Ljbg;->k:Z

    invoke-virtual {v4, v0}, Lgrn;->d(Z)Lgrn;

    iget-boolean v0, p3, Ljbg;->l:Z

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Lgrn;->f(Z)Lgrn;

    iget-boolean v0, p3, Ljbg;->m:Z

    if-nez v0, :cond_8

    :goto_4
    invoke-virtual {v4, v1}, Lgrn;->g(Z)Lgrn;

    iget-object v1, v4, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-object v0, p0

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lhfx;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v3

    goto :goto_3

    :cond_8
    move v1, v3

    goto :goto_4
.end method

.method public static a(Landroid/app/PendingIntent;)Landroid/os/Bundle;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 12

    const/4 v1, 0x1

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-static {p2}, Lhfx;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v4

    new-instance v5, Ljas;

    invoke-direct {v5}, Ljas;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v6

    new-instance v7, Liqi;

    invoke-direct {v7}, Liqi;-><init>()V

    new-instance v8, Liqz;

    invoke-direct {v8}, Liqz;-><init>()V

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Liqz;->a:Ljava/lang/String;

    :cond_0
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Liqz;->b:Ljava/lang/String;

    :cond_1
    const-wide/16 v9, 0x1

    iput-wide v9, v8, Liqz;->c:J

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->m()I

    move-result v0

    iput v0, v8, Liqz;->d:I

    const/4 v0, 0x0

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Liqa;

    invoke-direct {v2}, Liqa;-><init>()V

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1b

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->i()Ljava/lang/String;

    move-result-object v0

    const-string v9, "aztec"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    const/4 v0, 0x2

    :goto_0
    iput v0, v2, Liqa;->a:I

    :goto_1
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Liqa;->b:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Liqa;->c:Ljava/lang/String;

    :cond_2
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Liqa;->d:Ljava/lang/String;

    :cond_3
    move-object v0, v2

    :cond_4
    iput-object v0, v8, Liqz;->e:Liqa;

    invoke-static {v6}, Lhfy;->f(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)[Lirb;

    move-result-object v0

    iput-object v0, v8, Liqz;->f:[Lirb;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->o()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->o()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v0

    invoke-static {v0}, Lhfy;->a(Lcom/google/android/gms/wallet/wobs/TimeInterval;)Liqv;

    move-result-object v0

    iput-object v0, v8, Liqz;->g:Liqv;

    :cond_5
    invoke-static {v6}, Lhfy;->e(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)[Liqe;

    move-result-object v0

    iput-object v0, v8, Liqz;->h:[Liqe;

    invoke-static {v6}, Lhfy;->d(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)Liqp;

    move-result-object v0

    iput-object v0, v8, Liqz;->j:Liqp;

    invoke-static {v6}, Lhfy;->c(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)[Liqo;

    move-result-object v0

    iput-object v0, v8, Liqz;->k:[Liqo;

    invoke-static {v6}, Lhfy;->b(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)[Liqt;

    move-result-object v0

    iput-object v0, v8, Liqz;->l:[Liqt;

    invoke-static {v6}, Lhfy;->a(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)Liqs;

    move-result-object v0

    iput-object v0, v8, Liqz;->m:Liqs;

    iput-object v8, v7, Liqi;->a:Liqz;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Liqi;->b:Ljava/lang/String;

    :cond_6
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Liqi;->c:Ljava/lang/String;

    :cond_7
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->x()Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->x()Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;

    move-result-object v0

    new-instance v2, Liqj;

    invoke-direct {v2}, Liqj;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->b()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Liqj;->a:Ljava/lang/String;

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->c()Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->c()Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->g()I

    move-result v6

    const/4 v8, -0x1

    if-eq v6, v8, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->c()Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

    move-result-object v6

    new-instance v8, Liqk;

    invoke-direct {v8}, Liqk;-><init>()V

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->g()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :goto_2
    iput-object v8, v2, Liqj;->b:Liqk;

    :cond_9
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->d()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Liqj;->c:Ljava/lang/String;

    :cond_a
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->e()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v6

    if-eqz v6, :cond_b

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->e()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v0

    invoke-static {v0}, Lhfy;->a(Lcom/google/android/gms/wallet/wobs/TimeInterval;)Liqv;

    move-result-object v0

    iput-object v0, v2, Liqj;->d:Liqv;

    :cond_b
    iput-object v2, v7, Liqi;->d:Liqj;

    :cond_c
    new-array v0, v1, [Liqi;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    iput-object v0, v5, Ljas;->c:[Liqi;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->f()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.google.android.gms.wallet.onlinewallet.ACTION_CREATE_WALLET_OBJECTS"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "com.google.android.gms"

    const-class v7, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "com.google.android.gms.wallet.CREATE_WALLET_OBJECTS_REQUEST"

    invoke-static {v2, v6, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lizs;)V

    const-string v5, "com.google.android.gms.wallet.WOBS_ISSUER_NAME"

    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.WOBS_PROGRAM_NAME"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p0, v2, v0}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v3

    :cond_d
    const-string v9, "code39"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    const/4 v0, 0x3

    goto/16 :goto_0

    :cond_e
    const-string v9, "code128"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    const/4 v0, 0x5

    goto/16 :goto_0

    :cond_f
    const-string v9, "codabar"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    const/4 v0, 0x6

    goto/16 :goto_0

    :cond_10
    const-string v9, "dataMatrix"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    const/4 v0, 0x7

    goto/16 :goto_0

    :cond_11
    const-string v9, "ean8"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    const/16 v0, 0x8

    goto/16 :goto_0

    :cond_12
    const-string v9, "ean13"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    const/16 v0, 0x9

    goto/16 :goto_0

    :cond_13
    const-string v9, "itf14"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    const/16 v0, 0xa

    goto/16 :goto_0

    :cond_14
    const-string v9, "pdf417"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_15

    const/16 v0, 0xb

    goto/16 :goto_0

    :cond_15
    const-string v9, "qrCode"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_16

    const/16 v0, 0xe

    goto/16 :goto_0

    :cond_16
    const-string v9, "upcA"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_17

    const/16 v0, 0xf

    goto/16 :goto_0

    :cond_17
    const-string v9, "upcE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_18

    const/16 v0, 0x10

    goto/16 :goto_0

    :cond_18
    const-string v9, "pdf417Compact"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_19

    const/16 v0, 0x12

    goto/16 :goto_0

    :cond_19
    const-string v9, "textOnly"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/16 v0, 0x13

    goto/16 :goto_0

    :cond_1a
    move v0, v1

    goto/16 :goto_0

    :cond_1b
    iput v1, v2, Liqa;->a:I

    goto/16 :goto_1

    :pswitch_0
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->b()I

    move-result v6

    iput v6, v8, Liqk;->b:I

    goto/16 :goto_2

    :pswitch_1
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->c()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v8, Liqk;->a:Ljava/lang/String;

    goto/16 :goto_2

    :pswitch_2
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->d()D

    move-result-wide v9

    iput-wide v9, v8, Liqk;->c:D

    goto/16 :goto_2

    :pswitch_3
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->f()J

    move-result-wide v10

    new-instance v6, Liqm;

    invoke-direct {v6}, Liqm;-><init>()V

    iput-wide v10, v6, Liqm;->a:J

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1c

    iput-object v9, v6, Liqm;->b:Ljava/lang/String;

    :cond_1c
    iput-object v6, v8, Liqk;->d:Liqm;

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/os/Bundle;
    .locals 7

    const/4 v2, 0x0

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lhfx;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v6
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lhgb;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/os/Bundle;
    .locals 8

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iget-object v3, p3, Lhgb;->e:Ljbg;

    iget-object v4, p3, Lhgb;->b:Ljava/lang/String;

    iget-object v5, p3, Lhgb;->c:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lhfx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljbg;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v7
.end method

.method public static a(Landroid/content/Context;Ljbg;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/os/Bundle;
    .locals 8

    const/4 v1, 0x0

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, v1

    move-object v5, v1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lhfx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljbg;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v7
.end method

.method public static a(Lipv;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/gms/identity/intents/model/UserAddress;->a()Lepr;

    move-result-object v0

    iget-object v1, p0, Lipv;->d:Ljava/lang/String;

    invoke-static {v1}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepr;->l(Ljava/lang/String;)Lepr;

    iget-boolean v1, p0, Lipv;->e:Z

    invoke-virtual {v0, v1}, Lepr;->a(Z)Lepr;

    invoke-virtual {v0, p1}, Lepr;->n(Ljava/lang/String;)Lepr;

    iget-object v1, p0, Lipv;->a:Lixo;

    if-eqz v1, :cond_1

    iget-object v2, v1, Lixo;->s:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepr;->a(Ljava/lang/String;)Lepr;

    iget-object v2, v1, Lixo;->q:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    iget-object v3, v1, Lixo;->q:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lepr;->b(Ljava/lang/String;)Lepr;

    :goto_1
    if-lt v2, v5, :cond_3

    iget-object v3, v1, Lixo;->q:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lepr;->c(Ljava/lang/String;)Lepr;

    :goto_2
    if-lt v2, v6, :cond_4

    iget-object v3, v1, Lixo;->q:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v0, v3}, Lepr;->d(Ljava/lang/String;)Lepr;

    :goto_3
    if-lt v2, v7, :cond_5

    iget-object v3, v1, Lixo;->q:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-virtual {v0, v3}, Lepr;->e(Ljava/lang/String;)Lepr;

    :goto_4
    const/4 v3, 0x5

    if-lt v2, v3, :cond_6

    iget-object v2, v1, Lixo;->q:[Ljava/lang/String;

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Lepr;->f(Ljava/lang/String;)Lepr;

    :goto_5
    iget-object v2, v1, Lixo;->a:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepr;->i(Ljava/lang/String;)Lepr;

    iget-object v2, v1, Lixo;->f:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepr;->h(Ljava/lang/String;)Lepr;

    iget-object v2, v1, Lixo;->d:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepr;->g(Ljava/lang/String;)Lepr;

    iget-object v2, v1, Lixo;->k:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepr;->j(Ljava/lang/String;)Lepr;

    iget-object v2, v1, Lixo;->m:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepr;->k(Ljava/lang/String;)Lepr;

    iget-object v1, v1, Lixo;->r:Ljava/lang/String;

    invoke-static {v1}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepr;->m(Ljava/lang/String;)Lepr;

    :cond_1
    iget-object v0, v0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    goto/16 :goto_0

    :cond_2
    const-string v3, ""

    invoke-virtual {v0, v3}, Lepr;->b(Ljava/lang/String;)Lepr;

    goto :goto_1

    :cond_3
    const-string v3, ""

    invoke-virtual {v0, v3}, Lepr;->c(Ljava/lang/String;)Lepr;

    goto :goto_2

    :cond_4
    const-string v3, ""

    invoke-virtual {v0, v3}, Lepr;->d(Ljava/lang/String;)Lepr;

    goto :goto_3

    :cond_5
    const-string v3, ""

    invoke-virtual {v0, v3}, Lepr;->e(Ljava/lang/String;)Lepr;

    goto :goto_4

    :cond_6
    const-string v2, ""

    invoke-virtual {v0, v2}, Lepr;->f(Ljava/lang/String;)Lepr;

    goto :goto_5
.end method

.method public static a(Lipv;)Lcom/google/android/gms/wallet/Address;
    .locals 6

    const/4 v5, 0x2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/Address;->a()Lgqx;

    move-result-object v0

    iget-object v1, p0, Lipv;->d:Ljava/lang/String;

    invoke-static {v1}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgqx;->i(Ljava/lang/String;)Lgqx;

    iget-boolean v1, p0, Lipv;->e:Z

    invoke-virtual {v0, v1}, Lgqx;->a(Z)Lgqx;

    iget-object v1, p0, Lipv;->a:Lixo;

    if-eqz v1, :cond_1

    iget-object v2, v1, Lixo;->s:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgqx;->a(Ljava/lang/String;)Lgqx;

    iget-object v2, v1, Lixo;->q:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    iget-object v3, v1, Lixo;->q:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lgqx;->b(Ljava/lang/String;)Lgqx;

    :goto_1
    if-lt v2, v5, :cond_3

    iget-object v3, v1, Lixo;->q:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lgqx;->c(Ljava/lang/String;)Lgqx;

    :goto_2
    const/4 v3, 0x3

    if-lt v2, v3, :cond_4

    iget-object v2, v1, Lixo;->q:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-virtual {v0, v2}, Lgqx;->d(Ljava/lang/String;)Lgqx;

    :goto_3
    iget-object v2, v1, Lixo;->a:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgqx;->e(Ljava/lang/String;)Lgqx;

    iget-object v2, v1, Lixo;->f:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgqx;->f(Ljava/lang/String;)Lgqx;

    iget-object v2, v1, Lixo;->d:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgqx;->g(Ljava/lang/String;)Lgqx;

    iget-object v2, v1, Lixo;->k:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgqx;->h(Ljava/lang/String;)Lgqx;

    iget-object v1, v1, Lixo;->r:Ljava/lang/String;

    invoke-static {v1}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgqx;->j(Ljava/lang/String;)Lgqx;

    :cond_1
    iget-object v0, v0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    goto :goto_0

    :cond_2
    const-string v3, ""

    invoke-virtual {v0, v3}, Lgqx;->b(Ljava/lang/String;)Lgqx;

    goto :goto_1

    :cond_3
    const-string v3, ""

    invoke-virtual {v0, v3}, Lgqx;->c(Ljava/lang/String;)Lgqx;

    goto :goto_2

    :cond_4
    const-string v2, ""

    invoke-virtual {v0, v2}, Lgqx;->d(Ljava/lang/String;)Lgqx;

    goto :goto_3
.end method

.method public static a(Ljav;Ljau;Ljava/lang/String;J)Lcom/google/android/gms/wallet/FullWallet;
    .locals 11

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p1, Ljau;->f:Ljava/lang/String;

    iget-object v0, p1, Ljau;->g:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Ljav;->e:Ljava/lang/String;

    iget-object v3, p0, Ljav;->f:Ljava/lang/String;

    iget v4, p0, Ljav;->c:I

    iget v5, p0, Ljav;->d:I

    invoke-static {v3}, Lbpx;->a(Ljava/lang/String;)J

    move-result-wide v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v8, "%013d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    xor-long/2addr v6, p3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v9, v10

    invoke-static {v3, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v6, 0x10

    invoke-virtual {v2, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v6, 0x10

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    new-instance v6, Lcom/google/android/gms/wallet/ProxyCard;

    invoke-direct {v6, v3, v2, v4, v5}, Lcom/google/android/gms/wallet/ProxyCard;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lgrc;

    move-result-object v2

    invoke-virtual {v2, v6}, Lgrc;->a(Lcom/google/android/gms/wallet/ProxyCard;)Lgrc;

    move-result-object v2

    invoke-static {v1}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lgrc;->a(Ljava/lang/String;)Lgrc;

    move-result-object v1

    invoke-static {v0}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgrc;->b(Ljava/lang/String;)Lgrc;

    move-result-object v0

    iget-object v1, p0, Ljav;->g:Lipv;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljav;->g:Lipv;

    invoke-static {v1}, Lhfx;->a(Lipv;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgrc;->a(Lcom/google/android/gms/wallet/Address;)Lgrc;

    iget-object v1, p0, Ljav;->g:Lipv;

    invoke-static {v1, p2}, Lhfx;->a(Lipv;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v1

    iget-object v2, v0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object v1, v2, Lcom/google/android/gms/wallet/FullWallet;->h:Lcom/google/android/gms/identity/intents/model/UserAddress;

    :cond_1
    iget-object v1, p0, Ljav;->h:Lipv;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljav;->h:Lipv;

    invoke-static {v1}, Lhfx;->a(Lipv;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgrc;->b(Lcom/google/android/gms/wallet/Address;)Lgrc;

    iget-object v1, p0, Ljav;->h:Lipv;

    invoke-static {v1, p2}, Lhfx;->a(Lipv;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v1

    iget-object v2, v0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object v1, v2, Lcom/google/android/gms/wallet/FullWallet;->i:Lcom/google/android/gms/identity/intents/model/UserAddress;

    :cond_2
    iget-object v1, p0, Ljav;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgrc;->a([Ljava/lang/String;)Lgrc;

    invoke-virtual {v0, p2}, Lgrc;->c(Ljava/lang/String;)Lgrc;

    iget-object v1, p0, Ljav;->b:[Ljah;

    array-length v1, v1

    if-lez v1, :cond_3

    iget-object v1, p0, Ljav;->b:[Ljah;

    invoke-static {v1}, Lhfx;->a([Ljah;)[Lcom/google/android/gms/wallet/InstrumentInfo;

    move-result-object v1

    iget-object v2, v0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object v1, v2, Lcom/google/android/gms/wallet/FullWallet;->j:[Lcom/google/android/gms/wallet/InstrumentInfo;

    :cond_3
    iget-object v0, v0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    return-object v0
.end method

.method public static a(Ljbh;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;
    .locals 10

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lgrl;

    move-result-object v1

    iget-object v2, p0, Ljbh;->a:Ljava/lang/String;

    invoke-static {v2}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgrl;->a(Ljava/lang/String;)Lgrl;

    move-result-object v1

    invoke-static {p1}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgrl;->b(Ljava/lang/String;)Lgrl;

    move-result-object v2

    iget-object v1, p0, Ljbh;->e:Ljbi;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljbh;->e:Ljbi;

    iget-object v1, v1, Ljbi;->h:Ljab;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljbh;->e:Ljbi;

    iget-object v1, v1, Ljbi;->h:Ljab;

    invoke-static {}, Lcom/google/android/gms/wallet/Address;->a()Lgqx;

    move-result-object v3

    iget-object v4, v1, Ljab;->j:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->i(Ljava/lang/String;)Lgqx;

    iget-boolean v4, v1, Ljab;->k:Z

    invoke-virtual {v3, v4}, Lgqx;->a(Z)Lgqx;

    iget-object v4, v1, Ljab;->b:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->a(Ljava/lang/String;)Lgqx;

    iget-object v4, v1, Ljab;->c:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->b(Ljava/lang/String;)Lgqx;

    iget-object v4, v1, Ljab;->d:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->c(Ljava/lang/String;)Lgqx;

    iget-object v4, v1, Ljab;->e:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->d(Ljava/lang/String;)Lgqx;

    iget-object v4, v1, Ljab;->f:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->e(Ljava/lang/String;)Lgqx;

    iget-object v4, v1, Ljab;->g:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->f(Ljava/lang/String;)Lgqx;

    iget-object v4, v1, Ljab;->h:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->g(Ljava/lang/String;)Lgqx;

    iget-object v4, v1, Ljab;->i:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgqx;->h(Ljava/lang/String;)Lgqx;

    iget-object v1, v1, Ljab;->l:Ljava/lang/String;

    invoke-static {v1}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lgqx;->j(Ljava/lang/String;)Lgqx;

    iget-object v1, v3, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iget-object v3, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->e:Lcom/google/android/gms/wallet/Address;

    iget-object v1, p0, Ljbh;->e:Ljbi;

    iget-object v1, v1, Ljbi;->h:Ljab;

    invoke-static {}, Lcom/google/android/gms/identity/intents/model/UserAddress;->a()Lepr;

    move-result-object v3

    iget-object v4, v1, Ljab;->j:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->l(Ljava/lang/String;)Lepr;

    iget-boolean v4, v1, Ljab;->k:Z

    invoke-virtual {v3, v4}, Lepr;->a(Z)Lepr;

    iget-object v4, v1, Ljab;->b:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->a(Ljava/lang/String;)Lepr;

    iget-object v4, v1, Ljab;->c:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->b(Ljava/lang/String;)Lepr;

    iget-object v4, v1, Ljab;->d:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->c(Ljava/lang/String;)Lepr;

    iget-object v4, v1, Ljab;->e:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->d(Ljava/lang/String;)Lepr;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lepr;->e(Ljava/lang/String;)Lepr;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lepr;->f(Ljava/lang/String;)Lepr;

    iget-object v4, v1, Ljab;->f:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->i(Ljava/lang/String;)Lepr;

    iget-object v4, v1, Ljab;->g:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->h(Ljava/lang/String;)Lepr;

    iget-object v4, v1, Ljab;->h:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->g(Ljava/lang/String;)Lepr;

    iget-object v4, v1, Ljab;->i:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepr;->j(Ljava/lang/String;)Lepr;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lepr;->k(Ljava/lang/String;)Lepr;

    iget-object v1, v1, Ljab;->l:Ljava/lang/String;

    invoke-static {v1}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lepr;->m(Ljava/lang/String;)Lepr;

    invoke-virtual {v3, p2}, Lepr;->n(Ljava/lang/String;)Lepr;

    iget-object v1, v3, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iget-object v3, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->i:Lcom/google/android/gms/identity/intents/model/UserAddress;

    :cond_0
    iget-object v1, p0, Ljbh;->d:Lipv;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljbh;->d:Lipv;

    invoke-static {v1}, Lhfx;->a(Lipv;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    iget-object v3, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->f:Lcom/google/android/gms/wallet/Address;

    iget-object v1, p0, Ljbh;->d:Lipv;

    invoke-static {v1, p2}, Lhfx;->a(Lipv;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v1

    iget-object v3, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->j:Lcom/google/android/gms/identity/intents/model/UserAddress;

    :cond_1
    iget-object v1, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object p2, v1, Lcom/google/android/gms/wallet/MaskedWallet;->d:Ljava/lang/String;

    iget-object v1, p0, Ljbh;->b:[Ljava/lang/String;

    iget-object v3, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->c:[Ljava/lang/String;

    iget-object v1, p0, Ljbh;->c:[Ljah;

    array-length v1, v1

    if-lez v1, :cond_2

    iget-object v1, p0, Ljbh;->c:[Ljah;

    invoke-static {v1}, Lhfx;->a([Ljah;)[Lcom/google/android/gms/wallet/InstrumentInfo;

    move-result-object v1

    iget-object v3, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->k:[Lcom/google/android/gms/wallet/InstrumentInfo;

    :cond_2
    iget-object v1, p0, Ljbh;->f:[Ljai;

    array-length v1, v1

    if-lez v1, :cond_4

    iget-object v3, p0, Ljbh;->f:[Ljai;

    array-length v1, v3

    new-array v4, v1, [Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    array-length v5, v3

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_3

    aget-object v6, v3, v1

    invoke-static {}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->a()Lgrj;

    move-result-object v7

    iget-object v8, v6, Ljai;->a:Ljava/lang/String;

    iget-object v9, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->a:Ljava/lang/String;

    iget-object v8, v6, Ljai;->b:Ljava/lang/String;

    iget-object v9, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->b:Ljava/lang/String;

    iget-object v8, v6, Ljai;->c:Ljava/lang/String;

    iget-object v9, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->c:Ljava/lang/String;

    iget-object v8, v6, Ljai;->d:Ljava/lang/String;

    iget-object v9, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->d:Ljava/lang/String;

    iget-object v8, v6, Ljai;->e:Ljava/lang/String;

    iget-object v9, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->e:Ljava/lang/String;

    iget-object v8, v6, Ljai;->f:Ljava/lang/String;

    iget-object v9, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->f:Ljava/lang/String;

    iget-object v8, v6, Ljai;->g:Ljava/lang/String;

    iget-object v9, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->g:Ljava/lang/String;

    iget-object v6, v6, Ljai;->h:Ljava/lang/String;

    iget-object v8, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v6, v8, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->h:Ljava/lang/String;

    iget-object v6, v7, Lgrj;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    aput-object v6, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v4, v1, Lcom/google/android/gms/wallet/MaskedWallet;->g:[Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    :cond_4
    iget-object v1, p0, Ljbh;->g:[Ljaj;

    array-length v1, v1

    if-lez v1, :cond_6

    iget-object v1, p0, Ljbh;->g:[Ljaj;

    array-length v3, v1

    new-array v3, v3, [Lcom/google/android/gms/wallet/OfferWalletObject;

    array-length v4, v1

    :goto_1
    if-ge v0, v4, :cond_5

    aget-object v5, v1, v0

    invoke-static {}, Lcom/google/android/gms/wallet/OfferWalletObject;->a()Lgrq;

    move-result-object v6

    iget-object v7, v5, Ljaj;->a:Ljava/lang/String;

    iget-object v8, v6, Lgrq;->a:Lcom/google/android/gms/wallet/OfferWalletObject;

    iput-object v7, v8, Lcom/google/android/gms/wallet/OfferWalletObject;->a:Ljava/lang/String;

    iget-object v5, v5, Ljaj;->b:Ljava/lang/String;

    iget-object v7, v6, Lgrq;->a:Lcom/google/android/gms/wallet/OfferWalletObject;

    iput-object v5, v7, Lcom/google/android/gms/wallet/OfferWalletObject;->b:Ljava/lang/String;

    iget-object v5, v6, Lgrq;->a:Lcom/google/android/gms/wallet/OfferWalletObject;

    aput-object v5, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v0, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v3, v0, Lcom/google/android/gms/wallet/MaskedWallet;->h:[Lcom/google/android/gms/wallet/OfferWalletObject;

    :cond_6
    iget-object v0, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    const-string v1, "androidPackageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lgsn;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a()Lhgg;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhgg;->b(Ljava/lang/String;)Lhgg;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhgg;->d(Ljava/lang/String;)Lhgg;

    move-result-object v1

    const-string v2, "onlinewallet"

    invoke-virtual {v1, v2}, Lhgg;->c(Ljava/lang/String;)Lhgg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhgg;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lhgg;

    move-result-object v0

    invoke-virtual {v0}, Lhgg;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljbb;)Lizz;
    .locals 8

    const/4 v7, 0x1

    const/4 v0, 0x0

    new-instance v2, Lizz;

    invoke-direct {v2}, Lizz;-><init>()V

    iget-object v3, p0, Ljbb;->d:Ljava/lang/String;

    iget-object v1, p0, Ljbb;->c:[Ljbi;

    array-length v4, v1

    new-array v1, v4, [Lioj;

    iput-object v1, v2, Lizz;->b:[Lioj;

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v5, p0, Ljbb;->c:[Ljbi;

    aget-object v5, v5, v1

    invoke-static {v5}, Lgtk;->a(Ljbi;)Lioj;

    move-result-object v5

    iget-object v6, v5, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iput-boolean v7, v5, Lioj;->f:Z

    :cond_0
    iget-object v6, v2, Lizz;->b:[Lioj;

    aput-object v5, v6, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ljbb;->f:Ljava/lang/String;

    iget-object v3, p0, Ljbb;->e:[Lipv;

    array-length v3, v3

    new-array v4, v3, [Lipv;

    iput-object v4, v2, Lizz;->c:[Lipv;

    :goto_1
    if-ge v0, v3, :cond_3

    iget-object v4, p0, Ljbb;->e:[Lipv;

    aget-object v4, v4, v0

    iget-object v5, v4, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iput-boolean v7, v4, Lipv;->h:Z

    :cond_2
    iget-object v5, v2, Lizz;->c:[Lipv;

    aput-object v4, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iput-object p0, v2, Lizz;->a:Ljbb;

    return-object v2
.end method

.method public static a(Lcom/google/android/gms/wallet/Cart;)Ljaf;
    .locals 8

    new-instance v2, Ljaf;

    invoke-direct {v2}, Ljaf;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Ljaf;->a:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Ljaf;->b:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->d()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->d()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Ljag;

    iput-object v0, v2, Ljaf;->c:[Ljag;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_7

    iget-object v5, v2, Ljaf;->c:[Ljag;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/LineItem;

    new-instance v6, Ljag;

    invoke-direct {v6}, Ljag;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->b()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Ljag;->a:Ljava/lang/String;

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Ljag;->b:Ljava/lang/String;

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->d()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Ljag;->c:Ljava/lang/String;

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->e()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Ljag;->d:Ljava/lang/String;

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->g()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->g()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Ljag;->f:Ljava/lang/String;

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    aput-object v6, v5, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    iput v0, v6, Ljag;->e:I

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x2

    iput v0, v6, Ljag;->e:I

    goto :goto_1

    :cond_7
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/wallet/FullWalletRequest;Lhgb;)Ljau;
    .locals 4

    iget-object v0, p1, Lhgb;->e:Ljbg;

    new-instance v1, Ljau;

    invoke-direct {v1}, Ljau;-><init>()V

    iget-object v2, p1, Lhgb;->b:Ljava/lang/String;

    iput-object v2, v1, Ljau;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Ljau;->f:Ljava/lang/String;

    iget-boolean v2, v0, Ljbg;->i:Z

    iput-boolean v2, v1, Ljau;->h:Z

    iget-boolean v2, v0, Ljbg;->h:Z

    iput-boolean v2, v1, Ljau;->i:Z

    iget-boolean v2, v0, Ljbg;->k:Z

    iput-boolean v2, v1, Ljau;->t:Z

    iget-boolean v2, v0, Ljbg;->l:Z

    iput-boolean v2, v1, Ljau;->u:Z

    iget-boolean v2, v0, Ljbg;->m:Z

    iput-boolean v2, v1, Ljau;->v:Z

    iget-boolean v2, p1, Lhgb;->g:Z

    iput-boolean v2, v1, Ljau;->m:Z

    iget-boolean v2, p1, Lhgb;->i:Z

    iput-boolean v2, v1, Ljau;->w:Z

    iget-boolean v2, p1, Lhgb;->j:Z

    iput-boolean v2, v1, Ljau;->x:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->d()Lcom/google/android/gms/wallet/Cart;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->d()Lcom/google/android/gms/wallet/Cart;

    move-result-object v2

    invoke-static {v2}, Lhfx;->a(Lcom/google/android/gms/wallet/Cart;)Ljaf;

    move-result-object v2

    iput-object v2, v1, Ljau;->d:Ljaf;

    :cond_0
    iget-boolean v2, v0, Ljbg;->j:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljae;

    invoke-direct {v2}, Ljae;-><init>()V

    iput-object v2, v1, Ljau;->p:Ljae;

    iget-object v2, v1, Ljau;->p:Ljae;

    iget-object v3, v0, Ljbg;->f:Ljava/lang/String;

    iput-object v3, v2, Ljae;->a:Ljava/lang/String;

    :cond_1
    iget-object v2, v0, Ljbg;->n:[Ljbj;

    array-length v2, v2

    if-lez v2, :cond_2

    iget-object v2, v0, Ljbg;->n:[Ljbj;

    iput-object v2, v1, Ljau;->r:[Ljbj;

    :cond_2
    iget-object v2, p1, Lhgb;->h:[Ljava/lang/String;

    if-eqz v2, :cond_3

    array-length v3, v2

    if-lez v3, :cond_3

    iput-object v2, v1, Ljau;->l:[Ljava/lang/String;

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Ljau;->g:Ljava/lang/String;

    :cond_4
    iget-object v2, p1, Lhgb;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgb;->c:Ljava/lang/String;

    iput-object v2, v1, Ljau;->c:Ljava/lang/String;

    :cond_5
    iget-object v2, v0, Ljbg;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v0, v0, Ljbg;->d:Ljava/lang/String;

    iput-object v0, v1, Ljau;->s:Ljava/lang/String;

    :cond_6
    return-object v1
.end method

.method public static a(Ljbg;Ljava/lang/String;Ljava/lang/String;ZZLjaf;Ljava/lang/String;)Ljau;
    .locals 3

    new-instance v0, Ljau;

    invoke-direct {v0}, Ljau;-><init>()V

    iget-boolean v1, p0, Ljbg;->h:Z

    iput-boolean v1, v0, Ljau;->i:Z

    iget-boolean v1, p0, Ljbg;->i:Z

    iput-boolean v1, v0, Ljau;->h:Z

    iget-boolean v1, p0, Ljbg;->k:Z

    iput-boolean v1, v0, Ljau;->t:Z

    iget-boolean v1, p0, Ljbg;->l:Z

    iput-boolean v1, v0, Ljau;->u:Z

    iget-boolean v1, p0, Ljbg;->m:Z

    iput-boolean v1, v0, Ljau;->v:Z

    iput-boolean p3, v0, Ljau;->w:Z

    iput-boolean p4, v0, Ljau;->x:Z

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object p1, v0, Ljau;->b:Ljava/lang/String;

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object p2, v0, Ljau;->c:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Ljbg;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Ljbg;->b:Ljava/lang/String;

    iput-object v1, v0, Ljau;->f:Ljava/lang/String;

    :cond_2
    iget-object v1, p0, Ljbg;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Ljbg;->c:Ljava/lang/String;

    iput-object v1, v0, Ljau;->g:Ljava/lang/String;

    :cond_3
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iput-object p6, v0, Ljau;->q:Ljava/lang/String;

    :cond_4
    if-eqz p5, :cond_5

    iput-object p5, v0, Ljau;->d:Ljaf;

    :cond_5
    iget-boolean v1, p0, Ljbg;->j:Z

    if-eqz v1, :cond_6

    new-instance v1, Ljae;

    invoke-direct {v1}, Ljae;-><init>()V

    iput-object v1, v0, Ljau;->p:Ljae;

    iget-object v1, v0, Ljau;->p:Ljae;

    iget-object v2, p0, Ljbg;->f:Ljava/lang/String;

    iput-object v2, v1, Ljae;->a:Ljava/lang/String;

    :cond_6
    iget-object v1, p0, Ljbg;->n:[Ljbj;

    array-length v1, v1

    if-lez v1, :cond_7

    iget-object v1, p0, Ljbg;->n:[Ljbj;

    iput-object v1, v0, Ljau;->r:[Ljbj;

    :cond_7
    iget-object v1, p0, Ljbg;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Ljbg;->d:Ljava/lang/String;

    iput-object v1, v0, Ljau;->s:Ljava/lang/String;

    :cond_8
    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;ILandroid/accounts/Account;)Ljava/lang/String;
    .locals 3

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/16 v0, 0x15

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    const-string v0, "https://www.googleapis.com/auth/paymentssandbox.make_payments"

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "oauth2:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "callerUid"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "androidPackageName"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v2, v0, v1}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v0, "https://www.googleapis.com/auth/payments.make_payments"

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, ""

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p0, :cond_0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-virtual {v0}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static a()Ljbf;
    .locals 3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    new-instance v1, Ljbf;

    invoke-direct {v1}, Ljbf;-><init>()V

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Ljbf;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ljbf;->b:Ljava/lang/String;

    return-object v1
.end method

.method public static a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Ljbg;
    .locals 6

    const/4 v0, 0x0

    new-instance v2, Ljbg;

    invoke-direct {v2}, Ljbg;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v2, Ljbg;->j:Z

    if-eqz p0, :cond_1

    const-string v1, "USD"

    iput-object v1, v2, Ljbg;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e()Z

    move-result v1

    iput-boolean v1, v2, Ljbg;->i:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f()Z

    move-result v1

    iput-boolean v1, v2, Ljbg;->g:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g()Z

    move-result v1

    iput-boolean v1, v2, Ljbg;->h:Z

    iput-boolean v0, v2, Ljbg;->l:Z

    iput-boolean v0, v2, Ljbg;->m:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Ljbg;->o:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v1, v4, [Ljbj;

    iput-object v1, v2, Ljbg;->n:[Ljbj;

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    new-instance v5, Ljbj;

    invoke-direct {v5}, Ljbj;-><init>()V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-virtual {v0}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Ljbj;->a:Ljava/lang/String;

    iget-object v0, v2, Ljbg;->n:[Ljbj;

    aput-object v5, v0, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public static a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Ljan;)Ljbg;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v4, Ljbg;

    invoke-direct {v4}, Ljbg;-><init>()V

    if-eqz p0, :cond_6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, v4, Ljbg;->b:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Ljbg;->c:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Ljbg;->e:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Ljbg;->f:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Ljbg;->d:Ljava/lang/String;

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->p()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->p()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v0, v6, [Ljbj;

    iput-object v0, v4, Ljbg;->n:[Ljbj;

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_5

    new-instance v7, Ljbj;

    invoke-direct {v7}, Ljbj;-><init>()V

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-virtual {v0}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Ljbj;->a:Ljava/lang/String;

    iget-object v0, v4, Ljbg;->n:[Ljbj;

    aput-object v7, v0, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->d()Z

    move-result v0

    iput-boolean v0, v4, Ljbg;->i:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->e()Z

    move-result v0

    iput-boolean v0, v4, Ljbg;->g:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->f()Z

    move-result v0

    iput-boolean v0, v4, Ljbg;->h:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->k()Z

    move-result v0

    iput-boolean v0, v4, Ljbg;->k:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->l()Z

    move-result v0

    iput-boolean v0, v4, Ljbg;->j:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    iput-boolean v0, v4, Ljbg;->l:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->o()Z

    move-result v0

    if-nez v0, :cond_9

    :goto_2
    iput-boolean v1, v4, Ljbg;->m:Z

    :cond_6
    if-eqz p2, :cond_7

    iput-object p2, v4, Ljbg;->a:Ljan;

    :cond_7
    return-object v4

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v1, v2

    goto :goto_2
.end method

.method public static a(Ljau;)Ljbg;
    .locals 3

    const/4 v1, 0x1

    new-instance v2, Ljbg;

    invoke-direct {v2}, Ljbg;-><init>()V

    iget-object v0, p0, Ljau;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    iput-boolean v0, v2, Ljbg;->g:Z

    iget-boolean v0, p0, Ljau;->i:Z

    iput-boolean v0, v2, Ljbg;->h:Z

    iget-boolean v0, p0, Ljau;->h:Z

    iput-boolean v0, v2, Ljbg;->i:Z

    iget-boolean v0, p0, Ljau;->u:Z

    iput-boolean v0, v2, Ljbg;->l:Z

    iget-boolean v0, p0, Ljau;->v:Z

    iput-boolean v0, v2, Ljbg;->m:Z

    iget-object v0, p0, Ljau;->a:Ljan;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljau;->a:Ljan;

    iput-object v0, v2, Ljbg;->a:Ljan;

    :cond_0
    iget-object v0, p0, Ljau;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ljau;->f:Ljava/lang/String;

    iput-object v0, v2, Ljbg;->b:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Ljau;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ljau;->g:Ljava/lang/String;

    iput-object v0, v2, Ljbg;->c:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Ljau;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ljau;->s:Ljava/lang/String;

    iput-object v0, v2, Ljbg;->d:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Ljau;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Ljau;->y:Ljava/lang/String;

    iput-object v0, v2, Ljbg;->o:Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Ljau;->d:Ljaf;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljau;->d:Ljaf;

    iget-object v0, v0, Ljaf;->a:Ljava/lang/String;

    iput-object v0, v2, Ljbg;->e:Ljava/lang/String;

    iget-object v0, p0, Ljau;->d:Ljaf;

    iget-object v0, v0, Ljaf;->b:Ljava/lang/String;

    iput-object v0, v2, Ljbg;->f:Ljava/lang/String;

    :cond_5
    :goto_1
    iget-object v0, p0, Ljau;->r:[Ljbj;

    array-length v0, v0

    if-lez v0, :cond_6

    iget-object v0, p0, Ljau;->r:[Ljbj;

    iput-object v0, v2, Ljbg;->n:[Ljbj;

    :cond_6
    return-object v2

    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    :cond_8
    iget-object v0, p0, Ljau;->p:Ljae;

    if-eqz v0, :cond_5

    iput-boolean v1, v2, Ljbg;->j:Z

    iget-object v0, p0, Ljau;->p:Ljae;

    iget-object v0, v0, Ljae;->a:Ljava/lang/String;

    iput-object v0, v2, Ljbg;->f:Ljava/lang/String;

    goto :goto_1
.end method

.method public static a(Lgse;Ljava/lang/String;ILizz;)V
    .locals 1

    invoke-virtual {p0, p2, p1}, Lgse;->a(ILjava/lang/String;)Lgsf;

    move-result-object v0

    invoke-static {p0, p1, p2, p3, v0}, Lhfx;->a(Lgse;Ljava/lang/String;ILizz;Lgsf;)V

    return-void
.end method

.method public static a(Lgse;Ljava/lang/String;ILizz;Lgsf;)V
    .locals 5

    const/4 v1, 0x1

    iget-object v2, p3, Lizz;->a:Ljbb;

    const/4 v0, 0x0

    iget-boolean v3, v2, Ljbb;->i:Z

    iget-boolean v4, p4, Lgsf;->b:Z

    if-eq v3, v4, :cond_0

    iget-boolean v0, v2, Ljbb;->i:Z

    iput-boolean v0, p4, Lgsf;->b:Z

    move v0, v1

    :cond_0
    iget-object v3, v2, Ljbb;->h:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v2, Ljbb;->h:Ljava/lang/String;

    iget-object v4, p4, Lgsf;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, v2, Ljbb;->h:Ljava/lang/String;

    iput-object v0, p4, Lgsf;->c:Ljava/lang/String;

    move v0, v1

    :cond_1
    iget-boolean v3, v2, Ljbb;->j:Z

    iget-boolean v4, p4, Lgsf;->d:Z

    if-eq v3, v4, :cond_3

    iget-boolean v0, v2, Ljbb;->j:Z

    iput-boolean v0, p4, Lgsf;->d:Z

    :goto_0
    if-eqz v1, :cond_2

    iget-object v0, p0, Lgse;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p2, p1}, Lgse;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Lgsf;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    return-void

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public static a(Lgsg;Landroid/accounts/Account;Ljava/lang/String;ILizz;)V
    .locals 6

    invoke-virtual {p0, p3, p1, p2}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lgsh;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lhfx;->a(Lgsg;Landroid/accounts/Account;Ljava/lang/String;ILizz;Lgsh;)V

    return-void
.end method

.method public static a(Lgsg;Landroid/accounts/Account;Ljava/lang/String;ILizz;Lgsh;)V
    .locals 9

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p5, :cond_3

    iget-object v0, p5, Lgsh;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p5, Lgsh;->b:Ljava/lang/String;

    const-string v1, "dont_send_loyalty_wob_id"

    invoke-static {v0, v1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p4, Lizz;->a:Ljbb;

    iget-object v0, v0, Ljbb;->l:[Ljai;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v1, 0x0

    iget-object v5, p4, Lizz;->a:Ljbb;

    iget-object v0, v5, Ljbb;->l:[Ljai;

    array-length v6, v0

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_b

    iget-object v0, v5, Ljbb;->l:[Ljai;

    aget-object v0, v0, v4

    iget-object v7, p5, Lgsh;->b:Ljava/lang/String;

    iget-object v8, v0, Ljai;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v4, :cond_3

    new-array v7, v6, [Ljai;

    aput-object v0, v7, v2

    move v1, v3

    :goto_2
    if-ge v1, v6, :cond_2

    iget-object v8, v5, Ljbb;->l:[Ljai;

    if-gt v1, v4, :cond_1

    add-int/lit8 v0, v1, -0x1

    :goto_3
    aget-object v0, v8, v0

    aput-object v0, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_3

    :cond_2
    iput-object v7, v5, Ljbb;->l:[Ljai;

    :cond_3
    iget-object v1, p4, Lizz;->a:Ljbb;

    iget-object v0, v1, Ljbb;->l:[Ljai;

    array-length v4, v0

    new-array v5, v4, [Ljai;

    move v0, v2

    :goto_4
    if-ge v0, v4, :cond_4

    iget-object v6, v1, Ljbb;->l:[Ljai;

    aget-object v6, v6, v0

    aput-object v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    iget-object v1, p5, Lgsh;->c:[Ljai;

    if-eq v1, v5, :cond_a

    if-nez v1, :cond_6

    move v0, v2

    :goto_5
    if-nez v0, :cond_5

    iput-object v5, p5, Lgsh;->c:[Ljai;

    invoke-virtual {p0, p3, p1, p2, p5}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;Lgsh;)V

    :cond_5
    return-void

    :cond_6
    array-length v0, v1

    array-length v4, v5

    if-eq v0, v4, :cond_7

    move v0, v2

    goto :goto_5

    :cond_7
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v0, v4, :cond_8

    move v0, v2

    goto :goto_5

    :cond_8
    array-length v4, v1

    move v0, v2

    :goto_6
    if-ge v0, v4, :cond_a

    aget-object v6, v1, v0

    aget-object v7, v5, v0

    invoke-static {v6, v7}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Lizs;)Z

    move-result v6

    if-nez v6, :cond_9

    move v0, v2

    goto :goto_5

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    move v0, v3

    goto :goto_5

    :cond_b
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Lgsi;Landroid/accounts/Account;ILizz;ZZZ)V
    .locals 1

    iget-object v0, p3, Lizz;->b:[Lioj;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    if-nez p4, :cond_0

    const/4 v0, 0x2

    :cond_0
    if-nez p5, :cond_1

    if-nez p6, :cond_2

    :cond_1
    or-int/lit8 v0, v0, 0x1

    :cond_2
    invoke-virtual {p0, p1, p2, p3, v0}, Lgsi;->a(Landroid/accounts/Account;ILizz;I)V

    :cond_3
    return-void
.end method

.method public static a(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Lhfx;->a:Landroid/content/Intent;

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static a(Lipy;Z)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lipy;->d:Ljak;

    if-eqz v2, :cond_0

    iget-object v3, v2, Ljak;->c:Ljal;

    if-eqz v3, :cond_0

    iget-object v3, v2, Ljak;->c:Ljal;

    iget v2, v2, Ljak;->a:I

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    return v1

    :sswitch_0
    if-nez p1, :cond_1

    iget-object v2, v3, Ljal;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v3, Ljal;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :sswitch_1
    iget-object v2, v3, Ljal;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v3, Ljal;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0x2c -> :sswitch_1
    .end sparse-switch
.end method

.method private static a([Ljah;)[Lcom/google/android/gms/wallet/InstrumentInfo;
    .locals 6

    array-length v0, p0

    new-array v1, v0, [Lcom/google/android/gms/wallet/InstrumentInfo;

    const/4 v0, 0x0

    array-length v2, p0

    :goto_0
    if-ge v0, v2, :cond_0

    new-instance v3, Lcom/google/android/gms/wallet/InstrumentInfo;

    aget-object v4, p0, v0

    iget-object v4, v4, Ljah;->a:Ljava/lang/String;

    invoke-static {v4}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aget-object v5, p0, v0

    iget-object v5, v5, Ljah;->b:Ljava/lang/String;

    invoke-static {v5}, Lhfx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/wallet/InstrumentInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static b(I)Ljava/lang/String;
    .locals 2

    const-string v0, "oauth2:https://www.googleapis.com/auth/sierra"

    if-eqz p0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/16 v1, 0x15

    if-ne p0, v1, :cond_1

    :cond_0
    const-string v0, "oauth2:https://www.googleapis.com/auth/sierrasandbox"

    :cond_1
    return-object v0
.end method

.method static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    new-instance v1, Ljava/math/BigDecimal;

    sget-object v0, Lgzq;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-double v2, v0

    div-double/2addr v2, v5

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    new-instance v2, Ljava/math/BigDecimal;

    sget-object v0, Lgzq;->c:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-double v3, v0

    div-double/2addr v3, v5

    invoke-direct {v2, v3, v4}, Ljava/math/BigDecimal;-><init>(D)V

    new-instance v3, Ljava/math/BigDecimal;

    sget-object v0, Lgzq;->d:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    :try_start_0
    new-instance v4, Ljava/math/BigDecimal;

    invoke-direct {v4, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x2

    sget-object v6, Ljava/math/RoundingMode;->UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v1

    sget-object v3, Lhfx;->b:Ljava/text/DecimalFormat;

    invoke-virtual {v3, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "OwServiceUtils"

    const-string v3, "Exception parsing masked wallet price"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
