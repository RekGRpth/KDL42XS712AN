.class public final Lamn;
.super Lasa;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lasa;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lamn;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;)Landroid/app/PendingIntent;
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;->c()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;->b()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v2

    const-string v3, "suppressProgressScreen"

    invoke-virtual {v1, v3, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    new-instance v4, Lauj;

    iget-object v5, p0, Lamn;->a:Landroid/content/Context;

    const-class v6, Lcom/google/android/gms/auth/login/LoginActivity;

    invoke-direct {v4, v5, v6}, Lauj;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v2}, Lauj;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lauj;

    move-result-object v2

    const-string v4, "SID"

    invoke-virtual {v2, v4}, Lauj;->b(Ljava/lang/String;)Lauj;

    move-result-object v2

    invoke-virtual {v2, v3}, Lauj;->a(Z)Lauj;

    move-result-object v2

    invoke-virtual {v2, v0}, Lauj;->a(Ljava/lang/String;)Lauj;

    move-result-object v0

    invoke-virtual {v0, v1}, Lauj;->a(Landroid/os/Bundle;)Lauj;

    move-result-object v0

    iget-object v1, v0, Lauj;->a:Landroid/content/Intent;

    const-string v2, "confirming"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v1, Laso;->b:Laso;

    invoke-virtual {v0, v1}, Lauj;->a(Laso;)Lauj;

    move-result-object v0

    iget-object v0, v0, Lauj;->a:Landroid/content/Intent;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lamn;->a:Landroid/content/Context;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v7, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;)Landroid/app/PendingIntent;
    .locals 6

    const/4 v5, 0x0

    new-instance v1, Larw;

    invoke-direct {v1}, Larw;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->a()Z

    iget-object v0, v1, Larw;->a:Landroid/content/Intent;

    const-string v2, "multi_user"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->b()Z

    iget-object v0, v1, Larw;->a:Landroid/content/Intent;

    const-string v2, "setup_wizard"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->d()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, v1, Larw;->a:Landroid/content/Intent;

    const-string v3, "login_options"

    new-instance v4, Landroid/os/Bundle;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-direct {v4, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->c()Ljava/util/List;

    move-result-object v0

    iget-object v2, v1, Larw;->a:Landroid/content/Intent;

    const-string v3, "allowed_domains"

    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, v1, Larw;->a:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, "com.google.android.auth.delegate.legacy.SETUP_ACCOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Laoy;

    iget-object v2, p0, Lamn;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Laoy;-><init>(Landroid/content/Context;)V

    iget-object v2, v1, Laoy;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v0, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t resolve SetupAccountWorkflow intent to an activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v3, :cond_1

    iget v3, v1, Laoy;->e:I

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v1, v3, v4}, Laoy;->a(II)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cannot delegate to Activity with different sig."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    const-string v1, "GLSActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0

    :cond_1
    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-object v1, p0, Lamn;->a:Landroid/content/Context;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v5, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;)Landroid/app/PendingIntent;
    .locals 4

    new-instance v0, Lauj;

    iget-object v1, p0, Lamn;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/auth/login/LoginActivity;

    invoke-direct {v0, v1, v2}, Lauj;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;->g()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {v0, v1}, Lauj;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lauj;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lauj;->a(Ljava/lang/String;)Lauj;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lauj;->b(Ljava/lang/String;)Lauj;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;->c()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lauj;->a(Landroid/os/Bundle;)Lauj;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Lauj;->a(Z)Lauj;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;->d()Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    move-result-object v1

    iget-object v2, v0, Lauj;->a:Landroid/content/Intent;

    const-string v3, "pacl"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;->e()Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    move-result-object v1

    iget-object v2, v0, Lauj;->a:Landroid/content/Intent;

    const-string v3, "facl"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, v0, Lauj;->a:Landroid/content/Intent;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lamn;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;)Landroid/app/PendingIntent;
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;->c()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;->b()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v2

    const-string v3, "suppressProgressScreen"

    invoke-virtual {v1, v3, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    new-instance v4, Lauj;

    iget-object v5, p0, Lamn;->a:Landroid/content/Context;

    const-class v6, Lcom/google/android/gms/auth/login/LoginActivity;

    invoke-direct {v4, v5, v6}, Lauj;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "SID"

    invoke-virtual {v4, v5}, Lauj;->b(Ljava/lang/String;)Lauj;

    move-result-object v4

    invoke-virtual {v4, v0}, Lauj;->a(Ljava/lang/String;)Lauj;

    move-result-object v0

    invoke-virtual {v0, v1}, Lauj;->a(Landroid/os/Bundle;)Lauj;

    move-result-object v0

    invoke-virtual {v0, v3}, Lauj;->a(Z)Lauj;

    move-result-object v0

    invoke-virtual {v0, v2}, Lauj;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lauj;

    move-result-object v0

    sget-object v1, Laso;->b:Laso;

    invoke-virtual {v0, v1}, Lauj;->a(Laso;)Lauj;

    move-result-object v0

    iget-object v0, v0, Lauj;->a:Landroid/content/Intent;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lamn;->a:Landroid/content/Context;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v7, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
