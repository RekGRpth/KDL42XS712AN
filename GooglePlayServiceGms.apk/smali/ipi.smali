.class public final Lipi;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[Lioj;

.field public c:[Lipv;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizs;-><init>()V

    const/16 v0, 0x64

    iput v0, p0, Lipi;->a:I

    invoke-static {}, Lioj;->c()[Lioj;

    move-result-object v0

    iput-object v0, p0, Lipi;->b:[Lioj;

    invoke-static {}, Lipv;->c()[Lipv;

    move-result-object v0

    iput-object v0, p0, Lipi;->c:[Lipv;

    const/4 v0, -0x1

    iput v0, p0, Lipi;->C:I

    return-void
.end method

.method public static a([B)Lipi;
    .locals 1

    new-instance v0, Lipi;

    invoke-direct {v0}, Lipi;-><init>()V

    invoke-static {v0, p0}, Lizs;->a(Lizs;[B)Lizs;

    move-result-object v0

    check-cast v0, Lipi;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v2, p0, Lipi;->a:I

    const/16 v3, 0x64

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    iget v3, p0, Lipi;->a:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lipi;->b:[Lioj;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lipi;->b:[Lioj;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lipi;->b:[Lioj;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lipi;->b:[Lioj;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    :cond_3
    iget-object v2, p0, Lipi;->c:[Lipv;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lipi;->c:[Lipv;

    array-length v2, v2

    if-lez v2, :cond_5

    :goto_1
    iget-object v2, p0, Lipi;->c:[Lipv;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    iget-object v2, p0, Lipi;->c:[Lipv;

    aget-object v2, v2, v1

    if-eqz v2, :cond_4

    const/4 v3, 0x3

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iput v0, p0, Lipi;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lipi;->a:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lipi;->b:[Lioj;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lioj;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lipi;->b:[Lioj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lioj;

    invoke-direct {v3}, Lioj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lipi;->b:[Lioj;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lioj;

    invoke-direct {v3}, Lioj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Lipi;->b:[Lioj;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lipi;->c:[Lipv;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lipv;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lipi;->c:[Lipv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lipv;

    invoke-direct {v3}, Lipv;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lipi;->c:[Lipv;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lipv;

    invoke-direct {v3}, Lipv;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Lipi;->c:[Lipv;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lipi;->a:I

    const/16 v2, 0x64

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    iget v2, p0, Lipi;->a:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Lipi;->b:[Lioj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lipi;->b:[Lioj;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v2, p0, Lipi;->b:[Lioj;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lipi;->b:[Lioj;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lipi;->c:[Lipv;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lipi;->c:[Lipv;

    array-length v0, v0

    if-lez v0, :cond_4

    :goto_1
    iget-object v0, p0, Lipi;->c:[Lipv;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lipi;->c:[Lipv;

    aget-object v0, v0, v1

    if-eqz v0, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lipi;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lipi;

    iget v2, p0, Lipi;->a:I

    iget v3, p1, Lipi;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lipi;->b:[Lioj;

    iget-object v3, p1, Lipi;->b:[Lioj;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lipi;->c:[Lipv;

    iget-object v3, p1, Lipi;->c:[Lipv;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lipi;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lipi;->b:[Lioj;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lipi;->c:[Lipv;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
