.class public final Lcun;
.super Lcve;
.source "SourceFile"


# static fields
.field private static r:Lcun;

.field private static final s:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final a:Ldll;

.field private final b:Ldll;

.field private final c:Lbmi;

.field private final d:Lbmi;

.field private final e:Lbmi;

.field private final f:Lcud;

.field private final g:Lcuf;

.field private final h:Lcuo;

.field private final i:Lcva;

.field private final j:Lcvk;

.field private final k:Lcvl;

.field private final l:Lcvu;

.field private final m:Lcul;

.field private final n:Lcvq;

.field private final o:Lcvf;

.field private final p:Lcvo;

.field private final q:Lcvv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcun;->s:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-string v0, "DataBroker"

    sget-object v1, Lcun;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, v6}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ldll;

    sget-object v0, Lcwh;->f:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    sget-object v0, Lcwh;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v1, v4, v3, v0}, Ldll;-><init>(Landroid/content/Context;ZZZ)V

    iput-object v2, p0, Lcun;->a:Ldll;

    new-instance v2, Ldll;

    const/4 v3, 0x1

    sget-object v0, Lcwh;->f:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v0, Lcwh;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v1, v3, v5, v0}, Ldll;-><init>(Landroid/content/Context;ZZZ)V

    iput-object v2, p0, Lcun;->b:Ldll;

    invoke-static {v1}, Lgak;->b(Landroid/content/Context;)Lbmi;

    move-result-object v0

    iput-object v0, p0, Lcun;->c:Lbmi;

    invoke-static {v1}, Lgak;->a(Landroid/content/Context;)Lbmi;

    move-result-object v0

    iput-object v0, p0, Lcun;->d:Lbmi;

    new-instance v0, Lbmi;

    sget-object v2, Lcwh;->e:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "/suggest"

    sget-object v5, Lcwh;->g:Lbfy;

    invoke-virtual {v5}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcun;->e:Lbmi;

    new-instance v0, Lcud;

    invoke-direct {v0, p0}, Lcud;-><init>(Lcve;)V

    iput-object v0, p0, Lcun;->f:Lcud;

    new-instance v0, Lcuf;

    iget-object v1, p0, Lcun;->a:Ldll;

    iget-object v2, p0, Lcun;->b:Ldll;

    invoke-direct {v0, p0, v1, v2}, Lcuf;-><init>(Lcve;Lbmi;Lbmi;)V

    iput-object v0, p0, Lcun;->g:Lcuf;

    new-instance v0, Lcuo;

    iget-object v1, p0, Lcun;->a:Ldll;

    iget-object v2, p0, Lcun;->b:Ldll;

    iget-object v3, p0, Lcun;->e:Lbmi;

    invoke-direct {v0, p0, v1, v2, v3}, Lcuo;-><init>(Lcve;Lbmi;Lbmi;Lbmi;)V

    iput-object v0, p0, Lcun;->h:Lcuo;

    new-instance v0, Lcva;

    iget-object v1, p0, Lcun;->a:Ldll;

    iget-object v2, p0, Lcun;->b:Ldll;

    invoke-direct {v0, p0, v1, v2}, Lcva;-><init>(Lcve;Lbmi;Lbmi;)V

    iput-object v0, p0, Lcun;->i:Lcva;

    new-instance v0, Lcvk;

    invoke-direct {v0, p0}, Lcvk;-><init>(Lcve;)V

    iput-object v0, p0, Lcun;->j:Lcvk;

    new-instance v0, Lcvl;

    iget-object v2, p0, Lcun;->a:Ldll;

    iget-object v3, p0, Lcun;->b:Ldll;

    iget-object v4, p0, Lcun;->c:Lbmi;

    iget-object v5, p0, Lcun;->d:Lbmi;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcvl;-><init>(Lcve;Lbmi;Lbmi;Lbmi;Lbmi;)V

    iput-object v0, p0, Lcun;->k:Lcvl;

    new-instance v0, Lcvu;

    iget-object v1, p0, Lcun;->b:Ldll;

    invoke-direct {v0, p0, v1}, Lcvu;-><init>(Lcve;Lbmi;)V

    iput-object v0, p0, Lcun;->l:Lcvu;

    new-instance v0, Lcul;

    iget-object v1, p0, Lcun;->b:Ldll;

    invoke-direct {v0, p0, v1}, Lcul;-><init>(Lcve;Lbmi;)V

    iput-object v0, p0, Lcun;->m:Lcul;

    new-instance v0, Lcvq;

    iget-object v1, p0, Lcun;->b:Ldll;

    invoke-direct {v0, p0, v1}, Lcvq;-><init>(Lcve;Lbmi;)V

    iput-object v0, p0, Lcun;->n:Lcvq;

    new-instance v0, Lcvf;

    iget-object v1, p0, Lcun;->b:Ldll;

    invoke-direct {v0, p0, v1}, Lcvf;-><init>(Lcve;Lbmi;)V

    iput-object v0, p0, Lcun;->o:Lcvf;

    new-instance v0, Lcvo;

    iget-object v1, p0, Lcun;->a:Ldll;

    iget-object v2, p0, Lcun;->b:Ldll;

    invoke-direct {v0, p0, v1, v2}, Lcvo;-><init>(Lcve;Lbmi;Lbmi;)V

    iput-object v0, p0, Lcun;->p:Lcvo;

    new-instance v0, Lcvv;

    iget-object v1, p0, Lcun;->a:Ldll;

    iget-object v2, p0, Lcun;->b:Ldll;

    invoke-direct {v0, p0, v1, v2}, Lcvv;-><init>(Lcve;Lbmi;Lbmi;)V

    iput-object v0, p0, Lcun;->q:Lcvv;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p4}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    invoke-virtual {p4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    new-array v1, v4, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v1, p0, Lcun;->i:Lcva;

    invoke-static {p1, p2, p3, v0}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v4, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Lcun;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    sget-object v0, Lcun;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    sget-object v0, Lcun;->r:Lcun;

    if-nez v0, :cond_0

    new-instance v0, Lcun;

    invoke-direct {v0, p0}, Lcun;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcun;->r:Lcun;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    sget-object v0, Lcun;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    sget-object v0, Lcun;->r:Lcun;

    return-object v0

    :catchall_0
    move-exception v0

    sget-object v1, Lcun;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->m:Lcul;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->m:Lcul;

    invoke-static {p2}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p3, p4}, Lcul;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method private f(Landroid/content/Context;)Ljava/util/HashSet;
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Lcto;->b(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->f:Lcud;

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcun;->b([Lcve;)V

    :try_start_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lcun;->f:Lcud;

    invoke-static {p1, v0}, Lcud;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1, v0}, Lcum;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->f:Lcud;

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcun;->a([Lcve;)V

    return-object v4

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v3, p0, Lcun;->f:Lcud;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->f:Lcud;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2, p3}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->f:Lcud;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->f:Lcud;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    iget-object v1, v1, Lcvl;->e:Lcve;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x2

    :try_start_0
    iget-object v1, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcun;->k:Lcvl;

    invoke-virtual {v0, p2, v1, p3, p4}, Lcvl;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :cond_0
    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->e:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->e:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    if-nez p5, :cond_0

    iget-object v0, p0, Lcun;->p:Lcvo;

    invoke-virtual {v0, p1, p2, p4}, Lcvo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ldrc;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return v2

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcvv;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;Z)I
    .locals 9

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->g:Lcuf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->g:Lcuf;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->g:Lcuf;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)I
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-array v0, v8, [Lcve;

    iget-object v1, p0, Lcun;->g:Lcuf;

    aput-object v1, v0, v7

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->g:Lcuf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v8, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v7

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v8, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v7

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLandroid/content/SyncResult;)I
    .locals 6

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->n:Lcvq;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v4

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    if-eqz p4, :cond_0

    iget-object v1, p0, Lcun;->n:Lcvq;

    invoke-virtual {v1}, Lcvq;->a()V

    :cond_0
    iget-object v1, p0, Lcun;->n:Lcvq;

    invoke-virtual {v1, p1, p2, v0, p3}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/16 v1, 0x1f4

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v1, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_2
    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->m:Lcul;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->m:Lcul;

    invoke-virtual {v0, p1, p2}, Lcul;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ldpc;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    iget-object v0, p0, Lcun;->p:Lcvo;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcvo;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ldpc;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ZLandroid/os/Bundle;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->k:Lcvl;

    invoke-virtual {v0, p1, p2, p3}, Lcvl;->a(Lcom/google/android/gms/common/server/ClientContext;ZLandroid/os/Bundle;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IIZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-virtual {v0, p4}, Lcuo;->a(I)Lcve;

    move-result-object v8

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    aput-object v8, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v3, "me"

    :cond_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IIZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    iget-object v1, v1, Lcvl;->d:Lcve;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->d:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->k:Lcvl;

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcvl;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->d:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldfh;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->i:Lcva;

    invoke-virtual {p3}, Ldfh;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Ldfh;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p3, Ldfh;->a:Landroid/os/Bundle;

    const-string v2, "time_span"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iget-object v1, p3, Ldfh;->a:Landroid/os/Bundle;

    const-string v2, "leaderboard_collection"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iget-object v1, p3, Ldfh;->a:Landroid/os/Bundle;

    const-string v2, "page_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    move-object v1, p1

    move-object v2, p2

    move v7, p4

    move v9, p5

    invoke-virtual/range {v0 .. v9}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIII)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpi;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    iget-object v0, p0, Lcun;->p:Lcvo;

    invoke-virtual {v0, p1, p2, p3}, Lcvo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpi;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-static {p1, p2, p3, p4, p5}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    iget-object v1, v1, Lcvl;->f:Lcve;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->f:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->k:Lcvl;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->f:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->g:Lcve;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->k:Lcvl;

    iget-object v3, v3, Lcvl;->g:Lcve;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->k:Lcvl;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcvl;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->k:Lcvl;

    iget-object v3, v3, Lcvl;->g:Lcve;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->k:Lcve;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->h:Lcuo;

    iget-object v3, v3, Lcuo;->k:Lcve;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->h:Lcuo;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-virtual/range {v0 .. v9}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->h:Lcuo;

    iget-object v3, v3, Lcuo;->k:Lcve;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I[Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcun;->q:Lcvv;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/util/ArrayList;Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v8

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v8

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcun;->p:Lcvo;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcvo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/util/ArrayList;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->o:Lcvf;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->o:Lcvf;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    if-nez p3, :cond_1

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_0

    iget-object v0, p0, Lcun;->i:Lcva;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_1
    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    const/4 v0, 0x2

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0

    :cond_1
    move-object v3, p3

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIII)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->n:Lcvq;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->n:Lcvq;

    invoke-static/range {p1 .. p8}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIII)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->i:Lcva;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;II[B[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->n:Lcvq;

    invoke-virtual {v0}, Lcvq;->a()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-static/range {p8 .. p8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcun;->n:Lcvq;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->n:Lcvq;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->n:Lcvq;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    iget-object v0, p0, Lcun;->k:Lcvl;

    invoke-virtual {v0, p2, p3}, Lcvl;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcve;

    move-result-object v9

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    aput-object v9, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcun;->k:Lcvl;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    aput-object v9, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    aput-object v9, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v6

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    iget-object v0, p0, Lcun;->p:Lcvo;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcvo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ldrc;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v6

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v6

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    if-nez p7, :cond_0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-object v0, p0, Lcun;->q:Lcvv;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->o:Lcvf;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {p7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->o:Lcvf;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v6

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->i:Lcva;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v6

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v6

    move v1, v6

    :goto_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v3

    const-string v4, "collection"

    invoke-virtual {v2, v4, v0, v3}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)I

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "player_rank"

    invoke-virtual {v2, v4, v0, v3}, Lcom/google/android/gms/common/data/DataHolder;->g(Ljava/lang/String;II)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v7

    :cond_0
    const-string v4, "player_raw_score"

    invoke-virtual {v2, v4, v0, v3}, Lcom/google/android/gms/common/data/DataHolder;->g(Ljava/lang/String;II)Z

    move-result v3

    if-nez v3, :cond_1

    move v6, v7

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v6

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0

    :cond_2
    if-eqz v6, :cond_3

    const-string v0, "DataBroker"

    const-string v3, "User has public score, checking gameplay ACL status..."

    invoke-static {v0, v3}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, v1}, Lcun;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p2, p4, v2}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    iget-object v0, p0, Lcun;->q:Lcvv;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    if-nez p6, :cond_0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-object v0, p0, Lcun;->q:Lcvv;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v8

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-static {p6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v8

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->n:Lcvq;

    aput-object v1, v0, v6

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->n:Lcvq;

    invoke-virtual {v0}, Lcvq;->a()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcun;->n:Lcvq;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ldrc;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->i:Lcva;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLjava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->h:Lcuo;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLjava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcun;->k:Lcvl;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ZLjava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->k:Lcvl;

    invoke-virtual {v0, p1, p2, p3}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->k:Lcvl;

    invoke-virtual {v0, p1}, Lcvl;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Z)Ldfr;
    .locals 12

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->i:Lcva;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Z)Ldfr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;)Ldpi;
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->p:Lcvo;

    invoke-virtual {v0, p1, p2, p3}, Lcvo;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;)Ldpi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a()V
    .locals 0

    invoke-super {p0}, Lcve;->c()V

    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->i:Lcva;

    invoke-virtual {v0, p1, p2, p3}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v4, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "DataBroker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No account found for the given datastore! Bailing! (datastore name :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v4, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcun;->a([Lcve;)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcun;->j:Lcvk;

    invoke-static {p1, v0, p2}, Lcvk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-array v0, v4, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcun;->a([Lcve;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcve;

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v3

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-array v0, v8, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v7

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v5, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    if-eqz v2, :cond_0

    if-nez v5, :cond_1

    :cond_0
    const-string v0, "DataBroker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No account found for the given datastore! Bailing! (datastore name :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v8, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v7

    invoke-virtual {p0, v0}, Lcun;->a([Lcve;)V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcun;->j:Lcvk;

    move-object v0, p1

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcvk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-array v0, v8, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v7

    invoke-virtual {p0, v0}, Lcun;->a([Lcve;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v8, [Lcve;

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v7

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 3

    invoke-direct {p0, p1}, Lcun;->f(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->l:[Lcve;

    invoke-super {p0, v2}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v2, p0, Lcun;->h:Lcuo;

    invoke-virtual {v2, p1, v0, p2, p3}, Lcuo;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcun;->h:Lcuo;

    iget-object v0, v0, Lcuo;->l:[Lcve;

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcun;->h:Lcuo;

    iget-object v1, v1, Lcuo;->l:[Lcve;

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final bridge synthetic a([Lcve;)V
    .locals 0

    invoke-super {p0, p1}, Lcve;->a([Lcve;)V

    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->l:Lcvu;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->l:Lcvu;

    invoke-virtual {v0, p1, p2}, Lcvu;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->l:Lcvu;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->l:Lcvu;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    if-nez p5, :cond_0

    :try_start_0
    iget-object v0, p0, Lcun;->p:Lcvo;

    invoke-virtual {v0, p1, p2, p4}, Lcvo;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ldrc;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return v2

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcvv;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;Z)I
    .locals 9

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->g:Lcuf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcuf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->g:Lcuf;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->g:Lcuf;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)I
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-array v0, v8, [Lcve;

    iget-object v1, p0, Lcun;->g:Lcuf;

    aput-object v1, v0, v7

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->g:Lcuf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcuf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v8, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v7

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v8, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v7

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->h:Lcuo;

    iget-object v1, v1, Lcuo;->h:Lcve;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcuo;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->h:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->h:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLandroid/content/SyncResult;)I
    .locals 6

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v4

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    if-eqz p4, :cond_0

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    :cond_0
    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-virtual {v0, p1, p2, p5}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V

    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0, p1, p2, p3}, Lcvf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/16 v1, 0x1f4

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v1, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_2
    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    iget-object v1, v1, Lcvl;->c:Lcve;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->c:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->k:Lcvl;

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->c:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->k:Lcvl;

    invoke-virtual {v0, p1, p2, p3}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-static {p1, p2, p3, p4, p5}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    if-eqz p5, :cond_0

    :try_start_0
    iget-object v0, p0, Lcun;->j:Lcvk;

    invoke-static {p1, p2}, Lcvk;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    :cond_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0, p1, p2, p3}, Lcvf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcun;->o:Lcvf;

    invoke-static {p1, p2, p3, p4, v0}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->d:Lcve;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->h:Lcuo;

    iget-object v3, v3, Lcuo;->d:Lcve;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->h:Lcuo;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcuo;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->h:Lcuo;

    iget-object v3, v3, Lcuo;->d:Lcve;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0, p1, p2, p3}, Lcvf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcun;->q:Lcvv;

    invoke-static {p1, p2, p4, v0}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->i:Lcva;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcva;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-array v0, v6, [Lcve;

    iget-object v1, p0, Lcun;->g:Lcuf;

    aput-object v1, v0, v7

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->g:Lcuf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v6, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v7

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v6, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v7

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->n:Lcvq;

    aput-object v1, v0, v6

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->n:Lcvq;

    invoke-virtual {v0}, Lcvq;->a()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcun;->n:Lcvq;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcvq;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ldrc;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final b()V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    iget-object v0, p0, Lcun;->n:Lcvq;

    invoke-virtual {v0}, Lcvq;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;)V
    .locals 5

    invoke-direct {p0, p1}, Lcun;->f(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    invoke-super {p0}, Lcve;->g()V

    :try_start_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Ldir;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcve;->f()V

    throw v0

    :cond_0
    invoke-super {p0}, Lcve;->f()V

    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2, v1}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    if-lez v2, :cond_1

    new-instance v0, Lcts;

    invoke-direct {v0, v1}, Lcts;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcts;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    if-nez v0, :cond_2

    const-string v0, "DataBroker"

    const-string v1, "Unable to load profile for player!"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    :cond_2
    iget-object v1, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2, v0}, Lcud;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->g:Lcuf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->g:Lcuf;

    invoke-virtual {v0, p1, p2, p3}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->g:Lcuf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0, p1}, Lcun;->f(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, p2, v2}, Lcun;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final bridge synthetic b([Lcve;)V
    .locals 0

    invoke-super {p0, p1}, Lcve;->b([Lcve;)V

    return-void
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    iget-object v1, v1, Lcvl;->e:Lcve;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->e:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcun;->k:Lcvl;

    invoke-virtual {v1, p2, v0, p3}, Lcvl;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->e:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    iget-object v1, v1, Lcvl;->c:Lcve;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->c:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->k:Lcvl;

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->c:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->n:Lcvq;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->n:Lcvq;

    invoke-static {p1, p2, p3, p4, p5}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->n:Lcvq;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->i:Lcve;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->h:Lcuo;

    iget-object v3, v3, Lcuo;->i:Lcve;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->h:Lcuo;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->h:Lcuo;

    iget-object v3, v3, Lcuo;->i:Lcve;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcvv;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ldrc;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-array v0, v6, [Lcve;

    iget-object v1, p0, Lcun;->g:Lcuf;

    aput-object v1, v0, v7

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->g:Lcuf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcuf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v6, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v7

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v6, [Lcve;

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v1, v7

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcve;->c()V

    return-void
.end method

.method public final c(Landroid/content/Context;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->l:Lcvu;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->l:Lcvu;

    invoke-static {p1}, Lcvu;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->l:Lcvu;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->l:Lcvu;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->j:Lcvk;

    invoke-static {p1, p2}, Lcvk;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-virtual {v0, p1, p2, p3}, Lcvv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0, p1}, Lcun;->f(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-super {p0}, Lcve;->g()V

    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0}, Ldix;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "package_name=?"

    invoke-virtual {v3, v0, v4, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Lcve;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcve;->f()V

    throw v0

    :cond_0
    return-void
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    .locals 2

    iget-object v0, p0, Lcun;->h:Lcuo;

    iget-object v0, v0, Lcuo;->l:[Lcve;

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    iget-object v1, p0, Lcun;->h:Lcuo;

    iget-object v1, v1, Lcuo;->l:[Lcve;

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcun;->h:Lcuo;

    iget-object v1, v1, Lcuo;->l:[Lcve;

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcvv;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->m:Lcul;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->m:Lcul;

    invoke-virtual {v0, p1, p2}, Lcul;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-array v0, v7, [Lcve;

    iget-object v1, p0, Lcun;->k:Lcvl;

    iget-object v1, v1, Lcvl;->e:Lcve;

    aput-object v1, v0, v8

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->e:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->k:Lcvl;

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcvl;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcve;

    iget-object v2, p0, Lcun;->k:Lcvl;

    iget-object v2, v2, Lcvl;->e:Lcve;

    aput-object v2, v1, v8

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->h:Lcuo;

    iget-object v1, v1, Lcuo;->h:Lcve;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->h:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcun;->h:Lcuo;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->h:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    iget-object v0, p0, Lcun;->k:Lcvl;

    invoke-virtual {v0, p2, p3}, Lcvl;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcve;

    move-result-object v8

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    aput-object v8, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcun;->k:Lcvl;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final d(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->j:Lcvk;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->j:Lcvk;

    invoke-static {p1, p2}, Lcvk;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final bridge synthetic d()V
    .locals 0

    invoke-super {p0}, Lcve;->d()V

    return-void
.end method

.method public final d(Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-array v0, v5, [Lcve;

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v4

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->i:Lcva;

    invoke-virtual {v0}, Lcva;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    new-array v0, v5, [Lcve;

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v4

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    invoke-direct {p0, p1}, Lcun;->f(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v1

    iget-object v0, p0, Lcun;->k:Lcvl;

    iget-object v0, v0, Lcvl;->h:[Lcve;

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_1
    iget-object v0, p0, Lcun;->k:Lcvl;

    invoke-virtual {v0}, Lcvl;->a()V

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcun;->k:Lcvl;

    invoke-static {p1, v0}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcun;->k:Lcvl;

    iget-object v1, v1, Lcvl;->h:[Lcve;

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0

    :catchall_1
    move-exception v0

    new-array v1, v5, [Lcve;

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcun;->k:Lcvl;

    iget-object v0, v0, Lcvl;->h:[Lcve;

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    new-array v0, v5, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v0, v4

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_2
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcun;->o:Lcvf;

    invoke-static {p1, v0}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    :catchall_2
    move-exception v0

    new-array v1, v5, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0

    :cond_1
    new-array v0, v5, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v4

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void
.end method

.method public final d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    .locals 12

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-virtual {v0, p1, p2, p3}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->h:Lcuo;

    aput-object v3, v1, v2

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcun;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v8}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_3

    const/4 v0, 0x2

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcun;->b([Lcve;)V

    :try_start_1
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcur;

    iget-object v3, v0, Lcur;->a:Ljava/lang/String;

    iget-wide v1, v0, Lcur;->c:J

    iget-wide v10, v0, Lcur;->b:J

    cmp-long v1, v1, v10

    if-lez v1, :cond_1

    iget-object v1, p0, Lcun;->g:Lcuf;

    invoke-virtual {v1, p1, p2, v3, p3}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v1

    iget-object v2, p0, Lcun;->i:Lcva;

    invoke-virtual {v2, p1, p2, v3, p3}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v4, :cond_2

    iget-object v0, p0, Lcun;->g:Lcuf;

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->g:Lcuf;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcun;->a([Lcve;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :catchall_1
    move-exception v0

    const/4 v1, 0x2

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->g:Lcuf;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcun;->i:Lcva;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0

    :cond_3
    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcun;->b([Lcve;)V

    :try_start_2
    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-static {p1, p2, v9}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v0, 0x1

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcun;->a([Lcve;)V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcve;

    const/4 v2, 0x0

    iget-object v3, p0, Lcun;->h:Lcuo;

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcun;->a([Lcve;)V

    throw v0
.end method

.method public final e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-virtual {v0}, Lcvf;->a()V

    iget-object v0, p0, Lcun;->q:Lcvv;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcvv;->f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->h:Lcuo;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-static {p1, p2, p3}, Lcuo;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->f:Lcud;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->f:Lcud;

    invoke-static {p1, p2}, Lcud;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->f:Lcud;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->f:Lcud;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final e(Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    new-array v0, v5, [Lcve;

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v3}, Lcum;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    iget-object v4, p0, Lcun;->f:Lcud;

    invoke-static {p1, v3}, Lcud;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    iget-object v3, p0, Lcun;->j:Lcvk;

    aget-object v3, v2, v0

    invoke-static {p1, v3}, Lcvk;->a(Landroid/content/Context;Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-array v0, v5, [Lcve;

    iget-object v2, p0, Lcun;->j:Lcvk;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v2, v5, [Lcve;

    iget-object v3, p0, Lcun;->j:Lcvk;

    aput-object v3, v2, v1

    invoke-super {p0, v2}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->h:Lcuo;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-virtual {v0, p1, p2, p3}, Lcuo;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->h:Lcuo;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final bridge synthetic e()Z
    .locals 1

    invoke-super {p0}, Lcve;->e()Z

    move-result v0

    return v0
.end method

.method public final f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    .locals 5

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->m:Lcul;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcun;->i:Lcva;

    aput-object v1, v0, v4

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->m:Lcul;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcul;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcun;->i:Lcva;

    invoke-virtual {v1}, Lcva;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcun;->i:Lcva;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->o:Lcvf;

    invoke-static {p1, p2, p3}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lcve;->f()V

    return-void
.end method

.method public final g(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->o:Lcvf;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->p:Lcvo;

    invoke-virtual {v0, p1, p2, p3}, Lcvo;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->o:Lcvf;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final bridge synthetic g()V
    .locals 0

    invoke-super {p0}, Lcve;->g()V

    return-void
.end method

.method public final h(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->h:Lcuo;

    aput-object v1, v0, v4

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-virtual {v0, p1, p2, p3}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    new-array v2, v2, [Lcve;

    iget-object v3, p0, Lcun;->h:Lcuo;

    aput-object v3, v2, v4

    invoke-super {p0, v2}, Lcve;->a([Lcve;)V

    return-wide v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    aput-object v2, v1, v4

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final i(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->h:Lcuo;

    iget-object v1, v1, Lcuo;->j:Lcve;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->h:Lcuo;

    invoke-virtual {v0, p1, p2, p3}, Lcuo;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->j:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->h:Lcuo;

    iget-object v2, v2, Lcuo;->j:Lcve;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final j(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->m:Lcul;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    :try_start_0
    iget-object v0, p0, Lcun;->m:Lcul;

    invoke-virtual {v0, p1, p2, p3}, Lcul;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method

.method public final k(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->m:Lcul;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->b([Lcve;)V

    :try_start_0
    iget-object v0, p0, Lcun;->m:Lcul;

    invoke-static {p1, p2, p3}, Lcul;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Lcve;

    iget-object v1, p0, Lcun;->m:Lcul;

    aput-object v1, v0, v3

    invoke-super {p0, v0}, Lcve;->a([Lcve;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcve;

    iget-object v2, p0, Lcun;->m:Lcul;

    aput-object v2, v1, v3

    invoke-super {p0, v1}, Lcve;->a([Lcve;)V

    throw v0
.end method
