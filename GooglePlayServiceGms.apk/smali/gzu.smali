.class public final Lgzu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgxh;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 0

    iput-object p1, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    const/4 v5, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->j(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Landroid/accounts/Account;

    move-result-object v1

    const/4 v4, 0x1

    iget-object v3, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lioq;

    move-result-object v10

    iget-object v3, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Ljava/util/ArrayList;

    move-result-object v11

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    move v8, v5

    move-object v9, v2

    invoke-static/range {v0 .. v11}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lioq;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    const/16 v2, 0x1f6

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final a(Lioj;)V
    .locals 9

    const/4 v5, 0x0

    iget-object v0, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Lioj;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Landroid/accounts/Account;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lioq;

    move-result-object v6

    iget-object v2, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Ljava/util/ArrayList;

    move-result-object v7

    move-object v2, p1

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;ZZLjava/lang/String;Lioq;Ljava/util/Collection;Lipv;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    const/16 v2, 0x1f8

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgzu;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    goto :goto_0
.end method

.method public final b(Lioj;)V
    .locals 0

    return-void
.end method
