.class public final Lina;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Ljava/util/Map;

.field static b:Ljava/util/Map;

.field private static c:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lina;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lina;->b:Ljava/util/Map;

    new-instance v0, Linj;

    invoke-direct {v0}, Linj;-><init>()V

    invoke-static {v2, v0}, Lina;->a(BLizk;)V

    new-instance v0, Linh;

    invoke-direct {v0}, Linh;-><init>()V

    invoke-static {v3, v0}, Lina;->a(BLizk;)V

    const/4 v0, 0x4

    new-instance v1, Lind;

    invoke-direct {v1}, Lind;-><init>()V

    invoke-static {v0, v1}, Lina;->a(BLizk;)V

    const/16 v0, 0xa

    new-instance v1, Linr;

    invoke-direct {v1}, Linr;-><init>()V

    invoke-static {v0, v1}, Lina;->a(BLizk;)V

    const/4 v0, 0x3

    new-instance v1, Linn;

    invoke-direct {v1}, Linn;-><init>()V

    invoke-static {v0, v1}, Lina;->a(BLizk;)V

    const/4 v0, 0x7

    new-instance v1, Linl;

    invoke-direct {v1}, Linl;-><init>()V

    invoke-static {v0, v1}, Lina;->a(BLizk;)V

    const/16 v0, 0x8

    new-instance v1, Line;

    invoke-direct {v1}, Line;-><init>()V

    invoke-static {v0, v1}, Lina;->a(BLizk;)V

    const/16 v0, 0x10

    new-instance v1, Lino;

    invoke-direct {v1}, Lino;-><init>()V

    invoke-static {v0, v1}, Lina;->a(BLizk;)V

    const/4 v0, 0x2

    new-instance v1, Linm;

    invoke-direct {v1}, Linm;-><init>()V

    invoke-static {v0, v1}, Lina;->a(BLizk;)V

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "authToken"

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lina;->c:Ljava/util/List;

    return-void
.end method

.method public static a(Lizk;)B
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lina;->a:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    if-nez v0, :cond_1

    const/4 v0, -0x2

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    goto :goto_0
.end method

.method public static a()Linl;
    .locals 3

    new-instance v0, Linl;

    invoke-direct {v0}, Linl;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Linl;->a(I)Linl;

    const-string v1, ""

    invoke-virtual {v0, v1}, Linl;->a(Ljava/lang/String;)Linl;

    new-instance v1, Linq;

    invoke-direct {v1}, Linq;-><init>()V

    new-instance v1, Ling;

    invoke-direct {v1}, Ling;-><init>()V

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Ling;->a(I)Ling;

    sget-object v2, Lizf;->a:Lizf;

    invoke-virtual {v1, v2}, Ling;->a(Lizf;)Ling;

    invoke-virtual {v0, v1}, Linl;->a(Ling;)Linl;

    return-object v0
.end method

.method public static a(B[B)Lizk;
    .locals 2

    sget-object v0, Lina;->b:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizk;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unknown tag"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizk;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    array-length v1, p1

    if-lez v1, :cond_1

    array-length v1, p1

    invoke-virtual {v0, p1, v1}, Lizk;->a([BI)Lizk;

    :cond_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lizk;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0, p1}, Lina;->b(Ljava/lang/StringBuilder;Lizk;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(BLizk;)V
    .locals 3

    sget-object v0, Lina;->a:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lina;->b:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static a(Lizk;I)V
    .locals 1

    instance-of v0, p0, Linl;

    if-eqz v0, :cond_1

    check-cast p0, Linl;

    invoke-virtual {p0, p1}, Linl;->b(I)Linl;

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Linj;

    if-eqz v0, :cond_2

    check-cast p0, Linj;

    invoke-virtual {p0, p1}, Linj;->a(I)Linj;

    goto :goto_0

    :cond_2
    instance-of v0, p0, Linh;

    if-eqz v0, :cond_3

    check-cast p0, Linh;

    invoke-virtual {p0, p1}, Linh;->a(I)Linh;

    goto :goto_0

    :cond_3
    instance-of v0, p0, Line;

    if-eqz v0, :cond_0

    check-cast p0, Line;

    invoke-virtual {p0, p1}, Line;->a(I)Line;

    goto :goto_0
.end method

.method public static a(Lizk;J)V
    .locals 1

    instance-of v0, p0, Linl;

    if-eqz v0, :cond_1

    check-cast p0, Linl;

    invoke-virtual {p0, p1, p2}, Linl;->a(J)Linl;

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Linm;

    if-eqz v0, :cond_2

    check-cast p0, Linm;

    invoke-virtual {p0, p1, p2}, Linm;->b(J)Linm;

    goto :goto_0

    :cond_2
    instance-of v0, p0, Linj;

    if-eqz v0, :cond_3

    check-cast p0, Linj;

    invoke-virtual {p0, p1, p2}, Linj;->a(J)Linj;

    goto :goto_0

    :cond_3
    instance-of v0, p0, Linh;

    if-eqz v0, :cond_4

    check-cast p0, Linh;

    invoke-virtual {p0, p1, p2}, Linh;->a(J)Linh;

    goto :goto_0

    :cond_4
    instance-of v0, p0, Line;

    if-eqz v0, :cond_0

    check-cast p0, Line;

    invoke-virtual {p0, p1, p2}, Line;->c(J)Line;

    goto :goto_0
.end method

.method public static a(Lizk;Ljava/lang/String;)V
    .locals 1

    instance-of v0, p0, Linl;

    if-eqz v0, :cond_1

    check-cast p0, Linl;

    invoke-virtual {p0, p1}, Linl;->b(Ljava/lang/String;)Linl;

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Line;

    if-eqz v0, :cond_0

    check-cast p0, Line;

    invoke-virtual {p0, p1}, Line;->e(Ljava/lang/String;)Line;

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;Lizk;)V
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v4

    :try_start_0
    const-string v0, "("

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v0, v4, v3

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v6, v0, Ljava/lang/Boolean;

    if-nez v6, :cond_0

    const/4 v6, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lina;->c:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v6, 0x3d

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    instance-of v1, v0, Lizk;

    if-eqz v1, :cond_1

    check-cast v0, Lizk;

    invoke-static {p0, v0}, Lina;->b(Ljava/lang/StringBuilder;Lizk;)V

    :goto_1
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    instance-of v1, v0, [Lizk;

    if-eqz v1, :cond_3

    check-cast v0, [Lizk;

    const-string v1, "["

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v6, v0

    move v1, v2

    :goto_2
    if-ge v1, v6, :cond_2

    aget-object v7, v0, v1

    invoke-static {p0, v7}, Lina;->b(Ljava/lang/StringBuilder;Lizk;)V

    const-string v7, ","

    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error generating: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    return-void

    :cond_3
    :try_start_1
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    const-string v0, ")"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public static b(Lizk;)Ljava/lang/String;
    .locals 1

    instance-of v0, p0, Linl;

    if-eqz v0, :cond_0

    check-cast p0, Linl;

    iget-object v0, p0, Linl;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Line;

    if-eqz v0, :cond_1

    check-cast p0, Line;

    iget-object v0, p0, Line;->g:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/StringBuilder;Lizk;)V
    .locals 5

    invoke-static {p1}, Lina;->a(Lizk;)B

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Packet: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lina;->a(Ljava/lang/StringBuilder;Lizk;)V

    :cond_0
    :goto_0
    invoke-static {p1}, Lina;->c(Lizk;)I

    move-result v0

    if-lez v0, :cond_1

    const-string v1, "lastStream="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    invoke-static {p1}, Lina;->c(Lizk;)I

    move-result v0

    if-eqz v0, :cond_2

    const-string v1, "status="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_2
    return-void

    :pswitch_1
    const-string v0, "LoginRequest: ack("

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, p1

    check-cast v0, Linm;

    iget-object v1, v0, Linm;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    iget-object v0, v0, Linm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linp;

    iget-object v2, v0, Linp;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Linp;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const-string v0, ")"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_2
    move-object v0, p1

    check-cast v0, Linn;

    iget-object v0, v0, Linn;->a:Linf;

    if-eqz v0, :cond_5

    const-string v0, "LoginResponse ERROR "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lina;->a(Ljava/lang/StringBuilder;Lizk;)V

    goto/16 :goto_0

    :cond_5
    const-string v0, "LoginResponse OK"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_3
    move-object v0, p1

    check-cast v0, Line;

    const-string v1, "DataMessage: app="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Line;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " extras="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Line;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Line;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, v0, Line;->i:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const-string v1, " user="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v0, Line;->i:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_4
    const-string v1, "Heartbeat "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_5
    move-object v0, p1

    check-cast v0, Linl;

    iget v0, v0, Linl;->a:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_7

    const-string v0, "StreamAck"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    :goto_3
    const-string v0, "IQ: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lina;->a(Ljava/lang/StringBuilder;Lizk;)V

    goto/16 :goto_0

    :cond_7
    const/16 v1, 0xc

    if-ne v0, v1, :cond_6

    const-string v0, "SelectiveAck "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lina;->a(Ljava/lang/StringBuilder;Lizk;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method static c(Lizk;)I
    .locals 1

    instance-of v0, p0, Linl;

    if-eqz v0, :cond_0

    check-cast p0, Linl;

    iget v0, p0, Linl;->d:I

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Line;

    if-eqz v0, :cond_1

    check-cast p0, Line;

    iget v0, p0, Line;->h:I

    goto :goto_0

    :cond_1
    instance-of v0, p0, Linj;

    if-eqz v0, :cond_2

    check-cast p0, Linj;

    iget v0, p0, Linj;->a:I

    goto :goto_0

    :cond_2
    instance-of v0, p0, Linh;

    if-eqz v0, :cond_3

    check-cast p0, Linh;

    iget v0, p0, Linh;->a:I

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method
