.class public Ldaw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lcwm;

.field protected b:Ldax;


# direct methods
.method private constructor <init>(Lcwm;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldaw;->a:Lcwm;

    invoke-virtual {p0, p2}, Ldaw;->a(I)V

    return-void
.end method

.method synthetic constructor <init>(Lcwm;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ldaw;-><init>(Lcwm;I)V

    return-void
.end method

.method public static a(Lcwm;I)Ldaw;
    .locals 1

    const/16 v0, 0xc

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lday;

    invoke-direct {v0, p0, p1}, Lday;-><init>(Lcwm;I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldaw;

    invoke-direct {v0, p0, p1}, Ldaw;-><init>(Lcwm;I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 6

    iget-object v0, p0, Ldaw;->a:Lcwm;

    iget-object v1, p0, Ldaw;->b:Ldax;

    iget-object v1, v1, Ldax;->a:Landroid/os/IBinder;

    iget-object v2, p0, Ldaw;->b:Ldax;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "popupLocationInfo.gravity"

    iget v5, v2, Ldax;->b:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.displayId"

    iget v5, v2, Ldax;->c:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.left"

    iget v5, v2, Ldax;->d:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.top"

    iget v5, v2, Ldax;->e:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.right"

    iget v5, v2, Ldax;->f:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "popupLocationInfo.bottom"

    iget v2, v2, Ldax;->g:I

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v3}, Lcwm;->a(Landroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method protected a(I)V
    .locals 3

    new-instance v0, Ldax;

    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Ldax;-><init>(ILandroid/os/IBinder;B)V

    iput-object v0, p0, Ldaw;->b:Ldax;

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public final b()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Ldaw;->b:Ldax;

    iget-object v0, v0, Ldax;->a:Landroid/os/IBinder;

    return-object v0
.end method
