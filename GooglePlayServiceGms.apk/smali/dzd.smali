.class public final Ldzd;
.super Lebd;
.source "SourceFile"


# instance fields
.field private final a:Ldxm;

.field private final b:Ldyg;


# direct methods
.method public constructor <init>(Ldxm;)V
    .locals 1

    invoke-direct {p0}, Lebd;-><init>()V

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Ldzd;->a:Ldxm;

    new-instance v0, Ldyg;

    invoke-direct {v0, p1}, Ldyg;-><init>(Ldxm;)V

    iput-object v0, p0, Ldzd;->b:Ldyg;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->b()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-virtual {p0, v0}, Ldzd;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldzd;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldzd;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientReqInboxHelper"

    const-string v1, "onRequestClusterSeeMoreClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->o:Ldlf;

    invoke-interface {v1, v0, p1, p2}, Ldlf;->a(Lbdu;Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Ldzd;->a:Ldxm;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2}, Ldxm;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 3

    iget-object v0, p0, Ldzd;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldzd;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientReqInboxHelper"

    const-string v1, "onRequestDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->o:Ldlf;

    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ldlf;->a(Lbdu;Ljava/lang/String;)Lbeh;

    goto :goto_0
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 3

    iget-object v0, p0, Ldzd;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ClientReqInboxHelper"

    const-string v1, "onRequestClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lbha;->a([Lbgz;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "requests"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    iget-object v0, p0, Ldzd;->a:Ldxm;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Ldxm;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Ldzd;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->finish()V

    goto :goto_0
.end method

.method public final b_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Ldzd;->b:Ldyg;

    invoke-virtual {v0, p1}, Ldyg;->a(Lcom/google/android/gms/games/Game;)V

    return-void
.end method
