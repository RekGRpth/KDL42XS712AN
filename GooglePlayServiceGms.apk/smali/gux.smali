.class public final Lgux;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V
    .locals 0

    iput-object p1, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->R_()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;Z)V

    new-instance v1, Lioz;

    invoke-direct {v1}, Lioz;-><init>()V

    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    invoke-virtual {v2}, Lgvc;->a()Lixo;

    move-result-object v2

    iput-object v2, v1, Lioz;->b:Lixo;

    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    invoke-virtual {v2}, Lgvc;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    invoke-virtual {v2}, Lgvc;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lioz;->c:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lioz;->d:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Lioq;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Lioq;

    move-result-object v2

    iput-object v2, v1, Lioz;->a:Lioq;

    :cond_2
    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v1, Lioz;->b:Lixo;

    iget-object v3, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lixo;->t:Ljava/lang/String;

    :cond_3
    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->r:Landroid/widget/CheckBox;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->r:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->r:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_0
    iget-object v2, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j()Lgyi;

    move-result-object v2

    invoke-virtual {v2}, Lgyi;->a()Lhca;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lhca;->a(Lioz;Z)V

    :goto_1
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lgux;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->i()Z

    goto :goto_1
.end method
