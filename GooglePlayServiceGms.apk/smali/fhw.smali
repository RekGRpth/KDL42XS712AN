.class public final Lfhw;
.super Lbob;
.source "SourceFile"


# instance fields
.field private final a:Lfia;

.field private final b:Lfhz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.google.android.gms.people"

    invoke-direct {p0, v0, v1}, Lbob;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Lfia;

    invoke-direct {v0}, Lfia;-><init>()V

    iput-object v0, p0, Lfhw;->a:Lfia;

    const-string v0, "PeopleSync"

    const-string v1, "PeopleSyncAdapter created."

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    iput-object v0, p0, Lfhw;->b:Lfhz;

    return-void
.end method

.method private a(Landroid/accounts/Account;Landroid/os/Bundle;ILandroid/content/SyncResult;ZZZZZLfhx;)V
    .locals 24

    const/4 v14, 0x0

    const/4 v13, 0x0

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    const-wide/16 v17, 0x0

    const/16 v16, 0x1

    const/4 v15, 0x0

    const/4 v2, 0x1

    move/from16 v0, p3

    if-ne v0, v2, :cond_0

    :try_start_0
    sget-object v2, Lfbd;->r:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhw;->b:Lfhz;

    invoke-static/range {p2 .. p2}, Lfhz;->b(Landroid/os/Bundle;)I
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lfic; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lfic; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->a:Lfia;

    invoke-virtual {v2}, Lfia;->c()V

    :cond_0
    if-nez p8, :cond_d

    if-eqz p6, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lfhz;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    :cond_1
    if-nez p9, :cond_2

    new-instance v10, Lfid;

    invoke-direct {v10}, Lfid;-><init>()V
    :try_end_2
    .catch Lamq; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lfic; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lfhw;->a:Lfia;

    const/4 v9, 0x0

    move/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v8, p5

    move-object/from16 v11, p2

    move/from16 v12, p7

    invoke-static/range {v2 .. v12}, Lfie;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lfia;ZLfhx;Lfid;Landroid/os/Bundle;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    move-object/from16 v0, v19

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Stats="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v4, v5, v6}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->a:Lfia;

    invoke-virtual {v2}, Lfia;->c()V

    if-eqz p5, :cond_3

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lfbo;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    const-string v4, "Account opted out from G+."

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->d(Landroid/content/Context;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->a:Lfia;

    invoke-virtual {v2}, Lfia;->c()V

    if-nez p3, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lfhz;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lfbd;->q:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v5, v2

    invoke-virtual {v3, v4, v5, v6}, Lfhz;->a(Ljava/lang/String;J)Ljava/util/List;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_4
    .catch Lamq; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lfic; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result-object v22

    move v3, v14

    :goto_3
    :try_start_5
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v5, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v7, "Sync start"

    invoke-static {v2, v5, v6, v4, v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, Lfid;

    invoke-direct {v10}, Lfid;-><init>()V
    :try_end_5
    .catch Lamq; {:try_start_5 .. :try_end_5} :catch_1e
    .catch Lfic; {:try_start_5 .. :try_end_5} :catch_1b
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    add-int/lit8 v14, v3, 0x1

    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lfhw;->a:Lfia;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v11, p2

    invoke-static/range {v2 .. v12}, Lfie;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lfia;ZLfhx;Lfid;Landroid/os/Bundle;Z)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    move-object/from16 v0, v19

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Stats="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v5, v4, v6}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->a:Lfia;

    invoke-virtual {v2}, Lfia;->c()V

    move v3, v14

    goto :goto_3

    :pswitch_0
    sget-object v2, Lfbd;->s:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    move-object/from16 v0, v19

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Stats="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v4, v5, v6, v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2
    :try_end_7
    .catch Lamq; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lfic; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :catch_0
    move-exception v2

    move-object v11, v13

    move v4, v14

    :goto_4
    const/4 v3, 0x0

    :try_start_8
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception v2

    move-object v13, v11

    move v14, v4

    move v4, v3

    move-object v3, v2

    move v2, v15

    :goto_5
    :try_start_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lfhw;->a(Landroid/accounts/Account;Z)V

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, v4, Lfhz;->b:Lfbe;

    invoke-virtual {v2, v5}, Lfbe;->b(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v6, v2, 0x1

    iget-object v2, v4, Lfhz;->b:Lfbe;

    invoke-virtual {v2, v5, v6}, Lfbe;->b(Ljava/lang/String;I)V

    sget-object v2, Lfbd;->F:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v6, v2, :cond_4

    const-string v2, "Too many failures."

    invoke-virtual {v4, v5, v2}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    move-object/from16 v0, p4

    iput-boolean v2, v0, Landroid/content/SyncResult;->tooManyRetries:Z

    :cond_4
    sget-object v2, Lfbd;->G:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v6, v2, :cond_5

    const-string v2, "Too many failures.  Disabling sync."

    invoke-virtual {v4, v5, v2}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v4, Lfhz;->a:Landroid/content/Context;

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->e()Lfbk;

    move-result-object v2

    invoke-virtual {v2, v5}, Lfbk;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    const-string v5, "com.google.android.gms.people"

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v5, v6}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    :cond_5
    :goto_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_9
    .catch Lamq; {:try_start_9 .. :try_end_9} :catch_4
    .catch Lsp; {:try_start_9 .. :try_end_9} :catch_17
    .catch Lfja; {:try_start_9 .. :try_end_9} :catch_14
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_11
    .catch Lfic; {:try_start_9 .. :try_end_9} :catch_e
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_b

    move-result-wide v4

    sub-long v9, v4, v20

    :try_start_a
    throw v3
    :try_end_a
    .catch Lamq; {:try_start_a .. :try_end_a} :catch_1
    .catch Lsp; {:try_start_a .. :try_end_a} :catch_18
    .catch Lfja; {:try_start_a .. :try_end_a} :catch_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_12
    .catch Lfic; {:try_start_a .. :try_end_a} :catch_f
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_c

    :catch_1
    move-exception v7

    move-object v11, v13

    move v4, v14

    :goto_7
    const-string v2, "PeopleSync"

    const-string v3, "Cannot authenticate"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v3, v5, v7}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v5, v12

    iput-wide v5, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "Auth error"

    invoke-static {v2, v3, v5, v6, v8}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    const/4 v6, 0x3

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v8, v19

    invoke-virtual/range {v2 .. v11}, Lfhz;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLfeu;)V

    :goto_8
    return-void

    :cond_6
    :try_start_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lfhz;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    goto/16 :goto_2

    :catchall_2
    move-exception v2

    move-object/from16 v0, v19

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Stats="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v5, v6, v4, v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2
    :try_end_b
    .catch Lamq; {:try_start_b .. :try_end_b} :catch_0
    .catch Lfic; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :catch_2
    move-exception v2

    :goto_9
    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_c
    throw v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :catchall_3
    move-exception v2

    move-object/from16 v23, v2

    move v2, v3

    move-object/from16 v3, v23

    goto/16 :goto_5

    :cond_7
    move v4, v3

    :goto_a
    if-nez p7, :cond_c

    :try_start_d
    new-instance v2, Lfiu;

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v5}, Lfiu;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lfhw;->a:Lfia;

    invoke-virtual {v2, v3}, Lfiu;->a(Lfia;)Lfeu;
    :try_end_d
    .catch Lamq; {:try_start_d .. :try_end_d} :catch_1f
    .catch Lfic; {:try_start_d .. :try_end_d} :catch_1c
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    move-result-object v11

    :goto_b
    :try_start_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->a:Lfia;

    invoke-virtual {v2}, Lfia;->c()V
    :try_end_e
    .catch Lamq; {:try_start_e .. :try_end_e} :catch_20
    .catch Lfic; {:try_start_e .. :try_end_e} :catch_1d
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    const/4 v2, 0x0

    :try_start_f
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lfhw;->a(Landroid/accounts/Account;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lfhz;->d(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_f
    .catch Lamq; {:try_start_f .. :try_end_f} :catch_1a
    .catch Lsp; {:try_start_f .. :try_end_f} :catch_5
    .catch Lfja; {:try_start_f .. :try_end_f} :catch_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_7
    .catch Lfic; {:try_start_f .. :try_end_f} :catch_8
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_9

    move-result-wide v2

    sub-long v9, v2, v20

    :try_start_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v2, Lfhz;->b:Lfbe;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lfbe;->a(Ljava/lang/String;I)V

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Sync finished"

    invoke-static {v2, v3, v5, v6, v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v8, v19

    invoke-virtual/range {v2 .. v11}, Lfhz;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLfeu;)V
    :try_end_10
    .catch Lamq; {:try_start_10 .. :try_end_10} :catch_3
    .catch Lsp; {:try_start_10 .. :try_end_10} :catch_19
    .catch Lfja; {:try_start_10 .. :try_end_10} :catch_16
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_10 .. :try_end_10} :catch_13
    .catch Lfic; {:try_start_10 .. :try_end_10} :catch_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_d

    goto/16 :goto_8

    :catch_3
    move-exception v7

    goto/16 :goto_7

    :cond_8
    :try_start_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lfhz;->d(Ljava/lang/String;)V
    :try_end_11
    .catch Lamq; {:try_start_11 .. :try_end_11} :catch_4
    .catch Lsp; {:try_start_11 .. :try_end_11} :catch_17
    .catch Lfja; {:try_start_11 .. :try_end_11} :catch_14
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_11
    .catch Lfic; {:try_start_11 .. :try_end_11} :catch_e
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_b

    goto/16 :goto_6

    :catch_4
    move-exception v7

    move-wide/from16 v9, v17

    move-object v11, v13

    move v4, v14

    goto/16 :goto_7

    :catch_5
    move-exception v7

    move-wide/from16 v9, v17

    move v14, v4

    :goto_c
    const-string v2, "PeopleSync"

    const-string v3, "Network request failed"

    invoke-static {v2, v3, v7}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v7}, Lfjv;->a(Lsp;)V

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "VolleyError: "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lfjv;->a(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v2 .. v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhw;->b:Lfhz;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v7}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v4, Lfhz;->b:Lfbe;

    invoke-virtual {v2, v5}, Lfbe;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v7}, Lfhz;->a(Ljava/lang/Exception;)I

    move-result v6

    invoke-static {v6}, Lfbd;->a(I)I

    move-result v2

    invoke-static {v6}, Lfbd;->b(I)Z

    move-result v6

    const-string v8, "PeopleService"

    const/4 v12, 0x3

    invoke-static {v8, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_9

    const-string v8, "PeopleSync"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "backoff: lb="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " bos="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " exp="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v8, v12}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    if-lez v3, :cond_b

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-eqz v6, :cond_b

    mul-int/lit8 v2, v2, 0x2

    move v3, v2

    :goto_d
    sget-object v2, Lfbd;->B:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v4, Lfhz;->a:Landroid/content/Context;

    const-string v6, "PeopleSync"

    const/4 v8, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "backoff="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v3, v6, v5, v8, v12}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v4, Lfhz;->b:Lfbe;

    invoke-virtual {v3, v5, v2}, Lfbe;->a(Ljava/lang/String;I)V

    if-lez v2, :cond_a

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    int-to-long v5, v2

    add-long/2addr v3, v5

    const-string v5, "PeopleSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Delaying "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-double v12, v2

    const-wide v15, 0x40ac200000000000L    # 3600.0

    div-double/2addr v12, v15

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " hours"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p4

    iget-wide v5, v0, Landroid/content/SyncResult;->delayUntil:J

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    move-object/from16 v0, p4

    iput-wide v2, v0, Landroid/content/SyncResult;->delayUntil:J

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    const/4 v6, 0x4

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p2

    move-object/from16 v8, v19

    invoke-virtual/range {v2 .. v11}, Lfhz;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLfeu;)V

    goto/16 :goto_8

    :catch_6
    move-exception v7

    move-wide/from16 v9, v17

    move v14, v4

    :goto_e
    const-string v2, "PeopleSync"

    const-string v3, "ContactsProvider operation failed"

    invoke-static {v2, v3, v7}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const/4 v2, 0x1

    move-object/from16 v0, p4

    iput-boolean v2, v0, Landroid/content/SyncResult;->databaseError:Z

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "CP2 error"

    invoke-static/range {v2 .. v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    const/4 v6, 0x5

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p2

    move-object/from16 v8, v19

    invoke-virtual/range {v2 .. v11}, Lfhz;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLfeu;)V

    goto/16 :goto_8

    :catch_7
    move-exception v7

    move-wide/from16 v9, v17

    move v14, v4

    :goto_f
    const-string v2, "PeopleSync"

    const-string v3, "Database operation failed"

    invoke-static {v2, v3, v7}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v2, 0x1

    move-object/from16 v0, p4

    iput-boolean v2, v0, Landroid/content/SyncResult;->databaseError:Z

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "DB error"

    invoke-static/range {v2 .. v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    const/4 v6, 0x6

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p2

    move-object/from16 v8, v19

    invoke-virtual/range {v2 .. v11}, Lfhz;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLfeu;)V

    goto/16 :goto_8

    :catch_8
    move-exception v7

    move-wide/from16 v9, v17

    :goto_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    const/4 v6, 0x2

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v8, v19

    invoke-virtual/range {v2 .. v11}, Lfhz;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLfeu;)V

    throw v7

    :catch_9
    move-exception v7

    move-wide/from16 v9, v17

    move v14, v4

    :goto_11
    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Unknown error"

    invoke-static/range {v2 .. v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfhw;->b:Lfhz;

    const/16 v6, 0x64

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p2

    move-object/from16 v8, v19

    invoke-virtual/range {v2 .. v11}, Lfhz;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLfeu;)V

    invoke-virtual/range {p0 .. p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v7, v3}, Lfjv;->a(Landroid/content/Context;Ljava/lang/RuntimeException;Ljava/lang/String;)V

    goto/16 :goto_8

    :catch_a
    move-exception v2

    goto/16 :goto_1

    :catch_b
    move-exception v7

    move-wide/from16 v9, v17

    move-object v11, v13

    goto :goto_11

    :catch_c
    move-exception v7

    move-object v11, v13

    goto :goto_11

    :catch_d
    move-exception v7

    move v14, v4

    goto :goto_11

    :catch_e
    move-exception v7

    move-wide/from16 v9, v17

    move-object v11, v13

    move v4, v14

    goto :goto_10

    :catch_f
    move-exception v7

    move-object v11, v13

    move v4, v14

    goto :goto_10

    :catch_10
    move-exception v7

    goto :goto_10

    :catch_11
    move-exception v7

    move-wide/from16 v9, v17

    move-object v11, v13

    goto/16 :goto_f

    :catch_12
    move-exception v7

    move-object v11, v13

    goto/16 :goto_f

    :catch_13
    move-exception v7

    move v14, v4

    goto/16 :goto_f

    :catch_14
    move-exception v7

    move-wide/from16 v9, v17

    move-object v11, v13

    goto/16 :goto_e

    :catch_15
    move-exception v7

    move-object v11, v13

    goto/16 :goto_e

    :catch_16
    move-exception v7

    move v14, v4

    goto/16 :goto_e

    :catch_17
    move-exception v7

    move-wide/from16 v9, v17

    move-object v11, v13

    goto/16 :goto_c

    :catch_18
    move-exception v7

    move-object v11, v13

    goto/16 :goto_c

    :catch_19
    move-exception v7

    move v14, v4

    goto/16 :goto_c

    :catch_1a
    move-exception v7

    move-wide/from16 v9, v17

    goto/16 :goto_7

    :catchall_4
    move-exception v2

    move-object v3, v2

    move/from16 v4, v16

    move v2, v15

    goto/16 :goto_5

    :catchall_5
    move-exception v2

    move/from16 v4, v16

    move v14, v3

    move-object v3, v2

    move v2, v15

    goto/16 :goto_5

    :catchall_6
    move-exception v2

    move-object v3, v2

    move v14, v4

    move/from16 v4, v16

    move v2, v15

    goto/16 :goto_5

    :catchall_7
    move-exception v2

    move-object v3, v2

    move-object v13, v11

    move v14, v4

    move v2, v15

    move/from16 v4, v16

    goto/16 :goto_5

    :catch_1b
    move-exception v2

    move v14, v3

    goto/16 :goto_9

    :catch_1c
    move-exception v2

    move v14, v4

    goto/16 :goto_9

    :catch_1d
    move-exception v2

    move-object v13, v11

    move v14, v4

    goto/16 :goto_9

    :catch_1e
    move-exception v2

    move-object v11, v13

    move v4, v3

    goto/16 :goto_4

    :catch_1f
    move-exception v2

    move-object v11, v13

    goto/16 :goto_4

    :catch_20
    move-exception v2

    goto/16 :goto_4

    :cond_b
    move v3, v2

    goto/16 :goto_d

    :cond_c
    move-object v11, v13

    goto/16 :goto_b

    :cond_d
    move v4, v14

    goto/16 :goto_a

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/accounts/Account;Z)V
    .locals 5

    iget-object v0, p0, Lfhw;->b:Lfhz;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfhz;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v3

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    if-eqz p2, :cond_0

    const/16 v1, 0x80

    :goto_1
    invoke-virtual {v3, v4, v0, v1}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    const/16 v1, 0x40

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v0

    invoke-virtual {v0}, Lffh;->b()V

    return-void
.end method

.method private a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)Z
    .locals 13

    const-string v0, "PeopleSync"

    const-string v1, "onPerformSync()"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lfbo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfhw;->b:Lfhz;

    const-string v1, "Disabling sync for restricted user."

    invoke-virtual {v0, p1, v1}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-object v0, p0, Lfhw;->b:Lfhz;

    const-string v1, "com.google.android.gms.people"

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;I)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfhw;->b:Lfhz;

    invoke-static {p2}, Lfhz;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lfhw;->b:Lfhz;

    invoke-static {p2}, Lfhz;->c(Landroid/os/Bundle;)I

    move-result v3

    :try_start_0
    iget-object v0, p0, Lfhw;->a:Lfia;

    invoke-virtual {v0}, Lfia;->b()V
    :try_end_0
    .catch Lfic; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->g()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lfic; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    iget-object v0, p0, Lfhw;->b:Lfhz;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lfhz;->a(Z)Z

    if-eqz p2, :cond_5

    const-string v0, "initialize"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_6

    iget-object v1, p0, Lfhw;->b:Lfhz;

    iget-object v0, v1, Lfhz;->a:Landroid/content/Context;

    const-string v2, "PeopleSync"

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "initializeSyncState"

    invoke-static {v0, v2, v4, v5, v6}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.people"

    iget-object v2, v1, Lfhz;->a:Landroid/content/Context;

    invoke-static {v2}, Lfhn;->a(Landroid/content/Context;)Lfhn;

    invoke-static {p1, v0}, Lfhn;->b(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "initializeSyncState: syncable="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    if-gez v0, :cond_3

    invoke-static {}, Lfdk;->a()Z

    const-string v0, "com.google.android.gms.people"

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v0, "com.google.android.gms.people"

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    invoke-virtual {v1, p1}, Lfhz;->a(Landroid/accounts/Account;)V

    sget-object v0, Lfbd;->H:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PeopleSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Expediting sync for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "force"

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "expedited"

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "com.google.android.gms.people"

    invoke-virtual {v1, p1, v2, v0}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lfic;

    invoke-direct {v0}, Lfic;-><init>()V

    throw v0
    :try_end_2
    .catch Lfic; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PeopleSync"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v4, "Sync canceled"

    invoke-static {v0, v1, v2, v4}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    :try_start_3
    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v2}, Lfgy;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v5, 0x1

    :goto_3
    if-eqz p2, :cond_9

    const-string v0, "gms.people.contacts_sync"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v6, 0x1

    :goto_4
    if-eqz p2, :cond_a

    const-string v0, "page_only"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v7, 0x1

    :goto_5
    if-eqz p2, :cond_b

    const-string v0, "gms.people.skip_main_sync"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v8, 0x1

    :goto_6
    const/4 v9, 0x0

    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "PeopleSync"

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Sync start: feed="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " cannotHavePeople="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mode="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " contactOnly="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " pageOnly="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " skipMain="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0, v2, v4, v10, v11}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfhw;->b:Lfhz;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lfhz;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lfhw;->b:Lfhz;

    invoke-virtual {v0, p1}, Lfhz;->a(Landroid/accounts/Account;)V

    :cond_7
    iget-object v0, p0, Lfhw;->a:Lfia;

    invoke-virtual {v0}, Lfia;->c()V

    if-nez v3, :cond_e

    iget-object v2, p0, Lfhw;->b:Lfhz;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v0, Lfbd;->q:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v9, v0

    const/4 v0, 0x0

    invoke-virtual {v2, v4, v0, v9, v10}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {v2, v4, v9, v10}, Lfhz;->a(Ljava/lang/String;J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lfhw;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PeopleSync"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Data still fresh; skip periodic sync."

    invoke-static {v0, v1, v2, v4, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_4

    :cond_a
    const/4 v7, 0x0

    goto/16 :goto_5

    :cond_b
    const/4 v8, 0x0

    goto/16 :goto_6

    :cond_c
    const/4 v0, 0x0

    goto :goto_7

    :cond_d
    iget-object v2, p0, Lfhw;->b:Lfhz;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v9, 0x0

    sget-object v0, Lfbd;->q:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v10, v0

    invoke-virtual {v2, v4, v9, v10, v11}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v9

    :cond_e
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v11

    const-string v0, "PeopleSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Starting sync, feed="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v4, p3

    invoke-direct/range {v0 .. v10}, Lfhw;->a(Landroid/accounts/Account;Landroid/os/Bundle;ILandroid/content/SyncResult;ZZZZZLfhx;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v11

    const-string v2, "PeopleSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sync finished, duration: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v0, v1, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Lfic; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfhw;->a(Z)V

    :try_start_0
    invoke-direct {p0, p1, p2, p5}, Lfhw;->a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v1}, Lfhw;->a(Z)V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v1}, Lfhw;->a(Z)V

    throw v0
.end method

.method public final onSyncCanceled()V
    .locals 1

    iget-object v0, p0, Lfhw;->a:Lfia;

    invoke-virtual {v0}, Lfia;->a()V

    invoke-super {p0}, Lbob;->onSyncCanceled()V

    return-void
.end method
