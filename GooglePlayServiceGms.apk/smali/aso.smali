.class public final enum Laso;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Laso;

.field public static final enum B:Laso;

.field public static final enum C:Laso;

.field public static final enum D:Laso;

.field public static final enum E:Laso;

.field public static final enum F:Laso;

.field public static final enum G:Laso;

.field public static final enum H:Laso;

.field public static final enum I:Laso;

.field public static final enum J:Laso;

.field public static final enum K:Laso;

.field public static final enum L:Laso;

.field public static M:Ljava/lang/String;

.field public static N:Ljava/lang/String;

.field private static final synthetic P:[Laso;

.field public static final enum a:Laso;

.field public static final enum b:Laso;

.field public static final enum c:Laso;

.field public static final enum d:Laso;

.field public static final enum e:Laso;

.field public static final enum f:Laso;

.field public static final enum g:Laso;

.field public static final enum h:Laso;

.field public static final enum i:Laso;

.field public static final enum j:Laso;

.field public static final enum k:Laso;

.field public static final enum l:Laso;

.field public static final enum m:Laso;

.field public static final enum n:Laso;

.field public static final enum o:Laso;

.field public static final enum p:Laso;

.field public static final enum q:Laso;

.field public static final enum r:Laso;

.field public static final enum s:Laso;

.field public static final enum t:Laso;

.field public static final enum u:Laso;

.field public static final enum v:Laso;

.field public static final enum w:Laso;

.field public static final enum x:Laso;

.field public static final enum y:Laso;

.field public static final enum z:Laso;


# instance fields
.field private final O:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Laso;

    const-string v1, "SUCCESS"

    const-string v2, "Ok"

    invoke-direct {v0, v1, v4, v2}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->a:Laso;

    new-instance v0, Laso;

    const-string v1, "BAD_AUTHENTICATION"

    const-string v2, "BadAuthentication"

    invoke-direct {v0, v1, v5, v2}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->b:Laso;

    new-instance v0, Laso;

    const-string v1, "NEEDS_2F"

    const-string v2, "InvalidSecondFactor"

    invoke-direct {v0, v1, v6, v2}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->c:Laso;

    new-instance v0, Laso;

    const-string v1, "NOT_VERIFIED"

    const-string v2, "NotVerified"

    invoke-direct {v0, v1, v7, v2}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->d:Laso;

    new-instance v0, Laso;

    const-string v1, "TERMS_NOT_AGREED"

    const-string v2, "TermsNotAgreed"

    invoke-direct {v0, v1, v8, v2}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->e:Laso;

    new-instance v0, Laso;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "Unknown"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->f:Laso;

    new-instance v0, Laso;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x6

    const-string v3, "UNKNOWN_ERR"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->g:Laso;

    new-instance v0, Laso;

    const-string v1, "ACCOUNT_DELETED"

    const/4 v2, 0x7

    const-string v3, "AccountDeleted"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->h:Laso;

    new-instance v0, Laso;

    const-string v1, "ACCOUNT_DISABLED"

    const/16 v2, 0x8

    const-string v3, "AccountDisabled"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->i:Laso;

    new-instance v0, Laso;

    const-string v1, "SERVICE_DISABLED"

    const/16 v2, 0x9

    const-string v3, "ServiceDisabled"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->j:Laso;

    new-instance v0, Laso;

    const-string v1, "SERVICE_UNAVAILABLE"

    const/16 v2, 0xa

    const-string v3, "ServiceUnavailable"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->k:Laso;

    new-instance v0, Laso;

    const-string v1, "CAPTCHA"

    const/16 v2, 0xb

    const-string v3, "CaptchaRequired"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->l:Laso;

    new-instance v0, Laso;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0xc

    const-string v3, "NetworkError"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->m:Laso;

    new-instance v0, Laso;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0xd

    const-string v3, "UserCancel"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->n:Laso;

    new-instance v0, Laso;

    const-string v1, "PERMISSION_DENIED"

    const/16 v2, 0xe

    const-string v3, "PermissionDenied"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->o:Laso;

    new-instance v0, Laso;

    const-string v1, "DEVICE_MANAGEMENT_REQUIRED"

    const/16 v2, 0xf

    const-string v3, "DeviceManagementRequiredOrSyncDisabled"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->p:Laso;

    new-instance v0, Laso;

    const-string v1, "CLIENT_LOGIN_DISABLED"

    const/16 v2, 0x10

    const-string v3, "ClientLoginDisabled"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->q:Laso;

    new-instance v0, Laso;

    const-string v1, "NEED_PERMISSION"

    const/16 v2, 0x11

    const-string v3, "NeedPermission"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->r:Laso;

    new-instance v0, Laso;

    const-string v1, "BAD_PASSWORD"

    const/16 v2, 0x12

    const-string v3, "WeakPassword"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->s:Laso;

    new-instance v0, Laso;

    const-string v1, "ALREADY_HAS_GMAIL"

    const/16 v2, 0x13

    const-string v3, "ALREADY_HAS_GMAIL"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->t:Laso;

    new-instance v0, Laso;

    const-string v1, "BAD_REQUEST"

    const/16 v2, 0x14

    const-string v3, "BadRequest"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->u:Laso;

    new-instance v0, Laso;

    const-string v1, "BAD_USERNAME"

    const/16 v2, 0x15

    const-string v3, "BadUsername"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->v:Laso;

    new-instance v0, Laso;

    const-string v1, "LOGIN_FAIL"

    const/16 v2, 0x16

    const-string v3, "LoginFail"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->w:Laso;

    new-instance v0, Laso;

    const-string v1, "NOT_LOGGED_IN"

    const/16 v2, 0x17

    const-string v3, "NotLoggedIn"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->x:Laso;

    new-instance v0, Laso;

    const-string v1, "NO_GMAIL"

    const/16 v2, 0x18

    const-string v3, "NoGmail"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->y:Laso;

    new-instance v0, Laso;

    const-string v1, "REQUEST_DENIED"

    const/16 v2, 0x19

    const-string v3, "RequestDenied"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->z:Laso;

    new-instance v0, Laso;

    const-string v1, "SERVER_ERROR"

    const/16 v2, 0x1a

    const-string v3, "ServerError"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->A:Laso;

    new-instance v0, Laso;

    const-string v1, "USERNAME_UNAVAILABLE"

    const/16 v2, 0x1b

    const-string v3, "UsernameUnavailable"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->B:Laso;

    new-instance v0, Laso;

    const-string v1, "DELETED_GMAIL"

    const/16 v2, 0x1c

    const-string v3, "DeletedGmail"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->C:Laso;

    new-instance v0, Laso;

    const-string v1, "SOCKET_TIMEOUT"

    const/16 v2, 0x1d

    const-string v3, "SocketTimeout"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->D:Laso;

    new-instance v0, Laso;

    const-string v1, "EXISTING_USERNAME"

    const/16 v2, 0x1e

    const-string v3, "ExistingUsername"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->E:Laso;

    new-instance v0, Laso;

    const-string v1, "NEEDS_BROWSER"

    const/16 v2, 0x1f

    const-string v3, "NeedsBrowser"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->F:Laso;

    new-instance v0, Laso;

    const-string v1, "GPLUS_OTHER"

    const/16 v2, 0x20

    const-string v3, "GPlusOther"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->G:Laso;

    new-instance v0, Laso;

    const-string v1, "GPLUS_NICKNAME"

    const/16 v2, 0x21

    const-string v3, "GPlusNickname"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->H:Laso;

    new-instance v0, Laso;

    const-string v1, "GPLUS_INVALID_CHAR"

    const/16 v2, 0x22

    const-string v3, "GPlusInvalidChar"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->I:Laso;

    new-instance v0, Laso;

    const-string v1, "GPLUS_INTERSTITIAL"

    const/16 v2, 0x23

    const-string v3, "GPlusInterstitial"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->J:Laso;

    new-instance v0, Laso;

    const-string v1, "GPLUS_PROFILE_ERROR"

    const/16 v2, 0x24

    const-string v3, "ProfileUpgradeError"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->K:Laso;

    new-instance v0, Laso;

    const-string v1, "INVALID_SCOPE"

    const/16 v2, 0x25

    const-string v3, "INVALID_SCOPE"

    invoke-direct {v0, v1, v2, v3}, Laso;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Laso;->L:Laso;

    const/16 v0, 0x26

    new-array v0, v0, [Laso;

    sget-object v1, Laso;->a:Laso;

    aput-object v1, v0, v4

    sget-object v1, Laso;->b:Laso;

    aput-object v1, v0, v5

    sget-object v1, Laso;->c:Laso;

    aput-object v1, v0, v6

    sget-object v1, Laso;->d:Laso;

    aput-object v1, v0, v7

    sget-object v1, Laso;->e:Laso;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Laso;->f:Laso;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Laso;->g:Laso;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Laso;->h:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Laso;->i:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Laso;->j:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Laso;->k:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Laso;->l:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Laso;->m:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Laso;->n:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Laso;->o:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Laso;->p:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Laso;->q:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Laso;->r:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Laso;->s:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Laso;->t:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Laso;->u:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Laso;->v:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Laso;->w:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Laso;->x:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Laso;->y:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Laso;->z:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Laso;->A:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Laso;->B:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Laso;->C:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Laso;->D:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Laso;->E:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Laso;->F:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Laso;->G:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Laso;->H:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Laso;->I:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Laso;->J:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Laso;->K:Laso;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Laso;->L:Laso;

    aput-object v2, v0, v1

    sput-object v0, Laso;->P:[Laso;

    const-string v0, "Error"

    sput-object v0, Laso;->M:Ljava/lang/String;

    const-string v0, "status"

    sput-object v0, Laso;->N:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Laso;->O:Ljava/lang/String;

    return-void
.end method

.method public static final a(Ljava/lang/String;)Laso;
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Laso;->values()[Laso;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    iget-object v5, v0, Laso;->O:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Laso;
    .locals 1

    const-class v0, Laso;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laso;

    return-object v0
.end method

.method public static values()[Laso;
    .locals 1

    sget-object v0, Laso;->P:[Laso;

    invoke-virtual {v0}, [Laso;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laso;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Laso;->O:Ljava/lang/String;

    return-object v0
.end method
