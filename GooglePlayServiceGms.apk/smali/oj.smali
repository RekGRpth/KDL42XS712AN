.class final Loj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field public final a:Landroid/os/Messenger;

.field public final b:I

.field public c:Lny;

.field final synthetic d:Loh;

.field private final e:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>(Loh;Landroid/os/Messenger;I)V
    .locals 1

    iput-object p1, p0, Loj;->d:Loh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Loj;->e:Landroid/util/SparseArray;

    iput-object p2, p0, Loj;->a:Landroid/os/Messenger;

    iput p3, p0, Loj;->b:I

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Loj;->a:Landroid/os/Messenger;

    invoke-virtual {v1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Loj;->binderDied()V

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 2

    iget-object v0, p0, Loj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lod;

    if-eqz v0, :cond_0

    iget-object v1, p0, Loj;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v0}, Lod;->a()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Messenger;)Z
    .locals 2

    iget-object v0, p0, Loj;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 2

    iget-object v0, p0, Loj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Loj;->d:Loh;

    invoke-static {v0}, Loh;->a(Loh;)Lnz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lnz;->a(Ljava/lang/String;)Lod;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Loj;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lny;)Z
    .locals 1

    iget-object v0, p0, Loj;->c:Lny;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Loj;->c:Lny;

    if-eqz v0, :cond_0

    iget-object v0, p0, Loj;->c:Lny;

    invoke-virtual {v0, p1}, Lny;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object p1, p0, Loj;->c:Lny;

    iget-object v0, p0, Loj;->d:Loh;

    invoke-static {v0}, Loh;->b(Loh;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lod;
    .locals 1

    iget-object v0, p0, Loj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lod;

    return-object v0
.end method

.method public final b()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Loj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Loj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lod;

    invoke-virtual {v0}, Lod;->a()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Loj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Loj;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Loj;->a(Lny;)Z

    return-void
.end method

.method public final binderDied()V
    .locals 3

    iget-object v0, p0, Loj;->d:Loh;

    invoke-static {v0}, Loh;->c(Loh;)Lok;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Loj;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lok;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Loj;->a:Landroid/os/Messenger;

    invoke-static {v0}, Loh;->a(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
