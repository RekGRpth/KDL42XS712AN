.class public final Ldrn;
.super Lbob;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:Landroid/os/Bundle;

.field private static final e:Landroid/os/Bundle;

.field private static f:Ldro;


# instance fields
.field private final g:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "feed"

    aput-object v1, v0, v2

    sput-object v0, Ldrn;->a:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "http://games.google.com/sync/match/%s"

    aput-object v1, v0, v3

    const-string v1, "http://games.google.com/sync/request/%s"

    aput-object v1, v0, v2

    sput-object v0, Ldrn;->b:[Ljava/lang/String;

    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "com.google.android.gms.games.notification"

    aput-object v1, v0, v3

    sput-object v0, Ldrn;->c:[Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Ldrn;->d:Landroid/os/Bundle;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Ldrn;->e:Landroid/os/Bundle;

    const-string v1, "force"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v0, Ldro;

    invoke-direct {v0}, Ldro;-><init>()V

    sput-object v0, Ldrn;->f:Ldro;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "com.google.android.gms.games.background"

    invoke-direct {p0, p1, v0}, Lbob;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Ldrn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldrn;->g:Landroid/content/Context;

    return-void
.end method

.method public static a()V
    .locals 2

    const-string v0, "GamesSyncAdapter"

    const-string v1, "Forced tickle syncs are deprecated, ignoring request."

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .locals 2

    const/4 v1, 0x1

    invoke-static {p0, p1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    invoke-static {p0, p1, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    :cond_1
    invoke-static {p0, p1, v1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-static {p0, p1, v0}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public static a(Landroid/accounts/Account;Z)V
    .locals 1

    const-string v0, "com.google.android.gms.games.background"

    invoke-static {p0, v0, p1}, Ldrn;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    const-string v0, "com.google.android.gms.games"

    invoke-static {p0, v0, p1}, Ldrn;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ldrn;->d:Landroid/os/Bundle;

    invoke-static {v0, v1}, Ldrn;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    invoke-static {p0, p1}, Ldrn;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Landroid/accounts/Account;)Z
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    sget-object v3, Lbje;->e:[Ljava/lang/String;

    invoke-virtual {v0, p1, v3, v4, v4}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    const-wide/16 v3, 0x3c

    :try_start_0
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v3, v4, v5}, Landroid/accounts/AccountManagerFuture;->getResult(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    return v0

    :catch_0
    move-exception v0

    const-string v3, "GamesSyncAdapter"

    const-string v4, "Authenticator error checking account"

    invoke-static {v3, v4, v0}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v3, "GamesSyncAdapter"

    const-string v4, "Operation canceled error checking account"

    invoke-static {v3, v4, v0}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v3, "GamesSyncAdapter"

    const-string v4, "IO error checking account"

    invoke-static {v3, v4, v0}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;)Z
    .locals 14

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {}, Lbox;->a()J

    move-result-wide v8

    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-gtz v0, :cond_0

    const-string v0, "GamesSyncAdapter"

    const-string v1, "Unable to retrieve ID, failed to register for notifications."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    :goto_0
    return v0

    :cond_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lhih;->a:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "authority=?"

    sget-object v2, Ldrn;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lhih;->a:Landroid/net/Uri;

    sget-object v2, Ldrn;->a:[Ljava/lang/String;

    const-string v3, "_sync_account=? AND authority=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object v11, v4, v6

    const-string v5, "com.google.android.gms.games"

    aput-object v5, v4, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    sget-object v1, Ldrn;->b:[Ljava/lang/String;

    array-length v2, v1

    move v1, v6

    :goto_2
    if-ge v1, v2, :cond_3

    sget-object v3, Ldrn;->b:[Ljava/lang/String;

    aget-object v3, v3, v1

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v12, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "_sync_account"

    invoke-virtual {v4, v5, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "_sync_account_type"

    const-string v13, "com.google"

    invoke-virtual {v4, v5, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "feed"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "service"

    const-string v5, "games"

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "authority"

    const-string v5, "com.google.android.gms.games"

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lhih;->a:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    invoke-virtual {v12}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v3, Lhih;->a:Landroid/net/Uri;

    invoke-virtual {v12, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_4
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    :try_start_1
    sget-object v1, Lhih;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_5
    move v0, v7

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :catch_1
    move-exception v0

    const-string v1, "GamesSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error applying batch operation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    goto/16 :goto_0
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "feed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Ldrn;->g:Landroid/content/Context;

    invoke-static {}, Lbox;->a()J

    move-result-wide v1

    const-string v3, "feed"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "http://games.google.com/sync/match/%s"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Lcun;Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Z
    .locals 10

    const-wide/16 v8, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz p3, :cond_0

    const-string v0, "initialize"

    invoke-virtual {p3, v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2, v7}, Ldrn;->a(Landroid/accounts/Account;Z)V

    move v7, v6

    :goto_0
    return v7

    :cond_0
    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lbov;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GamesSyncAdapter"

    const-string v1, "Sync for NON-EXISTANT ACCOUNT"

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "com.google.android.gms.games"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p3}, Ldrn;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p3}, Ldrn;->b(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "GamesSyncAdapter"

    const-string v1, "Syncing notifications without tickle; exiting"

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2}, Ldrn;->a(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "GamesSyncAdapter"

    const-string v1, "User is not G+ enabled. Aborting sync"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcum;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    :try_start_0
    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v0, v2}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "GamesSyncAdapter"

    const-string v1, "Failed revision check during sync. Your version of Google Play services is out of date."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v8

    iput-wide v3, v0, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GamesSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth error executing an operation: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v8

    iput-wide v3, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    move v0, v7

    :goto_1
    iget-object v1, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v1, v2}, Lcun;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Leej;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v4, Ldeu;

    iget-object v5, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v5, v2}, Lcun;->d(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v5

    invoke-direct {v4, v5}, Ldeu;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_1
    iget-object v5, p0, Ldrn;->g:Landroid/content/Context;

    invoke-static {v5, v2, v3, v1, v4}, Ldrc;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldeu;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v4}, Ldeu;->b()V

    iget-object v1, p0, Ldrn;->g:Landroid/content/Context;

    invoke-static {v1, p2}, Ldrn;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    move v7, v0

    goto/16 :goto_0

    :cond_4
    :try_start_2
    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v0, v2}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-direct {p0, p3}, Ldrn;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Ldrn;->g:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p1

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLandroid/content/SyncResult;)I

    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v0, v2, p5}, Lcun;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V

    :goto_2
    move v0, v6

    goto :goto_1

    :cond_5
    invoke-direct {p0, p3}, Ldrn;->b(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v1, p0, Ldrn;->g:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p1

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLandroid/content/SyncResult;)I

    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v0, v2, p5}, Lcun;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    :try_end_2
    .catch Lamq; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ldqq; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v1, "GamesSyncAdapter"

    invoke-virtual {v0}, Ldqq;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v7

    goto :goto_1

    :cond_6
    :try_start_3
    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v0, v2, p5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V

    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v0, v2, p5}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V

    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v0, v2, p5}, Lcun;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V

    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-virtual {p1, v0, v2, p5}, Lcun;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    :try_end_3
    .catch Lamq; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ldqq; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Ldeu;->b()V

    throw v0
.end method

.method static synthetic b()Landroid/os/Bundle;
    .locals 1

    sget-object v0, Ldrn;->d:Landroid/os/Bundle;

    return-object v0
.end method

.method public static b(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    sget-object v0, Ldrn;->f:Ldro;

    invoke-virtual {v0, p0}, Ldro;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    return-void
.end method

.method private static b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    if-eqz p0, :cond_0

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.background"

    invoke-static {v0, v1, p1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private b(Landroid/os/Bundle;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "feed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Ldrn;->g:Landroid/content/Context;

    invoke-static {}, Lbox;->a()J

    move-result-wide v1

    const-string v3, "feed"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "http://games.google.com/sync/request/%s"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Ldrn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GamesSyncAdapter"

    const-string v1, "In restricted profile; skipping sync."

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Starting sync for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldac;->b()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldrn;->a(Z)V

    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    invoke-static {v0}, Lcun;->a(Landroid/content/Context;)Lcun;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    :try_start_0
    invoke-direct/range {v0 .. v5}, Ldrn;->a(Lcun;Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    invoke-virtual {v1}, Lcun;->a()V

    invoke-virtual {p0, v9}, Ldrn;->a(Z)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v4, v0, v7

    const-string v0, "feed"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "feed"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numIoExceptions:J

    iget-object v2, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v7, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v0, v7

    long-to-int v7, v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sync duration for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-static {}, Ldac;->b()V

    iget-object v0, p0, Ldrn;->g:Landroid/content/Context;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v2, p3

    invoke-static/range {v0 .. v7}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZI)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcun;->a()V

    invoke-virtual {p0, v9}, Ldrn;->a(Z)V

    throw v0

    :cond_1
    const-string v3, ""

    goto :goto_1
.end method
