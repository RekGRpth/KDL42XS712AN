.class public final Ldcn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lctw;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbdu;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->p()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public final a(Lbdu;I)Lbeh;
    .locals 1

    new-instance v0, Ldcs;

    invoke-direct {v0, p0, p2}, Ldcs;-><init>(Ldcn;I)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;IZ)Lbeh;
    .locals 1

    new-instance v0, Ldcr;

    invoke-direct {v0, p0, p2, p3}, Ldcr;-><init>(Ldcn;IZ)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;I)Lbeh;
    .locals 2

    new-instance v0, Ldco;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, p3, v1}, Ldco;-><init>(Ldcn;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;Z)Lbeh;
    .locals 2

    new-instance v0, Ldct;

    const/4 v1, 0x4

    invoke-direct {v0, p0, p2, v1, p3}, Ldct;-><init>(Ldcn;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/util/ArrayList;)Lbeh;
    .locals 2

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    new-instance v1, Ldcq;

    invoke-direct {v1, p0, v0}, Ldcq;-><init>(Ldcn;[Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Z)Lbeh;
    .locals 1

    new-instance v0, Ldcu;

    invoke-direct {v0, p0, p2}, Ldcu;-><init>(Ldcn;Z)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbdu;Ljava/lang/String;I)Lbeh;
    .locals 1

    new-instance v0, Ldcp;

    invoke-direct {v0, p0, p2, p3}, Ldcp;-><init>(Ldcn;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbdu;)Lcom/google/android/gms/games/Player;
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->k()Lcom/google/android/gms/games/Player;

    move-result-object v0

    return-object v0
.end method
