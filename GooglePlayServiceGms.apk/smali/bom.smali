.class public final Lbom;
.super Lbon;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbol;

.field private final c:[Lbon;

.field private final d:I


# direct methods
.method public varargs constructor <init>(Lbol;[Lbon;)V
    .locals 3

    const/4 v0, 0x0

    iput-object p1, p0, Lbom;->a:Lbol;

    invoke-direct {p0, p1}, Lbon;-><init>(Lbol;)V

    iput-object p2, p0, Lbom;->c:[Lbon;

    move v1, v0

    :goto_0
    iget-object v2, p0, Lbom;->c:[Lbon;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lbom;->c:[Lbon;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbom;->c:[Lbon;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lbon;->a()I

    move-result v2

    add-int/2addr v1, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iput v1, p0, Lbom;->d:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lbom;->d:I

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbom;->c:[Lbon;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lbon;->a()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lbon;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lbon;->a()I

    move-result v1

    sub-int/2addr p1, v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad item index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbom;->c:[Lbon;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lbon;->a()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Lbon;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lbon;->a()I

    move-result v1

    sub-int/2addr p1, v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad item index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final a(Ljava/util/ArrayList;I)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbom;->c:[Lbon;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lbon;->a(Ljava/util/ArrayList;I)V

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lbon;->a()I

    move-result v1

    add-int/2addr p2, v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b(I)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbom;->c:[Lbon;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lbon;->a()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Lbon;->b(I)I

    move-result v0

    return v0

    :cond_0
    iget-object v1, p0, Lbom;->c:[Lbon;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lbon;->a()I

    move-result v1

    sub-int/2addr p1, v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad item index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
