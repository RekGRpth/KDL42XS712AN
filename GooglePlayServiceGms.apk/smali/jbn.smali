.class public final Ljbn;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z

.field public c:I

.field private d:Z

.field private e:Ljbu;

.field private f:Ljava/util/List;

.field private g:Z

.field private h:Ljbu;

.field private i:Z

.field private j:Z

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-object v2, p0, Ljbn;->e:Ljbu;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljbn;->f:Ljava/util/List;

    iput-object v2, p0, Ljbn;->h:Ljbu;

    iput v1, p0, Ljbn;->a:I

    iput v1, p0, Ljbn;->c:I

    const/4 v0, 0x2

    iput v0, p0, Ljbn;->k:I

    const/4 v0, -0x1

    iput v0, p0, Ljbn;->l:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljbn;->l:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljbn;->b()I

    :cond_0
    iget v0, p0, Ljbn;->l:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbn;->d:Z

    iput-object v0, p0, Ljbn;->e:Ljbu;

    goto :goto_0

    :sswitch_2
    new-instance v0, Ljbs;

    invoke-direct {v0}, Ljbs;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iget-object v1, p0, Ljbn;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ljbn;->f:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ljbn;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_3
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbn;->g:Z

    iput-object v0, p0, Ljbn;->h:Ljbu;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->c()I

    move-result v0

    iput-boolean v2, p0, Ljbn;->i:Z

    iput v0, p0, Ljbn;->a:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->c()I

    move-result v0

    iput-boolean v2, p0, Ljbn;->b:Z

    iput v0, p0, Ljbn;->c:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->c()I

    move-result v0

    iput-boolean v2, p0, Ljbn;->j:Z

    iput v0, p0, Ljbn;->k:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x318 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(I)Ljbs;
    .locals 1

    iget-object v0, p0, Ljbn;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbs;

    return-object v0
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Ljbn;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbn;->e:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-object v0, p0, Ljbn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbs;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILizk;)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Ljbn;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljbn;->h:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    iget-boolean v0, p0, Ljbn;->i:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Ljbn;->a:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_3
    iget-boolean v0, p0, Ljbn;->b:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Ljbn;->c:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_4
    iget-boolean v0, p0, Ljbn;->j:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x63

    iget v1, p0, Ljbn;->k:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_5
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljbn;->d:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbn;->e:Ljbu;

    invoke-static {v0, v1}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Ljbn;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbs;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Ljbn;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v2, p0, Ljbn;->h:Ljbu;

    invoke-static {v0, v2}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_2
    iget-boolean v0, p0, Ljbn;->i:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v2, p0, Ljbn;->a:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_3
    iget-boolean v0, p0, Ljbn;->b:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v2, p0, Ljbn;->c:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_4
    iget-boolean v0, p0, Ljbn;->j:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x63

    iget v2, p0, Ljbn;->k:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_5
    iput v1, p0, Ljbn;->l:I

    return v1
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Ljbn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
