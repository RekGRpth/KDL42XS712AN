.class public final Lgyj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V
    .locals 0

    iput-object p1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->R_()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;Z)V

    new-instance v0, Lipo;

    invoke-direct {v0}, Lipo;-><init>()V

    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lipv;

    move-result-object v1

    iget-object v1, v1, Lipv;->b:Ljava/lang/String;

    iput-object v1, v0, Lipo;->b:Ljava/lang/String;

    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    invoke-virtual {v1}, Lgvc;->a()Lixo;

    move-result-object v1

    iput-object v1, v0, Lipo;->c:Lixo;

    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    invoke-virtual {v1}, Lgvc;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    invoke-virtual {v1}, Lgvc;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lipo;->d:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lipo;->e:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lioq;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lioq;

    move-result-object v1

    iput-object v1, v0, Lipo;->a:Lioq;

    :cond_2
    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lipo;->c:Lixo;

    iget-object v2, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lixo;->t:Ljava/lang/String;

    :cond_3
    iget-object v1, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    iget-object v2, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z

    move-result v2

    invoke-interface {v1, v0, v2}, Lhca;->a(Lipo;Z)V

    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Lgyj;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->i()Z

    goto :goto_0
.end method
