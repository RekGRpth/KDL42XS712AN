.class public Lhvx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhvv;


# static fields
.field private static final b:Ljava/lang/Iterable;

.field private static c:Lhvx;

.field private static final d:Ljava/lang/Object;


# instance fields
.field public final a:Lhvp;

.field private final e:Landroid/os/HandlerThread;

.field private final f:Landroid/os/PowerManager$WakeLock;

.field private final g:Landroid/os/Handler;

.field private final h:Lilp;

.field private i:Z

.field private j:Landroid/location/Location;

.field private final k:[Lhwa;

.field private final l:[Ljava/lang/Iterable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lhvx;->b:Ljava/lang/Iterable;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lhvx;->d:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-class v1, Lhvx;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhvx;->e:Landroid/os/HandlerThread;

    iput-boolean v2, p0, Lhvx;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhvx;->j:Landroid/location/Location;

    new-array v0, v3, [Lhwa;

    iput-object v0, p0, Lhvx;->k:[Lhwa;

    new-array v0, v3, [Ljava/lang/Iterable;

    sget-object v1, Lhvx;->b:Ljava/lang/Iterable;

    aput-object v1, v0, v2

    sget-object v1, Lhvx;->b:Ljava/lang/Iterable;

    aput-object v1, v0, v4

    iput-object v0, p0, Lhvx;->l:[Ljava/lang/Iterable;

    iget-object v0, p0, Lhvx;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    iget-object v0, p0, Lhvx;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v2, "GCoreFlp"

    invoke-virtual {v0, v4, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lhvx;->f:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lhvx;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iget-object v0, p0, Lhvx;->l:[Ljava/lang/Iterable;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lirg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lisc;

    invoke-direct {v2, v0}, Lisc;-><init>(Ljava/lang/Iterable;)V

    new-instance v0, Lhvz;

    invoke-direct {v0, p0, v1, v2}, Lhvz;-><init>(Lhvx;Landroid/os/Looper;Ljava/lang/Iterable;)V

    iput-object v0, p0, Lhvx;->g:Landroid/os/Handler;

    new-instance v0, Lilp;

    iget-object v2, p0, Lhvx;->f:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lhvx;->g:Landroid/os/Handler;

    invoke-direct {v0, v2, v3}, Lilp;-><init>(Landroid/os/PowerManager$WakeLock;Landroid/os/Handler;)V

    iput-object v0, p0, Lhvx;->h:Lilp;

    new-instance v0, Lhvp;

    invoke-direct {v0, p1, v1, p0}, Lhvp;-><init>(Landroid/content/Context;Landroid/os/Looper;Lhvv;)V

    iput-object v0, p0, Lhvx;->a:Lhvp;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lhvy;

    iget-object v3, p0, Lhvx;->g:Landroid/os/Handler;

    invoke-direct {v2, p0, v3, p1}, Lhvy;-><init>(Lhvx;Landroid/os/Handler;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method static synthetic a(Lhvx;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    iput-object p1, p0, Lhvx;->j:Landroid/location/Location;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Lhvx;
    .locals 2

    sget-object v1, Lhvx;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhvx;->c:Lhvx;

    if-nez v0, :cond_0

    new-instance v0, Lhvx;

    invoke-direct {v0, p0}, Lhvx;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhvx;->c:Lhvx;

    :cond_0
    sget-object v0, Lhvx;->c:Lhvx;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(ILandroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lhvx;->k:[Lhwa;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lhwa;->a(Landroid/location/Location;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lhvx;)V
    .locals 1

    iget-boolean v0, p0, Lhvx;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvx;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhvx;->j:Landroid/location/Location;

    invoke-direct {p0}, Lhvx;->g()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lhvx;Landroid/location/Location;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lhvx;->a(ILandroid/location/Location;)V

    return-void
.end method

.method static synthetic b(Lhvx;)[Lhwa;
    .locals 1

    iget-object v0, p0, Lhvx;->k:[Lhwa;

    return-object v0
.end method

.method static synthetic c(Lhvx;)V
    .locals 0

    invoke-direct {p0}, Lhvx;->g()V

    return-void
.end method

.method static synthetic d(Lhvx;)[Ljava/lang/Iterable;
    .locals 1

    iget-object v0, p0, Lhvx;->l:[Ljava/lang/Iterable;

    return-object v0
.end method

.method static synthetic e(Lhvx;)Z
    .locals 1

    iget-boolean v0, p0, Lhvx;->i:Z

    return v0
.end method

.method static synthetic f()Ljava/lang/Iterable;
    .locals 1

    sget-object v0, Lhvx;->b:Ljava/lang/Iterable;

    return-object v0
.end method

.method static synthetic f(Lhvx;)V
    .locals 1

    iget-boolean v0, p0, Lhvx;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvx;->i:Z

    invoke-direct {p0}, Lhvx;->g()V

    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lhvx;->k:[Lhwa;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lhvx;->k:[Lhwa;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhvx;->i:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lhvx;->a:Lhvp;

    invoke-virtual {v0}, Lhvp;->a()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhvx;->a:Lhvp;

    invoke-virtual {v0}, Lhvp;->b()V

    goto :goto_0
.end method


# virtual methods
.method final a(Z)Landroid/location/Location;
    .locals 3

    iget-boolean v0, p0, Lhvx;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhvx;->j:Landroid/location/Location;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lhvx;->a:Lhvp;

    const-string v1, "GCoreFlp"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "getLastLocation() request in engine"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, v0, Lhvp;->g:Landroid/location/Location;

    :goto_1
    if-nez v0, :cond_0

    const-string v1, "GCoreFlp"

    const-string v2, "No location to return for getLastLocation()"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lhvp;->c:Landroid/location/Location;

    goto :goto_1
.end method

.method final a()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lhvx;->h:Lilp;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method final a(ILhwa;)V
    .locals 3

    iget-object v0, p0, Lhvx;->h:Lilp;

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, p1, v2, p2}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method final a(ILjava/lang/Iterable;Z)V
    .locals 3

    iget-object v1, p0, Lhvx;->h:Lilp;

    const/4 v2, 0x3

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, p1, v0, p2}, Lilp;->a(IIILjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    iget-boolean v0, p0, Lhvx;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lhvx;->a(ILandroid/location/Location;)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lhvx;->a(ILandroid/location/Location;)V

    return-void
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 3

    iget-object v0, p0, Lhvx;->h:Lilp;

    const/16 v1, 0x8

    const/4 v2, -0x1

    invoke-virtual {v0, v1, p2, v2, p1}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 8

    iget-object v0, p0, Lhvx;->a:Lhvp;

    iget-object v1, v0, Lhvp;->c:Landroid/location/Location;

    iget-object v2, v0, Lhvp;->g:Landroid/location/Location;

    iget-object v3, v0, Lhvp;->b:Landroid/location/Location;

    iget-object v4, v0, Lhvp;->d:Landroid/location/Location;

    iget-object v5, v0, Lhvp;->f:Landroid/location/Location;

    iget-object v6, v0, Lhvp;->n:Lhwb;

    invoke-virtual {v6, p1}, Lhwb;->a(Ljava/io/PrintWriter;)V

    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    iget-boolean v6, v0, Lhvp;->h:Z

    if-eqz v6, :cond_0

    const-string v6, "Fused Location Provider Is Enabled"

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Current State: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lhvp;->k:Liwp;

    invoke-virtual {v0}, Liwp;->b()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Fused "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Gps "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Network "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Fused Coarse Interval "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Network Coarse Interval "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v6, "Fused Location Provider Is Disabled"

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final b()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lhvx;->h:Lilp;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method final b(Landroid/location/Location;)V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lhvx;->h:Lilp;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2, v2, p1}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method final b(Z)V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lhvx;->h:Lilp;

    const/4 v1, 0x6

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v3, v2}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method final c()V
    .locals 5

    iget-object v0, p0, Lhvx;->h:Lilp;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method public final d()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lhvx;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method final e()Lilf;
    .locals 1

    iget-object v0, p0, Lhvx;->a:Lhvp;

    iget-object v0, v0, Lhvp;->m:Lhvw;

    return-object v0
.end method
