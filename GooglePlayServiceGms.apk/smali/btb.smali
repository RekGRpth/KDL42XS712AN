.class public Lbtb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lbtb;


# instance fields
.field private final b:Lcga;

.field private final c:Lbur;

.field private final d:Lcmq;

.field private final e:Lcfz;

.field private final f:Lbve;

.field private final g:Lcnf;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    new-instance v1, Lbur;

    invoke-direct {v1, v0}, Lbur;-><init>(Lcoy;)V

    iput-object v1, p0, Lbtb;->c:Lbur;

    new-instance v1, Lcga;

    invoke-direct {v1, v0}, Lcga;-><init>(Lcoy;)V

    iput-object v1, p0, Lbtb;->b:Lcga;

    new-instance v1, Lcmq;

    invoke-direct {v1, v0}, Lcmq;-><init>(Lcoy;)V

    iput-object v1, p0, Lbtb;->d:Lcmq;

    invoke-virtual {v0}, Lcoy;->f()Lcfz;

    move-result-object v1

    iput-object v1, p0, Lbtb;->e:Lcfz;

    invoke-virtual {v0}, Lcoy;->e()Lbve;

    move-result-object v1

    iput-object v1, p0, Lbtb;->f:Lbve;

    invoke-virtual {v0}, Lcoy;->r()Lcnf;

    move-result-object v0

    iput-object v0, p0, Lbtb;->g:Lcnf;

    iget-object v0, p0, Lbtb;->c:Lbur;

    iget-object v1, p0, Lbtb;->d:Lcmq;

    invoke-virtual {v0, v1}, Lbur;->a(Lbus;)V

    iget-object v0, p0, Lbtb;->c:Lbur;

    iget-object v1, p0, Lbtb;->g:Lcnf;

    invoke-virtual {v0, v1}, Lbur;->a(Lbus;)V

    iget-object v0, p0, Lbtb;->e:Lcfz;

    invoke-interface {v0}, Lcfz;->i()V

    new-instance v0, Lbtc;

    const-string v1, "Background initialization thread"

    invoke-direct {v0, p0, v1}, Lbtc;-><init>(Lbtb;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic a(Lbtb;)Lcga;
    .locals 1

    iget-object v0, p0, Lbtb;->b:Lcga;

    return-object v0
.end method

.method public static final a(Landroid/content/Context;)V
    .locals 2

    const-class v1, Lbtb;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbtb;->a:Lbtb;

    if-nez v0, :cond_0

    new-instance v0, Lbtb;

    invoke-direct {v0, p0}, Lbtb;-><init>(Landroid/content/Context;)V

    sput-object v0, Lbtb;->a:Lbtb;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lbtb;)Lcmq;
    .locals 1

    iget-object v0, p0, Lbtb;->d:Lcmq;

    return-object v0
.end method

.method static synthetic c(Lbtb;)Lbve;
    .locals 1

    iget-object v0, p0, Lbtb;->f:Lbve;

    return-object v0
.end method

.method static synthetic d(Lbtb;)Lcnf;
    .locals 1

    iget-object v0, p0, Lbtb;->g:Lcnf;

    return-object v0
.end method
