.class public final Lfop;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfug;


# instance fields
.field private a:Lftz;

.field private b:Lftx;

.field private c:Landroid/accounts/Account;

.field private d:Z

.field private e:Lfoq;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Lbbo;


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lfop;-><init>(Lftz;)V

    return-void
.end method

.method private constructor <init>(Lftz;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-object p1, p0, Lfop;->a:Lftz;

    return-void
.end method

.method public static a(Landroid/accounts/Account;)Lfop;
    .locals 3

    sget-object v0, Lftx;->a:Lftz;

    new-instance v1, Lfop;

    invoke-direct {v1, v0}, Lfop;-><init>(Lftz;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lfop;->g(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfop;->e:Lfoq;

    return-void
.end method

.method public final P_()V
    .locals 1

    iget-boolean v0, p0, Lfop;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfop;->b:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfop;->d:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfop;->d:Z

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfoq;

    if-eqz v0, :cond_0

    check-cast p1, Lfoq;

    iput-object p1, p0, Lfop;->e:Lfoq;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DeleteMomentFragment must be hosted by an activity that implements DeleteMomentCallbacks."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lbbo;)V
    .locals 4

    const/4 v3, 0x0

    iput-boolean v3, p0, Lfop;->d:Z

    iput-object p1, p0, Lfop;->h:Lbbo;

    iget-boolean v0, p0, Lfop;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfop;->e:Lfoq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfop;->e:Lfoq;

    iget-object v1, p0, Lfop;->g:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lfoq;->a(Ljava/lang/String;Z)V

    :cond_0
    iput-boolean v3, p0, Lfop;->f:Z

    return-void
.end method

.method public final a(Lbbo;Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    iput-object p1, p0, Lfop;->h:Lbbo;

    iget-boolean v0, p0, Lfop;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfop;->e:Lfoq;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lfop;->e:Lfoq;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-interface {v2, p2, v0}, Lfoq;->a(Ljava/lang/String;Z)V

    :cond_1
    iput-boolean v1, p0, Lfop;->f:Z

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lfop;->f:Z

    if-eqz v1, :cond_2

    const-string v0, "DeleteMomentFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DeleteMomentFragment"

    const-string v1, "Can only delete one app activity at a time."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :cond_2
    iput-boolean v0, p0, Lfop;->f:Z

    iput-object p1, p0, Lfop;->g:Ljava/lang/String;

    iget-object v1, p0, Lfop;->b:Lftx;

    invoke-interface {v1}, Lftx;->d_()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v1}, Lfop;->b_(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lfop;->d:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lfop;->b:Lftx;

    invoke-interface {v1}, Lftx;->a()V

    iput-boolean v0, p0, Lfop;->d:Z

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lfop;->q()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lfop;->c:Landroid/accounts/Account;

    iget-object v0, p0, Lfop;->a:Lftz;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lfop;->c:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1, p0, p0, v2}, Lfob;->a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lfop;->b:Lftx;

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfop;->d:Z

    iget-boolean v0, p0, Lfop;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfop;->b:Lftx;

    iget-object v1, p0, Lfop;->g:Ljava/lang/String;

    invoke-interface {v0, p0, v1}, Lftx;->a(Lfug;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final y()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    iget-object v0, p0, Lfop;->b:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfop;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfop;->b:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    iput-object v2, p0, Lfop;->b:Lftx;

    iput-boolean v1, p0, Lfop;->d:Z

    iput-boolean v1, p0, Lfop;->f:Z

    iput-object v2, p0, Lfop;->g:Ljava/lang/String;

    return-void
.end method
