.class public final Lhcl;
.super Lhct;
.source "SourceFile"


# instance fields
.field private final a:Lhct;

.field private final b:Lhcg;

.field private final c:Lgsi;


# direct methods
.method public constructor <init>(Lhct;Lhcg;Lgsi;)V
    .locals 0

    invoke-direct {p0}, Lhct;-><init>()V

    iput-object p1, p0, Lhcl;->a:Lhct;

    iput-object p2, p0, Lhcl;->b:Lhcg;

    iput-object p3, p0, Lhcl;->c:Lgsi;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;->b()Lios;

    move-result-object v2

    iget-object v0, p0, Lhcl;->b:Lhcg;

    invoke-virtual {v0, v1, v2}, Lhcg;->a(Ljava/lang/String;Lizs;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v3

    const/16 v4, 0x1b

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lhcl;->b:Lhcg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v4

    invoke-virtual {v3, v1, v2, v4}, Lhcg;->b(Ljava/lang/String;Lizs;Lizs;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    const/16 v2, 0x1d

    if-ne v1, v2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhcl;->b:Lhcg;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->b()Liou;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->b()Lioz;

    move-result-object v2

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/16 v3, 0xb

    if-eq v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    iget-object v3, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lipv;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhcl;->c:Lgsi;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lipv;

    invoke-virtual {v3, v4, v5, v6}, Lgsi;->a(Landroid/accounts/Account;ILipv;)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lhcl;->b:Lhcg;

    invoke-virtual {v4, v3, v2, v0}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->b()Lipa;

    move-result-object v2

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/4 v3, 0x7

    if-eq v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    iget-object v3, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhcl;->c:Lgsi;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-virtual {v3, v4, v5, v6}, Lgsi;->a(Landroid/accounts/Account;ILioj;)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lhcl;->b:Lhcg;

    invoke-virtual {v4, v3, v2, v0}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhcl;->b:Lhcg;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->b()Lipb;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->b()Lipk;

    move-result-object v2

    iget-object v0, p0, Lhcl;->b:Lhcg;

    invoke-virtual {v0, v1, v2}, Lhcg;->a(Ljava/lang/String;Lizs;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lhcl;->b:Lhcg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v4

    invoke-virtual {v3, v1, v2, v4}, Lhcg;->b(Ljava/lang/String;Lizs;Lizs;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhcl;->b:Lhcg;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;->b()Lipm;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 8

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->b()Lipo;

    move-result-object v2

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/16 v3, 0xc

    if-eq v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    iget-object v3, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lipv;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhcl;->c:Lgsi;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lipv;

    iget-object v7, v2, Lipo;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6, v7}, Lgsi;->a(Landroid/accounts/Account;ILipv;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lhcl;->b:Lhcg;

    invoke-virtual {v4, v3, v2, v0}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 8

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->b()Lipp;

    move-result-object v2

    iget-object v0, p0, Lhcl;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/16 v3, 0x9

    if-eq v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    iget-object v3, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lioj;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhcl;->c:Lgsi;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lioj;

    iget-object v7, v2, Lipp;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6, v7}, Lgsi;->a(Landroid/accounts/Account;ILioj;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lhcl;->b:Lhcg;

    invoke-virtual {v4, v3, v2, v0}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    move-object v0, v1

    goto :goto_0
.end method
