.class public final Lbtd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Z

.field private c:I

.field private final d:Lbtf;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lbtd;->c:I

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lbtd;->a:Ljava/util/Map;

    sget-object v0, Lbqs;->i:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lbtd;->b:Z

    new-instance v0, Lbte;

    invoke-direct {v0, v1}, Lbte;-><init>(B)V

    iput-object v0, p0, Lbtd;->d:Lbtf;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lbtd;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lbtd;->c:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lbtd;->d:Lbtf;

    invoke-interface {v0}, Lbtf;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 4

    if-eqz p2, :cond_0

    const-string v0, "referrer"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lbtd;->d:Lbtf;

    const-string v2, "referredFrom"

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v0, v3}, Lbtf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lbtd;->d:Lbtf;

    invoke-interface {v1, v0}, Lbtf;->b(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lbtd;->d:Lbtf;

    invoke-interface {v0, p1}, Lbtf;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    iget-object v0, p0, Lbtd;->d:Lbtf;

    invoke-interface {v0, p1, p2, p3, p4}, Lbtf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbtd;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lbtd;->c:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    :cond_0
    iget v0, p0, Lbtd;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbtd;->c:I

    iget v0, p0, Lbtd;->c:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lbtd;->d:Lbtf;

    invoke-interface {v0}, Lbtf;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
