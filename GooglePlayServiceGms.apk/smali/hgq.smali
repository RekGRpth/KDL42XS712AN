.class public final Lhgq;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Ljava/lang/String;)I
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "countryCode must have length of 2!"

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "UK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "GB"

    :cond_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    add-int/lit8 v0, v0, -0x41

    add-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x5

    add-int/lit8 v1, v1, -0x41

    add-int/lit8 v1, v1, 0x1

    or-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static final a(I)Ljava/lang/String;
    .locals 3

    if-eqz p0, :cond_0

    and-int/lit16 v0, p0, -0x400

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "ZZ"

    :goto_0
    return-object v0

    :cond_1
    and-int/lit16 v0, p0, 0x3e0

    shr-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x41

    add-int/lit8 v0, v0, -0x1

    int-to-char v0, v0

    and-int/lit8 v1, p0, 0x1f

    add-int/lit8 v1, v1, 0x41

    add-int/lit8 v1, v1, -0x1

    int-to-char v1, v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final b(Ljava/lang/String;)I
    .locals 1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-static {p0}, Lhgq;->a(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x35a

    goto :goto_0
.end method
