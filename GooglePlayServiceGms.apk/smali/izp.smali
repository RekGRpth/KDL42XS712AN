.class public abstract Lizp;
.super Lizs;
.source "SourceFile"


# instance fields
.field protected q:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lizs;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lizp;->q:Ljava/util/List;

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    move v2, v0

    move v3, v0

    :goto_1
    if-ge v2, v1, :cond_1

    iget-object v0, p0, Lizp;->q:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    iget v4, v0, Lizu;->a:I

    invoke-static {v4}, Lizn;->d(I)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v0, v0, Lizu;->b:[B

    array-length v0, v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lizp;->q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_1
    iput v3, p0, Lizp;->C:I

    return v3
.end method

.method public a(Lizn;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lizp;->q:Ljava/util/List;

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    move v2, v0

    :goto_1
    if-ge v2, v1, :cond_1

    iget-object v0, p0, Lizp;->q:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    iget v3, v0, Lizu;->a:I

    invoke-virtual {p1, v3}, Lizn;->c(I)V

    iget-object v0, v0, Lizu;->b:[B

    invoke-virtual {p1, v0}, Lizn;->b([B)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lizp;->q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected final a(Lizm;I)Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p1}, Lizm;->l()I

    move-result v2

    invoke-virtual {p1, p2}, Lizm;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lizp;->q:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lizp;->q:Ljava/util/List;

    :cond_1
    invoke-virtual {p1}, Lizm;->l()I

    move-result v0

    sub-int v3, v0, v2

    if-nez v3, :cond_2

    sget-object v0, Lizv;->h:[B

    :goto_1
    iget-object v1, p0, Lizp;->q:Ljava/util/List;

    new-instance v2, Lizu;

    invoke-direct {v2, p2, v0}, Lizu;-><init>(I[B)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    new-array v0, v3, [B

    iget v4, p1, Lizm;->b:I

    add-int/2addr v2, v4

    iget-object v4, p1, Lizm;->a:[B

    invoke-static {v4, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1
.end method
