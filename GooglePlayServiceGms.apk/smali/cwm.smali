.class public final Lcwm;
.super Lbje;
.source "SourceFile"

# interfaces
.implements Lbdx;
.implements Lbdy;


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/Map;

.field private i:Lcom/google/android/gms/games/PlayerEntity;

.field private j:Lcom/google/android/gms/games/GameEntity;

.field private final k:Ldaw;

.field private l:Z

.field private m:Z

.field private n:I

.field private final o:Landroid/os/Binder;

.field private final p:J

.field private final q:Z

.field private final r:I

.field private final s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Ljava/lang/String;Ljava/lang/String;Lbdx;Lbdy;[Ljava/lang/String;ILandroid/view/View;ZZIZI)V
    .locals 7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v1 .. v6}, Lbje;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;[Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcwm;->l:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcwm;->m:Z

    iput-object p3, p0, Lcwm;->f:Ljava/lang/String;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcwm;->g:Ljava/lang/String;

    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    iput-object v1, p0, Lcwm;->o:Landroid/os/Binder;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcwm;->h:Ljava/util/Map;

    invoke-static {p0, p8}, Ldaw;->a(Lcwm;I)Ldaw;

    move-result-object v1

    iput-object v1, p0, Lcwm;->k:Ldaw;

    iget-object v1, p0, Lcwm;->k:Ldaw;

    move-object/from16 v0, p9

    invoke-virtual {v1, v0}, Ldaw;->a(Landroid/view/View;)V

    move/from16 v0, p11

    iput-boolean v0, p0, Lcwm;->m:Z

    move/from16 v0, p12

    iput v0, p0, Lcwm;->n:I

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcwm;->p:J

    move/from16 v0, p10

    iput-boolean v0, p0, Lcwm;->q:Z

    move/from16 v0, p13

    iput-boolean v0, p0, Lcwm;->s:Z

    move/from16 v0, p14

    iput v0, p0, Lcwm;->r:I

    invoke-virtual {p0, p0}, Lcwm;->a(Lbdx;)V

    invoke-virtual {p0, p0}, Lcwm;->a(Lbdy;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 1

    invoke-static {p0}, Lcwm;->b(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/multiplayer/realtime/Room;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 3

    new-instance v1, Ldgi;

    invoke-direct {v1, p0}, Ldgi;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Ldgi;->a()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ldgi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Ldgi;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldgi;->b()V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2, p3}, Ldag;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2}, Ldag;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1}, Ldag;->g(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 6

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Ldag;->a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Ldah;->a(Landroid/os/IBinder;)Ldag;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldgn;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Lcys;

    invoke-direct {v2, p0, p1, p1}, Lcys;-><init>(Lcwm;Ldgm;Ldgl;)V

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, v2, p2}, Ldag;->h(Ldad;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    new-instance v2, Ldgi;

    invoke-direct {v2, v0}, Ldgi;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v2}, Ldgi;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ldgi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    invoke-virtual {v2}, Ldgi;->b()V

    :goto_1
    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ldgi;->b()V

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v2, "service died"

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcwm;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-super {p0}, Lbje;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1}, Ldag;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "show_welcome_popup"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcwm;->l:Z

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lbje;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Lcwm;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2}, Ldag;->a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbbo;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcwm;->l:Z

    return-void
.end method

.method public final a(Lbds;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxc;

    invoke-direct {v1, p0, p1}, Lcxc;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1}, Ldag;->d(Ldad;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;III)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcym;

    invoke-direct {v1, p0, p1}, Lcym;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3, p4}, Ldag;->a(Ldad;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;IIZZ)V
    .locals 6

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcwy;

    invoke-direct {v1, p0, p1}, Lcwy;-><init>(Lcwm;Lbds;)V

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Ldag;->a(Ldad;IIZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;IZZ)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyd;

    invoke-direct {v1, p0, p1}, Lcyd;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3, p4}, Ldag;->a(Ldad;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;I[I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lczb;

    invoke-direct {v1, p0, p1}, Lczb;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3}, Ldag;->a(Ldad;I[I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ldfg;II)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxi;

    invoke-direct {v1, p0, p1}, Lcxi;-><init>(Lcwm;Lbds;)V

    invoke-virtual {p2}, Ldfg;->d()Ldfh;

    move-result-object v2

    iget-object v2, v2, Ldfh;->a:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, p3, p4}, Ldag;->a(Ldad;Landroid/os/Bundle;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyz;

    invoke-direct {v1, p0, p1}, Lcyz;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2}, Ldag;->l(Ldad;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyk;

    invoke-direct {v1, p0, p1}, Lcyk;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3}, Ldag;->a(Ldad;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;IIIZ)V
    .locals 7

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxi;

    invoke-direct {v1, p0, p1}, Lcxi;-><init>(Lcwm;Lbds;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Ldag;->a(Ldad;Ljava/lang/String;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;IZZ)V
    .locals 6

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyd;

    invoke-direct {v1, p0, p1}, Lcyd;-><init>(Lcwm;Lbds;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Ldag;->b(Ldad;Ljava/lang/String;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;I[I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lczb;

    invoke-direct {v1, p0, p1}, Lczb;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3, p4}, Ldag;->a(Ldad;Ljava/lang/String;I[I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyz;

    invoke-direct {v1, p0, p1}, Lcyz;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3}, Ldag;->d(Ldad;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 7

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcym;

    invoke-direct {v1, p0, p1}, Lcym;-><init>(Lcwm;Lbds;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Ldag;->a(Ldad;Ljava/lang/String;Ljava/lang/String;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 8

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxi;

    invoke-direct {v1, p0, p1}, Lcxi;-><init>(Lcwm;Lbds;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Ldag;->a(Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 7

    const-string v0, "playedWith"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "circled"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid player collection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyd;

    invoke-direct {v1, p0, p1}, Lcyd;-><init>(Lcwm;Lbds;)V

    const/4 v5, 0x0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Ldag;->a(Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyo;

    invoke-direct {v1, p0, p1}, Lcyo;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3, p4}, Ldag;->a(Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxa;

    invoke-direct {v1, p0, p1}, Lcxa;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3}, Ldag;->a(Ldad;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Ljava/lang/String;[Ljava/lang/String;I[BI)V
    .locals 7

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyi;

    invoke-direct {v1, p0, p1}, Lcyi;-><init>(Lcwm;Lbds;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Ldag;->a(Ldad;Ljava/lang/String;[Ljava/lang/String;I[BI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyd;

    invoke-direct {v1, p0, p1}, Lcyd;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2}, Ldag;->c(Ldad;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;ZLandroid/os/Bundle;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcwv;

    invoke-direct {v1, p0, p1}, Lcwv;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3}, Ldag;->a(Ldad;ZLandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbds;[Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyd;

    invoke-direct {v1, p0, p1}, Lcyd;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2}, Ldag;->c(Ldad;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Lbjy;Lbjj;)V
    .locals 10

    iget-object v0, p0, Lbje;->b_:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v0, "com.google.android.gms.games.key.isHeadless"

    iget-boolean v1, p0, Lcwm;->q:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "com.google.android.gms.games.key.showConnectingPopup"

    iget-boolean v1, p0, Lcwm;->m:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "com.google.android.gms.games.key.connectingPopupGravity"

    iget v1, p0, Lcwm;->n:I

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "com.google.android.gms.games.key.retryingSignIn"

    iget-boolean v1, p0, Lcwm;->s:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "com.google.android.gms.games.key.sdkVariant"

    iget v1, p0, Lcwm;->r:I

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const v2, 0x41fe88

    iget-object v0, p0, Lbje;->b_:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcwm;->g:Ljava/lang/String;

    iget-object v5, p0, Lbje;->c:[Ljava/lang/String;

    iget-object v6, p0, Lcwm;->f:Ljava/lang/String;

    iget-object v0, p0, Lcwm;->k:Ldaw;

    invoke-virtual {v0}, Ldaw;->b()Landroid/os/IBinder;

    move-result-object v7

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v9}, Lbjy;->a(Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Ldfz;)V
    .locals 4

    :try_start_0
    new-instance v1, Lcxe;

    invoke-direct {v1, p0, p1}, Lcxe;-><init>(Lcwm;Ldfz;)V

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v2, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, v3}, Ldag;->a(Ldad;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ldfz;Ljava/lang/String;)V
    .locals 4

    :try_start_0
    new-instance v1, Lcxe;

    invoke-direct {v1, p0, p1}, Lcxe;-><init>(Lcwm;Ldfz;)V

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v2, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, v3, p2}, Ldag;->a(Ldad;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ldgp;)V
    .locals 4

    :try_start_0
    new-instance v1, Lcxo;

    invoke-direct {v1, p0, p1}, Lcxo;-><init>(Lcwm;Ldgp;)V

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v2, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, v3}, Ldag;->b(Ldad;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ldgp;Ljava/lang/String;)V
    .locals 4

    :try_start_0
    new-instance v1, Lcxo;

    invoke-direct {v1, p0, p1}, Lcxo;-><init>(Lcwm;Ldgp;)V

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v2, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, v3, p2}, Ldag;->b(Ldad;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ldle;)V
    .locals 4

    :try_start_0
    new-instance v1, Lcyf;

    invoke-direct {v1, p0, p1}, Lcyf;-><init>(Lcwm;Ldle;)V

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v2, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, v3}, Ldag;->c(Ldad;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ldle;Ljava/lang/String;)V
    .locals 4

    :try_start_0
    new-instance v1, Lcyf;

    invoke-direct {v1, p0, p1}, Lcyf;-><init>(Lcwm;Ldle;)V

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v2, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, v3, p2}, Ldag;->c(Ldad;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2}, Ldag;->b(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2}, Ldag;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2, p3}, Ldag;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ldag;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final varargs a([Ljava/lang/String;)V
    .locals 7

    const/4 v4, 0x1

    const/4 v1, 0x0

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_2

    aget-object v5, p1, v0

    const-string v6, "https://www.googleapis.com/auth/games"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v4

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v6, "https://www.googleapis.com/auth/games.firstparty"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v4

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_4

    if-nez v3, :cond_3

    move v0, v4

    :goto_2
    const-string v2, "Cannot have both %s and %s!"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "https://www.googleapis.com/auth/games"

    aput-object v5, v3, v1

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :goto_3
    return-void

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    const-string v0, "Games APIs requires %s to function."

    new-array v2, v4, [Ljava/lang/Object;

    const-string v4, "https://www.googleapis.com/auth/games"

    aput-object v4, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    goto :goto_3
.end method

.method public final b()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcwm;->l:Z

    invoke-virtual {p0}, Lcwm;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->c()V

    iget-wide v1, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2}, Ldag;->a(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcwm;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "Failed to notify client disconnect."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcwm;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-super {p0}, Lbje;->b()V

    return-void
.end method

.method public final b(Lbds;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyw;

    invoke-direct {v1, p0, p1}, Lcyw;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1}, Ldag;->a(Ldad;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lbds;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyz;

    invoke-direct {v1, p0, p1}, Lcyz;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2}, Ldag;->m(Ldad;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lbds;Ljava/lang/String;IIIZ)V
    .locals 7

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxi;

    invoke-direct {v1, p0, p1}, Lcxi;-><init>(Lcwm;Lbds;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Ldag;->b(Ldad;Ljava/lang/String;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lbds;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyz;

    invoke-direct {v1, p0, p1}, Lcyz;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2, p3}, Ldag;->e(Ldad;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lbds;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 8

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxi;

    invoke-direct {v1, p0, p1}, Lcxi;-><init>(Lcwm;Lbds;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Ldag;->b(Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lbds;Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxk;

    invoke-direct {v1, p0, p1}, Lcxk;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2}, Ldag;->b(Ldad;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lbds;[Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcyo;

    invoke-direct {v1, p0, p1}, Lcyo;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2}, Ldag;->b(Ldad;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1}, Ldag;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2}, Ldag;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2}, Ldag;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2, p3}, Ldag;->b(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.games.service.START"

    return-object v0
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public final c(Lbds;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxr;

    invoke-direct {v1, p0, p1}, Lcxr;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1}, Ldag;->h(Ldad;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lbds;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcxt;

    invoke-direct {v1, p0, p1}, Lcxt;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2}, Ldag;->j(Ldad;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lbds;Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcwq;

    invoke-direct {v1, p0, p1}, Lcwq;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1, p2}, Ldag;->a(Ldad;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v1, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, p1}, Ldag;->a(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1, p2}, Ldag;->c(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final c_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    return-object v0
.end method

.method public final d(Lbds;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    new-instance v1, Lcwt;

    invoke-direct {v1, p0, p1}, Lcwt;-><init>(Lcwm;Lbds;)V

    invoke-interface {v0, v1}, Ldag;->i(Ldad;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v1, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, p1}, Ldag;->b(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->d()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v1, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2, p1}, Ldag;->c(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e_()Landroid/os/Bundle;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->b()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v1, Lcwm;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1}, Ldag;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)I
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0, p1}, Ldag;->d(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->e()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Lcom/google/android/gms/games/Player;
    .locals 2

    invoke-virtual {p0}, Lcwm;->h()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcwm;->i:Lcom/google/android/gms/games/PlayerEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    :try_start_1
    new-instance v1, Lcts;

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->f()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lcts;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1}, Lcts;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcts;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    iput-object v0, p0, Lcwm;->i:Lcom/google/android/gms/games/PlayerEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Lcts;->b()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_1
    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v0, p0, Lcwm;->i:Lcom/google/android/gms/games/PlayerEntity;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Lcts;->b()V

    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final l()Lcom/google/android/gms/games/Game;
    .locals 2

    invoke-virtual {p0}, Lcwm;->h()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcwm;->j:Lcom/google/android/gms/games/GameEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    :try_start_1
    new-instance v1, Lctb;

    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->h()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lctb;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1}, Lctb;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lctb;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/GameEntity;

    iput-object v0, p0, Lcwm;->j:Lcom/google/android/gms/games/GameEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Lctb;->b()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_1
    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v0, p0, Lcwm;->j:Lcom/google/android/gms/games/GameEntity;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Lctb;->b()V

    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 1

    iget-boolean v0, p0, Lcwm;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcwm;->k:Ldaw;

    invoke-virtual {v0}, Ldaw;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcwm;->l:Z

    :cond_0
    return-void
.end method

.method public final m()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v1, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2}, Ldag;->b(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final n()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v1, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2}, Ldag;->c(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final o()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    iget-wide v1, p0, Lcwm;->p:J

    invoke-interface {v0, v1, v2}, Ldag;->d(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final p()Landroid/content/Intent;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->o()Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Landroid/content/Intent;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->p()Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->a()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()I
    .locals 3

    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->s()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v2, "service died"

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public final t()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final u()Z
    .locals 3

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v2, "service died"

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public final v()V
    .locals 2

    invoke-virtual {p0}, Lcwm;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcwm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Ldag;

    invoke-interface {v0}, Ldag;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
