.class final Lcrh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcrc;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Lcrm;

.field private final d:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcrm;Ljava/util/concurrent/Executor;)V
    .locals 1

    const-string v0, "https://www.googleapis.com/androidantiabuse/v1/x/create?alt=PROTO&key=AIzaSyBofcZsgLSS7BOnBjZPEkk4rYwzOIz-lTI"

    invoke-direct {p0, p1, v0, p2, p3}, Lcrh;-><init>(Landroid/content/Context;Ljava/lang/String;Lcrm;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcrm;Ljava/util/concurrent/Executor;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcrh;->a:Landroid/content/Context;

    iput-object p2, p0, Lcrh;->b:Ljava/lang/String;

    iput-object p3, p0, Lcrh;->c:Lcrm;

    iput-object p4, p0, Lcrh;->d:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private a(Lcpa;)Lcpb;
    .locals 4

    new-instance v1, Lwk;

    iget-object v0, p0, Lcrh;->a:Landroid/content/Context;

    const-string v2, "DroidGuard/4325030"

    const/4 v3, 0x1

    invoke-direct {v1, v0, v2, v3}, Lwk;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    iget-object v2, p0, Lcrh;->b:Ljava/lang/String;

    invoke-direct {v0, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcrh;->d:Ljava/util/concurrent/Executor;

    new-instance v3, Lcri;

    invoke-direct {v3, p0, v0}, Lcri;-><init>(Lcrh;Lorg/apache/http/client/methods/HttpPost;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    new-instance v2, Lcrj;

    invoke-direct {v2, p1}, Lcrj;-><init>(Lcpa;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v2

    :try_start_1
    invoke-virtual {v1, v0}, Lwk;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    :try_start_3
    invoke-static {v0, v2}, Lbpp;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    new-instance v2, Lcpb;

    invoke-direct {v2}, Lcpb;-><init>()V

    array-length v3, v0

    invoke-virtual {v2, v0, v3}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Lcpb;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-virtual {v1}, Lwk;->a()V

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_5
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Lwk;->a()V

    throw v0

    :catchall_2
    move-exception v2

    :try_start_6
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
.end method

.method private a(Lcrl;)Lcqw;
    .locals 8

    new-instance v7, Lcqw;

    invoke-direct {v7}, Lcqw;-><init>()V

    :try_start_0
    new-instance v1, Lcpa;

    invoke-direct {v1}, Lcpa;-><init>()V

    new-instance v2, Lcpe;

    invoke-direct {v2}, Lcpe;-><init>()V

    iget-object v3, p1, Lcrl;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcpe;->a(Ljava/lang/String;)Lcpe;

    iget-object v3, p1, Lcrl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcpe;->b(Ljava/lang/String;)Lcpe;

    invoke-virtual {v1, v2}, Lcpa;->a(Lcpe;)Lcpa;

    const-string v2, "4.3.25 (1117461-030)"

    invoke-virtual {v1, v2}, Lcpa;->a(Ljava/lang/String;)Lcpa;

    const-string v2, "BOARD"

    sget-object v3, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "BOOTLOADER"

    sget-object v3, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "BRAND"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "CPU_ABI"

    sget-object v3, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "CPU_ABI2"

    sget-object v3, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "DEVICE"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "DISPLAY"

    sget-object v3, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "FINGERPRINT"

    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "HARDWARE"

    sget-object v3, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "HOST"

    sget-object v3, Landroid/os/Build;->HOST:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ID"

    sget-object v3, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "MANUFACTURER"

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "MODEL"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PRODUCT"

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "RADIO"

    sget-object v3, Landroid/os/Build;->RADIO:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "SERIAL"

    sget-object v3, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "TAGS"

    sget-object v3, Landroid/os/Build;->TAGS:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "TIME"

    sget-wide v3, Landroid/os/Build;->TIME:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "TYPE"

    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "USER"

    sget-object v3, Landroid/os/Build;->USER:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "VERSION.CODENAME"

    sget-object v3, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "VERSION.INCREMENTAL"

    sget-object v3, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "VERSION.RELEASE"

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "VERSION.SDK"

    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "VERSION.SDK_INT"

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcrh;->a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcrh;->a(Lcpa;)Lcpb;

    move-result-object v1

    iget-object v2, p0, Lcrh;->c:Lcrm;

    iget-object v3, v1, Lcpb;->b:Lizf;

    invoke-virtual {v3}, Lizf;->b()[B

    move-result-object v3

    iget-object v4, v1, Lcpb;->d:Lizf;

    invoke-virtual {v4}, Lizf;->b()[B

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcrm;->a([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v1, "Program signature verification failed."

    invoke-virtual {v7, v1}, Lcqw;->a(Ljava/lang/String;)Lcqw;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-boolean v2, v1, Lcpb;->a:Z

    if-nez v2, :cond_1

    new-instance v1, Lcrk;

    const-string v2, "program"

    invoke-direct {v1, v2}, Lcrk;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    invoke-virtual {v7, v1}, Lcqw;->a(Ljava/lang/Throwable;)Lcqw;

    :goto_1
    move-object v1, v7

    goto :goto_0

    :cond_1
    :try_start_1
    iget-boolean v2, v1, Lcpb;->c:Z

    if-nez v2, :cond_2

    new-instance v1, Lcrk;

    const-string v2, "signature"

    invoke-direct {v1, v2}, Lcrk;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, v1, Lcpb;->b:Lizf;

    invoke-virtual {v1}, Lizf;->b()[B

    move-result-object v1

    new-instance v2, Lcpd;

    invoke-direct {v2}, Lcpd;-><init>()V

    array-length v3, v1

    invoke-virtual {v2, v1, v3}, Lizk;->a([BI)Lizk;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcpd;

    move-object v5, v0

    iget-boolean v1, v5, Lcpd;->a:Z

    if-nez v1, :cond_3

    new-instance v1, Lcrk;

    const-string v2, "byteCode"

    invoke-direct {v1, v2}, Lcrk;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    iget-boolean v1, v5, Lcpd;->c:Z

    if-nez v1, :cond_4

    new-instance v1, Lcrk;

    const-string v2, "vmUrl"

    invoke-direct {v1, v2}, Lcrk;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    iget-boolean v1, v5, Lcpd;->e:Z

    if-nez v1, :cond_5

    new-instance v1, Lcrk;

    const-string v2, "vmChecksum"

    invoke-direct {v1, v2}, Lcrk;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    iget-boolean v1, v5, Lcpd;->g:Z

    if-nez v1, :cond_6

    new-instance v1, Lcrk;

    const-string v2, "expiryTimeSecs"

    invoke-direct {v1, v2}, Lcrk;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    new-instance v1, Lcre;

    iget-object v2, v5, Lcpd;->b:Lizf;

    invoke-virtual {v2}, Lizf;->b()[B

    move-result-object v2

    iget-object v3, v5, Lcpd;->f:Lizf;

    invoke-virtual {v3}, Lizf;->b()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lbox;->a([BZ)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v5, Lcpd;->d:Ljava/lang/String;

    iget v5, v5, Lcpd;->h:I

    int-to-long v5, v5

    invoke-direct/range {v1 .. v6}, Lcre;-><init>([BLjava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v7, v1}, Lcqw;->a(Ljava/lang/Object;)Lcqw;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private static a(Lcpa;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcpc;

    invoke-direct {v0}, Lcpc;-><init>()V

    invoke-virtual {v0, p1}, Lcpc;->a(Ljava/lang/String;)Lcpc;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcpc;->b(Ljava/lang/String;)Lcpc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcpa;->a(Lcpc;)Lcpa;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcqw;
    .locals 1

    check-cast p1, Lcrl;

    invoke-direct {p0, p1}, Lcrh;->a(Lcrl;)Lcqw;

    move-result-object v0

    return-object v0
.end method
