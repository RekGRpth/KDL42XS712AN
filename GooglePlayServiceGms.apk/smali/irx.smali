.class public abstract Lirx;
.super Lirs;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lirs;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lirx;
    .locals 1

    new-instance v0, Litb;

    invoke-direct {v0, p0}, Litb;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static c()Lirx;
    .locals 1

    sget-object v0, Lirq;->a:Lirq;

    return-object v0
.end method


# virtual methods
.method public abstract a()Litc;
.end method

.method b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lirx;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lirx;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lirx;

    invoke-virtual {v0}, Lirx;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lirx;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p0, p1}, Lisy;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    add-int/2addr v0, v2

    goto :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lirx;->a()Litc;

    move-result-object v0

    return-object v0
.end method
