.class public abstract Lgqr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lsf;

.field protected final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgqr;->c:Landroid/content/Context;

    invoke-static {p1}, Lti;->a(Landroid/content/Context;)Lsf;

    move-result-object v0

    iput-object v0, p0, Lgqr;->a:Lsf;

    iput-object p2, p0, Lgqr;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    new-instance v0, Lgqs;

    invoke-direct {v0, p0}, Lgqs;-><init>(Lgqr;)V

    new-instance v1, Lgqt;

    invoke-direct {v1, p0}, Lgqt;-><init>(Lgqr;)V

    new-instance v2, Lgqu;

    iget-object v3, p0, Lgqr;->c:Landroid/content/Context;

    iget-object v4, p0, Lgqr;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v1, v0}, Lgqu;-><init>(Landroid/content/Context;Ljava/lang/String;Lsk;Lsj;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lgqu;->a(Z)Lsc;

    iget-object v0, p0, Lgqr;->a:Lsf;

    invoke-virtual {v0, v2}, Lsf;->a(Lsc;)Lsc;

    return-void
.end method

.method public abstract b()V
.end method
