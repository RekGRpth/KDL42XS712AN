.class public final Lbcm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lbfx;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lbcm;->a:J

    return-void
.end method

.method public static a()Lbdh;
    .locals 3

    new-instance v0, Lbdh;

    invoke-direct {v0}, Lbdh;-><init>()V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-static {v1}, Lbox;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbdh;->a:Ljava/lang/String;

    invoke-static {}, Lbqe;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lbdh;->b:Ljava/lang/Long;

    invoke-static {}, Lbqe;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lbdh;->c:Ljava/lang/Integer;

    invoke-static {}, Lbqe;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lbdh;->d:Ljava/lang/Integer;

    invoke-static {}, Lbqe;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lbdh;->e:Ljava/lang/Integer;

    new-instance v1, Lbdl;

    invoke-direct {v1}, Lbdl;-><init>()V

    const-string v2, "java.vm.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iput-object v2, v1, Lbdl;->a:Ljava/lang/String;

    :cond_0
    const-string v2, "java.vm.vendor"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iput-object v2, v1, Lbdl;->b:Ljava/lang/String;

    :cond_1
    const-string v2, "java.vm.name"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iput-object v2, v1, Lbdl;->c:Ljava/lang/String;

    :cond_2
    const-string v2, "java.vm.specification.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    iput-object v2, v1, Lbdl;->d:Ljava/lang/String;

    :cond_3
    const-string v2, "java.vm.specification.vendor"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    iput-object v2, v1, Lbdl;->e:Ljava/lang/String;

    :cond_4
    const-string v2, "java.vm.specification.name"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iput-object v2, v1, Lbdl;->f:Ljava/lang/String;

    :cond_5
    iput-object v1, v0, Lbdh;->f:Lbdl;

    return-object v0
.end method

.method public static a(Lbdj;)V
    .locals 3

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lbdk;

    invoke-direct {v0}, Lbdk;-><init>()V

    iput-object p0, v0, Lbdk;->c:Lbdj;

    new-instance v1, Lbdi;

    invoke-direct {v1}, Lbdi;-><init>()V

    invoke-static {}, Lbcm;->a()Lbdh;

    move-result-object v2

    iput-object v2, v1, Lbdi;->a:Lbdh;

    iput-object v0, v1, Lbdi;->b:Lbdk;

    const-string v0, "system_health"

    invoke-static {v1}, Lizs;->a(Lizs;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lbcm;->a(Ljava/lang/String;[B)V

    return-void
.end method

.method private static a(Ljava/lang/String;[B)V
    .locals 3

    new-instance v0, Lfko;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lfko;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, p0, p1, v1}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v0}, Lfko;->a()V

    return-void
.end method

.method public static b()V
    .locals 2

    new-instance v0, Lbdi;

    invoke-direct {v0}, Lbdi;-><init>()V

    invoke-static {}, Lbcm;->a()Lbdh;

    move-result-object v1

    iput-object v1, v0, Lbdi;->a:Lbdh;

    const-string v1, "install"

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lbcm;->a(Ljava/lang/String;[B)V

    invoke-static {}, Lfmd;->a()Lfmd;

    move-result-object v0

    invoke-virtual {v0}, Lfmd;->e()V

    return-void
.end method

.method public static c()V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v4, Lbdi;

    invoke-direct {v4}, Lbdi;-><init>()V

    invoke-static {}, Lbcm;->a()Lbdh;

    move-result-object v0

    iput-object v0, v4, Lbdi;->a:Lbdh;

    new-instance v5, Lbdk;

    invoke-direct {v5}, Lbdk;-><init>()V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v3, "location"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/app/GmsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lbdk;->a:Ljava/lang/Boolean;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v3, "location"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/app/GmsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lbdk;->b:Ljava/lang/Boolean;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    new-instance v6, Lbdj;

    invoke-direct {v6}, Lbdj;-><init>()V

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v6, Lbdj;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    new-array v0, v8, [I

    iput-object v0, v6, Lbdj;->p:[I

    move v3, v2

    :goto_3
    if-ge v3, v8, :cond_4

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    const-string v1, "com.android.contacts"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x2

    :goto_4
    const-string v9, "com.google.android.gms.people"

    invoke-static {v0, v9}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    or-int/lit8 v1, v1, 0x1

    :cond_0
    iget-object v0, v6, Lbdj;->p:[I

    aput v1, v0, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    const-string v3, "network"

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    iput-object v6, v5, Lbdk;->c:Lbdj;

    iput-object v5, v4, Lbdi;->b:Lbdk;

    const-string v0, "system_health"

    invoke-static {v4}, Lizs;->a(Lizs;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lbcm;->a(Ljava/lang/String;[B)V

    return-void

    :cond_5
    move v1, v2

    goto :goto_4
.end method

.method public static d()V
    .locals 7

    const/4 v6, 0x1

    sget-wide v1, Lbcm;->a:J

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gms/common/analytics/CoreAnalyticsAlarmReceiver;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "type"

    const-string v5, "system_health"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v4, 0x0

    invoke-static {v0, v6, v3, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    const-string v4, "alarm"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v1, v4

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method
