.class public final Lhva;
.super Lcom/android/location/provider/LocationProviderBase;
.source "SourceFile"

# interfaces
.implements Lhwa;


# static fields
.field private static a:Lcom/android/location/provider/ProviderPropertiesUnbundled;


# instance fields
.field private final b:Lhvx;

.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v0, 0x0

    const/4 v4, 0x1

    move v1, v0

    move v2, v0

    move v3, v0

    move v5, v4

    move v6, v4

    move v7, v4

    move v8, v4

    invoke-static/range {v0 .. v8}, Lcom/android/location/provider/ProviderPropertiesUnbundled;->create(ZZZZZZZII)Lcom/android/location/provider/ProviderPropertiesUnbundled;

    move-result-object v0

    sput-object v0, Lhva;->a:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "FusedLocationProvider"

    sget-object v1, Lhva;->a:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    invoke-direct {p0, v0, v1}, Lcom/android/location/provider/LocationProviderBase;-><init>(Ljava/lang/String;Lcom/android/location/provider/ProviderPropertiesUnbundled;)V

    invoke-static {p1}, Lhvx;->a(Landroid/content/Context;)Lhvx;

    move-result-object v0

    iput-object v0, p0, Lhva;->b:Lhvx;

    iput-object p1, p0, Lhva;->c:Landroid/content/Context;

    return-void
.end method

.method private static a(I)I
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/16 v0, 0x66

    :goto_0
    return v0

    :sswitch_0
    const/16 v0, 0x64

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x68

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x69

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x68 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_1
        0xcb -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhva;->reportLocation(Landroid/location/Location;)V

    return-void
.end method

.method public final onDisable()V
    .locals 1

    iget-object v0, p0, Lhva;->b:Lhvx;

    invoke-virtual {v0}, Lhvx;->c()V

    return-void
.end method

.method public final onDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final onEnable()V
    .locals 2

    iget-object v0, p0, Lhva;->b:Lhvx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lhvx;->a(ILhwa;)V

    return-void
.end method

.method public final onGetStatus(Landroid/os/Bundle;)I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final onGetStatusUpdateTime()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final onSetRequest(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V
    .locals 12

    invoke-virtual {p1}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    move-result-object v8

    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/16 v5, 0x69

    const-wide v1, 0x7fffffffffffffffL

    const/4 v6, 0x0

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/location/provider/LocationRequestUnbundled;

    invoke-virtual {v0}, Lcom/android/location/provider/LocationRequestUnbundled;->getQuality()I

    move-result v3

    invoke-static {v3}, Lhva;->a(I)I

    move-result v7

    invoke-virtual {v0}, Lcom/android/location/provider/LocationRequestUnbundled;->getInterval()J

    move-result-wide v3

    cmp-long v11, v3, v1

    if-ltz v11, :cond_0

    if-ge v7, v5, :cond_5

    :cond_0
    move-wide v1, v3

    move v3, v7

    :goto_1
    move-object v6, v0

    move v5, v3

    goto :goto_0

    :cond_1
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/location/provider/LocationRequestUnbundled;

    invoke-virtual {v0}, Lcom/android/location/provider/LocationRequestUnbundled;->getQuality()I

    move-result v1

    invoke-static {v1}, Lhva;->a(I)I

    move-result v2

    const/4 v1, 0x0

    if-ne v0, v6, :cond_4

    const/16 v3, 0x12

    invoke-static {v3}, Lbpz;->a(I)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v4, p2

    :goto_3
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/location/provider/LocationRequestUnbundled;->getInterval()J

    move-result-wide v10

    invoke-virtual {v1, v10, v11}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    new-instance v0, Lhwl;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v4}, Lile;->a(Landroid/os/WorkSource;)Ljava/util/Collection;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lhwl;-><init>(Lcom/google/android/gms/location/LocationRequest;ZZLjava/util/Collection;Z)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Received location requests through overlay: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v9, v1, v2

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lhva;->b:Lhvx;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v9, v2}, Lhvx;->a(ILjava/lang/Iterable;Z)V

    return-void

    :cond_4
    move-object v4, v1

    goto :goto_3

    :cond_5
    move-object v0, v6

    move v3, v5

    goto :goto_1
.end method
