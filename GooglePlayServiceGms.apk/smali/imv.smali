.class public abstract Limv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected A:Ljava/util/concurrent/BlockingQueue;

.field protected B:I

.field protected C:I

.field protected D:Ljava/lang/Thread;

.field protected E:Ljava/lang/Thread;

.field F:Z

.field protected G:I

.field protected H:I

.field protected I:I

.field protected J:J

.field protected K:J

.field protected L:I

.field protected M:I

.field protected N:I

.field protected O:I

.field protected P:I

.field public c:Ljava/lang/String;

.field public d:I

.field protected e:Lizh;

.field protected f:Lizg;

.field protected g:Ljava/net/Socket;

.field protected h:I

.field protected volatile i:Z

.field protected j:Z

.field protected k:I

.field protected l:Ljava/lang/String;

.field protected m:Ljava/lang/String;

.field protected n:I

.field protected o:I

.field protected p:I

.field protected final q:Ljava/util/ArrayList;

.field protected r:Ljava/util/List;

.field protected final s:Ljava/util/Map;

.field protected t:Ljavax/net/ssl/SSLSocketFactory;

.field protected u:Z

.field protected v:J

.field protected w:J

.field protected x:Ljava/util/Map;

.field y:Ljava/lang/reflect/Method;

.field z:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "mtalk.google.com"

    iput-object v0, p0, Limv;->c:Ljava/lang/String;

    const/16 v0, 0x146c

    iput v0, p0, Limv;->d:I

    iput-boolean v1, p0, Limv;->i:Z

    iput-boolean v1, p0, Limv;->j:Z

    const/16 v0, 0xa

    iput v0, p0, Limv;->k:I

    iput v2, p0, Limv;->o:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Limv;->q:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Limv;->r:Ljava/util/List;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Limv;->s:Ljava/util/Map;

    iput-boolean v1, p0, Limv;->u:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Limv;->x:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    const/16 v0, 0x4e20

    iput v0, p0, Limv;->B:I

    const v0, 0x36ee80

    iput v0, p0, Limv;->C:I

    iput-boolean v1, p0, Limv;->F:Z

    const/4 v0, -0x1

    iput v0, p0, Limv;->P:I

    :try_start_0
    const-string v0, "com.android.org.conscrypt.OpenSSLSocketImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    :try_start_1
    const-string v1, "setUseSessionTickets"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Limv;->y:Ljava/lang/reflect/Method;

    const-string v1, "setHostname"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Limv;->z:Ljava/lang/reflect/Method;

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private a(ILjava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget v0, p0, Limv;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Limv;->P:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Limv;->w:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Limv;->K:J

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Limv;->i:Z

    if-nez v0, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Limv;->i:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Closing connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-boolean v4, p0, Limv;->j:Z

    :try_start_2
    iget-object v0, p0, Limv;->g:Ljava/net/Socket;

    if-eqz v0, :cond_2

    iget-object v0, p0, Limv;->g:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Limv;->g:Ljava/net/Socket;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Limv;->v:J

    iget-object v0, p0, Limv;->D:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    new-instance v1, Lind;

    invoke-direct {v1}, Lind;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-boolean v0, p0, Limv;->u:Z

    if-eqz v0, :cond_4

    iget-wide v0, p0, Limv;->J:J

    invoke-virtual {p0}, Limv;->l()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Limv;->J:J

    :cond_4
    :try_start_3
    iget-boolean v0, p0, Limv;->u:Z

    invoke-virtual {p0, p1, p2, v0}, Limv;->a(ILjava/lang/String;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error closing"

    invoke-virtual {p0, v1, v0}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Limv;)V
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    :try_start_0
    iget-object v1, p0, Limv;->f:Lizg;

    invoke-virtual {v1}, Lizg;->k()B

    move-result v1

    iput v1, p0, Limv;->h:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v8, v9

    :goto_0
    iget-boolean v1, p0, Limv;->i:Z

    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {p0}, Limv;->c()V

    invoke-direct {p0}, Limv;->p()I

    move-result v3

    invoke-virtual {p0}, Limv;->b()V

    iget-boolean v1, p0, Limv;->i:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Limv;->a(ILjava/lang/String;)V

    const-string v2, "Error connectin "

    invoke-virtual {p0, v2, v1}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    if-gez v3, :cond_2

    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Graceful close (FIN) "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "FIN "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Limv;->a(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v2, "Error reading messages"

    invoke-virtual {p0, v2, v1}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v2, 0x13

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v1}, Limv;->a(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_3
    iget-object v2, p0, Limv;->f:Lizg;

    invoke-virtual {v2}, Lizg;->h()I

    move-result v4

    iget v1, p0, Limv;->N:I

    add-int/lit8 v5, v4, 0x2

    add-int/2addr v1, v5

    iput v1, p0, Limv;->N:I

    const/16 v1, 0x80

    if-le v4, v1, :cond_3

    iget v1, p0, Limv;->N:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Limv;->N:I

    :cond_3
    const/4 v1, 0x0

    new-array v1, v1, [B

    if-lez v4, :cond_4

    invoke-virtual {v2, v4}, Lizg;->c(I)[B

    move-result-object v1

    :cond_4
    int-to-byte v2, v3

    invoke-static {v2, v1}, Lina;->a(B[B)Lizk;

    move-result-object v7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CH-IN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v7}, Lina;->a(Ljava/lang/String;Lizk;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget v1, p0, Limv;->L:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Limv;->L:I

    instance-of v1, v7, Line;

    if-eqz v1, :cond_5

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v7

    check-cast v0, Line;

    move-object v1, v0

    iget-object v1, v1, Line;->d:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v7}, Lina;->b(Lizk;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Limv;->o:I

    iget v6, p0, Limv;->p:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Limv;->a(ZILjava/lang/String;II)V

    :goto_2
    instance-of v1, v7, Linn;

    if-eqz v1, :cond_7

    check-cast v7, Linn;

    invoke-direct {p0, v7}, Limv;->b(Linn;)Z

    move-result v1

    if-eqz v1, :cond_6

    move v8, v10

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x0

    invoke-static {v7}, Lina;->b(Lizk;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Limv;->o:I

    iget v6, p0, Limv;->p:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Limv;->a(ZILjava/lang/String;II)V

    goto :goto_2

    :cond_6
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Limv;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    if-nez v8, :cond_8

    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Limv;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    instance-of v1, v7, Linj;

    if-eqz v1, :cond_9

    new-instance v1, Linh;

    invoke-direct {v1}, Linh;-><init>()V

    invoke-virtual {p0, v1}, Limv;->d(Lizk;)V

    :cond_9
    instance-of v1, v7, Lind;

    if-eqz v1, :cond_a

    const/16 v1, 0x12

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Limv;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    iget-object v2, p0, Limv;->q:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    iget v1, p0, Limv;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Limv;->o:I

    instance-of v1, v7, Line;

    if-eqz v1, :cond_e

    move-object v0, v7

    check-cast v0, Line;

    move-object v1, v0

    iget-boolean v1, v1, Line;->p:Z

    if-eqz v1, :cond_e

    move v1, v10

    :goto_3
    iget v3, p0, Limv;->o:I

    iget v4, p0, Limv;->n:I

    sub-int/2addr v3, v4

    iget v4, p0, Limv;->k:I

    if-ge v3, v4, :cond_b

    if-eqz v1, :cond_c

    :cond_b
    invoke-static {}, Lina;->a()Linl;

    move-result-object v1

    invoke-virtual {p0, v1}, Limv;->d(Lizk;)V

    :cond_c
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-static {v7}, Lina;->b(Lizk;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v7}, Lina;->c(Lizk;)I

    move-result v2

    invoke-direct {p0, v1, v2}, Limv;->a(Ljava/lang/String;I)V

    instance-of v1, v7, Linl;

    if-eqz v1, :cond_d

    move-object v0, v7

    check-cast v0, Linl;

    move-object v1, v0

    iget-object v1, v1, Linl;->b:Ling;

    if-eqz v1, :cond_d

    iget v2, v1, Ling;->a:I

    const/16 v3, 0xc

    if-ne v2, v3, :cond_d

    iget-object v1, v1, Ling;->b:Lizf;

    new-instance v7, Lino;

    invoke-direct {v7}, Lino;-><init>()V

    invoke-virtual {v1}, Lizf;->b()[B

    move-result-object v1

    array-length v2, v1

    invoke-virtual {v7, v1, v2}, Lizk;->a([BI)Lizk;

    iget-object v1, v7, Lino;->a:Ljava/util/List;

    invoke-virtual {p0, v1}, Limv;->a(Ljava/util/List;)V

    invoke-virtual {p0}, Limv;->a()V

    :cond_d
    invoke-virtual {p0, v7}, Limv;->b(Lizk;)V

    goto/16 :goto_0

    :cond_e
    move v1, v9

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
.end method

.method static synthetic a(Limv;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Limv;->a(ILjava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 6

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v2, p0, Limv;->r:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Limv;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Limv;->q:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, Limv;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limz;

    iget v5, v0, Limz;->a:I

    if-lt p2, v5, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-direct {p0, v2}, Limv;->c(Ljava/util/List;)V

    :cond_2
    iget-object v3, p0, Limv;->s:Ljava/util/Map;

    monitor-enter v3

    :try_start_3
    iget-object v0, p0, Limv;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v1

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p2, v0, :cond_5

    if-nez v2, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Limv;->s:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    :catchall_2
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v0, v1

    move-object v1, v2

    :goto_2
    invoke-direct {p0, p1, v0, v1}, Limv;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Limv;->a(Ljava/lang/String;)V

    :cond_0
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    if-lez v0, :cond_1

    invoke-virtual {p0, p2}, Limv;->b(Ljava/util/List;)V

    :cond_1
    if-eqz p3, :cond_3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v2, p0, Limv;->s:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Limv;->s:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    return-void
.end method

.method static synthetic b(Limv;)V
    .locals 8

    const/16 v7, 0xa

    const/16 v6, 0x10

    const/4 v2, 0x0

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Starting a new connection to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Limv;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Limv;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-boolean v2, p0, Limv;->u:Z

    iget v0, p0, Limv;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Limv;->H:I

    :try_start_0
    iget-object v0, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V

    iget-object v0, p0, Limv;->t:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iget v1, p0, Limv;->B:I

    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.org.conscrypt.OpenSSLSocketImpl"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    :try_start_1
    iget-object v1, p0, Limv;->y:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_2

    iget-object v1, p0, Limv;->y:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v1, p0, Limv;->z:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_3

    iget-object v1, p0, Limv;->z:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Limv;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    const-string v1, "Enabled SNI and Tickets"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    new-instance v1, Ljava/net/InetSocketAddress;

    iget-object v2, p0, Limv;->c:Ljava/lang/String;

    iget v3, p0, Limv;->d:I

    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iget v2, p0, Limv;->B:I

    invoke-virtual {v0, v1, v2}, Ljavax/net/ssl/SSLSocket;->connect(Ljava/net/SocketAddress;I)V

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v1

    if-nez v1, :cond_5

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cannot verify SSL socket without session"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4

    :catch_0
    move-exception v0

    invoke-direct {p0, v6, v5}, Limv;->a(ILjava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Timeout error connecting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_3
    return-void

    :cond_2
    :try_start_3
    const-string v1, "tickets failed"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tickets or SNI failed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-direct {p0, v6, v5}, Limv;->a(ILjava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Socket error connecting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_3
    :try_start_5
    const-string v1, "SNI failed"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v6, v1}, Limv;->a(ILjava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IO error connecting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    :try_start_6
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected socket class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_2

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v7, v1}, Limv;->a(ILjava/lang/String;)V

    const-string v1, "Unexpected error connecting "

    invoke-virtual {p0, v1, v0}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    :try_start_7
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v2

    iget-object v3, p0, Limv;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v1

    if-nez v1, :cond_6

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot verify hostname: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Limv;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iput-object v0, p0, Limv;->g:Ljava/net/Socket;

    iget-object v0, p0, Limv;->g:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-static {v0}, Lizh;->a(Ljava/io/OutputStream;)Lizh;

    move-result-object v0

    iput-object v0, p0, Limv;->e:Lizh;

    iget-object v0, p0, Limv;->g:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lizg;

    invoke-direct {v1, v0}, Lizg;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Limv;->f:Lizg;

    iget-object v0, p0, Limv;->e:Lizh;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Lizh;->a(B)V

    const/4 v0, 0x0

    iput v0, p0, Limv;->n:I

    const/4 v0, 0x1

    iput v0, p0, Limv;->o:I

    const/4 v0, 0x0

    iput v0, p0, Limv;->p:I

    invoke-direct {p0}, Limv;->q()V
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Limw;

    invoke-direct {v1, p0}, Limw;-><init>(Limv;)V

    const-string v2, "GCMReader"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Limv;->E:Ljava/lang/Thread;

    iget-object v0, p0, Limv;->E:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_7
    :goto_4
    iget-boolean v0, p0, Limv;->i:Z

    if-eqz v0, :cond_1

    :try_start_8
    iget-object v0, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizk;

    iget-boolean v1, p0, Limv;->F:Z

    if-eqz v1, :cond_8

    const/4 v1, 0x0

    iput-boolean v1, p0, Limv;->F:Z

    new-instance v1, Linj;

    invoke-direct {v1}, Linj;-><init>()V

    invoke-direct {p0, v1}, Limv;->e(Lizk;)Z

    invoke-virtual {p0}, Limv;->d()V

    :cond_8
    instance-of v1, v0, Linj;

    if-nez v1, :cond_7

    instance-of v1, v0, Lind;

    if-eqz v1, :cond_9

    invoke-virtual {p0, v0}, Limv;->a(Lizk;)V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_7

    goto :goto_4

    :catch_5
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_4

    :cond_9
    :try_start_9
    iget-boolean v1, p0, Limv;->i:Z

    if-eqz v1, :cond_7

    invoke-direct {p0, v0}, Limv;->e(Lizk;)Z
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_4

    :catch_6
    move-exception v0

    iget-boolean v1, p0, Limv;->i:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x15

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Limv;->a(ILjava/lang/String;)V

    const-string v1, "Error writing "

    invoke-virtual {p0, v1, v0}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    :catch_7
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v7, v1}, Limv;->a(ILjava/lang/String;)V

    const-string v1, "Unexpected error "

    invoke-virtual {p0, v1, v0}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3
.end method

.method private b(Linn;)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p1, Linn;->a:Linf;

    if-eqz v1, :cond_0

    iget v2, v1, Linf;->a:I

    if-eqz v2, :cond_0

    iget v0, v1, Linf;->a:I

    invoke-virtual {p0, p1}, Limv;->a(Linn;)V

    iget v0, p0, Limv;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Limv;->I:I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Limv;->P:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Limv;->K:J

    iget v1, p0, Limv;->G:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Limv;->G:I

    iput-boolean v0, p0, Limv;->u:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Limv;->w:J

    iget-object v1, p0, Limv;->g:Ljava/net/Socket;

    if-eqz v1, :cond_1

    iget-object v1, p0, Limv;->g:Ljava/net/Socket;

    iget v2, p0, Limv;->C:I

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    :cond_1
    iput-boolean v0, p0, Limv;->j:Z

    iget-object v1, p0, Limv;->r:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Limv;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, p1}, Limv;->a(Linn;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Ljava/lang/String;)I
    .locals 4

    iget v0, p0, Limv;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Limv;->p:I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Limz;

    iget v1, p0, Limv;->p:I

    invoke-direct {v0, v1, p1}, Limz;-><init>(ILjava/lang/String;)V

    iget-object v1, p0, Limv;->q:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Limv;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Limv;->r:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v2, p0, Limv;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v0, p0, Limv;->r:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Limv;->r:Ljava/util/List;

    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v1, p0, Limv;->s:Ljava/util/Map;

    monitor-enter v1

    if-eqz v0, :cond_2

    :try_start_2
    iget-object v2, p0, Limv;->s:Ljava/util/Map;

    iget v3, p0, Limv;->p:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget v0, p0, Limv;->p:I

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Ljava/util/List;)V
    .locals 5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, p0, Limv;->q:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limz;

    iget-object v4, v0, Limz;->b:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v0, Limz;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v4, v0, Limz;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v4, p0, Limv;->q:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0, v1}, Limv;->a(Ljava/util/List;)V

    return-void
.end method

.method private declared-synchronized e(Lizk;)Z
    .locals 9

    const/4 v8, 0x1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lina;->b(Lizk;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Limv;->c(Ljava/lang/String;)I

    iget-object v2, p0, Limv;->q:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v1, p0, Limv;->o:I

    iget v3, p0, Limv;->n:I

    if-le v1, v3, :cond_0

    iget v1, p0, Limv;->o:I

    invoke-static {p1, v1}, Lina;->a(Lizk;I)V

    iget v1, p0, Limv;->o:I

    iput v1, p0, Limv;->n:I

    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p0, p1}, Limv;->c(Lizk;)V

    iget-object v1, p0, Limv;->e:Lizh;

    invoke-static {p1}, Lina;->a(Lizk;)B

    move-result v2

    invoke-virtual {p1}, Lizk;->b()I

    move-result v7

    invoke-virtual {v1, v2}, Lizh;->a(B)V

    invoke-virtual {v1, v7}, Lizh;->c(I)V

    invoke-virtual {p1, v1}, Lizk;->a(Lizh;)V

    invoke-virtual {v1}, Lizh;->a()V

    iget v1, p0, Limv;->O:I

    add-int/2addr v1, v7

    iput v1, p0, Limv;->O:I

    iget v1, p0, Limv;->M:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Limv;->M:I

    invoke-virtual {p0, p1}, Limv;->a(Lizk;)V

    invoke-static {p1}, Lina;->a(Lizk;)B

    move-result v3

    instance-of v1, p1, Line;

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, p1

    check-cast v0, Line;

    move-object v1, v0

    iget-object v1, v1, Line;->d:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lina;->b(Lizk;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Limv;->o:I

    iget v6, p0, Limv;->p:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Limv;->a(ZILjava/lang/String;II)V

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CH-OUT: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lina;->a(Ljava/lang/String;Lizk;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v8

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_1
    const/4 v2, 0x1

    :try_start_4
    invoke-static {p1}, Lina;->b(Lizk;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Limv;->o:I

    iget v6, p0, Limv;->p:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Limv;->a(ZILjava/lang/String;II)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method private p()I
    .locals 1

    :try_start_0
    iget-object v0, p0, Limv;->f:Lizg;

    invoke-virtual {v0}, Lizg;->k()B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private q()V
    .locals 7

    iget-object v1, p0, Limv;->q:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Limv;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v1, p0, Limv;->s:Ljava/util/Map;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Limv;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v3, p0, Limv;->r:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_2
    iget-object v0, p0, Limv;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Limv;->l:Ljava/lang/String;

    iget-object v1, p0, Limv;->m:Ljava/lang/String;

    new-instance v2, Linm;

    invoke-direct {v2}, Linm;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Linm;->a(Ljava/lang/String;)Linm;

    const-string v3, "mcs.android.com"

    invoke-virtual {v2, v3}, Linm;->b(Ljava/lang/String;)Linm;

    invoke-virtual {v2, v0}, Linm;->d(Ljava/lang/String;)Linm;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "android-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Linm;->f(Ljava/lang/String;)Linm;

    const-wide/16 v3, 0x1

    invoke-virtual {v2, v3, v4}, Linm;->a(J)Linm;

    invoke-virtual {v2, v0}, Linm;->c(Ljava/lang/String;)Linm;

    invoke-virtual {v2, v1}, Linm;->e(Ljava/lang/String;)Linm;

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Linm;->a(I)Linm;

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Linm;->a(Z)Linm;

    iget v0, p0, Limv;->P:I

    if-lez v0, :cond_1

    new-instance v0, Linp;

    invoke-direct {v0}, Linp;-><init>()V

    const-string v1, "ERR"

    invoke-virtual {v0, v1}, Linp;->a(Ljava/lang/String;)Linp;

    iget v1, p0, Limv;->P:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Linp;->b(Ljava/lang/String;)Linp;

    invoke-virtual {v2, v0}, Linm;->a(Linp;)Linm;

    :cond_1
    iget-wide v0, p0, Limv;->K:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-lez v0, :cond_2

    new-instance v0, Linp;

    invoke-direct {v0}, Linp;-><init>()V

    const-string v1, "CT"

    invoke-virtual {v0, v1}, Linp;->a(Ljava/lang/String;)Linp;

    iget-wide v3, p0, Limv;->K:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Linp;->b(Ljava/lang/String;)Linp;

    invoke-virtual {v2, v0}, Linm;->a(Linp;)Linm;

    :cond_2
    iget v0, p0, Limv;->I:I

    if-lez v0, :cond_3

    new-instance v0, Linp;

    invoke-direct {v0}, Linp;-><init>()V

    const-string v1, "CONERR"

    invoke-virtual {v0, v1}, Linp;->a(Ljava/lang/String;)Linp;

    iget v1, p0, Limv;->I:I

    int-to-long v3, v1

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Linp;->b(Ljava/lang/String;)Linp;

    invoke-virtual {v2, v0}, Linm;->a(Linp;)Linm;

    :cond_3
    new-instance v0, Linp;

    invoke-direct {v0}, Linp;-><init>()V

    const-string v1, "CONOK"

    invoke-virtual {v0, v1}, Linp;->a(Ljava/lang/String;)Linp;

    iget v1, p0, Limv;->G:I

    int-to-long v3, v1

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Linp;->b(Ljava/lang/String;)Linp;

    invoke-virtual {v2, v0}, Linm;->a(Linp;)Linm;

    iget-object v0, p0, Limv;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Linm;->g(Ljava/lang/String;)Linm;

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v2}, Limv;->a(Linm;)V

    iget-object v1, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V

    invoke-virtual {p0, v2}, Limv;->d(Lizk;)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    return-void

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected a()V
    .locals 2

    iget-object v0, p0, Limv;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizk;

    invoke-virtual {p0, v0}, Limv;->d(Lizk;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 0

    iput p1, p0, Limv;->k:I

    return-void
.end method

.method protected a(ILjava/lang/String;Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Disconnected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method protected a(Linm;)V
    .locals 0

    return-void
.end method

.method protected abstract a(Linn;)V
.end method

.method protected a(Lizk;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Send message  "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Limv;->l:Ljava/lang/String;

    iput-object p2, p0, Limv;->m:Ljava/lang/String;

    return-void
.end method

.method protected abstract a(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method protected a(Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Limv;->x:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 0

    iput-object p1, p0, Limv;->t:Ljavax/net/ssl/SSLSocketFactory;

    return-void
.end method

.method public a(ZILjava/lang/String;II)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public final b(I)V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Limy;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Limy;-><init>(Limv;ILjava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected abstract b(Lizk;)V
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Limv;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method protected b(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method protected c(Lizk;)V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method public final d(Lizk;)V
    .locals 2

    iget-object v1, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Limv;->A:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Limv;->i:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Limv;->i:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Limv;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    iget-boolean v0, p0, Limv;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Limv;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Limv;->B:I

    return v0
.end method

.method public final i()V
    .locals 1

    iget-boolean v0, p0, Limv;->i:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Limv;->F:Z

    new-instance v0, Linj;

    invoke-direct {v0}, Linj;-><init>()V

    invoke-virtual {p0, v0}, Limv;->d(Lizk;)V

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    const-class v1, Limv;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Limv;->i:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Limv;->i:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Limv;->t:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v0, :cond_1

    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocketFactory;

    iput-object v0, p0, Limv;->t:Ljavax/net/ssl/SSLSocketFactory;

    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Limx;

    invoke-direct {v1, p0}, Limx;-><init>(Limv;)V

    const-string v2, "GCMWriter"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Limv;->D:Ljava/lang/Thread;

    iget-object v0, p0, Limv;->D:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Limv;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Limv;->u:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()J
    .locals 4

    iget-boolean v0, p0, Limv;->u:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Limv;->w:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Limv;->w:J

    iget-wide v2, p0, Limv;->v:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Limv;->v:J

    iget-wide v2, p0, Limv;->w:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public final m()J
    .locals 4

    iget-wide v0, p0, Limv;->w:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Limv;->w:J

    iget-wide v2, p0, Limv;->v:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Limv;->w:J

    sub-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final n()Ljava/net/InetAddress;
    .locals 1

    iget-object v0, p0, Limv;->g:Ljava/net/Socket;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Limv;->g:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Limv;->n()Ljava/net/InetAddress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Limv;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v1, p0, Limv;->i:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Limv;->g:Ljava/net/Socket;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connected="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Limv;->g:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",port="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Limv;->g:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getPort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\nstreamId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Limv;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",connects="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Limv;->G:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",connectAttempts="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Limv;->H:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",connectFailed="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Limv;->I:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",packets="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Limv;->L:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->M:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",bytes="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Limv;->N:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->O:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-boolean v1, p0, Limv;->j:Z

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connecting="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Limv;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pending="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Limv;->v:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",host="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Limv;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Limv;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method
