.class public final Laob;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/security/KeyPair;

.field private final d:Ljavax/crypto/SecretKey;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/security/KeyPair;Ljavax/crypto/SecretKey;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laob;->a:Landroid/content/Context;

    iput-object p2, p0, Laob;->b:Ljava/lang/String;

    iput-object p3, p0, Laob;->c:Ljava/security/KeyPair;

    iput-object p4, p0, Laob;->d:Ljavax/crypto/SecretKey;

    return-void
.end method


# virtual methods
.method public final a(Ljcn;)V
    .locals 6

    :try_start_0
    iget-object v0, p0, Laob;->c:Ljava/security/KeyPair;

    iget-object v3, p0, Laob;->d:Ljavax/crypto/SecretKey;

    if-eqz p1, :cond_0

    if-nez v3, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    const-string v1, "AuthZen"

    const-string v2, "Error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v2

    invoke-virtual {v0}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v1

    new-instance v0, Ljcw;

    invoke-direct {v0}, Ljcw;-><init>()V

    invoke-static {v2}, Ljci;->a(Ljava/security/PublicKey;)[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljcw;->b([B)Ljcw;

    move-result-object v0

    new-instance v4, Ljcl;

    invoke-direct {v4}, Ljcl;-><init>()V

    iget-object v5, p1, Ljcn;->a:Ljco;

    invoke-virtual {v5}, Ljco;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljcl;->a(I)Ljcl;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljcl;->b(I)Ljcl;

    move-result-object v4

    invoke-virtual {v4}, Ljcl;->d()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljcw;->a([B)Ljcw;

    move-result-object v0

    instance-of v4, v2, Ljava/security/interfaces/ECPublicKey;

    if-eqz v4, :cond_2

    sget-object v2, Ljcr;->b:Ljcr;

    :goto_1
    sget-object v4, Ljcq;->b:Ljcq;

    iget-object v5, p1, Ljcn;->b:[B

    invoke-virtual/range {v0 .. v5}, Ljcw;->a(Ljava/security/Key;Ljcr;Ljava/security/Key;Ljcq;[B)Ljde;

    move-result-object v0

    invoke-virtual {v0}, Ljde;->d()[B

    move-result-object v0

    iget-object v1, p0, Laob;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Laob;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "AuthZen"

    const-string v2, "Error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    :try_start_2
    instance-of v2, v2, Ljava/security/interfaces/RSAPublicKey;

    if-eqz v2, :cond_3

    sget-object v2, Ljcr;->c:Ljcr;

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/security/InvalidKeyException;

    const-string v1, "Unsupported key type"

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1
.end method
