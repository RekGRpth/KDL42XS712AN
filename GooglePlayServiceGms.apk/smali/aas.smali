.class public final Laas;
.super Laby;
.source "SourceFile"

# interfaces
.implements Laay;
.implements Lacp;


# instance fields
.field final a:Laar;

.field final b:Lacl;

.field final c:Ljava/lang/Object;

.field d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

.field private final g:Lzp;

.field private final h:Ljava/lang/Object;

.field private final i:Landroid/content/Context;

.field private final j:Labd;

.field private final k:Luf;

.field private l:Laby;

.field private m:Z

.field private n:Lze;

.field private o:Lzh;

.field private p:Lzm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Labd;Luf;Lacl;Lzp;Laar;)V
    .locals 1

    invoke-direct {p0}, Laby;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Laas;->h:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Laas;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Laas;->m:Z

    iput-object p5, p0, Laas;->g:Lzp;

    iput-object p6, p0, Laas;->a:Laar;

    iput-object p4, p0, Laas;->b:Lacl;

    iput-object p1, p0, Laas;->i:Landroid/content/Context;

    iput-object p2, p0, Laas;->j:Labd;

    iput-object p3, p0, Laas;->k:Luf;

    return-void
.end method

.method private a(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    .locals 11

    const/4 v3, 0x0

    iget-object v0, p0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Laaw;

    const-string v1, "The ad response must specify one of the supported ad sizes."

    invoke-direct {v0, v1, v3}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    iget-object v0, p0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->m:Ljava/lang/String;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    new-instance v0, Laaw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse the ad size from the ad response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v6, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    array-length v7, v6

    move v2, v3

    :goto_0
    if-ge v2, v7, :cond_5

    aget-object v8, v6, v2

    iget-object v0, p0, Laas;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iget v0, v8, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    const/4 v9, -0x1

    if-ne v0, v9, :cond_2

    iget v0, v8, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    float-to-int v0, v0

    :goto_1
    iget v9, v8, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->c:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_3

    iget v9, v8, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    int-to-float v9, v9

    div-float v1, v9, v1

    float-to-int v1, v1

    :goto_2
    if-ne v4, v0, :cond_4

    if-ne v5, v1, :cond_4

    new-instance v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-direct {v0, v8, v1}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Laaw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse the ad size from the ad response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_2
    iget v0, v8, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    goto :goto_1

    :cond_3
    iget v1, v8, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->c:I

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    new-instance v0, Laaw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The ad size from the ad response was not one of the requested sizes: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method private a(J)V
    .locals 3

    :cond_0
    invoke-direct {p0, p1, p2}, Laas;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Laaw;

    const-string v1, "Timed out waiting for WebView to finish loading."

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    iget-boolean v0, p0, Laas;->m:Z

    if-eqz v0, :cond_0

    return-void
.end method

.method private b(J)Z
    .locals 4

    const-wide/32 v0, 0xea60

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Laas;->c:Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Laaw;

    const-string v1, "Ad request cancelled."

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 29

    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->c:Ljava/lang/Object;

    move-object/from16 v28, v0

    monitor-enter v28

    :try_start_0
    const-string v2, "AdLoaderBackgroundTask started."

    invoke-static {v2}, Lacj;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->k:Luf;

    invoke-virtual {v2}, Luf;->a()Ltz;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Laas;->i:Landroid/content/Context;

    invoke-interface {v2, v3}, Ltz;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    new-instance v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    move-object/from16 v0, p0

    iget-object v3, v0, Laas;->j:Labd;

    invoke-direct {v12, v3, v2}, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;-><init>(Labd;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v5, 0x0

    const/4 v6, -0x2

    const-wide/16 v3, -0x1

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-object v9, v0, Laas;->i:Landroid/content/Context;

    iget-object v2, v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->e:Z

    if-eqz v2, :cond_1

    const-string v2, "Fetching ad response from local ad request service."

    invoke-static {v2}, Lacj;->a(Ljava/lang/String;)V

    new-instance v2, Laba;

    move-object/from16 v0, p0

    invoke-direct {v2, v9, v12, v0}, Laba;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Laay;)V

    iget-object v9, v2, Laby;->e:Ljava/lang/Runnable;

    invoke-static {v9}, Laca;->a(Ljava/lang/Runnable;)V

    :goto_0
    move-object/from16 v0, p0

    iget-object v9, v0, Laas;->h:Ljava/lang/Object;

    monitor-enter v9
    :try_end_1
    .catch Laaw; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    move-object/from16 v0, p0

    iput-object v2, v0, Laas;->l:Laby;

    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->l:Laby;

    if-nez v2, :cond_3

    new-instance v2, Laaw;

    const-string v6, "Could not start the ad request service."

    const/4 v7, 0x0

    invoke-direct {v2, v6, v7}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v9

    throw v2
    :try_end_3
    .catch Laaw; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v2

    :try_start_4
    invoke-virtual {v2}, Laaw;->a()I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_0

    const/4 v7, -0x1

    if-ne v6, v7, :cond_b

    :cond_0
    invoke-virtual {v2}, Laaw;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lacj;->b(Ljava/lang/String;)V

    :goto_1
    new-instance v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    invoke-direct {v2, v6}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    sget-object v2, Laci;->a:Landroid/os/Handler;

    new-instance v7, Laat;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Laat;-><init>(Laas;)V

    invoke-virtual {v2, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-wide/from16 v24, v3

    move-object/from16 v21, v5

    :goto_2
    new-instance v2, Labr;

    iget-object v3, v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    move-object/from16 v0, p0

    iget-object v4, v0, Laas;->b:Lacl;

    move-object/from16 v0, p0

    iget-object v5, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v7, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v7, v7, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v8, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v8, v8, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v9, v9, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->l:I

    move-object/from16 v0, p0

    iget-object v10, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v10, v10, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->k:J

    iget-object v12, v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v13, v13, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->h:Z

    move-object/from16 v0, p0

    iget-object v14, v0, Laas;->p:Lzm;

    if-eqz v14, :cond_c

    move-object/from16 v0, p0

    iget-object v14, v0, Laas;->p:Lzm;

    iget-object v14, v14, Lzm;->b:Lzg;

    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Laas;->p:Lzm;

    if-eqz v15, :cond_d

    move-object/from16 v0, p0

    iget-object v15, v0, Laas;->p:Lzm;

    iget-object v15, v15, Lzm;->c:Lzs;

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->p:Lzm;

    move-object/from16 v16, v0

    if-eqz v16, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->p:Lzm;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lzm;->d:Ljava/lang/String;

    move-object/from16 v16, v0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->o:Lzh;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->p:Lzm;

    move-object/from16 v18, v0

    if-eqz v18, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->p:Lzm;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lzm;->e:Lzj;

    move-object/from16 v18, v0

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->i:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->g:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->n:J

    move-wide/from16 v26, v0

    invoke-direct/range {v2 .. v27}, Labr;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lacl;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLzg;Lzs;Ljava/lang/String;Lzh;Lzj;JLcom/google/android/gms/ads/internal/client/AdSizeParcel;JJJ)V

    sget-object v3, Laci;->a:Landroid/os/Handler;

    new-instance v4, Laau;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Laau;-><init>(Laas;Labr;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit v28
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void

    :cond_1
    :try_start_5
    const-string v2, "Fetching ad response from remote ad request service."

    invoke-static {v2}, Lacj;->a(Ljava/lang/String;)V

    invoke-static {v9}, Lbbv;->a(Landroid/content/Context;)I

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Failed to connect to remote ad request service."

    invoke-static {v2}, Lacj;->d(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_2
    new-instance v2, Labb;

    move-object/from16 v0, p0

    invoke-direct {v2, v9, v12, v0}, Labb;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Laay;)V
    :try_end_5
    .catch Laaw; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v28

    throw v2

    :cond_3
    :try_start_6
    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_4
    :try_start_7
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Laas;->b(J)Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Laaw;

    const-string v6, "Timed out waiting for ad response."

    const/4 v7, 0x2

    invoke-direct {v2, v6, v7}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v9, v0, Laas;->h:Ljava/lang/Object;

    monitor-enter v9
    :try_end_7
    .catch Laaw; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    const/4 v2, 0x0

    :try_start_8
    move-object/from16 v0, p0

    iput-object v2, v0, Laas;->l:Laby;

    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->e:I

    const/4 v9, -0x2

    if-eq v2, v9, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->e:I

    const/4 v9, -0x3

    if-eq v2, v9, :cond_6

    new-instance v2, Laaw;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "There was a problem getting an ad response. ErrorCode: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v7, v7, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->e:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v7, v7, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->e:I

    invoke-direct {v2, v6, v7}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v2

    :catchall_2
    move-exception v2

    monitor-exit v9

    throw v2

    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->e:I

    const/4 v9, -0x3

    if-eq v2, v9, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v2, Laaw;

    const-string v6, "No fill from ad server."

    const/4 v7, 0x3

    invoke-direct {v2, v6, v7}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->h:Z
    :try_end_9
    .catch Laaw; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v2, :cond_8

    :try_start_a
    new-instance v2, Lzh;

    move-object/from16 v0, p0

    iget-object v9, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v9, v9, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->c:Ljava/lang/String;

    invoke-direct {v2, v9}, Lzh;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Laas;->o:Lzh;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_1
    .catch Laaw; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_8
    :try_start_b
    iget-object v2, v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Laas;->a(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v5

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->h:Z

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v9, v0, Laas;->h:Ljava/lang/Object;

    monitor-enter v9
    :try_end_b
    .catch Laaw; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    new-instance v2, Lze;

    move-object/from16 v0, p0

    iget-object v10, v0, Laas;->i:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Laas;->g:Lzp;

    move-object/from16 v0, p0

    iget-object v13, v0, Laas;->o:Lzh;

    invoke-direct {v2, v10, v12, v11, v13}, Lze;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lzp;Lzh;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Laas;->n:Lze;

    monitor-exit v9
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->n:Lze;

    invoke-virtual {v2, v7, v8}, Lze;->a(J)Lzm;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Laas;->p:Lzm;

    move-object/from16 v0, p0

    iget-object v2, v0, Laas;->p:Lzm;

    iget v2, v2, Lzm;->a:I

    packed-switch v2, :pswitch_data_0

    new-instance v2, Laaw;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unexpected mediation result: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Laas;->p:Lzm;

    iget v7, v7, Lzm;->a:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v2, v6, v7}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v2

    :catch_1
    move-exception v2

    new-instance v2, Laaw;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Could not parse mediation config: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v7, v7, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v2, v6, v7}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v2

    :catchall_3
    move-exception v2

    monitor-exit v9

    throw v2

    :pswitch_0
    move-wide/from16 v24, v3

    move-object/from16 v21, v5

    goto/16 :goto_2

    :pswitch_1
    new-instance v2, Laaw;

    const-string v6, "No fill from any mediation ad networks."

    const/4 v7, 0x3

    invoke-direct {v2, v6, v7}, Laaw;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_a
    sget-object v2, Laci;->a:Landroid/os/Handler;

    new-instance v9, Laav;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Laav;-><init>(Laas;)V

    invoke-virtual {v2, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Laas;->a(J)V
    :try_end_d
    .catch Laaw; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move-wide/from16 v24, v3

    move-object/from16 v21, v5

    goto/16 :goto_2

    :cond_b
    :try_start_e
    invoke-virtual {v2}, Laaw;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lacj;->d(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto/16 :goto_1

    :cond_c
    const/4 v14, 0x0

    goto/16 :goto_3

    :cond_d
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_e
    const/16 v16, 0x0

    goto/16 :goto_5

    :cond_f
    const/16 v18, 0x0

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lacl;)V
    .locals 2

    iget-object v1, p0, Laas;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "WebView finished loading."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Laas;->m:Z

    iget-object v0, p0, Laas;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V
    .locals 2

    iget-object v1, p0, Laas;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "Received ad response."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    iput-object p1, p0, Laas;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v0, p0, Laas;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 6

    iget-object v1, p0, Laas;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Laas;->l:Laby;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laas;->l:Laby;

    invoke-virtual {v0}, Laby;->f()V

    :cond_0
    iget-object v0, p0, Laas;->b:Lacl;

    invoke-virtual {v0}, Lacl;->stopLoading()V

    iget-object v0, p0, Laas;->b:Lacl;

    invoke-static {v0}, Lacd;->a(Landroid/webkit/WebView;)V

    iget-object v0, p0, Laas;->n:Lze;

    if-eqz v0, :cond_3

    iget-object v0, p0, Laas;->n:Lze;

    iget-object v2, v0, Lze;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, v0, Lze;->b:Z

    iget-object v3, v0, Lze;->c:Lzk;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lze;->c:Lzk;

    iget-object v4, v3, Lzk;->e:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, v3, Lzk;->g:Lzs;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lzk;->g:Lzs;

    invoke-interface {v0}, Lzs;->c()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    const/4 v0, -0x1

    :try_start_3
    iput v0, v3, Lzk;->h:I

    iget-object v0, v3, Lzk;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_3
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    return-void

    :catch_0
    move-exception v0

    :try_start_6
    const-string v5, "Could not destroy mediation adapter."

    invoke-static {v5, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_7
    monitor-exit v4

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v2

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0
.end method
