.class public final Lcle;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcje;

.field public static final b:Lcje;

.field public static final c:Lcje;

.field public static final d:Lcje;

.field public static final e:Lcje;

.field public static final f:Lcje;

.field public static final g:Lcje;

.field public static final h:Lcje;

.field public static final i:Lcje;

.field public static final j:Lcja;

.field public static final k:Lcje;

.field public static final l:Lcja;

.field public static final m:Lcje;

.field public static final n:Lcje;

.field public static final o:Lcje;

.field public static final p:Lcje;

.field public static final q:Lcje;

.field public static final r:Lcje;

.field public static final s:Lcje;

.field public static final t:Lcje;

.field public static final u:Lcje;

.field public static final v:Lcje;

.field public static final w:Lcje;

.field public static final x:Lcje;

.field public static final y:Lcje;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const v3, 0x3e8fa0

    const v2, 0x419ce0

    sget-object v0, Lclh;->c:Lclh;

    sput-object v0, Lcle;->a:Lcje;

    new-instance v0, Lcld;

    const-string v1, "title"

    invoke-direct {v0, v1, v3}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->b:Lcje;

    new-instance v0, Lcld;

    const-string v1, "mimeType"

    invoke-direct {v0, v1, v3}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->c:Lcje;

    new-instance v0, Lcku;

    const-string v1, "starred"

    invoke-direct {v0, v1, v3}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->d:Lcje;

    new-instance v0, Lclf;

    const-string v1, "trashed"

    invoke-direct {v0, v1}, Lclf;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcle;->e:Lcje;

    new-instance v0, Lcku;

    const-string v1, "isEditable"

    invoke-direct {v0, v1, v3}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->f:Lcje;

    new-instance v0, Lcku;

    const-string v1, "isPinned"

    invoke-direct {v0, v1, v3}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->g:Lcje;

    new-instance v0, Lcku;

    const-string v1, "isAppData"

    invoke-direct {v0, v1, v2}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->h:Lcje;

    new-instance v0, Lcku;

    const-string v1, "isShared"

    invoke-direct {v0, v1, v2}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->i:Lcje;

    new-instance v0, Lcla;

    const-string v1, "parents"

    invoke-direct {v0, v1}, Lcla;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcle;->j:Lcja;

    new-instance v0, Lcld;

    const-string v1, "alternateLink"

    invoke-direct {v0, v1, v2}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->k:Lcje;

    new-instance v0, Lclc;

    const-string v1, "ownerNames"

    invoke-direct {v0, v1}, Lclc;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcle;->l:Lcja;

    new-instance v0, Lcld;

    const-string v1, "description"

    invoke-direct {v0, v1, v2}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->m:Lcje;

    new-instance v0, Lcku;

    const-string v1, "isCopyable"

    invoke-direct {v0, v1, v2}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->n:Lcje;

    new-instance v0, Lcld;

    const-string v1, "embedLink"

    invoke-direct {v0, v1, v2}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->o:Lcje;

    new-instance v0, Lcld;

    const-string v1, "fileExtension"

    invoke-direct {v0, v1, v2}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->p:Lcje;

    new-instance v0, Lcky;

    const-string v1, "fileSize"

    invoke-direct {v0, v1}, Lcky;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcle;->q:Lcje;

    new-instance v0, Lcku;

    const-string v1, "isViewed"

    invoke-direct {v0, v1, v2}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->r:Lcje;

    new-instance v0, Lcku;

    const-string v1, "isRestricted"

    invoke-direct {v0, v1, v2}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->s:Lcje;

    new-instance v0, Lcld;

    const-string v1, "originalFilename"

    invoke-direct {v0, v1, v2}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->t:Lcje;

    new-instance v0, Lcky;

    const-string v1, "quotaBytesUsed"

    invoke-direct {v0, v1}, Lcky;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcle;->u:Lcje;

    new-instance v0, Lcld;

    const-string v1, "webContentLink"

    invoke-direct {v0, v1, v2}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->v:Lcje;

    new-instance v0, Lcld;

    const-string v1, "webViewLink"

    invoke-direct {v0, v1, v2}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->w:Lcje;

    new-instance v0, Lcld;

    const-string v1, "indexableText"

    invoke-direct {v0, v1, v2}, Lcld;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->x:Lcje;

    new-instance v0, Lcku;

    const-string v1, "hasThumbnail"

    invoke-direct {v0, v1, v2}, Lcku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcle;->y:Lcje;

    return-void
.end method
