.class public final Lgpe;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lfud;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:J


# instance fields
.field private Y:Z

.field private Z:Lcom/google/android/gms/plus/model/posts/Comment;

.field private aa:Lgph;

.field private ab:Ljava/lang/String;

.field private final c:Lftz;

.field private d:Lftx;

.field private e:Lgpg;

.field private f:Z

.field private g:Lfaj;

.field private h:Lgpf;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.settings"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "https://www.googleapis.com/auth/plus.pages.manage"

    aput-object v2, v0, v1

    sput-object v0, Lgpe;->a:[Ljava/lang/String;

    sget-object v0, Lfsr;->ac:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lgpe;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lgpe;-><init>(Lftz;)V

    return-void
.end method

.method private constructor <init>(Lftz;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-object p1, p0, Lgpe;->c:Lftz;

    return-void
.end method

.method static synthetic J()J
    .locals 2

    sget-wide v0, Lgpe;->b:J

    return-wide v0
.end method

.method public static a(Ljava/lang/String;)Lgpe;
    .locals 3

    sget-object v0, Lftx;->a:Lftz;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "specified_account_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lgpe;

    invoke-direct {v2, v0}, Lgpe;-><init>(Lftz;)V

    invoke-virtual {v2, v1}, Lgpe;->g(Landroid/os/Bundle;)V

    return-object v2
.end method

.method static synthetic a(Lgpe;)Lgph;
    .locals 1

    iget-object v0, p0, Lgpe;->aa:Lgph;

    return-object v0
.end method

.method static synthetic a(Lgpe;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lgpe;->i:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lgpe;)Lftx;
    .locals 1

    iget-object v0, p0, Lgpe;->d:Lftx;

    return-object v0
.end method

.method static synthetic c(Lgpe;)Lfaj;
    .locals 1

    iget-object v0, p0, Lgpe;->g:Lfaj;

    return-object v0
.end method

.method static synthetic d(Lgpe;)Z
    .locals 1

    iget-boolean v0, p0, Lgpe;->Y:Z

    return v0
.end method

.method static synthetic e(Lgpe;)Lcom/google/android/gms/plus/model/posts/Comment;
    .locals 1

    iget-object v0, p0, Lgpe;->Z:Lcom/google/android/gms/plus/model/posts/Comment;

    return-object v0
.end method

.method static synthetic f(Lgpe;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpe;->f:Z

    return v0
.end method

.method static synthetic g(Lgpe;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgpe;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lgpe;->aa:Lgph;

    return-void
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgpe;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lgph;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lgph;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lgph;

    iput-object p1, p0, Lgpe;->aa:Lgph;

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 1

    iget-boolean v0, p0, Lgpe;->Y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpe;->aa:Lgph;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpe;->aa:Lgph;

    invoke-interface {v0, p1}, Lgph;->b(Lbbo;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgpe;->Y:Z

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 6

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lgpe;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgpe;->ab:Ljava/lang/String;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lgpe;->i:Ljava/lang/String;

    iget-object v2, p0, Lgpe;->aa:Lgph;

    invoke-interface {v2}, Lgph;->e()Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lbda;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lgpe;->ab:Ljava/lang/String;

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 2

    iget-boolean v0, p0, Lgpe;->Y:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "One comment at a time please"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpe;->Y:Z

    iput-object p1, p0, Lgpe;->Z:Lcom/google/android/gms/plus/model/posts/Comment;

    iget-object v0, p0, Lgpe;->d:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpe;->e:Lgpg;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lgpg;->b_(Landroid/os/Bundle;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lgpe;->d:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpe;->d:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lgpe;->q()V

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lgpe;->f:Z

    return v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 7

    const/4 v0, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lgpe;->aa:Lgph;

    invoke-interface {v2}, Lgph;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lgpe;->ab:Ljava/lang/String;

    iget-object v2, p0, Lgpe;->aa:Lgph;

    invoke-interface {v2}, Lgph;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lbov;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v4, "specified_account_name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lgpe;->ab:Ljava/lang/String;

    invoke-static {v1, v3, v4, v2}, Lgpq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lfwa;

    invoke-direct {v3, v1}, Lfwa;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lgpe;->ab:Ljava/lang/String;

    iput-object v4, v3, Lfwa;->c:Ljava/lang/String;

    iput-object v2, v3, Lfwa;->a:Ljava/lang/String;

    iget-object v2, p0, Lgpe;->aa:Lgph;

    invoke-interface {v2}, Lgph;->e()Lgpx;

    move-result-object v2

    iget-object v2, v2, Lgpx;->m:Ljava/lang/String;

    iput-object v2, v3, Lfwa;->e:Ljava/lang/String;

    sget-object v2, Lgpe;->a:[Ljava/lang/String;

    invoke-virtual {v3, v2}, Lfwa;->a([Ljava/lang/String;)Lfwa;

    move-result-object v2

    iget-object v3, p0, Lgpe;->aa:Lgph;

    invoke-interface {v3}, Lgph;->e()Lgpx;

    move-result-object v3

    iget-object v3, v3, Lgpx;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lgpq;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-array v3, v0, [Ljava/lang/String;

    iput-object v3, v2, Lfwa;->d:[Ljava/lang/String;

    :cond_2
    iget-object v3, p0, Lgpe;->d:Lftx;

    if-nez v3, :cond_3

    new-instance v3, Lgpg;

    invoke-direct {v3, p0, v0}, Lgpg;-><init>(Lgpe;B)V

    iput-object v3, p0, Lgpe;->e:Lgpg;

    iget-object v3, p0, Lgpe;->c:Lftz;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v2

    iget-object v5, p0, Lgpe;->e:Lgpg;

    iget-object v6, p0, Lgpe;->e:Lgpg;

    invoke-interface {v3, v4, v2, v5, v6}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v2

    iput-object v2, p0, Lgpe;->d:Lftx;

    iget-object v2, p0, Lgpe;->d:Lftx;

    invoke-interface {v2}, Lftx;->a()V

    :cond_3
    iget-object v2, p0, Lgpe;->g:Lfaj;

    if-nez v2, :cond_0

    iget-object v2, p0, Lgpe;->aa:Lgph;

    invoke-interface {v2}, Lgph;->e()Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    :try_start_0
    iget-object v2, p0, Lgpe;->aa:Lgph;

    invoke-interface {v2}, Lgph;->e()Lgpx;

    move-result-object v2

    iget-object v2, v2, Lgpx;->m:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :goto_1
    new-instance v2, Lgpf;

    invoke-direct {v2, p0, v0}, Lgpf;-><init>(Lgpe;B)V

    iput-object v2, p0, Lgpe;->h:Lgpf;

    iget-object v0, p0, Lgpe;->c:Lftz;

    iget-object v2, p0, Lgpe;->h:Lgpf;

    iget-object v3, p0, Lgpe;->h:Lgpf;

    iget-object v5, p0, Lgpe;->ab:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lftz;->a(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)Lfaj;

    move-result-object v0

    iput-object v0, p0, Lgpe;->g:Lfaj;

    goto/16 :goto_0

    :catch_0
    move-exception v2

    :cond_4
    move v4, v0

    goto :goto_1
.end method

.method public final y()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    iget-object v0, p0, Lgpe;->d:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpe;->d:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgpe;->d:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    iput-object v1, p0, Lgpe;->d:Lftx;

    iget-object v0, p0, Lgpe;->g:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lgpe;->g:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lgpe;->g:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    :cond_3
    iput-object v1, p0, Lgpe;->g:Lfaj;

    iput-object v1, p0, Lgpe;->i:Ljava/lang/String;

    return-void
.end method
