.class public final Lgzw;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 0

    iput-object p1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    return-void
.end method

.method public final a(Liot;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Liot;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Liot;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Z)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    iget-object v1, p1, Liot;->d:Lioh;

    iget-object v2, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lioh;

    move-result-object v2

    iget-object v3, p1, Liot;->e:Lioh;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a(Lioh;Lioh;Lioh;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Z)V

    :cond_0
    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;

    move-result-object v0

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "create_to_ui_populated"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;

    move-result-object v1

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->d(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;

    :cond_1
    return-void
.end method

.method public final a(Liov;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Z)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v4, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget v0, p1, Liov;->a:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "make_payment_failure"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Lgxg;

    invoke-interface {v0, v1}, Lgxg;->a(Lioj;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iput-object v1, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    const v2, 0x7f0b011c    # com.google.android.gms.R.string.wallet_declined_card_error_message

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "make_payment_success"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;

    move-result-object v0

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "click_to_activity_result"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;

    move-result-object v1

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;

    :cond_0
    iget v0, p1, Liov;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->n(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Z

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "make_payment_failure"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lgzw;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->t(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    return-void
.end method
