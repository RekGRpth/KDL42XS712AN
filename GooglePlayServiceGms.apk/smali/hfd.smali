.class public final Lhfd;
.super Lhfb;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhfo;

.field private final c:Lhce;

.field private final d:Lhef;

.field private final e:Lgtm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhfo;Lhef;Lgtm;)V
    .locals 2

    invoke-direct {p0}, Lhfb;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhfd;->a:Landroid/content/Context;

    iput-object p2, p0, Lhfd;->b:Lhfo;

    new-instance v0, Lhce;

    const-string v1, "NetworkOwInternalServic"

    invoke-direct {v0, p1, v1}, Lhce;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lhfd;->c:Lhce;

    iput-object p3, p0, Lhfd;->d:Lhef;

    iput-object p4, p0, Lhfd;->e:Lgtm;

    return-void
.end method

.method static synthetic a(Lhfd;)Lgtm;
    .locals 1

    iget-object v0, p0, Lhfd;->e:Lgtm;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v0

    invoke-static {v0}, Lhfx;->a(I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lhfd;)Lhef;
    .locals 1

    iget-object v0, p0, Lhfd;->d:Lhef;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lhgi;->c(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lhfd;)Lhfo;
    .locals 1

    iget-object v0, p0, Lhfd;->b:Lhfo;

    return-object v0
.end method

.method static synthetic d(Lhfd;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lhfd;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 10

    iget-object v0, p0, Lhfd;->a:Landroid/content/Context;

    invoke-static {v0}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    sget-object v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->c()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lhfd;->d:Lhef;

    invoke-virtual {v0, v6}, Lhef;->a(Ljava/lang/String;)Ljan;

    move-result-object v4

    if-nez v4, :cond_1

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    sget-object v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    new-array v0, v0, [B

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    const-wide v2, 0xffffffffffffL

    and-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/16 v1, 0x30

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-static {v5}, Lhgi;->c(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Ljau;

    move-result-object v3

    iget-object v9, p0, Lhfd;->c:Lhce;

    new-instance v0, Lhfg;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v2

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lhfg;-><init>(Lhfd;Landroid/accounts/Account;Ljau;Ljan;Lcom/google/android/gms/wallet/shared/ApplicationParameters;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;)V

    invoke-virtual {v9, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v2

    new-instance v1, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    iget-object v0, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v1, v3, v4, v2}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(JLcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v0, p0, Lhfd;->a:Landroid/content/Context;

    invoke-static {v0}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    :goto_0
    return-object v0

    :cond_0
    iget-object v6, p0, Lhfd;->c:Lhce;

    new-instance v0, Lhfh;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhfh;-><init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v0, p0, Lhfd;->a:Landroid/content/Context;

    invoke-static {v0}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    :goto_0
    return-object v0

    :cond_0
    iget-object v6, p0, Lhfd;->c:Lhce;

    new-instance v0, Lhfj;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhfj;-><init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v0, p0, Lhfd;->a:Landroid/content/Context;

    invoke-static {v0}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    :goto_0
    return-object v0

    :cond_0
    iget-object v6, p0, Lhfd;->c:Lhce;

    new-instance v0, Lhfi;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhfi;-><init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhfd;->c:Lhce;

    new-instance v0, Lhff;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhff;-><init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhfd;->c:Lhce;

    new-instance v0, Lhfe;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhfe;-><init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    const/16 v2, 0xf

    if-eq v1, v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljbb;

    invoke-static {v0}, Lhfx;->a(Ljbb;)Lizz;

    move-result-object v1

    const/4 v0, 0x1

    iput v0, v1, Lizz;->d:I

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x13

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto :goto_0
.end method
