.class public Lgxn;
.super Lgyg;
.source "SourceFile"

# interfaces
.implements Lgyf;


# instance fields
.field private n:Z

.field private o:Ljava/util/HashSet;

.field protected w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected x:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lgyg;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lgxn;->o:Ljava/util/HashSet;

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const-string v0, "LoginRequiredSecureFrag"

    const-string v1, "onAuthTokensError: Network failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3, v4}, Lgxn;->a(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "LoginRequiredSecureFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAuthTokensError: Status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3, v4}, Lgxn;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected final a(ILandroid/content/Intent;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lgxn;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lgxn;->finish()V

    return-void
.end method

.method public final e()V
    .locals 0

    invoke-direct {p0}, Lgxn;->g()V

    return-void
.end method

.method public final f()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgxn;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lgyg;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lgxn;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Activity requires buyFlowConfig extra!"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lgxn;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Activity requires account extra!"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lgxn;->x:Landroid/accounts/Account;

    const-string v0, "com.google.android.gms.wallet.localMode"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgxn;->n:Z

    if-eqz p1, :cond_0

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lgxn;->o:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iget-boolean v0, p0, Lgxn;->n:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lgxn;->o:Ljava/util/HashSet;

    iget-object v1, p0, Lgxn;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lgxn;->o:Ljava/util/HashSet;

    iget-object v1, p0, Lgxn;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lgxn;->x:Landroid/accounts/Account;

    iget-object v1, p0, Lgxn;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-static {v1}, Lhgi;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgyd;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lgyd;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lgxn;->g()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lgyg;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lgxn;->o:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method
