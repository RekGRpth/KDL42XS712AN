.class final enum Lbjb;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbjb;

.field public static final enum b:Lbjb;

.field public static final enum c:Lbjb;

.field public static final enum d:Lbjb;

.field private static final synthetic e:[Lbjb;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lbjb;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lbjb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbjb;->a:Lbjb;

    new-instance v0, Lbjb;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, Lbjb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbjb;->b:Lbjb;

    new-instance v0, Lbjb;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lbjb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbjb;->c:Lbjb;

    new-instance v0, Lbjb;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lbjb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbjb;->d:Lbjb;

    const/4 v0, 0x4

    new-array v0, v0, [Lbjb;

    sget-object v1, Lbjb;->a:Lbjb;

    aput-object v1, v0, v2

    sget-object v1, Lbjb;->b:Lbjb;

    aput-object v1, v0, v3

    sget-object v1, Lbjb;->c:Lbjb;

    aput-object v1, v0, v4

    sget-object v1, Lbjb;->d:Lbjb;

    aput-object v1, v0, v5

    sput-object v0, Lbjb;->e:[Lbjb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbjb;
    .locals 1

    const-class v0, Lbjb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbjb;

    return-object v0
.end method

.method public static values()[Lbjb;
    .locals 1

    sget-object v0, Lbjb;->e:[Lbjb;

    invoke-virtual {v0}, [Lbjb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbjb;

    return-object v0
.end method
