.class public final Lbla;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lfea;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 2

    invoke-interface {p0}, Lfea;->c()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lfea;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lfea;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Lfea;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lfea;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lfee;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 3

    invoke-interface {p0}, Lfee;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lfee;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Lfee;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lfee;
    .locals 1

    new-instance v0, Lblb;

    invoke-direct {v0, p0}, Lblb;-><init>(Lcom/google/android/gms/common/people/data/AudienceMember;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/people/data/Audience;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "Audience must not be null."

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v0

    if-eq v0, v1, :cond_0

    const/4 v6, 0x4

    if-ne v0, v6, :cond_1

    :cond_0
    move v0, v1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method
