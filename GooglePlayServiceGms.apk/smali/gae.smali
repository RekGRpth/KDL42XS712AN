.class public final Lgae;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/content/ContentResolver;

.field final synthetic b:Lcom/google/android/gms/plus/provider/PlusProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/provider/PlusProvider;Landroid/content/ContentResolver;)V
    .locals 0

    iput-object p1, p0, Lgae;->b:Lcom/google/android/gms/plus/provider/PlusProvider;

    iput-object p2, p0, Lgae;->a:Landroid/content/ContentResolver;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a([Landroid/accounts/Account;)Ljava/lang/Void;
    .locals 2

    iget-object v0, p0, Lgae;->a:Landroid/content/ContentResolver;

    const-string v1, "com.google.android.gms.plus"

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/provider/PlusProvider;->a([Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    const/4 v0, 0x0

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    throw v0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/accounts/Account;

    invoke-direct {p0, p1}, Lgae;->a([Landroid/accounts/Account;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
