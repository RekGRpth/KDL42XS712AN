.class public final Lggl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public final c:Ljava/util/Set;

.field private d:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lggl;->c:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Lggk;
    .locals 6

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    iget-object v1, p0, Lggl;->c:Ljava/util/Set;

    iget-object v2, p0, Lggl;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

    iget-object v3, p0, Lggl;->a:Ljava/lang/String;

    iget v4, p0, Lggl;->e:I

    iget-object v5, p0, Lggl;->b:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method

.method public final a(I)Lggl;
    .locals 2

    iput p1, p0, Lggl;->e:I

    iget-object v0, p0, Lggl;->c:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lggg;)Lggl;
    .locals 2

    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

    iput-object p1, p0, Lggl;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

    iget-object v0, p0, Lggl;->c:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
