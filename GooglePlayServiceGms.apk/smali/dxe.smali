.class public abstract Ldxe;
.super Ldvp;
.source "SourceFile"

# interfaces
.implements Ledt;


# instance fields
.field protected Z:Leds;

.field protected aa:Z

.field protected ab:Landroid/support/v4/widget/SwipeRefreshLayout;

.field protected ac:Lje;

.field private ad:Landroid/database/ContentObserver;

.field protected i:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ldvp;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldxe;->aa:Z

    return-void
.end method


# virtual methods
.method public final P()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Ldxe;->aa:Z

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final Q()V
    .locals 2

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Z)V

    :cond_0
    return-void
.end method

.method public R()V
    .locals 0

    return-void
.end method

.method protected final S()V
    .locals 4

    iget-object v0, p0, Ldxe;->ad:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ldxf;

    invoke-direct {v0, p0}, Ldxf;-><init>(Ldxe;)V

    iput-object v0, p0, Ldxe;->ad:Landroid/database/ContentObserver;

    iget-object v0, p0, Ldxe;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Ldei;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Ldxe;->ad:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method protected final T()V
    .locals 2

    iget-object v0, p0, Ldxe;->ad:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldxe;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Ldxe;->ad:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Ldxe;->ad:Landroid/database/ContentObserver;

    :cond_0
    return-void
.end method

.method protected U()V
    .locals 0

    return-void
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    sget v0, Lxc;->l:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    sget v0, Lxa;->aU:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v2, p0, Ldxe;->aa:Z

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    sget v2, Lwx;->f:I

    sget v3, Lwx;->h:I

    sget v4, Lwx;->i:I

    sget v5, Lwx;->g:I

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(IIII)V

    iget-boolean v0, p0, Ldxe;->aa:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldxe;->ac:Lje;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v2, p0, Ldxe;->ac:Lje;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Lje;)V

    :cond_0
    return-object v1
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    iput-object p1, p0, Ldxe;->i:Landroid/view/LayoutInflater;

    invoke-virtual {p0, p1, p2}, Ldxe;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Leds;

    invoke-direct {v1, v0, p0}, Leds;-><init>(Landroid/view/View;Ledt;)V

    iput-object v1, p0, Ldxe;->Z:Leds;

    iget-object v1, p0, Ldxe;->Z:Leds;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Leds;->a(I)V

    return-object v0
.end method

.method public final a(Lje;)V
    .locals 2

    iput-object p1, p0, Ldxe;->ac:Lje;

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldxe;->ab:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Ldxe;->ac:Lje;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Lje;)V

    :cond_0
    return-void
.end method
