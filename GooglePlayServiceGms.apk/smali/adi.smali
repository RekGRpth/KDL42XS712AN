.class public final Ladi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ladm;


# static fields
.field private static final a:Ladi;


# instance fields
.field private b:Ladk;

.field private c:Lxv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ladi;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Ladi;-><init>(Landroid/content/Context;)V

    sput-object v0, Ladi;->a:Ladi;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ladk;

    invoke-direct {v0, p1}, Ladk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladi;->b:Ladk;

    return-void
.end method

.method public static a()Ladi;
    .locals 1

    sget-object v0, Ladi;->a:Ladi;

    return-object v0
.end method

.method private b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Ladi;->b:Ladk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladi;->b:Ladk;

    invoke-virtual {v0}, Ladk;->b()V

    iget-object v0, p0, Ladi;->b:Ladk;

    invoke-virtual {v0, p0}, Ladk;->b(Ladm;)V

    iput-object v1, p0, Ladi;->b:Ladk;

    :cond_0
    iput-object v1, p0, Ladi;->c:Lxv;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Ladi;->c:Lxv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lxv;->c(Z)V

    invoke-direct {p0}, Ladi;->b()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Ladi;->c:Lxv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lxv;->c(Z)V

    invoke-direct {p0}, Ladi;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lxv;)V
    .locals 1

    iput-object p1, p0, Ladi;->c:Lxv;

    iget-object v0, p0, Ladi;->b:Ladk;

    invoke-virtual {v0, p0}, Ladk;->a(Ladm;)V

    iget-object v0, p0, Ladi;->b:Ladk;

    invoke-virtual {v0}, Ladk;->a()V

    iget-object v0, p0, Ladi;->b:Ladk;

    invoke-virtual {v0}, Ladk;->e()V

    return-void
.end method
