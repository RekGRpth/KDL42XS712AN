.class public final enum Lbqr;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbqr;

.field public static final enum b:Lbqr;

.field public static final enum c:Lbqr;

.field public static final enum d:Lbqr;

.field public static final e:Ljava/util/Set;

.field private static final f:Landroid/util/SparseArray;

.field private static final synthetic j:[Lbqr;


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v11, 0x4

    const/4 v14, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lbqr;

    const-string v1, "FILE"

    const-string v4, "https://www.googleapis.com/auth/drive.file"

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lbqr;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v0, Lbqr;->a:Lbqr;

    new-instance v4, Lbqr;

    const-string v5, "APPDATA"

    const-string v8, "https://www.googleapis.com/auth/drive.appdata"

    move v6, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lbqr;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v4, Lbqr;->b:Lbqr;

    new-instance v5, Lbqr;

    const-string v6, "FULL"

    const-string v9, "https://www.googleapis.com/auth/drive"

    move v8, v14

    move v10, v2

    invoke-direct/range {v5 .. v10}, Lbqr;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v5, Lbqr;->c:Lbqr;

    new-instance v8, Lbqr;

    const-string v9, "APPS"

    const-string v12, "https://www.googleapis.com/auth/drive.apps"

    move v10, v14

    move v13, v3

    invoke-direct/range {v8 .. v13}, Lbqr;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v8, Lbqr;->d:Lbqr;

    new-array v0, v11, [Lbqr;

    sget-object v1, Lbqr;->a:Lbqr;

    aput-object v1, v0, v2

    sget-object v1, Lbqr;->b:Lbqr;

    aput-object v1, v0, v3

    sget-object v1, Lbqr;->c:Lbqr;

    aput-object v1, v0, v7

    sget-object v1, Lbqr;->d:Lbqr;

    aput-object v1, v0, v14

    sput-object v0, Lbqr;->j:[Lbqr;

    sget-object v0, Lbqr;->c:Lbqr;

    sget-object v1, Lbqr;->b:Lbqr;

    sget-object v3, Lbqr;->d:Lbqr;

    invoke-static {v0, v1, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lbqr;->e:Ljava/util/Set;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lbqr;->f:Landroid/util/SparseArray;

    invoke-static {}, Lbqr;->values()[Lbqr;

    move-result-object v0

    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    sget-object v4, Lbqr;->f:Landroid/util/SparseArray;

    iget v5, v3, Lbqr;->h:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lbqr;->h:I

    iput-object p4, p0, Lbqr;->g:Ljava/lang/String;

    iput-boolean p5, p0, Lbqr;->i:Z

    return-void
.end method

.method public static a(I)Lbqr;
    .locals 1

    sget-object v0, Lbqr;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqr;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbqr;
    .locals 1

    const-class v0, Lbqr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbqr;

    return-object v0
.end method

.method public static values()[Lbqr;
    .locals 1

    sget-object v0, Lbqr;->j:[Lbqr;

    invoke-virtual {v0}, [Lbqr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbqr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lbqr;->h:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbqr;->g:Ljava/lang/String;

    return-object v0
.end method
