.class public final Ldye;
.super Ldwx;
.source "SourceFile"


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Landroid/view/LayoutInflater;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldxm;)V
    .locals 1

    const v0, 0x7f09000f    # com.google.android.gms.R.integer.games_mixed_tile_num_columns

    invoke-direct {p0, p1, v0}, Ldwx;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Ldye;->g:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Ldxm;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldye;->h:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 8

    check-cast p4, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyf;

    iget-object v1, v0, Ldyf;->f:Ldye;

    iget-object v1, v1, Ldye;->i:Ljava/lang/String;

    const-string v2, "You must call setCurrentPlayerId() before any bindView() calls come in"

    invoke-static {v1, v2}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, v0, Ldyf;->f:Ldye;

    iget-object v1, v1, Ldye;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    if-nez p4, :cond_1

    const/4 v1, 0x1

    move v5, v1

    :goto_0
    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p4, :cond_10

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Ldyf;->f:Ldye;

    iget-object v3, v3, Ldye;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    move v3, v1

    move v4, v2

    :goto_3
    if-eqz v4, :cond_4

    iget-object v1, v0, Ldyf;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldyf;->b:Landroid/widget/TextView;

    iget-object v2, v0, Ldyf;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    const/4 v6, 0x1

    invoke-virtual {v1, v2, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :goto_4
    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldyf;->d:Landroid/database/CharArrayBuffer;

    iget-object v2, v2, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v6, 0x0

    iget-object v7, v0, Ldyf;->d:Landroid/database/CharArrayBuffer;

    iget v7, v7, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v2, v6, v7}, Landroid/widget/TextView;->setText([CII)V

    if-eqz p4, :cond_6

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v1

    :goto_5
    iget-object v2, v0, Ldyf;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v6, 0x7f0200d6    # com.google.android.gms.R.drawable.games_default_profile_img

    invoke-virtual {v2, v1, v6}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    if-eqz p4, :cond_7

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Participant;->a()I

    move-result v1

    move v2, v1

    :goto_6
    if-eqz p4, :cond_0

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Participant;->d()Z

    :cond_0
    if-eqz v4, :cond_8

    const/4 v1, 0x0

    :goto_7
    if-lez v1, :cond_c

    iget-object v3, v0, Ldyf;->e:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Ldyf;->e:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_8
    const/4 v1, 0x2

    if-ne v2, v1, :cond_d

    iget-object v1, v0, Ldyf;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldyf;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldyf;->f:Ldye;

    iget-object v2, v2, Ldye;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00de    # com.google.android.gms.R.color.games_tile_text_color_primary_text

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Ldyf;->e:Landroid/widget/TextView;

    iget-object v0, v0, Ldyf;->f:Ldye;

    iget-object v0, v0, Ldye;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c00df    # com.google.android.gms.R.color.games_tile_text_color_secondary_text

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_9
    return-void

    :cond_1
    const/4 v1, 0x0

    move v5, v1

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_4
    if-eqz v5, :cond_5

    iget-object v1, v0, Ldyf;->b:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f0b0225    # com.google.android.gms.R.string.games_select_players_auto_pick_chip_name

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Ldyf;->d:Landroid/database/CharArrayBuffer;

    invoke-static {v1, v2}, Lbpf;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    goto/16 :goto_4

    :cond_5
    iget-object v1, v0, Ldyf;->b:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldyf;->d:Landroid/database/CharArrayBuffer;

    invoke-interface {p4, v1}, Lcom/google/android/gms/games/multiplayer/Participant;->a(Landroid/database/CharArrayBuffer;)V

    goto/16 :goto_4

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_7
    const/4 v1, 0x1

    move v2, v1

    goto/16 :goto_6

    :cond_8
    if-eqz v5, :cond_9

    const/4 v1, 0x0

    goto/16 :goto_7

    :cond_9
    packed-switch v2, :pswitch_data_0

    const-string v1, "WaitingRoomAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bindView: unexpected participant status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "bindView: unexpected participant status: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbiq;->b(Ljava/lang/Object;)V

    const/4 v1, 0x0

    goto/16 :goto_7

    :pswitch_0
    const v1, 0x7f0b023d    # com.google.android.gms.R.string.games_waiting_room_participant_status_invited

    goto/16 :goto_7

    :pswitch_1
    iget-object v1, v0, Ldyf;->f:Ldye;

    iget-object v1, v1, Ldye;->j:Ljava/lang/String;

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const v1, 0x7f0b0240    # com.google.android.gms.R.string.games_waiting_room_participant_status_creator_joined

    goto/16 :goto_7

    :cond_a
    if-eqz v3, :cond_b

    const v1, 0x7f0b023f    # com.google.android.gms.R.string.games_waiting_room_participant_status_automatch_joined

    goto/16 :goto_7

    :cond_b
    const v1, 0x7f0b023e    # com.google.android.gms.R.string.games_waiting_room_participant_status_player_joined

    goto/16 :goto_7

    :pswitch_2
    const v1, 0x7f0b0241    # com.google.android.gms.R.string.games_waiting_room_participant_status_declined

    goto/16 :goto_7

    :pswitch_3
    const v1, 0x7f0b0242    # com.google.android.gms.R.string.games_waiting_room_participant_status_left

    goto/16 :goto_7

    :pswitch_4
    const/4 v1, 0x0

    goto/16 :goto_7

    :cond_c
    iget-object v1, v0, Ldyf;->e:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    :cond_d
    const/4 v1, 0x3

    if-eq v2, v1, :cond_e

    const/4 v1, 0x4

    if-eq v2, v1, :cond_e

    const/4 v1, -0x1

    if-ne v2, v1, :cond_f

    :cond_e
    iget-object v1, v0, Ldyf;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v2, 0x7f0c00e5    # com.google.android.gms.R.color.games_tile_red_color_filter

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldyf;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldyf;->f:Ldye;

    iget-object v2, v2, Ldye;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00e5    # com.google.android.gms.R.color.games_tile_red_color_filter

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Ldyf;->e:Landroid/widget/TextView;

    iget-object v0, v0, Ldyf;->f:Ldye;

    iget-object v0, v0, Ldye;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c00e5    # com.google.android.gms.R.color.games_tile_red_color_filter

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_9

    :cond_f
    iget-object v1, v0, Ldyf;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v2, 0x7f0c00e4    # com.google.android.gms.R.color.games_tile_white_color_filter

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldyf;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Ldyf;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldyf;->f:Ldye;

    iget-object v2, v2, Ldye;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00de    # com.google.android.gms.R.color.games_tile_text_color_primary_text

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Ldyf;->e:Landroid/widget/TextView;

    iget-object v0, v0, Ldyf;->f:Ldye;

    iget-object v0, v0, Ldye;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c00df    # com.google.android.gms.R.color.games_tile_text_color_secondary_text

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_9

    :cond_10
    move v3, v1

    move v4, v2

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 7

    const/4 v2, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->w_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldye;->j:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Ldye;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Ldye;->i:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    move v1, v2

    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    iget-object v6, p0, Ldye;->i:Ljava/lang/String;

    if-eqz v6, :cond_1

    if-eqz v5, :cond_1

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Ldye;->i:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    invoke-static {p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->m(Lcom/google/android/gms/games/multiplayer/realtime/Room;)I

    move-result v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_2
    if-ge v2, v0, :cond_5

    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/games/multiplayer/Participant;

    new-instance v1, Lbhb;

    invoke-direct {v1, v0}, Lbhb;-><init>([Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Ldye;->a(Lbgo;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldye;->i:Ljava/lang/String;

    return-void
.end method

.method public final isEnabled(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final l()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Ldye;->h:Landroid/view/LayoutInflater;

    const v1, 0x7f040096    # com.google.android.gms.R.layout.games_tile_waiting_room

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ldyf;

    invoke-direct {v1, p0, v0}, Ldyf;-><init>(Ldye;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
