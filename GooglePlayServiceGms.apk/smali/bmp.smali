.class public final Lbmp;
.super Lsc;
.source "SourceFile"


# instance fields
.field private final f:Landroid/content/Context;

.field private final g:Landroid/net/Uri;

.field private final h:Lsk;

.field private final i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLsk;Lsj;)V
    .locals 0

    invoke-direct {p0, p3, p6}, Lsc;-><init>(Ljava/lang/String;Lsj;)V

    iput-object p1, p0, Lbmp;->f:Landroid/content/Context;

    iput-object p2, p0, Lbmp;->g:Landroid/net/Uri;

    iput-object p5, p0, Lbmp;->h:Lsk;

    iput-boolean p4, p0, Lbmp;->i:Z

    return-void
.end method


# virtual methods
.method protected final a(Lrz;)Lsi;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lbmp;->g:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbmp;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "url"

    iget-object v4, p0, Lsc;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "image_data"

    iget-object v4, p1, Lrz;->b:[B

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    iget-object v3, p0, Lbmp;->g:Landroid/net/Uri;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    iget-object v1, p1, Lrz;->b:[B

    invoke-static {v1, v0}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p1, Lrz;->b:[B

    iget-boolean v2, p0, Lbmp;->i:Z

    if-eqz v2, :cond_1

    invoke-static {p1}, Ltb;->a(Lrz;)Lrp;

    move-result-object v0

    :cond_1
    invoke-static {v1, v0}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, [B

    iget-object v0, p0, Lbmp;->h:Lsk;

    invoke-interface {v0, p1}, Lsk;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final o()Lse;
    .locals 1

    sget-object v0, Lse;->a:Lse;

    return-object v0
.end method
