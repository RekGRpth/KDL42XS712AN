.class public final Lefa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Leff;

.field final b:Lefg;

.field final c:Lefh;

.field d:I

.field e:Ljava/util/List;

.field f:J

.field g:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Runnable;Lefw;)V
    .locals 3

    new-instance v0, Lefb;

    invoke-direct {v0, p1, p2}, Lefb;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    new-instance v1, Lefc;

    invoke-direct {v1}, Lefc;-><init>()V

    new-instance v2, Lefd;

    invoke-direct {v2, p3}, Lefd;-><init>(Lefw;)V

    invoke-direct {p0, v0, v1, v2}, Lefa;-><init>(Leff;Lefg;Lefh;)V

    return-void
.end method

.method private constructor <init>(Leff;Lefg;Lefh;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lefa;->d:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lefa;->e:Ljava/util/List;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lefa;->f:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lefa;->g:J

    iput-object p1, p0, Lefa;->a:Leff;

    iput-object p2, p0, Lefa;->b:Lefg;

    iput-object p3, p0, Lefa;->c:Lefh;

    return-void
.end method


# virtual methods
.method final a()I
    .locals 2

    iget v0, p0, Lefa;->d:I

    iget-object v1, p0, Lefa;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method final b()Z
    .locals 4

    iget-wide v0, p0, Lefa;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()V
    .locals 2

    invoke-virtual {p0}, Lefa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefa;->a:Leff;

    invoke-interface {v0}, Leff;->a()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lefa;->f:J

    :cond_0
    return-void
.end method
