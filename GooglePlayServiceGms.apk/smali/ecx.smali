.class public final Lecx;
.super Lecz;
.source "SourceFile"

# interfaces
.implements Lbel;
.implements Ldel;
.implements Ldem;


# instance fields
.field private b:Lduk;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lecz;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public final synthetic a(Lbek;)V
    .locals 6

    check-cast p1, Lctn;

    invoke-interface {p1}, Lctn;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Lctn;->a()Lctb;

    move-result-object v1

    invoke-virtual {p0}, Lecx;->J()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lctb;->b()V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    invoke-virtual {v1}, Lctb;->b()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lecx;->c(I)V

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {v1}, Lctb;->a()I

    move-result v2

    if-lez v2, :cond_3

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lctb;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lecx;->b:Lduk;

    iget-object v2, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lduk;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v4, v0, Lduk;->f:Ldam;

    new-instance v5, Ldur;

    invoke-direct {v5, v0, p0}, Ldur;-><init>(Lduk;Ldel;)V

    invoke-interface {v4, v5, v2, v3}, Ldam;->a(Ldaj;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {v1}, Lctb;->b()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "SignInClient"

    const-string v2, "service died"

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lctb;->b()V

    throw v0

    :cond_2
    const/4 v0, 0x6

    :try_start_3
    invoke-virtual {p0, v0}, Lecx;->c(I)V

    goto :goto_1

    :cond_3
    const-string v2, "CheckGameplayAcl"

    const-string v3, "Unable to load metadata for game"

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lecx;->d(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lecx;->e(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    invoke-virtual {p0}, Lecx;->J()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lecx;->c(I)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lecx;->c(I)V

    goto :goto_0

    :cond_2
    const-string v1, "CheckGameplayAcl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error when updating gameplay ACL; status code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lecx;->d(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lecx;->e(I)V

    goto :goto_0
.end method

.method public final a(Ldek;)V
    .locals 6

    const/4 v0, 0x0

    invoke-interface {p1}, Ldek;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-interface {p1}, Ldek;->g()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {p0}, Lecx;->J()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lecx;->c(I)V

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    const-string v1, "specified_by_user"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lecx;->c(I)V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-static {v1}, Lecx;->d(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lecx;->e(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    :cond_3
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lecx;->f(I)V

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o()V

    iget-object v0, p0, Lecx;->b:Lduk;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lecx;->b:Lduk;

    invoke-virtual {v0}, Lduk;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lecx;->b:Lduk;

    iget-object v1, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v0}, Lduk;->c()V

    :try_start_2
    iget-object v4, v0, Lduk;->f:Ldam;

    new-instance v5, Ldut;

    invoke-direct {v5, v0, p0}, Ldut;-><init>(Lduk;Ldem;)V

    invoke-interface {v4, v5, v1, v2, v3}, Ldam;->b(Ldaj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lecx;->c(I)V

    goto :goto_0
.end method

.method public final a(Lduk;)V
    .locals 5

    iput-object p1, p0, Lecx;->b:Lduk;

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lecx;->c(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lecx;->b:Lduk;

    iget-object v1, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lduk;->c()V

    :try_start_0
    iget-object v3, v0, Lduk;->f:Ldam;

    new-instance v4, Ldup;

    invoke-direct {v4, v0, p0}, Ldup;-><init>(Lduk;Lbel;)V

    iget-object v0, v0, Lduk;->d:Ljava/lang/String;

    invoke-interface {v3, v4, v1, v2, v0}, Ldam;->a(Ldaj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x9

    return v0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lecz;->e(Landroid/os/Bundle;)V

    return-void
.end method
