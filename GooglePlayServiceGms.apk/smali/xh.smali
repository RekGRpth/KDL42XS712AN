.class public final Lxh;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/AdRequestBrokerService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/AdRequestBrokerService;)V
    .locals 0

    iput-object p1, p0, Lxh;->a:Lcom/google/android/gms/ads/AdRequestBrokerService;

    invoke-direct {p0}, Lbip;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbjv;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lxh;->a:Lcom/google/android/gms/ads/AdRequestBrokerService;

    iget-object v1, p0, Lxh;->a:Lcom/google/android/gms/ads/AdRequestBrokerService;

    const-string v1, "Location will be disabled for ad requests."

    invoke-static {v1}, Lacj;->a(Ljava/lang/String;)V

    new-instance v1, Lzd;

    invoke-direct {v1}, Lzd;-><init>()V

    invoke-static {v0, v1}, Labj;->a(Landroid/content/Context;Lzc;)Labj;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v2}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Client died while brokering the ad request service."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
