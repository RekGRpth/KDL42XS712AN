.class public final Ljak;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljal;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljak;->a:I

    const-string v0, ""

    iput-object v0, p0, Ljak;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljak;->c:Ljal;

    const/4 v0, -0x1

    iput v0, p0, Ljak;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Ljak;->a:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ljak;->a:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljak;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljak;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljak;->c:Ljal;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljak;->c:Ljal;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Ljak;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Ljak;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljak;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljak;->c:Ljal;

    if-nez v0, :cond_1

    new-instance v0, Ljal;

    invoke-direct {v0}, Ljal;-><init>()V

    iput-object v0, p0, Ljak;->c:Ljal;

    :cond_1
    iget-object v0, p0, Ljak;->c:Ljal;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x1f -> :sswitch_2
        0x20 -> :sswitch_2
        0x29 -> :sswitch_2
        0x2a -> :sswitch_2
        0x2b -> :sswitch_2
        0x2c -> :sswitch_2
        0x33 -> :sswitch_2
        0x3d -> :sswitch_2
        0x3e -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget v0, p0, Ljak;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljak;->a:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Ljak;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljak;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljak;->c:Ljal;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljak;->c:Ljal;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljak;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljak;

    iget v2, p0, Ljak;->a:I

    iget v3, p1, Ljak;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljak;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Ljak;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljak;->b:Ljava/lang/String;

    iget-object v3, p1, Ljak;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljak;->c:Ljal;

    if-nez v2, :cond_6

    iget-object v2, p1, Ljak;->c:Ljal;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljak;->c:Ljal;

    iget-object v3, p1, Ljak;->c:Ljal;

    invoke-virtual {v2, v3}, Ljal;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Ljak;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljak;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljak;->c:Ljal;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljak;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ljak;->c:Ljal;

    invoke-virtual {v1}, Ljal;->hashCode()I

    move-result v1

    goto :goto_1
.end method
