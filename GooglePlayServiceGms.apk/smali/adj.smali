.class public final Ladj;
.super Lsc;
.source "SourceFile"


# instance fields
.field private final f:Lsk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lsk;Lsj;)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ladr;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p4}, Lsc;-><init>(ILjava/lang/String;Lsj;)V

    iput-object p3, p0, Ladj;->f:Lsk;

    return-void
.end method


# virtual methods
.method protected final a(Lrz;)Lsi;
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p1, Lrz;->c:Ljava/util/Map;

    const-string v1, "X-Mobile-PrefMgr"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "OPTED_IN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0, v2}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "OPTED_OUT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0, v2}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v1, "AdPrefsRequest"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "AdPrefsRequest"

    const-string v2, "result header %s for %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    iget-object v0, p0, Lsc;->b:Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v0, Lsb;

    invoke-direct {v0}, Lsb;-><init>()V

    invoke-static {v0}, Lsi;->a(Lsp;)Lsi;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Ladj;->f:Lsk;

    invoke-interface {v0, p1}, Lsk;->a(Ljava/lang/Object;)V

    return-void
.end method
