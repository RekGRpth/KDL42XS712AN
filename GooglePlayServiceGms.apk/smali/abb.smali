.class public final Labb;
.super Laaz;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;


# instance fields
.field private final a:Laay;

.field private final b:Labc;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Laay;)V
    .locals 2

    invoke-direct {p0, p2, p3}, Laaz;-><init>(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Laay;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Labb;->c:Ljava/lang/Object;

    iput-object p3, p0, Labb;->a:Laay;

    new-instance v0, Labc;

    iget-object v1, p2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->d:I

    invoke-direct {v0, p1, p0, p0, v1}, Labc;-><init>(Landroid/content/Context;Lbbr;Lbbs;I)V

    iput-object v0, p0, Labb;->b:Labc;

    iget-object v0, p0, Labb;->b:Labc;

    invoke-virtual {v0}, Labc;->a()V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 1

    const-string v0, "Disconnected from remote ad request service."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 3

    iget-object v0, p0, Labb;->a:Laay;

    new-instance v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    invoke-interface {v0, v1}, Laay;->a(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Laby;->e:Ljava/lang/Runnable;

    invoke-static {v0}, Laca;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v1, p0, Labb;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Labb;->b:Labc;

    invoke-virtual {v0}, Labc;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Labb;->b:Labc;

    invoke-virtual {v0}, Labc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Labb;->b:Labc;

    invoke-virtual {v0}, Labc;->b()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Labg;
    .locals 2

    iget-object v1, p0, Labb;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Labb;->b:Labc;

    invoke-virtual {v0}, Labc;->c()Labg;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
