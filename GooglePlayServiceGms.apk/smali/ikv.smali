.class public final Likv;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Ljava/util/ArrayList;


# instance fields
.field private Y:Landroid/widget/RadioButton;

.field private Z:Landroid/widget/RadioButton;

.field private b:Likx;

.field private c:I

.field private d:Lcom/google/android/gms/common/acl/ScopeData;

.field private e:Ljava/util/ArrayList;

.field private f:Landroid/view/View;

.field private g:Lcom/google/android/gms/common/people/views/AudienceView;

.field private h:Landroid/view/View;

.field private i:Lcom/google/android/gms/common/people/views/AudienceView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Likv;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/acl/ScopeData;)Likv;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "index"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "scope_data"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Likv;

    invoke-direct {v1}, Likv;-><init>()V

    invoke-virtual {v1, v0}, Likv;->g(Landroid/os/Bundle;)V

    return-object v1
.end method

.method static synthetic a(Likv;)Likx;
    .locals 1

    iget-object v0, p0, Likv;->b:Likx;

    return-object v0
.end method

.method private a()Ljava/util/ArrayList;
    .locals 5

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->e()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {v1}, Lbpd;->b(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbky;->a([B)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to parse audience from roster: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "myCircles"

    invoke-virtual {p0}, Likv;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b045e    # com.google.android.gms.R.string.common_chips_label_your_circles

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p2, v0

    :goto_0
    return-object p2

    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_1

    aget-object v3, v2, v1

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_2

    move-object p2, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to parse audience from circle ID list: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/acl/ScopeData;->a(Z)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    iget-object v0, p0, Likv;->Y:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v1, p0, Likv;->Z:Landroid/widget/RadioButton;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Likv;->c:I

    const-string v0, "scope_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    const-string v0, "facl_audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Likv;->e:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    const/16 v5, 0x8

    const v9, 0x7f0a00b1    # com.google.android.gms.R.id.warning

    const v8, 0x7f0a00af    # com.google.android.gms.R.id.warning_layout

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    instance-of v3, v0, Likx;

    if-eqz v3, :cond_4

    check-cast v0, Likx;

    :goto_0
    iput-object v0, p0, Likv;->b:Likx;

    if-nez p3, :cond_5

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v3, "index"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Likv;->c:I

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v3, "scope_data"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Likv;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v3}, Likv;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Likv;->e:Ljava/util/ArrayList;

    :cond_0
    :goto_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04002f    # com.google.android.gms.R.layout.auth_permissions_scope_list_item

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_1
    const v0, 0x7f0a00ad    # com.google.android.gms.R.id.scope_label

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a00ae    # com.google.android.gms.R.id.details_layout

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f0a00b2    # com.google.android.gms.R.id.facl_layout

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a00b4    # com.google.android.gms.R.id.facl_link_layout

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Likv;->f:Landroid/view/View;

    iget-object v0, p0, Likv;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Likv;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00ac    # com.google.android.gms.R.id.scope_icon

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Likv;->j()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02006f    # com.google.android.gms.R.drawable.auth_ic_scope_icon_facl

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_3
    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0a00b6    # com.google.android.gms.R.id.pacl_layout

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a00ba    # com.google.android.gms.R.id.private_pacl_layout

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Likv;->a()Ljava/util/ArrayList;

    move-result-object v4

    const v0, 0x7f0a00b8    # com.google.android.gms.R.id.audience_view

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/views/AudienceView;

    iput-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    iget-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/people/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Z)V

    iget-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->b()V

    iget-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Ljava/util/Collection;)V

    const v0, 0x7f0a00b9    # com.google.android.gms.R.id.pacl_edit_layout

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Likv;->h:Landroid/view/View;

    iget-object v0, p0, Likv;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Likv;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00bc    # com.google.android.gms.R.id.only_you_audience_view

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/views/AudienceView;

    iput-object v0, p0, Likv;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    iget-object v0, p0, Likv;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/people/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Likv;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Z)V

    iget-object v0, p0, Likv;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->c()V

    const v0, 0x7f0a00b7    # com.google.android.gms.R.id.acl_radio_button

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Likv;->Y:Landroid/widget/RadioButton;

    iget-object v5, p0, Likv;->Y:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_4
    invoke-virtual {v5, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    const v0, 0x7f0a00bb    # com.google.android.gms.R.id.private_radio_button

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Likv;->Z:Landroid/widget/RadioButton;

    iget-object v0, p0, Likv;->Z:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Likv;->Y:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Likv;->Z:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00ac    # com.google.android.gms.R.id.scope_icon

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Likv;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020070    # com.google.android.gms.R.drawable.auth_ic_scope_icon_pacl

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    :goto_5
    return-object v3

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0, p3}, Likv;->c(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Likw;

    iget-object v5, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Likv;->j()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b04c9    # com.google.android.gms.R.string.auth_facl_scope_details_dialog_title

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lbcj;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {v0, p0, v5, v6, v7}, Likw;-><init>(Likv;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_6
    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Likw;

    iget-object v5, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Likv;->j()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b04c8    # com.google.android.gms.R.string.auth_pacl_scope_details_dialog_title

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lbcj;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {v0, p0, v5, v6, v7}, Likw;-><init>(Likv;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_6

    :cond_8
    new-instance v0, Likw;

    iget-object v5, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, p0, v5}, Likw;-><init>(Likv;Ljava/lang/String;)V

    goto :goto_6

    :cond_9
    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_a
    move v0, v2

    goto/16 :goto_4

    :cond_b
    const v0, 0x7f0a00ad    # com.google.android.gms.R.id.scope_label

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a00ae    # com.google.android.gms.R.id.details_layout

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_7
    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_c
    new-instance v1, Likw;

    iget-object v4, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, p0, v4}, Likw;-><init>(Likv;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_7
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Likv;->c(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "index"

    iget v1, p0, Likv;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "scope_data"

    iget-object v1, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "facl_audience"

    iget-object v1, p0, Likv;->e:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Likv;->Y:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Likv;->h:Landroid/view/View;

    if-ne p1, v0, :cond_3

    :cond_0
    iget-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Likv;->Y:Landroid/widget/RadioButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Likv;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/people/views/AudienceView;->setEnabled(Z)V

    iget-object v0, p0, Likv;->Y:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Likv;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Likv;->a(Z)V

    iget-object v0, p0, Likv;->b:Likx;

    iget v0, p0, Likv;->c:I

    iget-object v0, p0, Likv;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->a()Ljava/util/ArrayList;

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Likv;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eq p1, v0, :cond_4

    iget-object v0, p0, Likv;->Z:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_5

    :cond_4
    invoke-direct {p0, v1}, Likv;->a(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Likv;->f:Landroid/view/View;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Likv;->f:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v0, p0, Likv;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_6
    iget-object v0, p0, Likv;->b:Likx;

    iget v0, p0, Likv;->c:I

    iget-object v0, p0, Likv;->e:Ljava/util/ArrayList;

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->h()Ljava/lang/String;

    iget-object v0, p0, Likv;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->j()Z

    goto :goto_0
.end method
