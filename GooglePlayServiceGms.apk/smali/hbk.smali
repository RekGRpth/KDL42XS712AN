.class public final Lhbk;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final a:[I

.field private static final ai:Ljava/util/ArrayList;


# instance fields
.field Y:Z

.field Z:Z

.field private aA:Lut;

.field aa:Z

.field ab:Lizz;

.field ac:I

.field ad:Lipv;

.field final ae:Lhcb;

.field af:Lgvu;

.field ag:Lgvu;

.field ah:Lgxh;

.field private aj:Landroid/view/View;

.field private ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

.field private am:Ljava/util/ArrayList;

.field private an:Landroid/accounts/Account;

.field private ao:Z

.field private ap:Ljava/util/ArrayList;

.field private aq:I

.field private ar:Z

.field private as:Z

.field private at:Z

.field private au:Z

.field private av:Ljau;

.field private aw:Lhbp;

.field private ax:I

.field private ay:I

.field private az:Luu;

.field b:Landroid/view/View;

.field c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

.field d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field e:Lgxg;

.field f:Lgvt;

.field g:Lgvt;

.field h:Lhca;

.field i:Lcom/google/android/gms/wallet/common/PaymentModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lhbk;->ai:Ljava/util/ArrayList;

    sput-object v0, Lhbk;->a:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-boolean v0, p0, Lhbk;->ar:Z

    iput-boolean v0, p0, Lhbk;->as:Z

    iput-boolean v0, p0, Lhbk;->at:Z

    iput-boolean v0, p0, Lhbk;->Y:Z

    iput-boolean v0, p0, Lhbk;->Z:Z

    iput-boolean v0, p0, Lhbk;->aa:Z

    const/4 v0, -0x1

    iput v0, p0, Lhbk;->ac:I

    new-instance v0, Lhbl;

    invoke-direct {v0, p0}, Lhbl;-><init>(Lhbk;)V

    iput-object v0, p0, Lhbk;->ae:Lhcb;

    new-instance v0, Lhbm;

    invoke-direct {v0, p0}, Lhbm;-><init>(Lhbk;)V

    iput-object v0, p0, Lhbk;->af:Lgvu;

    new-instance v0, Lhbn;

    invoke-direct {v0, p0}, Lhbn;-><init>(Lhbk;)V

    iput-object v0, p0, Lhbk;->ag:Lgvu;

    new-instance v0, Lhbo;

    invoke-direct {v0, p0}, Lhbo;-><init>(Lhbk;)V

    iput-object v0, p0, Lhbk;->ah:Lgxh;

    return-void
.end method

.method static synthetic J()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lhbk;->ai:Ljava/util/ArrayList;

    return-object v0
.end method

.method private K()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput-boolean v3, p0, Lhbk;->aa:Z

    iput-boolean v2, p0, Lhbk;->Z:Z

    iput v2, p0, Lhbk;->aq:I

    invoke-direct {p0, v2}, Lhbk;->c(I)V

    iget-object v0, p0, Lhbk;->h:Lhca;

    iget-object v1, p0, Lhbk;->ae:Lhcb;

    invoke-interface {v0, v1}, Lhca;->a(Lhcb;)V

    iget-boolean v0, p0, Lhbk;->Y:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhbk;->ab:Lizz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhbk;->ae:Lhcb;

    iget-object v1, p0, Lhbk;->ab:Lizz;

    invoke-virtual {v0, v1}, Lhcb;->a(Lizz;)V

    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v3, v2}, Lhbk;->a(ZI)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lhbk;->h:Lhca;

    iget-object v1, p0, Lhbk;->ae:Lhcb;

    iget v2, p0, Lhbk;->ac:I

    invoke-interface {v0, v1, v2}, Lhca;->a(Lhcb;I)V

    const/4 v0, -0x1

    iput v0, p0, Lhbk;->ac:I

    return-void

    :cond_1
    invoke-direct {p0, v2}, Lhbk;->a(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v3}, Lhbk;->a(Z)V

    goto :goto_0
.end method

.method private L()V
    .locals 2

    iget v0, p0, Lhbk;->ac:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lhbk;->h:Lhca;

    iget-object v1, p0, Lhbk;->ae:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lhbk;->ac:I

    :cond_0
    return-void
.end method

.method private M()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lhbk;->N()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->ad:Lipv;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhbk;->ar:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lhbk;->d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method private N()Z
    .locals 1

    iget-object v0, p0, Lhbk;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lhbk;Lipv;)I
    .locals 1

    invoke-direct {p0, p1}, Lhbk;->a(Lipv;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lioj;)I
    .locals 1

    invoke-static {p0}, Lhbk;->b(Lioj;)I

    move-result v0

    return v0
.end method

.method private a(Lipv;)I
    .locals 1

    if-nez p1, :cond_0

    const/16 v0, 0x7b

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lgth;->b(Lipv;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x7c

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lgth;->c(Lipv;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x7d

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lhbk;->as:Z

    if-eqz v0, :cond_3

    invoke-static {p1}, Lgth;->a(Lipv;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x7e

    goto :goto_0

    :cond_3
    const/16 v0, 0x7f

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/accounts/Account;ZII)Lhbk;
    .locals 3

    new-instance v0, Lhbk;

    invoke-direct {v0}, Lhbk;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "allowChangeAccounts"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "startingContentHeight"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "startingContentWidth"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lhbk;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private a([Lipv;Z)Lipv;
    .locals 5

    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v0, p1, v1

    invoke-direct {p0, v0}, Lhbk;->a(Lipv;)I

    move-result v3

    const/16 v4, 0x7f

    if-eq v3, v4, :cond_0

    if-eqz p2, :cond_1

    const/16 v4, 0x7d

    if-ne v3, v4, :cond_1

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method static synthetic a(Lhbk;)V
    .locals 8

    iget-object v0, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v1}, Lhfx;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Ljbg;

    move-result-object v1

    iget-object v2, p0, Lhbk;->an:Landroid/accounts/Account;

    const-string v3, "US"

    sget-object v4, Lhbk;->ai:Ljava/util/ArrayList;

    iget-boolean v5, p0, Lhbk;->au:Z

    iget-object v6, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-boolean v7, p0, Lhbk;->ao:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljbg;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f5

    invoke-virtual {p0, v0, v1}, Lhbk;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lhbk;I)V
    .locals 0

    invoke-direct {p0, p1}, Lhbk;->d(I)V

    return-void
.end method

.method static synthetic a(Lhbk;ILjava/util/ArrayList;)V
    .locals 12

    const/4 v2, 0x0

    iget-object v0, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhbk;->an:Landroid/accounts/Account;

    iget-object v6, p0, Lhbk;->ap:Ljava/util/ArrayList;

    iget-boolean v8, p0, Lhbk;->as:Z

    iget-object v3, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i()Z

    move-result v10

    iget-object v3, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v11

    const/4 v9, 0x1

    move-object v3, p2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-static/range {v0 .. v11}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lioq;Ljava/util/Collection;Lipv;ZZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lhbk;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lhbk;Lioj;)V
    .locals 11

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhbk;->an:Landroid/accounts/Account;

    iget-object v2, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v10

    const/4 v9, 0x1

    move-object v2, p1

    move v4, v3

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-static/range {v0 .. v10}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;ZZLjava/lang/String;Lioq;Ljava/util/Collection;Lipv;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f9

    invoke-virtual {p0, v0, v1}, Lhbk;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lhbk;Lipv;ILjava/util/ArrayList;)V
    .locals 10

    const/4 v4, 0x0

    iget-object v0, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhbk;->an:Landroid/accounts/Account;

    iget-object v6, p0, Lhbk;->ap:Ljava/util/ArrayList;

    iget-boolean v7, p0, Lhbk;->as:Z

    iget-object v2, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v9

    const/4 v8, 0x1

    move-object v2, p1

    move-object v3, p3

    move-object v5, v4

    invoke-static/range {v0 .. v9}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lipv;Ljava/util/ArrayList;Ljava/lang/String;Lioq;Ljava/util/Collection;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lhbk;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lhbk;Lizz;)V
    .locals 5

    iget-object v0, p1, Lizz;->a:Ljbb;

    if-eqz v0, :cond_0

    iget-object v0, v0, Ljbb;->a:[I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lboz;->a([II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhbk;->au:Z

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhbk;->ap:Ljava/util/ArrayList;

    iget-object v0, p1, Lizz;->c:[Lipv;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v1, p1, Lizz;->c:[Lipv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, p0, Lhbk;->ap:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Z)V
    .locals 3

    iget-boolean v0, p0, Lhbk;->at:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    const v1, 0x7f0b014b    # com.google.android.gms.R.string.wallet_local_spinner_text

    invoke-direct {p0, v0, v1}, Lhbk;->a(ZI)V

    :cond_0
    iget-object v0, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v0}, Lhfx;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Ljbg;

    move-result-object v0

    new-instance v1, Ljba;

    invoke-direct {v1}, Ljba;-><init>()V

    iput-object v0, v1, Ljba;->a:Ljbg;

    iget-object v0, p0, Lhbk;->h:Lhca;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lhca;->a(Ljba;Z)V

    :cond_1
    return-void
.end method

.method private a(ZI)V
    .locals 3

    const/4 v1, 0x0

    iget v2, p0, Lhbk;->aq:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lhbk;->aq:I

    invoke-direct {p0, p2}, Lhbk;->c(I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static b(Lioj;)I
    .locals 5

    const/16 v0, 0x79

    if-nez p0, :cond_1

    const/16 v0, 0x77

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lioj;->c:I

    sget-object v2, Lhbk;->a:[I

    invoke-static {v2, v1}, Lboz;->a([II)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v0, 0x3

    if-ne v1, v0, :cond_2

    const/16 v0, 0x7b

    goto :goto_0

    :cond_2
    const/16 v0, 0x7a

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lioj;->g:[I

    array-length v1, v1

    if-lez v1, :cond_5

    iget-object v1, p0, Lioj;->g:[I

    array-length v4, v1

    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_4

    iget-object v1, p0, Lioj;->g:[I

    aget v1, v1, v2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    const/16 v3, 0x7d

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget v1, p0, Lioj;->h:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x7f

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x78

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lhbk;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lhbk;->aj:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lhbk;Lizz;)V
    .locals 9

    const/16 v7, 0x7f

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v4, p1, Lizz;->b:[Lioj;

    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-static {v4, v0}, Lgth;->a([Lioj;Lioj;)Lioj;

    move-result-object v2

    invoke-static {v2}, Lhbk;->b(Lioj;)I

    move-result v0

    if-nez v2, :cond_8

    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_5

    aget-object v0, v4, v2

    iget-boolean v6, v0, Lioj;->f:Z

    if-eqz v6, :cond_4

    :goto_1
    if-nez v0, :cond_0

    array-length v5, v4

    move v2, v3

    :goto_2
    if-ge v2, v5, :cond_7

    aget-object v0, v4, v2

    invoke-static {v0}, Lhbk;->b(Lioj;)I

    move-result v6

    if-ne v6, v7, :cond_6

    :cond_0
    :goto_3
    invoke-static {v0}, Lhbk;->b(Lioj;)I

    move-result v2

    :goto_4
    if-eq v2, v7, :cond_1

    move-object v0, v1

    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lioj;->a:Ljava/lang/String;

    :cond_2
    const-string v2, "LocalPaymentDetailsFra"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "updatePaymentInstruments selectedInstrument id = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-boolean v1, p0, Lhbk;->at:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1, v3}, Lgxg;->a(Z)V

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1, v3}, Lgxg;->b(Z)V

    iget-object v1, p0, Lhbk;->e:Lgxg;

    sget-object v2, Lhbk;->a:[I

    invoke-interface {v1, v2}, Lgxg;->a([I)V

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1, v4}, Lgxg;->a([Lioj;)V

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1, v0}, Lgxg;->a(Lioj;)V

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1

    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_3

    :cond_8
    move v8, v0

    move-object v0, v2

    move v2, v8

    goto :goto_4
.end method

.method private c(I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lhbk;->aq:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lhbk;->N()Z

    move-result v3

    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lhbk;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1, v2}, Lgxg;->setEnabled(Z)V

    iget-object v1, p0, Lhbk;->f:Lgvt;

    invoke-interface {v1, v2}, Lgvt;->setEnabled(Z)V

    iget-object v1, p0, Lhbk;->g:Lgvt;

    invoke-interface {v1, v2}, Lgvt;->setEnabled(Z)V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lhbk;->M()V

    if-eqz v0, :cond_1

    if-nez p1, :cond_4

    :cond_1
    iget-object v0, p0, Lhbk;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->b()V

    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lhbk;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v2, p0, Lhbk;->e:Lgxg;

    invoke-interface {v2, v1}, Lgxg;->setEnabled(Z)V

    iget-object v2, p0, Lhbk;->f:Lgvt;

    invoke-interface {v2, v1}, Lgvt;->setEnabled(Z)V

    iget-object v2, p0, Lhbk;->g:Lgvt;

    invoke-interface {v2, v1}, Lgvt;->setEnabled(Z)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lhbk;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->a(I)V

    goto :goto_2
.end method

.method static synthetic c(Lhbk;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lhbk;->a(ZI)V

    return-void
.end method

.method static synthetic c(Lhbk;Lizz;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v4, p1, Lizz;->c:[Lipv;

    iget-object v1, p0, Lhbk;->ad:Lipv;

    invoke-static {v4, v1}, Lgth;->a([Lipv;Lipv;)Lipv;

    move-result-object v3

    invoke-direct {p0, v3}, Lhbk;->a(Lipv;)I

    move-result v1

    if-nez v3, :cond_7

    array-length v5, v4

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_5

    aget-object v1, v4, v3

    iget-boolean v6, v1, Lipv;->j:Z

    if-eqz v6, :cond_4

    :goto_1
    if-nez v1, :cond_6

    iget-boolean v1, p0, Lhbk;->au:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v4, v0}, Lhbk;->a([Lipv;Z)Lipv;

    move-result-object v0

    :goto_2
    invoke-direct {p0, v0}, Lhbk;->a(Lipv;)I

    move-result v1

    :goto_3
    const/16 v3, 0x7f

    if-eq v1, v3, :cond_2

    const/16 v3, 0x7d

    if-ne v1, v3, :cond_1

    iget-boolean v1, p0, Lhbk;->au:Z

    if-eqz v1, :cond_2

    :cond_1
    move-object v0, v2

    :cond_2
    iput-object v0, p0, Lhbk;->ad:Lipv;

    iget-boolean v1, p0, Lhbk;->at:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lhbk;->f:Lgvt;

    invoke-interface {v1, v4}, Lgvt;->a([Lipv;)V

    iget-object v1, p0, Lhbk;->f:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    iget-object v1, p0, Lhbk;->f:Lgvt;

    iget-boolean v2, p0, Lhbk;->as:Z

    invoke-interface {v1, v2}, Lgvt;->a(Z)V

    iput-object v0, p0, Lhbk;->ad:Lipv;

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_5
    move-object v1, v2

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_2

    :cond_7
    move-object v0, v3

    goto :goto_3
.end method

.method static synthetic d(Lhbk;)Luu;
    .locals 1

    iget-object v0, p0, Lhbk;->az:Luu;

    return-object v0
.end method

.method private d(I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lhbk;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic d(Lhbk;Lizz;)V
    .locals 11

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-boolean v0, p0, Lhbk;->ar:Z

    if-eqz v0, :cond_4

    iget-object v6, p1, Lizz;->c:[Lipv;

    array-length v0, v6

    new-array v7, v0, [Lipv;

    array-length v8, v6

    move v2, v3

    move v4, v3

    :goto_0
    if-ge v2, v8, :cond_1

    aget-object v0, v6, v2

    iget-boolean v5, v0, Lipv;->g:Z

    if-eqz v5, :cond_0

    iget-object v5, v0, Lipv;->a:Lixo;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lhbk;->am:Ljava/util/ArrayList;

    iget-object v9, v0, Lipv;->a:Lixo;

    iget-object v9, v9, Lixo;->a:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-boolean v3, v0, Lipv;->g:Z

    iget-object v5, v0, Lipv;->i:[I

    const/4 v9, 0x2

    invoke-static {v5, v9}, Lboz;->b([II)[I

    move-result-object v5

    iput-object v5, v0, Lipv;->i:[I

    :cond_0
    add-int/lit8 v5, v4, 0x1

    aput-object v0, v7, v4

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v4, v5

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-static {v7, v0}, Lgth;->a([Lipv;Lipv;)Lipv;

    move-result-object v2

    invoke-direct {p0, v2}, Lhbk;->a(Lipv;)I

    move-result v0

    if-nez v2, :cond_7

    array-length v4, v7

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v0, v7, v2

    iget-boolean v5, v0, Lipv;->h:Z

    if-eqz v5, :cond_5

    :goto_2
    if-nez v0, :cond_2

    invoke-direct {p0, v7, v3}, Lhbk;->a([Lipv;Z)Lipv;

    move-result-object v0

    :cond_2
    invoke-direct {p0, v0}, Lhbk;->a(Lipv;)I

    move-result v2

    :goto_3
    const/16 v3, 0x7f

    if-eq v2, v3, :cond_3

    move-object v0, v1

    :cond_3
    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-boolean v1, p0, Lhbk;->at:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lhbk;->g:Lgvt;

    invoke-interface {v1, v7}, Lgvt;->a([Lipv;)V

    iget-object v1, p0, Lhbk;->g:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    iget-object v1, p0, Lhbk;->g:Lgvt;

    iget-boolean v2, p0, Lhbk;->as:Z

    invoke-interface {v1, v2}, Lgvt;->a(Z)V

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    :cond_4
    return-void

    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_2

    :cond_7
    move v10, v0

    move-object v0, v2

    move v2, v10

    goto :goto_3
.end method

.method static synthetic e(Lhbk;)Lut;
    .locals 1

    iget-object v0, p0, Lhbk;->aA:Lut;

    return-object v0
.end method

.method static synthetic f(Lhbk;)Luu;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhbk;->az:Luu;

    return-object v0
.end method

.method static synthetic g(Lhbk;)Lut;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhbk;->aA:Lut;

    return-object v0
.end method

.method static synthetic h(Lhbk;)Ljau;
    .locals 1

    iget-object v0, p0, Lhbk;->av:Ljau;

    return-object v0
.end method

.method static synthetic i(Lhbk;)Lhbp;
    .locals 1

    iget-object v0, p0, Lhbk;->aw:Lhbp;

    return-object v0
.end method

.method static synthetic j(Lhbk;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lhbk;->am:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Lhbk;)V
    .locals 0

    invoke-direct {p0}, Lhbk;->M()V

    return-void
.end method

.method static synthetic l(Lhbk;)Z
    .locals 1

    iget-boolean v0, p0, Lhbk;->au:Z

    return v0
.end method

.method static synthetic m(Lhbk;)V
    .locals 15

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhbk;->an:Landroid/accounts/Account;

    sget-object v6, Lhbk;->a:[I

    iget-object v3, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i()Z

    move-result v13

    iget-object v3, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v14

    const/4 v12, 0x1

    move-object v3, v2

    move v5, v4

    move-object v7, v2

    move v8, v4

    move-object v9, v2

    move-object v10, v2

    move-object v11, v2

    invoke-static/range {v0 .. v14}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lioq;Ljava/util/Collection;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f6

    invoke-virtual {p0, v0, v1}, Lhbk;->a(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhbk;->aw:Lhbp;

    return-void
.end method

.method public final a()I
    .locals 1

    iget-object v0, p0, Lhbk;->aj:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->aj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhbk;->aj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    const v0, 0x7f040135    # com.google.android.gms.R.layout.wallet_fragment_payment_details

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0a0334    # com.google.android.gms.R.id.payment_details_content

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhbk;->b:Landroid/view/View;

    const v0, 0x7f0a030c    # com.google.android.gms.R.id.content_wrapper

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhbk;->aj:Landroid/view/View;

    const v0, 0x7f0a0338    # com.google.android.gms.R.id.payment_details_prog_bar_view

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iput-object v0, p0, Lhbk;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iget v0, p0, Lhbk;->ax:I

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_0
    iget v3, p0, Lhbk;->ay:I

    if-ne v3, v5, :cond_2

    move v3, v1

    :goto_1
    if-ne v0, v3, :cond_3

    :goto_2
    invoke-static {v1}, Lbkm;->b(Z)V

    iget v0, p0, Lhbk;->ax:I

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lhbk;->aj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lhbk;->ax:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v1, p0, Lhbk;->ay:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v5, p0, Lhbk;->ax:I

    iput v5, p0, Lhbk;->ay:I

    :cond_0
    const v0, 0x7f0a0335    # com.google.android.gms.R.id.payment_details_instrument_selector

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgxg;

    iput-object v0, p0, Lhbk;->e:Lgxg;

    iget-object v0, p0, Lhbk;->e:Lgxg;

    iget-object v1, p0, Lhbk;->ah:Lgxh;

    invoke-interface {v0, v1}, Lgxg;->a(Lgxh;)V

    const v0, 0x7f0a0336    # com.google.android.gms.R.id.payment_details_billing_address_selector

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgvt;

    iput-object v0, p0, Lhbk;->f:Lgvt;

    iget-object v0, p0, Lhbk;->f:Lgvt;

    iget-object v1, p0, Lhbk;->ag:Lgvu;

    invoke-interface {v0, v1}, Lgvt;->a(Lgvu;)V

    const v0, 0x7f0a0337    # com.google.android.gms.R.id.payment_details_shipping_address_selector

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgvt;

    iput-object v0, p0, Lhbk;->g:Lgvt;

    iget-boolean v0, p0, Lhbk;->ar:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhbk;->g:Lgvt;

    iget-object v1, p0, Lhbk;->af:Lgvu;

    invoke-interface {v0, v1}, Lgvt;->a(Lgvu;)V

    :goto_3
    const v0, 0x7f0a0339    # com.google.android.gms.R.id.payment_details_button_bar

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lhbk;->d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lhbk;->d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    return-object v4

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhbk;->g:Lgvt;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lgvt;->setVisibility(I)V

    goto :goto_3
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iput-boolean v3, p0, Lhbk;->at:Z

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "payment_details"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lhbk;->Z:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lhbk;->K()V

    :cond_1
    invoke-direct {p0, v3, v3}, Lhbk;->a(ZI)V

    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    :goto_1
    return-void

    :pswitch_0
    packed-switch p2, :pswitch_data_1

    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to add billing address to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    iput-object v0, p0, Lhbk;->ab:Lizz;

    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lipv;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, p0, Lhbk;->ad:Lipv;

    iget-boolean v1, p0, Lhbk;->aa:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->f:Lgvt;

    invoke-interface {v1}, Lgvt;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->f:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhbk;->f:Lgvt;

    invoke-interface {v0}, Lgvt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->f:Lgvt;

    iget-object v1, p0, Lhbk;->ad:Lipv;

    invoke-interface {v0, v1}, Lgvt;->a(Lipv;)V

    goto :goto_0

    :pswitch_3
    packed-switch p2, :pswitch_data_2

    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to update billing address to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_4
    iput-object v0, p0, Lhbk;->ab:Lizz;

    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lipv;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, p0, Lhbk;->ad:Lipv;

    iget-boolean v1, p0, Lhbk;->aa:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->f:Lgvt;

    invoke-interface {v1}, Lgvt;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->f:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lhbk;->f:Lgvt;

    invoke-interface {v0}, Lgvt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->f:Lgvt;

    iget-object v1, p0, Lhbk;->ad:Lipv;

    invoke-interface {v0, v1}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_6
    packed-switch p2, :pswitch_data_3

    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to add shipping address to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_7
    iput-object v0, p0, Lhbk;->ab:Lizz;

    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lipv;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-boolean v1, p0, Lhbk;->aa:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->g:Lgvt;

    invoke-interface {v1}, Lgvt;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->g:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lhbk;->g:Lgvt;

    invoke-interface {v0}, Lgvt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->g:Lgvt;

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-interface {v0, v1}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_9
    packed-switch p2, :pswitch_data_4

    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to update shipping address to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_a
    iput-object v0, p0, Lhbk;->ab:Lizz;

    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lipv;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-boolean v1, p0, Lhbk;->aa:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->g:Lgvt;

    invoke-interface {v1}, Lgvt;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->g:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lhbk;->g:Lgvt;

    invoke-interface {v0}, Lgvt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->g:Lgvt;

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-interface {v0, v1}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_c
    packed-switch p2, :pswitch_data_5

    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to add instrument to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_d
    iput-object v0, p0, Lhbk;->ab:Lizz;

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lioj;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-boolean v1, p0, Lhbk;->aa:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1}, Lgxg;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1, v0}, Lgxg;->a(Lioj;)V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lhbk;->e:Lgxg;

    invoke-interface {v0}, Lgxg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->e:Lgxg;

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-interface {v0, v1}, Lgxg;->a(Lioj;)V

    goto/16 :goto_0

    :pswitch_f
    packed-switch p2, :pswitch_data_6

    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to add instrument to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_10
    iput-object v0, p0, Lhbk;->ab:Lizz;

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lioj;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-boolean v1, p0, Lhbk;->aa:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1}, Lgxg;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhbk;->e:Lgxg;

    invoke-interface {v1, v0}, Lgxg;->a(Lioj;)V

    goto/16 :goto_0

    :pswitch_11
    iget-object v0, p0, Lhbk;->e:Lgxg;

    invoke-interface {v0}, Lgxg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->e:Lgxg;

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-interface {v0, v1}, Lgxg;->a(Lioj;)V

    goto/16 :goto_0

    :pswitch_12
    packed-switch p2, :pswitch_data_7

    :pswitch_13
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to signup to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_14
    iget-object v1, p0, Lhbk;->aw:Lhbp;

    const-string v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-interface {v1, v0}, Lhbp;->a(Landroid/accounts/Account;)V

    goto/16 :goto_1

    :pswitch_15
    invoke-direct {p0, p2, p3}, Lhbk;->a(ILandroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_12
        :pswitch_c
        :pswitch_0
        :pswitch_6
        :pswitch_f
        :pswitch_3
        :pswitch_9
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_4
    .packed-switch -0x1
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_5
    .packed-switch -0x1
        :pswitch_d
        :pswitch_e
    .end packed-switch

    :pswitch_data_6
    .packed-switch -0x1
        :pswitch_10
        :pswitch_11
    .end packed-switch

    :pswitch_data_7
    .packed-switch -0x1
        :pswitch_15
        :pswitch_15
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v0, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v0, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v0, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhbk;->au:Z

    iget-object v0, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f()Z

    move-result v0

    iput-boolean v0, p0, Lhbk;->ar:Z

    iget-boolean v0, p0, Lhbk;->au:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lhbk;->ar:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x194

    invoke-direct {p0, v0}, Lhbk;->d(I)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhbk;->as:Z

    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lhbk;->an:Landroid/accounts/Account;

    const-string v0, "allowChangeAccounts"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhbk;->ao:Z

    iget-boolean v0, p0, Lhbk;->ar:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lhfx;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lhbk;->am:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p0, Lhbk;->h:Lhca;

    if-nez v0, :cond_3

    new-instance v0, Lhbt;

    iget-object v1, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lhbk;->an:Landroid/accounts/Account;

    iget-object v3, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, p1, v3}, Lhbt;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lhbk;->h:Lhca;

    :cond_3
    :try_start_0
    check-cast p1, Lhbp;

    iput-object p1, p0, Lhbk;->aw:Lhbp;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Activity must implement PaymentsDialogEventListener"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v2, 0x0

    iput-boolean v0, p0, Lhbk;->at:Z

    iput-boolean v2, p0, Lhbk;->aa:Z

    invoke-direct {p0, v0, v2}, Lhbk;->a(ZI)V

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Intent;I)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v1, 0x7f050011    # com.google.android.gms.R.anim.wallet_push_up_in

    invoke-virtual {v0, v1, v2}, Lo;->overridePendingTransition(II)V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, -0x1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lhbk;->q()V

    if-eqz p1, :cond_4

    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhbk;->at:Z

    const-string v0, "walletItemsReceived"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhbk;->Y:Z

    const-string v0, "getWalletItemsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "getWalletItemsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lizz;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lizz;

    iput-object v0, p0, Lhbk;->ab:Lizz;

    :cond_0
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lhbk;->ac:I

    :cond_1
    const-string v0, "fullWalletRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "fullWalletRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Ljau;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljau;

    iput-object v0, p0, Lhbk;->av:Ljau;

    :cond_2
    const-string v0, "selectedBillingAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "selectedBillingAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lipv;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, p0, Lhbk;->ad:Lipv;

    :cond_3
    :goto_0
    return-void

    :cond_4
    new-instance v0, Luu;

    const-string v1, "get_wallet_items_from_chrome"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhbk;->az:Luu;

    iget-object v0, p0, Lhbk;->az:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lhbk;->aA:Lut;

    new-instance v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    iput-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "startingContentHeight"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lhbk;->ax:I

    const-string v1, "startingContentHeight"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string v1, "startingContentWidth"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lhbk;->ay:I

    const-string v1, "startingContentWidth"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhbk;->ak:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "payment_details"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lhbk;->aj:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->aj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhbk;->aj:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lhbk;->L()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lhbk;->ac:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "model"

    iget-object v1, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lhbk;->at:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "walletItemsReceived"

    iget-boolean v1, p0, Lhbk;->Y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lhbk;->ab:Lizz;

    if-eqz v0, :cond_0

    const-string v0, "getWalletItemsResponse"

    iget-object v1, p0, Lhbk;->ab:Lizz;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    iget-object v0, p0, Lhbk;->av:Ljau;

    if-eqz v0, :cond_1

    const-string v0, "fullWalletRequest"

    iget-object v1, p0, Lhbk;->av:Ljau;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_1
    iget-object v0, p0, Lhbk;->ad:Lipv;

    if-eqz v0, :cond_2

    const-string v0, "selectedBillingAddress"

    iget-object v1, p0, Lhbk;->ad:Lipv;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_2
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v3, Ljau;

    invoke-direct {v3}, Ljau;-><init>()V

    new-instance v0, Ljae;

    invoke-direct {v0}, Ljae;-><init>()V

    iput-object v0, v3, Ljau;->p:Ljae;

    iget-object v0, v3, Ljau;->p:Ljae;

    const-string v4, "USD"

    iput-object v4, v0, Ljae;->a:Ljava/lang/String;

    iput-boolean v2, v3, Ljau;->m:Z

    iget-boolean v0, p0, Lhbk;->as:Z

    iput-boolean v0, v3, Ljau;->h:Z

    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v0, v0, Lioj;->a:Ljava/lang/String;

    iput-object v0, v3, Ljau;->b:Ljava/lang/String;

    iget-boolean v0, p0, Lhbk;->au:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Ljau;->i:Z

    iget-object v0, p0, Lhbk;->al:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ljau;->y:Ljava/lang/String;

    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhbk;->ar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v0, v0, Lipv;->b:Ljava/lang/String;

    iput-object v0, v3, Ljau;->c:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v4, p0, Lhbk;->ad:Lipv;

    iput-object v4, v0, Lioj;->e:Lipv;

    iput-object v3, p0, Lhbk;->av:Ljau;

    iget-object v0, p0, Lhbk;->h:Lhca;

    invoke-interface {v0, v3, v1}, Lhca;->a(Ljau;Z)V

    iget-object v0, p0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    invoke-direct {p0, v1, v2}, Lhbk;->a(ZI)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public final w()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget-boolean v0, p0, Lhbk;->at:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhbk;->Z:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lhbk;->K()V

    goto :goto_0
.end method

.method public final x()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhbk;->aa:Z

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->x()V

    invoke-direct {p0}, Lhbk;->L()V

    return-void
.end method
