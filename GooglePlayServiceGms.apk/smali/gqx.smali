.class public final Lgqx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/Address;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/Address;)V
    .locals 0

    iput-object p1, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/wallet/Address;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgqx;-><init>(Lcom/google/android/gms/wallet/Address;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/Address;->j:Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->f:Ljava/lang/String;

    return-object p0
.end method

.method public final g(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final h(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->h:Ljava/lang/String;

    return-object p0
.end method

.method public final i(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->i:Ljava/lang/String;

    return-object p0
.end method

.method public final j(Ljava/lang/String;)Lgqx;
    .locals 1

    iget-object v0, p0, Lgqx;->a:Lcom/google/android/gms/wallet/Address;

    iput-object p1, v0, Lcom/google/android/gms/wallet/Address;->k:Ljava/lang/String;

    return-object p0
.end method
