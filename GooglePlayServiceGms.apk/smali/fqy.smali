.class public abstract Lfqy;
.super Lah;
.source "SourceFile"


# instance fields
.field Y:Ljava/lang/String;

.field public Z:Ljava/lang/String;

.field public aa:Ljava/lang/String;

.field ab:Z

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lah;-><init>()V

    return-void
.end method

.method protected static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    const-string v0, "Account name must not be empty."

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "plusPageId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "client_application_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public E_()V
    .locals 1

    invoke-super {p0}, Lah;->E_()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqy;->ab:Z

    return-void
.end method

.method protected J()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract L()V
.end method

.method protected abstract M()Landroid/widget/BaseAdapter;
.end method

.method protected final Q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfqy;->Y:Ljava/lang/String;

    return-object v0
.end method

.method protected final R()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfqy;->i:Ljava/lang/String;

    return-object v0
.end method

.method protected final S()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfqy;->Z:Ljava/lang/String;

    return-object v0
.end method

.method protected final T()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfqy;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public final U()Landroid/widget/BaseAdapter;
    .locals 1

    invoke-super {p0}, Lah;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lah;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfqy;->Y:Ljava/lang/String;

    const-string v1, "plusPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfqy;->i:Ljava/lang/String;

    const-string v1, "client_application_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfqy;->Z:Ljava/lang/String;

    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfqy;->aa:Ljava/lang/String;

    return-void
.end method

.method public final bridge synthetic b()Landroid/widget/ListAdapter;
    .locals 1

    invoke-super {p0}, Lah;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lah;->d(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lfqy;->J()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfqy;->a()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lfqy;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {p0}, Lfqy;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    invoke-virtual {p0}, Lfqy;->L()V

    invoke-virtual {p0}, Lfqy;->M()Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfqy;->a(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public g_()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfqy;->ab:Z

    invoke-super {p0}, Lah;->g_()V

    return-void
.end method
