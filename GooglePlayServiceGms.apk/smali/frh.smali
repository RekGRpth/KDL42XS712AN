.class public final Lfrh;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfaz;


# instance fields
.field private Y:Z

.field private Z:Z

.field private final a:Lftz;

.field private aa:Lbbo;

.field private ab:Ljava/util/ArrayList;

.field private ac:Ljava/util/ArrayList;

.field private b:Lfaj;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/ArrayList;

.field private g:Ljava/util/ArrayList;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lfrh;-><init>(Lftz;)V

    return-void
.end method

.method private constructor <init>(Lftz;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-object p1, p0, Lfrh;->a:Lftz;

    return-void
.end method

.method private J()V
    .locals 4

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Lfri;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lfrh;->aa:Lbbo;

    iget-object v2, p0, Lfrh;->ab:Ljava/util/ArrayList;

    iget-object v3, p0, Lfrh;->ac:Ljava/util/ArrayList;

    invoke-interface {v0, v1, v2, v3}, Lfri;->a(Lbbo;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Lfrh;
    .locals 3

    sget-object v1, Lftx;->a:Lftz;

    const-string v0, "Account name must not be empty."

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "Update person ID must not be empty"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Circles to add and remove are empty, nothing to do."

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "accountName"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "plusPageId"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "updatePersonId"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "circleIdsToAdd"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v2, "circleIdsToRemove"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v2, "callingPackageName"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "clientApplicationId"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lfrh;

    invoke-direct {v2, v1}, Lfrh;-><init>(Lftz;)V

    invoke-virtual {v2, v0}, Lfrh;->g(Landroid/os/Bundle;)V

    return-object v2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    if-nez p1, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private b()V
    .locals 8

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfrh;->Z:Z

    iget-object v0, p0, Lfrh;->b:Lfaj;

    iget-object v2, p0, Lfrh;->c:Ljava/lang/String;

    iget-object v3, p0, Lfrh;->d:Ljava/lang/String;

    iget-object v4, p0, Lfrh;->e:Ljava/lang/String;

    iget-object v1, p0, Lfrh;->f:Ljava/util/ArrayList;

    invoke-static {v1}, Lfrh;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v1, p0, Lfrh;->g:Ljava/util/ArrayList;

    invoke-static {v1}, Lfrh;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    sget-object v7, Lfny;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v0, v0, Lfaj;->a:Lfch;

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lfch;->a(Lfaz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-boolean v0, p0, Lfrh;->Y:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfrh;->Z:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfrh;->Y:Z

    iget-object v0, p0, Lfrh;->b:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    :cond_1
    return-void
.end method

.method public final a()V
    .locals 2

    iget-boolean v0, p0, Lfrh;->Z:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfrh;->Y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrh;->aa:Lbbo;

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "UpdateCirclesFragment should only be used once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lfrh;->b:Lfaj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfrh;->b:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lfrh;->Z:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lfrh;->b()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfrh;->Y:Z

    iget-object v0, p0, Lfrh;->b:Lfaj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfrh;->b:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lfrh;->b:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfri;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement UpdateCirclesFragmentHost."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    iput-object p1, p0, Lfrh;->aa:Lbbo;

    invoke-direct {p0}, Lfrh;->J()V

    return-void
.end method

.method public final a(Lbbo;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrh;->Z:Z

    iput-object p1, p0, Lfrh;->aa:Lbbo;

    iget-object v0, p0, Lfrh;->f:Ljava/util/ArrayList;

    invoke-static {v0, p2}, Lfrh;->a(Ljava/util/ArrayList;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lfrh;->ab:Ljava/util/ArrayList;

    iget-object v0, p0, Lfrh;->g:Ljava/util/ArrayList;

    invoke-static {v0, p3}, Lfrh;->a(Ljava/util/ArrayList;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lfrh;->ac:Ljava/util/ArrayList;

    invoke-direct {p0}, Lfrh;->J()V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lfrh;->q()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfrh;->c:Ljava/lang/String;

    const-string v1, "plusPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfrh;->d:Ljava/lang/String;

    const-string v1, "updatePersonId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfrh;->e:Ljava/lang/String;

    const-string v1, "circleIdsToAdd"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lfrh;->f:Ljava/util/ArrayList;

    const-string v1, "circleIdsToRemove"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lfrh;->g:Ljava/util/ArrayList;

    const-string v1, "callingPackageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfrh;->h:Ljava/lang/String;

    const-string v1, "clientApplicationId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfrh;->i:Ljava/lang/String;

    iget-object v0, p0, Lfrh;->b:Lfaj;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrh;->a:Lftz;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lfrh;->i:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/plus/service/PlusService;->a(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lfrh;->h:Ljava/lang/String;

    move-object v2, p0

    move-object v3, p0

    invoke-interface/range {v0 .. v5}, Lftz;->a(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)Lfaj;

    move-result-object v0

    iput-object v0, p0, Lfrh;->b:Lfaj;

    :cond_0
    iget-object v0, p0, Lfrh;->b:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    iget-boolean v0, p0, Lfrh;->Y:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrh;->Y:Z

    invoke-direct {p0}, Lfrh;->b()V

    :cond_0
    return-void
.end method

.method public final y()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    iget-object v0, p0, Lfrh;->b:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    return-void
.end method
