.class public final Ljbl;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljbs;

.field private c:Z

.field private d:Ljbu;

.field private e:Z

.field private f:Ljbu;

.field private g:Z

.field private h:Ljbu;

.field private i:Z

.field private j:Ljbu;

.field private k:Z

.field private l:Ljbu;

.field private m:Z

.field private n:Ljbu;

.field private o:Z

.field private p:I

.field private q:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-object v0, p0, Ljbl;->d:Ljbu;

    iput-object v0, p0, Ljbl;->f:Ljbu;

    iput-object v0, p0, Ljbl;->h:Ljbu;

    iput-object v0, p0, Ljbl;->j:Ljbu;

    iput-object v0, p0, Ljbl;->l:Ljbu;

    iput-object v0, p0, Ljbl;->b:Ljbs;

    iput-object v0, p0, Ljbl;->n:Ljbu;

    const/4 v0, 0x1

    iput v0, p0, Ljbl;->p:I

    const/4 v0, -0x1

    iput v0, p0, Ljbl;->q:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljbl;->q:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljbl;->b()I

    :cond_0
    iget v0, p0, Ljbl;->q:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Ljbl;->c:Z

    iput-object v0, p0, Ljbl;->d:Ljbu;

    goto :goto_0

    :sswitch_2
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Ljbl;->e:Z

    iput-object v0, p0, Ljbl;->f:Ljbu;

    goto :goto_0

    :sswitch_3
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Ljbl;->g:Z

    iput-object v0, p0, Ljbl;->h:Ljbu;

    goto :goto_0

    :sswitch_4
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Ljbl;->i:Z

    iput-object v0, p0, Ljbl;->j:Ljbu;

    goto :goto_0

    :sswitch_5
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Ljbl;->k:Z

    iput-object v0, p0, Ljbl;->l:Ljbu;

    goto :goto_0

    :sswitch_6
    new-instance v0, Ljbs;

    invoke-direct {v0}, Ljbs;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Ljbl;->a:Z

    iput-object v0, p0, Ljbl;->b:Ljbs;

    goto :goto_0

    :sswitch_7
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Ljbl;->m:Z

    iput-object v0, p0, Ljbl;->n:Ljbu;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v1, p0, Ljbl;->o:Z

    iput v0, p0, Ljbl;->p:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x318 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Ljbl;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbl;->d:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-boolean v0, p0, Ljbl;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljbl;->f:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_1
    iget-boolean v0, p0, Ljbl;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljbl;->h:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    iget-boolean v0, p0, Ljbl;->i:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljbl;->j:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_3
    iget-boolean v0, p0, Ljbl;->k:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljbl;->l:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_4
    iget-boolean v0, p0, Ljbl;->a:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljbl;->b:Ljbs;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_5
    iget-boolean v0, p0, Ljbl;->m:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Ljbl;->n:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_6
    iget-boolean v0, p0, Ljbl;->o:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x63

    iget v1, p0, Ljbl;->p:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_7
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljbl;->c:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbl;->d:Ljbu;

    invoke-static {v0, v1}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljbl;->e:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljbl;->f:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljbl;->g:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljbl;->h:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljbl;->i:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljbl;->j:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Ljbl;->k:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljbl;->l:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Ljbl;->a:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ljbl;->b:Ljbs;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Ljbl;->m:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Ljbl;->n:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Ljbl;->o:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x63

    iget v2, p0, Ljbl;->p:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Ljbl;->q:I

    return v0
.end method
