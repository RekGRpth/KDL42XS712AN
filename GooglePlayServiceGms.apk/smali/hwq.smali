.class public final Lhwq;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/fused/NlpLocationReceiverService;


# direct methods
.method public constructor <init>(Lcom/google/android/location/fused/NlpLocationReceiverService;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lhwq;->a:Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 11

    const/4 v10, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "GCoreFlp"

    const-string v1, "unknown message when trying to process location"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    iget-object v1, p0, Lhwq;->a:Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-static {v1}, Lcom/google/android/location/fused/NlpLocationReceiverService;->a(Lcom/google/android/location/fused/NlpLocationReceiverService;)Lhvp;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lhvp;->a(Landroid/location/Location;Liwf;)V

    goto :goto_0

    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    if-eq v0, v2, :cond_2

    iget-object v4, p0, Lhwq;->a:Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-static {v4}, Lcom/google/android/location/fused/NlpLocationReceiverService;->a(Lcom/google/android/location/fused/NlpLocationReceiverService;)Lhvp;

    move-result-object v4

    iget-object v5, v4, Lhvp;->l:Lhwr;

    invoke-virtual {v5}, Lhwr;->c()Z

    move-result v5

    iget-object v6, v4, Lhvp;->l:Lhwr;

    iget-object v7, v6, Lhwr;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iput v0, v6, Lhwr;->d:I

    iget-object v8, v6, Lhwr;->b:Lhuz;

    invoke-static {}, Lhuz;->a()J

    move-result-wide v8

    iput-wide v8, v6, Lhwr;->e:J

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v6, v4, Lhvp;->l:Lhwr;

    invoke-virtual {v6}, Lhwr;->c()Z

    move-result v6

    if-eq v6, v5, :cond_2

    const-string v5, "GCoreFlp"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "Wifi availability changed, mNlpWifiStatus=%s"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-static {v5, v6}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iget-object v0, v4, Lhvp;->n:Lhwb;

    iget-object v4, v4, Lhvp;->l:Lhwr;

    invoke-virtual {v0, v4}, Lhwb;->a(Lhwr;)V

    :cond_2
    if-eq v1, v2, :cond_0

    iget-object v0, p0, Lhwq;->a:Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-static {v0}, Lcom/google/android/location/fused/NlpLocationReceiverService;->a(Lcom/google/android/location/fused/NlpLocationReceiverService;)Lhvp;

    move-result-object v0

    iget-object v4, v0, Lhvp;->l:Lhwr;

    invoke-virtual {v4}, Lhwr;->b()Z

    move-result v4

    iget-object v5, v0, Lhvp;->l:Lhwr;

    iget-object v6, v5, Lhwr;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_1
    iput v1, v5, Lhwr;->c:I

    iget-object v7, v5, Lhwr;->b:Lhuz;

    invoke-static {}, Lhuz;->a()J

    move-result-wide v7

    iput-wide v7, v5, Lhwr;->e:J

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v5, v0, Lhvp;->l:Lhwr;

    invoke-virtual {v5}, Lhwr;->b()Z

    move-result v5

    if-eq v5, v4, :cond_0

    const-string v4, "GCoreFlp"

    invoke-static {v4, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "Cell status changed, mNlpCellStatus=%s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v4, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    iget-object v1, v0, Lhvp;->n:Lhwb;

    iget-object v0, v0, Lhvp;->l:Lhwr;

    invoke-virtual {v1, v0}, Lhwb;->a(Lhwr;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v6

    throw v0

    :pswitch_2
    iget-object v0, p0, Lhwq;->a:Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-static {v0}, Lcom/google/android/location/fused/NlpLocationReceiverService;->a(Lcom/google/android/location/fused/NlpLocationReceiverService;)Lhvp;

    move-result-object v4

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    sget-object v1, Liwz;->a:Liwz;

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v5

    const-wide/32 v7, 0xf4240

    mul-long/2addr v5, v7

    iget-object v0, v4, Lhvp;->k:Liwp;

    invoke-virtual {v0}, Liwp;->b()I

    move-result v7

    iget-object v8, v4, Lhvp;->k:Liwp;

    iget-object v0, v8, Liwp;->a:Liwm;

    invoke-static {}, Liwm;->c()V

    iget-object v9, v8, Liwp;->b:Lixa;

    iget-object v0, v9, Lixa;->b:Liwm;

    invoke-static {}, Liwm;->c()V

    sget-object v0, Liwz;->a:Liwz;

    if-eq v1, v0, :cond_4

    sget-object v0, Liwz;->b:Liwz;

    if-eq v1, v0, :cond_4

    iput-object v1, v9, Lixa;->h:Liwz;

    :cond_4
    sget-object v0, Liwz;->b:Liwz;

    if-ne v1, v0, :cond_5

    move v0, v2

    :goto_2
    iput-boolean v0, v9, Lixa;->i:Z

    iget-object v0, v8, Liwp;->c:Liwg;

    iget-object v0, v8, Liwp;->i:Liws;

    invoke-interface {v0, v1}, Liws;->a(Liwz;)V

    iget-object v0, v8, Liwp;->i:Liws;

    invoke-interface {v0, v5, v6}, Liws;->a(J)Liws;

    move-result-object v0

    iput-object v0, v8, Liwp;->i:Liws;

    iget-object v0, v4, Lhvp;->k:Liwp;

    invoke-virtual {v0}, Liwp;->b()I

    move-result v0

    invoke-virtual {v4, v7, v0}, Lhvp;->a(II)V

    goto/16 :goto_0

    :pswitch_3
    sget-object v1, Liwz;->b:Liwz;

    goto :goto_1

    :pswitch_4
    sget-object v1, Liwz;->c:Liwz;

    goto :goto_1

    :pswitch_5
    sget-object v1, Liwz;->d:Liwz;

    goto :goto_1

    :pswitch_6
    sget-object v1, Liwz;->e:Liwz;

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
