.class public final Lada;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field a:Lbbu;

.field b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lada;->c:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lada;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)V

    return-void
.end method

.method private varargs a()Ljava/lang/Integer;
    .locals 4

    const/4 v3, 0x2

    :try_start_0
    iget-object v0, p0, Lada;->c:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lxu;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lada;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbbt; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbbu; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "AdsSettingsActivity"

    const-string v2, "Could not clear advertising ID."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "AdsSettingsActivity"

    const-string v2, "Google Play services not available?"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "AdsSettingsActivity"

    const-string v2, "Google Play services repairable."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iput-object v0, p0, Lada;->a:Lbbu;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lada;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ljava/lang/Integer;

    iget-object v0, p0, Lada;->c:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lada;->c:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    iget-object v1, p0, Lada;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lada;->c:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    iget-object v1, p0, Lada;->a:Lbbu;

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Lbbu;)V

    goto :goto_0
.end method
