.class public final Lhaq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;)V
    .locals 0

    iput-object p1, p0, Lhaq;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    iget-object v0, p0, Lhaq;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lhaq;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1, p2}, Lhhe;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lhaq;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lhaq;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lhaq;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->finish()V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    return-void
.end method
