.class public Layf;
.super Laxx;
.source "SourceFile"


# static fields
.field private static final b:J

.field private static final c:J

.field private static final d:J

.field private static final e:J


# instance fields
.field private f:J

.field private g:Lawm;

.field private final h:Landroid/os/Handler;

.field private final i:Layj;

.field private final j:Layj;

.field private final k:Layj;

.field private final l:Layj;

.field private final m:Layj;

.field private final n:Layj;

.field private final o:Layj;

.field private final p:Layj;

.field private final q:Ljava/lang/Runnable;

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/16 v2, 0x18

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Layf;->b:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Layf;->c:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Layf;->d:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Layf;->e:J

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const-string v0, "urn:x-cast:com.google.cast.media"

    const-string v1, "MediaControlChannel"

    invoke-direct {p0, v0, v1}, Laxx;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Layf;->h:Landroid/os/Handler;

    new-instance v0, Layg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Layg;-><init>(Layf;B)V

    iput-object v0, p0, Layf;->q:Ljava/lang/Runnable;

    new-instance v0, Layj;

    sget-wide v1, Layf;->c:J

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Layf;->i:Layj;

    new-instance v0, Layj;

    sget-wide v1, Layf;->b:J

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Layf;->j:Layj;

    new-instance v0, Layj;

    sget-wide v1, Layf;->b:J

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Layf;->k:Layj;

    new-instance v0, Layj;

    sget-wide v1, Layf;->b:J

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Layf;->l:Layj;

    new-instance v0, Layj;

    sget-wide v1, Layf;->d:J

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Layf;->m:Layj;

    new-instance v0, Layj;

    sget-wide v1, Layf;->b:J

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Layf;->n:Layj;

    new-instance v0, Layj;

    sget-wide v1, Layf;->b:J

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Layf;->o:Layj;

    new-instance v0, Layj;

    sget-wide v1, Layf;->b:J

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Layf;->p:Layj;

    invoke-direct {p0}, Layf;->k()V

    return-void
.end method

.method static synthetic a(Layf;Z)V
    .locals 0

    invoke-direct {p0, p1}, Layf;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    iget-boolean v0, p0, Layf;->r:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Layf;->r:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Layf;->h:Landroid/os/Handler;

    iget-object v1, p0, Layf;->q:Ljava/lang/Runnable;

    sget-wide v2, Layf;->e:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Layf;->h:Landroid/os/Handler;

    iget-object v1, p0, Layf;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Layf;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Layf;->r:Z

    return v0
.end method

.method static synthetic b(Layf;)Layj;
    .locals 1

    iget-object v0, p0, Layf;->i:Layj;

    return-object v0
.end method

.method static synthetic c(Layf;)Layj;
    .locals 1

    iget-object v0, p0, Layf;->j:Layj;

    return-object v0
.end method

.method static synthetic d(Layf;)Layj;
    .locals 1

    iget-object v0, p0, Layf;->k:Layj;

    return-object v0
.end method

.method static synthetic e(Layf;)Layj;
    .locals 1

    iget-object v0, p0, Layf;->l:Layj;

    return-object v0
.end method

.method static synthetic f(Layf;)Layj;
    .locals 1

    iget-object v0, p0, Layf;->m:Layj;

    return-object v0
.end method

.method static synthetic g(Layf;)Layj;
    .locals 1

    iget-object v0, p0, Layf;->n:Layj;

    return-object v0
.end method

.method static synthetic h(Layf;)Layj;
    .locals 1

    iget-object v0, p0, Layf;->o:Layj;

    return-object v0
.end method

.method static synthetic i(Layf;)Layj;
    .locals 1

    iget-object v0, p0, Layf;->p:Layj;

    return-object v0
.end method

.method private k()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Layf;->a(Z)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Layf;->f:J

    const/4 v0, 0x0

    iput-object v0, p0, Layf;->g:Lawm;

    iget-object v0, p0, Layf;->i:Layj;

    invoke-virtual {v0}, Layj;->a()V

    iget-object v0, p0, Layf;->m:Layj;

    invoke-virtual {v0}, Layj;->a()V

    iget-object v0, p0, Layf;->n:Layj;

    invoke-virtual {v0}, Layj;->a()V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 11

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Layf;->d()Lawi;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-wide v2

    :cond_1
    iget-wide v0, p0, Layf;->f:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Layf;->g:Lawm;

    iget-wide v9, v0, Lawm;->c:D

    iget-object v0, p0, Layf;->g:Lawm;

    iget-wide v4, v0, Lawm;->f:J

    iget-object v0, p0, Layf;->g:Lawm;

    iget v0, v0, Lawm;->d:I

    const-wide/16 v6, 0x0

    cmpl-double v1, v9, v6

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    :cond_2
    move-wide v2, v4

    goto :goto_0

    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v6, p0, Layf;->f:J

    sub-long/2addr v0, v6

    cmp-long v6, v0, v2

    if-gez v6, :cond_7

    move-wide v6, v2

    :goto_1
    cmp-long v0, v6, v2

    if-nez v0, :cond_4

    move-wide v2, v4

    goto :goto_0

    :cond_4
    iget-wide v0, v8, Lawi;->e:J

    long-to-double v6, v6

    mul-double/2addr v6, v9

    double-to-long v6, v6

    add-long/2addr v4, v6

    cmp-long v6, v4, v0

    if-lez v6, :cond_5

    :goto_2
    move-wide v2, v0

    goto :goto_0

    :cond_5
    cmp-long v0, v4, v2

    if-gez v0, :cond_6

    move-wide v0, v2

    goto :goto_2

    :cond_6
    move-wide v0, v4

    goto :goto_2

    :cond_7
    move-wide v6, v0

    goto :goto_1
.end method

.method public final a(Layi;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Layf;->f()J

    move-result-wide v1

    iget-object v3, p0, Layf;->p:Layj;

    invoke-virtual {v3, v1, v2, p1}, Layj;->a(JLayi;)V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Layf;->a(Z)V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "GET_STATUS"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v3, p0, Layf;->g:Lawm;

    if-eqz v3, :cond_0

    const-string v3, "mediaSessionId"

    iget-object v4, p0, Layf;->g:Lawm;

    iget-wide v4, v4, Lawm;->a:J

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v2}, Layf;->a(Ljava/lang/String;J)V

    return-wide v1

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final a(Layi;JLorg/json/JSONObject;)J
    .locals 8

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Layf;->f()J

    move-result-wide v1

    iget-object v3, p0, Layf;->m:Layj;

    invoke-virtual {v3, v1, v2, p1}, Layj;->a(JLayi;)V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Layf;->a(Z)V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "SEEK"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "mediaSessionId"

    invoke-virtual {p0}, Layf;->h()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "currentTime"

    long-to-double v4, p2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    if-eqz p4, :cond_0

    const-string v3, "customData"

    invoke-virtual {v0, v3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v2}, Layf;->a(Ljava/lang/String;J)V

    return-wide v1

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final a(Layi;Lawi;JLorg/json/JSONObject;)J
    .locals 8

    const/4 v4, 0x1

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Layf;->f()J

    move-result-wide v1

    iget-object v3, p0, Layf;->i:Layj;

    invoke-virtual {v3, v1, v2, p1}, Layj;->a(JLayi;)V

    invoke-direct {p0, v4}, Layf;->a(Z)V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "LOAD"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "media"

    invoke-virtual {p2}, Lawi;->a()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "autoplay"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v3, "currentTime"

    long-to-double v4, p3

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    if-eqz p5, :cond_0

    const-string v3, "customData"

    invoke-virtual {v0, v3, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v2}, Layf;->a(Ljava/lang/String;J)V

    return-wide v1

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final a(Layi;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Layf;->f()J

    move-result-wide v1

    iget-object v3, p0, Layf;->j:Layj;

    invoke-virtual {v3, v1, v2, p1}, Layj;->a(JLayi;)V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Layf;->a(Z)V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "PAUSE"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "mediaSessionId"

    invoke-virtual {p0}, Layf;->h()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string v3, "customData"

    invoke-virtual {v0, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v2}, Layf;->a(Ljava/lang/String;J)V

    return-wide v1

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 9

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Layf;->a:Laye;

    const-string v3, "message received: %s"

    new-array v5, v4, [Ljava/lang/Object;

    aput-object p1, v5, v2

    invoke-virtual {v0, v3, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "type"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "requestId"

    const-wide/16 v6, -0x1

    invoke-virtual {v0, v5, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v5

    const-string v7, "MEDIA_STATUS"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    const-string v3, "status"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_b

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    iget-object v0, p0, Layf;->i:Layj;

    invoke-virtual {v0, v5, v6}, Layj;->a(J)Z

    move-result v8

    iget-object v0, p0, Layf;->m:Layj;

    invoke-virtual {v0}, Layj;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Layf;->m:Layj;

    invoke-virtual {v0, v5, v6}, Layj;->a(J)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v4

    :goto_0
    iget-object v3, p0, Layf;->n:Layj;

    invoke-virtual {v3}, Layj;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Layf;->n:Layj;

    invoke-virtual {v3, v5, v6}, Layj;->a(J)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Layf;->o:Layj;

    invoke-virtual {v3}, Layj;->b()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Layf;->o:Layj;

    invoke-virtual {v3, v5, v6}, Layj;->a(J)Z

    move-result v3

    if-nez v3, :cond_9

    :cond_1
    move v3, v4

    :goto_1
    if-eqz v0, :cond_10

    move v0, v1

    :goto_2
    if-eqz v3, :cond_2

    or-int/lit8 v0, v0, 0x1

    :cond_2
    if-nez v8, :cond_3

    iget-object v3, p0, Layf;->g:Lawm;

    if-nez v3, :cond_a

    :cond_3
    new-instance v0, Lawm;

    invoke-direct {v0, v7}, Lawm;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Layf;->g:Lawm;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    iput-wide v7, p0, Layf;->f:J

    const/4 v0, 0x7

    :goto_3
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    iput-wide v7, p0, Layf;->f:J

    invoke-virtual {p0}, Layf;->i()V

    :cond_4
    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    iput-wide v7, p0, Layf;->f:J

    invoke-virtual {p0}, Layf;->i()V

    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Layf;->j()V

    :cond_6
    iget-object v0, p0, Layf;->i:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z

    iget-object v0, p0, Layf;->j:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z

    iget-object v0, p0, Layf;->k:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z

    iget-object v0, p0, Layf;->l:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z

    iget-object v0, p0, Layf;->m:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z

    iget-object v0, p0, Layf;->n:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z

    iget-object v0, p0, Layf;->o:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z

    iget-object v0, p0, Layf;->p:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z

    :cond_7
    :goto_4
    return-void

    :cond_8
    move v0, v2

    goto/16 :goto_0

    :cond_9
    move v3, v2

    goto :goto_1

    :cond_a
    iget-object v3, p0, Layf;->g:Lawm;

    invoke-virtual {v3, v7, v0}, Lawm;->a(Lorg/json/JSONObject;I)I

    move-result v0

    goto :goto_3

    :cond_b
    const/4 v0, 0x0

    iput-object v0, p0, Layf;->g:Lawm;

    invoke-virtual {p0}, Layf;->i()V

    invoke-virtual {p0}, Layf;->j()V

    iget-object v0, p0, Layf;->p:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v5, v6, v3}, Layj;->a(JI)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    iget-object v3, p0, Layf;->a:Laye;

    const-string v5, "Message is malformed (%s); ignoring: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    aput-object p1, v1, v4

    invoke-virtual {v3, v5, v1}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_c
    :try_start_1
    const-string v7, "INVALID_PLAYER_STATE"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    iget-object v3, p0, Layf;->a:Laye;

    const-string v7, "received unexpected error: Invalid Player State."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v7, v8}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, "customData"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v3, p0, Layf;->i:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->j:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->k:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->l:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->m:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->n:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->o:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->p:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    goto :goto_4

    :cond_d
    const-string v7, "LOAD_FAILED"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    const-string v3, "customData"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v3, p0, Layf;->i:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    goto/16 :goto_4

    :cond_e
    const-string v7, "LOAD_CANCELLED"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    const-string v3, "customData"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v3, p0, Layf;->i:Layj;

    const/4 v7, 0x2

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    goto/16 :goto_4

    :cond_f
    const-string v7, "INVALID_REQUEST"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Layf;->a:Laye;

    const-string v7, "received unexpected error: Invalid Request."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v7, v8}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, "customData"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v3, p0, Layf;->i:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->j:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->k:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->l:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->m:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->n:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->o:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z

    iget-object v3, p0, Layf;->p:Layj;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7, v0}, Layj;->a(JILorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public final b()J
    .locals 2

    invoke-virtual {p0}, Layf;->d()Lawi;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v0, v0, Lawi;->e:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final b(Layi;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Layf;->f()J

    move-result-wide v1

    iget-object v3, p0, Layf;->l:Layj;

    invoke-virtual {v3, v1, v2, p1}, Layj;->a(JLayi;)V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Layf;->a(Z)V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "STOP"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "mediaSessionId"

    invoke-virtual {p0}, Layf;->h()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string v3, "customData"

    invoke-virtual {v0, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v2}, Layf;->a(Ljava/lang/String;J)V

    return-wide v1

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final c(Layi;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Layf;->f()J

    move-result-wide v1

    iget-object v3, p0, Layf;->k:Layj;

    invoke-virtual {v3, v1, v2, p1}, Layj;->a(JLayi;)V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Layf;->a(Z)V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "PLAY"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "mediaSessionId"

    invoke-virtual {p0}, Layf;->h()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string v3, "customData"

    invoke-virtual {v0, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v2}, Layf;->a(Ljava/lang/String;J)V

    return-wide v1

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final c()Lawm;
    .locals 1

    iget-object v0, p0, Layf;->g:Lawm;

    return-object v0
.end method

.method public final d()Lawi;
    .locals 1

    iget-object v0, p0, Layf;->g:Lawm;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Layf;->g:Lawm;

    iget-object v0, v0, Lawm;->b:Lawi;

    goto :goto_0
.end method

.method public final g()V
    .locals 0

    invoke-direct {p0}, Layf;->k()V

    return-void
.end method

.method public final h()J
    .locals 2

    iget-object v0, p0, Layf;->g:Lawm;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No current media session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Layf;->g:Lawm;

    iget-wide v0, v0, Lawm;->a:J

    return-wide v0
.end method

.method protected i()V
    .locals 0

    return-void
.end method

.method protected j()V
    .locals 0

    return-void
.end method
