.class public final Ldvv;
.super Ldvp;
.source "SourceFile"

# interfaces
.implements Ldvf;


# instance fields
.field private Z:Ljava/lang/String;

.field private aa:Ljava/lang/String;

.field private ab:Ldwk;

.field private ac:Ldwu;

.field private ad:Ljava/util/HashMap;

.field private ae:Z

.field private af:Landroid/os/Bundle;

.field private ag:Ljava/util/ArrayList;

.field private ah:Z

.field private i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ldvp;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldvv;->ad:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldvv;->ah:Z

    return-void
.end method

.method public static Q()V
    .locals 0

    return-void
.end method

.method private R()V
    .locals 3

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->n()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lbli;

    invoke-direct {v2}, Lbli;-><init>()V

    invoke-static {v2, v1}, Lbky;->a(Lbli;Ljava/util/List;)V

    invoke-virtual {v2}, Lbli;->d()[B

    move-result-object v1

    sget-object v2, Lcte;->p:Ldej;

    invoke-static {v1}, Lbpd;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ldej;->a(Lbdu;Ljava/lang/String;)Lbeh;

    move-result-object v0

    new-instance v1, Ldvy;

    invoke-direct {v1, p0}, Ldvy;-><init>(Ldvv;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(Ljava/util/ArrayList;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private S()V
    .locals 3

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->f:Lctl;

    invoke-interface {v1, v0}, Lctl;->c(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Ldvz;

    invoke-direct {v1, p0}, Ldvz;-><init>(Ldvv;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method private T()Z
    .locals 6

    const/4 v2, 0x0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    iget-object v1, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_2

    move v1, v2

    :goto_1
    move v3, v2

    :goto_2
    if-ge v3, v1, :cond_0

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v5, "@google.com"

    invoke-virtual {v0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2
.end method

.method private U()V
    .locals 4

    invoke-virtual {p0}, Ldvv;->L()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "problemDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ldwn;

    invoke-direct {v0}, Ldwn;-><init>()V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v3, "problemDialog"

    invoke-virtual {v0, v1, v2, v3}, Ldwn;->a(Landroid/content/Context;Lu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Ldvv;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Ldvv;->af:Landroid/os/Bundle;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ldvv;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "destAppVersion"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ldvv;

    invoke-direct {v1}, Ldvv;-><init>()V

    invoke-virtual {v1, v0}, Ldvv;->g(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private a(Lbdu;I)V
    .locals 3

    invoke-static {p2}, Ldec;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Ldvv;->af:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iget-object v2, p0, Ldvv;->af:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget-object v0, Lcte;->n:Lctp;

    iget-boolean v1, p0, Ldvv;->ae:Z

    iget-object v2, p0, Ldvv;->af:Landroid/os/Bundle;

    invoke-interface {v0, p1, v1, v2}, Lctp;->a(Lbdu;ZLandroid/os/Bundle;)Lbeh;

    move-result-object v0

    new-instance v1, Ldwa;

    invoke-direct {v1, p0}, Ldwa;-><init>(Ldvv;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    iget-object v0, p0, Ldvv;->ac:Ldwu;

    invoke-virtual {v0}, Ldwu;->notifyDataSetInvalidated()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Ldvv;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Ldvv;->ag:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Ldvv;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldvv;->aa:Ljava/lang/String;

    return-object v0
.end method

.method static d(I)I
    .locals 3

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown channel type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Ldvp;->E_()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldvv;->ah:Z

    return-void
.end method

.method public final P()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldvv;->Z:Ljava/lang/String;

    return-object v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Ldvp;->a(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-static {p3}, Lbet;->a(Landroid/content/Intent;)Lbev;

    move-result-object v0

    invoke-interface {v0}, Lbev;->b()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(Ljava/util/ArrayList;)V

    invoke-direct {p0}, Ldvv;->R()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/view/View;I)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ldvv;->ac:Ldwu;

    invoke-virtual {v0, p2}, Ldwu;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Ldwj;

    if-eqz v2, :cond_3

    check-cast v0, Ldwj;

    iget v0, v0, Ldwj;->b:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Ldvv;->a(Lbdu;I)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1, v3}, Ldvv;->a(Lbdu;I)V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Ldvv;->ah:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lbet;->a()Lbeu;

    move-result-object v1

    iget-object v0, p0, Ldvv;->ag:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldvv;->ag:Ljava/util/ArrayList;

    :goto_1
    iget-object v2, p0, Ldvv;->Z:Ljava/lang/String;

    invoke-interface {v1, v2}, Lbeu;->a(Ljava/lang/String;)Lbeu;

    move-result-object v1

    invoke-interface {v1, v0}, Lbeu;->a(Ljava/util/List;)Lbeu;

    move-result-object v0

    const-string v1, " "

    invoke-interface {v0, v1}, Lbeu;->c(Ljava/lang/String;)Lbeu;

    move-result-object v0

    invoke-interface {v0}, Lbeu;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Ldvv;->a(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1

    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_GOOGLE_DEBUG_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Ldvv;->a(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_5
    sget-object v0, Ldvi;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Ldvv;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    instance-of v1, v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-static {v0}, Ldwo;->a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)Ldwo;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v3, "unmuteDialog"

    invoke-virtual {v0, v1, v2, v3}, Ldwo;->a(Landroid/content/Context;Lu;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lbdu;)V
    .locals 2

    invoke-direct {p0}, Ldvv;->R()V

    sget-object v0, Lcte;->n:Lctp;

    invoke-interface {v0, p1}, Lctp;->d(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Ldvw;

    invoke-direct {v1, p0}, Ldvw;-><init>(Ldvv;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    invoke-direct {p0}, Ldvv;->S()V

    sget-object v0, Lcte;->p:Ldej;

    invoke-interface {v0, p1}, Ldej;->a(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Ldvx;

    invoke-direct {v1, p0}, Ldvx;-><init>(Ldvv;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()I

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j()Lbdu;

    move-result-object v0

    sget-object v1, Lcte;->p:Ldej;

    invoke-interface {v1, v0}, Ldej;->a(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Ldwc;

    invoke-direct {v1, p0}, Ldwc;-><init>(Ldvv;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 4

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Ldvv;->ad:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->n_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcte;->n:Lctp;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lctp;->b(Lbdu;Ljava/lang/String;)Lbeh;

    move-result-object v0

    new-instance v1, Ldwb;

    invoke-direct {v1, p0}, Ldwb;-><init>(Ldvv;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method public final a(Lctm;)V
    .locals 2

    invoke-interface {p1}, Lctm;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Lctm;->a()Lden;

    move-result-object v1

    invoke-static {v0}, Leee;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldvv;->ab:Ldwk;

    invoke-virtual {v0}, Ldwk;->h()V

    :cond_0
    iget-object v0, p0, Ldvv;->ab:Ldwk;

    invoke-virtual {v0, v1}, Ldwk;->a(Lbgo;)V

    return-void
.end method

.method public final a(Lctq;)V
    .locals 5

    invoke-interface {p1}, Lctq;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Lctq;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Ldvv;->U()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    const-string v2, "mobile_notifications_enabled"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v2

    iput-boolean v2, p0, Ldvv;->ae:Z

    const-string v2, "match_notifications_enabled"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v2

    const-string v3, "request_notifications_enabled"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v0

    iget-object v3, p0, Ldvv;->af:Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-static {v4}, Ldec;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Ldvv;->af:Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-static {v3}, Ldec;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Ldvv;->ac:Ldwu;

    invoke-virtual {v0}, Ldwu;->notifyDataSetChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a(Lctr;)V
    .locals 5

    const/4 v4, 0x0

    invoke-interface {p1}, Lctr;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-interface {p1}, Lctr;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lctr;->s_()Z

    move-result v3

    iget-object v0, p0, Ldvv;->ad:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v0, "GamesSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Application was not unmuted as it should have been. (status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", externalGameId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Ldvv;->S()V

    return-void

    :cond_0
    if-eqz v0, :cond_1

    const v1, 0x7f0b02a5    # com.google.android.gms.R.string.games_toast_dialog_app_unmuted

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Ldvv;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const-string v0, "GamesSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Display name of unmuted game with externalGameId: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ldek;)V
    .locals 4

    const/4 v3, 0x0

    invoke-interface {p1}, Ldek;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Ldek;->g()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Ldvv;->U()V

    iput-boolean v3, p0, Ldvv;->ah:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Ldvv;->ah:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    const-string v2, "pacl"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbpd;->a(Ljava/lang/String;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0}, Lbky;->a([B)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Ldvv;->ag:Ljava/util/ArrayList;

    iget-object v0, p0, Ldvv;->ac:Ldwu;

    invoke-virtual {v0}, Ldwu;->notifyDataSetInvalidated()V
    :try_end_1
    .catch Lizj; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "GamesSettings"

    const-string v2, "Unable to parse ACL data."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a_(I)V
    .locals 2

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "GamesSettings"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->f:Lctl;

    invoke-interface {v1, v0}, Lctl;->d(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Ldwd;

    invoke-direct {v1, p0}, Ldwd;-><init>(Ldvv;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Ldvp;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    iput-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvv;->Z:Ljava/lang/String;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "destAppVersion"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvv;->aa:Ljava/lang/String;

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->o()Z

    move-result v0

    iput-boolean v0, p0, Ldvv;->ae:Z

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Ldvv;->af:Landroid/os/Bundle;

    new-instance v1, Ldwg;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ldwl;

    invoke-direct {v2, p0, v5}, Ldwl;-><init>(Ldvv;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Ldwl;

    invoke-direct {v2, p0, v6}, Ldwl;-><init>(Ldvv;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Ldwm;

    invoke-direct {v2, p0}, Ldwm;-><init>(Ldvv;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Ldwi;

    invoke-direct {v2, p0}, Ldwi;-><init>(Ldvv;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Ldvv;->T()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ldwf;

    invoke-direct {v2, p0}, Ldwf;-><init>(Ldvv;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v2, Ldwh;

    const v3, 0x7f0b0207    # com.google.android.gms.R.string.games_gcore_muted_games

    invoke-direct {v2, p0, v3}, Ldwh;-><init>(Ldvv;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {v1, p0, v0}, Ldwg;-><init>(Ldvv;Ljava/util/ArrayList;)V

    new-instance v0, Ldwk;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, p0, v2}, Ldwk;-><init>(Ldvv;Landroid/content/Context;)V

    iput-object v0, p0, Ldvv;->ab:Ldwk;

    iget-object v0, p0, Ldvv;->ab:Ldwk;

    invoke-virtual {v0, p0}, Ldwk;->a(Ldvf;)V

    iget-object v0, p0, Ldvv;->ab:Ldwk;

    invoke-virtual {v0}, Ldwk;->f()V

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    const v2, 0x7f040063    # com.google.android.gms.R.layout.games_generic_empty_message

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0b0208    # com.google.android.gms.R.string.games_gcore_muted_games_empty

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Ldvv;->ab:Ldwk;

    invoke-virtual {v2, v0}, Ldwk;->a(Landroid/view/View;)V

    new-instance v0, Ldwg;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Ldvv;->aa:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ldwh;

    const v4, 0x7f0b0209    # com.google.android.gms.R.string.games_gcore_about

    invoke-direct {v3, p0, v4}, Ldwh;-><init>(Ldvv;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Ldwe;

    invoke-direct {v3, p0}, Ldwe;-><init>(Ldvv;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-direct {v0, p0, v2}, Ldwg;-><init>(Ldvv;Ljava/util/ArrayList;)V

    new-instance v2, Ldwu;

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/widget/BaseAdapter;

    aput-object v1, v3, v5

    iget-object v1, p0, Ldvv;->ab:Ldwk;

    aput-object v1, v3, v6

    const/4 v1, 0x2

    aput-object v0, v3, v1

    invoke-direct {v2, v3}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v2, p0, Ldvv;->ac:Ldwu;

    iget-object v0, p0, Ldvv;->ac:Ldwu;

    invoke-virtual {p0, v0}, Ldvv;->a(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Ldvv;->ab:Ldwk;

    invoke-virtual {v0}, Ldwk;->a()V

    invoke-super {p0}, Ldvp;->f()V

    return-void
.end method

.method public final g_()V
    .locals 3

    iget-object v0, p0, Ldvv;->i:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    iget-boolean v1, p0, Ldvv;->ae:Z

    iget-object v2, p0, Ldvv;->af:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(ZLandroid/os/Bundle;)V

    invoke-super {p0}, Ldvp;->g_()V

    return-void
.end method
