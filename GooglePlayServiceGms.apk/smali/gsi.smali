.class public final Lgsi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:Lbqa;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgsj;

    invoke-direct {v0}, Lgsj;-><init>()V

    sput-object v0, Lgsi;->d:Lbqa;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgsi;->a:Landroid/content/Context;

    const-string v0, "com.google.android.gms.wallet.cache.WalletPersistentCache"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgsi;->b:Landroid/content/SharedPreferences;

    const-string v0, "com.google.android.gms.wallet.cache.CacheUpdateLog"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgsi;->c:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lgsi;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "VERSION"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    iget-object v0, p0, Lgsi;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "VERSION"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Lizx;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lgsi;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lizx;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lizx;

    goto :goto_0
.end method

.method private a(Landroid/accounts/Account;ILizx;I)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    and-int/lit8 v0, p4, 0x2

    if-nez v0, :cond_3

    iget-object v0, p3, Lizx;->b:[Lipv;

    array-length v3, v0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    iget-object v4, p3, Lizx;->b:[Lipv;

    iget-object v0, p3, Lizx;->b:[Lipv;

    aget-object v0, v0, v2

    iget-boolean v5, v0, Lipv;->g:Z

    if-nez v5, :cond_2

    iget-object v5, v0, Lipv;->i:[I

    array-length v5, v5

    if-ne v5, v7, :cond_2

    iget-object v5, v0, Lipv;->i:[I

    aget v5, v5, v1

    if-ne v5, v8, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-boolean v7, v0, Lipv;->g:Z

    iget-object v5, v0, Lipv;->i:[I

    new-array v6, v7, [I

    aput v8, v6, v1

    invoke-static {v5, v6}, Lboz;->a([I[I)[I

    move-result-object v5

    iput-object v5, v0, Lipv;->i:[I

    :cond_2
    aput-object v0, v4, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    invoke-static {p1, p2}, Lgsi;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v3

    if-eqz p4, :cond_4

    invoke-direct {p0, v3}, Lgsi;->a(Ljava/lang/String;)Lizx;

    move-result-object v4

    if-eqz p3, :cond_4

    if-nez v4, :cond_5

    :cond_4
    :goto_2
    invoke-direct {p0, v3, p3}, Lgsi;->a(Ljava/lang/String;Lizx;)V

    if-nez p4, :cond_0

    invoke-virtual {p0, p1, p2}, Lgsi;->c(Landroid/accounts/Account;I)V

    goto :goto_0

    :cond_5
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_8

    iget-object v0, v4, Lizx;->a:[Lioj;

    array-length v0, v0

    if-lez v0, :cond_8

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iget-object v2, v4, Lizx;->a:[Lioj;

    array-length v6, v2

    move v0, v1

    :goto_3
    if-ge v0, v6, :cond_6

    aget-object v7, v2, v0

    iget-object v8, v7, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v0, p3, Lizx;->a:[Lioj;

    array-length v6, v0

    move v2, v1

    :goto_4
    if-ge v2, v6, :cond_8

    iget-object v0, p3, Lizx;->a:[Lioj;

    aget-object v1, v0, v2

    iget-object v0, v1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lioj;

    if-eqz v0, :cond_7

    iget-object v7, v1, Lioj;->e:Lipv;

    if-eqz v7, :cond_7

    iget-object v7, v0, Lioj;->e:Lipv;

    if-eqz v7, :cond_7

    iget-object v7, v1, Lioj;->e:Lipv;

    iget-object v8, v0, Lioj;->e:Lipv;

    invoke-static {v7, v8}, Lgsi;->a(Lipv;Lipv;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v7, p3, Lizx;->a:[Lioj;

    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v1

    check-cast v1, Lioj;

    aput-object v1, v7, v2

    iget-object v1, p3, Lizx;->a:[Lioj;

    aget-object v1, v1, v2

    iget-object v0, v0, Lioj;->e:Lipv;

    iput-object v0, v1, Lioj;->e:Lipv;

    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_8
    invoke-static {p3, v4, p4}, Lgsi;->a(Lizx;Lizx;I)V

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "com.google.android.gms.wallet.cache.WalletPersistentCache"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    const-string v0, "com.google.android.gms.wallet.cache.CacheUpdateLog"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private static a(Landroid/content/SharedPreferences;Ljava/util/HashSet;)V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :cond_2
    invoke-static {v2}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_3
    return-void
.end method

.method private static a(Lizx;Lizx;I)V
    .locals 7

    const/4 v0, 0x0

    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_1

    iget-object v0, p1, Lizx;->b:[Lipv;

    iget-object v1, p1, Lizx;->b:[Lipv;

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lipv;

    iput-object v0, p0, Lizx;->b:[Lipv;

    :cond_0
    return-void

    :cond_1
    iget-object v1, p1, Lizx;->b:[Lipv;

    array-length v1, v1

    if-eqz v1, :cond_0

    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v3, p1, Lizx;->b:[Lipv;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    iget-object v6, v5, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v2, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lizx;->b:[Lipv;

    array-length v3, v1

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lizx;->b:[Lipv;

    aget-object v4, v0, v1

    iget-object v0, v4, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipv;

    if-eqz v0, :cond_3

    invoke-static {v4, v0}, Lgsi;->a(Lipv;Lipv;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-boolean v4, v4, Lipv;->h:Z

    iput-boolean v4, v0, Lipv;->h:Z

    iget-object v4, p0, Lizx;->b:[Lipv;

    aput-object v0, v4, v1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lizx;)V
    .locals 2

    invoke-static {p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->c(Lizs;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgsi;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;I[Landroid/accounts/Account;J)V
    .locals 5

    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p3, v0

    invoke-virtual {p0, v2, p2}, Lgsi;->b(Landroid/accounts/Account;I)J

    move-result-wide v3

    cmp-long v3, v3, p4

    if-gez v3, :cond_0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a()Z
    .locals 1

    sget-object v0, Lgzp;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private static a(Lipv;Lipv;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lipv;->g:Z

    iget-boolean v2, p1, Lipv;->g:Z

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lipv;->d:Ljava/lang/String;

    iget-object v2, p1, Lipv;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lipv;->a:Lixo;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lipv;->a:Lixo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lipv;->a:Lixo;

    iget-object v2, p1, Lipv;->a:Lixo;

    invoke-static {v1, v2}, Lgty;->a(Lixo;Lixo;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-boolean v1, p0, Lipv;->e:Z

    iget-boolean v2, p1, Lipv;->e:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Landroid/accounts/Account;ILioj;Ljava/lang/String;)V
    .locals 7

    const/4 v0, 0x0

    invoke-static {p1, p2}, Lgsi;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lgsi;->a(Ljava/lang/String;)Lizx;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, v3, Lizx;->a:[Lioj;

    array-length v4, v1

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    iget-object v5, v3, Lizx;->a:[Lioj;

    aget-object v5, v5, v1

    iget-object v6, v5, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-boolean v0, v5, Lioj;->f:Z

    iput-boolean v0, p3, Lioj;->f:Z

    iget-object v0, v3, Lizx;->a:[Lioj;

    aput-object p3, v0, v1

    const/4 v0, 0x1

    :cond_1
    if-nez v0, :cond_2

    iget-object v0, v3, Lizx;->a:[Lioj;

    invoke-static {v0, p3}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lioj;

    iput-object v0, v3, Lizx;->a:[Lioj;

    :cond_2
    invoke-direct {p0, v2, v3}, Lgsi;->a(Ljava/lang/String;Lizx;)V

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private b(Landroid/accounts/Account;ILipv;Ljava/lang/String;)V
    .locals 7

    const/4 v0, 0x0

    invoke-static {}, Lgsi;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1, p2}, Lgsi;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lgsi;->a(Ljava/lang/String;)Lizx;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v1, v3, Lizx;->b:[Lipv;

    array-length v4, v1

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    iget-object v5, v3, Lizx;->b:[Lipv;

    aget-object v5, v5, v1

    iget-object v6, v5, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-boolean v0, v5, Lipv;->h:Z

    iput-boolean v0, p3, Lipv;->h:Z

    iget-object v0, v3, Lizx;->b:[Lipv;

    aput-object p3, v0, v1

    const/4 v0, 0x1

    :cond_2
    if-nez v0, :cond_3

    iget-object v0, v3, Lizx;->b:[Lipv;

    invoke-static {v0, p3}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lipv;

    iput-object v0, v3, Lizx;->b:[Lipv;

    :cond_3
    invoke-direct {p0, v2, v3}, Lgsi;->a(Ljava/lang/String;Lizx;)V

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static d(Landroid/accounts/Account;I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;I)Lizx;
    .locals 1

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lgsi;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgsi;->a(Ljava/lang/String;)Lizx;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILioj;)V
    .locals 1

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p3, Lioj;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lgsi;->b(Landroid/accounts/Account;ILioj;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILioj;Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lgsi;->b(Landroid/accounts/Account;ILioj;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILipi;)V
    .locals 4

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lizx;

    invoke-direct {v1}, Lizx;-><init>()V

    iget-object v0, p3, Lipi;->b:[Lioj;

    iget-object v2, p3, Lipi;->b:[Lioj;

    sget-object v3, Lgsi;->d:Lbqa;

    invoke-static {v2, v3}, Lboz;->a([Ljava/lang/Object;Lbqa;)I

    move-result v2

    invoke-static {v0, v2}, Lboz;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lioj;

    iput-object v0, v1, Lizx;->a:[Lioj;

    iget-object v0, p3, Lipi;->c:[Lipv;

    iget-object v2, p3, Lipi;->c:[Lipv;

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lipv;

    iput-object v0, v1, Lizx;->b:[Lipv;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v1, v0}, Lgsi;->a(Landroid/accounts/Account;ILizx;I)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILipv;)V
    .locals 1

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p3, Lipv;->b:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lgsi;->b(Landroid/accounts/Account;ILipv;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILipv;Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lgsi;->b(Landroid/accounts/Account;ILipv;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILizx;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lgsi;->a(Landroid/accounts/Account;ILizx;I)V

    return-void
.end method

.method public final a(Landroid/accounts/Account;ILizz;I)V
    .locals 3

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lizx;

    invoke-direct {v1}, Lizx;-><init>()V

    iget-object v0, p3, Lizz;->b:[Lioj;

    iget-object v2, p3, Lizz;->b:[Lioj;

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lioj;

    iput-object v0, v1, Lizx;->a:[Lioj;

    iget-object v0, p3, Lizz;->c:[Lipv;

    iget-object v2, p3, Lizz;->c:[Lipv;

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lipv;

    iput-object v0, v1, Lizx;->b:[Lipv;

    invoke-direct {p0, p1, p2, v1, p4}, Lgsi;->a(Landroid/accounts/Account;ILizx;I)V

    goto :goto_0
.end method

.method public final a([Landroid/accounts/Account;)V
    .locals 6

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    array-length v3, p1

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, p1, v0

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lgsi;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v4, v1}, Lgsi;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lgsi;->b:Landroid/content/SharedPreferences;

    invoke-static {v0, v2}, Lgsi;->a(Landroid/content/SharedPreferences;Ljava/util/HashSet;)V

    iget-object v0, p0, Lgsi;->c:Landroid/content/SharedPreferences;

    invoke-static {v0, v2}, Lgsi;->a(Landroid/content/SharedPreferences;Ljava/util/HashSet;)V

    goto :goto_0
.end method

.method public final b(Landroid/accounts/Account;I)J
    .locals 4

    iget-object v0, p0, Lgsi;->c:Landroid/content/SharedPreferences;

    invoke-static {p1, p2}, Lgsi;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b([Landroid/accounts/Account;)Ljava/util/ArrayList;
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x240c8400

    sub-long v4, v2, v4

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lgsi;->a(Ljava/util/ArrayList;I[Landroid/accounts/Account;J)V

    iget-object v0, p0, Lgsi;->a:Landroid/content/Context;

    invoke-static {v0}, Lbox;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lgsi;->a(Ljava/util/ArrayList;I[Landroid/accounts/Account;J)V

    goto :goto_0
.end method

.method public final c(Landroid/accounts/Account;I)V
    .locals 4

    invoke-static {}, Lgsi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgsi;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1, p2}, Lgsi;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method
