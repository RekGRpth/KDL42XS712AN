.class public final Lhfk;
.super Lhak;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lhfo;

.field private final d:Lhce;

.field private final e:Lhga;

.field private final f:Lgsg;

.field private final g:Lhef;

.field private final h:Lgsi;

.field private final i:Lher;

.field private final j:Lhfb;

.field private final k:Lhep;

.field private final l:Lgse;

.field private final m:Lgtm;

.field private final n:Lhcg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lgzp;->l:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lhfk;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lhfo;Lhga;Lgsg;Lhef;Lgsi;Lher;Lhfb;Lhep;Lgse;Lgtm;Lhcg;)V
    .locals 2

    invoke-direct {p0}, Lhak;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhfk;->b:Landroid/content/Context;

    iput-object p2, p0, Lhfk;->c:Lhfo;

    iput-object p3, p0, Lhfk;->e:Lhga;

    new-instance v0, Lhce;

    const-string v1, "NetworkOwService"

    invoke-direct {v0, p1, v1}, Lhce;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lhfk;->d:Lhce;

    iput-object p4, p0, Lhfk;->f:Lgsg;

    iput-object p5, p0, Lhfk;->g:Lhef;

    iput-object p6, p0, Lhfk;->h:Lgsi;

    iput-object p7, p0, Lhfk;->i:Lher;

    iput-object p8, p0, Lhfk;->j:Lhfb;

    iput-object p9, p0, Lhfk;->k:Lhep;

    iput-object p10, p0, Lhfk;->l:Lgse;

    iput-object p11, p0, Lhfk;->m:Lgtm;

    iput-object p12, p0, Lhfk;->n:Lhcg;

    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljau;Lgsm;Lhgb;)Lcom/google/android/gms/wallet/FullWallet;
    .locals 10

    if-nez p1, :cond_0

    const-string v0, "NetworkOwService"

    const-string v1, "fullWalletResponse=null, probably due to exception in FetchFullWalletTask"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lhfn;

    const/16 v1, 0x19d

    invoke-direct {v0, v1}, Lhfn;-><init>(I)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    const-string v1, "NetworkOwService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected ServerResponse type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lhfn;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lhfn;-><init>(I)V

    throw v0

    :sswitch_0
    iget-object v0, p0, Lhfk;->b:Landroid/content/Context;

    iget-object v1, p3, Ljau;->f:Ljava/lang/String;

    iget-object v2, p3, Ljau;->g:Ljava/lang/String;

    iget-object v3, p5, Lhgb;->e:Ljbg;

    iget-object v4, p5, Lhgb;->b:Ljava/lang/String;

    iget-object v5, p5, Lhgb;->c:Ljava/lang/String;

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lhfx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljbg;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Lhfn;

    invoke-direct {v1, v0}, Lhfn;-><init>(Landroid/app/PendingIntent;)V

    throw v1

    :sswitch_1
    new-instance v0, Lhfn;

    const/16 v1, 0x19b

    invoke-direct {v0, v1}, Lhfn;-><init>(I)V

    throw v0

    :sswitch_2
    const-string v0, "NetworkOwService"

    const-string v1, "NetworkError in getFullWallet"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lhfn;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Lhfn;-><init>(I)V

    throw v0

    :sswitch_3
    new-instance v1, Lhfn;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lipy;

    invoke-static {v0}, Lhfx;->a(Lipy;)I

    move-result v0

    invoke-direct {v1, v0}, Lhfn;-><init>(I)V

    throw v1

    :sswitch_4
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljav;

    iget-object v0, v7, Ljav;->i:[I

    array-length v0, v0

    const/4 v8, 0x0

    const/4 v1, 0x0

    add-int/lit8 v0, v0, -0x1

    move v9, v0

    move-object v0, v1

    :goto_0
    if-ltz v9, :cond_5

    iget-object v1, v7, Ljav;->i:[I

    aget v1, v1, v9

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Lhfn;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lhfn;-><init>(I)V

    throw v0

    :pswitch_1
    const/4 v1, 0x1

    :goto_1
    add-int/lit8 v2, v9, -0x1

    move v9, v2

    move v8, v1

    goto :goto_0

    :pswitch_2
    if-eqz p5, :cond_1

    iget-object v0, p5, Lhgb;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Lhfn;

    const/16 v1, 0x19a

    invoke-direct {v0, v1}, Lhfn;-><init>(I)V

    throw v0

    :cond_2
    iget-object v1, p5, Lhgb;->b:Ljava/lang/String;

    iget-object v0, p0, Lhfk;->h:Lgsi;

    iget-object v2, p5, Lhgb;->d:Landroid/accounts/Account;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lgsi;->a(Landroid/accounts/Account;I)Lizx;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_3

    iget-object v0, v2, Lizx;->a:[Lioj;

    invoke-static {v0, v1}, Lgth;->a([Lioj;Ljava/lang/String;)Lioj;

    move-result-object v0

    :cond_3
    if-nez v0, :cond_4

    new-instance v0, Lhfn;

    const/16 v1, 0x19a

    invoke-direct {v0, v1}, Lhfn;-><init>(I)V

    throw v0

    :cond_4
    iget-object v1, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v1, v0, p3, p2}, Lhfx;->a(Landroid/content/Context;Lioj;Ljau;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;

    move-result-object v0

    move v1, v8

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lhfk;->b:Landroid/content/Context;

    iget-object v1, p3, Ljau;->f:Ljava/lang/String;

    iget-object v2, p3, Ljau;->g:Ljava/lang/String;

    iget-object v3, p5, Lhgb;->e:Ljbg;

    iget-object v4, p5, Lhgb;->b:Ljava/lang/String;

    iget-object v5, p5, Lhgb;->c:Ljava/lang/String;

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lhfx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljbg;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;

    move-result-object v0

    move v1, v8

    goto :goto_1

    :cond_5
    if-eqz v8, :cond_6

    new-instance v0, Lhfn;

    const/16 v1, 0x199

    invoke-direct {v0, v1}, Lhfn;-><init>(I)V

    throw v0

    :cond_6
    if-eqz v0, :cond_7

    new-instance v1, Lhfn;

    invoke-direct {v1, v0}, Lhfn;-><init>(Landroid/app/PendingIntent;)V

    throw v1

    :cond_7
    const-string v0, "onlinewallet"

    const-string v1, "full_wallet"

    invoke-static {p4, v0, v1}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p5, Lhgb;->d:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->a()J

    move-result-wide v1

    invoke-static {v7, p3, v0, v1, v2}, Lhfx;->a(Ljav;Ljau;Ljava/lang/String;J)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v0

    iget-object v1, p0, Lhfk;->e:Lhga;

    iget-object v2, p3, Ljau;->f:Ljava/lang/String;

    iget-object v1, v1, Lhga;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x10 -> :sswitch_4
        0x11 -> :sswitch_3
        0x16 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lgrl;

    move-result-object v0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p0}, Lgrl;->a(Ljava/lang/String;)Lgrl;

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p1}, Lgrl;->b(Ljava/lang/String;)Lgrl;

    :cond_1
    iget-object v0, v0, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    return-object v0
.end method

.method static synthetic a(Lhfk;)Lhfo;
    .locals 1

    iget-object v0, p0, Lhfk;->c:Lhfo;

    return-object v0
.end method

.method private a(ILjava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {}, Lhfk;->getCallingUid()I

    move-result v2

    invoke-static {v1, p1, p2, v2, p3}, Lhfx;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/accounts/Account;)Ljava/lang/String;
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method static synthetic a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lgsn;->b(Landroid/os/Bundle;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v0, "oauth2:https://www.googleapis.com/auth/sierra"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "oauth2:https://www.googleapis.com/auth/sierrasandbox"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_0
        0x15 -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic a(Lhfk;ILjava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lhfk;->a(ILjava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    const/4 v0, 0x1

    invoke-static {p4}, Lgsn;->b(Landroid/os/Bundle;)I

    move-result v1

    if-ne v0, v1, :cond_2

    sget-object v0, Lgzp;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    new-instance v0, Litf;

    invoke-direct {v0}, Litf;-><init>()V

    invoke-static {}, Lbcm;->a()Lbdh;

    move-result-object v3

    iput-object v3, v0, Litf;->a:Lbdh;

    iput-object p3, v0, Litf;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Litf;->f:Ljava/lang/Integer;

    iput-object p2, v0, Litf;->g:Ljava/lang/String;

    iget-object v3, p0, Lhfk;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    const/16 v4, 0x80

    :try_start_1
    invoke-virtual {v3, p3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    iget v4, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v0, Litf;->d:Ljava/lang/Integer;

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v4, v0, Litf;->e:Ljava/lang/String;

    :cond_0
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v4, :cond_1

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v4, :cond_1

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "com.google.android.gms.version"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Litf;->b:Ljava/lang/Integer;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    new-instance v3, Lfko;

    iget-object v4, p0, Lhfk;->b:Landroid/content/Context;

    const/16 v5, 0x17

    invoke-direct {v3, v4, v5}, Lfko;-><init>(Landroid/content/Context;I)V

    const-string v4, "merchantError"

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v3, v4, v0, v5}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v3}, Lfko;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :cond_2
    return-void

    :catch_0
    move-exception v3

    :try_start_3
    const-string v3, "NetworkOwService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to retrieve package info to log merchant error for: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method private static a(Landroid/os/Bundle;Lhgb;)V
    .locals 2

    iget-object v0, p1, Lhgb;->d:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    iget-object v1, p1, Lhgb;->d:Landroid/accounts/Account;

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    const-string v0, "com.google.android.gms.wallet.EXTRA_ALLOW_ACCOUNT_SELECTION"

    iget-boolean v1, p1, Lhgb;->f:Z

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method private static a(Lhal;Lcom/google/android/gms/wallet/FullWalletRequest;I)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lgrc;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgrc;->a(Ljava/lang/String;)Lgrc;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgrc;->b(Ljava/lang/String;)Lgrc;

    :cond_1
    iget-object v0, v0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p0, p2, v0, v1}, Lhal;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Lhal;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhfk;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v1

    iget-object v2, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {p3}, Lhfx;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v3

    invoke-static {v2, p2, v3}, Lhfx;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lhal;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    return-void
.end method

.method private static a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    invoke-static {p1, p2}, Lhfk;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p0, p3, v0, v1}, Lhal;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Lhal;Ljbg;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 3

    const/4 v0, 0x6

    const/4 v1, 0x0

    iget-object v2, p2, Ljbg;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lhfk;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v1

    iget-object v2, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v2, p2, p3}, Lhfx;->a(Landroid/content/Context;Ljbg;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lhal;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    return-void
.end method

.method private static a(Lcom/google/android/gms/wallet/Cart;ZLjava/lang/StringBuilder;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_2

    if-nez p1, :cond_1

    const-string v1, "Cart is a required field"

    invoke-static {v1, p2}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Cart.totalPrice"

    invoke-static {v2, v3, p2}, Lhfk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->c()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Cart.currencyCode"

    invoke-static {v3, v4, p2}, Lhfk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z
    .locals 2

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lhfk;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is invalid. The input was \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", but should be a string in the regex format \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lhfk;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z
    .locals 1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "googleTransactionId is a required field."

    invoke-static {v0, p1}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ljaz;Ljay;Landroid/accounts/Account;Lgsm;Lhal;Landroid/os/Bundle;Lcom/google/android/gms/wallet/Cart;ILjava/lang/String;)Z
    .locals 18

    move-object/from16 v0, p2

    iget-object v13, v0, Ljay;->a:Ljbg;

    invoke-static/range {p6 .. p6}, Lhfx;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v2, v0, Ljaz;->c:Ljbb;

    if-eqz v2, :cond_1

    invoke-static/range {p6 .. p6}, Lgsn;->b(Landroid/os/Bundle;)I

    move-result v4

    move-object/from16 v0, p1

    iget-object v2, v0, Ljaz;->c:Ljbb;

    invoke-static {v2}, Lhfx;->a(Ljbb;)Lizz;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lhfk;->h:Lgsi;

    iget-boolean v6, v13, Ljbg;->g:Z

    iget-boolean v7, v13, Ljbg;->h:Z

    iget-boolean v8, v13, Ljbg;->i:Z

    move-object/from16 v3, p3

    invoke-static/range {v2 .. v8}, Lhfx;->a(Lgsi;Landroid/accounts/Account;ILizz;ZZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhfk;->l:Lgse;

    move-object/from16 v0, p9

    invoke-static {v2, v0, v4, v5}, Lhfx;->a(Lgse;Ljava/lang/String;ILizz;)V

    iget-boolean v2, v13, Ljbg;->k:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lhfk;->f:Lgsg;

    move-object/from16 v0, p3

    move-object/from16 v1, p9

    invoke-static {v2, v0, v1, v4, v5}, Lhfx;->a(Lgsg;Landroid/accounts/Account;Ljava/lang/String;ILizz;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lhfk;->n:Lhcg;

    invoke-virtual {v12}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v4, v0, Ljay;->a:Ljbg;

    invoke-virtual {v2, v3, v4, v5}, Lhcg;->b(Ljava/lang/String;Lizs;Lizs;)V

    :cond_1
    iget-object v14, v13, Ljbg;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Ljaz;->a:[I

    array-length v2, v2

    if-lez v2, :cond_4

    move-object/from16 v0, p1

    iget-object v3, v0, Ljaz;->a:[I

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_3

    aget v5, v3, v2

    packed-switch v5, :pswitch_data_0

    :cond_2
    :pswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x0

    const/16 v3, 0x199

    move-object/from16 v0, p5

    invoke-static {v0, v2, v14, v3}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x0

    :goto_1
    return v2

    :pswitch_2
    if-lez p8, :cond_2

    move-object/from16 v0, p2

    iget-object v5, v0, Ljay;->e:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lhfk;->b:Landroid/content/Context;

    move-object/from16 v0, p2

    iget-object v3, v0, Ljay;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p2

    iget-object v2, v0, Ljay;->a:Ljbg;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1, v2, v12}, Lhfk;->a(Lhal;Ljbg;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    move-object/from16 v0, p1

    iget-object v15, v0, Ljaz;->b:Ljbh;

    iget-object v0, v15, Ljbh;->a:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v2, v15, Ljbh;->e:Ljbi;

    if-eqz v2, :cond_6

    iget-object v2, v15, Ljbh;->e:Ljbi;

    iget-object v3, v2, Ljbi;->j:Ljava/lang/String;

    :goto_2
    iget-object v2, v15, Ljbh;->d:Lipv;

    if-eqz v2, :cond_7

    iget-object v2, v15, Ljbh;->d:Lipv;

    iget-object v4, v2, Lipv;->b:Ljava/lang/String;

    :goto_3
    new-instance v2, Lhgb;

    const-string v5, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/accounts/Account;

    move-object/from16 v0, p2

    iget-object v6, v0, Ljay;->a:Ljbg;

    const-string v7, "com.google.android.gms.wallet.EXTRA_ALLOW_ACCOUNT_SELECTION"

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-direct/range {v2 .. v11}, Lhgb;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljbg;ZZ[Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhfk;->e:Lhga;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0, v2}, Lhga;->a(Ljava/lang/String;Lhgb;)V

    move-object/from16 v0, v16

    iput-object v0, v13, Ljbg;->b:Ljava/lang/String;

    invoke-static {}, Ljaj;->c()[Ljaj;

    move-result-object v3

    iput-object v3, v15, Ljbh;->g:[Ljaj;

    iget-object v3, v15, Ljbh;->f:[Ljai;

    array-length v6, v3

    if-lez v6, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lhfk;->f:Lgsg;

    invoke-static/range {p6 .. p6}, Lgsn;->b(Landroid/os/Bundle;)I

    move-result v4

    move-object/from16 v0, p3

    move-object/from16 v1, p9

    invoke-virtual {v3, v4, v0, v1}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lgsh;

    move-result-object v7

    iget-object v3, v7, Lgsh;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    iget-object v3, v7, Lgsh;->b:Ljava/lang/String;

    const-string v4, "dont_send_loyalty_wob_id"

    invoke-static {v3, v4}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-static {}, Ljai;->c()[Ljai;

    move-result-object v3

    iput-object v3, v15, Ljbh;->f:[Ljai;

    :cond_5
    :goto_4
    const-string v3, "onlinewallet"

    const-string v4, "preauthorized_masked_wallet"

    move-object/from16 v0, p4

    invoke-static {v0, v3, v4}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p3

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v15, v14, v3}, Lhfx;->a(Ljbh;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v5, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    move-object/from16 v0, p5

    invoke-interface {v0, v4, v3, v5}, Lhal;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhfk;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhfk;->j:Lhfb;

    iget-object v8, v2, Lhgb;->b:Ljava/lang/String;

    iget-object v9, v2, Lhgb;->c:Ljava/lang/String;

    iget-boolean v10, v2, Lhgb;->i:Z

    iget-boolean v11, v2, Lhgb;->j:Z

    move-object/from16 v5, p3

    move-object v6, v13

    move-object/from16 v7, p7

    invoke-static/range {v3 .. v12}, Lheo;->a(Landroid/content/Context;Lhfb;Landroid/accounts/Account;Ljbg;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lheo;

    move-result-object v3

    new-instance v4, Lheq;

    iget-object v5, v2, Lhgb;->b:Ljava/lang/String;

    iget-object v2, v2, Lhgb;->c:Ljava/lang/String;

    invoke-direct {v4, v13, v5, v2}, Lheq;-><init>(Ljbg;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhfk;->k:Lhep;

    invoke-virtual {v2, v4, v3}, Lhep;->a(Lheq;Lheo;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v3, v2}, Lbox;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v4, 0x0

    const/4 v3, 0x0

    move/from16 v17, v3

    move-object v3, v4

    move/from16 v4, v17

    :goto_5
    if-ge v4, v6, :cond_9

    iget-object v3, v15, Ljbh;->f:[Ljai;

    aget-object v3, v3, v4

    iget-object v5, v3, Ljai;->a:Ljava/lang/String;

    iget-object v8, v7, Lgsh;->b:Ljava/lang/String;

    invoke-static {v5, v8}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    const/4 v5, 0x0

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v5

    goto :goto_5

    :cond_9
    if-eqz v3, :cond_a

    const/4 v4, 0x1

    new-array v4, v4, [Ljai;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    iput-object v4, v15, Ljbh;->f:[Ljai;

    goto :goto_4

    :cond_a
    invoke-static {}, Ljai;->c()[Ljai;

    move-result-object v3

    iput-object v3, v15, Ljbh;->f:[Ljai;

    goto :goto_4

    :cond_b
    move-object/from16 v0, p2

    iget-object v2, v0, Ljay;->a:Ljbg;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1, v2, v12}, Lhfk;->a(Lhal;Ljbg;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    const/4 v2, 0x0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lhfk;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lhfk;->b:Landroid/content/Context;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    const-string v0, "androidPackageName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "com.google.android.gms.wallet.EXTRA_ENVIRONMENT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    if-nez v0, :cond_3

    const-string v0, "com.google.android.gms.wallet.EXTRA_ALLOW_ACCOUNT_SELECTION"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v0, v4}, Lbov;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, v2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_1
    iget-object v1, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    if-eqz v0, :cond_1

    array-length v6, v2

    move v1, v5

    :goto_2
    if-ge v1, v6, :cond_1

    aget-object v7, v2, v1

    invoke-virtual {v0, v7}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    aget-object v6, v2, v5

    aput-object v0, v2, v5

    aput-object v6, v2, v1

    :cond_1
    :goto_3
    iget-object v6, p0, Lhfk;->b:Landroid/content/Context;

    new-instance v0, Lhfm;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhfm;-><init>(Lhfk;[Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "check_preauth"

    invoke-static {v6, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    new-array v2, v2, [Landroid/accounts/Account;

    aput-object v0, v2, v5

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 1

    const-string v0, "WalletClient"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z
    .locals 2

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is a required field."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lhfk;)Lgsg;
    .locals 1

    iget-object v0, p0, Lhfk;->f:Lgsg;

    return-object v0
.end method

.method private c(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 3

    const-string v0, "parameters is required"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "androidPackageName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "packageName is required"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Lhal;)V
    .locals 4

    const/4 v1, 0x0

    const-string v0, "callbacks is required"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lhfk;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lgsm;

    iget-object v3, p0, Lhfk;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lgsm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v0, "onlinewallet"

    const-string v3, "check_for_pre_auth"

    invoke-static {v2, v0, v3}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x199

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p2, v0, v1, v2}, Lhal;->a(IZLandroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lhfk;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p2, v1, v0, v2}, Lhal;->a(IZLandroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;Lhal;)V
    .locals 6

    const/16 v2, 0x194

    const/4 v1, 0x0

    const-string v0, "callbacks is required"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2}, Lhfk;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "CreateWalletObjects "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    if-nez p1, :cond_1

    const-string v0, "CreateWalletObjectsRequest was null."

    invoke-static {v0, v4}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    move v0, v1

    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    :goto_1
    if-eqz v1, :cond_5

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v1, v0}, Lhal;->a(ILandroid/os/Bundle;)V

    :goto_2
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v5

    if-nez v5, :cond_2

    const-string v0, "LoyaltyWalletObject was null."

    invoke-static {v0, v4}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v0, "issuerName is not defined for LoyaltyWalletObject."

    invoke-static {v0, v4}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    move v0, v1

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v0, "programName is not defined for LoyaltyWalletObject."

    invoke-static {v0, v4}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0, v3, p2}, Lhfk;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move v1, v2

    goto :goto_1

    :cond_5
    const/4 v0, 0x6

    iget-object v1, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lhfx;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {p3, v0, v1}, Lhal;->a(ILandroid/os/Bundle;)V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/wallet/FullWalletRequest;Landroid/os/Bundle;Lhal;)V
    .locals 10

    const/16 v9, 0x19a

    const/16 v2, 0x194

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v3, "callbacks is required"

    invoke-static {p3, v3}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2}, Lhfk;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    new-instance v4, Lgsm;

    iget-object v3, p0, Lhfk;->b:Landroid/content/Context;

    invoke-direct {v4, v3, v6}, Lgsm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v3, "onlinewallet"

    const-string v5, "load_full_wallet"

    invoke-static {v4, v3, v5}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v3, "FullWallet"

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const-string v0, "FullWalletRequest was null."

    invoke-static {v0, v7}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    :cond_0
    :goto_0
    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    :goto_2
    if-eqz v1, :cond_5

    invoke-static {p3, p1, v1}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    :goto_3
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7}, Lhfk;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v0

    :goto_4
    if-eqz v3, :cond_c

    iget-object v5, p0, Lhfk;->e:Lhga;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lhga;->a(Ljava/lang/String;)Lhgb;

    move-result-object v5

    if-eqz v5, :cond_c

    iget-object v8, v5, Lhgb;->e:Ljbg;

    if-eqz v8, :cond_c

    iget-object v5, v5, Lhgb;->e:Ljbg;

    iget-boolean v5, v5, Ljbg;->j:Z

    :goto_5
    if-eqz v5, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->d()Lcom/google/android/gms/wallet/Cart;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v0, "Cart should not be set for billing agreement requests."

    invoke-static {v0, v7}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_4

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->d()Lcom/google/android/gms/wallet/Cart;

    move-result-object v5

    invoke-static {v5, v1, v7}, Lhfk;->a(Lcom/google/android/gms/wallet/Cart;ZLjava/lang/StringBuilder;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0, v6, p2}, Lhfk;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move v1, v2

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x199

    invoke-static {p3, p1, v0}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhfk;->e:Lhga;

    invoke-virtual {v1, v0}, Lhga;->a(Ljava/lang/String;)Lhgb;

    move-result-object v5

    if-eqz v5, :cond_7

    iget-object v1, v5, Lhgb;->b:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, v5, Lhgb;->d:Landroid/accounts/Account;

    if-nez v1, :cond_8

    :cond_7
    const-string v1, "NetworkOwService"

    const-string v2, "Full wallet requested without buyer account or a selected instrument/address/account"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetFullWallet\ninvalid google transaction id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v9, v0, v6, p2}, Lhfk;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p3, p1, v9}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto/16 :goto_3

    :cond_8
    invoke-static {p2, v5}, Lhfk;->a(Landroid/os/Bundle;Lhgb;)V

    iget-object v0, p0, Lhfk;->g:Lhef;

    invoke-virtual {v0, v6}, Lhef;->a(Ljava/lang/String;)Ljan;

    move-result-object v0

    if-nez v0, :cond_9

    const/16 v0, 0x19d

    invoke-static {p3, p1, v0}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto/16 :goto_3

    :cond_9
    invoke-static {p1, v5}, Lhfx;->a(Lcom/google/android/gms/wallet/FullWalletRequest;Lhgb;)Ljau;

    move-result-object v3

    iput-object v0, v3, Ljau;->a:Ljan;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    iget-object v1, v5, Lhgb;->d:Landroid/accounts/Account;

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;-><init>(Landroid/accounts/Account;Ljau;Ljava/lang/String;)V

    iget-object v1, p0, Lhfk;->i:Lher;

    invoke-virtual {v1, v0, p2, v4}, Lher;->a(Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Landroid/os/Bundle;Lgsm;)Landroid/util/Pair;

    move-result-object v0

    :try_start_0
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lhfk;->a(Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljau;Lgsm;Lhgb;)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v1, v0, v2}, Lhal;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lhfn; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    :catch_0
    move-exception v0

    iget v1, v0, Lhfn;->a:I

    packed-switch v1, :pswitch_data_0

    iget v0, v0, Lhfn;->a:I

    invoke-static {p3, p1, v0}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto/16 :goto_3

    :pswitch_0
    iget-object v1, v0, Lhfn;->b:Landroid/app/PendingIntent;

    if-nez v1, :cond_a

    const/16 v0, 0x8

    invoke-static {p3, p1, v0}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto/16 :goto_3

    :cond_a
    iget-object v0, v0, Lhfn;->b:Landroid/app/PendingIntent;

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lgrc;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgrc;->a(Ljava/lang/String;)Lgrc;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgrc;->b(Ljava/lang/String;)Lgrc;

    move-result-object v1

    iget-object v1, v1, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    const/4 v2, 0x6

    invoke-static {v0}, Lhfx;->a(Landroid/app/PendingIntent;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {p3, v2, v1, v0}, Lhal;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_b
    move v0, v3

    goto/16 :goto_1

    :cond_c
    move v5, v1

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lhal;)V
    .locals 10

    const/4 v8, 0x1

    move-object v5, p3

    move-object v6, p2

    move-object v0, p0

    :goto_0
    const-string v1, "callbacks is required"

    invoke-static {v5, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {v0, v6}, Lhfk;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v9

    new-instance v4, Lgsm;

    iget-object v1, v0, Lhfk;->b:Landroid/content/Context;

    invoke-direct {v4, v1, v9}, Lgsm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v1, "onlinewallet"

    const-string v2, "load_masked_wallet"

    invoke-static {v4, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "MaskedWallet"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_3

    const-string v1, "MaskedWalletRequest was null."

    invoke-static {v1, v2}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    :cond_0
    :goto_1
    const/4 v1, 0x0

    :cond_1
    :goto_2
    if-eqz v1, :cond_8

    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_a

    if-nez p1, :cond_9

    const/4 v0, 0x0

    :goto_4
    const/4 v2, 0x0

    invoke-static {v5, v2, v0, v1}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_2
    :goto_5
    return-void

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->h()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MaskedWalletRequest.currencyCode"

    invoke-static {v1, v3, v2}, Lhfk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_6
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->l()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v3

    if-eqz v3, :cond_4

    const-string v1, "Cart should not be set for billing agreement requests."

    invoke-static {v1, v2}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const/4 v1, 0x0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v1, "Estimated total price should not be set for billing agreement requests."

    invoke-static {v1, v2}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_6

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g()Ljava/lang/String;

    move-result-object v3

    const-string v7, "MaskedWalletRequest.estimatedTotalPrice"

    invoke-static {v3, v7, v2}, Lhfk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_7
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v3

    const/4 v7, 0x1

    invoke-static {v3, v7, v2}, Lhfk;->a(Lcom/google/android/gms/wallet/Cart;ZLjava/lang/StringBuilder;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_7

    :cond_8
    const/16 v1, 0x194

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v9, v6}, Lhfk;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const/16 v1, 0x194

    goto :goto_3

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_a
    invoke-static {p1}, Lgtl;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object p1

    iget-object v1, v0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v1}, Lbov;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x199

    invoke-static {v5, v0, v1, v2}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_5

    :cond_b
    sget-object v1, Lgzp;->j:Lbfy;

    invoke-virtual {v1}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-direct {v0, v5, p1, v6}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V

    goto/16 :goto_5

    :cond_c
    iget-object v1, v0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v1}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v5, v0, v1, v2}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_5

    :cond_d
    invoke-direct {v0, v6}, Lhfk;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->l()Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_e
    invoke-direct {v0, v5, p1, v6}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V

    goto/16 :goto_5

    :cond_f
    iget-object v2, v0, Lhfk;->g:Lhef;

    invoke-virtual {v2, v9}, Lhef;->a(Ljava/lang/String;)Ljan;

    move-result-object v2

    if-nez v2, :cond_10

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x19d

    invoke-static {v5, v0, v1, v2}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_5

    :cond_10
    const-string v3, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/accounts/Account;

    const/4 v7, 0x0

    invoke-static {p1, v7, v2}, Lhfx;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Ljan;)Ljbg;

    move-result-object v7

    new-instance v2, Ljay;

    invoke-direct {v2}, Ljay;-><init>()V

    iput-object v7, v2, Ljay;->a:Ljbg;

    invoke-static {v6}, Lgsn;->b(Landroid/os/Bundle;)I

    move-result v7

    invoke-static {v7}, Lhfx;->a(I)Z

    move-result v7

    iput-boolean v7, v2, Ljay;->d:Z

    iput-object v1, v2, Ljay;->e:Ljava/lang/String;

    invoke-static {}, Lhfx;->a()Ljbf;

    move-result-object v1

    iput-object v1, v2, Ljay;->f:Ljbf;

    iget-object v1, v2, Ljay;->c:[I

    const/4 v7, 0x2

    invoke-static {v1, v7}, Lboz;->b([II)[I

    move-result-object v1

    iput-object v1, v2, Ljay;->c:[I

    iget-object v1, v0, Lhfk;->m:Lgtm;

    iget-object v7, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v7, v9}, Lgtm;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_11

    iput-object v1, v2, Ljay;->b:Ljava/lang/String;

    :cond_11
    iget-object v1, v0, Lhfk;->d:Lhce;

    new-instance v7, Lhfl;

    invoke-direct {v7, v0, v3, v6, v2}, Lhfl;-><init>(Lhfk;Landroid/accounts/Account;Landroid/os/Bundle;Ljay;)V

    invoke-virtual {v1, v7}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    const-string v0, "NetworkOwService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected ServerResponse type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v5, v0, v1, v2}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_5

    :sswitch_0
    invoke-direct {v0, v5, p1, v6}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V

    goto/16 :goto_5

    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x19b

    invoke-static {v5, v0, v1, v2}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_5

    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v5, v0, v1, v2}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_5

    :sswitch_3
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v1

    check-cast v1, Lipy;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lhfx;->a(Lipy;Z)Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-direct {v0, v5, p1, v6}, Lhfk;->a(Lhal;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V

    goto/16 :goto_5

    :cond_12
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lhfx;->a(Lipy;)I

    move-result v1

    invoke-static {v5, v0, v2, v1}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_5

    :sswitch_4
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v1

    check-cast v1, Ljaz;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v7

    invoke-direct/range {v0 .. v9}, Lhfk;->a(Ljaz;Ljay;Landroid/accounts/Account;Lgsm;Lhal;Landroid/os/Bundle;Lcom/google/android/gms/wallet/Cart;ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v8, v8, -0x1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0xd -> :sswitch_4
        0x11 -> :sswitch_3
        0x16 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;Landroid/os/Bundle;)V
    .locals 7

    const/16 v3, 0x194

    invoke-direct {p0, p2}, Lhfk;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NotifyTransactionStatus"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v1, "NotifyTransactionStatusRequest was null."

    invoke-static {v1, v0}, Lhfk;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0, v2, p2}, Lhfk;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lhfk;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0, v2, p2}, Lhfk;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    const-string v1, "UNKNOWN"

    const-string v0, ""

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;->b()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    const-string v0, "UNKNOWN"

    :goto_1
    new-instance v3, Lgsm;

    iget-object v4, p0, Lhfk;->b:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Lgsm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v2, "onlinewallet"

    const-string v4, "notify_transaction_status"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v0, v5, v1

    invoke-static {v3, v2, v4, v5}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    const-string v1, "SUCCESS"

    goto :goto_1

    :pswitch_2
    const-string v1, "FAILURE"

    const-string v0, "AVS_DECLINE"

    goto :goto_1

    :pswitch_3
    const-string v1, "FAILURE"

    const-string v0, "BAD_CARD"

    goto :goto_1

    :pswitch_4
    const-string v1, "FAILURE"

    const-string v0, "BAD_CVC"

    goto :goto_1

    :pswitch_5
    const-string v1, "FAILURE"

    const-string v0, "DECLINED"

    goto :goto_1

    :pswitch_6
    const-string v1, "FAILURE"

    const-string v0, "FRAUD_DECLINE"

    goto :goto_1

    :pswitch_7
    const-string v1, "FAILURE"

    const-string v0, "OTHER"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lhal;)V
    .locals 6

    const/16 v5, 0x19a

    const/16 v4, 0x194

    const-string v0, "callbacks is required"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p3}, Lhfk;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgsm;

    iget-object v2, p0, Lhfk;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lgsm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v2, "onlinewallet"

    const-string v3, "change_masked_wallet"

    invoke-static {v1, v2, v3}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChangeMaskedWallet"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1}, Lhfk;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v4, v1, v0, p3}, Lhfk;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p4, p1, p2, v4}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {v2}, Lbov;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v0, 0x199

    invoke-static {p4, p1, p2, v0}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lhfk;->e:Lhga;

    invoke-virtual {v2, p1}, Lhga;->a(Ljava/lang/String;)Lhgb;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v3, v2, Lhgb;->e:Ljbg;

    if-nez v3, :cond_3

    :cond_2
    const-string v2, "\ninvalid transaction id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v5, v1, v0, p3}, Lhfk;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p4, p1, p2, v5}, Lhfk;->a(Lhal;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    invoke-static {p3, v2}, Lhfk;->a(Landroid/os/Bundle;Lhgb;)V

    const/4 v0, 0x6

    invoke-static {p1, p2}, Lhfk;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v1

    iget-object v3, p0, Lhfk;->b:Landroid/content/Context;

    invoke-static {p3}, Lhfx;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v4

    invoke-static {v3, p1, p2, v2, v4}, Lhfx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lhgb;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {p4, v0, v1, v2}, Lhal;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method
