.class final Lbep;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbeo;


# direct methods
.method constructor <init>(Lbeo;)V
    .locals 0

    iput-object p1, p0, Lbep;->a:Lbeo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    const-string v0, "This runnable can only be called in the Main thread!"

    invoke-static {v0}, Lbkm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lbep;->a:Lbeo;

    invoke-static {v0}, Lbeo;->a(Lbeo;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lber;

    iget-boolean v0, v0, Lber;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbep;->a:Lbeo;

    invoke-virtual {v0}, Lbeo;->stopSelf()V

    :cond_2
    return-void
.end method
