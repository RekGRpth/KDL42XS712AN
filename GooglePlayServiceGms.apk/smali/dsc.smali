.class public final Ldsc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldrb;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ldqy;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldqy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldsc;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Ldsc;->b:Ldqy;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcun;)V
    .locals 2

    iget-object v0, p0, Ldsc;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lduj;->e(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/games/service/GamesAndroidService;->a()V

    invoke-virtual {p2}, Lcun;->b()V

    :try_start_0
    iget-object v0, p0, Ldsc;->b:Ldqy;

    invoke-virtual {v0}, Ldqy;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldqy;->d:Ldqv;

    invoke-static {v1}, Ldqv;->c(Ldqv;)V

    iget-object v1, v0, Ldqy;->a:Ldad;

    if-eqz v1, :cond_0

    iget-object v0, v0, Ldqy;->a:Ldad;

    invoke-interface {v0}, Ldad;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
