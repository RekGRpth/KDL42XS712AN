.class public final Lgpx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/android/gms/plus/model/posts/PlusPage;

.field c:Ljava/lang/String;

.field d:I

.field e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field j:Z

.field k:Z

.field l:Z

.field public m:Ljava/lang/String;

.field n:Ljava/lang/String;

.field public o:Lgqb;

.field p:Lgqa;

.field public q:Lcom/google/android/gms/common/people/data/Audience;

.field r:Lcom/google/android/gms/common/people/data/AudienceMember;

.field s:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->a:Ljava/lang/String;

    const-string v0, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->n:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lgqb;->a(Landroid/os/Bundle;)Lgqb;

    move-result-object v0

    iput-object v0, p0, Lgpx;->o:Lgqb;

    const-string v0, "com.google.android.apps.plus.CALL_TO_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lgqa;->a(Landroid/os/Bundle;)Lgqa;

    move-result-object v0

    iput-object v0, p0, Lgpx;->p:Lgqa;

    const-string v0, "com.google.android.apps.plus.RECIPIENT_IDS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lgpq;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    iput-object v0, p0, Lgpx;->q:Lcom/google/android/gms/common/people/data/Audience;

    const-string v0, "com.google.android.gms.plus.intent.extra.INTERNAL_PREFILLED_PLUS_MENTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    iput-object v0, p0, Lgpx;->r:Lcom/google/android/gms/common/people/data/AudienceMember;

    const/4 v0, 0x0

    iput v0, p0, Lgpx;->i:I

    sget-object v0, Lfsr;->Y:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lgpx;->j:Z

    sget-object v0, Lfsr;->Z:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lgpx;->k:Z

    sget-object v0, Lfsr;->aa:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lgpx;->l:Z

    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->s:Ljava/lang/String;

    iget-object v0, p0, Lgpx;->s:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->s:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lgpx;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpx;->o:Lgqb;

    invoke-virtual {v0}, Lgqb;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgpx;->a(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lgpx;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpx;->s:Ljava/lang/String;

    invoke-direct {p0, v0}, Lgpx;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lgpx;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgpx;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput-object v0, p0, Lgpx;->s:Ljava/lang/String;

    :cond_2
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v1

    const-class v2, Landroid/text/style/URLSpan;

    invoke-virtual {v0, v3, v1, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v1, v0

    if-lez v1, :cond_0

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->n:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_ID"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_IMAGE_URL"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_DISPLAY_NAME"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-direct {v5, v0, v4, v3}, Lcom/google/android/gms/plus/model/posts/PlusPage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v5, p0, Lgpx;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    :cond_0
    const-string v0, "com.google.android.gms.plus.intent.extra.INTERNAL_APP_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->c:Ljava/lang/String;

    const-string v0, "com.google.android.gms.plus.intent.extra.INTERNAL_APP_ICON_RESOURCE"

    const/4 v3, -0x1

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgpx;->d:I

    const-string v0, "com.google.android.gms.plus.intent.extra.INTERNAL_SHARE_BUTTON_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->e:Ljava/lang/String;

    const-string v0, "com.google.android.gms.plus.intent.extra.SHARE_CONTEXT_TYPE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->f:Ljava/lang/String;

    iget-object v0, p0, Lgpx;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lgpq;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v3

    const-string v4, "com.google.android.gms.plus.intent.extra.OVERRIDE_FIRST_VIEW"

    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p2, v4, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgpx;->i:I

    const-string v4, "com.google.android.gms.plus.intent.extra.SHOW_ACL_SUGGESTED_SECTION"

    if-nez v3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p2, v4, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpx;->j:Z

    const-string v4, "com.google.android.gms.plus.intent.extra.INCLUDE_SUGGESTIONS_WITH_PEOPLE"

    if-nez v3, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p2, v4, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpx;->k:Z

    const-string v0, "com.google.android.gms.plus.intent.extra.SHOW_ADD_TO_CIRCLE"

    if-nez v3, :cond_4

    :goto_3
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpx;->l:Z

    const-string v0, "com.google.android.gms.plus.intent.extra.CLIENT_APPLICATION_ID"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->m:Ljava/lang/String;

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lgpx;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lgpx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpx;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/PlusPage;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lgpx;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpx;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/PlusPage;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lgpx;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lgpx;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lgpx;->o:Lgqb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lgpx;->p:Lgqa;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lgpx;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
