.class final Lan;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcd;


# instance fields
.field final a:I

.field final b:Landroid/os/Bundle;

.field c:Lal;

.field d:Lcb;

.field e:Z

.field f:Z

.field g:Ljava/lang/Object;

.field h:Z

.field i:Z

.field j:Z

.field k:Z

.field l:Z

.field m:Z

.field n:Lan;

.field final synthetic o:Lam;


# direct methods
.method public constructor <init>(Lam;ILandroid/os/Bundle;Lal;)V
    .locals 0

    iput-object p1, p0, Lan;->o:Lam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lan;->a:I

    iput-object p3, p0, Lan;->b:Landroid/os/Bundle;

    iput-object p4, p0, Lan;->c:Lal;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lan;->i:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lan;->j:Z

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lan;->h:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lan;->h:Z

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lan;->h:Z

    sget-boolean v0, Lam;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Starting: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lan;->d:Lcb;

    if-nez v0, :cond_3

    iget-object v0, p0, Lan;->c:Lal;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lan;->c:Lal;

    iget v1, p0, Lan;->a:I

    iget-object v2, p0, Lan;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2}, Lal;->a(ILandroid/os/Bundle;)Lcb;

    move-result-object v0

    iput-object v0, p0, Lan;->d:Lcb;

    :cond_3
    iget-object v0, p0, Lan;->d:Lcb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lan;->d:Lcb;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lan;->d:Lcb;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Object returned from onCreateLoader must not be a non-static inner member class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lan;->d:Lcb;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-boolean v0, p0, Lan;->m:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lan;->d:Lcb;

    iget v1, p0, Lan;->a:I

    iget-object v2, v0, Lcb;->n:Lcd;

    if-eqz v2, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a listener registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iput-object p0, v0, Lcb;->n:Lcd;

    iput v1, v0, Lcb;->m:I

    iput-boolean v3, p0, Lan;->m:Z

    :cond_6
    iget-object v0, p0, Lan;->d:Lcb;

    invoke-virtual {v0}, Lcb;->h()V

    goto/16 :goto_0
.end method

.method public final a(Lcb;Ljava/lang/Object;)V
    .locals 5

    const/4 v4, 0x0

    sget-boolean v0, Lam;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadComplete: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lan;->l:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lam;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "LoaderManager"

    const-string v1, "  Ignoring load complete -- destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->b:Ldt;

    iget v1, p0, Lan;->a:I

    invoke-virtual {v0, v1}, Ldt;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_3

    sget-boolean v0, Lam;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "LoaderManager"

    const-string v1, "  Ignoring load complete -- not active"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lan;->n:Lan;

    if-eqz v0, :cond_5

    sget-boolean v1, Lam;->a:Z

    if-eqz v1, :cond_4

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Switching to pending loader: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iput-object v4, p0, Lan;->n:Lan;

    iget-object v1, p0, Lan;->o:Lam;

    iget-object v1, v1, Lam;->b:Ldt;

    iget v2, p0, Lan;->a:I

    invoke-virtual {v1, v2, v4}, Ldt;->a(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lan;->c()V

    iget-object v1, p0, Lan;->o:Lam;

    invoke-virtual {v1, v0}, Lam;->a(Lan;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lan;->g:Ljava/lang/Object;

    if-ne v0, p2, :cond_6

    iget-boolean v0, p0, Lan;->e:Z

    if-nez v0, :cond_7

    :cond_6
    iput-object p2, p0, Lan;->g:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lan;->e:Z

    iget-boolean v0, p0, Lan;->h:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0, p1, p2}, Lan;->b(Lcb;Ljava/lang/Object;)V

    :cond_7
    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->c:Ldt;

    iget v1, p0, Lan;->a:I

    invoke-virtual {v0, v1}, Ldt;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lan;

    if-eqz v0, :cond_8

    if-eq v0, p0, :cond_8

    const/4 v1, 0x0

    iput-boolean v1, v0, Lan;->f:Z

    invoke-virtual {v0}, Lan;->c()V

    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->c:Ldt;

    iget v1, p0, Lan;->a:I

    invoke-virtual {v0, v1}, Ldt;->b(I)V

    :cond_8
    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lan;->o:Lam;

    invoke-virtual {v0}, Lam;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    iget-object v0, v0, Lo;->b:Lw;

    invoke-virtual {v0}, Lw;->e()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    :goto_0
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lan;->a:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lan;->b:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCallbacks="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lan;->c:Lal;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLoader="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lan;->d:Lcb;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Lan;->d:Lcb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lan;->d:Lcb;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lcb;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lan;->e:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lan;->f:Z

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHaveData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lan;->e:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, "  mDeliveredData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lan;->f:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lan;->g:Ljava/lang/Object;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_2
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lan;->h:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mReportNextStart="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lan;->k:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lan;->l:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lan;->i:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mRetainingStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lan;->j:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mListenerRegistered="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lan;->m:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    iget-object v0, p0, Lan;->n:Lan;

    if-eqz v0, :cond_3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Loader "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lan;->n:Lan;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object p0, p0, Lan;->n:Lan;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method final b()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lam;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Stopping: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v3, p0, Lan;->h:Z

    iget-boolean v0, p0, Lan;->i:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lan;->d:Lcb;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lan;->m:Z

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lan;->m:Z

    iget-object v0, p0, Lan;->d:Lcb;

    invoke-virtual {v0, p0}, Lcb;->a(Lcd;)V

    iget-object v0, p0, Lan;->d:Lcb;

    invoke-virtual {v0}, Lcb;->i()V

    :cond_1
    return-void
.end method

.method final b(Lcb;Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lan;->c:Lal;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iget-object v1, p0, Lan;->o:Lam;

    iget-object v1, v1, Lam;->e:Lo;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    iget-object v0, v0, Lo;->b:Lw;

    iget-object v0, v0, Lw;->u:Ljava/lang/String;

    iget-object v1, p0, Lan;->o:Lam;

    iget-object v1, v1, Lam;->e:Lo;

    iget-object v1, v1, Lo;->b:Lw;

    const-string v2, "onLoadFinished"

    iput-object v2, v1, Lw;->u:Ljava/lang/String;

    move-object v1, v0

    :goto_0
    :try_start_0
    sget-boolean v0, Lam;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  onLoadFinished in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x40

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-static {p2, v3}, Ldj;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    const-string v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lan;->c:Lal;

    invoke-interface {v0, p1, p2}, Lal;->a(Lcb;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    iget-object v0, v0, Lo;->b:Lw;

    iput-object v1, v0, Lw;->u:Ljava/lang/String;

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lan;->f:Z

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    iget-object v2, p0, Lan;->o:Lam;

    iget-object v2, v2, Lam;->e:Lo;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lan;->o:Lam;

    iget-object v2, v2, Lam;->e:Lo;

    iget-object v2, v2, Lo;->b:Lw;

    iput-object v1, v2, Lw;->u:Ljava/lang/String;

    :cond_3
    throw v0

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method final c()V
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    sget-boolean v0, Lam;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "  Destroying: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v5, p0, Lan;->l:Z

    iget-boolean v0, p0, Lan;->f:Z

    iput-boolean v4, p0, Lan;->f:Z

    iget-object v1, p0, Lan;->c:Lal;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lan;->d:Lcb;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lan;->e:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    sget-boolean v0, Lam;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "  Reseting: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    iget-object v0, v0, Lo;->b:Lw;

    iget-object v0, v0, Lw;->u:Ljava/lang/String;

    iget-object v1, p0, Lan;->o:Lam;

    iget-object v1, v1, Lam;->e:Lo;

    iget-object v1, v1, Lo;->b:Lw;

    const-string v3, "onLoaderReset"

    iput-object v3, v1, Lw;->u:Ljava/lang/String;

    move-object v1, v0

    :goto_1
    :try_start_0
    iget-object v0, p0, Lan;->c:Lal;

    iget-object v3, p0, Lan;->d:Lcb;

    invoke-interface {v0}, Lal;->N_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lan;->o:Lam;

    iget-object v0, v0, Lam;->e:Lo;

    iget-object v0, v0, Lo;->b:Lw;

    iput-object v1, v0, Lw;->u:Ljava/lang/String;

    :cond_2
    iput-object v2, p0, Lan;->c:Lal;

    iput-object v2, p0, Lan;->g:Ljava/lang/Object;

    iput-boolean v4, p0, Lan;->e:Z

    iget-object v0, p0, Lan;->d:Lcb;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lan;->m:Z

    if-eqz v0, :cond_3

    iput-boolean v4, p0, Lan;->m:Z

    iget-object v0, p0, Lan;->d:Lcb;

    invoke-virtual {v0, p0}, Lcb;->a(Lcd;)V

    :cond_3
    iget-object v0, p0, Lan;->d:Lcb;

    invoke-virtual {v0}, Lcb;->g()V

    iput-boolean v5, v0, Lcb;->r:Z

    iput-boolean v4, v0, Lcb;->p:Z

    iput-boolean v4, v0, Lcb;->q:Z

    iput-boolean v4, v0, Lcb;->s:Z

    iput-boolean v4, v0, Lcb;->t:Z

    :cond_4
    iget-object v0, p0, Lan;->n:Lan;

    if-eqz v0, :cond_6

    iget-object p0, p0, Lan;->n:Lan;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v2, p0, Lan;->o:Lam;

    iget-object v2, v2, Lam;->e:Lo;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lan;->o:Lam;

    iget-object v2, v2, Lam;->e:Lo;

    iget-object v2, v2, Lo;->b:Lw;

    iput-object v1, v2, Lw;->u:Ljava/lang/String;

    :cond_5
    throw v0

    :cond_6
    return-void

    :cond_7
    move-object v1, v2

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "LoaderInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lan;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lan;->d:Lcb;

    invoke-static {v1, v0}, Ldj;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
