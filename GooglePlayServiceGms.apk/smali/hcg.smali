.class public final Lhcg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/Class;

.field public static final b:Ljava/lang/Class;

.field public static final c:Ljava/lang/Class;


# instance fields
.field private d:Ljava/util/LinkedHashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Ljbg;

    sput-object v0, Lhcg;->a:Ljava/lang/Class;

    const-class v0, Lipk;

    sput-object v0, Lhcg;->b:Ljava/lang/Class;

    const-class v0, Lios;

    sput-object v0, Lhcg;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lhch;

    sget-object v0, Lgzp;->k:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, p0, v0}, Lhch;-><init>(Lhcg;I)V

    iput-object v1, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    return-void
.end method

.method public static a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/16 v2, 0xa

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;
    .locals 7

    const/4 v6, 0x0

    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ":"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v4, v3, v6, v5}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/Class;Lizs;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a([Lioj;Ljava/lang/String;Lioj;)V
    .locals 4

    const/4 v0, 0x0

    array-length v1, p0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p0, v0

    iget-object v3, v2, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v1, v2, Lioj;->f:Z

    iput-boolean v1, p2, Lioj;->f:Z

    aput-object p2, p0, v0

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Lizs;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 3

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, Lgtj;->a(Ljava/lang/String;Lizs;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    instance-of v2, v0, Lizz;

    if-eqz v2, :cond_1

    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x13

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    instance-of v2, v0, Lipl;

    if-eqz v2, :cond_2

    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v2, 0x3

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, v0, Liot;

    if-eqz v2, :cond_3

    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x1b

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lizs;Lizs;)V
    .locals 14

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    if-eqz v1, :cond_3

    check-cast p3, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    if-eqz v1, :cond_1

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    invoke-direct {p0, p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Lizs;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-eqz v1, :cond_0

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;)V

    sget-object v1, Lhcg;->a:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizz;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Lizz;

    iget-object v3, v2, Lizz;->b:[Lioj;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-static {v3, v7}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lioj;

    iput-object v3, v2, Lizz;->b:[Lioj;

    iget-object v3, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_2
    move-object/from16 v0, p2

    instance-of v1, v0, Lipa;

    if-eqz v1, :cond_0

    check-cast p2, Lipa;

    move-object/from16 v0, p2

    iget-object v1, v0, Lipa;->a:Lioq;

    if-eqz v1, :cond_0

    move-object/from16 v0, p2

    iget-object v1, v0, Lipa;->a:Lioq;

    iget-object v1, v1, Lioq;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lhcg;->c:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v1

    :goto_2
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liot;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Liot;

    iget-object v3, v2, Liot;->a:[Lioj;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-static {v3, v7}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lioj;

    iput-object v3, v2, Liot;->a:[Lioj;

    iget-object v3, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_3
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    if-eqz v1, :cond_6

    check-cast p2, Lipp;

    check-cast p3, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->b:Lipl;

    if-eqz v1, :cond_4

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->b:Lipl;

    invoke-direct {p0, p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Lizs;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_4
    :try_start_2
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lioj;

    if-eqz v1, :cond_0

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;)V

    move-object/from16 v0, p2

    iget-object v4, v0, Lipp;->b:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lioj;

    sget-object v1, Lhcg;->a:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v6

    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v1

    :goto_3
    if-ge v3, v7, :cond_5

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizz;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Lizz;

    iget-object v8, v2, Lizz;->b:[Lioj;

    invoke-static {v8, v4, v5}, Lhcg;->a([Lioj;Ljava/lang/String;Lioj;)V

    iget-object v8, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v8, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    :cond_5
    move-object/from16 v0, p2

    iget-object v1, v0, Lipp;->a:Lioq;

    if-eqz v1, :cond_0

    move-object/from16 v0, p2

    iget-object v1, v0, Lipp;->a:Lioq;

    iget-object v1, v1, Lioq;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lhcg;->c:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v6

    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v1

    :goto_4
    if-ge v3, v7, :cond_0

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liot;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Liot;

    iget-object v8, v2, Liot;->a:[Lioj;

    invoke-static {v8, v4, v5}, Lhcg;->a([Lioj;Ljava/lang/String;Lioj;)V

    iget-object v8, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v8, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    :cond_6
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    if-eqz v1, :cond_8

    check-cast p3, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->b:Lipl;

    if-eqz v1, :cond_7

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->b:Lipl;

    invoke-direct {p0, p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Lizs;)V

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lipv;

    if-eqz v1, :cond_0

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;)V

    sget-object v1, Lhcg;->a:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v1

    :goto_5
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizz;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Lizz;

    iget-object v3, v2, Lizz;->c:[Lipv;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lipv;

    invoke-static {v3, v7}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lipv;

    iput-object v3, v2, Lizz;->c:[Lipv;

    iget-object v3, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_5

    :cond_8
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    if-eqz v1, :cond_c

    check-cast p2, Lipo;

    check-cast p3, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lipl;

    if-eqz v1, :cond_9

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lipl;

    invoke-direct {p0, p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Lizs;)V

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lipv;

    if-eqz v1, :cond_0

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;)V

    move-object/from16 v0, p2

    iget-object v5, v0, Lipo;->b:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lipv;

    sget-object v1, Lhcg;->a:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v1, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v1

    :goto_6
    if-ge v4, v8, :cond_0

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizz;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Lizz;

    iget-object v3, v2, Lizz;->c:[Lipv;

    array-length v9, v3

    const/4 v3, 0x0

    :goto_7
    if-ge v3, v9, :cond_a

    iget-object v10, v2, Lizz;->c:[Lipv;

    aget-object v10, v10, v3

    iget-object v11, v10, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    iget-boolean v9, v10, Lipv;->h:Z

    iput-boolean v9, v6, Lipv;->h:Z

    iget-object v9, v2, Lizz;->c:[Lipv;

    aput-object v6, v9, v3

    :cond_a
    iget-object v3, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_6

    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_c
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    if-eqz v1, :cond_e

    check-cast p3, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lipl;

    if-eqz v1, :cond_d

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lipl;

    invoke-direct {p0, p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Lizs;)V

    goto/16 :goto_0

    :cond_d
    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;)V

    goto/16 :goto_0

    :cond_e
    move-object/from16 v0, p3

    instance-of v1, v0, Lipn;

    if-eqz v1, :cond_12

    check-cast p3, Lipn;

    move-object/from16 v0, p3

    iget v1, v0, Lipn;->b:I

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    check-cast p2, Lipm;

    move-object/from16 v0, p2

    iget-object v6, v0, Lipm;->b:Ljava/lang/String;

    sget-object v1, Lhcg;->b:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v1, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v5, v1

    :goto_8
    if-ge v5, v8, :cond_0

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lipl;

    iget-object v9, v2, Lipl;->b:[Lion;

    invoke-static {v9, v6}, Lgth;->a([Lion;Ljava/lang/String;)Lion;

    move-result-object v10

    if-eqz v10, :cond_11

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Lipl;

    const/4 v3, 0x0

    array-length v11, v9

    move v4, v3

    :goto_9
    if-ge v4, v11, :cond_10

    aget-object v3, v9, v4

    iget-object v12, v3, Lion;->a:Lioj;

    iget-object v12, v12, Lioj;->a:Ljava/lang/String;

    iget-object v13, v10, Lion;->a:Lioj;

    iget-object v13, v13, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_f

    iget-object v12, v3, Lion;->a:Lioj;

    invoke-static {v12}, Lgth;->g(Lioj;)Lioj;

    move-result-object v12

    invoke-static {v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v3

    check-cast v3, Lion;

    iput-object v12, v3, Lion;->a:Lioj;

    iget-object v12, v2, Lipl;->b:[Lion;

    aput-object v3, v12, v4

    :cond_f
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_9

    :cond_10
    iget-object v3, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_11
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_8

    :cond_12
    move-object/from16 v0, p3

    instance-of v1, v0, Ljax;

    if-eqz v1, :cond_15

    check-cast p3, Ljax;

    check-cast p2, Ljaw;

    move-object/from16 v0, p3

    iget-object v1, v0, Ljax;->a:[I

    array-length v1, v1

    if-nez v1, :cond_0

    move-object/from16 v0, p3

    iget-object v1, v0, Ljax;->b:Ljbh;

    if-eqz v1, :cond_0

    move-object/from16 v0, p3

    iget-object v5, v0, Ljax;->b:Ljbh;

    move-object/from16 v0, p2

    iget-object v1, v0, Ljaw;->e:Linv;

    if-eqz v1, :cond_0

    iget-object v1, v5, Ljbh;->e:Ljbi;

    if-eqz v1, :cond_0

    sget-object v1, Lhcg;->a:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v6

    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v1

    :goto_a
    if-ge v4, v7, :cond_0

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizz;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Lizz;

    iget-object v3, v2, Lizz;->b:[Lioj;

    iget-object v8, v5, Ljbh;->e:Ljbi;

    invoke-static {v8}, Lgtk;->a(Ljbi;)Lioj;

    move-result-object v8

    invoke-static {v3, v8}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lioj;

    iput-object v3, v2, Lizz;->b:[Lioj;

    move-object/from16 v0, p2

    iget-object v3, v0, Ljaw;->g:Lipv;

    if-eqz v3, :cond_13

    iget-object v3, v2, Lizz;->c:[Lipv;

    iget-object v8, v5, Ljbh;->d:Lipv;

    invoke-static {v3, v8}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lipv;

    iput-object v3, v2, Lizz;->c:[Lipv;

    :cond_13
    iget-object v3, v2, Lizz;->a:Ljbb;

    if-eqz v3, :cond_14

    iget-object v3, v2, Lizz;->a:Ljbb;

    iget-object v8, v2, Lizz;->a:Ljbb;

    iget-object v8, v8, Ljbb;->a:[I

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_0

    invoke-static {v8, v9}, Lboz;->a([I[I)[I

    move-result-object v8

    iput-object v8, v3, Ljbb;->a:[I

    iget-object v3, v2, Lizz;->a:Ljbb;

    invoke-static {}, Ljbe;->c()[Ljbe;

    move-result-object v8

    iput-object v8, v3, Ljbb;->g:[Ljbe;

    :cond_14
    iget-object v3, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_a

    :cond_15
    move-object/from16 v0, p3

    instance-of v1, v0, Liov;

    if-eqz v1, :cond_0

    check-cast p3, Liov;

    move-object/from16 v0, p3

    iget v1, v0, Liov;->a:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    check-cast p2, Liou;

    move-object/from16 v0, p2

    iget-object v4, v0, Liou;->b:Ljava/lang/String;

    sget-object v1, Lhcg;->c:Ljava/lang/Class;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lhcg;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v1

    :goto_b
    if-ge v3, v6, :cond_0

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liot;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Liot;

    iget-object v7, v2, Liot;->a:[Lioj;

    invoke-static {v7, v4}, Lgth;->a([Lioj;Ljava/lang/String;)Lioj;

    move-result-object v7

    if-eqz v7, :cond_16

    const/4 v8, 0x2

    iput v8, v7, Lioj;->h:I

    :cond_16
    iget-object v7, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v7, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_b

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method public final declared-synchronized b(Ljava/lang/String;Lizs;Lizs;)V
    .locals 3

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, Lgtj;->a(Ljava/lang/String;Lizs;)Ljava/lang/String;

    move-result-object v1

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    instance-of v2, p3, Lipe;

    if-nez v2, :cond_0

    instance-of v2, p3, Lipy;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhcg;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
