.class public final Lduk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Ldux;

.field final c:Ljava/util/ArrayList;

.field public final d:Ljava/lang/String;

.field public final e:[Ljava/lang/String;

.field public f:Ldam;

.field public g:Ldvd;

.field h:Ldul;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Lduw;Ldux;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lduk;->a:Landroid/content/Context;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lduk;->d:Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lduk;->e:[Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lduk;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Lduk;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p5, p0, Lduk;->b:Ldux;

    new-instance v0, Ldul;

    invoke-direct {v0, p0}, Ldul;-><init>(Lduk;)V

    iput-object v0, p0, Lduk;->h:Ldul;

    return-void
.end method


# virtual methods
.method public final a(Z)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lduk;->c()V

    :try_start_0
    iget-object v0, p0, Lduk;->f:Ldam;

    iget-object v1, p0, Lduk;->d:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ldam;->a(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.signin.service.START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lduk;->g:Ldvd;

    if-eqz v1, :cond_0

    const-string v1, "SignInClient"

    const-string v2, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lduk;->f:Ldam;

    iget-object v1, p0, Lduk;->a:Landroid/content/Context;

    iget-object v2, p0, Lduk;->g:Ldvd;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    new-instance v1, Ldvd;

    iget-object v2, p0, Lduk;->a:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Ldvd;-><init>(Lduk;Landroid/content/Context;)V

    iput-object v1, p0, Lduk;->g:Ldvd;

    iget-object v1, p0, Lduk;->a:Landroid/content/Context;

    iget-object v2, p0, Lduk;->g:Ldvd;

    const/16 v3, 0x81

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lduk;->f:Ldam;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    invoke-virtual {p0}, Lduk;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
