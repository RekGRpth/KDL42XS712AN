.class public final Lfag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfaf;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Landroid/graphics/BitmapRegionDecoder;

.field private final f:Leym;


# direct methods
.method private constructor <init>(Ljava/io/InputStream;Leym;)V
    .locals 6

    const/16 v1, 0x200

    const-wide/high16 v4, 0x4080000000000000L    # 512.0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lfag;->f:Leym;

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v0

    iput-object v0, p0, Lfag;->e:Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lfag;->e:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v0

    int-to-double v2, v0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    iput v0, p0, Lfag;->a:I

    iget-object v0, p0, Lfag;->e:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v0

    rem-int/lit16 v0, v0, 0x200

    if-lez v0, :cond_1

    :goto_0
    iput v0, p0, Lfag;->c:I

    iget-object v0, p0, Lfag;->e:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v0

    int-to-double v2, v0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    iput v0, p0, Lfag;->b:I

    iget-object v0, p0, Lfag;->e:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v0

    rem-int/lit16 v0, v0, 0x200

    if-lez v0, :cond_0

    move v1, v0

    :cond_0
    iput v1, p0, Lfag;->d:I

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not create decoder"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Leym;)Lfaf;
    .locals 3

    :try_start_0
    new-instance v0, Lfag;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1, p1}, Lfag;-><init>(Ljava/io/InputStream;Leym;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "TileProviderImpl"

    const-string v2, "File not found"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(II)Landroid/graphics/Rect;
    .locals 5

    mul-int/lit16 v2, p1, 0x200

    mul-int/lit16 v3, p2, 0x200

    add-int/lit16 v0, v2, 0x200

    add-int/lit16 v1, v3, 0x200

    iget v4, p0, Lfag;->a:I

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_0

    iget v4, p0, Lfag;->c:I

    rsub-int v4, v4, 0x200

    sub-int/2addr v0, v4

    :cond_0
    iget v4, p0, Lfag;->b:I

    add-int/lit8 v4, v4, -0x1

    if-ne p2, v4, :cond_1

    iget v4, p0, Lfag;->d:I

    rsub-int v4, v4, 0x200

    sub-int/2addr v1, v4

    :cond_1
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v2, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0x200

    return v0
.end method

.method public final declared-synchronized a(II)Lfae;
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lfag;->b(II)Landroid/graphics/Rect;

    move-result-object v0

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lfag;->f:Leym;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Leym;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    iget-object v0, p0, Lfag;->e:Landroid/graphics/BitmapRegionDecoder;

    invoke-direct {p0, p1, p2}, Lfag;->b(II)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Lfae;

    const/16 v4, 0x200

    const/16 v5, 0x200

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lfae;-><init>(Landroid/graphics/Bitmap;IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 0

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lfag;->a:I

    return v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lfag;->b:I

    return v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lfag;->c:I

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lfag;->d:I

    return v0
.end method

.method public final f()F
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public final g()Leym;
    .locals 1

    iget-object v0, p0, Lfag;->f:Leym;

    return-object v0
.end method
