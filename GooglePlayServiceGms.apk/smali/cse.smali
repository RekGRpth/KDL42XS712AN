.class public final Lcse;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcsn;

.field final synthetic b:Lcom/google/android/gms/feedback/ErrorReport;

.field final synthetic c:Lcom/google/android/gms/feedback/FeedbackActivity;

.field final synthetic d:Lcom/google/android/gms/feedback/FeedbackActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcsn;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackActivity;)V
    .locals 0

    iput-object p1, p0, Lcse;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    iput-object p2, p0, Lcse;->a:Lcsn;

    iput-object p3, p0, Lcse;->b:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object p4, p0, Lcse;->c:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcse;->a:Lcsn;

    iget-object v2, p0, Lcse;->b:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, p0, Lcse;->b:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcsn;->a(Lcom/google/android/gms/feedback/ErrorReport;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Lcsp;

    iget-object v1, p0, Lcse;->c:Lcom/google/android/gms/feedback/FeedbackActivity;

    iget-object v2, p0, Lcse;->b:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0, v1, v2}, Lcsp;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;)V

    invoke-virtual {v0}, Lcsp;->start()V

    :goto_1
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v1

    const-string v1, "FeedbackActivity"

    const-string v2, "Error doing instant send"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcse;->c:Lcom/google/android/gms/feedback/FeedbackActivity;

    const v1, 0x7f0b00aa    # com.google.android.gms.R.string.gf_report_being_sent

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(I)V

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcse;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
