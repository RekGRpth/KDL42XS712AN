.class final Lfcm;
.super Lfbx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lfch;

.field private final b:Lfan;


# direct methods
.method public constructor <init>(Lfch;Lfan;)V
    .locals 0

    iput-object p1, p0, Lfcm;->a:Lfch;

    invoke-direct {p0}, Lfbx;-><init>()V

    iput-object p2, p0, Lfcm;->b:Lfan;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 8

    const/4 v6, 0x0

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bundle callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nbundle="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1, p2}, Lfch;->a(ILandroid/os/Bundle;)Lbbo;

    move-result-object v3

    if-nez p3, :cond_1

    move-object v4, v6

    :goto_0
    if-nez p3, :cond_2

    move-object v5, v6

    :goto_1
    if-nez p3, :cond_3

    :goto_2
    iget-object v7, p0, Lfcm;->a:Lfch;

    new-instance v0, Lfcn;

    iget-object v1, p0, Lfcm;->a:Lfch;

    iget-object v2, p0, Lfcm;->b:Lfan;

    invoke-direct/range {v0 .. v6}, Lfcn;-><init>(Lfch;Lfan;Lbbo;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lfch;->b(Lbjg;)V

    return-void

    :cond_1
    const-string v0, "circle_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    const-string v0, "circle_name"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_3
    const-string v0, "added_people"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method
