.class public interface abstract Ldag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Ldad;[BLjava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract a([BLjava/lang/String;[Ljava/lang/String;)I
.end method

.method public abstract a(IIZ)Landroid/content/Intent;
.end method

.method public abstract a(I[BILjava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract a(Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;I)Landroid/content/Intent;
.end method

.method public abstract a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;
.end method

.method public abstract a(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(JLjava/lang/String;)V
.end method

.method public abstract a(Landroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract a(Ldad;)V
.end method

.method public abstract a(Ldad;I)V
.end method

.method public abstract a(Ldad;III)V
.end method

.method public abstract a(Ldad;IIZZ)V
.end method

.method public abstract a(Ldad;II[Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract a(Ldad;IZZ)V
.end method

.method public abstract a(Ldad;I[I)V
.end method

.method public abstract a(Ldad;J)V
.end method

.method public abstract a(Ldad;JLjava/lang/String;)V
.end method

.method public abstract a(Ldad;Landroid/os/Bundle;II)V
.end method

.method public abstract a(Ldad;Landroid/os/IBinder;I[Ljava/lang/String;Landroid/os/Bundle;ZJ)V
.end method

.method public abstract a(Ldad;Landroid/os/IBinder;Ljava/lang/String;ZJ)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;I)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;IIIZ)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;IZ)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;IZZ)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;IZZZZ)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;I[I)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;J)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;JLjava/lang/String;)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Ljava/lang/String;III)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;Z)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;[BLjava/lang/String;[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;[I)V
.end method

.method public abstract a(Ldad;Ljava/lang/String;[Ljava/lang/String;I[BI)V
.end method

.method public abstract a(Ldad;Z)V
.end method

.method public abstract a(Ldad;ZLandroid/os/Bundle;)V
.end method

.method public abstract a(Ldad;[I)V
.end method

.method public abstract a(Ldad;[Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;I)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b(IIZ)Landroid/content/Intent;
.end method

.method public abstract b()Landroid/os/Bundle;
.end method

.method public abstract b(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract b(J)V
.end method

.method public abstract b(JLjava/lang/String;)V
.end method

.method public abstract b(Ldad;)V
.end method

.method public abstract b(Ldad;IZZ)V
.end method

.method public abstract b(Ldad;J)V
.end method

.method public abstract b(Ldad;JLjava/lang/String;)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;IIIZ)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;IZ)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;IZZ)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract b(Ldad;Ljava/lang/String;Z)V
.end method

.method public abstract b(Ldad;Z)V
.end method

.method public abstract b(Ldad;[Ljava/lang/String;)V
.end method

.method public abstract b(Ljava/lang/String;I)V
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract c()V
.end method

.method public abstract c(J)V
.end method

.method public abstract c(JLjava/lang/String;)V
.end method

.method public abstract c(Ldad;)V
.end method

.method public abstract c(Ldad;IZZ)V
.end method

.method public abstract c(Ldad;J)V
.end method

.method public abstract c(Ldad;JLjava/lang/String;)V
.end method

.method public abstract c(Ldad;Ljava/lang/String;)V
.end method

.method public abstract c(Ldad;Ljava/lang/String;IZZ)V
.end method

.method public abstract c(Ldad;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract c(Ldad;Ljava/lang/String;Z)V
.end method

.method public abstract c(Ldad;Z)V
.end method

.method public abstract c(Ldad;[Ljava/lang/String;)V
.end method

.method public abstract c(Ljava/lang/String;)V
.end method

.method public abstract c(Ljava/lang/String;I)V
.end method

.method public abstract d(Ljava/lang/String;)I
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract d(J)V
.end method

.method public abstract d(Ldad;)V
.end method

.method public abstract d(Ldad;IZZ)V
.end method

.method public abstract d(Ldad;Ljava/lang/String;)V
.end method

.method public abstract d(Ldad;Ljava/lang/String;IZZ)V
.end method

.method public abstract d(Ldad;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract d(Ldad;Ljava/lang/String;Z)V
.end method

.method public abstract d(Ljava/lang/String;I)V
.end method

.method public abstract e(Ljava/lang/String;)Landroid/net/Uri;
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract e(Ldad;)V
.end method

.method public abstract e(Ldad;IZZ)V
.end method

.method public abstract e(Ldad;Ljava/lang/String;)V
.end method

.method public abstract e(Ldad;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract f()Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract f(Ldad;)V
.end method

.method public abstract f(Ldad;Ljava/lang/String;)V
.end method

.method public abstract f(Ljava/lang/String;)V
.end method

.method public abstract g(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract g(Ldad;)V
.end method

.method public abstract g(Ldad;Ljava/lang/String;)V
.end method

.method public abstract g()Z
.end method

.method public abstract h(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
.end method

.method public abstract h()Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract h(Ldad;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract h(Ldad;)V
.end method

.method public abstract i()I
.end method

.method public abstract i(Ldad;)V
.end method

.method public abstract i(Ldad;Ljava/lang/String;)V
.end method

.method public abstract j()V
.end method

.method public abstract j(Ldad;Ljava/lang/String;)V
.end method

.method public abstract k()Landroid/content/Intent;
.end method

.method public abstract k(Ldad;Ljava/lang/String;)V
.end method

.method public abstract l()Landroid/content/Intent;
.end method

.method public abstract l(Ldad;Ljava/lang/String;)V
.end method

.method public abstract m()Landroid/content/Intent;
.end method

.method public abstract m(Ldad;Ljava/lang/String;)V
.end method

.method public abstract n()Landroid/content/Intent;
.end method

.method public abstract n(Ldad;Ljava/lang/String;)V
.end method

.method public abstract o()Landroid/content/Intent;
.end method

.method public abstract o(Ldad;Ljava/lang/String;)V
.end method

.method public abstract p()Landroid/content/Intent;
.end method

.method public abstract p(Ldad;Ljava/lang/String;)V
.end method

.method public abstract q()Landroid/content/Intent;
.end method

.method public abstract q(Ldad;Ljava/lang/String;)V
.end method

.method public abstract r()I
.end method

.method public abstract s()I
.end method

.method public abstract t()I
.end method

.method public abstract u()Landroid/content/Intent;
.end method
