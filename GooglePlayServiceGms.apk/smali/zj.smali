.class public final Lzj;
.super Lzw;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Lzn;

.field private c:Lzi;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lzw;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lzj;->a:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v1, p0, Lzj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lzj;->c:Lzi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzj;->c:Lzi;

    invoke-interface {v0}, Lzi;->i()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(I)V
    .locals 3

    iget-object v1, p0, Lzj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lzj;->b:Lzn;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lzj;->b:Lzn;

    invoke-interface {v2, v0}, Lzn;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lzj;->b:Lzn;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lzi;)V
    .locals 2

    iget-object v1, p0, Lzj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lzj;->c:Lzi;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lzn;)V
    .locals 2

    iget-object v1, p0, Lzj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lzj;->b:Lzn;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 2

    iget-object v1, p0, Lzj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lzj;->c:Lzi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzj;->c:Lzi;

    invoke-interface {v0}, Lzi;->j()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 2

    iget-object v1, p0, Lzj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lzj;->c:Lzi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzj;->c:Lzi;

    invoke-interface {v0}, Lzi;->k()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 2

    iget-object v1, p0, Lzj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lzj;->c:Lzi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzj;->c:Lzi;

    invoke-interface {v0}, Lzi;->l()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()V
    .locals 3

    iget-object v1, p0, Lzj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lzj;->b:Lzn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzj;->b:Lzn;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lzn;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lzj;->b:Lzn;

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lzj;->c:Lzi;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lzj;->c:Lzi;

    invoke-interface {v0}, Lzi;->m()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
