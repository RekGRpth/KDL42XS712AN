.class public final Lze;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:Z

.field public c:Lzk;

.field private final d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

.field private final e:Lzp;

.field private final f:Landroid/content/Context;

.field private final g:Lzh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lzp;Lzh;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lze;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lze;->b:Z

    iput-object p1, p0, Lze;->f:Landroid/content/Context;

    iput-object p2, p0, Lze;->d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iput-object p3, p0, Lze;->e:Lzp;

    iput-object p4, p0, Lze;->g:Lzh;

    return-void
.end method


# virtual methods
.method public final a(J)Lzm;
    .locals 12

    const-string v0, "Starting mediation."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lze;->g:Lzh;

    iget-object v0, v0, Lzh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lzg;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Trying mediation network: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v5, Lzg;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    iget-object v0, v5, Lzg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v11, p0, Lze;->a:Ljava/lang/Object;

    monitor-enter v11

    :try_start_0
    iget-boolean v0, p0, Lze;->b:Z

    if-eqz v0, :cond_2

    new-instance v0, Lzm;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lzm;-><init>(I)V

    monitor-exit v11

    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Lzk;

    iget-object v1, p0, Lze;->f:Landroid/content/Context;

    iget-object v3, p0, Lze;->e:Lzp;

    iget-object v4, p0, Lze;->g:Lzh;

    iget-object v6, p0, Lze;->d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v6, v6, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v7, p0, Lze;->d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v7, v7, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v8, p0, Lze;->d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v8, v8, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-direct/range {v0 .. v8}, Lzk;-><init>(Landroid/content/Context;Ljava/lang/String;Lzp;Lzh;Lzg;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    iput-object v0, p0, Lze;->c:Lzk;

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lze;->c:Lzk;

    const-wide/32 v1, 0xea60

    invoke-virtual {v0, p1, p2, v1, v2}, Lzk;->a(JJ)Lzm;

    move-result-object v0

    iget v1, v0, Lzm;->a:I

    if-nez v1, :cond_3

    const-string v1, "Adapter succeeded."

    invoke-static {v1}, Lacj;->a(Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0

    :cond_3
    iget-object v1, v0, Lzm;->c:Lzs;

    if-eqz v1, :cond_1

    sget-object v1, Laci;->a:Landroid/os/Handler;

    new-instance v2, Lzf;

    invoke-direct {v2, p0, v0}, Lzf;-><init>(Lze;Lzm;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_4
    new-instance v0, Lzm;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lzm;-><init>(I)V

    goto :goto_1
.end method
