.class public final Lexg;
.super Lsc;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/Class;

.field private final g:Lsk;

.field private final h:Lizs;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLsk;Lsj;Ljava/lang/Class;Lizs;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p4}, Lsc;-><init>(ILjava/lang/String;Lsj;)V

    iput-boolean v2, p0, Lexg;->k:Z

    if-eqz p2, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "MDM"

    const-string v1, "want to send authenticated request, but no Google account on device"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lexg;->i:Ljava/lang/String;

    :goto_0
    iput-object p5, p0, Lexg;->f:Ljava/lang/Class;

    iput-object p3, p0, Lexg;->g:Lsk;

    iput-object p6, p0, Lexg;->h:Lizs;

    return-void

    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lexg;->i:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lrz;)Lsi;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lexg;->f:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p1, Lrz;->b:[B

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Lizm;->a([BII)Lizm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lizs;->a(Lizm;)Lizs;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lsp;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lsp;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Lsi;->a(Lsp;)Lsi;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lizs;

    iget-object v0, p0, Lexg;->g:Lsk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lexg;->g:Lsk;

    invoke-interface {v0, p1}, Lsk;->a(Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lexg;->k:Z

    return-void
.end method

.method public final b(Lsp;)V
    .locals 3

    instance-of v0, p1, Lrn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lexg;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iget-object v1, p0, Lexg;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lexg;->k:Z

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lsc;->b(Lsp;)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "MDM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not delivering error response for request=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], error=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] because response already delivered."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final i()Ljava/util/Map;
    .locals 4

    iget-object v0, p0, Lexg;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    iget-object v2, p0, Lexg;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "oauth2:"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lexh;->n:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexg;->j:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lexg;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "MDM"

    const-string v2, "GoogleAuthUtil.getToken threw IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Lexe;

    const-string v2, "GoogleAuthUtil.getToken threw IOException"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v0, v3}, Lexe;-><init>(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v1

    :catch_1
    move-exception v0

    const-string v1, "MDM"

    const-string v2, "GoogleAuthUtil.getToken threw GoogleAuthException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Lexe;

    const-string v2, "GoogleAuthUtil.getToken threw GoogleAuthException"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lexe;-><init>(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v1

    :cond_0
    invoke-super {p0}, Lsc;->i()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()[B
    .locals 1

    iget-object v0, p0, Lexg;->h:Lizs;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    return-object v0
.end method
