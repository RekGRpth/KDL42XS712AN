.class public final Ljbs;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:Ljava/util/List;

.field private d:Z

.field private e:Ljbu;

.field private f:Z

.field private g:Ljbu;

.field private h:Z

.field private i:Ljbu;

.field private j:Z

.field private k:Ljbu;

.field private l:Z

.field private m:Ljbu;

.field private n:Z

.field private o:Ljbu;

.field private p:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-object v1, p0, Ljbs;->e:Ljbu;

    const/4 v0, 0x1

    iput v0, p0, Ljbs;->b:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljbs;->c:Ljava/util/List;

    iput-object v1, p0, Ljbs;->g:Ljbu;

    iput-object v1, p0, Ljbs;->i:Ljbu;

    iput-object v1, p0, Ljbs;->k:Ljbu;

    iput-object v1, p0, Ljbs;->m:Ljbu;

    iput-object v1, p0, Ljbs;->o:Ljbu;

    const/4 v0, -0x1

    iput v0, p0, Ljbs;->p:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljbs;->p:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljbs;->b()I

    :cond_0
    iget v0, p0, Ljbs;->p:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbs;->d:Z

    iput-object v0, p0, Ljbs;->e:Ljbu;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v2, p0, Ljbs;->a:Z

    iput v0, p0, Ljbs;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iget-object v1, p0, Ljbs;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ljbs;->c:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ljbs;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_4
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbs;->f:Z

    iput-object v0, p0, Ljbs;->g:Ljbu;

    goto :goto_0

    :sswitch_5
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbs;->h:Z

    iput-object v0, p0, Ljbs;->i:Ljbu;

    goto :goto_0

    :sswitch_6
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbs;->j:Z

    iput-object v0, p0, Ljbs;->k:Ljbu;

    goto :goto_0

    :sswitch_7
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbs;->l:Z

    iput-object v0, p0, Ljbs;->m:Ljbu;

    goto :goto_0

    :sswitch_8
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Ljbs;->n:Z

    iput-object v0, p0, Ljbs;->o:Ljbu;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Ljbs;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbs;->e:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-boolean v0, p0, Ljbs;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljbs;->b:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_1
    iget-object v0, p0, Ljbs;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lizh;->a(II)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Ljbs;->f:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljbs;->g:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_3
    iget-boolean v0, p0, Ljbs;->h:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljbs;->i:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_4
    iget-boolean v0, p0, Ljbs;->j:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljbs;->k:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_5
    iget-boolean v0, p0, Ljbs;->l:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Ljbs;->m:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_6
    iget-boolean v0, p0, Ljbs;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Ljbs;->o:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_7
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Ljbs;->d:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    iget-object v2, p0, Ljbs;->e:Ljbu;

    invoke-static {v0, v2}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-boolean v2, p0, Ljbs;->a:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x2

    iget v3, p0, Ljbs;->b:I

    invoke-static {v2, v3}, Lizh;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    :goto_1
    iget-object v0, p0, Ljbs;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lizh;->a(I)I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_2

    :cond_0
    add-int v0, v2, v1

    iget-object v1, p0, Ljbs;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget-boolean v1, p0, Ljbs;->f:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    iget-object v2, p0, Ljbs;->g:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljbs;->h:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    iget-object v2, p0, Ljbs;->i:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljbs;->j:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x6

    iget-object v2, p0, Ljbs;->k:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Ljbs;->l:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x7

    iget-object v2, p0, Ljbs;->m:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Ljbs;->n:Z

    if-eqz v1, :cond_5

    const/16 v1, 0x8

    iget-object v2, p0, Ljbs;->o:Ljbu;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Ljbs;->p:I

    return v0

    :cond_6
    move v2, v0

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Ljbs;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
