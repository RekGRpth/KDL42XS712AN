.class public final Lijj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lijt;

.field private final d:Liid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lijj;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lijt;

    invoke-direct {v0, p1}, Lijt;-><init>(Landroid/content/Context;)V

    new-instance v1, Liib;

    invoke-direct {v1, p1}, Liib;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lijj;-><init>(Landroid/content/Context;Lijt;Liid;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lijt;Liid;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lijj;->b:Landroid/content/Context;

    iput-object p2, p0, Lijj;->c:Lijt;

    iput-object p3, p0, Lijj;->d:Liid;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)Z
    .locals 3

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requesting insistent sync for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "com.google.android.location.reporting.ACTION_INSISTENT_SYNC"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "label"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "reportingEnabled"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "historyEnabled"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Likh;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Boolean;ZLjava/lang/Boolean;)Z
    .locals 3

    const/4 v1, 0x4

    if-eqz p1, :cond_0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, p2, :cond_3

    const-string v0, "GCoreUlr"

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sync() insistent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " overriding server value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0

    :cond_3
    const-string v0, "GCoreUlr"

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sync() ignoring insistent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inconsistent with local value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    new-instance v0, Lijk;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " inconsistency: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lijk;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)Z
    .locals 10

    const/4 v6, 0x2

    const/4 v9, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lijj;->c:Lijt;

    invoke-virtual {v2, p1}, Lijt;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->s()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v1, "GCoreUlr"

    invoke-static {v1, v6}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ineligible, skipping sync: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->q()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v3, "GCoreUlr"

    invoke-static {v3, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GCore ULR sync started for account "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v3, p0, Lijj;->b:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/location/reporting/service/InitializerService;->a(Landroid/content/Context;Lcom/google/android/location/reporting/service/AccountConfig;)I

    move-result v3

    if-eq v3, v6, :cond_0

    sget-object v3, Lijj;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->c()J

    move-result-wide v4

    invoke-static {p1}, Liir;->a(Landroid/accounts/Account;)Liis;

    move-result-object v6

    const/4 v7, 0x1

    iput-boolean v7, v6, Liis;->f:Z

    invoke-virtual {v6, v4, v5}, Liis;->a(J)Liis;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->k()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v0, p0, Lijj;->d:Liid;

    invoke-interface {v0, p1}, Liid;->a(Landroid/accounts/Account;)Lihk;

    move-result-object v0

    invoke-static {v0}, Likf;->a(Lihk;)V

    iget-object v5, v0, Lihk;->a:Ljava/lang/Long;

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->h()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-gez v5, :cond_4

    const-string v4, "GCoreUlr"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sync() rejecting stale server values for account "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "; local: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->h()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    monitor-exit v3

    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget v2, v0, Lihk;->e:I

    if-ne v2, v1, :cond_5

    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Liis;->b(Z)Liis;

    move-result-object v2

    invoke-virtual {v2}, Liis;->a()Liir;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sync() server returned empty "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", will try uploading ours"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lijj;->c:Lijt;

    const-string v6, "sync_source_no_data"

    invoke-virtual {v5, v4, v2, v6}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    const-string v2, "GCoreUlr"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sync() server returned empty "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " for "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", will send ours"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    :try_start_1
    const-string v2, "GCoreUlr"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {p1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "GCoreUlr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Accepting server values for account "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ": "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-virtual {v4, v0}, Liis;->a(Lihk;)Liis;

    move-result-object v2

    const/4 v4, 0x3

    iput v4, v2, Liis;->h:I

    invoke-virtual {v2}, Liis;->a()Liir;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sync() server wins "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lijj;->c:Lijt;

    const-string v5, "sync_server_wins"

    invoke-virtual {v4, v0, v2, v5}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    goto/16 :goto_1

    :cond_7
    new-instance v5, Lihk;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->h()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->f()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->g()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lihk;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iget-object v6, p0, Lijj;->d:Liid;

    invoke-interface {v6, p1, v5}, Liid;->a(Landroid/accounts/Account;Lihk;)Lihk;

    move-result-object v6

    invoke-static {v6}, Likf;->b(Lihk;)V

    iget v7, v6, Lihk;->e:I

    if-ne v7, v9, :cond_8

    move v0, v1

    :cond_8
    if-nez v0, :cond_a

    const-string v0, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "Successfully uploaded changes for account "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ": "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " -> "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v4, v6}, Liis;->a(Lihk;)Liis;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Liis;->b(Z)Liis;

    move-result-object v0

    invoke-virtual {v0}, Liis;->a()Liir;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "sync() local wins "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lijj;->c:Lijt;

    const-string v5, "sync_local_wins"

    invoke-virtual {v4, v2, v0, v5}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    goto/16 :goto_1

    :cond_a
    const-string v0, "GCoreUlr"

    const/4 v7, 0x3

    invoke-static {v0, v7}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "GCoreUlr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Concurrent updates for account "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "; "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_b
    :try_start_2
    const-string v0, "reporting"

    iget-object v5, v6, Lihk;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->f()Z

    move-result v7

    invoke-static {v0, v5, v7, p2}, Lijj;->a(Ljava/lang/String;Ljava/lang/Boolean;ZLjava/lang/Boolean;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Liis;->i:Ljava/lang/Boolean;
    :try_end_2
    .catch Lijk; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    :try_start_3
    const-string v0, "history"

    iget-object v5, v6, Lihk;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/AccountConfig;->g()Z

    move-result v2

    invoke-static {v0, v5, v2, p3}, Lijj;->a(Ljava/lang/String;Ljava/lang/Boolean;ZLjava/lang/Boolean;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Liis;->j:Ljava/lang/Boolean;
    :try_end_3
    .catch Lijk; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_3
    :try_start_4
    iget-object v0, v6, Lihk;->a:Ljava/lang/Long;

    iput-object v0, v4, Liis;->k:Ljava/lang/Long;

    iget v0, v6, Lihk;->d:I

    invoke-virtual {v4, v0}, Liis;->a(I)Liis;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Liis;->b(Z)Liis;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "sync() concurrent "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lijj;->c:Lijt;

    invoke-virtual {v4}, Liis;->a()Liir;

    move-result-object v4

    const-string v5, "sync_concurrent"

    invoke-virtual {v2, v0, v4, v5}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v5, "GCoreUlr"

    const/4 v7, 0x6

    invoke-static {v5, v7}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_c

    const-string v5, "GCoreUlr"

    const-string v7, ""

    invoke-static {v5, v7, v0}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_c
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Liis;->a(Z)Liis;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v0, Liis;->i:Ljava/lang/Boolean;

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v2, "GCoreUlr"

    const/4 v5, 0x6

    invoke-static {v2, v5}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "GCoreUlr"

    const-string v5, ""

    invoke-static {v2, v5, v0}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_d
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Liis;->a(Z)Liis;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Liis;->j:Ljava/lang/Boolean;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Lijj;->b(Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lijj;->c:Lijt;

    invoke-virtual {v0, p1}, Lijt;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "Preference values still (or newly) dirty, retrying sync"

    invoke-static {v0, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lijj;->b(Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    move-result v0

    iget-object v1, p0, Lijj;->c:Lijt;

    invoke-virtual {v1, p1}, Lijt;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/AccountConfig;->k()Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    const-string v0, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GCoreUlr"

    const-string v2, "Preference values still dirty after two sync attempts"

    invoke-static {v0, v2}, Lijy;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {v1}, Likf;->a(Z)V

    :cond_2
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x0

    const-string v0, "label"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    const-string v1, "reportingEnabled"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "reportingEnabled"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    const-string v3, "historyEnabled"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "historyEnabled"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    :goto_1
    :try_start_0
    invoke-virtual {p0, v0, v1, v3}, Lijj;->a(Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    if-eqz v2, :cond_1

    const-string v1, "GCoreUlr"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    const-string v3, "Insistent sync failed, requesting regular sync with retry"

    invoke-static {v1, v3, v2}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    invoke-static {v0, v4}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-static {v2}, Likf;->b(Ljava/lang/Exception;)V

    :cond_1
    return-void

    :cond_2
    move-object v1, v2

    goto :goto_0

    :cond_3
    move-object v3, v2

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_2
.end method
