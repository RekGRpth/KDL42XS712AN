.class public final Lgxl;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgxf;
.implements Lgyq;


# instance fields
.field a:Lgxs;

.field b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->c:Z

    return-void
.end method

.method public static a([I)Lgxl;
    .locals 3

    new-instance v0, Lgxl;

    invoke-direct {v0}, Lgxl;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    invoke-virtual {v0, v1}, Lgxl;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lgxl;->a:Lgxs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgxl;->a:Lgxs;

    iget-boolean v1, p0, Lgxl;->c:Z

    invoke-virtual {v0, v1}, Lgxs;->a(Z)V

    iget-object v0, p0, Lgxl;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgxl;->c:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private b(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [Lgyo;

    iget-object v0, p0, Lgxl;->a:Lgxs;

    aput-object v0, v4, v2

    iget-object v0, p0, Lgxl;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v0, v4, v1

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgxl;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgxl;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const v5, 0x7f0a0314    # com.google.android.gms.R.id.instrument_entry_fragment_holder

    const v0, 0x7f040132    # com.google.android.gms.R.layout.wallet_fragment_local_add_instrument

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    invoke-virtual {p0}, Lgxl;->k()Lu;

    move-result-object v0

    invoke-virtual {v0, v5}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgxs;

    iput-object v0, p0, Lgxl;->a:Lgxs;

    iget-object v0, p0, Lgxl;->a:Lgxs;

    if-nez v0, :cond_0

    new-instance v0, Lgxs;

    invoke-direct {v0}, Lgxs;-><init>()V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "disallowedCreditCardTypes"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    invoke-virtual {v0, v3}, Lgxs;->g(Landroid/os/Bundle;)V

    iput-object v0, p0, Lgxl;->a:Lgxs;

    invoke-virtual {p0}, Lgxl;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v2, p0, Lgxl;->a:Lgxs;

    invoke-virtual {v0, v5, v2}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const v0, 0x7f0a0331    # com.google.android.gms.R.id.card_holder_name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgxl;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {p0}, Lgxl;->b()V

    return-object v1
.end method

.method public final a()Linv;
    .locals 3

    iget-object v0, p0, Lgxl;->a:Lgxs;

    invoke-virtual {v0}, Lgxs;->a()Linv;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Linv;

    iget-object v1, v0, Linv;->b:Lint;

    new-instance v2, Lixo;

    invoke-direct {v2}, Lixo;-><init>()V

    iput-object v2, v1, Lint;->d:Lixo;

    iget-object v1, v0, Linv;->b:Lint;

    iget-object v1, v1, Lint;->d:Lixo;

    iget-object v2, p0, Lgxl;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lixo;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lgxl;->c:Z

    invoke-direct {p0}, Lgxl;->b()V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgxl;->c:Z

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "enabled"

    iget-boolean v1, p0, Lgxl;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lgxl;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgxl;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgxl;->a:Lgxs;

    invoke-virtual {v0}, Lgxs;->i()Z

    move-result v0

    goto :goto_0
.end method
