.class public abstract Liri;
.super Litc;
.source "SourceFile"


# instance fields
.field private a:Lirk;

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Litc;-><init>()V

    sget-object v0, Lirk;->b:Lirk;

    iput-object v0, p0, Liri;->a:Lirk;

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method protected final c()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lirk;->c:Lirk;

    iput-object v0, p0, Liri;->a:Lirk;

    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Liri;->a:Lirk;

    sget-object v3, Lirk;->d:Lirk;

    if-eq v2, v3, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    sget-object v2, Lirj;->a:[I

    iget-object v3, p0, Liri;->a:Lirk;

    invoke-virtual {v3}, Lirk;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v2, Lirk;->d:Lirk;

    iput-object v2, p0, Liri;->a:Lirk;

    invoke-virtual {p0}, Liri;->a()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Liri;->b:Ljava/lang/Object;

    iget-object v2, p0, Liri;->a:Lirk;

    sget-object v3, Lirk;->c:Lirk;

    if-eq v2, v3, :cond_2

    sget-object v0, Lirk;->a:Lirk;

    iput-object v0, p0, Liri;->a:Lirk;

    move v0, v1

    :cond_2
    :goto_1
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Liri;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lirk;->b:Lirk;

    iput-object v0, p0, Liri;->a:Lirk;

    iget-object v0, p0, Liri;->b:Ljava/lang/Object;

    return-object v0
.end method
