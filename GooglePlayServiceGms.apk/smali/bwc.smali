.class public final Lbwc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcfz;

.field public final b:Lcos;

.field public final c:Lcby;

.field public final d:Ljava/util/Set;

.field private final e:Lcoy;

.field private final f:Lbtd;

.field private final g:Lcll;

.field private final h:Lbxa;

.field private final i:Lbvp;


# direct methods
.method public constructor <init>(Lcoy;)V
    .locals 2

    new-instance v0, Lbxa;

    new-instance v1, Lbvu;

    invoke-direct {v1, p1}, Lbvu;-><init>(Lcoy;)V

    invoke-direct {v0, p1, v1}, Lbxa;-><init>(Lcoy;Lbvu;)V

    invoke-direct {p0, p1, v0}, Lbwc;-><init>(Lcoy;Lbxa;)V

    return-void
.end method

.method private constructor <init>(Lcoy;Lbxa;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbwc;->d:Ljava/util/Set;

    iput-object p1, p0, Lbwc;->e:Lcoy;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbwc;->a:Lcfz;

    invoke-virtual {p1}, Lcoy;->b()Lcos;

    move-result-object v0

    iput-object v0, p0, Lbwc;->b:Lcos;

    invoke-virtual {p1}, Lcoy;->i()Lcby;

    move-result-object v0

    iput-object v0, p0, Lbwc;->c:Lcby;

    invoke-virtual {p1}, Lcoy;->j()Lcll;

    move-result-object v0

    iput-object v0, p0, Lbwc;->g:Lcll;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxa;

    iput-object v0, p0, Lbwc;->h:Lbxa;

    invoke-virtual {p1}, Lcoy;->m()Lbtd;

    move-result-object v0

    iput-object v0, p0, Lbwc;->f:Lbtd;

    new-instance v0, Lbvp;

    invoke-direct {v0, p1}, Lbvp;-><init>(Lcoy;)V

    iput-object v0, p0, Lbwc;->i:Lbvp;

    return-void
.end method

.method private a(Landroid/content/SyncResult;Lbwz;)Z
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lbvt;

    iget-object v1, p0, Lbwc;->e:Lcoy;

    invoke-direct {v0, v1}, Lbvt;-><init>(Lcoy;)V

    invoke-interface {p2, v0, p1}, Lbwz;->a(Lbvt;Landroid/content/SyncResult;)V

    invoke-virtual {v0, p1}, Lbvt;->a(Landroid/content/SyncResult;)V

    invoke-interface {p2, p1, v2}, Lbwz;->a(Landroid/content/SyncResult;Z)V

    return v2
.end method

.method private a(Lcfc;ILandroid/content/SyncResult;)Z
    .locals 3

    sget-object v0, Lbth;->a:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwc;->h:Lbxa;

    new-instance v1, Lbwv;

    iget-object v2, v0, Lbxa;->a:Lcoy;

    iget-object v0, v0, Lbxa;->b:Lbvu;

    invoke-direct {v1, v2, p1, p2, v0}, Lbwv;-><init>(Lcoy;Lcfc;ILbvu;)V

    invoke-virtual {v1}, Lbwv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p3, v1}, Lbwc;->a(Landroid/content/SyncResult;Lbwz;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(ZLcfc;Landroid/content/SyncResult;ILcfe;)Z
    .locals 10

    const/4 v8, 0x1

    const/4 v6, 0x0

    sget-object v0, Lbqs;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {p2}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    invoke-virtual {v0}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    iget-object v0, p0, Lbwc;->g:Lcll;

    invoke-virtual {p5}, Lcfe;->h()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    int-to-long v4, v9

    invoke-interface/range {v0 .. v5}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;JJ)Lclj;

    move-result-object v1

    sget-object v0, Lbth;->a:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lbwc;->i:Lbvp;

    iget-object v0, v2, Lbvp;->a:Lcfz;

    invoke-interface {v0, p2}, Lcfz;->e(Lcfc;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, v2, Lbvp;->a:Lcfz;

    invoke-interface {v0, p2, v4, v5}, Lcfz;->a(Lcfc;J)Lbsp;

    move-result-object v0

    :try_start_0
    invoke-virtual {v2, v0}, Lbvp;->b(Lbsp;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "AppDataFolderIdUpdater"

    const-string v5, "Failed to get real appData folder from server"

    invoke-static {v4, v0, v5}, Lcbv;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Lclj;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbwc;->f:Lbtd;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "Error fetching remainingChangestamps"

    invoke-virtual {v0, v1, v2, v3}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    :cond_1
    :goto_1
    return v0

    :cond_2
    invoke-virtual {p5}, Lcfe;->a()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-interface {v1}, Lclj;->b()J

    move-result-wide v2

    int-to-long v4, v9

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    :cond_3
    move v5, v8

    :goto_2
    if-nez v5, :cond_4

    if-nez p1, :cond_6

    :cond_4
    move v0, v8

    :goto_3
    if-eqz v0, :cond_7

    invoke-interface {v1}, Lclj;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v6

    iget-object v2, p0, Lbwc;->h:Lbxa;

    new-instance v0, Lbwx;

    iget-object v1, v2, Lbxa;->a:Lcoy;

    iget-object v2, v2, Lbxa;->b:Lbvu;

    move-object v3, p2

    move v4, p4

    invoke-direct/range {v0 .. v7}, Lbwx;-><init>(Lcoy;Lbvu;Lcfc;IZIZ)V

    invoke-direct {p0, p3, v0}, Lbwc;->a(Landroid/content/SyncResult;Lbwz;)Z

    move-result v0

    :goto_4
    if-eqz v0, :cond_1

    iget-object v1, p0, Lbwc;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->c()V

    :try_start_1
    iget-object v1, p0, Lbwc;->a:Lcfz;

    iget-object v2, p2, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Lcfe;->a(Ljava/util/Date;)V

    invoke-virtual {v1}, Lcfe;->k()V

    iget-object v1, p0, Lbwc;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lbwc;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    goto :goto_1

    :cond_5
    move v5, v6

    goto :goto_2

    :cond_6
    move v0, v6

    goto :goto_3

    :cond_7
    invoke-interface {v1}, Lclj;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_9

    invoke-direct {p0, p2, p4, p3}, Lbwc;->a(Lcfc;ILandroid/content/SyncResult;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbwc;->h:Lbxa;

    new-instance v1, Lbwu;

    iget-object v2, v0, Lbxa;->a:Lcoy;

    iget-object v0, v0, Lbxa;->b:Lbvu;

    invoke-direct {v1, v2, v0, p2}, Lbwu;-><init>(Lcoy;Lbvu;Lcfc;)V

    invoke-direct {p0, p3, v1}, Lbwc;->a(Landroid/content/SyncResult;Lbwz;)Z

    move-result v0

    goto :goto_4

    :cond_8
    move v0, v6

    goto :goto_4

    :cond_9
    invoke-direct {p0, p2, p4, p3}, Lbwc;->a(Lcfc;ILandroid/content/SyncResult;)Z

    move-result v0

    goto :goto_4

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbwc;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0
.end method
