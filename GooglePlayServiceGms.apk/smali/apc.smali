.class public final Lapc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lapc;

.field public static final b:Lapc;

.field public static final c:Lapc;

.field public static final d:Lapc;

.field private static final g:Ljava/lang/String;


# instance fields
.field public final e:Laso;

.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lapc;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lapc;->g:Ljava/lang/String;

    sget-object v0, Laso;->b:Laso;

    invoke-static {v0}, Lapc;->a(Laso;)Lapc;

    move-result-object v0

    sput-object v0, Lapc;->a:Lapc;

    sget-object v0, Laso;->a:Laso;

    invoke-static {v0}, Lapc;->a(Laso;)Lapc;

    move-result-object v0

    sput-object v0, Lapc;->b:Lapc;

    sget-object v0, Laso;->m:Laso;

    invoke-static {v0}, Lapc;->a(Laso;)Lapc;

    move-result-object v0

    sput-object v0, Lapc;->c:Lapc;

    sget-object v0, Laso;->n:Laso;

    invoke-static {v0}, Lapc;->a(Laso;)Lapc;

    move-result-object v0

    sput-object v0, Lapc;->d:Lapc;

    return-void
.end method

.method private constructor <init>(Laso;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lapc;->e:Laso;

    iput p2, p0, Lapc;->f:I

    return-void
.end method

.method public static a(Laso;)Lapc;
    .locals 3

    sget-object v0, Lapc;->g:Ljava/lang/String;

    sget-object v0, Lapd;->a:[I

    invoke-virtual {p0}, Laso;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lapc;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " No resource configured for status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Lapc;

    invoke-direct {v1, p0, v0}, Lapc;-><init>(Laso;I)V

    return-object v1

    :pswitch_0
    const v0, 0x7f0b04c2    # com.google.android.gms.R.string.auth_login_activity_loginfail_text_pwonly

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b04e1    # com.google.android.gms.R.string.auth_error_invalid_second_factor

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b04b1    # com.google.android.gms.R.string.auth_error_account_not_verified

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0b04ac    # com.google.android.gms.R.string.auth_error_account_disabled

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0b04b6    # com.google.android.gms.R.string.auth_error_bad_password

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0b04ab    # com.google.android.gms.R.string.auth_account_already_has_gmail

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0b04b7    # com.google.android.gms.R.string.auth_error_bad_username

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0b04b0    # com.google.android.gms.R.string.auth_error_login_failed

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0b04b4    # com.google.android.gms.R.string.auth_error_not_logged_in

    goto :goto_0

    :pswitch_9
    const v0, 0x7f0b04b3    # com.google.android.gms.R.string.auth_doesnt_use_gmail

    goto :goto_0

    :pswitch_a
    const v0, 0x7f0b04b5    # com.google.android.gms.R.string.auth_error_username_unavailable

    goto :goto_0

    :pswitch_b
    const v0, 0x7f0b049f    # com.google.android.gms.R.string.auth_plus_failure_text

    goto :goto_0

    :pswitch_c
    const v0, 0x7f0b04a0    # com.google.android.gms.R.string.auth_invalid_scope

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static a(Landroid/content/Intent;)Laso;
    .locals 1

    if-nez p0, :cond_1

    sget-object v0, Laso;->a:Laso;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Laso;->M:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Laso;->a:Laso;

    goto :goto_0

    :cond_2
    invoke-static {v0}, Laso;->a(Ljava/lang/String;)Laso;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Laso;->f:Laso;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Laso;
    .locals 4

    if-nez p0, :cond_1

    sget-object v0, Laso;->a:Laso;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Laso;->a(Ljava/lang/String;)Laso;

    move-result-object v0

    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lapc;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Status from wire: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    sget-object v0, Laso;->f:Laso;

    goto :goto_0
.end method

.method public static a(Lorg/json/JSONObject;)Laso;
    .locals 1

    sget-object v0, Laso;->N:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lapc;->b(Ljava/lang/String;)Laso;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Laso;
    .locals 1

    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Laso;->A:Laso;

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-static {p0}, Laso;->valueOf(Ljava/lang/String;)Laso;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Laso;->A:Laso;

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Laso;->M:Ljava/lang/String;

    iget-object v1, p0, Lapc;->e:Laso;

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method
