.class public interface abstract Lcho;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;)Landroid/content/IntentSender;
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;)Landroid/content/IntentSender;
.end method

.method public abstract a(Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/AddEventListenerRequest;Lcht;Ljava/lang/String;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/CloseContentsRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/CreateFileRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/CreateFolderRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/DisconnectRequest;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/GetMetadataRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/ListParentsRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OpenContentsRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/QueryRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;Lcht;Ljava/lang/String;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/TrashResourceRequest;Lchq;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;Lchq;)V
.end method

.method public abstract b(Lchq;)V
.end method
