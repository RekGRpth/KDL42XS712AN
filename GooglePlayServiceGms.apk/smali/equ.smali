.class public interface abstract Lequ;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Landroid/location/Location;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract a(JZLandroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;Leqr;Ljava/lang/String;)V
.end method

.method public abstract a(Landroid/location/Location;)V
.end method

.method public abstract a(Landroid/location/Location;I)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Leqc;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Leqc;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/location/places/internal/PlacesParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
.end method

.method public abstract a(Leqc;)V
.end method

.method public abstract a(Leqr;Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
.end method

.method public abstract a(Ljava/util/List;)V
.end method

.method public abstract a(Ljava/util/List;Landroid/app/PendingIntent;Leqr;Ljava/lang/String;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a([Ljava/lang/String;Leqr;Ljava/lang/String;)V
.end method

.method public abstract b(Ljava/lang/String;)Lcom/google/android/gms/location/LocationStatus;
.end method

.method public abstract b(Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
.end method
