.class public final Lamc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lalr;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Laku;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Laku;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lamc;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lamc;->b:Laku;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lakk;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lamc;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p2, p1, v0}, Lakk;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Laln; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    :goto_0
    :try_start_1
    iget-object v1, p0, Lamc;->b:Laku;

    invoke-interface {v1, v0}, Laku;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "WipeStateOp"

    const-string v2, "Auth error while performing operation, requesting reconnect"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x2

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "WipeStateOp"

    invoke-virtual {v0}, Laln;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v0}, Laln;->a()I

    move-result v0

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "WipeStateOp"

    const-string v2, "Runtime exception while performing operation"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v1, "WipeStateOp"

    const-string v2, "Killing (on development devices) due to RuntimeException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x1

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_1
.end method
