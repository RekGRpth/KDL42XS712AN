.class public final Lgfl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;Landroid/os/Parcel;I)V
    .locals 8

    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbkr;->a(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->b()Ljava/util/Set;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->c()I

    move-result v2

    invoke-static {p1, v4, v2}, Lbkr;->b(Landroid/os/Parcel;II)V

    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->g()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v3, v2, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->i()Z

    move-result v2

    invoke-static {p1, v5, v2}, Lbkr;->a(Landroid/os/Parcel;IZ)V

    :cond_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v6, v2, v4}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    :cond_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->n()Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;

    move-result-object v2

    invoke-static {p1, v7, v2, p2, v4}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    :cond_4
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->p()Z

    move-result v3

    invoke-static {p1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;IZ)V

    :cond_5
    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->s()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_6
    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->u()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    :cond_7
    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->w()Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    move-result-object v3

    invoke-static {p1, v2, v3, p2, v4}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    :cond_8
    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;->y()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    :cond_9
    invoke-static {p1, v0}, Lbkr;->b(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 14

    const/4 v7, 0x0

    const/4 v11, 0x0

    invoke-static {p1}, Lbkp;->a(Landroid/os/Parcel;)I

    move-result v12

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    move-object v10, v11

    move-object v9, v11

    move-object v8, v11

    move-object v6, v11

    move-object v5, v11

    move v4, v7

    move-object v3, v11

    move v2, v7

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v12, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const v13, 0xffff

    and-int/2addr v13, v0

    packed-switch v13, :pswitch_data_0

    :pswitch_0
    invoke-static {p1, v0}, Lbkp;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_1
    invoke-static {p1, v0}, Lbkp;->g(Landroid/os/Parcel;I)I

    move-result v2

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    sget-object v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ErrorsEntity;->CREATOR:Lgfx;

    invoke-static {p1, v0, v3}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_3
    invoke-static {p1, v0}, Lbkp;->d(Landroid/os/Parcel;I)Z

    move-result v4

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_4
    invoke-static {p1, v0}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_5
    sget-object v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;->CREATOR:Lgfz;

    invoke-static {p1, v0, v6}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;

    const/4 v6, 0x6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v6, v0

    goto :goto_0

    :pswitch_6
    invoke-static {p1, v0}, Lbkp;->d(Landroid/os/Parcel;I)Z

    move-result v7

    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_7
    sget-object v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$OptionsEntity;->CREATOR:Lggb;

    invoke-static {p1, v0, v8}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v8

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_8
    invoke-static {p1, v0}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v9

    const/16 v0, 0x9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_9
    sget-object v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;->CREATOR:Lggc;

    invoke-static {p1, v0, v10}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    const/16 v10, 0xa

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v1, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v10, v0

    goto/16 :goto_0

    :pswitch_a
    invoke-static {p1, v0}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v11

    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v12, :cond_1

    new-instance v0, Lbkq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Overread allowed size end="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lbkq;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;-><init>(Ljava/util/Set;ILjava/util/List;ZLjava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;ZLjava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;Ljava/lang/String;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;

    return-object v0
.end method
