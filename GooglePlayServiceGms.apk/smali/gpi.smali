.class public final Lgpi;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lal;
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lfrc;


# instance fields
.field private a:Lgpw;

.field private b:Lfqr;

.field private c:Lfpt;

.field private d:Lfpw;

.field private e:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private a(Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a02c1    # com.google.android.gms.R.id.acl_fragment_container

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lgpi;->j()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_1

    const v0, 0x7f0c011b    # com.google.android.gms.R.color.plus_sharebox_search_background

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0c00fc    # com.google.android.gms.R.color.plus_solid_white

    goto :goto_0
.end method


# virtual methods
.method public final J()Z
    .locals 1

    iget-object v0, p0, Lgpi;->d:Lfpw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpi;->d:Lfpw;

    iget-boolean v0, v0, Landroid/support/v4/app/Fragment;->I:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lgpi;->K()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final K()V
    .locals 2

    invoke-virtual {p0}, Lgpi;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgpi;->c:Lfpt;

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    iget-object v1, p0, Lgpi;->d:Lfpw;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lgpi;->e:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    iget-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lgpi;->a:Lgpw;

    invoke-interface {v0}, Lgpw;->h()Lgpy;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lgpy;->c(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgpi;->a(Z)V

    return-void
.end method

.method public final L()I
    .locals 1

    iget-object v0, p0, Lgpi;->c:Lfpt;

    invoke-virtual {v0}, Lfqq;->U()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lfqe;

    invoke-virtual {v0}, Lfqe;->g()I

    move-result v0

    return v0
.end method

.method public final M()I
    .locals 1

    iget-object v0, p0, Lgpi;->c:Lfpt;

    invoke-virtual {v0}, Lfqq;->U()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lfqe;

    invoke-virtual {v0}, Lfqe;->h()I

    move-result v0

    return v0
.end method

.method public final M_()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lgpi;->a:Lgpw;

    invoke-virtual {p0}, Lgpi;->s()Lak;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lak;->a(I)V

    return-void
.end method

.method public final N()I
    .locals 1

    iget-object v0, p0, Lgpi;->c:Lfpt;

    invoke-virtual {v0}, Lfqq;->U()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lfqe;

    invoke-virtual {v0}, Lfqe;->i()I

    move-result v0

    return v0
.end method

.method public final N_()V
    .locals 0

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040108    # com.google.android.gms.R.layout.plus_sharebox_acl_selection_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/os/Bundle;)Lcb;
    .locals 6

    if-nez p1, :cond_0

    new-instance v0, Lfrm;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v2, p0, Lgpi;->a:Lgpw;

    invoke-interface {v2}, Lgpw;->g()Lgpr;

    move-result-object v2

    invoke-virtual {v2}, Lgpr;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpi;->a:Lgpw;

    invoke-interface {v3}, Lgpw;->j()Lgpx;

    move-result-object v3

    invoke-virtual {v3}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lgpi;->a:Lgpw;

    invoke-interface {v4}, Lgpw;->j()Lgpx;

    move-result-object v4

    iget-object v4, v4, Lgpx;->m:Ljava/lang/String;

    iget-object v5, p0, Lgpi;->a:Lgpw;

    invoke-interface {v5}, Lgpw;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lfrm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lgpw;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lgpw;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lgpw;

    iput-object v0, p0, Lgpi;->a:Lgpw;

    instance-of v0, p1, Lfqr;

    if-eqz v0, :cond_1

    check-cast p1, Lfqr;

    iput-object p1, p0, Lgpi;->b:Lfqr;

    :cond_1
    return-void
.end method

.method public final synthetic a(Lcb;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lfed;

    iget v0, p1, Lcb;->m:I

    if-nez v0, :cond_0

    check-cast p1, Lfrm;

    invoke-virtual {p1}, Lfrm;->b()Lbbo;

    move-result-object v0

    invoke-virtual {v0}, Lbbo;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lfed;->a()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lfed;->b(I)Lfec;

    move-result-object v0

    iget-object v1, p0, Lgpi;->c:Lfpt;

    invoke-virtual {v1, v0}, Lfpt;->a(Lfec;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lgpi;->a:Lgpw;

    invoke-interface {v1, v0}, Lgpw;->a(Lbbo;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lgpi;->b:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    iget-object v0, v0, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lblc;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpi;->c:Lfpt;

    iget-boolean v0, v0, Landroid/support/v4/app/Fragment;->I:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgpi;->K()V

    :cond_0
    return-void
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lgpi;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgpi;->d:Lfpw;

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    iget-object v1, p0, Lgpi;->c:Lfpt;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    iget-object v0, p0, Lgpi;->a:Lgpw;

    invoke-interface {v0}, Lgpw;->h()Lgpy;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lgpy;->c(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgpi;->a(Z)V

    iget-object v0, p0, Lgpi;->d:Lfpw;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfpw;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lgpi;->K()V

    goto :goto_0
.end method

.method public final b()V
    .locals 18

    invoke-virtual/range {p0 .. p0}, Lgpi;->k()Lu;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lu;->a()Lag;

    move-result-object v17

    const-string v1, "selection"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lfpt;

    move-object/from16 v0, p0

    iput-object v1, v0, Lgpi;->c:Lfpt;

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->c:Lfpt;

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->g()Lgpr;

    move-result-object v1

    invoke-virtual {v1}, Lgpr;->M()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->g()I

    move-result v1

    if-lez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->g()Lgpr;

    move-result-object v1

    invoke-virtual {v1}, Lgpr;->M()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->h()I

    move-result v12

    const/4 v13, 0x0

    :goto_0
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->j()Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->q:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->j()Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->q:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v15

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->g()Lgpr;

    move-result-object v1

    invoke-virtual {v1}, Lgpr;->b()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lgpi;->a:Lgpw;

    invoke-interface {v2}, Lgpw;->j()Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lgpi;->a:Lgpw;

    invoke-interface {v3}, Lgpw;->j()Lgpx;

    move-result-object v3

    iget-boolean v3, v3, Lgpx;->j:Z

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lgpi;->a:Lgpw;

    invoke-interface {v8}, Lgpw;->j()Lgpx;

    move-result-object v8

    iget-object v8, v8, Lgpx;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lgpi;->a:Lgpw;

    invoke-interface {v9}, Lgpw;->getCallingPackage()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lgpi;->a:Lgpw;

    invoke-interface {v10}, Lgpw;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lgpi;->a:Lgpw;

    invoke-interface {v11}, Lgpw;->j()Lgpx;

    move-result-object v11

    iget-boolean v11, v11, Lgpx;->k:Z

    const/4 v14, 0x0

    invoke-static/range {v1 .. v15}, Lfpt;->a(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIIILjava/util/List;)Lfpt;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lgpi;->c:Lfpt;

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->c:Lfpt;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lfpt;->a(Z)V

    const v1, 0x7f0a02c1    # com.google.android.gms.R.id.acl_fragment_container

    move-object/from16 v0, p0

    iget-object v2, v0, Lgpi;->c:Lfpt;

    const-string v3, "selection"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v2, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_1
    const-string v1, "search"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lfpw;

    move-object/from16 v0, p0

    iput-object v1, v0, Lgpi;->d:Lfpw;

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->d:Lfpw;

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->g()Lgpr;

    move-result-object v1

    invoke-virtual {v1}, Lgpr;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->j()Lgpx;

    move-result-object v1

    invoke-virtual {v1}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/app/Fragment;->C:Lo;

    move-object/from16 v0, p0

    iget-object v4, v0, Lgpi;->a:Lgpw;

    invoke-interface {v4}, Lgpw;->j()Lgpx;

    move-result-object v4

    iget-object v4, v4, Lgpx;->f:Ljava/lang/String;

    invoke-static {v1, v4}, Lgpq;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lgpi;->a:Lgpw;

    invoke-interface {v4}, Lgpw;->j()Lgpx;

    move-result-object v4

    iget-object v4, v4, Lgpx;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgpi;->a:Lgpw;

    invoke-interface {v5}, Lgpw;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v1, v4, v5}, Lgpk;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lfpw;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lgpi;->d:Lfpw;

    const v1, 0x7f0a02c1    # com.google.android.gms.R.id.acl_fragment_container

    move-object/from16 v0, p0

    iget-object v2, v0, Lgpi;->d:Lfpw;

    const-string v3, "search"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v2, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->c:Lfpt;

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->d:Lfpw;

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    invoke-virtual/range {v17 .. v17}, Lag;->d()I

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lgpi;->s()Lak;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v1, v2, v3, v0}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    :cond_3
    return-void

    :cond_4
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lgpi;->a:Lgpw;

    invoke-interface {v1}, Lgpw;->g()Lgpr;

    move-result-object v1

    invoke-virtual {v1}, Lgpr;->M()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->h()I

    move-result v13

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final c(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Z)V

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lgpi;->K()V

    iget-object v0, p0, Lgpi;->a:Lgpw;

    invoke-interface {v0}, Lgpw;->h()Lgpy;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgpy;->a(Z)V

    iget-object v0, p0, Lgpi;->b:Lfqr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpi;->b:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpi;->b:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->a(Lfrc;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgpi;->b:Lfqr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpi;->b:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpi;->b:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->b(Lfrc;)V

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lgpi;->c:Lfpt;

    if-eqz v0, :cond_0

    const-string v0, "selection_hidden"

    iget-object v1, p0, Lgpi;->c:Lfpt;

    iget-boolean v1, v1, Landroid/support/v4/app/Fragment;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lgpi;->d:Lfpw;

    if-eqz v0, :cond_1

    const-string v0, "search_hidden"

    iget-object v1, p0, Lgpi;->d:Lfpw;

    iget-boolean v1, v1, Landroid/support/v4/app/Fragment;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method public final f()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->f()V

    iget-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public final h(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->h(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a02c0    # com.google.android.gms.R.id.plus_sharebox_searchbox

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    iget-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    const v1, 0x7f0b03c5    # com.google.android.gms.R.string.plus_sharebox_search_hint

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    iget-object v0, p0, Lgpi;->e:Landroid/widget/EditText;

    const/16 v1, 0x61

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    invoke-virtual {p0}, Lgpi;->k()Lu;

    move-result-object v1

    const-string v0, "selection"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lfpt;

    iput-object v0, p0, Lgpi;->c:Lfpt;

    const-string v0, "search"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lfpw;

    iput-object v0, p0, Lgpi;->d:Lfpw;

    if-eqz p1, :cond_3

    iget-object v0, p0, Lgpi;->b:Lfqr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpi;->b:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpi;->b:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->a(Lfrc;)V

    :cond_0
    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgpi;->c:Lfpt;

    if-eqz v1, :cond_1

    const-string v1, "selection_hidden"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgpi;->c:Lfpt;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    :cond_1
    :goto_0
    iget-object v1, p0, Lgpi;->d:Lfpw;

    if-eqz v1, :cond_2

    const-string v1, "search_hidden"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lgpi;->d:Lfpw;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    :cond_2
    :goto_1
    invoke-virtual {v0}, Lag;->f()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lag;->c()I

    :cond_3
    return-void

    :cond_4
    iget-object v1, p0, Lgpi;->c:Lfpt;

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lgpi;->a(Z)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lgpi;->d:Lfpw;

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lgpi;->a(Z)V

    goto :goto_1
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lgpi;->e:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
