.class public final Lauc;
.super Landroid/widget/ScrollView;
.source "SourceFile"


# instance fields
.field private a:Laud;

.field private b:I

.field private c:Z

.field private d:I

.field private e:Z


# direct methods
.method private a()V
    .locals 2

    iget-object v0, p0, Lauc;->a:Laud;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lauc;->c:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lauc;->getScrollY()I

    move-result v0

    iget v1, p0, Lauc;->b:I

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lauc;->a:Laud;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lauc;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lauc;->e:Z

    iget-object v0, p0, Lauc;->a:Laud;

    goto :goto_0
.end method


# virtual methods
.method protected final onLayout(ZIIII)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lauc;->c:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    sub-int v0, p5, p3

    iget v1, p0, Lauc;->d:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lauc;->c:Z

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    invoke-virtual {p0, v2}, Lauc;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2}, Lauc;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr v0, p5

    add-int/2addr v0, p3

    invoke-virtual {p0}, Lauc;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lauc;->b:I

    :cond_1
    sub-int v0, p5, p3

    if-lez v0, :cond_2

    invoke-direct {p0}, Lauc;->a()V

    :cond_2
    sub-int v0, p5, p3

    iput v0, p0, Lauc;->d:I

    return-void
.end method

.method protected final onScrollChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    if-eq p4, p2, :cond_0

    invoke-direct {p0}, Lauc;->a()V

    :cond_0
    return-void
.end method
