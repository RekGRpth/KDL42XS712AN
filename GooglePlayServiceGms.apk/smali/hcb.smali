.class public Lhcb;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lhcb;->a:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhcb;->b:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;)V
    .locals 0

    return-void
.end method

.method public a(Lioj;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    return-void
.end method

.method public a(Liot;)V
    .locals 0

    return-void
.end method

.method public a(Liov;)V
    .locals 0

    return-void
.end method

.method public a(Lipg;)V
    .locals 0

    return-void
.end method

.method public a(Lipl;)V
    .locals 0

    return-void
.end method

.method public a(Lipn;)V
    .locals 0

    return-void
.end method

.method public a(Lipv;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    return-void
.end method

.method public a(Lipy;)V
    .locals 0

    return-void
.end method

.method public a(Lizz;)V
    .locals 0

    return-void
.end method

.method public a(Ljap;)V
    .locals 0

    return-void
.end method

.method public a(Ljar;)V
    .locals 0

    return-void
.end method

.method public a(Ljav;J)V
    .locals 0

    return-void
.end method

.method public a(Ljax;)V
    .locals 0

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lhcb;->b:Z

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public b(Lioj;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    return-void
.end method

.method public b(Lipv;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lhcb;->a:I

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget-boolean v0, p0, Lhcb;->b:Z

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p0, Lhcb;->a:I

    if-gt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lhcb;->a:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "PaymentServiceResponseH"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown ServerResponse type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lhcb;->b()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lhcb;->a()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    invoke-virtual {p0}, Lhcb;->d()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_2

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-nez v2, :cond_3

    :cond_2
    invoke-virtual {p0, v0}, Lhcb;->a(Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;)V

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-virtual {p0, v0, v1}, Lhcb;->a(Lioj;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_4

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lioj;

    if-nez v2, :cond_5

    :cond_4
    invoke-virtual {p0, v0}, Lhcb;->a(Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;)V

    goto :goto_0

    :cond_5
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lioj;

    invoke-virtual {p0, v0, v1}, Lhcb;->b(Lioj;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_6

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lipv;

    if-nez v2, :cond_7

    :cond_6
    invoke-virtual {p0, v0}, Lhcb;->a(Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lipv;

    invoke-virtual {p0, v0, v1}, Lhcb;->a(Lipv;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto/16 :goto_0

    :pswitch_6
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_8

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lipv;

    if-nez v2, :cond_9

    :cond_8
    invoke-virtual {p0, v0}, Lhcb;->a(Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lipv;

    invoke-virtual {p0, v0, v1}, Lhcb;->b(Lipv;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto/16 :goto_0

    :pswitch_7
    invoke-virtual {p0}, Lhcb;->c()V

    goto/16 :goto_0

    :pswitch_8
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljax;

    invoke-virtual {p0, v0}, Lhcb;->a(Ljax;)V

    goto/16 :goto_0

    :pswitch_9
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lizz;

    invoke-virtual {p0, v0}, Lhcb;->a(Lizz;)V

    goto/16 :goto_0

    :pswitch_a
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lipy;

    invoke-virtual {p0, v0}, Lhcb;->a(Lipy;)V

    goto/16 :goto_0

    :pswitch_b
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljap;

    invoke-virtual {p0, v0}, Lhcb;->a(Ljap;)V

    goto/16 :goto_0

    :pswitch_c
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljav;

    invoke-virtual {p0, v0, v2, v3}, Lhcb;->a(Ljav;J)V

    goto/16 :goto_0

    :pswitch_d
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljar;

    invoke-virtual {p0, v0}, Lhcb;->a(Ljar;)V

    goto/16 :goto_0

    :pswitch_e
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lipl;

    invoke-virtual {p0, v0}, Lhcb;->a(Lipl;)V

    goto/16 :goto_0

    :pswitch_f
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lipn;

    invoke-virtual {p0, v0}, Lhcb;->a(Lipn;)V

    goto/16 :goto_0

    :pswitch_10
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_a

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v2, v2

    if-gtz v2, :cond_a

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v2, v2

    if-lez v2, :cond_b

    :cond_a
    invoke-virtual {p0, v0}, Lhcb;->a(Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0, v1}, Lhcb;->a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto/16 :goto_0

    :pswitch_11
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lipg;

    invoke-virtual {p0, v0}, Lhcb;->a(Lipg;)V

    goto/16 :goto_0

    :pswitch_12
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    invoke-virtual {p0}, Lhcb;->e()V

    goto/16 :goto_0

    :pswitch_13
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Liot;

    invoke-virtual {p0, v0}, Lhcb;->a(Liot;)V

    goto/16 :goto_0

    :pswitch_14
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    invoke-virtual {p0}, Lhcb;->f()V

    goto/16 :goto_0

    :pswitch_15
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Liov;

    invoke-virtual {p0, v0}, Lhcb;->a(Liov;)V

    goto/16 :goto_0

    :pswitch_16
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    invoke-virtual {p0}, Lhcb;->g()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_f
        :pswitch_e
        :pswitch_2
        :pswitch_1
        :pswitch_7
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_10
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_c
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_d
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method
