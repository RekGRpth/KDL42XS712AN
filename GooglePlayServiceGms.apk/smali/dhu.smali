.class public final Ldhu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:[Ljava/lang/String;

.field private final b:Ljava/util/HashMap;


# direct methods
.method private constructor <init>(Ldhv;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Ldhv;->a:Ljava/util/HashMap;

    iput-object v0, p0, Ldhu;->b:Ljava/util/HashMap;

    iget-object v0, p0, Ldhu;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Ldhu;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Ldhu;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Ldhu;->a:[Ljava/lang/String;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Ldhv;B)V
    .locals 0

    invoke-direct {p0, p1}, Ldhu;-><init>(Ldhv;)V

    return-void
.end method

.method public static a()Ldhv;
    .locals 2

    new-instance v0, Ldhv;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldhv;-><init>(B)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ldhw;
    .locals 3

    iget-object v0, p0, Ldhu;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No entry for column "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldhu;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhw;

    return-object v0
.end method
