.class public final Lilp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/os/PowerManager$WakeLock;Landroid/os/Handler;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lilp;->b:Landroid/os/PowerManager$WakeLock;

    new-instance v0, Lilq;

    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lilq;-><init>(Lilp;Landroid/os/Looper;Landroid/os/Handler;)V

    iput-object v0, p0, Lilp;->a:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public final a(IIILjava/lang/Object;)V
    .locals 4

    const-wide v2, 0x7fffffffffffffffL

    iget-object v0, p0, Lilp;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    cmp-long v1, v2, v2

    if-gez v1, :cond_0

    iget-object v1, p0, Lilp;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    :goto_0
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_0
    iget-object v1, p0, Lilp;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0
.end method
