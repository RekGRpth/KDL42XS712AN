.class public final Ldzs;
.super Ldve;
.source "SourceFile"


# instance fields
.field private final e:Landroid/view/LayoutInflater;

.field private f:Ljava/lang/String;

.field private g:J

.field private final h:Z

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:Z

.field private final m:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Ldvn;ZLandroid/view/View$OnClickListener;)V
    .locals 2

    invoke-direct {p0, p1}, Ldve;-><init>(Landroid/content/Context;)V

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Ldvn;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldzs;->e:Landroid/view/LayoutInflater;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldzs;->g:J

    iput-boolean p2, p0, Ldzs;->h:Z

    invoke-virtual {p1}, Ldvn;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p2, :cond_0

    sget v0, Lwx;->u:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Ldzs;->i:I

    sget v0, Lwx;->m:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Ldzs;->j:I

    sget v0, Lwx;->n:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Ldzs;->k:I

    sget v0, Lww;->b:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Ldzs;->l:Z

    iput-object p3, p0, Ldzs;->m:Landroid/view/View$OnClickListener;

    return-void

    :cond_0
    sget v0, Lwx;->t:I

    goto :goto_0
.end method

.method static synthetic a(Ldzs;)Z
    .locals 1

    iget-boolean v0, p0, Ldzs;->h:Z

    return v0
.end method

.method static synthetic b(Ldzs;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Ldzs;->m:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Ldzs;->e:Landroid/view/LayoutInflater;

    sget v1, Lxc;->y:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ldzt;

    invoke-direct {v1, p0, v0}, Ldzt;-><init>(Ldzs;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(J)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iput-wide p1, p0, Ldzs;->g:J

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 11

    const/4 v10, -0x1

    const/16 v9, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p3, Ldff;

    iget-object v0, p0, Ldzs;->f:Ljava/lang/String;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-wide v3, p0, Ldzs;->g:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-ltz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzt;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {p3}, Ldff;->m()Lcom/google/android/gms/games/Player;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-interface {v6}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Ldzt;->k:Ldzs;

    iget-object v4, v4, Ldzs;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_1
    iget-object v4, v0, Ldzt;->k:Ldzs;

    iget-boolean v4, v4, Ldzs;->l:Z

    if-eqz v4, :cond_0

    invoke-interface {p3}, Ldff;->k()Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    invoke-interface {p3}, Ldff;->i()Landroid/net/Uri;

    move-result-object v4

    :cond_1
    iget-object v7, v0, Ldzt;->k:Ldzs;

    iget-boolean v7, v7, Ldvj;->d:Z

    if-eqz v7, :cond_5

    iget-object v7, v0, Ldzt;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v8, Lwz;->f:I

    invoke-virtual {v7, v4, v8}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_2
    if-eqz v3, :cond_6

    iget-object v4, v0, Ldzt;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, v0, Ldzt;->d:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    iget-object v4, v0, Ldzt;->f:Landroid/database/CharArrayBuffer;

    invoke-interface {p3, v4}, Ldff;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v4, v0, Ldzt;->e:Landroid/widget/TextView;

    iget-object v7, v0, Ldzt;->f:Landroid/database/CharArrayBuffer;

    iget-object v7, v7, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v8, v0, Ldzt;->f:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v4, v7, v2, v8}, Landroid/widget/TextView;->setText([CII)V

    if-eqz v3, :cond_7

    iget-object v3, v0, Ldzt;->e:Landroid/widget/TextView;

    iget-object v4, v0, Ldzt;->k:Ldzs;

    iget v4, v4, Ldzs;->j:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, v0, Ldzt;->g:Landroid/view/View;

    iget-object v4, v0, Ldzt;->k:Ldzs;

    iget v4, v4, Ldzs;->i:I

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v3, v0, Ldzt;->i:Landroid/widget/TextView;

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, v0, Ldzt;->h:Landroid/widget/TextView;

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_4
    invoke-interface {p3}, Ldff;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ledm;->a(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v1, v0, Ldzt;->i:Landroid/widget/TextView;

    invoke-interface {p3}, Ldff;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Ldzt;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_5
    iget-object v1, v0, Ldzt;->k:Ldzs;

    iget-boolean v1, v1, Ldzs;->h:Z

    if-eqz v1, :cond_2

    iget-object v0, v0, Ldzt;->j:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto/16 :goto_0

    :cond_4
    move v3, v2

    goto/16 :goto_1

    :cond_5
    iget-object v4, v0, Ldzt;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_2

    :cond_6
    iget-object v4, v0, Ldzt;->c:Landroid/database/CharArrayBuffer;

    invoke-interface {p3, v4}, Ldff;->b(Landroid/database/CharArrayBuffer;)V

    iget-object v4, v0, Ldzt;->b:Landroid/widget/TextView;

    iget-object v7, v0, Ldzt;->c:Landroid/database/CharArrayBuffer;

    iget-object v7, v7, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v8, v0, Ldzt;->c:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v4, v7, v2, v8}, Landroid/widget/TextView;->setText([CII)V

    iget-object v4, v0, Ldzt;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, v0, Ldzt;->d:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_7
    iget-object v3, v0, Ldzt;->e:Landroid/widget/TextView;

    iget-object v4, v0, Ldzt;->k:Ldzs;

    iget v4, v4, Ldzs;->k:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, v0, Ldzt;->g:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, v0, Ldzt;->i:Landroid/widget/TextView;

    iget-object v4, v0, Ldzt;->k:Ldzs;

    iget v4, v4, Ldzs;->i:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, v0, Ldzt;->h:Landroid/widget/TextView;

    iget-object v4, v0, Ldzt;->k:Ldzs;

    iget v4, v4, Ldzs;->i:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4

    :cond_8
    const-wide/16 v7, 0x64

    mul-long/2addr v3, v7

    iget-object v7, v0, Ldzt;->k:Ldzs;

    iget-wide v7, v7, Ldzs;->g:J

    div-long/2addr v3, v7

    long-to-int v3, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    sget v4, Lxf;->aK:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v5, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Ldzt;->i:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Ldzt;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldzs;->f:Ljava/lang/String;

    return-void
.end method

.method public final isEnabled(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
