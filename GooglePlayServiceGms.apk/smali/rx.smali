.class public final Lrx;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/BlockingQueue;

.field private final b:Lrw;

.field private final c:Lro;

.field private final d:Lsl;

.field private volatile e:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Lrw;Lro;Lsl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lrx;->e:Z

    iput-object p1, p0, Lrx;->a:Ljava/util/concurrent/BlockingQueue;

    iput-object p2, p0, Lrx;->b:Lrw;

    iput-object p3, p0, Lrx;->c:Lro;

    iput-object p4, p0, Lrx;->d:Lsl;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lrx;->e:Z

    invoke-virtual {p0}, Lrx;->interrupt()V

    return-void
.end method

.method public final run()V
    .locals 6

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lrx;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsc;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v1, "network-queue-take"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lsc;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "network-discard-cancelled"

    invoke-virtual {v0, v1}, Lsc;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {v1}, Lsc;->a(Lsp;)Lsp;

    move-result-object v1

    iget-object v2, p0, Lrx;->d:Lsl;

    invoke-interface {v2, v0, v1}, Lsl;->a(Lsc;Lsp;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-boolean v0, p0, Lrx;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_1
    :try_start_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_2

    invoke-virtual {v0}, Lsc;->c()I

    move-result v1

    invoke-static {v1}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    :cond_2
    iget-object v1, p0, Lrx;->b:Lrw;

    invoke-interface {v1, v0}, Lrw;->a(Lsc;)Lrz;

    move-result-object v1

    const-string v2, "network-http-complete"

    invoke-virtual {v0, v2}, Lsc;->a(Ljava/lang/String;)V

    iget-boolean v2, v1, Lrz;->d:Z

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lsc;->s()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v1, "not-modified"

    invoke-virtual {v0, v1}, Lsc;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Lsp; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v2, "Unhandled exception %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lsq;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lrx;->d:Lsl;

    new-instance v3, Lsp;

    invoke-direct {v3, v1}, Lsp;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {v2, v0, v3}, Lsl;->a(Lsc;Lsp;)V

    goto :goto_0

    :cond_3
    :try_start_3
    invoke-virtual {v0, v1}, Lsc;->a(Lrz;)Lsi;

    move-result-object v1

    const-string v2, "network-parse-complete"

    invoke-virtual {v0, v2}, Lsc;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lsc;->n()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v1, Lsi;->b:Lrp;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lrx;->c:Lro;

    invoke-virtual {v0}, Lsc;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lsi;->b:Lrp;

    invoke-interface {v2, v3, v4}, Lro;->a(Ljava/lang/String;Lrp;)V

    const-string v2, "network-cache-written"

    invoke-virtual {v0, v2}, Lsc;->a(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lsc;->r()V

    iget-object v2, p0, Lrx;->d:Lsl;

    invoke-interface {v2, v0, v1}, Lsl;->a(Lsc;Lsi;)V
    :try_end_3
    .catch Lsp; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0
.end method
