.class public Ldwu;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Ldvh;


# instance fields
.field private a:[Landroid/widget/BaseAdapter;

.field private b:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method public varargs constructor <init>([Landroid/widget/BaseAdapter;)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-direct {p0, p1}, Ldwu;->b([Landroid/widget/BaseAdapter;)V

    return-void
.end method

.method private a(I)Ldww;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    array-length v3, v1

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    aget-object v4, v0, v2

    invoke-virtual {v4}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_0

    new-instance v0, Ldww;

    sub-int v1, p1, v1

    invoke-direct {v0, v4, v1}, Ldww;-><init>(Landroid/widget/BaseAdapter;I)V

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private varargs b([Landroid/widget/BaseAdapter;)V
    .locals 3

    new-instance v0, Ldwv;

    invoke-direct {v0, p0}, Ldwv;-><init>(Ldwu;)V

    iput-object v0, p0, Ldwu;->b:Landroid/database/DataSetObserver;

    iput-object p1, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    aget-object v1, v1, v0

    iget-object v2, p0, Ldwu;->b:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected final varargs a([Landroid/widget/BaseAdapter;)V
    .locals 2

    iget-object v0, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "mAdapters has already been set!"

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Ldwu;->b([Landroid/widget/BaseAdapter;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v2

    and-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public getCount()I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-direct {p0, p1}, Ldwu;->a(I)Ldww;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Ldww;->a:Landroid/widget/BaseAdapter;

    iget v0, v0, Ldww;->b:I

    invoke-virtual {v1, v0, p2, p3}, Landroid/widget/BaseAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    invoke-direct {p0, p1}, Ldwu;->a(I)Ldww;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Ldww;->a:Landroid/widget/BaseAdapter;

    iget v0, v0, Ldww;->b:I

    invoke-virtual {v1, v0}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Ldwu;->a(I)Ldww;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    :goto_1
    iget-object v3, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    aget-object v3, v3, v0

    iget-object v4, v2, Ldww;->a:Landroid/widget/BaseAdapter;

    if-eq v3, v4, :cond_2

    invoke-virtual {v3}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, v2, Ldww;->a:Landroid/widget/BaseAdapter;

    iget v2, v2, Ldww;->b:I

    invoke-virtual {v0, v2}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-direct {p0, p1}, Ldwu;->a(I)Ldww;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Ldww;->a:Landroid/widget/BaseAdapter;

    iget v0, v0, Ldww;->b:I

    invoke-virtual {v1, v0, p2, p3}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public final i()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    aget-object v0, v0, v1

    instance-of v2, v0, Ldvh;

    if-eqz v2, :cond_0

    check-cast v0, Ldvh;

    invoke-interface {v0}, Ldvh;->i()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public isEnabled(I)Z
    .locals 2

    invoke-direct {p0, p1}, Ldwu;->a(I)Ldww;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, v0, Ldww;->a:Landroid/widget/BaseAdapter;

    iget v0, v0, Ldww;->b:I

    invoke-virtual {v1, v0}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Ldwu;->a:[Landroid/widget/BaseAdapter;

    aget-object v0, v0, v1

    instance-of v2, v0, Ldvh;

    if-eqz v2, :cond_0

    check-cast v0, Ldvh;

    invoke-interface {v0}, Ldvh;->j()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method
