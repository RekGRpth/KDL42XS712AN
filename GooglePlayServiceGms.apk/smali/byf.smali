.class public final Lbyf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Intent;

.field private final b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbsp;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbyf;->b:Ljava/util/ArrayList;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    iget-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v0, "Authorized app not specified"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    const-string v1, "accountName"

    iget-object v2, p2, Lbsp;->a:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    const-string v1, "callerIdentity"

    iget-object v2, p2, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    iget-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    const-string v1, "disabledAncestors"

    iget-object v2, p0, Lbyf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    iget-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;)Lbyf;
    .locals 2

    iget-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    const-string v1, "driveId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lbyf;
    .locals 2

    iget-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final a([Ljava/lang/String;)Lbyf;
    .locals 2

    iget-object v0, p0, Lbyf;->a:Landroid/content/Intent;

    const-string v1, "mimeTypes"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method
