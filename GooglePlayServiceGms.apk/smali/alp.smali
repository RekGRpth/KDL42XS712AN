.class public final Lalp;
.super Laky;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/server/ClientContext;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Laky;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbfy;->a(Landroid/content/Context;)V

    iput-object p1, p0, Lalp;->a:Landroid/content/Context;

    iput-object p2, p0, Lalp;->b:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p3, p0, Lalp;->c:Ljava/lang/String;

    return-void
.end method

.method private a(I)Z
    .locals 1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lalp;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    sget-object v0, Lakl;->f:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v0

    return v0
.end method

.method public final a(Laku;)V
    .locals 3

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lalp;->a:Landroid/content/Context;

    iget-object v1, p0, Lalp;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lalp;->c:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Laku;I)V
    .locals 3

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2}, Lalp;->a(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State key is out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not between 0 and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lalp;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lalp;->a:Landroid/content/Context;

    iget-object v1, p0, Lalp;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lalp;->c:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Laku;ILjava/lang/String;[B)V
    .locals 7

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2}, Lalp;->a(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State key is out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not between 0 and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lalp;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    const-string v0, "Must provide a non-null resolved version"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    array-length v0, p4

    invoke-virtual {p0}, Lalp;->a()I

    move-result v1

    if-gt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "App state data is too large ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes). The maximum is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lalp;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lalp;->a:Landroid/content/Context;

    iget-object v1, p0, Lalp;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lalp;->c:Ljava/lang/String;

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;ILjava/lang/String;[B)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Laku;I[B)V
    .locals 6

    invoke-direct {p0, p2}, Lalp;->a(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State key is out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not between 0 and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lalp;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-virtual {p0}, Lalp;->a()I

    move-result v1

    if-gt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "App state data is too large ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes). The maximum is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lalp;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lalp;->a:Landroid/content/Context;

    iget-object v1, p0, Lalp;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lalp;->c:Ljava/lang/String;

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I[B)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    sget-object v0, Lakl;->g:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v0

    return v0
.end method

.method public final b(Laku;)V
    .locals 2

    iget-object v0, p0, Lalp;->a:Landroid/content/Context;

    iget-object v1, p0, Lalp;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbov;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a()V

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Laku;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Laku;I)V
    .locals 3

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2}, Lalp;->a(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State key is out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not between 0 and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lalp;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lalp;->a:Landroid/content/Context;

    iget-object v1, p0, Lalp;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lalp;->c:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I)V

    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lalp;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Laku;)V
    .locals 2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_0
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lalp;->a:Landroid/content/Context;

    iget-object v1, p0, Lalp;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;)V

    return-void
.end method
