.class public final Ldvi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lbfy;

.field public static b:Lbfy;

.field public static c:Lbfy;

.field public static d:Lbfy;

.field public static e:Lbfy;

.field public static f:Lbfy;

.field public static g:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "games.activate_cheat_code"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Ldvi;->a:Lbfy;

    const-string v0, "games.google_settings_enabled"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Ldvi;->b:Lbfy;

    const-string v0, "games.play_games_help_webpage_url"

    const-string v1, "https://support.google.com/googleplay/?p=games_default"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Ldvi;->c:Lbfy;

    const-string v0, "games.play_sign_in_problems_webpage_url"

    const-string v1, "https://support.google.com/googleplay/?p=account_password"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Ldvi;->d:Lbfy;

    const-string v0, "games.play_games_dogfood"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Ldvi;->e:Lbfy;

    const-string v0, "games.play_games_id_sharing_webpage_url"

    const-string v1, "https://support.google.com/googleplay/?p=games_visibility"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Ldvi;->f:Lbfy;

    const-string v0, "games.learn_more_notifications_url"

    const-string v1, "https://support.google.com/googleplay/?p=games_notifications"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Ldvi;->g:Lbfy;

    return-void
.end method
