.class public final Lcua;
.super Lbgq;
.source "SourceFile"

# interfaces
.implements Lcty;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbgq;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const-string v0, "type"

    invoke-virtual {p0, v0}, Lcua;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/database/CharArrayBuffer;)V
    .locals 1

    const-string v0, "name"

    invoke-virtual {p0, v0, p1}, Lcua;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    invoke-virtual {p0, v0}, Lcua;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/database/CharArrayBuffer;)V
    .locals 1

    const-string v0, "description"

    invoke-virtual {p0, v0, p1}, Lcua;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    const-string v0, "unlocked_icon_image_uri"

    invoke-virtual {p0, v0}, Lcua;->g(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    const-string v0, "revealed_icon_image_uri"

    invoke-virtual {p0, v0}, Lcua;->g(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 2

    const/4 v0, 0x1

    const-string v1, "type"

    invoke-virtual {p0, v1}, Lcua;->c(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    const-string v0, "total_steps"

    invoke-virtual {p0, v0}, Lcua;->c(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    const-string v0, "state"

    invoke-virtual {p0, v0}, Lcua;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    const/4 v0, 0x1

    const-string v1, "type"

    invoke-virtual {p0, v1}, Lcua;->c(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    const-string v0, "current_steps"

    invoke-virtual {p0, v0}, Lcua;->c(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lbkj;->a(Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "external_achievement_id"

    invoke-virtual {p0, v2}, Lcua;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {p0, v2}, Lcua;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "state"

    const-string v2, "state"

    invoke-virtual {p0, v2}, Lcua;->c(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {p0, v2}, Lcua;->c(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {p0, v1}, Lcua;->c(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const-string v1, "steps"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcua;->g()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcua;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    :cond_0
    invoke-virtual {v0}, Lbkk;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
