.class public Lbmi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final j:Ljava/util/HashMap;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lsf;

.field protected final c:Z

.field protected final d:Ljava/lang/String;

.field protected final e:Ljava/lang/String;

.field protected final f:Ljava/lang/ThreadLocal;

.field private final g:Z

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lbmi;->j:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lbmj;

    invoke-direct {v0, p0}, Lbmj;-><init>(Lbmi;)V

    iput-object v0, p0, Lbmi;->f:Ljava/lang/ThreadLocal;

    iput-object p1, p0, Lbmi;->a:Landroid/content/Context;

    iput-object p2, p0, Lbmi;->i:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmi;->h:Ljava/lang/String;

    iput-boolean p4, p0, Lbmi;->c:Z

    iput-boolean p5, p0, Lbmi;->g:Z

    iput-object p6, p0, Lbmi;->d:Ljava/lang/String;

    iput-object p7, p0, Lbmi;->e:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lsf;

    move-result-object v0

    iput-object v0, p0, Lbmi;->b:Lsf;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lbmi;
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lbmi;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lbmi;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    const-string v0, "&"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "trace="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lbmi;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "?"

    goto :goto_0
.end method

.method private a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v1, v0, Lsp;

    if-eqz v1, :cond_1

    check-cast v0, Lsp;

    invoke-static {v0}, Lbmi;->a(Lsp;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbmi;->a:Landroid/content/Context;

    invoke-static {v1, p2}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    throw v0

    :cond_1
    return-void
.end method

.method private a(Lsc;ZLjava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1, p3}, Lbmi;->a(Lsc;Ljava/lang/String;)V

    invoke-direct {p0}, Lbmi;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Non-batchable request in batch"

    invoke-static {p2, v0}, Lbiq;->a(ZLjava/lang/Object;)V

    instance-of v0, p1, Lbme;

    const-string v1, "Non-batchable request in batch"

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbmi;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmk;

    check-cast p1, Lbme;

    iget-object v0, v0, Lbmk;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbmi;->b:Lsf;

    invoke-virtual {v0, p1}, Lsf;->a(Lsc;)Lsc;

    goto :goto_0
.end method

.method public static a(Lsp;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lsp;->a:Lrz;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lsp;->a:Lrz;

    iget v1, v1, Lrz;->a:I

    const/16 v2, 0x191

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lbng;->b(Lsp;Ljava/lang/String;)Lbne;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lbne;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "authError"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "expired"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 11

    invoke-virtual {p0, p1}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lbmu;->a()Lbmu;

    move-result-object v8

    iget-object v0, p0, Lbmi;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v7}, Lbmi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {p0}, Lbmi;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lbmi;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object v9, v8

    invoke-virtual/range {v0 .. v10}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Ljava/lang/String;Lsk;Lsj;Ljava/util/HashMap;)Lbme;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v7}, Lbmi;->a(Lsc;ZLjava/lang/String;)V

    :try_start_0
    invoke-virtual {v8}, Lbmu;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lsp;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0, v7}, Lbmi;->a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    new-instance v1, Lsp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lsp;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    .locals 8

    invoke-virtual {p0, p1}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lbmu;->a()Lbmu;

    move-result-object v4

    iget-object v0, p0, Lbmi;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v6}, Lbmi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v7

    new-instance v0, Lbmq;

    invoke-virtual {p0}, Lbmi;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p3}, Lbmi;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move v1, p2

    move-object v3, p4

    move-object v5, v4

    invoke-direct/range {v0 .. v7}, Lbmq;-><init>(ILjava/lang/String;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;Ljava/util/HashMap;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v6}, Lbmi;->a(Lsc;ZLjava/lang/String;)V

    :try_start_0
    invoke-virtual {v4}, Lbmu;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lsp;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0, v6}, Lbmi;->a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    new-instance v1, Lsp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lsp;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lbmi;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Ljava/lang/String;Lsk;Lsj;Ljava/util/HashMap;)Lbme;
    .locals 11

    new-instance v0, Lbme;

    iget-boolean v9, p0, Lbmi;->c:Z

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p7

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lbme;-><init>(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 2

    :try_start_0
    invoke-direct/range {p0 .. p6}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lbmi;->a(Lsp;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct/range {p0 .. p6}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    goto :goto_0

    :cond_0
    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 6

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;[Lbmd;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 10

    invoke-virtual {p0, p1}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lbmu;->a()Lbmu;

    move-result-object v5

    iget-object v0, p0, Lbmi;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v7}, Lbmi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v9

    new-instance v0, Lbmc;

    invoke-virtual {p0}, Lbmi;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lbmi;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    iget-boolean v8, p0, Lbmi;->c:Z

    move-object v2, p3

    move-object v3, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v9}, Lbmc;-><init>(Ljava/lang/String;[Lbmd;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v7}, Lbmi;->a(Lsc;ZLjava/lang/String;)V

    :try_start_0
    invoke-virtual {v5}, Lbmu;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lsp;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0, v7}, Lbmi;->a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    new-instance v1, Lsp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lsp;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbmi;->i:Ljava/lang/String;

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lbmi;->c(Lcom/google/android/gms/common/server/ClientContext;)Lbmx;

    move-result-object v0

    iget-object v1, p0, Lbmi;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lbmz;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lbmi;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "X-Google-Backend-Override"

    iget-object v2, p0, Lbmi;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    .locals 2

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lbmi;->a(Lsp;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Lsk;Lsj;)V
    .locals 12

    invoke-virtual {p0, p1}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v1, p0, Lbmi;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, p1, v9}, Lbmi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v11

    new-instance v1, Lbme;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbmi;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, p3}, Lbmi;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-boolean v10, p0, Lbmi;->c:Z

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v11}, Lbme;-><init>(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2, v9}, Lbmi;->a(Lsc;ZLjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lsp;

    const-string v2, "Unable to obtain auth token - is the device online?"

    invoke-direct {v1, v2}, Lsp;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p8

    invoke-interface {v0, v1}, Lsj;->a(Lsp;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lsk;Lsj;)V
    .locals 9

    const/4 v4, 0x0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, v4

    move-object v7, p4

    move-object v8, p5

    invoke-virtual/range {v0 .. v8}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Lsk;Lsj;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2, p3}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lsk;Lsj;)V
    .locals 8

    invoke-virtual {p0, p1}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lbmi;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v6}, Lbmi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v7

    if-eqz v6, :cond_0

    new-instance v0, Lbmq;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lbmi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lbmi;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lbmq;-><init>(ILjava/lang/String;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;Ljava/util/HashMap;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v6}, Lbmi;->a(Lsc;ZLjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lsp;

    const-string v1, "Unable to obtain auth token - is the device online?"

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    invoke-interface {p4, v0}, Lsj;->a(Lsp;)V

    goto :goto_0
.end method

.method public a(Lsc;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lbml;

    iget-object v1, p0, Lbmi;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lbml;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lsc;->a(Lsm;)Lsc;

    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B
    .locals 7

    invoke-static {}, Lbmu;->a()Lbmu;

    move-result-object v5

    new-instance v0, Lbmp;

    iget-boolean v4, p0, Lbmi;->c:Z

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lbmp;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLsk;Lsj;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lbmi;->a(Lsc;ZLjava/lang/String;)V

    :try_start_0
    invoke-virtual {v5}, Lbmu;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lsp;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Lsp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lsp;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)[B
    .locals 7

    const/4 v2, 0x0

    invoke-static {}, Lbmu;->a()Lbmu;

    move-result-object v5

    new-instance v0, Lbmp;

    iget-boolean v4, p0, Lbmi;->c:Z

    move-object v1, p1

    move-object v3, p2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lbmp;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLsk;Lsj;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lbmi;->a(Lsc;ZLjava/lang/String;)V

    :try_start_0
    invoke-virtual {v5}, Lbmu;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lsp;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Lsp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lsp;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbmi;->h:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    const-string v0, "auth_token"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/server/ClientContext;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lbmi;->c(Lcom/google/android/gms/common/server/ClientContext;)Lbmx;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lbmi;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lbmx;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lsp;

    invoke-direct {v1, v0}, Lsp;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 7

    const/4 v4, 0x0

    invoke-static {}, Lbmu;->a()Lbmu;

    move-result-object v5

    new-instance v0, Lbmp;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lbmp;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLsk;Lsj;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v4, v1}, Lbmi;->a(Lsc;ZLjava/lang/String;)V

    return-void
.end method

.method protected c(Lcom/google/android/gms/common/server/ClientContext;)Lbmx;
    .locals 1

    new-instance v0, Lbmx;

    invoke-direct {v0, p1}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    return-object v0
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lbmi;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    new-instance v1, Lbmk;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lbmk;-><init>(B)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final d()V
    .locals 3

    invoke-direct {p0}, Lbmi;->e()Z

    move-result v0

    const-string v1, "Not currently in an Apiary batch."

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbmi;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmk;

    iget-object v1, v0, Lbmk;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbma;->t()V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbmi;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lbma;

    iget-object v0, v0, Lbmk;->a:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v0}, Lbma;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lbmi;->b:Lsf;

    invoke-virtual {v0, v1}, Lsf;->a(Lsc;)Lsc;

    move-object v0, v1

    goto :goto_1
.end method
