.class public final Lbaz;
.super Lbak;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbac;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lbak;-><init>(Lbac;)V

    iput-object p2, p0, Lbaz;->c:Ljava/lang/String;

    iput-object p3, p0, Lbaz;->d:Ljava/lang/String;

    iput-wide p4, p0, Lbaz;->e:J

    iput-object p6, p0, Lbaz;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    :try_start_0
    iget-object v0, p0, Lbaz;->b:Lbac;

    iget-object v1, p0, Lbaz;->c:Ljava/lang/String;

    iget-object v2, p0, Lbaz;->d:Ljava/lang/String;

    iget-wide v3, p0, Lbaz;->e:J

    iget-object v5, p0, Lbaz;->f:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lbac;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lbaz;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
