.class public final Lfzb;
.super Landroid/text/style/ClickableSpan;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    iput-object p1, p0, Lfzb;->a:Landroid/content/Context;

    iput-object p2, p0, Lfzb;->b:Ljava/lang/String;

    iput-object p3, p0, Lfzb;->c:Ljava/lang/String;

    iput-object p4, p0, Lfzb;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lfzb;->d:Ljava/lang/String;

    iget-object v1, p0, Lfzb;->d:Ljava/lang/String;

    iget-object v2, p0, Lfzb;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lboi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lboi;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lo;

    invoke-virtual {v0}, Lo;->K_()Lu;

    move-result-object v0

    iget-object v2, p0, Lfzb;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lboi;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    iget-object v0, p0, Lfzb;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110014    # com.google.android.gms.R.bool.plus_links_underlined

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    const v1, 0x7f0c011c    # com.google.android.gms.R.color.plus_oob_link_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    return-void
.end method
