.class public Lbqi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/PowerManager$WakeLock;

.field public b:Lbql;

.field c:Landroid/os/HandlerThread;

.field private final d:Ljava/lang/String;

.field private final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lbqi;-><init>(Ljava/lang/String;ZLandroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLandroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    invoke-direct {v0, p1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbqi;->c:Landroid/os/HandlerThread;

    iget-object v0, p0, Lbqi;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    iget-object v0, p0, Lbqi;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object p1, p0, Lbqi;->d:Ljava/lang/String;

    iput-boolean p2, p0, Lbqi;->e:Z

    if-eqz p2, :cond_0

    const-string v0, "power"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0, v2, p1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    :goto_0
    new-instance v0, Lbql;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lbql;-><init>(Landroid/os/Looper;Lbqi;B)V

    iput-object v0, p0, Lbqi;->b:Lbql;

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method private e(I)Landroid/os/Message;
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0}, Lbql;->d(Lbql;)Lbqk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbqk;->a(I)V

    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lbqi;->b:Lbql;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_1
    iget-object v1, p0, Lbqi;->b:Lbql;

    iget-object v0, p0, Lbqi;->b:Lbql;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lbql;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    goto :goto_1
.end method

.method protected final a(Landroid/os/Message;)V
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0, p1}, Lbql;->a(Lbql;Landroid/os/Message;)V

    return-void
.end method

.method protected final a(Lbqg;)V
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0, p1}, Lbql;->a(Lbql;Lbqg;)V

    return-void
.end method

.method protected final a(Lbqh;)V
    .locals 2

    iget-object v0, p0, Lbqi;->b:Lbql;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lbql;->a(Lbql;Lbqh;Lbqh;)Lbqo;

    return-void
.end method

.method protected final a(Lbqh;Lbqh;)V
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0, p1, p2}, Lbql;->a(Lbql;Lbqh;Lbqh;)Lbqo;

    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 6

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbqi;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " total records="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbqi;->b:Lbql;

    invoke-static {v2}, Lbql;->d(Lbql;)Lbqk;

    move-result-object v2

    invoke-virtual {v2}, Lbqk;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lbqi;->b:Lbql;

    invoke-static {v2}, Lbql;->d(Lbql;)Lbqk;

    move-result-object v2

    invoke-virtual {v2}, Lbqk;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    const-string v2, " rec[%d]: %s\n"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x1

    iget-object v5, p0, Lbqi;->b:Lbql;

    invoke-static {v5}, Lbql;->d(Lbql;)Lbqk;

    move-result-object v5

    invoke-virtual {v5, v0}, Lbqk;->b(I)Lbqj;

    move-result-object v5

    invoke-virtual {v5, p0}, Lbqj;->a(Lbqi;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {p1}, Ljava/io/PrintWriter;->flush()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "curState="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbqi;->b:Lbql;

    invoke-static {v1}, Lbql;->b(Lbql;)Lbqg;

    move-result-object v1

    invoke-interface {v1}, Lbqg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0, p1}, Lbql;->a(Lbql;Z)V

    goto :goto_0
.end method

.method protected b(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "(0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0}, Lbql;->f(Lbql;)V

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0}, Lbql;->g(Lbql;)V

    goto :goto_0
.end method

.method protected final b(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0}, Lbql;->c(Lbql;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbqi;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - unhandledMessage: msg.what="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {p0, v2}, Lbqi;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected final b(Lbqh;)V
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0, p1}, Lbql;->a(Lbql;Lbqh;)V

    return-void
.end method

.method public final c(I)V
    .locals 2

    iget-object v0, p0, Lbqi;->b:Lbql;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_1
    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-direct {p0, p1}, Lbqi;->e(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbql;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected final c(Landroid/os/Message;)V
    .locals 1

    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_0
    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-virtual {v0, p1}, Lbql;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    return-void
.end method

.method protected final d(I)V
    .locals 2

    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_0
    iget-object v0, p0, Lbqi;->b:Lbql;

    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lbqi;->e(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbql;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    return-void
.end method
