.class public final Ldyd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    iput-object p1, p0, Ldyd;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    const-string v0, "return to game sequence not on main thread"

    invoke-static {v0}, Lbiq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldyd;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->i(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyd;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Ldyd;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;I)V

    :goto_0
    iget-object v0, p0, Ldyd;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    return-void

    :cond_1
    iget-object v0, p0, Ldyd;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;I)V

    goto :goto_0
.end method
