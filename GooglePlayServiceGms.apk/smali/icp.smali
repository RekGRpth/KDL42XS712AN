.class public final Licp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lidp;

.field private final b:Ljava/util/LinkedList;

.field private final c:Lima;

.field private final d:Ljava/io/PrintWriter;

.field private final e:Ljava/util/Date;

.field private final f:Ljava/lang/StringBuffer;

.field private final g:Ljava/text/FieldPosition;

.field private final h:Ljava/text/SimpleDateFormat;

.field private final i:Ljava/util/EnumSet;

.field private final j:[Lido;

.field private final k:[I

.field private final l:[J

.field private m:J

.field private n:I


# direct methods
.method public constructor <init>(Lidp;Lima;Ljava/io/PrintWriter;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Licp;->e:Ljava/util/Date;

    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Licp;->f:Ljava/lang/StringBuffer;

    new-instance v0, Ljava/text/FieldPosition;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/text/FieldPosition;-><init>(I)V

    iput-object v0, p0, Licp;->g:Ljava/text/FieldPosition;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy.MM.dd HH:mm:ss "

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Licp;->h:Ljava/text/SimpleDateFormat;

    sget-object v0, Licn;->h:Licn;

    sget-object v1, Licn;->L:Licn;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Licp;->i:Ljava/util/EnumSet;

    invoke-static {}, Licn;->values()[Licn;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lido;

    iput-object v0, p0, Licp;->j:[Lido;

    invoke-static {}, Licn;->values()[Licn;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Licp;->k:[I

    invoke-static {}, Licn;->values()[Licn;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Licp;->l:[J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Licp;->m:J

    const/4 v0, -0x1

    iput v0, p0, Licp;->n:I

    iput-object p1, p0, Licp;->a:Lidp;

    iput-object p2, p0, Licp;->c:Lima;

    iput-object p3, p0, Licp;->d:Ljava/io/PrintWriter;

    iget-object v0, p0, Licp;->l:[J

    const-wide/16 v1, -0x1

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->fill([JJ)V

    return-void
.end method

.method private declared-synchronized a(Ljava/io/PrintWriter;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lido;

    iget-object v2, v0, Lido;->f:Licn;

    iget-object v2, v2, Licn;->Y:Lico;

    sget-object v3, Lico;->a:Lico;

    if-ne v2, v3, :cond_0

    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(C)V

    :cond_0
    invoke-virtual {v0, p1}, Lido;->b(Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    new-instance v0, Lidh;

    sget-object v2, Licn;->e:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lidh;-><init>(Licp;Licn;JI)V

    invoke-virtual {p0, v0, p1}, Licp;->a(Lido;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V
    .locals 7

    new-instance v0, Lidd;

    sget-object v2, Licn;->R:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lidd;-><init>(Licp;Licn;JLcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final a(Lhtf;)V
    .locals 6

    new-instance v0, Lidj;

    sget-object v2, Licn;->g:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lidj;-><init>(Licp;Licn;JLhtf;)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final a(Licn;)V
    .locals 3

    new-instance v0, Lido;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v1

    invoke-direct {v0, p1, v1, v2}, Lido;-><init>(Licn;J)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final declared-synchronized a(Lido;)V
    .locals 1

    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    invoke-virtual {p0, p1, v0}, Licp;->a(Lido;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lido;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lido;

    iget-object v1, p0, Licp;->l:[J

    iget-object v0, v0, Lido;->f:Licn;

    invoke-virtual {v0}, Licn;->ordinal()I

    move-result v0

    iget-wide v2, p1, Lido;->g:J

    aput-wide v2, v1, v0

    :cond_0
    iget-object v0, p0, Licp;->j:[Lido;

    iget-object v1, p1, Lido;->f:Licn;

    invoke-virtual {v1}, Licn;->ordinal()I

    move-result v1

    aput-object p1, v0, v1

    iget-object v0, p0, Licp;->k:[I

    iget-object v1, p1, Lido;->f:Licn;

    invoke-virtual {v1}, Licn;->ordinal()I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    iget-object v0, p1, Lido;->f:Licn;

    sget-object v1, Licn;->n:Licn;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lido;

    iget-object v0, v0, Lido;->f:Licn;

    sget-object v1, Licn;->n:Licn;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    iget-object v1, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lido;

    iget-object v0, v0, Lido;->f:Licn;

    sget-object v1, Licn;->n:Licn;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0xc8

    if-le v0, v1, :cond_2

    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p1, Lido;->f:Licn;

    iget-object v0, v0, Licn;->Y:Lico;

    sget-object v1, Lico;->a:Lico;

    if-ne v0, v1, :cond_3

    iget-wide v0, p1, Lido;->g:J

    iput-wide v0, p0, Licp;->m:J

    :cond_3
    iget-object v0, p1, Lido;->f:Licn;

    sget-object v1, Licn;->u:Licn;

    if-eq v0, v1, :cond_4

    sget-object v1, Licn;->t:Licn;

    if-eq v0, v1, :cond_4

    sget-object v1, Licn;->e:Licn;

    if-eq v0, v1, :cond_4

    sget-object v1, Licn;->M:Licn;

    if-eq v0, v1, :cond_4

    sget-object v1, Licn;->N:Licn;

    if-ne v0, v1, :cond_7

    :cond_4
    iput p2, p0, Licp;->n:I

    :goto_1
    iget-object v0, p0, Licp;->c:Lima;

    if-eqz v0, :cond_5

    iget-object v0, p0, Licp;->i:Ljava/util/EnumSet;

    iget-object v1, p1, Lido;->f:Licn;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Licp;->c:Lima;

    const-string v1, "gmmNlpEventLog"

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lima;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p1, v1}, Lido;->b(Ljava/io/PrintWriter;)V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Licp;->c:Lima;

    const-string v2, "gmmNlpEventLog"

    invoke-interface {v1, v2, v0}, Lima;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Licp;->d:Ljava/io/PrintWriter;

    if-eqz v0, :cond_6

    iget-object v0, p0, Licp;->e:Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    iget-object v0, p0, Licp;->f:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    iget-object v0, p0, Licp;->h:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Licp;->e:Ljava/util/Date;

    iget-object v2, p0, Licp;->f:Ljava/lang/StringBuffer;

    iget-object v3, p0, Licp;->g:Ljava/text/FieldPosition;

    invoke-virtual {v0, v1, v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Licp;->d:Ljava/io/PrintWriter;

    iget-object v1, p0, Licp;->f:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Licp;->d:Ljava/io/PrintWriter;

    invoke-virtual {p1, v0}, Lido;->b(Ljava/io/PrintWriter;)V

    iget-object v0, p0, Licp;->d:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    monitor-exit p0

    return-void

    :cond_7
    const/4 v0, -0x1

    :try_start_2
    iput v0, p0, Licp;->n:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final a(Lidr;)V
    .locals 6

    new-instance v0, Lidn;

    sget-object v2, Licn;->n:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lidn;-><init>(Licp;Licn;JLidr;)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v0, Lidg;

    sget-object v2, Licn;->L:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lidg;-><init>(Licp;Licn;JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final declared-synchronized a(Ljava/text/Format;JJLjava/io/PrintWriter;)V
    .locals 9

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    const-wide/32 v2, 0x927c0

    sub-long v2, p4, v2

    iget-object v0, p0, Licp;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lido;

    iget-wide v5, v0, Lido;->g:J

    cmp-long v5, v5, v2

    if-ltz v5, :cond_0

    iget-object v5, v0, Lido;->f:Licn;

    iget-object v5, v5, Licn;->Y:Lico;

    sget-object v6, Lico;->a:Lico;

    if-ne v5, v6, :cond_1

    const/16 v5, 0xa

    invoke-virtual {p6, v5}, Ljava/io/PrintWriter;->print(C)V

    :cond_1
    invoke-virtual {v1, p2, p3}, Ljava/util/Date;->setTime(J)V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    iget-wide v7, v0, Lido;->g:J

    add-long/2addr v5, v7

    invoke-virtual {v1, v5, v6}, Ljava/util/Date;->setTime(J)V

    invoke-virtual {p1, v1}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p6, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/16 v5, 0x20

    invoke-virtual {p6, v5}, Ljava/io/PrintWriter;->print(C)V

    invoke-virtual {v0, p6}, Lido;->b(Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final a(Z)V
    .locals 6

    new-instance v0, Lidb;

    sget-object v2, Licn;->d:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lidb;-><init>(Licp;Licn;JZ)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final a(ZZI)V
    .locals 8

    new-instance v0, Lidm;

    sget-object v2, Licn;->o:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lidm;-><init>(Licp;Licn;JZZI)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final b(Z)V
    .locals 6

    new-instance v0, Licr;

    sget-object v2, Licn;->q:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Licr;-><init>(Licp;Licn;JZ)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final c(Z)V
    .locals 6

    new-instance v0, Lict;

    sget-object v2, Licn;->s:Licn;

    iget-object v1, p0, Licp;->a:Lidp;

    invoke-interface {v1}, Lidp;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lict;-><init>(Licp;Licn;JZ)V

    invoke-virtual {p0, v0}, Licp;->a(Lido;)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x2710

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, v1}, Licp;->a(Ljava/io/PrintWriter;)V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
