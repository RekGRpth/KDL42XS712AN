.class final enum Lccl;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lccl;

.field public static final enum b:Lccl;

.field public static final enum c:Lccl;

.field private static final synthetic d:[Lccl;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lccl;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lccl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lccl;->a:Lccl;

    new-instance v0, Lccl;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, Lccl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lccl;->b:Lccl;

    new-instance v0, Lccl;

    const-string v1, "READY_FOR_SCHEDULING"

    invoke-direct {v0, v1, v4}, Lccl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lccl;->c:Lccl;

    const/4 v0, 0x3

    new-array v0, v0, [Lccl;

    sget-object v1, Lccl;->a:Lccl;

    aput-object v1, v0, v2

    sget-object v1, Lccl;->b:Lccl;

    aput-object v1, v0, v3

    sget-object v1, Lccl;->c:Lccl;

    aput-object v1, v0, v4

    sput-object v0, Lccl;->d:[Lccl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lccl;
    .locals 1

    const-class v0, Lccl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lccl;

    return-object v0
.end method

.method public static values()[Lccl;
    .locals 1

    sget-object v0, Lccl;->d:[Lccl;

    invoke-virtual {v0}, [Lccl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lccl;

    return-object v0
.end method
