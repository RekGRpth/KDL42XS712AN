.class public final Lepr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/identity/intents/model/UserAddress;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/identity/intents/model/UserAddress;)V
    .locals 0

    iput-object p1, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-boolean p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->m:Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->f:Ljava/lang/String;

    return-object p0
.end method

.method public final g(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final h(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->h:Ljava/lang/String;

    return-object p0
.end method

.method public final i(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->i:Ljava/lang/String;

    return-object p0
.end method

.method public final j(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->j:Ljava/lang/String;

    return-object p0
.end method

.method public final k(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->k:Ljava/lang/String;

    return-object p0
.end method

.method public final l(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->l:Ljava/lang/String;

    return-object p0
.end method

.method public final m(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->n:Ljava/lang/String;

    return-object p0
.end method

.method public final n(Ljava/lang/String;)Lepr;
    .locals 1

    iget-object v0, p0, Lepr;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v0, Lcom/google/android/gms/identity/intents/model/UserAddress;->o:Ljava/lang/String;

    return-object p0
.end method
