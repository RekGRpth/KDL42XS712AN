.class public final Ldti;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZ)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldti;->b:Ldad;

    iput-object p3, p0, Ldti;->c:Ljava/lang/String;

    iput p4, p0, Ldti;->d:I

    iput-boolean p5, p0, Ldti;->e:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldti;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->d(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    iget-object v2, p0, Ldti;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldti;->c:Ljava/lang/String;

    iget v4, p0, Ldti;->d:I

    iget-boolean v5, p0, Ldti;->e:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
