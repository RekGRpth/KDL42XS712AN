.class public final Lchx;
.super Lizs;
.source "SourceFile"


# static fields
.field public static final a:[Lchx;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lchx;

    sput-object v0, Lchx;->a:[Lchx;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lchx;->b:I

    const-string v0, ""

    iput-object v0, p0, Lchx;->c:Ljava/lang/String;

    iput-wide v1, p0, Lchx;->d:J

    iput-wide v1, p0, Lchx;->e:J

    const/4 v0, -0x1

    iput v0, p0, Lchx;->f:I

    return-void
.end method

.method public static a([B)Lchx;
    .locals 1

    new-instance v0, Lchx;

    invoke-direct {v0}, Lchx;-><init>()V

    invoke-static {v0, p0}, Lizs;->a(Lizs;[B)Lizs;

    move-result-object v0

    check-cast v0, Lchx;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, Lchx;->b:I

    invoke-static {v0, v1}, Lizn;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    const/4 v1, 0x2

    iget-object v2, p0, Lchx;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lchx;->d:J

    invoke-static {v1, v2, v3}, Lizn;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lchx;->e:J

    invoke-static {v1, v2, v3}, Lizn;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lchx;->f:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lchx;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lchx;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lchx;->d:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lchx;->e:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lchx;->b:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    const/4 v0, 0x2

    iget-object v1, p0, Lchx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    const/4 v0, 0x3

    iget-wide v1, p0, Lchx;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->c(IJ)V

    const/4 v0, 0x4

    iget-wide v1, p0, Lchx;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->c(IJ)V

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lchx;->f:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lchx;->a()I

    :cond_0
    iget v0, p0, Lchx;->f:I

    return v0
.end method
