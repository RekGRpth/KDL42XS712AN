.class public abstract Ldrw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldrb;


# instance fields
.field protected final a:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Ldrw;->a:Lcom/google/android/gms/common/server/ClientContext;

    return-void
.end method


# virtual methods
.method protected abstract a(I)V
.end method

.method public final a(Landroid/content/Context;Lcun;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0, p1, p2}, Ldrw;->b(Landroid/content/Context;Lcun;)I
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {p0, v0}, Ldrw;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "StatusOperation"

    const-string v2, "Auth error while performing operation, requesting reconnect"

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x2

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v0, "StatusOperation"

    invoke-virtual {v1}, Ldqq;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1}, Ldqq;->b()I

    move-result v0

    invoke-virtual {v1}, Ldqq;->a()I

    move-result v1

    const/16 v2, 0x5dc

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Ldrw;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v1, p2}, Leep;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcun;)V

    iget-object v1, p0, Ldrw;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lduj;->e(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_0

    invoke-virtual {p2, p1}, Lcun;->c(Landroid/content/Context;)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "StatusOperation"

    const-string v2, "Runtime exception while performing operation"

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "StatusOperation"

    const-string v2, "Killing (on development devices) due to RuntimeException"

    invoke-static {v1, v2, v0}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x1

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method protected abstract b(Landroid/content/Context;Lcun;)I
.end method
