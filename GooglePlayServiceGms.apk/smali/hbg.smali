.class public final Lhbg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/SignupActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 0

    iput-object p1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/4 v2, 0x1

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->R_()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    new-instance v1, Luu;

    const-string v3, "get_masked_wallet_signup"

    invoke-direct {v1, v3}, Luu;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Luu;)Luu;

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;)Luu;

    move-result-object v1

    invoke-virtual {v1}, Luu;->a()Lut;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Lut;)Lut;

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Z)V

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->a()Linv;

    move-result-object v0

    new-instance v3, Ljaw;

    invoke-direct {v3}, Ljaw;-><init>()V

    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Lcom/google/android/gms/wallet/ow/SignupActivity;)Ljbg;

    move-result-object v1

    iput-object v1, v3, Ljaw;->a:Ljbg;

    iput-object v0, v3, Ljaw;->e:Linv;

    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->d(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, v0, Linv;->b:Lint;

    iget-object v1, v1, Lint;->d:Lixo;

    iget-object v0, v0, Linv;->b:Lint;

    iget-object v0, v0, Lint;->g:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    :goto_0
    new-instance v4, Lipv;

    invoke-direct {v4}, Lipv;-><init>()V

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lixo;

    iput-object v0, v4, Lipv;->a:Lixo;

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lipv;->d:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->f(Lcom/google/android/gms/wallet/ow/SignupActivity;)Lipv;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->f(Lcom/google/android/gms/wallet/ow/SignupActivity;)Lipv;

    move-result-object v0

    invoke-static {v0, v4}, Lgty;->a(Lipv;Lipv;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->f(Lcom/google/android/gms/wallet/ow/SignupActivity;)Lipv;

    move-result-object v0

    iget-object v0, v0, Lipv;->b:Ljava/lang/String;

    iput-object v0, v3, Ljaw;->d:Ljava/lang/String;

    :cond_1
    :goto_1
    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b()[Ljbe;

    move-result-object v0

    invoke-static {v0}, Lgth;->a([Ljbe;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ljaw;->h:[Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, v3, Ljaw;->i:Z

    :cond_3
    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v3, Ljaw;->e:Linv;

    iget-object v1, v1, Linv;->b:Lint;

    iget-object v1, v1, Lint;->d:Lixo;

    iput-object v0, v1, Lixo;->t:Ljava/lang/String;

    :cond_4
    iget-object v1, v3, Ljaw;->g:Lipv;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v3, Ljaw;->g:Lipv;

    iget-object v1, v1, Lipv;->a:Lixo;

    iput-object v0, v1, Lixo;->t:Ljava/lang/String;

    :cond_5
    :goto_2
    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    :goto_3
    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    iget-object v2, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->g(Lcom/google/android/gms/wallet/ow/SignupActivity;)Lcom/google/android/gms/wallet/Cart;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v1, v3, v2, v4, v0}, Lhca;->a(Ljaw;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Z)V

    :goto_4
    return-void

    :cond_6
    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0}, Lgvc;->a()Lixo;

    move-result-object v0

    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v1}, Lgvc;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :cond_7
    iput-object v4, v3, Ljaw;->g:Lipv;

    goto/16 :goto_1

    :cond_8
    iget-object v0, v3, Ljaw;->g:Lipv;

    iget-object v0, v0, Lipv;->a:Lixo;

    iget-object v1, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lixo;->t:Ljava/lang/String;

    goto :goto_2

    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lhbg;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->i()Z

    goto :goto_4
.end method
