.class public final Lguz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 0

    iput-object p1, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    invoke-interface {v1}, Lgxf;->R_()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;Z)V

    new-instance v1, Lipa;

    invoke-direct {v1}, Lipa;-><init>()V

    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    invoke-interface {v2}, Lgxf;->a()Linv;

    move-result-object v2

    iput-object v2, v1, Lipa;->b:Linv;

    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lipa;->c:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Lioq;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Lioq;

    move-result-object v2

    iput-object v2, v1, Lipa;->a:Lioq;

    :cond_1
    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Landroid/widget/CheckBox;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    iget-object v2, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g()Lgyi;

    move-result-object v2

    invoke-virtual {v2}, Lgyi;->a()Lhca;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lhca;->a(Lipa;Z)V

    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lguz;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->i()Z

    goto :goto_1
.end method
