.class public abstract Lfzi;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final h:I


# instance fields
.field protected a:Z

.field protected final b:Landroid/widget/FrameLayout;

.field final c:Landroid/widget/CompoundButton;

.field final d:Lfwc;

.field e:I

.field protected f:Ljava/lang/String;

.field protected g:Landroid/view/View$OnClickListener;

.field private final i:Landroid/content/res/Resources;

.field private final j:Landroid/view/LayoutInflater;

.field private final k:Landroid/widget/LinearLayout;

.field private final l:Landroid/widget/ProgressBar;

.field private final m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

.field private n:I

.field private o:I

.field private p:[Landroid/net/Uri;

.field private q:[Ljava/lang/String;

.field private r:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "#666666"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lfzi;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V
    .locals 10

    const/4 v5, 0x2

    const/4 v9, 0x1

    const/16 v8, 0x11

    const/4 v1, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, p1, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iput-object v0, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iput v9, p0, Lfzi;->e:I

    iput v5, p0, Lfzi;->n:I

    const/4 v0, 0x3

    iput v0, p0, Lfzi;->o:I

    const-string v0, "Context must not be null."

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "URL must not be null."

    invoke-static {p5, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput p3, p0, Lfzi;->o:I

    iput p4, p0, Lfzi;->n:I

    iput-object p5, p0, Lfzi;->f:Ljava/lang/String;

    invoke-static {p1}, Lbbv;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    iput-object v7, p0, Lfzi;->i:Landroid/content/res/Resources;

    iput-object v7, p0, Lfzi;->j:Landroid/view/LayoutInflater;

    :goto_0
    invoke-direct {p0}, Lfzi;->k()Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p0}, Lfzi;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setGravity(I)V

    const-string v2, "[ +1 ]"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v2, v3, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lfzi;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v7, p0, Lfzi;->d:Lfwc;

    iput-object v7, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    iput-object v7, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    iput-object v7, p0, Lfzi;->b:Landroid/widget/FrameLayout;

    iput-object v7, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    :goto_1
    return-void

    :cond_0
    invoke-direct {p0}, Lfzi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lfzi;->i:Landroid/content/res/Resources;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lfzi;->j:Landroid/view/LayoutInflater;

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v9}, Lfzi;->setFocusable(Z)V

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v2, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v2, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v2}, Lfzi;->addView(Landroid/view/View;)V

    new-instance v2, Lfzj;

    invoke-direct {v2, p0, p1}, Lfzj;-><init>(Lfzi;Landroid/content/Context;)V

    iput-object v2, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    iget-object v2, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v2, v7}, Landroid/widget/CompoundButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v2, Lfwc;

    invoke-direct {v2, p1}, Lfwc;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Lfwc;->setFocusable(Z)V

    invoke-virtual {v2}, Lfwc;->b()V

    invoke-virtual {v2}, Lfwc;->a()V

    iget v3, p0, Lfzi;->o:I

    iget v4, p0, Lfzi;->n:I

    invoke-static {v3, v4}, Lfzi;->a(II)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v5, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    invoke-virtual {v2, v3}, Lfwc;->a(F)V

    sget v3, Lfzi;->h:I

    invoke-virtual {v2, v3}, Lfwc;->a(I)V

    invoke-virtual {v2, v1}, Lfwc;->setVisibility(I)V

    iput-object v2, p0, Lfzi;->d:Lfwc;

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    iput-object v2, p0, Lfzi;->b:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lfzi;->b:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v6, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v4, v5, v6, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, v0}, Lfzi;->a(Landroid/graphics/Point;)V

    new-instance v2, Landroid/widget/ProgressBar;

    const v3, 0x1010288    # android.R.attr.progressBarStyleSmallInverse

    invoke-direct {v2, p1, v7, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setFocusable(Z)V

    invoke-virtual {v2, v9}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iput-object v2, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, p0, Lfzi;->b:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v4, v5, v0, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v2, v0

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_2

    iget-object v1, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    invoke-virtual {p0}, Lfzi;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    invoke-direct {v4, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;-><init>(Landroid/content/Context;)V

    const/16 v3, 0x8

    invoke-virtual {v4, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    aput-object v4, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lfzi;->d()V

    goto/16 :goto_1
.end method

.method private static a(II)I
    .locals 2

    const/16 v0, 0xd

    packed-switch p0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/16 v0, 0xf

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xb

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/graphics/Point;)V
    .locals 4

    iget v0, p1, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    const/4 v1, 0x1

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-virtual {p0}, Lfzi;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Point;->y:I

    iget v0, p1, Landroid/graphics/Point;->y:I

    iput v0, p1, Landroid/graphics/Point;->x:I

    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lfzi;->i:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfzi;->i:Landroid/content/res/Resources;

    iget-object v1, p0, Lfzi;->i:Landroid/content/res/Resources;

    const-string v2, "string"

    const-string v3, "com.google.android.gms"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private e()Landroid/content/Context;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lfzi;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "PlusOneButtonView"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonView"

    const-string v1, "Google Play services is not installed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lfzi;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v0, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v3, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lfzi;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v0, v2, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v4, v2

    iget-object v2, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v5, v2

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    iget-object v6, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v7, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v7, v7, v2

    invoke-virtual {v7}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz v0, :cond_1

    invoke-virtual {v6, v3, v1, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    move v0, v1

    :goto_1
    iget-object v7, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v7, v7, v2

    invoke-virtual {v7, v6}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v6, v4, v1, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private h()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v1, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lfzi;->i:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v0, p0, Lfzi;->e:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    const-string v1, "plus_one_description_standard"

    invoke-direct {p0, v1}, Lfzi;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    iget-object v2, p0, Lfzi;->i:Landroid/content/res/Resources;

    iget-object v3, p0, Lfzi;->i:Landroid/content/res/Resources;

    iget v0, p0, Lfzi;->o:I

    packed-switch v0, :pswitch_data_1

    const-string v0, "ic_plusone_standard"

    :goto_2
    const-string v4, "drawable"

    const-string v5, "com.google.android.gms"

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    const-string v0, "ic_plusone_small"

    goto :goto_2

    :pswitch_1
    const-string v0, "ic_plusone_medium"

    goto :goto_2

    :pswitch_2
    const-string v0, "ic_plusone_tall"

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    const-string v1, "plus_one_description_remove"

    invoke-direct {p0, v1}, Lfzi;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    const-string v1, "plus_one_description"

    invoke-direct {p0, v1}, Lfzi;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    const-string v1, "plus_one_description_standard"

    invoke-direct {p0, v1}, Lfzi;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private i()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lfzi;->n:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lfzi;->d:Lfwc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfwc;->a([Ljava/lang/String;)V

    iget-object v0, p0, Lfzi;->d:Lfwc;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lfwc;->setVisibility(I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lfzi;->d:Lfwc;

    iget-object v1, p0, Lfzi;->q:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfwc;->a([Ljava/lang/String;)V

    iget-object v0, p0, Lfzi;->d:Lfwc;

    invoke-virtual {v0, v2}, Lfwc;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lfzi;->d:Lfwc;

    iget-object v1, p0, Lfzi;->r:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfwc;->a([Ljava/lang/String;)V

    iget-object v0, p0, Lfzi;->d:Lfwc;

    invoke-virtual {v0, v2}, Lfwc;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private j()V
    .locals 11

    const/16 v10, 0x8

    const/4 v0, 0x0

    iget-object v1, p0, Lfzi;->p:[Landroid/net/Uri;

    if-eqz v1, :cond_2

    iget v1, p0, Lfzi;->n:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    invoke-direct {p0}, Lfzi;->k()Landroid/graphics/Point;

    move-result-object v3

    iget v1, v3, Landroid/graphics/Point;->y:I

    iput v1, v3, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v4, v1

    iget-object v1, p0, Lfzi;->p:[Landroid/net/Uri;

    array-length v5, v1

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_3

    if-ge v2, v5, :cond_0

    iget-object v1, p0, Lfzi;->p:[Landroid/net/Uri;

    aget-object v1, v1, v2

    :goto_1
    if-nez v1, :cond_1

    iget-object v1, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v1, v1, v2

    invoke-virtual {v1, v10}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    iget-object v6, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v6, v6, v2

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    iget v8, v3, Landroid/graphics/Point;->x:I

    iget v9, v3, Landroid/graphics/Point;->y:I

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v7}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v6, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v6, v6, v2

    iget v7, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v6, v1, v7}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v1, v1

    :goto_3
    if-ge v0, v1, :cond_3

    iget-object v2, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v10}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    invoke-direct {p0}, Lfzi;->g()V

    return-void
.end method

.method private k()Landroid/graphics/Point;
    .locals 8

    const/16 v1, 0x18

    const/16 v0, 0x14

    const/4 v3, 0x1

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    iget v2, p0, Lfzi;->o:I

    packed-switch v2, :pswitch_data_0

    const/16 v0, 0x26

    move v7, v1

    move v1, v0

    move v0, v7

    :goto_0
    invoke-virtual {p0}, Lfzi;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    int-to-float v1, v1

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    int-to-float v0, v0

    invoke-static {v3, v0, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    new-instance v2, Landroid/graphics/Point;

    float-to-double v3, v1

    add-double/2addr v3, v5

    double-to-int v1, v3

    float-to-double v3, v0

    add-double/2addr v3, v5

    double-to-int v0, v3

    invoke-direct {v2, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v2

    :pswitch_0
    const/16 v1, 0x20

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xe

    goto :goto_0

    :pswitch_2
    const/16 v1, 0x32

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lfzi;->i:Landroid/content/res/Resources;

    const-string v1, "layout"

    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lfzi;->j:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lfzi;->i:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final a()V
    .locals 10

    const/16 v9, 0x11

    const/4 v8, -0x2

    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lfzi;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-direct {p0}, Lfzi;->k()Landroid/graphics/Point;

    move-result-object v0

    iget-object v1, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v6, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v4, v5, v6, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v4}, Landroid/widget/CompoundButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, v0}, Lfzi;->a(Landroid/graphics/Point;)V

    iget-object v1, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v4, v5, v0, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v0, p0, Lfzi;->n:I

    if-ne v0, v2, :cond_1

    iget-object v1, p0, Lfzi;->d:Lfwc;

    iget v0, p0, Lfzi;->o:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "global_count_bubble_standard"

    :goto_1
    invoke-static {v0}, Lbkn;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfwc;->a(Landroid/net/Uri;)V

    :goto_2
    invoke-direct {p0}, Lfzi;->j()V

    iget-object v4, p0, Lfzi;->d:Lfwc;

    iget v0, p0, Lfzi;->n:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    :goto_3
    iget v1, p0, Lfzi;->o:I

    if-ne v1, v7, :cond_2

    move v1, v2

    :goto_4
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iget v1, p0, Lfzi;->o:I

    if-ne v1, v7, :cond_3

    move v1, v3

    :goto_5
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v4, v0}, Lfwc;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v0, p0, Lfzi;->o:I

    iget v1, p0, Lfzi;->n:I

    invoke-static {v0, v1}, Lfzi;->a(II)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lfzi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v7, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iget-object v1, p0, Lfzi;->d:Lfwc;

    invoke-virtual {v1, v0}, Lfwc;->a(F)V

    iget-object v4, p0, Lfzi;->d:Lfwc;

    const/high16 v0, 0x40400000    # 3.0f

    invoke-virtual {p0}, Lfzi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v1, v0

    const/high16 v0, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lfzi;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v2, v0, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iget v5, p0, Lfzi;->n:I

    if-ne v5, v7, :cond_4

    :goto_6
    iget v5, p0, Lfzi;->o:I

    if-ne v5, v7, :cond_5

    iget v5, p0, Lfzi;->n:I

    if-ne v5, v2, :cond_5

    :goto_7
    invoke-virtual {v4, v0, v3, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    iget v0, p0, Lfzi;->o:I

    if-ne v0, v7, :cond_6

    iget v0, p0, Lfzi;->n:I

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lfzi;->d:Lfwc;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lfzi;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_8
    invoke-virtual {p0}, Lfzi;->requestLayout()V

    goto/16 :goto_0

    :pswitch_0
    const-string v0, "global_count_bubble_medium"

    goto/16 :goto_1

    :pswitch_1
    const-string v0, "global_count_bubble_small"

    goto/16 :goto_1

    :pswitch_2
    const-string v0, "global_count_bubble_tall"

    goto/16 :goto_1

    :cond_1
    iget-object v0, p0, Lfzi;->d:Lfwc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfwc;->a(Landroid/net/Uri;)V

    goto/16 :goto_2

    :pswitch_3
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v8, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto/16 :goto_3

    :pswitch_4
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto/16 :goto_3

    :cond_2
    move v1, v3

    goto/16 :goto_4

    :cond_3
    move v1, v2

    goto/16 :goto_5

    :cond_4
    move v0, v3

    goto :goto_6

    :cond_5
    move v1, v3

    goto :goto_7

    :cond_6
    iget-object v0, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lfzi;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v0, v0

    :goto_9
    if-ge v3, v0, :cond_7

    iget-object v1, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_7
    iget-object v0, p0, Lfzi;->k:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lfzi;->d:Lfwc;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final a(I)V
    .locals 1

    iget v0, p0, Lfzi;->o:I

    iput p1, p0, Lfzi;->e:I

    iput v0, p0, Lfzi;->o:I

    invoke-virtual {p0}, Lfzi;->a()V

    return-void
.end method

.method protected final a(Lfsu;)V
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lfsu;->a:Landroid/os/Bundle;

    const-string v1, "inline_annotations"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzi;->q:[Ljava/lang/String;

    invoke-direct {p0}, Lfzi;->i()V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p1, Lfsu;->a:Landroid/os/Bundle;

    const-string v2, "bubble_text"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lfzi;->r:[Ljava/lang/String;

    invoke-direct {p0}, Lfzi;->i()V

    iget-object v0, p1, Lfsu;->a:Landroid/os/Bundle;

    const-string v1, "profile_photo_uris"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lfzi;->p:[Landroid/net/Uri;

    invoke-direct {p0}, Lfzi;->j()V

    invoke-virtual {p1}, Lfsu;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lfzi;->f()V

    goto :goto_0

    :cond_1
    array-length v0, v1

    new-array v0, v0, [Landroid/net/Uri;

    array-length v2, v1

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lfzi;->d()V

    goto :goto_0
.end method

.method protected final a(Lftx;)V
    .locals 3

    iget-object v0, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v1, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lfzi;->m:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lftx;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lfzi;->a(I)V

    iget-object v0, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-direct {p0}, Lfzi;->h()V

    return-void
.end method

.method public c()V
    .locals 2

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lfzi;->a(I)V

    iget-object v0, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-direct {p0}, Lfzi;->h()V

    return-void
.end method

.method protected d()V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfzi;->a(I)V

    iget-object v0, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-direct {p0}, Lfzi;->h()V

    return-void
.end method

.method protected f()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfzi;->a(I)V

    iget-object v0, p0, Lfzi;->l:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-direct {p0}, Lfzi;->h()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lfzi;->g:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzi;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->performClick()Z

    iget-object v0, p0, Lfzi;->d:Lfwc;

    invoke-virtual {v0}, Lfwc;->performClick()Z

    iget-object v0, p0, Lfzi;->g:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lfzi;->g:Landroid/view/View$OnClickListener;

    invoke-super {p0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
