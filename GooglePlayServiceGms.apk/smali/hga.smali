.class public final Lhga;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/SharedPreferences;

.field private final b:J

.field private final c:I

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhga;-><init>(Landroid/content/Context;B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhga;->d:Landroid/content/Context;

    const-string v0, "com.google.android.gms.wallet.service.ow.TransactionContextStorage"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lhga;->a:Landroid/content/SharedPreferences;

    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lhga;->b:J

    const/16 v0, 0x10

    iput v0, p0, Lhga;->c:I

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lhgb;
    .locals 13

    const/4 v3, 0x0

    const/4 v12, 0x0

    iget-object v0, p0, Lhga;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v3

    :cond_0
    invoke-static {v0}, Lgtq;->a(Ljava/lang/String;)Lgtr;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lgtr;->a(J)J

    move-result-wide v10

    invoke-virtual {v0}, Lgtr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lgtr;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lgtr;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lgtr;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    new-instance v3, Landroid/accounts/Account;

    invoke-direct {v3, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-class v4, Ljbg;

    invoke-virtual {v0, v4}, Lgtr;->a(Ljava/lang/Class;)Lizs;

    move-result-object v4

    check-cast v4, Ljbg;

    invoke-virtual {v0, v12}, Lgtr;->a(Z)Z

    move-result v5

    invoke-virtual {v0}, Lgtr;->c()Ljava/lang/String;

    invoke-virtual {v0, v12}, Lgtr;->a(Z)Z

    move-result v6

    sget-object v7, Lizv;->f:[Ljava/lang/String;

    invoke-virtual {v0}, Lgtr;->c()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-static {}, Lgtq;->c()Ljava/util/regex/Pattern;

    move-result-object v7

    const/4 v9, -0x1

    invoke-virtual {v7, v8, v9}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v7

    :cond_2
    invoke-virtual {v0, v12}, Lgtr;->a(Z)Z

    move-result v8

    invoke-virtual {v0, v12}, Lgtr;->a(Z)Z

    move-result v9

    new-instance v0, Lhgb;

    invoke-direct/range {v0 .. v11}, Lhgb;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljbg;ZZ[Ljava/lang/String;ZZJ)V

    move-object v3, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lhgb;)V
    .locals 5

    iget-object v0, p0, Lhga;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lhga;->b:J

    add-long/2addr v1, v3

    iput-wide v1, p2, Lhgb;->a:J

    invoke-virtual {p2}, Lhgb;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    new-instance v0, Lhgc;

    iget-object v1, p0, Lhga;->d:Landroid/content/Context;

    iget-object v2, p0, Lhga;->a:Landroid/content/SharedPreferences;

    iget v3, p0, Lhga;->c:I

    invoke-direct {v0, v1, v2, v3}, Lhgc;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhgc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
