.class public Ldf;
.super Lds;
.source "SourceFile"

# interfaces
.implements Ljava/util/Map;


# instance fields
.field a:Ldm;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lds;-><init>()V

    return-void
.end method

.method private b()Ldm;
    .locals 1

    iget-object v0, p0, Ldf;->a:Ldm;

    if-nez v0, :cond_0

    new-instance v0, Ldg;

    invoke-direct {v0, p0}, Ldg;-><init>(Ldf;)V

    iput-object v0, p0, Ldf;->a:Ldm;

    :cond_0
    iget-object v0, p0, Ldf;->a:Ldm;

    return-object v0
.end method


# virtual methods
.method public entrySet()Ljava/util/Set;
    .locals 2

    invoke-direct {p0}, Ldf;->b()Ldm;

    move-result-object v0

    iget-object v1, v0, Ldm;->b:Ldo;

    if-nez v1, :cond_0

    new-instance v1, Ldo;

    invoke-direct {v1, v0}, Ldo;-><init>(Ldm;)V

    iput-object v1, v0, Ldm;->b:Ldo;

    :cond_0
    iget-object v0, v0, Ldm;->b:Ldo;

    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 2

    invoke-direct {p0}, Ldf;->b()Ldm;

    move-result-object v0

    iget-object v1, v0, Ldm;->c:Ldp;

    if-nez v1, :cond_0

    new-instance v1, Ldp;

    invoke-direct {v1, v0}, Ldp;-><init>(Ldm;)V

    iput-object v1, v0, Ldm;->c:Ldp;

    :cond_0
    iget-object v0, v0, Ldm;->c:Ldp;

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Ldf;->h:I

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lds;->f:[I

    array-length v1, v1

    if-ge v1, v0, :cond_1

    iget-object v1, p0, Lds;->f:[I

    iget-object v2, p0, Lds;->g:[Ljava/lang/Object;

    invoke-super {p0, v0}, Lds;->a(I)V

    iget v0, p0, Lds;->h:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lds;->f:[I

    iget v3, p0, Lds;->h:I

    invoke-static {v1, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lds;->g:[Ljava/lang/Object;

    iget v3, p0, Lds;->h:I

    shl-int/lit8 v3, v3, 0x1

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    iget v0, p0, Lds;->h:I

    invoke-static {v1, v2, v0}, Lds;->a([I[Ljava/lang/Object;I)V

    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Ldf;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-void
.end method

.method public values()Ljava/util/Collection;
    .locals 2

    invoke-direct {p0}, Ldf;->b()Ldm;

    move-result-object v0

    iget-object v1, v0, Ldm;->d:Ldr;

    if-nez v1, :cond_0

    new-instance v1, Ldr;

    invoke-direct {v1, v0}, Ldr;-><init>(Ldm;)V

    iput-object v1, v0, Ldm;->d:Ldr;

    :cond_0
    iget-object v0, v0, Ldm;->d:Ldr;

    return-object v0
.end method
