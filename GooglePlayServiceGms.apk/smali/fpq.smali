.class public final Lfpq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfuc;


# static fields
.field private static a:Lfpq;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/LinkedList;

.field private e:Lftz;

.field private f:Lftx;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, p1, v0}, Lfpq;-><init>(Landroid/content/Context;Lftz;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lftz;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfpq;->c:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfpq;->b:Landroid/content/Context;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfpq;->d:Ljava/util/LinkedList;

    iput-object p2, p0, Lfpq;->e:Lftz;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lfpq;
    .locals 1

    sget-object v0, Lfpq;->a:Lfpq;

    if-nez v0, :cond_0

    new-instance v0, Lfpq;

    invoke-direct {v0, p0}, Lfpq;-><init>(Landroid/content/Context;)V

    sput-object v0, Lfpq;->a:Lfpq;

    :cond_0
    sget-object v0, Lfpq;->a:Lfpq;

    return-object v0
.end method


# virtual methods
.method public final P_()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lfpq;->f:Lftx;

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lfpq;->f:Lftx;

    return-void
.end method

.method public final a(Lbbo;Lfxi;Ljava/lang/String;)V
    .locals 9

    const/4 v3, 0x0

    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lfxi;->a()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_2

    invoke-virtual {p2, v4}, Lfxi;->b(I)Lfxh;

    move-result-object v0

    invoke-interface {v0}, Lfxh;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfpq;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lfpq;->c:Ljava/util/HashMap;

    new-instance v6, Lfpr;

    invoke-interface {v0}, Lfxh;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Lfxh;->c()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v1, v7, v8, v3}, Lfpr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    invoke-virtual {v2, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lfpq;->c:Ljava/util/HashMap;

    invoke-interface {v0}, Lfxh;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpr;

    iget-object v1, p0, Lfpq;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_1

    iget-object v1, p0, Lfpq;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfps;

    invoke-interface {v1, v0}, Lfps;->a(Lfpr;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    iget-object v1, p0, Lfpq;->f:Lftx;

    sget-object v0, Lfsr;->E:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, p0, v0, p3}, Lftx;->b(Lfuc;ILjava/lang/String;)V

    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lfpq;->f:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfpq;->f:Lftx;

    goto :goto_2
.end method

.method public final a(Lfps;)V
    .locals 1

    iget-object v0, p0, Lfpq;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lfpq;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfpq;->f:Lftx;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfpq;->f:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpq;->f:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfpq;->f:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lfpq;->f:Lftx;

    :cond_2
    return-void
.end method

.method public final a(Lfps;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lfpq;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpr;

    if-nez v0, :cond_2

    iget-object v0, p0, Lfpq;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpq;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lfpq;->f:Lftx;

    if-nez v0, :cond_1

    iget-object v0, p0, Lfpq;->e:Lftz;

    iget-object v1, p0, Lfpq;->b:Landroid/content/Context;

    invoke-static {v0, v1, p0, p0, p3}, Lfob;->a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lfpq;->f:Lftx;

    iget-object v0, p0, Lfpq;->f:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-interface {p1, v0}, Lfps;->a(Lfpr;)V

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    iget-object v1, p0, Lfpq;->f:Lftx;

    sget-object v0, Lfsr;->E:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x0

    invoke-interface {v1, p0, v0, v2}, Lftx;->b(Lfuc;ILjava/lang/String;)V

    return-void
.end method
