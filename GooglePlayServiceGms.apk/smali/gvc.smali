.class public final Lgvc;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lgxt;
.implements Lgyq;


# static fields
.field static final a:I

.field private static d:I


# instance fields
.field private Y:Lixo;

.field private Z:Ljava/lang/Integer;

.field private aa:Lgxt;

.field private ab:Lgzl;

.field private ac:I

.field private ad:Z

.field b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private e:Z

.field private f:Landroid/view/View;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lgvc;->a:I

    const/4 v0, 0x0

    sput v0, Lgvc;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-boolean v1, p0, Lgvc;->e:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lgvc;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lgvc;->Z:Ljava/lang/Integer;

    iput-boolean v1, p0, Lgvc;->ad:Z

    return-void
.end method

.method private M()V
    .locals 2

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-boolean v1, p0, Lgvc;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Z)V

    :cond_0
    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgvc;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;Ljava/util/Collection;ZZ)Lgvc;
    .locals 3

    new-instance v0, Lgvc;

    invoke-direct {v0}, Lgvc;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "addressEntryParams"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "addressHints"

    invoke-static {v1, v2, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v2, "phoneNumberRequired"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "launchedToAddPhoneNumber"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lgvc;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private d(I)V
    .locals 1

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez v0, :cond_0

    iput p1, p0, Lgvc;->g:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(I)V

    const/4 v0, 0x0

    iput v0, p0, Lgvc;->g:I

    goto :goto_0
.end method


# virtual methods
.method public final J()V
    .locals 4

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    invoke-virtual {p0}, Lgvc;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0168    # com.google.android.gms.R.string.wallet_error_phone_invalid

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    :cond_0
    return-void
.end method

.method public final K()V
    .locals 2

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    const/16 v1, 0x43

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(C)V

    return-void
.end method

.method public final L()V
    .locals 2

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    const/16 v1, 0x5a

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(C)V

    return-void
.end method

.method public final R_()Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->I:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->R_()Z

    move-result v1

    iget-boolean v2, p0, Lgvc;->ad:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final S_()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->I:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->S_()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lgvc;->ad:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const/4 v3, 0x0

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "addressEntryParams"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    const-string v1, "phoneNumberRequired"

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lgvc;->ad:Z

    iput v3, p0, Lgvc;->ac:I

    const v1, 0x7f04012b    # com.google.android.gms.R.layout.wallet_fragment_address_and_phone_number

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lgvc;->f:Landroid/view/View;

    iget-object v1, p0, Lgvc;->f:Landroid/view/View;

    const v2, 0x7f0a0317    # com.google.android.gms.R.id.phone_number

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgvc;->ad:Z

    if-eqz v1, :cond_5

    const-string v1, "launchedToAddPhoneNumber"

    invoke-virtual {v4, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    new-instance v1, Lgzl;

    const v5, 0x7f0b0168    # com.google.android.gms.R.string.wallet_error_phone_invalid

    invoke-virtual {p0, v5}, Lgvc;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Lgzl;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lgvc;->ab:Lgzl;

    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v5, p0, Lgvc;->ab:Lgzl;

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "addressHints"

    const-class v6, Lipv;

    invoke-static {v4, v1, v6}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Lgup;

    invoke-direct {v4, v1}, Lgup;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    instance-of v1, v1, Lguk;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v1, Lguk;

    invoke-interface {v1}, Lguk;->b()Lguh;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setThreshold(I)V

    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v4, Lgxu;

    iget-object v6, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v4, v6, v5}, Lgxu;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    if-nez p3, :cond_1

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lgvc;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lgvd;

    iget-object v4, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v1, v4, v5}, Lgvd;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/util/ArrayList;)V

    new-array v4, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v4}, Lgvd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    :goto_1
    iget-object v1, p0, Lgvc;->f:Landroid/view/View;

    const v3, 0x7f0a0316    # com.google.android.gms.R.id.address_entry_fragment_holder

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lgvc;->Z:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setId(I)V

    invoke-virtual {p0}, Lgvc;->k()Lu;

    move-result-object v1

    iget-object v3, p0, Lgvc;->Z:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object v1, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v1, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez v1, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    move-result-object v0

    iput-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {p0}, Lgvc;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgvc;->Z:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v3, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1, v3}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_2
    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lgxt;)V

    invoke-direct {p0}, Lgvc;->M()V

    if-nez p3, :cond_3

    if-eqz v2, :cond_3

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    :cond_3
    iget-object v0, p0, Lgvc;->f:Landroid/view/View;

    return-object v0

    :cond_4
    new-instance v1, Lguh;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v1, v4}, Lguh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    move v2, v3

    goto :goto_1
.end method

.method public final a()Lixo;
    .locals 1

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Lixo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lgvc;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgvc;->Y:Lixo;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lgxt;)V
    .locals 1

    iput-object p1, p0, Lgvc;->aa:Lgxt;

    if-eqz p1, :cond_0

    iget v0, p0, Lgvc;->ac:I

    if-eqz v0, :cond_0

    iget v0, p0, Lgvc;->ac:I

    invoke-interface {p1, v0}, Lgxt;->c(I)V

    :cond_0
    return-void
.end method

.method public final a(Lixo;)V
    .locals 1

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez v0, :cond_0

    iput-object p1, p0, Lgvc;->Y:Lixo;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgvc;->i:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lixo;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lgvc;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lgvc;->Y:Lixo;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez v0, :cond_1

    iput-object p1, p0, Lgvc;->h:Ljava/lang/String;

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lgvc;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lgvc;->e:Z

    invoke-direct {p0}, Lgvc;->M()V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "addressEntryFragmentId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "addressEntryFragmentId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgvc;->Z:Ljava/lang/Integer;

    :goto_0
    return-void

    :cond_0
    sget v0, Lgvc;->d:I

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lgvc;->d:I

    sget v0, Lgvc;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgvc;->Z:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgvc;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    iput p1, p0, Lgvc;->ac:I

    iget-object v0, p0, Lgvc;->aa:Lgxt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvc;->aa:Lgxt;

    invoke-interface {v0, p1}, Lgxt;->c(I)V

    :cond_0
    iget-object v0, p0, Lgvc;->ab:Lgzl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgvc;->ab:Lgzl;

    invoke-virtual {v0, p1}, Lgzl;->a(I)V

    :cond_1
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgvc;->e:Z

    const-string v0, "pendingSelectedCountry"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgvc;->g:I

    iget v0, p0, Lgvc;->g:I

    invoke-direct {p0, v0}, Lgvc;->d(I)V

    const-string v0, "hasPendingAddress"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgvc;->i:Z

    iget-boolean v0, p0, Lgvc;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "pendingAddress"

    const-class v1, Lixo;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lixo;

    iput-object v0, p0, Lgvc;->Y:Lixo;

    :cond_0
    const-string v0, "pendingPhoneNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvc;->h:Ljava/lang/String;

    :cond_1
    iget-boolean v0, p0, Lgvc;->i:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgvc;->Y:Lixo;

    invoke-virtual {p0, v0}, Lgvc;->a(Lixo;)V

    :cond_2
    iget-object v0, p0, Lgvc;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgvc;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lgvc;->a(Ljava/lang/String;)V

    :cond_3
    iget v0, p0, Lgvc;->g:I

    if-eqz v0, :cond_4

    iget v0, p0, Lgvc;->g:I

    invoke-direct {p0, v0}, Lgvc;->d(I)V

    :cond_4
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "enabled"

    iget-boolean v1, p0, Lgvc;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "hasPendingAddress"

    iget-boolean v1, p0, Lgvc;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-boolean v0, p0, Lgvc;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "pendingAddress"

    iget-object v1, p0, Lgvc;->Y:Lixo;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    iget v0, p0, Lgvc;->g:I

    if-eqz v0, :cond_1

    const-string v0, "pendingSelectedCountry"

    iget v1, p0, Lgvc;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lgvc;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, "pendingPhoneNumber"

    iget-object v1, p0, Lgvc;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lgvc;->Z:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    const-string v0, "addressEntryFragmentId"

    iget-object v1, p0, Lgvc;->Z:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3
    return-void
.end method

.method public final i()Z
    .locals 2

    iget-object v0, p0, Lgvc;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v1, p0, Lgvc;->ad:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    move-result v0

    :cond_0
    return v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-ne v0, v1, :cond_5

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgur;

    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgvc;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->focusSearch(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :cond_0
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    check-cast v1, Lgxu;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lgur;->c()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit8 v6, v4, 0x4

    if-eqz v6, :cond_1

    const-string v6, "best"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    and-int/lit8 v6, v4, 0x2

    if-eqz v6, :cond_2

    const-string v6, "contact"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    const-string v4, "phone"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "|"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_4

    sget v6, Lgvc;->a:I

    sub-int v6, v4, v6

    invoke-virtual {v5, v6, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Lgur;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const-string v4, "phone_number_entry"

    const-string v5, "autocomplete_phone_number"

    invoke-virtual {v1}, Lgxu;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lgxu;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v3, v4, v5, v2, v0}, Lgsl;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Long;)V

    :cond_5
    return-void

    :cond_6
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final y()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    return-void
.end method
