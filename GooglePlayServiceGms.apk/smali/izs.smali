.class public abstract Lizs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected C:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lizs;->C:I

    return-void
.end method

.method public static final a(Lizs;[B)Lizs;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Lizs;->b(Lizs;[BII)Lizs;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lizs;[BII)V
    .locals 3

    :try_start_0
    invoke-static {p1, p2, p3}, Lizn;->a([BII)Lizn;

    move-result-object v0

    invoke-virtual {p0, v0}, Lizs;->a(Lizn;)V

    invoke-virtual {v0}, Lizn;->a()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    return-void
.end method

.method public static final a(Lizs;)[B
    .locals 3

    invoke-virtual {p0}, Lizs;->a()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, v0, v1, v2}, Lizs;->a(Lizs;[BII)V

    return-object v0
.end method

.method public static final b(Lizs;[BII)Lizs;
    .locals 2

    :try_start_0
    invoke-static {p1, p2, p3}, Lizm;->a([BII)Lizm;

    move-result-object v0

    invoke-virtual {p0, v0}, Lizs;->a(Lizm;)Lizs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lizm;->a(I)V
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object p0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lizs;->C:I

    return v0
.end method

.method public abstract a(Lizm;)Lizs;
.end method

.method public a(Lizn;)V
    .locals 0

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lizs;->C:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lizs;->a()I

    :cond_0
    iget v0, p0, Lizs;->C:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lizt;->a(Lizs;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
