.class public final Lefs;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field static g:J


# instance fields
.field private A:Z

.field private B:Ljava/lang/Object;

.field private C:Leer;

.field private D:J

.field private E:J

.field private F:Z

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:Z

.field private L:Leeu;

.field private M:J

.field private N:J

.field private O:I

.field public a:Leeq;

.field b:Landroid/net/NetworkInfo$State;

.field c:I

.field d:Landroid/net/NetworkInfo$State;

.field e:I

.field f:Z

.field h:J

.field public i:J

.field public j:J

.field private k:Landroid/content/Context;

.field private l:Landroid/net/ConnectivityManager;

.field private m:Z

.field private n:J

.field private o:J

.field private p:I

.field private q:J

.field private r:I

.field private s:D

.field private t:I

.field private u:Ljava/util/Random;

.field private v:J

.field private w:Z

.field private x:J

.field private y:Z

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x1

    sput-wide v0, Lefs;->g:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lefs;->u:Ljava/util/Random;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefs;->w:Z

    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    iput-object v0, p0, Lefs;->b:Landroid/net/NetworkInfo$State;

    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    iput-object v0, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lefs;->f:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lefs;->B:Ljava/lang/Object;

    const/16 v0, 0x146c

    iput v0, p0, Lefs;->O:I

    iput-object p1, p0, Lefs;->k:Landroid/content/Context;

    iget-object v0, p0, Lefs;->k:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    new-instance v0, Leeq;

    iget-object v1, p0, Lefs;->k:Landroid/content/Context;

    const-string v2, "GCM_CONN_ALARM"

    invoke-direct {v0, v1, v2, p0}, Leeq;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    iput-object v0, p0, Lefs;->a:Leeq;

    iget-object v0, p0, Lefs;->a:Leeq;

    const-string v1, "com.google.android.intent.action.GCM_RECONNECT"

    iput-object v1, v0, Leeq;->b:Ljava/lang/String;

    iget-object v0, p0, Lefs;->a:Leeq;

    invoke-virtual {v0}, Leeq;->b()V

    iget-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lefs;->N:J

    :cond_0
    return-void
.end method

.method static synthetic a(Lefs;)Leeu;
    .locals 1

    iget-object v0, p0, Lefs;->L:Leeu;

    return-object v0
.end method

.method private a(Landroid/net/NetworkInfo$State;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setInternalNetworkState: type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    sget-object v0, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lefs;->w:Z

    iput-object p1, p0, Lefs;->b:Landroid/net/NetworkInfo$State;

    iput p2, p0, Lefs;->c:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "GCM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "A "

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lefs;->b:Landroid/net/NetworkInfo$State;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lefs;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lefs;->a:Leeq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lefs;->a:Leeq;

    invoke-virtual {v0}, Leeq;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lefs;->a:Leeq;

    iget-wide v3, v0, Leeq;->c:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "a "

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method private static a(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/net/ConnectivityManager;II)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-ge v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "reportInetCondition"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "GCM"

    const-string v2, "reportInetCondition not supported"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private d(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lefs;->F:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lefs;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefs;->m:Z

    iget-object v0, p0, Lefs;->a:Leeq;

    invoke-virtual {v0}, Leeq;->e()V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lefs;->F:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private i()Z
    .locals 1

    iget-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 1

    iget-object v0, p0, Lefs;->a:Leeq;

    iget-object v0, v0, Leeq;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lefs;->v:J

    iget-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lefs;->y:Z

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lefs;->a(Landroid/net/NetworkInfo$State;I)V

    :goto_0
    invoke-virtual {p0}, Lefs;->b()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lefs;->y:Z

    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lefs;->a(Landroid/net/NetworkInfo$State;I)V

    goto :goto_0
.end method

.method final a(IZ)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lefs;->K:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    iget v0, p0, Lefs;->c:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lefs;->C:Leer;

    iget v0, v0, Leer;->d:I

    iget v1, p0, Lefs;->O:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lefs;->C:Leer;

    const/16 v1, 0x1bb

    iput v1, v0, Leer;->d:I

    invoke-virtual {p0, v3}, Lefs;->a(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lefs;->C:Leer;

    iget v1, p0, Lefs;->O:I

    iput v1, v0, Leer;->d:I

    invoke-virtual {p0, v2}, Lefs;->c(Z)V

    invoke-virtual {p0, v2}, Lefs;->a(Z)V

    invoke-direct {p0}, Lefs;->j()V

    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->c()V

    goto :goto_0
.end method

.method public final a(Leer;)V
    .locals 0

    iput-object p1, p0, Lefs;->C:Leer;

    return-void
.end method

.method public final a(Leeu;)V
    .locals 0

    iput-object p1, p0, Lefs;->L:Leeu;

    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 8

    const/4 v0, 0x0

    const-wide/16 v6, 0x3e8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v3, p0, Lefs;->a:Leeq;

    invoke-virtual {v3}, Leeq;->a()J

    move-result-wide v3

    sub-long/2addr v3, v1

    iget-object v5, p0, Lefs;->a:Leeq;

    invoke-virtual {v5}, Leeq;->d()Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "ReconnectManager: next reconnect attempt in "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    div-long/2addr v3, v6

    invoke-static {v3, v4}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "s "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lefs;->a:Leeq;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Last network state notification: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lefs;->e:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", time: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lefs;->x:J

    sub-long/2addr v1, v3

    div-long/2addr v1, v6

    invoke-static {v1, v2}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s ago"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "active network type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lefs;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    iget v2, p0, Lefs;->c:I

    if-eq v1, v2, :cond_0

    const-string v1, "Type missmatch between polled and callback:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "active network type (polled): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "active network state (polled): "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lefs;->b:Landroid/net/NetworkInfo$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    iget-wide v0, p0, Lefs;->N:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lefs;->N:J

    sub-long/2addr v0, v2

    div-long/2addr v0, v6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lefs;->M:J

    div-long/2addr v1, v6

    invoke-static {v1, v2}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Network status: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lefs;->i()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "ON"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Previous Network status:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lefs;->y:Z

    if-eqz v0, :cond_9

    const-string v0, "ON"

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lefs;->w:Z

    if-eqz v0, :cond_a

    const-string v0, " NetworkSuspended"

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lefs;->A:Z

    if-eqz v0, :cond_b

    const-string v0, " InMobileHipriMode"

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lefs;->f:Z

    if-nez v0, :cond_c

    const-string v0, " NoNetworkStatusIcon"

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ReconnectManager OFF "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    :goto_7
    if-eqz v0, :cond_4

    const-string v0, ""

    :goto_8
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Connected"

    :goto_9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->f()Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "Connecting"

    :goto_a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    invoke-direct {p0}, Lefs;->h()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v0, 0x1

    goto :goto_7

    :cond_4
    const-string v0, "Connection disabled"

    goto :goto_8

    :cond_5
    const-string v0, ""

    goto :goto_9

    :cond_6
    const-string v0, ""

    goto :goto_a

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Disconnected, connected time: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lefs;->M:J

    div-long/2addr v1, v6

    invoke-static {v1, v2}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    const-string v0, "OFF"

    goto/16 :goto_2

    :cond_9
    const-string v0, "OFF"

    goto/16 :goto_3

    :cond_a
    const-string v0, ""

    goto/16 :goto_4

    :cond_b
    const-string v0, ""

    goto/16 :goto_5

    :cond_c
    const-string v0, ""

    goto/16 :goto_6
.end method

.method public final a(Z)V
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Leet;->d()I

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lefs;->C:Leer;

    iget v2, v2, Leer;->d:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lefs;->k:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-object v0, p0, Lefs;->C:Leer;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Leer;->b(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lefs;->C:Leer;

    invoke-virtual {v2}, Leer;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "ReconnectManager: skip retry, isActive()"

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lefs;->C:Leer;

    invoke-virtual {v2}, Leer;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v0, "ReconnectManager: skip retry, isConnected()"

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_5

    const-string v0, "ReconnectManager: connect()"

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lefs;->a:Leeq;

    iget-object v0, v0, Leeq;->a:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->j()V

    goto :goto_0

    :cond_5
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lefs;->k:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_6

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_6
    :try_start_1
    iget-boolean v2, p0, Lefs;->m:Z

    if-eqz v2, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Lefs;->a:Leeq;

    invoke-virtual {v4}, Leeq;->a()J

    move-result-wide v4

    cmp-long v6, v4, v2

    if-gez v6, :cond_7

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "alarm failed to fire: alarmTime="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", now="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lefs;->a(Ljava/lang/String;)V

    :cond_7
    const/4 v2, 0x1

    iput-boolean v2, p0, Lefs;->m:Z

    iget-object v2, p0, Lefs;->C:Leer;

    invoke-virtual {v2}, Leer;->k()Z

    move-result v3

    iget-object v2, p0, Lefs;->C:Leer;

    invoke-virtual {v2}, Leer;->l()J

    move-result-wide v4

    iget v2, p0, Lefs;->p:I

    int-to-long v6, v2

    cmp-long v2, v4, v6

    if-ltz v2, :cond_c

    move v2, v0

    :goto_1
    if-eqz v3, :cond_d

    if-eqz v2, :cond_d

    move v4, v0

    :goto_2
    if-nez v4, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "shouldResetReconnectTimer: lastConnectionWasOfMininumDuration="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastAttemptSuccessful="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    :cond_8
    if-eqz v4, :cond_9

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lefs;->b(Z)V

    :cond_9
    iget-object v0, p0, Lefs;->a:Leeq;

    iget-wide v1, p0, Lefs;->n:J

    invoke-virtual {v0, v1, v2}, Leeq;->a(J)V

    iget-wide v0, p0, Lefs;->n:J

    long-to-double v0, v0

    iget-wide v2, p0, Lefs;->s:D

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iget-wide v5, p0, Lefs;->n:J

    iget-wide v2, p0, Lefs;->q:J

    iget-boolean v7, p0, Lefs;->y:Z

    if-nez v7, :cond_a

    iget v7, p0, Lefs;->r:I

    if-lez v7, :cond_a

    iget v2, p0, Lefs;->r:I

    int-to-long v2, v2

    :cond_a
    cmp-long v7, v0, v2

    if-gez v7, :cond_e

    :goto_3
    iput-wide v0, p0, Lefs;->n:J

    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setReconAlarm: set delay to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v1, 0x3e8

    div-long v1, v5, v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s retry in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lefs;->n:J

    const-wide/16 v5, 0x3e8

    div-long/2addr v1, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v4, :cond_f

    const-string v0, " no backoff "

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    :cond_b
    monitor-exit p0

    goto/16 :goto_0

    :cond_c
    move v2, v1

    goto/16 :goto_1

    :cond_d
    move v4, v1

    goto/16 :goto_2

    :cond_e
    move-wide v0, v2

    goto :goto_3

    :cond_f
    const-string v0, " backoff"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4
.end method

.method public final b()V
    .locals 6

    const/16 v3, 0x2710

    const/4 v5, 0x0

    iget-object v0, p0, Lefs;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v0, "gms_max_reconnect_delay"

    const v1, 0x493e0

    invoke-static {v2, v0, v1}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lefs;->q:J

    const-string v0, "gms_max_no_network_reconnect_delay"

    const v1, 0x36ee80

    invoke-static {v2, v0, v1}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefs;->r:I

    const-string v0, "gms_min_connection_duration"

    const/16 v1, 0x78

    invoke-static {v2, v0, v1}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefs;->p:I

    const-string v0, "gms_min_reconnect_delay_short"

    const/16 v1, 0x1388

    invoke-static {v2, v0, v1}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefs;->I:I

    const-string v0, "gtalk_reconnect_variant_short"

    invoke-static {v2, v0, v3}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefs;->J:I

    const-string v0, "gms_min_reconnect_delay_long"

    invoke-static {v2, v0, v3}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefs;->G:I

    const-string v0, "gtalk_reconnect_variant_long"

    const/16 v1, 0x7530

    invoke-static {v2, v0, v1}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefs;->H:I

    const-string v0, "gtalk_short_network_downtime"

    const v1, 0x2932e0

    invoke-static {v2, v0, v1}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefs;->t:I

    iget-object v0, p0, Lefs;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gcm_http_save"

    sget-wide v3, Lefs;->g:J

    invoke-static {v0, v1, v3, v4}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lefs;->h:J

    const-string v0, "gcm_secure_port"

    const/16 v1, 0x146c

    invoke-static {v2, v0, v1}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefs;->O:I

    const-string v0, "gtalk_reconnect_backoff_multiplier"

    invoke-static {v2, v0}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    if-eqz v3, :cond_0

    :try_start_0
    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :cond_0
    :goto_0
    iput-wide v0, p0, Lefs;->s:D

    const-string v0, "gcm_fallback_reconnect"

    invoke-static {v2, v0, v5}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lefs;->K:Z

    invoke-virtual {p0, v5}, Lefs;->b(Z)V

    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 5

    iget-wide v0, p0, Lefs;->n:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lefs;->n:J

    iget-wide v2, p0, Lefs;->o:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, Lefs;->G:I

    iget-object v1, p0, Lefs;->u:Ljava/util/Random;

    iget v2, p0, Lefs;->H:I

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Lefs;->o:J

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "resetReconnectionTimer "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lefs;->o:J

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    iget-wide v0, p0, Lefs;->o:J

    iput-wide v0, p0, Lefs;->n:J

    goto :goto_0

    :cond_1
    iget v0, p0, Lefs;->I:I

    iget-object v1, p0, Lefs;->u:Ljava/util/Random;

    iget v2, p0, Lefs;->J:I

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Lefs;->o:J

    goto :goto_1
.end method

.method final c(Z)V
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lefs;->f:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/16 v0, 0x64

    :cond_1
    iget-object v1, p0, Lefs;->B:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {p0}, Lefs;->e()I

    move-result v3

    invoke-static {v2, v3, v0}, Lefs;->a(Landroid/net/ConnectivityManager;II)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefs;->f:Z

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final c()Z
    .locals 4

    iget-wide v0, p0, Lefs;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lefs;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lefs;->i()Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lefs;->m:Z

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lefs;->a:Leeq;

    invoke-virtual {v2}, Leeq;->a()J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-gez v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "alarm failed to fire: alarmTime="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", now="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lefs;->m:Z

    iget-object v0, p0, Lefs;->a:Leeq;

    iget-wide v1, p0, Lefs;->n:J

    invoke-virtual {v0, v1, v2}, Leeq;->a(J)V

    iget-wide v0, p0, Lefs;->n:J

    long-to-double v0, v0

    iget-wide v2, p0, Lefs;->s:D

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iget-wide v2, p0, Lefs;->n:J

    iget-wide v4, p0, Lefs;->q:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_5

    :goto_1
    iput-wide v0, p0, Lefs;->n:J

    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GCM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Set HTTP resend alarm "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " next: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lefs;->n:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    monitor-exit p0

    goto/16 :goto_0

    :cond_4
    monitor-exit p0

    goto/16 :goto_0

    :cond_5
    iget-wide v0, p0, Lefs;->q:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0
.end method

.method final f()V
    .locals 1

    invoke-direct {p0}, Lefs;->g()V

    invoke-direct {p0}, Lefs;->j()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lefs;->c(Z)V

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "GCM"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceive intent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    :cond_2
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lefs;->d(Z)V

    iget-object v0, p0, Lefs;->C:Leer;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Leer;->b(I)V

    goto :goto_0

    :cond_3
    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lefs;->d(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lefs;->a(Z)V

    goto :goto_0

    :cond_4
    const-string v1, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lefs;->a(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lefs;->C:Leer;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Leer;->b(I)V

    goto :goto_0

    :cond_6
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_30

    iget-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-wide v0, p0, Lefs;->N:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lefs;->N:J

    :cond_7
    :goto_1
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    const-string v1, "noConnectivity"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-nez v1, :cond_8

    if-eqz v0, :cond_8

    iget-object v2, p0, Lefs;->k:Landroid/content/Context;

    invoke-static {v2}, Lefr;->a(Landroid/content/Context;)Lefr;

    move-result-object v2

    invoke-virtual {v2}, Lefr;->b()V

    :cond_8
    iget-object v2, p0, Lefs;->k:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GCM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "reason"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "isFailover"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Connectivity status: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_f

    const-string v1, "NO CONNECTIVITY"

    :goto_2
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " type="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " state="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v2, :cond_12

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, " reason="

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v3, :cond_13

    const-string v1, " failover"

    :goto_6
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    const-string v1, "otherNetwork"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    if-eqz v1, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    net2.type= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " net2.state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    :cond_9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lefs;->x:J

    if-nez v0, :cond_14

    const-string v0, "GCM"

    const-string v1, "Connection event without network info, shouldn\'t happen !"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    iput v0, p0, Lefs;->e:I

    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    iput-object v0, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    :goto_7
    iget-object v0, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    const/4 v0, -0x1

    if-eqz v2, :cond_15

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "GCM"

    const-string v1, "ACTIVE NETWORK NOT CONNECTED"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lefs;->y:Z

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lefs;->i:J

    :goto_8
    iget v1, p0, Lefs;->e:I

    const/4 v3, 0x5

    if-ne v1, v3, :cond_c

    iget-object v1, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v3, :cond_19

    if-eqz v0, :cond_b

    const/4 v1, 0x6

    if-ne v0, v1, :cond_c

    :cond_b
    iget-object v1, p0, Lefs;->C:Leer;

    invoke-virtual {v1}, Leer;->e()Z

    move-result v1

    if-nez v1, :cond_16

    const/4 v1, 0x0

    :goto_9
    iput-boolean v1, p0, Lefs;->A:Z

    iget-boolean v1, p0, Lefs;->A:Z

    if-eqz v1, :cond_18

    const-string v1, "networkStateChanged for MOBILE_HIPRI: set MOBILE_HIPRI=true"

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    :cond_c
    :goto_a
    iget v1, p0, Lefs;->e:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_d

    iget-object v1, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    sget-object v3, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v3, :cond_1a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, p0, Lefs;->z:J

    :cond_d
    :goto_b
    iget-boolean v1, p0, Lefs;->y:Z

    if-eqz v1, :cond_2a

    invoke-virtual {p0}, Lefs;->d()V

    iget-boolean v1, p0, Lefs;->A:Z

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lefs;->C:Leer;

    invoke-virtual {v1}, Leer;->e()Z

    move-result v1

    if-eqz v1, :cond_1b

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "networkStateChanged: active_net_type="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", current_net_type="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lefs;->c:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", in MOBILE_HIPRI, ignore"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    const/4 v1, 0x1

    :goto_c
    if-eqz v1, :cond_1d

    const-string v0, "Keep hipri network"

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    iget-wide v0, p0, Lefs;->N:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    iget-wide v0, p0, Lefs;->M:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lefs;->N:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lefs;->M:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lefs;->N:J

    goto/16 :goto_1

    :cond_f
    const-string v1, ""

    goto/16 :goto_2

    :cond_10
    const-string v1, "unknown"

    goto/16 :goto_3

    :cond_11
    const-string v1, "unknown"

    goto/16 :goto_4

    :cond_12
    const-string v1, ""

    goto/16 :goto_5

    :cond_13
    const-string v1, ""

    goto/16 :goto_6

    :cond_14
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    iput v1, p0, Lefs;->e:I

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    iput-object v0, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    goto/16 :goto_7

    :cond_15
    const/4 v1, 0x0

    iput-boolean v1, p0, Lefs;->y:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lefs;->j:J

    goto/16 :goto_8

    :cond_16
    iget-object v1, p0, Lefs;->C:Leer;

    invoke-virtual {v1}, Leer;->n()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v3

    const/4 v4, 0x3

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    const/4 v5, 0x2

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    const/4 v5, 0x1

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    const/4 v5, 0x0

    aget-byte v3, v3, v5

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "--- requestRouteToHost for TYPE_MOBILE_HIPRI, host_addr="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lefs;->l:Landroid/net/ConnectivityManager;

    const/4 v4, 0x5

    invoke-virtual {v1, v4, v3}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v1

    if-nez v1, :cond_17

    const-string v1, "GCM"

    const-string v3, "requestRouteToHost: failed!"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_17
    const/4 v1, 0x1

    goto/16 :goto_9

    :cond_18
    const-string v1, "networkStateChanged for MOBILE_HIPRI: MOBILE_HIPRI=false, ensureRouteOverMobileHipriNetworkInterface() failed"

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_19
    iget-object v1, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    sget-object v3, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v3, :cond_c

    const-string v1, "networkStateChanged: MOBILE_HIPRI disconnected"

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lefs;->A:Z

    goto/16 :goto_a

    :cond_1a
    iget-object v1, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v3, :cond_d

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lefs;->z:J

    goto/16 :goto_b

    :cond_1b
    const-string v1, "networkStateChanged: reset MOBILE_HIPRI to false"

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lefs;->A:Z

    :cond_1c
    const/4 v1, 0x0

    goto/16 :goto_c

    :cond_1d
    const-string v1, "GCM"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1e

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "networkStateChanged (has active network): active_network_type="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", curr_network_type="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lefs;->c:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", curr_network_state="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lefs;->b:Landroid/net/NetworkInfo$State;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    :cond_1e
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    iget-object v2, p0, Lefs;->C:Leer;

    invoke-virtual {v2}, Leer;->e()Z

    move-result v2

    iget-object v3, p0, Lefs;->C:Leer;

    invoke-virtual {v3}, Leer;->f()Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lefs;->c(Z)V

    :cond_1f
    if-nez v2, :cond_20

    if-eqz v3, :cond_21

    :cond_20
    iget-object v2, p0, Lefs;->b:Landroid/net/NetworkInfo$State;

    if-ne v1, v2, :cond_21

    iget v2, p0, Lefs;->c:I

    if-ne v0, v2, :cond_21

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "### networkStateChanged: active and curr network type/state are the same("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), ignore"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_21
    iget v2, p0, Lefs;->c:I

    iget-object v4, p0, Lefs;->b:Landroid/net/NetworkInfo$State;

    invoke-direct {p0, v1, v0}, Lefs;->a(Landroid/net/NetworkInfo$State;I)V

    iget-wide v5, p0, Lefs;->z:J

    iget-wide v7, p0, Lefs;->E:J

    invoke-static {v0}, Lefs;->a(I)Z

    move-result v1

    if-nez v1, :cond_23

    const/4 v1, 0x0

    :goto_d
    iget-boolean v5, p0, Lefs;->w:Z

    if-nez v3, :cond_22

    if-eqz v1, :cond_22

    if-eqz v5, :cond_28

    :cond_22
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Reconnect on network change "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v3, :cond_26

    const-string v0, " connected "

    :goto_e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v5, :cond_27

    const-string v0, " suspended"

    :goto_f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lefs;->C:Leer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leer;->b(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lefs;->a(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lefs;->b(Z)V

    :goto_10
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lefs;->v:J

    goto/16 :goto_0

    :cond_23
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    sub-long v5, v9, v5

    const-wide/16 v11, 0x4e20

    cmp-long v1, v5, v11

    if-gtz v1, :cond_24

    const/4 v1, 0x0

    goto :goto_d

    :cond_24
    sub-long v5, v9, v7

    const-wide/32 v7, 0xea60

    cmp-long v1, v5, v7

    if-gtz v1, :cond_25

    const/4 v1, 0x0

    goto :goto_d

    :cond_25
    const-string v1, "checkThrottleReconnect = true"

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    const/4 v1, 0x1

    goto :goto_d

    :cond_26
    const-string v0, " "

    goto :goto_e

    :cond_27
    const-string v0, ""

    goto :goto_f

    :cond_28
    const/4 v1, 0x0

    invoke-static {v0}, Lefs;->a(I)Z

    move-result v0

    if-eqz v0, :cond_35

    iget-wide v2, p0, Lefs;->v:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_35

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lefs;->v:J

    sub-long/2addr v2, v4

    iget v0, p0, Lefs;->t:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_35

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "networkStateChanged: mLastMobileNetworkOutageTs="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lefs;->v:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", diff="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", use long delay"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    :goto_11
    invoke-virtual {p0, v0}, Lefs;->b(Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Network event while disconnected / throttling "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lefs;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v0, :cond_29

    const-string v0, " Long "

    :goto_12
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lefs;->n:J

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lefs;->a(Z)V

    goto/16 :goto_10

    :cond_29
    const-string v0, " "

    goto :goto_12

    :cond_2a
    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "networkStateChanged (no active network): , notify_network_type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lefs;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", notify_network_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    :cond_2b
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lefs;->a(Landroid/net/NetworkInfo$State;I)V

    iget-wide v0, p0, Lefs;->v:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2c

    iget-object v0, p0, Lefs;->d:Landroid/net/NetworkInfo$State;

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v0, v1, :cond_2e

    iget-wide v0, p0, Lefs;->D:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2e

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lefs;->D:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x4e20

    cmp-long v0, v0, v2

    if-gez v0, :cond_2e

    const/4 v0, 0x1

    :goto_13
    if-nez v0, :cond_2c

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lefs;->v:J

    :cond_2c
    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->e()Z

    move-result v0

    if-eqz v0, :cond_2f

    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2d

    const-string v0, "========== network down, force close conn ========="

    invoke-direct {p0, v0}, Lefs;->a(Ljava/lang/String;)V

    :cond_2d
    iget-object v0, p0, Lefs;->C:Leer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leer;->b(I)V

    const-string v0, "Disconnect on network down"

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2e
    const/4 v0, 0x0

    goto :goto_13

    :cond_2f
    const-string v0, "Network down, already disconnected"

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_30
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_32

    const-string v0, "state"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleAirplaneModeChanged: airplaneModeOn="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lefs;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_31

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lefs;->D:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lefs;->E:J

    goto/16 :goto_0

    :cond_31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lefs;->D:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lefs;->E:J

    goto/16 :goto_0

    :cond_32
    const-string v1, "com.google.android.intent.action.GCM_RECONNECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefs;->C:Leer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefs;->C:Leer;

    invoke-virtual {v0}, Leer;->f()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-direct {p0}, Lefs;->g()V

    goto/16 :goto_0

    :cond_33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lefs;->m:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lefs;->a(Z)V

    iget-object v0, p0, Lefs;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lefs;->L:Leeu;

    iget-boolean v0, v0, Leeu;->h:Z

    if-nez v0, :cond_34

    iget-object v0, p0, Lefs;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_34
    iget-wide v0, p0, Lefs;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lefs;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Left;

    invoke-direct {v1, p0}, Left;-><init>(Lefs;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :cond_35
    move v0, v1

    goto/16 :goto_11
.end method
