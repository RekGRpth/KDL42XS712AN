.class public final Ldtj;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Z

.field private final g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldtj;->b:Ldad;

    iput-object p3, p0, Ldtj;->c:Ljava/lang/String;

    iput-object p4, p0, Ldtj;->d:Ljava/lang/String;

    iput p5, p0, Ldtj;->e:I

    iput-boolean p6, p0, Ldtj;->f:Z

    iput-boolean p7, p0, Ldtj;->g:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldtj;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->e(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    iget-object v2, p0, Ldtj;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldtj;->c:Ljava/lang/String;

    iget-object v4, p0, Ldtj;->d:Ljava/lang/String;

    iget v5, p0, Ldtj;->e:I

    iget-boolean v6, p0, Ldtj;->f:Z

    iget-boolean v7, p0, Ldtj;->g:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
