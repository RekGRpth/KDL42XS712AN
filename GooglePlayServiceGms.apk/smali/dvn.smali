.class public abstract Ldvn;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Lbdx;
.implements Lbdy;


# instance fields
.field protected o:Z

.field private p:Lbdu;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbbo;)V
    .locals 5

    const/16 v4, 0x385

    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    const-string v1, "GamesFragmentActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connection to service apk failed with error "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lbbo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ldvn;->o:Z

    const/16 v0, 0x385

    invoke-virtual {p1, p0, v0}, Lbbo;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Ldvn;->finish()V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, p0, v4, v1}, Lbbv;->a(ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Ldvn;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public final a(Lbdx;)V
    .locals 2

    iget-object v0, p0, Ldvn;->p:Lbdu;

    if-nez v0, :cond_0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to register a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldvn;->p:Lbdu;

    invoke-interface {v0, p1}, Lbdu;->a(Lbdx;)V

    goto :goto_0
.end method

.method public final a(Lbdy;)V
    .locals 2

    iget-object v0, p0, Ldvn;->p:Lbdu;

    if-nez v0, :cond_0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to register a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldvn;->p:Lbdu;

    invoke-interface {v0, p1}, Lbdu;->a(Lbdy;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljj;->b(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final b(Lbdx;)V
    .locals 2

    iget-object v0, p0, Ldvn;->p:Lbdu;

    if-nez v0, :cond_0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to unregister a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldvn;->p:Lbdu;

    invoke-interface {v0, p1}, Lbdu;->b(Lbdx;)V

    goto :goto_0
.end method

.method public final b(Lbdy;)V
    .locals 2

    iget-object v0, p0, Ldvn;->p:Lbdu;

    if-nez v0, :cond_0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to unregister a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldvn;->p:Lbdu;

    invoke-interface {v0, p1}, Lbdu;->b(Lbdy;)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public d(I)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected abstract f()Lbdu;
.end method

.method public g()Ljava/util/ArrayList;
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public h()V
    .locals 0

    return-void
.end method

.method protected final i()V
    .locals 2

    invoke-virtual {p0}, Ldvn;->f()Lbdu;

    move-result-object v0

    iput-object v0, p0, Ldvn;->p:Lbdu;

    iget-object v0, p0, Ldvn;->p:Lbdu;

    if-nez v0, :cond_0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to instantiate GoogleApiClient; bailing out..."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Ldvn;->finish()V

    :cond_0
    return-void
.end method

.method public final j()Lbdu;
    .locals 2

    iget-object v0, p0, Ldvn;->p:Lbdu;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient instance not created yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ldvn;->p:Lbdu;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    iget-object v0, p0, Ldvn;->p:Lbdu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public abstract l()Z
.end method

.method public abstract m()Z
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v0, "GamesFragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityResult: unhandled request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Ljp;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iput-boolean v2, p0, Ldvn;->o:Z

    iget-object v0, p0, Ldvn;->p:Lbdu;

    invoke-interface {v0}, Lbdu;->a()V

    goto :goto_0

    :cond_0
    const/16 v0, 0x2712

    if-ne p2, v0, :cond_1

    const-string v0, "GamesFragmentActivity"

    const-string v1, "REQUEST_RESOLVE_FAILURE resulted in SIGN_IN_FAILED"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, p0, Ldvn;->o:Z

    invoke-virtual {p0}, Ldvn;->finish()V

    goto :goto_0

    :cond_1
    const-string v0, "GamesFragmentActivity"

    const-string v1, "REQUEST_RESOLVE_FAILURE failed, bailing out..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Ldvn;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x384
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Ldvn;->i()V

    if-eqz p1, :cond_0

    const-string v0, "savedStateResolutionInProgress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldvn;->o:Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Ljp;->onResume()V

    iget-boolean v0, p0, Ldvn;->o:Z

    if-eqz v0, :cond_0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "onResume with a resolutionIntentInProgress. This should never have happened ..."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldvn;->o:Z

    iget-object v0, p0, Ldvn;->p:Lbdu;

    invoke-interface {v0}, Lbdu;->a()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "savedStateResolutionInProgress"

    iget-boolean v1, p0, Ldvn;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Ljp;->onStart()V

    iget-boolean v0, p0, Ldvn;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldvn;->p:Lbdu;

    invoke-interface {v0}, Lbdu;->a()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Ljp;->onStop()V

    iget-object v0, p0, Ldvn;->p:Lbdu;

    invoke-interface {v0}, Lbdu;->b()V

    return-void
.end method

.method public setTitle(I)V
    .locals 1

    invoke-super {p0, p1}, Ljp;->setTitle(I)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljj;->d(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    invoke-super {p0, p1}, Ljp;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljj;->a(Ljava/lang/CharSequence;)V

    return-void
.end method
