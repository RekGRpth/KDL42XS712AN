.class public final Laiu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/StorageStats;
    .locals 11

    const-wide/16 v7, 0x0

    invoke-static {p0}, Lbkp;->a(Landroid/os/Parcel;)I

    move-result v9

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-wide v5, v7

    move-wide v3, v7

    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v9, :cond_0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const v10, 0xffff

    and-int/2addr v10, v0

    sparse-switch v10, :sswitch_data_0

    invoke-static {p0, v0}, Lbkp;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    sget-object v2, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lail;

    invoke-static {p0, v0, v2}, Lbkp;->b(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    move-object v2, v0

    goto :goto_0

    :sswitch_1
    invoke-static {p0, v0}, Lbkp;->g(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :sswitch_2
    invoke-static {p0, v0}, Lbkp;->h(Landroid/os/Parcel;I)J

    move-result-wide v3

    goto :goto_0

    :sswitch_3
    invoke-static {p0, v0}, Lbkp;->h(Landroid/os/Parcel;I)J

    move-result-wide v5

    goto :goto_0

    :sswitch_4
    invoke-static {p0, v0}, Lbkp;->h(Landroid/os/Parcel;I)J

    move-result-wide v7

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v9, :cond_1

    new-instance v0, Lbkq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Overread allowed size end="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbkq;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/appdatasearch/StorageStats;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/appdatasearch/StorageStats;-><init>(I[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;JJJ)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/StorageStats;Landroid/os/Parcel;I)V
    .locals 4

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbkr;->a(Landroid/os/Parcel;I)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->b:[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    invoke-static {p1, v1, v2, p2}, Lbkr;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;I)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->a:I

    invoke-static {p1, v1, v2}, Lbkr;->b(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->c:J

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->d:J

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->e:J

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;IJ)V

    invoke-static {p1, v0}, Lbkr;->b(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Laiu;->a(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/StorageStats;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/appdatasearch/StorageStats;

    return-object v0
.end method
