.class public final Lege;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:J

.field final synthetic b:Legd;

.field private c:I

.field private d:I


# direct methods
.method private constructor <init>(Legd;)V
    .locals 0

    iput-object p1, p0, Lege;->b:Legd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Legd;B)V
    .locals 0

    invoke-direct {p0, p1}, Lege;-><init>(Legd;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    const/4 v1, 0x3

    iget v0, p0, Lege;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lege;->c:I

    iget v0, p0, Lege;->c:I

    if-ne v0, v1, :cond_1

    const-wide v0, 0x408f400000000000L    # 1000.0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x40c1940000000000L    # 9000.0

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lege;->d:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lege;->c:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lege;->d:I

    int-to-long v0, v0

    iget v2, p0, Lege;->c:I

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    add-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lege;->d:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget v2, p0, Lege;->d:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lege;->a:J

    goto :goto_0
.end method
