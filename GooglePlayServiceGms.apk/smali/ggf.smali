.class public final Lggf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;

.field private e:Z

.field private f:Z

.field private final g:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lggf;->g:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Lgge;
    .locals 8

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v1, p0, Lggf;->g:Ljava/util/Set;

    iget-object v2, p0, Lggf;->a:Ljava/lang/String;

    iget-boolean v3, p0, Lggf;->b:Z

    iget-object v4, p0, Lggf;->c:Ljava/lang/String;

    iget-object v5, p0, Lggf;->d:Ljava/util/List;

    iget-boolean v6, p0, Lggf;->e:Z

    iget-boolean v7, p0, Lggf;->f:Z

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;-><init>(Ljava/util/Set;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;ZZ)V

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lggf;
    .locals 2

    iput-object p1, p0, Lggf;->d:Ljava/util/List;

    iget-object v0, p0, Lggf;->g:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Z)Lggf;
    .locals 2

    iput-boolean p1, p0, Lggf;->b:Z

    iget-object v0, p0, Lggf;->g:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
