.class public final Lhbt;
.super Lhbr;
.source "SourceFile"


# instance fields
.field private final j:Lhec;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0, p1, p2, p3}, Lhbr;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    new-instance v0, Lhec;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lheb;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p3, v3, p4}, Lheb;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lhec;-><init>(Ljava/lang/String;Lheb;)V

    iput-object v0, p0, Lhbt;->j:Lhec;

    return-void
.end method

.method private static a(Lint;)Lioj;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lint;->a:Linu;

    iget-object v3, v0, Linu;->a:Ljava/lang/String;

    new-instance v4, Lioj;

    invoke-direct {v4}, Lioj;-><init>()V

    iget-object v0, p0, Lint;->d:Lixo;

    iget-object v5, p0, Lint;->g:Ljava/lang/String;

    invoke-static {v0, v5}, Lhbt;->a(Lixo;Ljava/lang/String;)Lipv;

    move-result-object v0

    iput-object v0, v4, Lioj;->e:Lipv;

    iget v0, p0, Lint;->b:I

    iput v0, v4, Lioj;->j:I

    iget v0, p0, Lint;->c:I

    iput v0, v4, Lioj;->k:I

    iget v0, p0, Lint;->f:I

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    iput v0, v4, Lioj;->c:I

    iput v2, v4, Lioj;->d:I

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lioj;->m:Ljava/lang/String;

    iput v1, v4, Lioj;->h:I

    invoke-static {v4}, Lgth;->b(Lioj;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lioj;->b:Ljava/lang/String;

    return-object v4

    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lixo;Ljava/lang/String;)Lipv;
    .locals 2

    new-instance v0, Lipv;

    invoke-direct {v0}, Lipv;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lipv;->g:Z

    iput-object p0, v0, Lipv;->a:Lixo;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object p1, v0, Lipv;->d:Ljava/lang/String;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Lios;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Billing get payment options requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Liou;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Billing make payment requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Liow;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Billing update payment settings requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lioz;Z)V
    .locals 2

    const/4 v0, 0x5

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lhbt;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Lipa;Z)V
    .locals 2

    const/4 v0, 0x3

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lhbt;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Lipb;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Create profile requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lipc;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Enroll with broker requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lipf;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Get legal documents requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lipk;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Purchase options requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lipm;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Purchase requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lipo;Z)V
    .locals 2

    const/4 v0, 0x6

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lhbt;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Lipp;Z)V
    .locals 2

    const/4 v0, 0x4

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lhbt;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Ljas;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Create wallet objects requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljau;Z)V
    .locals 2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {p0, v1, v0}, Lhbt;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "BIN requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljao;)V
    .locals 1

    const/16 v0, 0xa

    invoke-virtual {p0, v0, p1}, Lhbt;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Ljaw;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Z)V
    .locals 2

    const/16 v0, 0x8

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lhbt;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Ljba;Z)V
    .locals 1

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Lhbt;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method protected final c(ILjava/lang/Object;)Landroid/util/Pair;
    .locals 10

    const/16 v9, 0x10

    const/16 v5, 0xc

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :pswitch_1
    check-cast p2, Landroid/util/Pair;

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lipa;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, v0, Lipa;->b:Linv;

    iget-object v0, v0, Linv;->b:Lint;

    invoke-static {v0}, Lhbt;->a(Lint;)Lioj;

    move-result-object v2

    iget-object v3, p0, Lhbt;->j:Lhec;

    iget-object v0, v0, Lint;->a:Linu;

    invoke-virtual {v3, v2, v0, v1}, Lhec;->a(Lioj;Linu;Z)Lioj;

    move-result-object v0

    new-instance v1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;-><init>()V

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v2, 0x7

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto :goto_0

    :pswitch_2
    check-cast p2, Landroid/util/Pair;

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lipp;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v3, v0, Lipp;->c:Linv;

    iget-object v3, v3, Linv;->b:Lint;

    iget-object v0, v0, Lipp;->b:Ljava/lang/String;

    iget-object v4, p0, Lhbt;->j:Lhec;

    invoke-virtual {v4, v0}, Lhec;->b(Ljava/lang/String;)Lioj;

    move-result-object v4

    iget-object v5, v3, Lint;->d:Lixo;

    iget-object v6, v3, Lint;->g:Ljava/lang/String;

    invoke-static {v5, v6}, Lhbt;->a(Lixo;Ljava/lang/String;)Lipv;

    move-result-object v5

    iput-object v5, v4, Lioj;->e:Lipv;

    iget v5, v3, Lint;->b:I

    iput v5, v4, Lioj;->j:I

    iget v3, v3, Lint;->c:I

    iput v3, v4, Lioj;->k:I

    iput v2, v4, Lioj;->h:I

    sget-object v2, Lizv;->a:[I

    iput-object v2, v4, Lioj;->g:[I

    iget-object v2, p0, Lhbt;->j:Lhec;

    invoke-virtual {v2, v0}, Lhec;->a(Ljava/lang/String;)Linu;

    move-result-object v0

    iget-object v2, p0, Lhbt;->j:Lhec;

    invoke-virtual {v2, v4, v0, v1}, Lhec;->b(Lioj;Linu;Z)V

    new-instance v1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;-><init>()V

    iput-object v4, v1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lioj;

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x9

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto/16 :goto_0

    :pswitch_3
    check-cast p2, Landroid/util/Pair;

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lioz;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, v0, Lioz;->b:Lixo;

    iget-object v3, v0, Lioz;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lhbt;->a(Lixo;Ljava/lang/String;)Lipv;

    move-result-object v2

    iget-object v3, p0, Lhbt;->f:Landroid/content/Context;

    iget-object v0, v0, Lioz;->b:Lixo;

    invoke-static {v3, v0}, Lheb;->a(Landroid/content/Context;Lixo;)Z

    move-result v0

    iput-boolean v0, v2, Lipv;->f:Z

    iget-object v0, p0, Lhbt;->j:Lhec;

    invoke-virtual {v0, v2, v1}, Lhec;->a(Lipv;Z)Lipv;

    move-result-object v0

    new-instance v1, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;-><init>()V

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lipv;

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0xb

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto/16 :goto_0

    :pswitch_4
    check-cast p2, Landroid/util/Pair;

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lipo;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, v0, Lipo;->c:Lixo;

    iget-object v3, v0, Lipo;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lhbt;->a(Lixo;Ljava/lang/String;)Lipv;

    move-result-object v2

    iget-object v3, v0, Lipo;->b:Ljava/lang/String;

    iput-object v3, v2, Lipv;->b:Ljava/lang/String;

    iget-object v3, p0, Lhbt;->f:Landroid/content/Context;

    iget-object v0, v0, Lipo;->c:Lixo;

    invoke-static {v3, v0}, Lheb;->a(Landroid/content/Context;Lixo;)Z

    move-result v0

    iput-boolean v0, v2, Lipv;->f:Z

    iget-object v0, p0, Lhbt;->j:Lhec;

    invoke-virtual {v0, v2, v1}, Lhec;->b(Lipv;Z)V

    new-instance v1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;-><init>()V

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lipv;

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v5, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lhbt;->j:Lhec;

    invoke-virtual {v0}, Lhec;->b()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lizz;

    invoke-direct {v1}, Lizz;-><init>()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lioj;

    iput-object v2, v1, Lizz;->b:[Lioj;

    iget-object v2, v1, Lizz;->b:[Lioj;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    iget-object v0, p0, Lhbt;->j:Lhec;

    invoke-virtual {v0}, Lhec;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lipv;

    iput-object v2, v1, Lizz;->c:[Lipv;

    iget-object v2, v1, Lizz;->c:[Lipv;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x13

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto/16 :goto_0

    :pswitch_6
    check-cast p2, Landroid/util/Pair;

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljau;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, v0, Ljau;->b:Ljava/lang/String;

    iget-object v0, v0, Ljau;->c:Ljava/lang/String;

    iget-object v3, p0, Lhbt;->j:Lhec;

    invoke-virtual {v3, v2}, Lhec;->a(Ljava/lang/String;)Linu;

    move-result-object v3

    iget-object v4, p0, Lhbt;->j:Lhec;

    invoke-virtual {v4, v2}, Lhec;->b(Ljava/lang/String;)Lioj;

    move-result-object v2

    new-instance v4, Ljav;

    invoke-direct {v4}, Ljav;-><init>()V

    iget-object v3, v3, Linu;->a:Ljava/lang/String;

    iput-object v3, v4, Ljav;->e:Ljava/lang/String;

    iget v3, v2, Lioj;->j:I

    iput v3, v4, Ljav;->c:I

    iget v3, v2, Lioj;->k:I

    iput v3, v4, Ljav;->d:I

    iget-object v2, v2, Lioj;->e:Lipv;

    iput-object v2, v4, Ljav;->g:Lipv;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lhbt;->j:Lhec;

    invoke-virtual {v2, v0}, Lhec;->c(Ljava/lang/String;)Lipv;

    move-result-object v0

    iput-object v0, v4, Ljav;->h:Lipv;

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, v4, Ljav;->i:[I

    invoke-static {v0, v5}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v4, Ljav;->i:[I

    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v9, v4}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto/16 :goto_0

    :pswitch_7
    new-instance v1, Ljap;

    invoke-direct {v1}, Ljap;-><init>()V

    iput v2, v1, Ljap;->a:I

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x17

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto/16 :goto_0

    :pswitch_8
    check-cast p2, Landroid/util/Pair;

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljaw;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v1, v0, Ljaw;->e:Linv;

    iget-object v1, v1, Linv;->b:Lint;

    iget-object v5, v1, Lint;->a:Linu;

    invoke-static {v1}, Lhbt;->a(Lint;)Lioj;

    move-result-object v1

    iget-object v6, p0, Lhbt;->j:Lhec;

    invoke-virtual {v6, v1, v5, v4}, Lhec;->a(Lioj;Linu;Z)Lioj;

    iget-object v6, p0, Lhbt;->j:Lhec;

    iget-object v7, v1, Lioj;->e:Lipv;

    invoke-virtual {v6, v7, v4}, Lhec;->a(Lipv;Z)Lipv;

    new-instance v6, Ljav;

    invoke-direct {v6}, Ljav;-><init>()V

    iget-object v7, v5, Linu;->a:Ljava/lang/String;

    iput-object v7, v6, Ljav;->e:Ljava/lang/String;

    iget v7, v1, Lioj;->j:I

    iput v7, v6, Ljav;->c:I

    iget v7, v1, Lioj;->k:I

    iput v7, v6, Ljav;->d:I

    iget-object v7, v1, Lioj;->e:Lipv;

    iput-object v7, v6, Ljav;->g:Lipv;

    iget-object v5, v5, Linu;->b:Ljava/lang/String;

    iput-object v5, v6, Ljav;->f:Ljava/lang/String;

    iget-object v5, v0, Ljaw;->g:Lipv;

    if-eqz v5, :cond_3

    iget-object v1, v1, Lioj;->e:Lipv;

    iget-object v5, v0, Ljaw;->g:Lipv;

    if-ne v1, v5, :cond_4

    move v1, v2

    :goto_1
    if-nez v1, :cond_2

    iget-object v1, p0, Lhbt;->j:Lhec;

    iget-object v3, v0, Ljaw;->g:Lipv;

    invoke-virtual {v1, v3, v4}, Lhec;->a(Lipv;Z)Lipv;

    :cond_2
    iget-object v0, v0, Ljaw;->g:Lipv;

    iput-object v0, v6, Ljav;->h:Lipv;

    iget-object v0, v6, Ljav;->h:Lipv;

    iput-boolean v2, v0, Lipv;->g:Z

    :cond_3
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v9, v6}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto/16 :goto_0

    :cond_4
    if-eqz v1, :cond_5

    if-nez v5, :cond_6

    :cond_5
    move v1, v3

    goto :goto_1

    :cond_6
    iget-object v7, v1, Lipv;->a:Lixo;

    iget-object v8, v5, Lipv;->a:Lixo;

    invoke-static {v7, v8}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Lizs;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v1, v1, Lipv;->d:Ljava/lang/String;

    iget-object v5, v5, Lipv;->d:Ljava/lang/String;

    invoke-static {v1, v5}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    goto :goto_1

    :cond_7
    move v1, v3

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
