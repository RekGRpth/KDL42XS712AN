.class public final Lisq;
.super Lirv;
.source "SourceFile"


# instance fields
.field private final transient a:[List;

.field private final transient b:[List;

.field private final transient c:I

.field private final transient d:I

.field private transient e:Lirx;

.field private transient f:Lirx;

.field private transient g:Lirs;


# direct methods
.method public varargs constructor <init>([Ljava/util/Map$Entry;)V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lirv;-><init>()V

    array-length v6, p1

    new-array v0, v6, [List;

    iput-object v0, p0, Lisq;->a:[List;

    invoke-static {v6}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v3, v0, 0x1

    if-lez v3, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "table too large: %s"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v2

    invoke-static {v0, v4, v5}, Lirg;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    new-array v0, v3, [List;

    iput-object v0, p0, Lisq;->b:[List;

    add-int/lit8 v0, v3, -0x1

    iput v0, p0, Lisq;->c:I

    move v3, v2

    move v0, v2

    :goto_1
    if-ge v3, v6, :cond_4

    aget-object v7, p1, v3

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->hashCode()I

    move-result v5

    add-int v4, v0, v5

    invoke-static {v5}, Lirr;->a(I)I

    move-result v0

    iget v5, p0, Lisq;->c:I

    and-int v9, v0, v5

    iget-object v0, p0, Lisq;->b:[List;

    aget-object v5, v0, v9

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    if-nez v5, :cond_1

    new-instance v0, Lisv;

    invoke-direct {v0, v8, v7}, Lisv;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    check-cast v0, List;

    iget-object v7, p0, Lisq;->b:[List;

    aput-object v0, v7, v9

    iget-object v7, p0, Lisq;->a:[List;

    aput-object v0, v7, v3

    :goto_3
    if-eqz v5, :cond_3

    invoke-interface {v5}, List;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_4
    const-string v7, "duplicate key: %s"

    new-array v9, v1, [Ljava/lang/Object;

    aput-object v8, v9, v2

    invoke-static {v0, v7, v9}, Lirg;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v5}, List;->a()List;

    move-result-object v0

    move-object v5, v0

    goto :goto_3

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    new-instance v0, Lisu;

    invoke-direct {v0, v8, v7, v5}, Lisu;-><init>(Ljava/lang/Object;Ljava/lang/Object;List;)V

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_4

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    goto :goto_1

    :cond_4
    iput v0, p0, Lisq;->d:I

    return-void
.end method

.method static synthetic a(Lisq;)[List;
    .locals 1

    iget-object v0, p0, Lisq;->a:[List;

    return-object v0
.end method

.method static synthetic b(Lisq;)I
    .locals 1

    iget v0, p0, Lisq;->d:I

    return v0
.end method


# virtual methods
.method public final a()Lirx;
    .locals 1

    iget-object v0, p0, Lisq;->e:Lirx;

    if-nez v0, :cond_0

    new-instance v0, Lisr;

    invoke-direct {v0, p0}, Lisr;-><init>(Lisq;)V

    iput-object v0, p0, Lisq;->e:Lirx;

    :cond_0
    return-object v0
.end method

.method public final b()Lirx;
    .locals 1

    iget-object v0, p0, Lisq;->f:Lirx;

    if-nez v0, :cond_0

    new-instance v0, Liss;

    invoke-direct {v0, p0}, Liss;-><init>(Lisq;)V

    iput-object v0, p0, Lisq;->f:Lirx;

    :cond_0
    return-object v0
.end method

.method public final c()Lirs;
    .locals 1

    iget-object v0, p0, Lisq;->g:Lirs;

    if-nez v0, :cond_0

    new-instance v0, Lisw;

    invoke-direct {v0, p0}, Lisw;-><init>(Lisq;)V

    iput-object v0, p0, Lisq;->g:Lirs;

    :cond_0
    return-object v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lisq;->a:[List;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lisq;->a()Lirx;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Lirr;->a(I)I

    move-result v1

    iget v2, p0, Lisq;->c:I

    and-int/2addr v1, v2

    iget-object v2, p0, Lisq;->b:[List;

    aget-object v1, v2, v1

    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, List;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, List;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {v1}, List;->a()List;

    move-result-object v1

    goto :goto_1
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lisq;->b()Lirx;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lisq;->a:[List;

    array-length v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lisq;->size()I

    move-result v0

    invoke-static {v0}, Lirm;->a(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lirm;->a:Lird;

    iget-object v2, p0, Lisq;->a:[List;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lird;->a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lisq;->c()Lirs;

    move-result-object v0

    return-object v0
.end method
