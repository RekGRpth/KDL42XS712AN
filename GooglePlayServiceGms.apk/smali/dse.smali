.class public final Ldse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldrb;


# instance fields
.field private final a:Lbjv;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[Ljava/lang/String;

.field private final h:Z

.field private final i:Z

.field private final j:I

.field private final k:Z

.field private final l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbjv;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZIZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldse;->a:Lbjv;

    iput p2, p0, Ldse;->b:I

    iput-object p3, p0, Ldse;->c:Ljava/lang/String;

    iput p4, p0, Ldse;->d:I

    iput-object p5, p0, Ldse;->e:Ljava/lang/String;

    iput-object p6, p0, Ldse;->f:Ljava/lang/String;

    iput-object p7, p0, Ldse;->g:[Ljava/lang/String;

    iput-boolean p8, p0, Ldse;->h:Z

    iput-boolean p9, p0, Ldse;->i:Z

    iput p10, p0, Ldse;->j:I

    iput-boolean p11, p0, Ldse;->k:Z

    iput p12, p0, Ldse;->l:I

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lctb;

    invoke-static {p0, v0, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lctb;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Lctb;->a()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lctb;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/GameRef;->a(Lcom/google/android/gms/games/Game;)Landroid/content/ContentValues;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    invoke-virtual {v1}, Lctb;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lctb;->b()V

    throw v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SCOPES"

    iget-object v2, p0, Ldse;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_CONNECTING_POPUP"

    iget-boolean v2, p0, Ldse;->i:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.CONNECTING_POPUP_GRAVITY"

    iget v2, p0, Ldse;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.RETRYING_SIGN_IN"

    iget-boolean v2, p0, Ldse;->k:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz p3, :cond_0

    const-string v1, "<<default account>>"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldse;->f:Ljava/lang/String;

    invoke-static {p1, v1, p3}, Lduj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.USE_DESIRED_ACCOUNT_NAME"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0

    :cond_0
    iget-object v1, p0, Ldse;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lduj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.USE_DESIRED_ACCOUNT_NAME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;I)Lcom/google/android/gms/common/server/ClientContext;
    .locals 8

    const/16 v2, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x0

    iget-object v0, p0, Ldse;->e:Ljava/lang/String;

    iget-object v1, p0, Ldse;->f:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lbov;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "<<default account>>"

    iget-object v4, p0, Ldse;->e:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x5

    invoke-direct {p0, p1, v0, v3}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v3

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ldse;->f:Ljava/lang/String;

    invoke-static {p1, v1}, Lduj;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v0, p0, Ldse;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Ldse;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v3

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldse;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lduj;->d(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Ldse;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Ldse;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v3

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldse;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lbov;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ldse;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lduj;->d(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Ldse;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Ldse;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v3

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ldse;->f:Ljava/lang/String;

    invoke-static {p1, p3, v4, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    :goto_1
    iget-object v5, p0, Ldse;->g:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_4

    iget-object v5, p0, Ldse;->g:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v1, v5}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    move-object v1, v3

    :cond_4
    if-nez v1, :cond_7

    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v5, p0, Ldse;->f:Ljava/lang/String;

    invoke-direct {v0, p3, v4, v4, v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v1, p0, Ldse;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    new-instance v1, Lbmx;

    const/4 v4, 0x1

    invoke-direct {v1, v0, v4}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    invoke-interface {v1, p1}, Lbmz;->b(Landroid/content/Context;)Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/ClientContext;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ldqq; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_2
    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    instance-of v0, v0, Lane;

    if-eqz v0, :cond_6

    invoke-direct {p0, p1, p2, v3}, Ldse;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v3

    goto/16 :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    invoke-direct {p0, p1, v2, v3}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v3

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const/4 v0, 0x7

    invoke-direct {p0, p1, v0, v3}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v3

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v1, "ValidateServiceOp"

    invoke-virtual {v0}, Ldqq;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0}, Ldqq;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    move v0, v2

    :goto_3
    invoke-direct {p0, p1, v0, v3}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v3

    goto/16 :goto_0

    :sswitch_0
    const/16 v0, 0xb

    goto :goto_3

    :sswitch_1
    const/16 v0, 0xa

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x3ea -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Landroid/content/Context;I)V
    .locals 6

    iget-object v1, p0, Ldse;->m:Ljava/lang/String;

    iget-object v2, p0, Ldse;->o:Ljava/lang/String;

    iget-object v3, p0, Ldse;->n:Ljava/lang/String;

    iget v4, p0, Ldse;->d:I

    move-object v0, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method private a(Landroid/content/Context;ILandroid/content/Intent;)V
    .locals 6

    const/4 v3, 0x0

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Ldse;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 2

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sparse-switch p2, :sswitch_data_0

    :goto_0
    if-eqz p4, :cond_0

    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p1, v0, p4, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v1, "pendingIntent"

    invoke-virtual {p5, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldse;->a:Lbjv;

    invoke-interface {v0, p2, p3, p5}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :sswitch_0
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Ldse;->a(Landroid/content/Context;I)V

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Ldse;->a(Landroid/content/Context;I)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x4 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcun;)V
    .locals 13

    const/16 v10, 0xa

    const/16 v9, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Ldse;->f:Ljava/lang/String;

    const-string v1, "com.google.android.gms.games.APP_ID"

    invoke-static {p1, v0, v1}, Lbox;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v4, ""

    :goto_0
    iput-object v4, p0, Ldse;->o:Ljava/lang/String;

    iget-object v0, p0, Ldse;->f:Ljava/lang/String;

    iput-object v0, p0, Ldse;->m:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldse;->c:Ljava/lang/String;

    iput-object v0, p0, Ldse;->m:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Ldse;->e:Ljava/lang/String;

    iput-object v0, p0, Ldse;->n:Ljava/lang/String;

    const-string v0, "<<default account>>"

    iget-object v1, p0, Ldse;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v6, p0, Ldse;->n:Ljava/lang/String;

    :cond_1
    invoke-direct {p0, p1, v7}, Ldse;->a(Landroid/content/Context;I)V

    invoke-static {p1}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lboe;

    invoke-direct {v0, p1}, Lboe;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lboe;->a()Lboe;

    move-result-object v0

    iget-object v0, v0, Lboe;->a:Landroid/content/Intent;

    const/4 v1, 0x6

    invoke-direct {p0, p1, v1, v0}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    :try_start_0
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "ValidateServiceOp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Application ID ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") must be a numeric value. Please verify that your manifest refers to the correct project ID."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v10, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto :goto_1

    :cond_4
    iget v0, p0, Ldse;->b:I

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Ldse;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Ldse;->g:[Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/games.firstparty"

    invoke-static {v2, v3}, Lboz;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    if-nez v1, :cond_5

    const-string v0, "ValidateServiceOp"

    const-string v1, "External caller attempting to use the 1P scope"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v9, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldse;->c:Ljava/lang/String;

    iget-object v3, p0, Ldse;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    if-nez v1, :cond_6

    const-string v0, "ValidateServiceOp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Caller attempting to connect as another package (calling package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldse;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", attempting to connect as: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldse;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v9, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto :goto_1

    :cond_6
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Ldse;->f:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_7
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-nez v1, :cond_8

    const-string v0, "ValidateServiceOp"

    const-string v1, "Using Google Play games services requires a metadata tag with the name \"com.google.android.gms.games.APP_ID\" in the application tag of your manifest"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v10, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    const-string v1, "ValidateServiceOp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Ldse;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, p1, v9, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :cond_8
    iget-boolean v1, p0, Ldse;->h:Z

    if-eqz v1, :cond_c

    iget v1, p0, Ldse;->b:I

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    if-eq v1, v2, :cond_9

    const-string v0, "ValidateServiceOp"

    const-string v1, "Headless Client must be started from your process."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_9
    iget-object v1, p0, Ldse;->e:Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v1, p0, Ldse;->e:Ljava/lang/String;

    invoke-static {p1, v1}, Lbov;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    :cond_a
    const-string v0, "ValidateServiceOp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Headless Client requires a valid account (account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldse;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x5

    invoke-direct {p0, p1, v0, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v6

    :goto_2
    move-object v2, v0

    :goto_3
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldse;->n:Ljava/lang/String;

    iget v0, p0, Ldse;->l:I

    invoke-static {v2, v0}, Ldll;->a(Lcom/google/android/gms/common/server/ClientContext;I)V

    :try_start_2
    invoke-static {v2}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    :try_end_2
    .catch Lamq; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ldqq; {:try_start_2 .. :try_end_2} :catch_3

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "ValidateServiceOp"

    const-string v1, "The installed version of Google Play services is out-of-date; it must be updated to communicate with the Play Games service."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :cond_b
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldse;->e:Ljava/lang/String;

    iget-object v3, p0, Ldse;->e:Ljava/lang/String;

    iget-object v5, p0, Ldse;->f:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldse;->g:[Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_2

    :cond_c
    invoke-direct {p0, p1, v4, v0}, Ldse;->a(Landroid/content/Context;Ljava/lang/String;I)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    goto :goto_3

    :catch_2
    move-exception v0

    invoke-direct {p0, p1, v9, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :catch_3
    move-exception v0

    const-string v1, "ValidateServiceOp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error from revision check: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ldqq;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ldqq;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "ValidateServiceOp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error from revision check: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ldqq;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ldqq;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->f(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v9, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :cond_d
    invoke-static {v2}, Lcom/google/android/gms/games/service/GamesAndroidService;->a(Lcom/google/android/gms/common/server/ClientContext;)Ldqv;

    move-result-object v3

    if-eqz v3, :cond_e

    invoke-virtual {v3}, Ldqv;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Ldse;->h:Z

    invoke-virtual {v3}, Ldqv;->w()Z

    move-result v1

    if-eq v0, v1, :cond_f

    :cond_e
    iget-boolean v5, p0, Ldse;->h:Z

    new-instance v0, Ldqv;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Ldse;->d:I

    invoke-direct/range {v0 .. v5}, Ldqv;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Z)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    iget-object v1, p0, Ldse;->f:Ljava/lang/String;

    invoke-virtual {p2, p1, v2, v1, v7}, Lcun;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_10

    move-object v0, v6

    :goto_4
    move-object v3, v0

    :cond_f
    if-nez v3, :cond_11

    invoke-direct {p0, p1, v10, v6}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :cond_10
    invoke-static {v2, v0}, Lcom/google/android/gms/games/service/GamesAndroidService;->a(Lcom/google/android/gms/common/server/ClientContext;Ldqv;)V

    goto :goto_4

    :cond_11
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    iget-boolean v0, p0, Ldse;->h:Z

    if-nez v0, :cond_16

    invoke-virtual {p2, p1, v2}, Lcun;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldjo;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lcts;

    invoke-static {p1, v0, v8}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lcts;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_3
    invoke-virtual {v1}, Lcts;->a()I

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lduj;->e(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x4

    const/4 v2, 0x0

    invoke-direct {p0, p1, v4, v2}, Ldse;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, p1, v0, v2}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcts;->b()V

    goto/16 :goto_1

    :cond_12
    const/4 v0, 0x0

    :try_start_4
    invoke-virtual {v1, v0}, Lcts;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/PlayerRef;->a(Lcom/google/android/gms/games/Player;)Landroid/content/ContentValues;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    invoke-virtual {v1}, Lcts;->b()V

    invoke-virtual {v3, v0}, Ldqv;->a(Landroid/content/ContentValues;)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    invoke-static {p1, v2, v4}, Ldse;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    if-nez v0, :cond_13

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lduj;->e(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-direct {p0, p1, v4, v6}, Ldse;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Ldse;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcts;->b()V

    throw v0

    :cond_13
    invoke-virtual {v3, v0}, Ldqv;->b(Landroid/content/ContentValues;)V

    :cond_14
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lduj;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v0}, Lduj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lduj;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    move v0, v7

    :goto_5
    invoke-virtual {p2, p1, v2, v4}, Lcun;->h(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v9

    const-wide/16 v11, -0x1

    cmp-long v1, v9, v11

    if-eqz v1, :cond_15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long v9, v11, v9

    const-wide/32 v11, 0xdbba00

    cmp-long v1, v9, v11

    if-ltz v1, :cond_18

    move v1, v7

    :goto_6
    or-int/2addr v0, v1

    :cond_15
    if-eqz v0, :cond_16

    const-string v0, "show_welcome_popup"

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {p1, v2}, Lduj;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    :cond_16
    move-object v0, p0

    move-object v1, p1

    move v2, v8

    move-object v4, v6

    invoke-direct/range {v0 .. v5}, Ldse;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto/16 :goto_1

    :cond_17
    move v0, v8

    goto :goto_5

    :cond_18
    move v1, v8

    goto :goto_6
.end method
