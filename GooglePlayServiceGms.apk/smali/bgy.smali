.class public abstract Lbgy;
.super Lbgo;
.source "SourceFile"


# instance fields
.field protected final b:Lbgo;


# direct methods
.method public constructor <init>(Lbgo;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbgo;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    instance-of v0, p1, Lbgy;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Not possible to have nested FilteredDataBuffers."

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lbgy;->b:Lbgo;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lbgy;->b:Lbgo;

    invoke-virtual {p0, p1}, Lbgy;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(I)I
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lbgy;->b:Lbgo;

    invoke-virtual {v0}, Lbgo;->b()V

    return-void
.end method

.method public final c()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lbgy;->b:Lbgo;

    invoke-virtual {v0}, Lbgo;->c()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method
