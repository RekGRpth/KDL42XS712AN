.class public final Lfoe;
.super Lfpc;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private Y:Ljj;

.field private Z:[Lfog;

.field private aa:I

.field private ab:Lfoi;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfpc;-><init>()V

    return-void
.end method

.method public static a(Z)Lfoe;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "all_apps"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Lfoe;

    invoke-direct {v1}, Lfoe;-><init>()V

    invoke-virtual {v1, v0}, Lfoe;->g(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private c(I)V
    .locals 2

    iput p1, p0, Lfoe;->aa:I

    invoke-virtual {p0}, Lfoe;->O()Lfpa;

    move-result-object v0

    iget-object v1, p0, Lfoe;->Z:[Lfog;

    aget-object v1, v1, p1

    iget-object v1, v1, Lfog;->c:Lfpb;

    invoke-virtual {v0, v1}, Lfpa;->a(Lfpb;)V

    iget-object v0, p0, Lfoe;->Y:Ljj;

    iget-object v1, p0, Lfoe;->Z:[Lfog;

    aget-object v1, v1, p1

    iget v1, v1, Lfog;->d:I

    invoke-virtual {v0, v1}, Ljj;->b(I)V

    invoke-virtual {p0}, Lfoe;->c()V

    return-void
.end method


# virtual methods
.method public final J()I
    .locals 2

    iget-object v0, p0, Lfoe;->Z:[Lfog;

    iget v1, p0, Lfoe;->aa:I

    aget-object v0, v0, v1

    iget v0, v0, Lfog;->b:I

    return v0
.end method

.method protected final K()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    iget-object v0, p0, Lfoe;->Z:[Lfog;

    iget v1, p0, Lfoe;->aa:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lfog;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method final L()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lfoe;->Z:[Lfog;

    iget v1, p0, Lfoe;->aa:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lfog;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method final M()Ljava/lang/CharSequence;
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lfot;->a()Lfot;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, v1}, Lfot;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lfot;->a()Lfot;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, v1}, Lfot;->b(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfoe;->Z:[Lfog;

    iget v1, p0, Lfoe;->aa:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lfog;->f:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method final N()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfoe;->Z:[Lfog;

    iget v2, p0, Lfoe;->aa:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lfog;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method final a(Landroid/content/Context;)Lfpa;
    .locals 4

    new-instance v0, Lfpa;

    iget-object v1, p0, Lfoe;->i:Lfob;

    new-instance v2, Lfof;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lfof;-><init>(Lfoe;B)V

    invoke-direct {v0, p1, v1, v2}, Lfpa;-><init>(Landroid/content/Context;Lfob;Lfpb;)V

    return-object v0
.end method

.method public final a(Ljj;)V
    .locals 4

    const/4 v1, 0x0

    iput-object p1, p0, Lfoe;->Y:Ljj;

    iget v0, p0, Lfoe;->aa:I

    invoke-direct {p0, v0}, Lfoe;->c(I)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lfob;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lfoe;->ab:Lfoi;

    if-nez v2, :cond_1

    new-instance v2, Lfoi;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v2, v3}, Lfoi;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lfoe;->ab:Lfoi;

    iget-object v2, p0, Lfoe;->ab:Lfoi;

    invoke-virtual {v2, v0}, Lfoi;->a([Ljava/lang/String;)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lfoe;->Z:[Lfog;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lfoe;->ab:Lfoi;

    iget-object v3, p0, Lfoe;->Z:[Lfog;

    aget-object v3, v3, v0

    iget-object v3, v3, Lfog;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lfoi;->a(Ljava/lang/String;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lfoe;->ab:Lfoi;

    const v2, 0x7f0b0378    # com.google.android.gms.R.string.plus_app_settings_accounts_label

    invoke-virtual {p0, v2}, Lfoe;->a(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lfoi;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lfoe;->i:Lfob;

    iget-object v0, v0, Lfob;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lfoe;->ab:Lfoi;

    invoke-virtual {v2, v0}, Lfoi;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lfoe;->ab:Lfoi;

    iget v2, p0, Lfoe;->aa:I

    invoke-virtual {v0, v2}, Lfoi;->a(I)V

    const v0, 0x7f040104    # com.google.android.gms.R.layout.plus_settings_action_bar_spinner

    invoke-virtual {p1, v0}, Ljj;->a(I)V

    invoke-virtual {p1}, Ljj;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v2, p0, Lfoe;->ab:Lfoi;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    iget v1, p0, Lfoe;->aa:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 13

    const v12, 0x7f0b0377    # com.google.android.gms.R.string.plus_app_settings_sign_in_apps_label

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-super {p0, p1}, Lfpc;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lfoe;->Z:[Lfog;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Lfog;

    iput-object v0, p0, Lfoe;->Z:[Lfog;

    iget-object v11, p0, Lfoe;->Z:[Lfog;

    new-instance v0, Lfog;

    const v1, 0x7f0b0376    # com.google.android.gms.R.string.plus_app_settings_all_apps_label

    invoke-virtual {p0, v1}, Lfoe;->b(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b0374    # com.google.android.gms.R.string.plus_app_settings_apps_page_label

    new-instance v3, Lfof;

    invoke-direct {v3, p0, v9}, Lfof;-><init>(Lfoe;B)V

    const v4, 0x7f0200a1    # com.google.android.gms.R.drawable.common_settings_icon

    const v5, 0x7f0b036f    # com.google.android.gms.R.string.plus_list_apps_empty_message

    invoke-virtual {p0, v5}, Lfoe;->a(I)Ljava/lang/CharSequence;

    move-result-object v5

    const v6, 0x7f0b0383    # com.google.android.gms.R.string.plus_list_apps_error

    invoke-virtual {p0, v6}, Lfoe;->a(I)Ljava/lang/CharSequence;

    move-result-object v6

    sget-object v7, Lbck;->q:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v8, Lfsr;->B:Lbfy;

    invoke-virtual {v8}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lfog;-><init>(Ljava/lang/String;ILfpb;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    aput-object v0, v11, v9

    iget-object v11, p0, Lfoe;->Z:[Lfog;

    new-instance v0, Lfog;

    invoke-virtual {p0, v12}, Lfoe;->b(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lfoh;

    invoke-direct {v3, p0, v9}, Lfoh;-><init>(Lfoe;B)V

    const v4, 0x7f020207    # com.google.android.gms.R.drawable.plus_icon_red_32

    const v2, 0x7f0b036e    # com.google.android.gms.R.string.plus_list_apps_aspen_empty_message

    invoke-virtual {p0, v2}, Lfoe;->a(I)Ljava/lang/CharSequence;

    move-result-object v5

    sget-object v2, Lfsr;->z:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v5, v2}, Lfoy;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    const v2, 0x7f0b0384    # com.google.android.gms.R.string.plus_list_apps_error_aspen

    invoke-virtual {p0, v2}, Lfoe;->a(I)Ljava/lang/CharSequence;

    move-result-object v6

    sget-object v2, Lfsr;->z:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v6, v2}, Lfoy;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    sget-object v7, Lbck;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lfsr;->A:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move v2, v12

    invoke-direct/range {v0 .. v8}, Lfog;-><init>(Ljava/lang/String;ILfpb;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    aput-object v0, v11, v10

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "all_apps"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz p1, :cond_1

    const-string v0, "connected_apps_filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lfoe;->aa:I

    return-void

    :cond_1
    if-eqz v0, :cond_2

    move v0, v9

    goto :goto_0

    :cond_2
    move v0, v10

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lfpc;->e(Landroid/os/Bundle;)V

    const-string v0, "connected_apps_filter"

    iget v1, p0, Lfoe;->aa:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lfoe;->ab:Lfoi;

    invoke-virtual {v0, p3}, Lfoi;->b(I)Lfoj;

    move-result-object v0

    iget v1, v0, Lfoj;->a:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lfoe;->i:Lfob;

    sget-object v2, Lbcj;->A:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lfoe;->K()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lfob;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget-object v1, p0, Lfoe;->i:Lfob;

    iget-object v2, v0, Lfoj;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lfob;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lfoe;->ab:Lfoi;

    iget-object v0, v0, Lfoj;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lfoi;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lfoe;->i:Lfob;

    sget-object v2, Lbcj;->z:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lfoe;->K()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lfob;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget v1, v0, Lfoj;->b:I

    invoke-direct {p0, v1}, Lfoe;->c(I)V

    iget-object v1, p0, Lfoe;->ab:Lfoi;

    iget v0, v0, Lfoj;->b:I

    invoke-virtual {v1, v0}, Lfoi;->a(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
