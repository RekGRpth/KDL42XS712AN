.class public final Ldxs;
.super Ldzx;
.source "SourceFile"


# instance fields
.field private final a:Ldxm;

.field private final b:Ldyg;


# direct methods
.method public constructor <init>(Ldxm;)V
    .locals 2

    invoke-direct {p0}, Ldzx;-><init>()V

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Ldxs;->a:Ldxm;

    new-instance v0, Ldyg;

    invoke-direct {v0, p1}, Ldyg;-><init>(Ldxm;)V

    iput-object v0, p0, Ldxs;->b:Ldyg;

    iget-object v0, p0, Ldxs;->a:Ldxm;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v0, v1}, Ledd;->a(Lo;Ljava/lang/String;)V

    return-void
.end method

.method private d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "turn_based_match"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Ldxs;->a:Ldxm;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Ldxm;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->finish()V

    return-void
.end method

.method private e(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 5

    const/4 v2, 0x1

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "acceptInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->q_()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "ClientInboxHelper"

    const-string v1, "acceptInvitation: invitation not valid anymore..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const v2, 0x7f0b02a6    # com.google.android.gms.R.string.games_progress_dialog_accepting_invitation

    invoke-virtual {v1, v2}, Ldxm;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lebo;->a(Ljava/lang/String;)Lebo;

    move-result-object v1

    iget-object v2, p0, Ldxs;->a:Ldxm;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ldgs;->b(Lbdu;Ljava/lang/String;)Lbeh;

    move-result-object v0

    new-instance v1, Ldxt;

    invoke-direct {v1, p0}, Ldxt;-><init>(Ldxs;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-static {v0}, Lbox;->b(Landroid/app/Activity;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/InvitationEntity;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "invitation"

    iget-object v4, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v4}, Ldxm;->q()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v0, v1, v4}, Lbis;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ClientInboxHelper"

    const-string v1, "Unable to return invitation to game. Something has gone very wrong."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldxs;->a:Ldxm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldxm;->setResult(I)V

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->finish()V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Ldxm;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->finish()V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->b()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Ldxs;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "onInvitationClusterSeeMoreClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->i:Ldfx;

    invoke-interface {v1, v0, p1, p2, p3}, Ldfx;->a(Lbdu;Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2}, Ldxm;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 3

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "onInvitationAccepted: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Leee;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-static {v0, p1, p0}, Leee;->a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Leei;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Ldxs;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v1

    iget-object v0, p0, Ldxs;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "onInvitationParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Leee;->a(Landroid/content/Context;Lbdu;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 0

    invoke-direct {p0, p1}, Ldxs;->d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v1

    iget-object v0, p0, Ldxs;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "onMatchParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Leee;->a(Landroid/content/Context;Lbdu;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Ldgt;)V
    .locals 4

    invoke-interface {p1}, Ldgt;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Ldgt;->g()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    move-result-object v1

    iget-object v2, p0, Ldxs;->a:Ldxm;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v3}, Ledd;->a(Lo;Ljava/lang/String;)V

    invoke-static {v0}, Leee;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const v0, 0x7f0b0254    # com.google.android.gms.R.string.games_inbox_network_error_dialog_message

    invoke-static {v0}, Lebn;->c(I)Lebn;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const-string v2, "com.google.android.gms.games.ui.dialog.alertDialogNetworkError"

    invoke-static {v1, v0, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    if-nez v1, :cond_1

    const-string v1, "ClientInboxHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No turn-based match received after accepting invite: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Ldxs;->d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final a(Ldlb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "RequestSummary tiles are not displayed in the client UI."

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final a_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    const-string v0, "Trying to install game from Client UI!"

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Ldxs;->b:Ldyg;

    invoke-virtual {v0, p1}, Ldyg;->a(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->b()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Ldxs;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 0

    invoke-direct {p0, p1}, Ldxs;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "onMatchDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ldgs;->e(Lbdu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/Game;)V
    .locals 1

    const-string v0, "RequestSummary tiles are not displayed in the client UI."

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "onInvitationDeclined: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {v1, v0, v2}, Ldgs;->c(Lbdu;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcte;->k:Ldgh;

    invoke-interface {v1, v0, v2}, Ldgh;->a(Lbdu;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "onMatchRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Ldxs;->a:Ldxm;

    const v2, 0x7f0b02a7    # com.google.android.gms.R.string.games_progress_dialog_starting_rematch

    invoke-virtual {v1, v2}, Ldxm;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lebo;->a(Ljava/lang/String;)Lebo;

    move-result-object v1

    iget-object v2, p0, Ldxs;->a:Ldxm;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ldgs;->a(Lbdu;Ljava/lang/String;)Lbeh;

    move-result-object v0

    new-instance v1, Ldxu;

    invoke-direct {v1, p0}, Ldxu;-><init>(Ldxs;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    iget-object v0, p0, Ldxs;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldxs;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientInboxHelper"

    const-string v1, "onInvitationDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {v1, v0, v2}, Ldgs;->d(Lbdu;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcte;->k:Ldgh;

    invoke-interface {v1, v0, v2}, Ldgh;->b(Lbdu;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
