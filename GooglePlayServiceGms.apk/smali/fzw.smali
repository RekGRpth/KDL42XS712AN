.class final Lfzw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field final synthetic i:Lfzm;


# direct methods
.method public constructor <init>(Lfzm;Landroid/widget/FrameLayout;)V
    .locals 9

    const/16 v1, 0x30

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lfzw;->i:Lfzm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lfzw;->c:I

    iput v2, p0, Lfzw;->e:I

    invoke-virtual {p2, v2, v2}, Landroid/widget/FrameLayout;->measure(II)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v4, p1, Lfzm;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v0}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    aget v5, v0, v2

    aget v6, v0, v3

    iget-object v0, p1, Lfzm;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v6

    iget-object v4, p1, Lfzm;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v4

    sub-int v4, v0, v4

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    add-int v7, v4, v0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xb

    if-lt v0, v8, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v7

    invoke-static {p1}, Lfzm;->k(Lfzm;)Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v7

    if-le v0, v7, :cond_1

    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lfzw;->a:Z

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v5

    invoke-static {p1}, Lfzm;->k(Lfzm;)Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v7

    if-ge v0, v7, :cond_2

    move v0, v3

    :goto_2
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v5

    iget-object v8, p1, Lfzm;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    invoke-static {p1}, Lfzm;->k(Lfzm;)Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getWidth()I

    move-result v8

    if-ge v7, v8, :cond_3

    :goto_3
    iget-object v7, p1, Lfzm;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    invoke-static {p1}, Lfzm;->l(Lfzm;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    if-eqz v0, :cond_5

    iget-boolean v8, p0, Lfzw;->a:Z

    if-eqz v8, :cond_4

    const/16 v1, 0x50

    iput v1, p0, Lfzw;->f:I

    :goto_4
    iput v7, p0, Lfzw;->b:I

    iput v2, p0, Lfzw;->d:I

    :goto_5
    if-eqz v0, :cond_9

    iput v5, p0, Lfzw;->g:I

    :goto_6
    iget-boolean v0, p0, Lfzw;->a:Z

    if-eqz v0, :cond_b

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v6, v0

    iget-object v1, p1, Lfzm;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_7
    iput v0, p0, Lfzw;->h:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_3

    :cond_4
    iput v1, p0, Lfzw;->f:I

    goto :goto_4

    :cond_5
    if-eqz v3, :cond_7

    iget-boolean v1, p0, Lfzw;->a:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x51

    iput v1, p0, Lfzw;->f:I

    :goto_8
    iput v2, p0, Lfzw;->b:I

    iput v2, p0, Lfzw;->d:I

    goto :goto_5

    :cond_6
    const/16 v1, 0x31

    iput v1, p0, Lfzw;->f:I

    goto :goto_8

    :cond_7
    iget-boolean v1, p0, Lfzw;->a:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x55

    iput v1, p0, Lfzw;->f:I

    :goto_9
    iput v2, p0, Lfzw;->b:I

    iput v7, p0, Lfzw;->d:I

    goto :goto_5

    :cond_8
    const/16 v1, 0x35

    iput v1, p0, Lfzw;->f:I

    goto :goto_9

    :cond_9
    if-eqz v3, :cond_a

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v5, v0

    iget-object v1, p1, Lfzm;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lfzw;->g:I

    goto :goto_6

    :cond_a
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v5, v0

    iget-object v1, p1, Lfzm;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lfzw;->g:I

    goto :goto_6

    :cond_b
    move v0, v4

    goto :goto_7
.end method
