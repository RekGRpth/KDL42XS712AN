.class public final Ldzl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldvl;


# instance fields
.field a:[I

.field private final b:Ldvk;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldvk;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Ldzl;->a:[I

    iput-object p1, p0, Ldzl;->c:Landroid/content/Context;

    iput-object p2, p0, Ldzl;->b:Ldvk;

    iget-object v0, p0, Ldzl;->b:Ldvk;

    invoke-virtual {v0, p0}, Ldvk;->a(Ldvl;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private b(I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ltz p1, :cond_1

    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    move v0, v2

    :goto_1
    if-ge v0, p1, :cond_3

    iget-object v3, p0, Ldzl;->a:[I

    aget v3, v3, v0

    if-ne v3, v1, :cond_2

    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ldzl;->a:[I

    aget v0, v0, p1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    move v2, v1

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 6

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    if-ltz v3, :cond_1

    if-ge v3, v5, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Ldzl;->a:[I

    aget v0, v0, v3

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    iget-object v0, p0, Ldzl;->a:[I

    const/4 v4, 0x2

    aput v4, v0, v3

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    return-void
.end method

.method final a(I)V
    .locals 2

    const/4 v1, 0x0

    if-ltz p1, :cond_1

    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Ldzl;->a:[I

    aput v1, v0, p1

    invoke-virtual {p0}, Ldzl;->b()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldzl;->b:Ldvk;

    invoke-virtual {v0}, Ldvk;->a()V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0, v4}, Ldzl;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldzl;->b:Ldvk;

    iget-object v1, p0, Ldzl;->c:Landroid/content/Context;

    sget v2, Lxf;->Y:I

    invoke-static {v1, v2}, Leee;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Ldzl;->c:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lxf;->X:I

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a    # android.R.string.ok

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v1, Lxf;->aG:I

    invoke-virtual {v2, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldvk;->a(Landroid/app/AlertDialog;)V

    iget-object v0, p0, Ldzl;->b:Ldvk;

    sget v1, Lxf;->Y:I

    invoke-virtual {v0, v1}, Ldvk;->a(I)V

    iget-object v0, p0, Ldzl;->a:[I

    aput v4, v0, v4

    goto :goto_0
.end method

.method final b()I
    .locals 4

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Ldzl;->a:[I

    aget v2, v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final c()V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Ldzl;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldzl;->b:Ldvk;

    iget-object v1, p0, Ldzl;->c:Landroid/content/Context;

    sget v2, Lxf;->aa:I

    sget v3, Lxf;->Z:I

    invoke-virtual {v0, v1, v2, v3}, Ldvk;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Ldzl;->b:Ldvk;

    sget v1, Lxf;->aa:I

    invoke-virtual {v0, v1}, Ldvk;->a(I)V

    iget-object v0, p0, Ldzl;->a:[I

    const/4 v1, 0x1

    aput v1, v0, v4

    goto :goto_0
.end method

.method public final d()V
    .locals 5

    const/4 v4, 0x2

    invoke-direct {p0, v4}, Ldzl;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldzl;->b:Ldvk;

    iget-object v1, p0, Ldzl;->c:Landroid/content/Context;

    sget v2, Lxf;->ac:I

    sget v3, Lxf;->ab:I

    invoke-virtual {v0, v1, v2, v3}, Ldvk;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Ldzl;->b:Ldvk;

    sget v1, Lxf;->ac:I

    invoke-virtual {v0, v1}, Ldvk;->a(I)V

    iget-object v0, p0, Ldzl;->a:[I

    const/4 v1, 0x1

    aput v1, v0, v4

    goto :goto_0
.end method
