.class public final Ldrj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldri;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ldaj;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldrj;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Ldrj;->b:Ldaj;

    iput-object p3, p0, Ldrj;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcun;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrj;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Ldrj;->c:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    :try_end_0
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    :try_start_1
    iget-object v1, p0, Ldrj;->b:Ldaj;

    invoke-interface {v1, v0}, Ldaj;->b(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SignInIntentService"

    invoke-virtual {v0}, Ldqq;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0}, Ldqq;->b()I

    move-result v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method
