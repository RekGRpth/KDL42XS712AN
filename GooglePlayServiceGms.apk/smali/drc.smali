.class public final Ldrc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static final b:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "GamesNotificationManage"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, Ldrc;->a:I

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Ldrc;->b:Landroid/util/SparseArray;

    return-void
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_MULTIPLAYER_INBOX_INTERNAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lbhw;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p1, p0, p2}, Lbhw;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v1, "GamesNotificationManage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to parse image content for icon URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ldet;)Landroid/net/Uri;
    .locals 1

    invoke-interface {p1}, Ldet;->e()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Ldet;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Ldjb;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0244    # com.google.android.gms.R.string.games_notification_single_account_call_to_action

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5

    const/4 v1, 0x0

    invoke-static {p0}, Ldrc;->b(Ljava/lang/String;)I

    move-result v0

    sget-object v2, Ldrc;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_1

    move v2, v1

    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v1

    :goto_1
    if-ge v3, v2, :cond_2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldrd;

    iget-object v1, v1, Ldrd;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto :goto_0

    :cond_2
    return-object v4
.end method

.method private static a(ILandroid/app/NotificationManager;)V
    .locals 5

    const/4 v3, 0x0

    sget-object v0, Ldrc;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x1

    move v2, v3

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldrd;

    iget-boolean v1, v1, Ldrd;->d:Z

    if-nez v1, :cond_2

    :goto_2
    if-eqz v3, :cond_0

    invoke-virtual {p1, p0}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    move v3, v4

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ldeu;)V
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p2}, Ldeu;->a()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p2}, Ldeu;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldet;

    invoke-interface {v0}, Ldet;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {p0, p1, v1}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldeu;)V
    .locals 19

    invoke-static/range {p1 .. p1}, Ldrc;->b(Ljava/lang/String;)I

    move-result v14

    const-string v3, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Landroid/app/NotificationManager;

    invoke-virtual/range {p4 .. p4}, Ldeu;->a()I

    move-result v3

    if-gtz v3, :cond_2

    invoke-static/range {p1 .. p1}, Ldrc;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    const-string v4, "GamesNotificationManage"

    const-string v5, "Clearing displayed notifications."

    invoke-static {v4, v5}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1, v3}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    const-string v3, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-virtual {v3, v14}, Landroid/app/NotificationManager;->cancel(I)V

    sget-object v3, Ldrc;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, v14}, Landroid/util/SparseArray;->remove(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v3, Ldrc;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, v14}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Ljava/util/ArrayList;

    invoke-virtual/range {p4 .. p4}, Ldeu;->a()I

    move-result v15

    new-instance v16, Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v3, 0x0

    move v13, v3

    :goto_1
    if-ge v13, v15, :cond_3

    move-object/from16 v0, p4

    invoke-virtual {v0, v13}, Ldeu;->b(I)Ldet;

    move-result-object v17

    new-instance v3, Ldrd;

    invoke-interface/range {v17 .. v17}, Ldet;->a()J

    move-result-wide v4

    invoke-interface/range {v17 .. v17}, Ldet;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v17 .. v17}, Ldet;->h()Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v17 .. v17}, Ldet;->k()Z

    move-result v8

    invoke-direct/range {v3 .. v8}, Ldrd;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface/range {v17 .. v17}, Ldet;->j()Z

    move-result v3

    if-nez v3, :cond_c

    const/4 v3, 0x1

    :goto_2
    invoke-interface/range {v17 .. v17}, Ldet;->k()Z

    move-result v4

    if-nez v4, :cond_b

    const/4 v4, 0x0

    :goto_3
    add-int/lit8 v5, v13, 0x1

    move v13, v5

    move v12, v4

    move v11, v3

    goto :goto_1

    :cond_3
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v11, :cond_1

    if-nez v12, :cond_4

    invoke-static {v14, v9}, Ldrc;->a(ILandroid/app/NotificationManager;)V

    :cond_4
    if-eqz v12, :cond_6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static/range {p1 .. p1}, Ldrc;->b(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v4, v1, v2}, Ldrc;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, Ldeu;->a()I

    move-result v5

    const v6, 0x7f0f0015    # com.google.android.gms.R.plurals.games_notification_new_activity

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v8

    invoke-virtual {v3, v6, v5, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f02023a    # com.google.android.gms.R.drawable.stat_notify_google_play

    invoke-static {v3, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Ldrc;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lax;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lax;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8}, Lax;->b()Lax;

    move-result-object v8

    iput-object v3, v8, Lax;->g:Landroid/graphics/Bitmap;

    const v3, 0x7f02023b    # com.google.android.gms.R.drawable.stat_notify_play_games

    invoke-virtual {v8, v3}, Lax;->a(I)Lax;

    move-result-object v3

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Lax;->a(Ljava/lang/CharSequence;)Lax;

    move-result-object v3

    iput-object v6, v3, Lax;->b:Ljava/lang/CharSequence;

    iput-object v7, v3, Lax;->c:Ljava/lang/CharSequence;

    iput-object v4, v3, Lax;->d:Landroid/app/PendingIntent;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lax;->h:Ljava/lang/CharSequence;

    invoke-static/range {p0 .. p1}, Ldrc;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v4}, Lax;->a(Landroid/app/PendingIntent;)Lax;

    move-result-object v3

    invoke-virtual {v3}, Lax;->a()Lax;

    move-result-object v3

    const/4 v4, -0x2

    iput v4, v3, Lax;->j:I

    invoke-virtual {v3}, Lax;->c()Lax;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Ldrc;->a(Landroid/content/Context;Ljava/lang/String;Ldeu;)V

    invoke-virtual {v3}, Lax;->d()Landroid/app/Notification;

    move-result-object v3

    :goto_4
    if-eqz v3, :cond_5

    invoke-virtual {v9, v14, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_5
    sget-object v3, Ldrc;->b:Landroid/util/SparseArray;

    move-object/from16 v0, v16

    invoke-virtual {v3, v14, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual/range {p4 .. p4}, Ldeu;->a()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    invoke-static/range {p1 .. p1}, Ldrc;->b(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ldeu;->b(I)Ldet;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v3, v1, v2}, Ldrc;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-static/range {p0 .. p0}, Ldrc;->a(Landroid/content/Context;)Z

    move-result v10

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-interface {v7}, Ldet;->d()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_9

    if-eqz v10, :cond_9

    const v3, 0x7f0f0015    # com.google.android.gms.R.plurals.games_notification_new_activity

    const/4 v4, 0x1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v5, v6

    invoke-virtual {v11, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const v3, 0x7f02023a    # com.google.android.gms.R.drawable.stat_notify_google_play

    invoke-static {v11, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Ldrc;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v6, v5

    move-object/from16 v18, v4

    move-object v4, v3

    move-object/from16 v3, v18

    :goto_5
    if-nez v3, :cond_7

    const v3, 0x7f02023b    # com.google.android.gms.R.drawable.stat_notify_play_games

    invoke-static {v11, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    :cond_7
    new-instance v11, Lax;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lax;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11}, Lax;->b()Lax;

    move-result-object v11

    iput-object v3, v11, Lax;->g:Landroid/graphics/Bitmap;

    const v3, 0x7f02023b    # com.google.android.gms.R.drawable.stat_notify_play_games

    invoke-virtual {v11, v3}, Lax;->a(I)Lax;

    move-result-object v3

    invoke-virtual {v3, v6}, Lax;->a(Ljava/lang/CharSequence;)Lax;

    move-result-object v3

    iput-object v5, v3, Lax;->b:Ljava/lang/CharSequence;

    iput-object v4, v3, Lax;->c:Ljava/lang/CharSequence;

    iput-object v8, v3, Lax;->d:Landroid/app/PendingIntent;

    const-string v4, "1"

    iput-object v4, v3, Lax;->h:Ljava/lang/CharSequence;

    invoke-static/range {p0 .. p1}, Ldrc;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v4}, Lax;->a(Landroid/app/PendingIntent;)Lax;

    move-result-object v3

    invoke-virtual {v3}, Lax;->a()Lax;

    move-result-object v3

    const/4 v4, 0x0

    iput v4, v3, Lax;->j:I

    invoke-virtual {v3}, Lax;->c()Lax;

    move-result-object v3

    if-nez v10, :cond_8

    const/16 v4, 0x10

    invoke-static {v4}, Lbpz;->a(I)Z

    move-result v4

    if-eqz v4, :cond_8

    new-instance v4, Lay;

    invoke-direct {v4}, Lay;-><init>()V

    invoke-interface {v7}, Ldet;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lay;->b(Ljava/lang/CharSequence;)Lay;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lay;->a(Ljava/lang/CharSequence;)Lay;

    invoke-virtual {v3, v4}, Lax;->a(Lbf;)Lax;

    move-object/from16 v0, p2

    iput-object v0, v3, Lax;->c:Ljava/lang/CharSequence;

    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Ldrc;->a(Landroid/content/Context;Ljava/lang/String;Ldeu;)V

    invoke-virtual {v3}, Lax;->d()Landroid/app/Notification;

    move-result-object v3

    goto/16 :goto_4

    :cond_9
    invoke-static/range {p0 .. p0}, Lbhw;->a(Landroid/content/Context;)Lbhw;

    move-result-object v3

    invoke-interface {v7}, Ldet;->f()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v7}, Ldet;->g()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v7}, Ldrc;->a(Ljava/lang/String;Ldet;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Ldrc;->a(Landroid/content/Context;Lbhw;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-interface {v7}, Ldet;->h()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v18, v3

    move-object v3, v4

    move-object/from16 v4, v18

    goto/16 :goto_5

    :cond_a
    invoke-static/range {p0 .. p4}, Ldrc;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldeu;)Landroid/app/Notification;

    move-result-object v3

    goto/16 :goto_4

    :cond_b
    move v4, v12

    goto/16 :goto_3

    :cond_c
    move v3, v11

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    invoke-static {p0}, Lcto;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "useNewPlayerNotifications"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    invoke-static {p0}, Lcto;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "useNewPlayerNotifications"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;)I
    .locals 2

    sget v0, Ldrc;->a:I

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldeu;)Landroid/app/Notification;
    .locals 11

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p4}, Ldeu;->a()I

    move-result v5

    invoke-static {p1}, Ldrc;->b(Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0, p2, p3}, Ldrc;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-static {p0}, Ldrc;->a(Landroid/content/Context;)Z

    move-result v7

    const v0, 0x7f0f0015    # com.google.android.gms.R.plurals.games_notification_new_activity

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v3, v0, v5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz v7, :cond_2

    invoke-static {p0, p2}, Ldrc;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f02023a    # com.google.android.gms.R.drawable.stat_notify_google_play

    invoke-static {v3, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v4, v1

    :goto_0
    if-nez v0, :cond_0

    const v0, 0x7f02023b    # com.google.android.gms.R.drawable.stat_notify_play_games

    invoke-static {v3, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    const/4 v1, 0x1

    invoke-static {p1}, Ldrc;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_1

    invoke-virtual {p4, v3}, Ldeu;->b(I)Ldet;

    move-result-object v9

    invoke-interface {v9}, Ldet;->j()Z

    move-result v10

    if-nez v10, :cond_3

    invoke-interface {v9}, Ldet;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    invoke-interface {v9}, Ldet;->k()Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v1, 0x0

    :cond_1
    new-instance v3, Lax;

    invoke-direct {v3, p0}, Lax;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lax;->b()Lax;

    move-result-object v3

    iput-object v0, v3, Lax;->g:Landroid/graphics/Bitmap;

    const v0, 0x7f02023b    # com.google.android.gms.R.drawable.stat_notify_play_games

    invoke-virtual {v3, v0}, Lax;->a(I)Lax;

    move-result-object v3

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v3, v0}, Lax;->a(Ljava/lang/CharSequence;)Lax;

    move-result-object v0

    iput-object v2, v0, Lax;->b:Ljava/lang/CharSequence;

    iput-object v4, v0, Lax;->c:Ljava/lang/CharSequence;

    iput-object v6, v0, Lax;->d:Landroid/app/PendingIntent;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lax;->h:Ljava/lang/CharSequence;

    invoke-static {p0, p1}, Ldrc;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v2}, Lax;->a(Landroid/app/PendingIntent;)Lax;

    move-result-object v2

    if-eqz v1, :cond_5

    const/4 v0, -0x2

    :goto_3
    iput v0, v2, Lax;->j:I

    invoke-virtual {v2}, Lax;->a()Lax;

    move-result-object v0

    invoke-virtual {v0}, Lax;->c()Lax;

    move-result-object v1

    if-nez v7, :cond_8

    invoke-virtual {p4}, Ldeu;->a()I

    move-result v2

    new-instance v3, Lay;

    invoke-direct {v3}, Lay;-><init>()V

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_7

    invoke-virtual {p4, v0}, Ldeu;->b(I)Ldet;

    move-result-object v4

    invoke-interface {v4}, Ldet;->k()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-interface {v4}, Ldet;->i()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Lay;->b(Ljava/lang/CharSequence;)Lay;

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Ldeu;->b(I)Ldet;

    move-result-object v0

    invoke-static {p0}, Lbhw;->a(Landroid/content/Context;)Lbhw;

    move-result-object v1

    invoke-static {p1, v0}, Ldrc;->a(Ljava/lang/String;Ldet;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v1, v0}, Ldrc;->a(Landroid/content/Context;Lbhw;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v4, p2

    goto/16 :goto_0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    sub-int v0, v2, v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0016    # com.google.android.gms.R.plurals.games_notification_non_acl_summary

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lay;->b(Ljava/lang/CharSequence;)Lay;

    :cond_7
    invoke-virtual {v3, p2}, Lay;->a(Ljava/lang/CharSequence;)Lay;

    invoke-virtual {v1, v3}, Lax;->a(Lbf;)Lax;

    :cond_8
    invoke-static {p0, p2, p4}, Ldrc;->a(Landroid/content/Context;Ljava/lang/String;Ldeu;)V

    invoke-virtual {v1}, Lax;->d()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.ACKNOWLEDGE_NOTIFICATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p1}, Ldrc;->b(Ljava/lang/String;)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
