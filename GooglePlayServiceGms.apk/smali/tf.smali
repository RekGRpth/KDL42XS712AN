.class public abstract Ltf;
.super Lsc;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final g:Lsk;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string v0, "application/json; charset=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "utf-8"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltf;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Lsk;Lsj;)V
    .locals 0

    invoke-direct {p0, p1, p2, p5}, Lsc;-><init>(ILjava/lang/String;Lsj;)V

    iput-object p4, p0, Ltf;->g:Lsk;

    iput-object p3, p0, Ltf;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected abstract a(Lrz;)Lsi;
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Ltf;->g:Lsk;

    invoke-interface {v0, p1}, Lsk;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ltf;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()[B
    .locals 1

    invoke-virtual {p0}, Ltf;->m()[B

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    sget-object v0, Ltf;->f:Ljava/lang/String;

    return-object v0
.end method

.method public m()[B
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Ltf;->h:Ljava/lang/String;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ltf;->h:Ljava/lang/String;

    const-string v2, "utf-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "Unsupported Encoding while trying to get the bytes of %s using %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Ltf;->h:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "utf-8"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lsq;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
