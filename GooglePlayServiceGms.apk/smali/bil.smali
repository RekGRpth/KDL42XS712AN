.class final Lbil;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# static fields
.field private static final a:Lbil;

.field private static final b:Lbim;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lbil;

    invoke-direct {v0}, Lbil;-><init>()V

    sput-object v0, Lbil;->a:Lbil;

    new-instance v0, Lbim;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lbim;-><init>(B)V

    sput-object v0, Lbil;->b:Lbim;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    return-void
.end method

.method static synthetic a()Lbil;
    .locals 1

    sget-object v0, Lbil;->a:Lbil;

    return-object v0
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public final getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    sget-object v0, Lbil;->b:Lbim;

    return-object v0
.end method

.method public final getOpacity()I
    .locals 1

    const/4 v0, -0x2

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method
