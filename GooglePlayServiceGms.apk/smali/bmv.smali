.class public final Lbmv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lbmv;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lbmv;
    .locals 2

    const-class v1, Lbmv;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbmv;->a:Lbmv;

    if-nez v0, :cond_0

    new-instance v0, Lbmv;

    invoke-direct {v0}, Lbmv;-><init>()V

    sput-object v0, Lbmv;->a:Lbmv;

    :cond_0
    sget-object v0, Lbmv;->a:Lbmv;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbmv;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {p1}, Lbox;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmv;->b:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lbmv;->b:Ljava/lang/String;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2

    const-string v0, "X-Container-Url"

    invoke-virtual {p2, v0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "X-Network-ID"

    invoke-direct {p0, p1}, Lbmv;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "X-Auth-Time"

    if-nez p4, :cond_0

    const-string v0, "none"

    :goto_0
    invoke-virtual {p2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    invoke-virtual {p4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
