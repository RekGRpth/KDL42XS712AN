.class public final Ldtk;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:I

.field private final d:Z

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldtk;->b:Ldad;

    iput p3, p0, Ldtk;->c:I

    iput-boolean p4, p0, Ldtk;->d:Z

    iput-boolean p5, p0, Ldtk;->e:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldtk;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->d(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    iget-object v0, p0, Ldtk;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Ldtk;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v3, p0, Ldtk;->c:I

    iget-boolean v4, p0, Ldtk;->d:Z

    iget-boolean v5, p0, Ldtk;->e:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Ldtk;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v3, p0, Ldtk;->c:I

    iget-boolean v4, p0, Ldtk;->d:Z

    iget-boolean v5, p0, Ldtk;->e:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
