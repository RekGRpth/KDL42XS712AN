.class public final enum Ljco;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ljco;

.field public static final enum b:Ljco;

.field public static final enum c:Ljco;

.field public static final enum d:Ljco;

.field private static final synthetic f:[Ljco;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljco;

    const-string v1, "ENROLLMENT"

    invoke-direct {v0, v1, v2, v2}, Ljco;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljco;->a:Ljco;

    new-instance v0, Ljco;

    const-string v1, "TICKLE"

    invoke-direct {v0, v1, v3, v3}, Ljco;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljco;->b:Ljco;

    new-instance v0, Ljco;

    const-string v1, "TX_REQUEST"

    invoke-direct {v0, v1, v4, v4}, Ljco;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljco;->c:Ljco;

    new-instance v0, Ljco;

    const-string v1, "TX_REPLY"

    invoke-direct {v0, v1, v5, v5}, Ljco;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljco;->d:Ljco;

    const/4 v0, 0x4

    new-array v0, v0, [Ljco;

    sget-object v1, Ljco;->a:Ljco;

    aput-object v1, v0, v2

    sget-object v1, Ljco;->b:Ljco;

    aput-object v1, v0, v3

    sget-object v1, Ljco;->c:Ljco;

    aput-object v1, v0, v4

    sget-object v1, Ljco;->d:Ljco;

    aput-object v1, v0, v5

    sput-object v0, Ljco;->f:[Ljco;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Ljco;->e:I

    return-void
.end method

.method public static a(I)Ljco;
    .locals 5

    invoke-static {}, Ljco;->values()[Ljco;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget v4, v3, Ljco;->e:I

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported payload type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Ljco;
    .locals 1

    const-class v0, Ljco;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljco;

    return-object v0
.end method

.method public static values()[Ljco;
    .locals 1

    sget-object v0, Ljco;->f:[Ljco;

    invoke-virtual {v0}, [Ljco;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljco;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljco;->e:I

    return v0
.end method
