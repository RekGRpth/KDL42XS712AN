.class final Lbu;
.super Lci;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/lang/Object;

.field b:Z

.field final synthetic c:Lbt;

.field private e:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>(Lbt;)V
    .locals 2

    iput-object p1, p0, Lbu;->c:Lbt;

    invoke-direct {p0}, Lci;-><init>()V

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbu;->e:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lbu;->c:Lbt;

    iget-object v1, p0, Lbu;->a:Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Lbt;->a(Lbu;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbu;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbu;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lbu;->c:Lbt;

    iget-object v1, v0, Lbt;->a:Lbu;

    if-eq v1, p0, :cond_0

    invoke-virtual {v0, p0, p1}, Lbt;->a(Lbu;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lbu;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void

    :cond_0
    :try_start_1
    iget-boolean v1, v0, Lcb;->q:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0, p1}, Lbt;->a(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbu;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0

    :cond_1
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, v0, Lcb;->t:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lbt;->d:J

    const/4 v1, 0x0

    iput-object v1, v0, Lbt;->a:Lbu;

    invoke-virtual {v0, p1}, Lbt;->b(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected final synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbu;->c:Lbt;

    invoke-virtual {v0}, Lbt;->d()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lbu;->a:Ljava/lang/Object;

    iget-object v0, p0, Lbu;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final run()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbu;->b:Z

    iget-object v0, p0, Lbu;->c:Lbt;

    invoke-virtual {v0}, Lbt;->c()V

    return-void
.end method
