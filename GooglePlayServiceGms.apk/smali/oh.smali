.class public abstract Loh;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Lom;

.field private final d:Landroid/os/Messenger;

.field private final e:Lok;

.field private final f:Lol;

.field private g:Lnz;

.field private h:Lny;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "MediaRouteProviderSrv"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Loh;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Loh;->b:Ljava/util/ArrayList;

    new-instance v0, Lom;

    invoke-direct {v0, p0}, Lom;-><init>(Loh;)V

    iput-object v0, p0, Loh;->c:Lom;

    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Loh;->c:Lom;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Loh;->d:Landroid/os/Messenger;

    new-instance v0, Lok;

    invoke-direct {v0, p0, v2}, Lok;-><init>(Loh;B)V

    iput-object v0, p0, Loh;->e:Lok;

    new-instance v0, Lol;

    invoke-direct {v0, p0, v2}, Lol;-><init>(Loh;B)V

    iput-object v0, p0, Loh;->f:Lol;

    return-void
.end method

.method static synthetic a(Loh;Landroid/os/Messenger;)I
    .locals 1

    invoke-direct {p0, p1}, Loh;->c(Landroid/os/Messenger;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/os/Messenger;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Loh;->d(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Loh;)Lnz;
    .locals 1

    iget-object v0, p0, Loh;->g:Lnz;

    return-object v0
.end method

.method static synthetic a(Landroid/os/Messenger;I)V
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move-object v0, p0

    move v2, p1

    move v3, v1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Loh;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 4

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not send message to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Loh;->d(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Landroid/os/Messenger;IILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Loh;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Loh;Loe;)V
    .locals 9

    const/4 v5, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v4, p1, Loe;->a:Landroid/os/Bundle;

    :goto_0
    iget-object v0, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v7, v2

    :goto_1
    if-ge v7, v8, :cond_2

    iget-object v0, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Loj;

    iget-object v0, v6, Loj;->a:Landroid/os/Messenger;

    const/4 v1, 0x5

    move v3, v2

    invoke-static/range {v0 .. v5}, Loh;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    sget-boolean v0, Loh;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": Sent descriptor change event, descriptor="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_1
    move-object v4, v5

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Landroid/os/Messenger;II)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    if-lez p3, :cond_3

    invoke-direct {p0, p1}, Loh;->c(Landroid/os/Messenger;)I

    move-result v0

    if-gez v0, :cond_3

    new-instance v0, Loj;

    invoke-direct {v0, p0, p1, p3}, Loj;-><init>(Loh;Landroid/os/Messenger;I)V

    invoke-virtual {v0}, Loj;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Registered, version="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Loh;->g:Lnz;

    iget-object v0, v0, Lnz;->g:Loe;

    const/4 v1, 0x2

    if-eqz v0, :cond_2

    iget-object v4, v0, Loe;->a:Landroid/os/Bundle;

    :goto_0
    move-object v0, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Loh;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    :cond_1
    :goto_1
    return v3

    :cond_2
    move-object v4, v5

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method static synthetic a(Loh;Landroid/os/Messenger;I)Z
    .locals 4

    invoke-direct {p0, p1}, Loh;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loj;

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Unregistered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Loj;->b()V

    invoke-static {p1, p2}, Loh;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Loh;Landroid/os/Messenger;II)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Loh;->a(Landroid/os/Messenger;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Loh;Landroid/os/Messenger;III)Z
    .locals 3

    invoke-direct {p0, p1}, Loh;->b(Landroid/os/Messenger;)Loj;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Loj;->b(I)Lod;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p4}, Lod;->a(I)V

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route volume changed, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", volume="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Loh;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Loh;Landroid/os/Messenger;IILandroid/content/Intent;)Z
    .locals 8

    invoke-direct {p0, p1}, Loh;->b(Landroid/os/Messenger;)Loj;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, p3}, Loj;->b(I)Lod;

    move-result-object v7

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    new-instance v0, Loi;

    move-object v1, p0

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Loi;-><init>(Loh;Loj;ILandroid/content/Intent;Landroid/os/Messenger;I)V

    :cond_0
    invoke-virtual {v7, p4, v0}, Lod;->a(Landroid/content/Intent;Loq;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Loh;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Route control request delivered, controllerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Loh;Landroid/os/Messenger;IILjava/lang/String;)Z
    .locals 3

    invoke-direct {p0, p1}, Loh;->b(Landroid/os/Messenger;)Loj;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p4, p3}, Loj;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route controller created, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", routeId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Loh;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Loh;Landroid/os/Messenger;ILny;)Z
    .locals 4

    invoke-direct {p0, p1}, Loh;->b(Landroid/os/Messenger;)Loj;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Loj;->a(Lny;)Z

    move-result v1

    sget-boolean v2, Loh;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "MediaRouteProviderSrv"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": Set discovery request, request="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", actuallyChanged="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compositeDiscoveryRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Loh;->h:Lny;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Loh;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/os/Messenger;)Loj;
    .locals 2

    invoke-direct {p0, p1}, Loh;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loj;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/os/Messenger;I)V
    .locals 6

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    const/4 v3, 0x0

    move-object v0, p0

    move v2, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Loh;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Loh;Landroid/os/Messenger;)V
    .locals 4

    invoke-direct {p0, p1}, Loh;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loj;

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Binder died"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Loj;->b()V

    :cond_1
    return-void
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Loh;->a:Z

    return v0
.end method

.method static synthetic b(Loh;)Z
    .locals 9

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v7, v6

    move v1, v6

    move-object v3, v2

    :goto_0
    if-ge v7, v8, :cond_3

    iget-object v0, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loj;

    iget-object v4, v0, Loj;->c:Lny;

    if-eqz v4, :cond_b

    invoke-virtual {v4}, Lny;->a()Lon;

    move-result-object v0

    invoke-virtual {v0}, Lon;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lny;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_0
    invoke-virtual {v4}, Lny;->b()Z

    move-result v0

    or-int/2addr v0, v1

    if-nez v3, :cond_1

    move-object v1, v2

    move-object v2, v4

    :goto_1
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move-object v3, v2

    move-object v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v2, :cond_a

    new-instance v1, Loo;

    invoke-virtual {v3}, Lny;->a()Lon;

    move-result-object v2

    invoke-direct {v1, v2}, Loo;-><init>(Lon;)V

    :goto_2
    invoke-virtual {v4}, Lny;->a()Lon;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {v2}, Lon;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Loo;->a(Ljava/util/Collection;)Loo;

    move-object v2, v3

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_4

    new-instance v3, Lny;

    iget-object v0, v2, Loo;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_7

    sget-object v0, Lon;->a:Lon;

    :goto_3
    invoke-direct {v3, v0, v1}, Lny;-><init>(Lon;Z)V

    :cond_4
    iget-object v0, p0, Loh;->h:Lny;

    if-eq v0, v3, :cond_9

    iget-object v0, p0, Loh;->h:Lny;

    if-eqz v0, :cond_5

    iget-object v0, p0, Loh;->h:Lny;

    invoke-virtual {v0, v3}, Lny;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_5
    iput-object v3, p0, Loh;->h:Lny;

    iget-object v0, p0, Loh;->g:Lnz;

    invoke-static {}, Lop;->a()V

    iget-object v1, v0, Lnz;->e:Lny;

    if-eq v1, v3, :cond_6

    iget-object v1, v0, Lnz;->e:Lny;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lnz;->e:Lny;

    invoke-virtual {v1, v3}, Lny;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_6
    :goto_4
    move v0, v5

    :goto_5
    return v0

    :cond_7
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v0, "controlCategories"

    iget-object v7, v2, Loo;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0, v7}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    new-instance v0, Lon;

    iget-object v2, v2, Loo;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v4, v2, v6}, Lon;-><init>(Landroid/os/Bundle;Ljava/util/List;B)V

    goto :goto_3

    :cond_8
    iput-object v3, v0, Lnz;->e:Lny;

    iget-boolean v1, v0, Lnz;->f:Z

    if-nez v1, :cond_6

    iput-boolean v5, v0, Lnz;->f:Z

    iget-object v0, v0, Lnz;->c:Lob;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lob;->sendEmptyMessage(I)Z

    goto :goto_4

    :cond_9
    move v0, v6

    goto :goto_5

    :cond_a
    move-object v1, v2

    goto :goto_2

    :cond_b
    move v0, v1

    move-object v1, v2

    move-object v2, v3

    goto/16 :goto_1
.end method

.method static synthetic b(Loh;Landroid/os/Messenger;II)Z
    .locals 3

    invoke-direct {p0, p1}, Loh;->b(Landroid/os/Messenger;)Loj;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Loj;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route controller released, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Loh;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Loh;Landroid/os/Messenger;III)Z
    .locals 3

    invoke-direct {p0, p1}, Loh;->b(Landroid/os/Messenger;)Loj;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Loj;->b(I)Lod;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p4}, Lod;->b(I)V

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route volume updated, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", delta="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Loh;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/os/Messenger;)I
    .locals 3

    iget-object v0, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Loh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loj;

    invoke-virtual {v0, p1}, Loj;->a(Landroid/os/Messenger;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic c(Loh;)Lok;
    .locals 1

    iget-object v0, p0, Loh;->e:Lok;

    return-object v0
.end method

.method static synthetic c(Loh;Landroid/os/Messenger;II)Z
    .locals 3

    invoke-direct {p0, p1}, Loh;->b(Landroid/os/Messenger;)Loj;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Loj;->b(I)Lod;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lod;->b()V

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route selected, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Loh;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Landroid/os/Messenger;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Client connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Loh;Landroid/os/Messenger;II)Z
    .locals 3

    invoke-direct {p0, p1}, Loh;->b(Landroid/os/Messenger;)Loj;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Loj;->b(I)Lod;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lod;->c()V

    sget-boolean v1, Loh;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route unselected, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Loh;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Lnz;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Loh;->g:Lnz;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Loh;->a()Lnz;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lnz;->b:Loc;

    iget-object v1, v1, Loc;->a:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Loh;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onCreateMediaRouteProvider() returned a provider whose package name does not match the package name of the service.  A media route provider service can only export its own media route providers.  Provider package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".  Service package name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Loh;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v0, p0, Loh;->g:Lnz;

    iget-object v0, p0, Loh;->g:Lnz;

    iget-object v1, p0, Loh;->f:Lol;

    invoke-static {}, Lop;->a()V

    iput-object v1, v0, Lnz;->d:Loa;

    :cond_1
    iget-object v0, p0, Loh;->g:Lnz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Loh;->d:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
