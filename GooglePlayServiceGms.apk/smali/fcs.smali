.class final Lfcs;
.super Lfbx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lfch;

.field private final b:Lfaq;


# direct methods
.method public constructor <init>(Lfch;Lfaq;)V
    .locals 0

    iput-object p1, p0, Lfcs;->a:Lfch;

    invoke-direct {p0}, Lfbx;-><init>()V

    iput-object p2, p0, Lfcs;->b:Lfaq;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Circles callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1, p2}, Lfch;->a(ILandroid/os/Bundle;)Lbbo;

    move-result-object v1

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lfcs;->a:Lfch;

    new-instance v3, Lfci;

    iget-object v4, p0, Lfcs;->a:Lfch;

    iget-object v5, p0, Lfcs;->b:Lfaq;

    invoke-direct {v3, v4, v5, v1, v0}, Lfci;-><init>(Lfch;Lfaq;Lbbo;Lfeb;)V

    invoke-virtual {v2, v3}, Lfch;->b(Lbjg;)V

    return-void

    :cond_1
    new-instance v0, Lfeb;

    invoke-direct {v0, p3}, Lfeb;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method
