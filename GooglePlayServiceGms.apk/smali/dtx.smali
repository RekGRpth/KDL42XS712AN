.class public final Ldtx;
.super Ldrv;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private h:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V
    .locals 1

    invoke-virtual {p7}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, p1, v0}, Ldrv;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    iput-object p2, p0, Ldtx;->b:Ldad;

    iput-object p3, p0, Ldtx;->c:Ljava/lang/String;

    iput p6, p0, Ldtx;->d:I

    iput-object p7, p0, Ldtx;->e:Ljava/util/ArrayList;

    iput-object p4, p0, Ldtx;->f:Ljava/lang/String;

    iput p5, p0, Ldtx;->g:I

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lcun;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v4, 0x0

    if-ltz p3, :cond_1

    iget-object v0, p0, Ldtx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p3, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    if-nez p3, :cond_0

    iget-object v2, p0, Ldtx;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldtx;->c:Ljava/lang/String;

    new-instance v5, Landroid/content/SyncResult;

    invoke-direct {v5}, Landroid/content/SyncResult;-><init>()V

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLandroid/content/SyncResult;)I

    move-result v0

    iput v0, p0, Ldtx;->h:I

    :cond_0
    iget-object v0, p0, Ldtx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v2, p0, Ldtx;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldtx;->c:Ljava/lang/String;

    iget-object v4, p0, Ldtx;->f:Ljava/lang/String;

    iget v5, p0, Ldtx;->g:I

    iget v7, p0, Ldtx;->d:I

    iget v8, p0, Ldtx;->h:I

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIII)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v4

    goto :goto_0
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    const/4 v0, 0x0

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    move v1, v0

    move v2, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Ldtx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lded;->a(I)Ljava/lang/String;

    move-result-object v0

    aget-object v4, p1, v1

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    aget-object v0, p1, v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ldtx;->b:Ldad;

    iget v1, p0, Ldtx;->h:I

    invoke-static {v2, v1}, Lcum;->a(II)I

    move-result v1

    invoke-interface {v0, v1, v3}, Ldad;->b(ILandroid/os/Bundle;)V

    return-void
.end method
