.class final Lctf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbdo;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Lbdv;Lbdx;Lbdy;)Lbdn;
    .locals 16

    new-instance v1, Lcti;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcti;-><init>(B)V

    if-eqz p4, :cond_0

    move-object/from16 v0, p4

    instance-of v1, v0, Lcti;

    const-string v2, "Must provide valid GamesOptions!"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    check-cast p4, Lcti;

    :goto_0
    new-instance v1, Lcwm;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->d()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/common/internal/ClientSettings;->a()[Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->c()I

    move-result v9

    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/google/android/gms/common/internal/ClientSettings;->b:Landroid/view/View;

    move-object/from16 v0, p4

    iget-boolean v11, v0, Lcti;->a:Z

    move-object/from16 v0, p4

    iget-boolean v12, v0, Lcti;->b:Z

    move-object/from16 v0, p4

    iget v13, v0, Lcti;->c:I

    move-object/from16 v0, p4

    iget-boolean v14, v0, Lcti;->d:Z

    move-object/from16 v0, p4

    iget v15, v0, Lcti;->e:I

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v15}, Lcwm;-><init>(Landroid/content/Context;Landroid/os/Looper;Ljava/lang/String;Ljava/lang/String;Lbdx;Lbdy;[Ljava/lang/String;ILandroid/view/View;ZZIZI)V

    return-object v1

    :cond_0
    move-object/from16 p4, v1

    goto :goto_0
.end method
