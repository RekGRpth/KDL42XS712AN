.class public final Lmaps/be/f;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Lmaps/bv/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/f;->a:I

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/f;->b:I

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/f;->c:I

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/f;->d:I

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/f;->e:I

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/f;->f:I

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lmaps/bv/a;->e(I)J

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lmaps/bv/a;->b(I)Z

    return-void
.end method

.method public static b()Lmaps/bv/a;
    .locals 2

    new-instance v0, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/f;->l:Lmaps/bv/c;

    invoke-direct {v0, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    iget v0, p0, Lmaps/be/f;->f:I

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "maxTiles: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lmaps/be/f;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " maxServerTiles: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/f;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " prefetchPeriod: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/f;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " prefetchInitiatorDelay: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/f;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " prefetchInitiatorPeriod: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/f;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timeToWipe: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/f;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
