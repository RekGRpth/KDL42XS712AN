.class public abstract Lmaps/ap/e;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/ap/f;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/ap/f;)V
    .locals 0

    iput-object p1, p0, Lmaps/ap/e;->a:Lmaps/ap/f;

    return-void
.end method

.method protected a(Z)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method final b(Z)V
    .locals 2

    invoke-virtual {p0, p1}, Lmaps/ap/e;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/e;->a:Lmaps/ap/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/ap/f;->a(Lmaps/ap/e;)V

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final b()Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ap/e;->b:Z

    return v0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method
