.class public final Lmaps/aa/g;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/aa/r;


# direct methods
.method constructor <init>(Lmaps/aa/r;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2

    iget-object v0, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    invoke-virtual {v0}, Lmaps/aa/r;->d()D

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    invoke-virtual {v0}, Lmaps/aa/r;->f()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    invoke-virtual {v0}, Lmaps/aa/r;->g()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    invoke-virtual {v0}, Lmaps/aa/r;->e()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    invoke-virtual {v0}, Lmaps/aa/r;->h()Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lmaps/aa/g;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lmaps/aa/g;

    iget-object v0, p1, Lmaps/aa/g;->a:Lmaps/aa/r;

    iget-object v1, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    invoke-virtual {v0, v1}, Lmaps/aa/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method final f()Lmaps/aa/r;
    .locals 1

    iget-object v0, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    invoke-virtual {v0}, Lmaps/aa/r;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmaps/aa/g;->a:Lmaps/aa/r;

    invoke-virtual {v1}, Lmaps/aa/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
