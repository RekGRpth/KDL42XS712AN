.class public final Lmaps/az/c;
.super Lmaps/az/a;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lmaps/aj/ag;

.field private final c:F

.field private d:[F

.field private e:Lmaps/as/b;

.field private final f:Lmaps/at/i;

.field private volatile g:Ljava/lang/String;

.field private volatile h:I

.field private volatile i:I

.field private j:Lmaps/ap/b;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/az/a;-><init>()V

    iput-object v1, p0, Lmaps/az/c;->e:Lmaps/as/b;

    iput-object v1, p0, Lmaps/az/c;->g:Ljava/lang/String;

    iput v0, p0, Lmaps/az/c;->h:I

    iput v0, p0, Lmaps/az/c;->i:I

    sget-object v0, Lmaps/ap/b;->f:Lmaps/ap/b;

    iput-object v0, p0, Lmaps/az/c;->j:Lmaps/ap/b;

    iput-object p1, p0, Lmaps/az/c;->a:Landroid/content/res/Resources;

    sget v0, Lmaps/b/d;->i:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/az/c;->c:F

    new-instance v0, Lmaps/aj/ag;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lmaps/aj/ag;-><init>(F)V

    iput-object v0, p0, Lmaps/az/c;->b:Lmaps/aj/ag;

    new-instance v0, Lmaps/at/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lmaps/at/i;-><init>(I)V

    iput-object v0, p0, Lmaps/az/c;->f:Lmaps/at/i;

    return-void
.end method

.method private static a(Ljava/util/HashSet;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_1

    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lmaps/az/c;->e:Lmaps/as/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/az/c;->e:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/az/c;->e:Lmaps/as/b;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    iput p1, p0, Lmaps/az/c;->h:I

    iput p2, p0, Lmaps/az/c;->i:I

    return-void
.end method

.method public final a(Ljava/util/HashSet;Ljava/util/HashSet;I)V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v0, -0x1

    if-eq p3, v0, :cond_1

    :goto_0
    invoke-static {p1}, Lmaps/az/c;->a(Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lmaps/az/c;->a(Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lmaps/az/c;->a:Landroid/content/res/Resources;

    sget v3, Lmaps/b/h;->b:I

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v1, v4, v8

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v9

    const/4 v1, 0x4

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lmaps/az/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lmaps/az/c;->g:Ljava/lang/String;

    invoke-direct {p0}, Lmaps/az/c;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/az/c;->g:Ljava/lang/String;

    const-string v1, "&copy;"

    const-string v2, "\u00a9"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/az/c;->g:Ljava/lang/String;

    return-void

    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result p3

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lmaps/az/c;->a:Landroid/content/res/Resources;

    sget v1, Lmaps/b/h;->c:I

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v1, p0, Lmaps/az/c;->a:Landroid/content/res/Resources;

    sget v2, Lmaps/b/h;->e:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lmaps/az/c;->a:Landroid/content/res/Resources;

    sget v2, Lmaps/b/h;->d:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v1, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 11

    const/4 v4, 0x0

    const/4 v10, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lmaps/az/c;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v0

    iget-object v1, p0, Lmaps/az/c;->j:Lmaps/ap/b;

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lmaps/az/c;->b()V

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/az/c;->j:Lmaps/ap/b;

    :cond_1
    iget-object v0, p0, Lmaps/az/c;->e:Lmaps/as/b;

    if-nez v0, :cond_3

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v0

    const/high16 v6, -0x1000000

    sget-object v1, Lmaps/ap/b;->b:Lmaps/ap/b;

    if-ne v0, v1, :cond_4

    const/4 v6, -0x1

    :cond_2
    :goto_1
    iget-object v0, p0, Lmaps/az/c;->b:Lmaps/aj/ag;

    iget-object v2, p0, Lmaps/az/c;->g:Ljava/lang/String;

    sget-object v3, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    iget v5, p0, Lmaps/az/c;->c:F

    move-object v1, p1

    move v8, v7

    invoke-virtual/range {v0 .. v8}, Lmaps/aj/ag;->a(Lmaps/as/a;Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)Lmaps/as/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/az/c;->e:Lmaps/as/b;

    iget-object v1, p0, Lmaps/az/c;->b:Lmaps/aj/ag;

    iget-object v2, p0, Lmaps/az/c;->g:Ljava/lang/String;

    sget-object v3, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    iget v5, p0, Lmaps/az/c;->c:F

    move v6, v7

    invoke-virtual/range {v1 .. v6}, Lmaps/aj/ag;->a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FZ)[F

    move-result-object v0

    iput-object v0, p0, Lmaps/az/c;->d:[F

    iget-object v0, p0, Lmaps/az/c;->f:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/az/c;->e:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->e()F

    move-result v0

    iget-object v1, p0, Lmaps/az/c;->e:Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->f()F

    move-result v1

    iget-object v2, p0, Lmaps/az/c;->f:Lmaps/at/i;

    invoke-virtual {v2, v10, v10}, Lmaps/at/i;->a(FF)V

    iget-object v2, p0, Lmaps/az/c;->f:Lmaps/at/i;

    invoke-virtual {v2, v10, v1}, Lmaps/at/i;->a(FF)V

    iget-object v2, p0, Lmaps/az/c;->f:Lmaps/at/i;

    invoke-virtual {v2, v0, v10}, Lmaps/at/i;->a(FF)V

    iget-object v2, p0, Lmaps/az/c;->f:Lmaps/at/i;

    invoke-virtual {v2, v0, v1}, Lmaps/at/i;->a(FF)V

    :cond_3
    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p2}, Lmaps/ar/a;->i()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/az/c;->d:[F

    aget v1, v1, v7

    sub-float/2addr v0, v1

    iget v1, p0, Lmaps/az/c;->h:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lmaps/az/c;->i:I

    int-to-float v1, v1

    invoke-interface {v9, v0, v1, v10}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v0, p0, Lmaps/az/c;->d:[F

    aget v0, v0, v7

    iget-object v1, p0, Lmaps/az/c;->d:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v9, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, Lmaps/as/a;->r()V

    const/16 v0, 0x2300

    const/16 v1, 0x2200

    const/16 v2, 0x1e01

    invoke-interface {v9, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v0, p0, Lmaps/az/c;->f:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    iget-object v0, p1, Lmaps/as/a;->d:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/az/c;->e:Lmaps/as/b;

    invoke-virtual {v0, v9}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-interface {v9, v0, v7, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p1}, Lmaps/as/a;->s()V

    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0

    :cond_4
    sget-object v1, Lmaps/ap/b;->c:Lmaps/ap/b;

    if-ne v0, v1, :cond_2

    const v6, -0x3f3f40

    goto/16 :goto_1
.end method

.method public final c(Lmaps/as/a;)V
    .locals 1

    invoke-direct {p0}, Lmaps/az/c;->b()V

    iget-object v0, p0, Lmaps/az/c;->b:Lmaps/aj/ag;

    invoke-virtual {v0}, Lmaps/aj/ag;->a()V

    return-void
.end method
