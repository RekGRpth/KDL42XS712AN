.class public abstract Lmaps/m/be;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Map;


# instance fields
.field private transient a:Lmaps/m/bo;

.field private transient b:Lmaps/m/bo;

.field private transient c:Lmaps/m/ap;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "null key in entry: null=%s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "null value in entry: %s=null"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {p1, v0, v1}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1}, Lmaps/m/co;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Map;)Lmaps/m/be;
    .locals 4

    const/4 v1, 0x0

    instance-of v0, p0, Lmaps/m/be;

    if-eqz v0, :cond_0

    instance-of v0, p0, Lmaps/m/bv;

    if-nez v0, :cond_0

    move-object v0, p0

    check-cast v0, Lmaps/m/be;

    invoke-virtual {v0}, Lmaps/m/be;->e()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Ljava/util/EnumMap;

    if-eqz v0, :cond_2

    check-cast p0, Ljava/util/EnumMap;

    invoke-virtual {p0}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/util/EnumMap;

    invoke-direct {v0, p0}, Ljava/util/EnumMap;-><init>(Ljava/util/EnumMap;)V

    invoke-static {v0}, Lmaps/m/at;->a(Ljava/util/EnumMap;)Lmaps/m/be;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    new-array v2, v1, [Ljava/util/Map$Entry;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/Map$Entry;

    array-length v2, v0

    packed-switch v2, :pswitch_data_0

    :goto_2
    array-length v2, v0

    if-ge v1, v2, :cond_3

    aget-object v2, v0, v1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    aget-object v3, v0, v1

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/m/be;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :pswitch_0
    sget-object v0, Lmaps/m/ab;->a:Lmaps/m/ab;

    goto :goto_0

    :pswitch_1
    new-instance v2, Lmaps/m/dn;

    aget-object v3, v0, v1

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    aget-object v0, v0, v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Lmaps/m/be;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {v2, v0}, Lmaps/m/dn;-><init>(Ljava/util/Map$Entry;)V

    move-object v0, v2

    goto :goto_0

    :cond_3
    new-instance v1, Lmaps/m/db;

    invoke-direct {v1, v0}, Lmaps/m/db;-><init>([Ljava/util/Map$Entry;)V

    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static g()Lmaps/m/be;
    .locals 1

    sget-object v0, Lmaps/m/ab;->a:Lmaps/m/ab;

    return-object v0
.end method

.method public static h()Lmaps/m/bf;
    .locals 1

    new-instance v0, Lmaps/m/bf;

    invoke-direct {v0}, Lmaps/m/bf;-><init>()V

    return-object v0
.end method


# virtual methods
.method a()Lmaps/m/bo;
    .locals 1

    new-instance v0, Lmaps/m/bh;

    invoke-direct {v0, p0}, Lmaps/m/bh;-><init>(Lmaps/m/be;)V

    return-object v0
.end method

.method public b()Lmaps/m/bo;
    .locals 1

    iget-object v0, p0, Lmaps/m/be;->a:Lmaps/m/bo;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/m/be;->c()Lmaps/m/bo;

    move-result-object v0

    iput-object v0, p0, Lmaps/m/be;->a:Lmaps/m/bo;

    :cond_0
    return-object v0
.end method

.method abstract c()Lmaps/m/bo;
.end method

.method public final clear()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/m/be;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p0, p1}, Lmaps/m/co;->e(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lmaps/m/bo;
    .locals 1

    iget-object v0, p0, Lmaps/m/be;->b:Lmaps/m/bo;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/m/be;->a()Lmaps/m/bo;

    move-result-object v0

    iput-object v0, p0, Lmaps/m/be;->b:Lmaps/m/bo;

    :cond_0
    return-object v0
.end method

.method abstract e()Z
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/be;->b()Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {p0, p1}, Lmaps/m/co;->d(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Lmaps/m/ap;
    .locals 1

    iget-object v0, p0, Lmaps/m/be;->c:Lmaps/m/ap;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/m/bj;

    invoke-direct {v0, p0}, Lmaps/m/bj;-><init>(Lmaps/m/be;)V

    iput-object v0, p0, Lmaps/m/be;->c:Lmaps/m/ap;

    :cond_0
    return-object v0
.end method

.method public abstract get(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lmaps/m/be;->b()Lmaps/m/bo;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/bo;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/m/be;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/be;->d()Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lmaps/m/co;->b(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/be;->f()Lmaps/m/ap;

    move-result-object v0

    return-object v0
.end method
