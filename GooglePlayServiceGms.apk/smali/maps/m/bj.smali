.class final Lmaps/m/bj;
.super Lmaps/m/ap;


# instance fields
.field private final a:Lmaps/m/be;


# direct methods
.method constructor <init>(Lmaps/m/be;)V
    .locals 0

    invoke-direct {p0}, Lmaps/m/ap;-><init>()V

    iput-object p1, p0, Lmaps/m/bj;->a:Lmaps/m/be;

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b()Lmaps/m/ee;
    .locals 1

    iget-object v0, p0, Lmaps/m/bj;->a:Lmaps/m/be;

    invoke-virtual {v0}, Lmaps/m/be;->b()Lmaps/m/bo;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/bo;->b()Lmaps/m/ee;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/co;->a(Lmaps/m/ee;)Lmaps/m/ee;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/m/bj;->a:Lmaps/m/be;

    invoke-virtual {v0, p1}, Lmaps/m/be;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final i()Lmaps/m/ay;
    .locals 2

    iget-object v0, p0, Lmaps/m/bj;->a:Lmaps/m/be;

    invoke-virtual {v0}, Lmaps/m/be;->b()Lmaps/m/bo;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/bo;->c()Lmaps/m/ay;

    move-result-object v0

    new-instance v1, Lmaps/m/bk;

    invoke-direct {v1, p0, v0}, Lmaps/m/bk;-><init>(Lmaps/m/bj;Lmaps/m/ay;)V

    return-object v1
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/bj;->b()Lmaps/m/ee;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lmaps/m/bj;->a:Lmaps/m/be;

    invoke-virtual {v0}, Lmaps/m/be;->size()I

    move-result v0

    return v0
.end method
