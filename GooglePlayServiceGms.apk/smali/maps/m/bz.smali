.class public final Lmaps/m/bz;
.super Ljava/lang/Object;


# direct methods
.method public static a(Ljava/lang/Iterable;Lmaps/k/e;)Ljava/lang/Iterable;
    .locals 1

    invoke-static {p0}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lmaps/m/cb;

    invoke-direct {v0, p0, p1}, Lmaps/m/cb;-><init>(Ljava/lang/Iterable;Lmaps/k/e;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;Lmaps/k/p;)Ljava/lang/Iterable;
    .locals 1

    invoke-static {p0}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lmaps/m/ca;

    invoke-direct {v0, p0, p1}, Lmaps/m/ca;-><init>(Ljava/lang/Iterable;Lmaps/k/p;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/cc;->c(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
    .locals 1

    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/m/z;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lmaps/m/cc;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    move-result v0

    goto :goto_0
.end method

.method static b(Ljava/lang/Iterable;)[Ljava/lang/Object;
    .locals 1

    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/Collection;

    :goto_0
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object p0

    goto :goto_0
.end method
