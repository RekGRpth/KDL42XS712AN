.class Lmaps/m/o;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field private b:Ljava/util/Collection;

.field private synthetic c:Lmaps/m/n;


# direct methods
.method constructor <init>(Lmaps/m/n;)V
    .locals 1

    iput-object p1, p0, Lmaps/m/o;->c:Lmaps/m/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lmaps/m/o;->c:Lmaps/m/n;

    iget-object v0, v0, Lmaps/m/n;->b:Ljava/util/Collection;

    iput-object v0, p0, Lmaps/m/o;->b:Ljava/util/Collection;

    iget-object v0, p1, Lmaps/m/n;->d:Lmaps/m/e;

    iget-object v0, p1, Lmaps/m/n;->b:Ljava/util/Collection;

    invoke-static {v0}, Lmaps/m/e;->a(Ljava/util/Collection;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lmaps/m/o;->a:Ljava/util/Iterator;

    return-void
.end method

.method constructor <init>(Lmaps/m/n;Ljava/util/Iterator;)V
    .locals 1

    iput-object p1, p0, Lmaps/m/o;->c:Lmaps/m/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lmaps/m/o;->c:Lmaps/m/n;

    iget-object v0, v0, Lmaps/m/n;->b:Ljava/util/Collection;

    iput-object v0, p0, Lmaps/m/o;->b:Ljava/util/Collection;

    iput-object p2, p0, Lmaps/m/o;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Lmaps/m/o;->c:Lmaps/m/n;

    invoke-virtual {v0}, Lmaps/m/n;->a()V

    iget-object v0, p0, Lmaps/m/o;->c:Lmaps/m/n;

    iget-object v0, v0, Lmaps/m/n;->b:Ljava/util/Collection;

    iget-object v1, p0, Lmaps/m/o;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/m/o;->a()V

    iget-object v0, p0, Lmaps/m/o;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/o;->a()V

    iget-object v0, p0, Lmaps/m/o;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    iget-object v0, p0, Lmaps/m/o;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    iget-object v0, p0, Lmaps/m/o;->c:Lmaps/m/n;

    iget-object v0, v0, Lmaps/m/n;->d:Lmaps/m/e;

    invoke-static {v0}, Lmaps/m/e;->b(Lmaps/m/e;)I

    iget-object v0, p0, Lmaps/m/o;->c:Lmaps/m/n;

    invoke-virtual {v0}, Lmaps/m/n;->b()V

    return-void
.end method
