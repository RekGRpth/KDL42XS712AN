.class public abstract Lmaps/m/ap;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Collection;


# instance fields
.field private transient a:Lmaps/m/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmaps/m/ar;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/m/ar;-><init>(B)V

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Z
.end method

.method public final add(Ljava/lang/Object;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract b()Lmaps/m/ee;
.end method

.method public c()Lmaps/m/ay;
    .locals 1

    iget-object v0, p0, Lmaps/m/ap;->a:Lmaps/m/ay;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/m/ap;->i()Lmaps/m/ay;

    move-result-object v0

    iput-object v0, p0, Lmaps/m/ap;->a:Lmaps/m/ay;

    :cond_0
    return-object v0
.end method

.method public final clear()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lmaps/m/ap;->b()Lmaps/m/ee;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/m/cc;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1

    invoke-static {p0, p1}, Lmaps/m/z;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method i()Lmaps/m/ay;
    .locals 2

    invoke-virtual {p0}, Lmaps/m/ap;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lmaps/m/cz;

    invoke-virtual {p0}, Lmaps/m/ap;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lmaps/m/cz;-><init>(Lmaps/m/ap;[Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {}, Lmaps/m/ay;->e()Lmaps/m/ay;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lmaps/m/ap;->b()Lmaps/m/ee;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/ee;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/ay;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/m/ap;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/ap;->b()Lmaps/m/ee;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    invoke-static {p0}, Lmaps/m/cx;->a(Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    invoke-static {p0, p1}, Lmaps/m/cx;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lmaps/m/z;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
