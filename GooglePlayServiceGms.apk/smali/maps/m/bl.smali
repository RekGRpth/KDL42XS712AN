.class public abstract Lmaps/m/bl;
.super Lmaps/m/u;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private transient a:I

.field final transient b:Lmaps/m/be;


# direct methods
.method constructor <init>(Lmaps/m/be;I)V
    .locals 0

    invoke-direct {p0}, Lmaps/m/u;-><init>()V

    iput-object p1, p0, Lmaps/m/bl;->b:Lmaps/m/be;

    iput p2, p0, Lmaps/m/bl;->a:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lmaps/m/bl;->a:I

    return v0
.end method

.method public abstract a(Ljava/lang/Object;)Lmaps/m/ap;
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/m/bl;->a(Ljava/lang/Object;)Lmaps/m/ap;

    move-result-object v0

    return-object v0
.end method

.method final b()Ljava/util/Map;
    .locals 2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final bridge synthetic c()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/m/bl;->b:Lmaps/m/be;

    return-object v0
.end method

.method public final synthetic d()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/m/bl;->b:Lmaps/m/be;

    invoke-virtual {v0}, Lmaps/m/be;->d()Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic e()Z
    .locals 1

    invoke-super {p0}, Lmaps/m/u;->e()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-super {p0, p1}, Lmaps/m/u;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    invoke-super {p0}, Lmaps/m/u;->hashCode()I

    move-result v0

    return v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lmaps/m/u;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
