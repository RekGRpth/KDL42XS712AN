.class final Lmaps/m/y;
.super Lmaps/m/cy;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Lmaps/k/e;

.field private b:Lmaps/m/cy;


# direct methods
.method constructor <init>(Lmaps/k/e;Lmaps/m/cy;)V
    .locals 1

    invoke-direct {p0}, Lmaps/m/cy;-><init>()V

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/k/e;

    iput-object v0, p0, Lmaps/m/y;->a:Lmaps/k/e;

    invoke-static {p2}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/m/cy;

    iput-object v0, p0, Lmaps/m/y;->b:Lmaps/m/cy;

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lmaps/m/y;->b:Lmaps/m/cy;

    iget-object v1, p0, Lmaps/m/y;->a:Lmaps/k/e;

    invoke-interface {v1, p1}, Lmaps/k/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lmaps/m/y;->a:Lmaps/k/e;

    invoke-interface {v2, p2}, Lmaps/k/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/m/cy;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/m/y;

    if-eqz v2, :cond_3

    check-cast p1, Lmaps/m/y;

    iget-object v2, p0, Lmaps/m/y;->a:Lmaps/k/e;

    iget-object v3, p1, Lmaps/m/y;->a:Lmaps/k/e;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmaps/m/y;->b:Lmaps/m/cy;

    iget-object v3, p1, Lmaps/m/y;->b:Lmaps/m/cy;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/m/y;->a:Lmaps/k/e;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/m/y;->b:Lmaps/m/cy;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmaps/m/y;->b:Lmaps/m/cy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onResultOf("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/m/y;->a:Lmaps/k/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
