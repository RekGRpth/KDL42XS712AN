.class final Lmaps/m/db;
.super Lmaps/m/be;


# instance fields
.field private final transient a:[Lmaps/m/dd;

.field private final transient b:[Lmaps/m/dd;

.field private final transient c:I


# direct methods
.method varargs constructor <init>([Ljava/util/Map$Entry;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/m/be;-><init>()V

    array-length v5, p1

    new-array v0, v5, [Lmaps/m/dd;

    iput-object v0, p0, Lmaps/m/db;->a:[Lmaps/m/dd;

    invoke-static {v5}, Lmaps/m/am;->b(I)I

    move-result v0

    new-array v3, v0, [Lmaps/m/dd;

    iput-object v3, p0, Lmaps/m/db;->b:[Lmaps/m/dd;

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/m/db;->c:I

    move v4, v2

    :goto_0
    if-ge v4, v5, :cond_3

    aget-object v0, p1, v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Lmaps/m/am;->a(I)I

    move-result v3

    iget v7, p0, Lmaps/m/db;->c:I

    and-int/2addr v7, v3

    iget-object v3, p0, Lmaps/m/db;->b:[Lmaps/m/dd;

    aget-object v3, v3, v7

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    if-nez v3, :cond_0

    new-instance v0, Lmaps/m/df;

    invoke-direct {v0, v6, v8}, Lmaps/m/df;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    iget-object v8, p0, Lmaps/m/db;->b:[Lmaps/m/dd;

    aput-object v0, v8, v7

    iget-object v7, p0, Lmaps/m/db;->a:[Lmaps/m/dd;

    aput-object v0, v7, v4

    :goto_2
    if-eqz v3, :cond_2

    invoke-interface {v3}, Lmaps/m/dd;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_3
    const-string v7, "duplicate key: %s"

    new-array v8, v1, [Ljava/lang/Object;

    aput-object v6, v8, v2

    invoke-static {v0, v7, v8}, Lmaps/k/o;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v3}, Lmaps/m/dd;->a()Lmaps/m/dd;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    :cond_0
    new-instance v0, Lmaps/m/de;

    invoke-direct {v0, v6, v8, v3}, Lmaps/m/de;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lmaps/m/dd;)V

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    return-void
.end method

.method static synthetic a(Lmaps/m/db;)[Lmaps/m/dd;
    .locals 1

    iget-object v0, p0, Lmaps/m/db;->a:[Lmaps/m/dd;

    return-object v0
.end method


# virtual methods
.method final c()Lmaps/m/bo;
    .locals 2

    new-instance v0, Lmaps/m/dc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/m/dc;-><init>(Lmaps/m/db;B)V

    return-object v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lmaps/m/db;->a:[Lmaps/m/dd;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method final e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Lmaps/m/am;->a(I)I

    move-result v1

    iget v2, p0, Lmaps/m/db;->c:I

    and-int/2addr v1, v2

    iget-object v2, p0, Lmaps/m/db;->b:[Lmaps/m/dd;

    aget-object v1, v2, v1

    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lmaps/m/dd;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lmaps/m/dd;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Lmaps/m/dd;->a()Lmaps/m/dd;

    move-result-object v1

    goto :goto_1
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lmaps/m/db;->a:[Lmaps/m/dd;

    array-length v0, v0

    return v0
.end method
