.class final Lmaps/m/cn;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field private a:Z

.field private b:Z

.field private synthetic c:Ljava/util/ListIterator;

.field private synthetic d:Lmaps/m/cm;


# direct methods
.method constructor <init>(Lmaps/m/cm;Ljava/util/ListIterator;)V
    .locals 0

    iput-object p1, p0, Lmaps/m/cn;->d:Lmaps/m/cm;

    iput-object p2, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/m/cn;->a:Z

    iput-boolean v0, p0, Lmaps/m/cn;->b:Z

    return-void
.end method

.method public final hasNext()Z
    .locals 1

    iget-object v0, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final hasPrevious()Z
    .locals 1

    iget-object v0, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/cn;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/m/cn;->a:Z

    iput-boolean v0, p0, Lmaps/m/cn;->b:Z

    iget-object v0, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .locals 2

    iget-object v0, p0, Lmaps/m/cn;->d:Lmaps/m/cm;

    iget-object v1, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v1}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    invoke-static {v0, v1}, Lmaps/m/cm;->a(Lmaps/m/cm;I)I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/cn;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/m/cn;->a:Z

    iput-boolean v0, p0, Lmaps/m/cn;->b:Z

    iget-object v0, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    invoke-virtual {p0}, Lmaps/m/cn;->nextIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .locals 1

    iget-boolean v0, p0, Lmaps/m/cn;->a:Z

    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    iget-object v0, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/m/cn;->b:Z

    iput-boolean v0, p0, Lmaps/m/cn;->a:Z

    return-void
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1

    iget-boolean v0, p0, Lmaps/m/cn;->b:Z

    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    iget-object v0, p0, Lmaps/m/cn;->c:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    return-void
.end method
