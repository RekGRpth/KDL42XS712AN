.class abstract Lmaps/m/an;
.super Lmaps/m/ay;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmaps/m/ay;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/m/an;->d()Lmaps/m/ap;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/ap;->a()Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0}, Lmaps/m/an;->d()Lmaps/m/ap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/m/ap;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method abstract d()Lmaps/m/ap;
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/m/an;->d()Lmaps/m/ap;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/ap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    invoke-virtual {p0}, Lmaps/m/an;->d()Lmaps/m/ap;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/ap;->size()I

    move-result v0

    return v0
.end method
