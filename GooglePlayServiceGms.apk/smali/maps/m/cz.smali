.class Lmaps/m/cz;
.super Lmaps/m/an;


# instance fields
.field private final a:Lmaps/m/ap;

.field private final b:Lmaps/m/ay;


# direct methods
.method private constructor <init>(Lmaps/m/ap;Lmaps/m/ay;)V
    .locals 0

    invoke-direct {p0}, Lmaps/m/an;-><init>()V

    iput-object p1, p0, Lmaps/m/cz;->a:Lmaps/m/ap;

    iput-object p2, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    return-void
.end method

.method constructor <init>(Lmaps/m/ap;[Ljava/lang/Object;)V
    .locals 1

    invoke-static {p2}, Lmaps/m/ay;->b([Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmaps/m/cz;-><init>(Lmaps/m/ap;Lmaps/m/ay;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lmaps/m/ef;
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    invoke-virtual {v0, p1}, Lmaps/m/ay;->a(I)Lmaps/m/ef;

    move-result-object v0

    return-object v0
.end method

.method final d()Lmaps/m/ap;
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->a:Lmaps/m/ap;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    invoke-virtual {v0, p1}, Lmaps/m/ay;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    invoke-virtual {v0, p1}, Lmaps/m/ay;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    invoke-virtual {v0}, Lmaps/m/ay;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    invoke-virtual {v0, p1}, Lmaps/m/ay;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    invoke-virtual {v0, p1}, Lmaps/m/ay;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/m/cz;->a(I)Lmaps/m/ef;

    move-result-object v0

    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    invoke-virtual {v0}, Lmaps/m/ay;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/cz;->b:Lmaps/m/ay;

    invoke-virtual {v0, p1}, Lmaps/m/ay;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
