.class abstract Lmaps/m/u;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/m/cu;


# instance fields
.field private transient a:Ljava/util/Set;

.field private transient b:Ljava/util/Map;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/m/u;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method abstract b()Ljava/util/Map;
.end method

.method public c()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/m/u;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/m/u;->b()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/m/u;->b:Ljava/util/Map;

    :cond_0
    return-object v0
.end method

.method public d()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/m/u;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/m/u;->h()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmaps/m/u;->a:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public e()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/m/u;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/m/cu;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/m/cu;

    invoke-virtual {p0}, Lmaps/m/u;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lmaps/m/cu;->c()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method h()Ljava/util/Set;
    .locals 1

    new-instance v0, Lmaps/m/v;

    invoke-direct {v0, p0}, Lmaps/m/v;-><init>(Lmaps/m/u;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lmaps/m/u;->c()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/u;->c()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
