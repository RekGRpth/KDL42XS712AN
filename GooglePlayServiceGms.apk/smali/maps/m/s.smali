.class final Lmaps/m/s;
.super Lmaps/m/n;

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field private synthetic e:Lmaps/m/e;


# direct methods
.method constructor <init>(Lmaps/m/e;Ljava/lang/Object;Ljava/util/SortedSet;Lmaps/m/n;)V
    .locals 0

    iput-object p1, p0, Lmaps/m/s;->e:Lmaps/m/e;

    invoke-direct {p0, p1, p2, p3, p4}, Lmaps/m/n;-><init>(Lmaps/m/e;Ljava/lang/Object;Ljava/util/Collection;Lmaps/m/n;)V

    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lmaps/m/n;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/s;->a()V

    iget-object v0, p0, Lmaps/m/n;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    invoke-virtual {p0}, Lmaps/m/s;->a()V

    new-instance v1, Lmaps/m/s;

    iget-object v2, p0, Lmaps/m/s;->e:Lmaps/m/e;

    iget-object v3, p0, Lmaps/m/n;->a:Ljava/lang/Object;

    iget-object v0, p0, Lmaps/m/n;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lmaps/m/n;->c:Lmaps/m/n;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lmaps/m/s;-><init>(Lmaps/m/e;Ljava/lang/Object;Ljava/util/SortedSet;Lmaps/m/n;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lmaps/m/n;->c:Lmaps/m/n;

    goto :goto_0
.end method

.method public final last()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/s;->a()V

    iget-object v0, p0, Lmaps/m/n;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    invoke-virtual {p0}, Lmaps/m/s;->a()V

    new-instance v1, Lmaps/m/s;

    iget-object v2, p0, Lmaps/m/s;->e:Lmaps/m/e;

    iget-object v3, p0, Lmaps/m/n;->a:Ljava/lang/Object;

    iget-object v0, p0, Lmaps/m/n;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lmaps/m/n;->c:Lmaps/m/n;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lmaps/m/s;-><init>(Lmaps/m/e;Ljava/lang/Object;Ljava/util/SortedSet;Lmaps/m/n;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lmaps/m/n;->c:Lmaps/m/n;

    goto :goto_0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    invoke-virtual {p0}, Lmaps/m/s;->a()V

    new-instance v1, Lmaps/m/s;

    iget-object v2, p0, Lmaps/m/s;->e:Lmaps/m/e;

    iget-object v3, p0, Lmaps/m/n;->a:Ljava/lang/Object;

    iget-object v0, p0, Lmaps/m/n;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lmaps/m/n;->c:Lmaps/m/n;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lmaps/m/s;-><init>(Lmaps/m/e;Ljava/lang/Object;Ljava/util/SortedSet;Lmaps/m/n;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lmaps/m/n;->c:Lmaps/m/n;

    goto :goto_0
.end method
