.class public Lmaps/y/b;
.super Lmaps/y/f;


# instance fields
.field private final d:Lmaps/ay/as;


# direct methods
.method public constructor <init>(Lmaps/ao/a;)V
    .locals 1

    sget-object v0, Lmaps/ao/b;->h:Lmaps/ao/b;

    invoke-direct {p0, v0, p1}, Lmaps/y/f;-><init>(Lmaps/ao/b;Lmaps/ao/a;)V

    new-instance v0, Lmaps/ay/as;

    invoke-direct {v0}, Lmaps/ay/as;-><init>()V

    iput-object v0, p0, Lmaps/y/b;->d:Lmaps/ay/as;

    return-void
.end method


# virtual methods
.method public final b(Lmaps/ar/a;)Ljava/util/List;
    .locals 4

    const/16 v3, 0x10

    invoke-super {p0, p1}, Lmaps/y/f;->b(Lmaps/ar/a;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/b;->d:Lmaps/ay/as;

    invoke-virtual {p1}, Lmaps/ar/a;->g()Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ay/as;->a(Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/y/b;->d:Lmaps/ay/as;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v3, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    :cond_0
    return-object v0
.end method
