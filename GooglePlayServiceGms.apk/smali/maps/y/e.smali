.class public final Lmaps/y/e;
.super Lmaps/u/b;


# instance fields
.field private a:Lmaps/u/a;

.field private b:Lmaps/y/f;

.field private c:Lmaps/ao/b;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmaps/u/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lmaps/ao/b;IZLmaps/ao/a;)Lmaps/u/a;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/e;->a:Lmaps/u/a;

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3, p4}, Lmaps/u/b;->a(Lmaps/ao/b;IZLmaps/ao/a;)Lmaps/u/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/e;->a:Lmaps/u/a;

    iput-object p1, p0, Lmaps/y/e;->c:Lmaps/ao/b;

    iput p2, p0, Lmaps/y/e;->d:I

    iput-boolean p3, p0, Lmaps/y/e;->e:Z

    :cond_0
    iget-object v0, p0, Lmaps/y/e;->a:Lmaps/u/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/ao/b;ZLmaps/ao/a;)Lmaps/y/f;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/e;->b:Lmaps/y/f;

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lmaps/u/b;->a(Lmaps/ao/b;ZLmaps/ao/a;)Lmaps/y/f;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/e;->b:Lmaps/y/f;

    :cond_0
    iget-object v0, p0, Lmaps/y/e;->b:Lmaps/y/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
