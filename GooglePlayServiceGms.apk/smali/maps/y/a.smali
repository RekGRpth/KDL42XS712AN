.class public final Lmaps/y/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/u/a;


# instance fields
.field private final a:Lmaps/u/a;

.field private final b:Lmaps/ab/e;

.field private volatile c:Z

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:Ljava/util/Set;

.field private g:Ljava/util/Set;

.field private h:Lmaps/ac/r;

.field private i:Lmaps/ac/ae;

.field private final j:Lmaps/ax/f;

.field private final k:Lmaps/ab/q;

.field private l:J


# direct methods
.method public constructor <init>(Lmaps/u/a;Lmaps/ab/e;ILmaps/ab/q;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/a;->c:Z

    iput-object v1, p0, Lmaps/y/a;->h:Lmaps/ac/r;

    iput-object v1, p0, Lmaps/y/a;->i:Lmaps/ac/ae;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/y/a;->l:J

    iput-object p1, p0, Lmaps/y/a;->a:Lmaps/u/a;

    iput-object p2, p0, Lmaps/y/a;->b:Lmaps/ab/e;

    new-instance v0, Lmaps/ax/f;

    invoke-direct {v0, p3}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/y/a;->j:Lmaps/ax/f;

    iput-object p4, p0, Lmaps/y/a;->k:Lmaps/ab/q;

    return-void
.end method

.method private e(Lmaps/ar/a;)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/y/a;->a:Lmaps/u/a;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Lmaps/u/a;->b(Lmaps/ar/a;)Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/y/a;->c:Z

    if-nez v1, :cond_0

    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/y/a;->d:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-wide v1, v0, Lmaps/y/a;->l:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    move-object/from16 v0, p0

    iput-wide v1, v0, Lmaps/y/a;->l:J

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/y/a;->c:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/y/a;->k:Lmaps/ab/q;

    invoke-virtual {v1}, Lmaps/ab/q;->c()Lmaps/ac/z;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-virtual {v7}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v1

    move-object v3, v1

    :goto_1
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/bt;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/y/a;->j:Lmaps/ax/f;

    invoke-virtual {v2, v1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/y/a;->b:Lmaps/ab/e;

    invoke-interface {v2, v1}, Lmaps/ab/e;->a(Lmaps/ac/bt;)Ljava/util/Collection;

    move-result-object v2

    sget-object v4, Lmaps/ab/e;->a:Ljava/util/Collection;

    if-eq v2, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/y/a;->j:Lmaps/ax/f;

    invoke-virtual {v4, v1, v2}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_2
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ab/a;

    invoke-virtual {v2}, Lmaps/ab/a;->a()Lmaps/ac/r;

    move-result-object v4

    invoke-interface {v10, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/y/a;->k:Lmaps/ab/q;

    invoke-virtual {v2}, Lmaps/ab/a;->a()Lmaps/ac/r;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/ab/q;->b(Lmaps/ac/r;)Lmaps/ac/ae;

    move-result-object v4

    if-eqz v4, :cond_4

    new-instance v5, Lmaps/ac/cf;

    invoke-direct {v5}, Lmaps/ac/cf;-><init>()V

    invoke-virtual {v5, v4}, Lmaps/ac/cf;->a(Lmaps/ac/bw;)V

    invoke-virtual {v1, v5}, Lmaps/ac/bt;->a(Lmaps/ac/cf;)Lmaps/ac/bt;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    monitor-enter p0

    :try_start_0
    invoke-virtual {v2}, Lmaps/ab/a;->a()Lmaps/ac/r;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/y/a;->h:Lmaps/ac/r;

    invoke-virtual {v5, v13}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v5, Lmaps/ac/cf;

    invoke-direct {v5}, Lmaps/ac/cf;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/y/a;->i:Lmaps/ac/ae;

    invoke-virtual {v5, v13}, Lmaps/ac/cf;->a(Lmaps/ac/bw;)V

    invoke-virtual {v1, v5}, Lmaps/ac/bt;->a(Lmaps/ac/cf;)Lmaps/ac/bt;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lmaps/ab/a;->a()Lmaps/ac/r;

    move-result-object v2

    invoke-virtual {v2, v3}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v4}, Lmaps/ac/ae;->c()Lmaps/ac/ad;

    move-result-object v2

    invoke-virtual {v7, v2}, Lmaps/ac/z;->b(Lmaps/ac/ad;)I

    move-result v13

    const/4 v2, -0x1

    if-eq v13, v2, :cond_3

    invoke-virtual {v7}, Lmaps/ac/z;->b()Ljava/util/List;

    move-result-object v14

    add-int/lit8 v2, v13, -0x1

    const/4 v4, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v4, v13, 0x1

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v15

    move v5, v2

    :goto_2
    if-ge v5, v15, :cond_3

    if-eq v5, v13, :cond_6

    new-instance v4, Lmaps/ac/cf;

    invoke-direct {v4}, Lmaps/ac/cf;-><init>()V

    invoke-interface {v14, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ac/aa;

    invoke-virtual {v2}, Lmaps/ac/aa;->a()Lmaps/ac/ad;

    move-result-object v2

    invoke-static {v2}, Lmaps/ac/ae;->a(Lmaps/ac/ad;)Lmaps/ac/ae;

    move-result-object v2

    invoke-virtual {v4, v2}, Lmaps/ac/cf;->a(Lmaps/ac/bw;)V

    invoke-virtual {v1, v4}, Lmaps/ac/bt;->a(Lmaps/ac/cf;)Lmaps/ac/bt;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lmaps/y/a;->a(Lmaps/ac/bt;Lmaps/ac/av;)Lmaps/ac/bt;

    move-result-object v4

    if-nez v4, :cond_9

    :goto_3
    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    move-object v3, v1

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_8
    move-object/from16 v0, p0

    iput-object v6, v0, Lmaps/y/a;->d:Ljava/util/List;

    invoke-static {v8}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/y/a;->e:Ljava/util/List;

    move-object/from16 v0, p0

    iput-object v9, v0, Lmaps/y/a;->f:Ljava/util/Set;

    move-object/from16 v0, p0

    iput-object v10, v0, Lmaps/y/a;->g:Ljava/util/Set;

    goto/16 :goto_0

    :cond_9
    move-object v2, v4

    goto :goto_3
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lmaps/y/a;->l:J

    return-wide v0
.end method

.method public final a(Lmaps/ac/av;)Ljava/util/List;
    .locals 1

    invoke-static {}, Lmaps/m/ay;->e()Lmaps/m/ay;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lmaps/ar/a;)Ljava/util/Set;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/y/a;->e(Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/y/a;->f:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lmaps/ac/bt;Lmaps/ac/av;)Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/y/a;->a:Lmaps/u/a;

    invoke-interface {v0, p1, p2}, Lmaps/u/a;->a(Lmaps/ac/bt;Lmaps/ac/av;)Lmaps/ac/bt;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lmaps/ac/r;Lmaps/ac/ad;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/a;->h:Lmaps/ac/r;

    new-instance v0, Lmaps/ac/af;

    invoke-direct {v0}, Lmaps/ac/af;-><init>()V

    invoke-virtual {v0, p2}, Lmaps/ac/af;->a(Lmaps/ac/ad;)Lmaps/ac/af;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/af;->a()Lmaps/ac/ae;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/a;->i:Lmaps/ac/ae;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/a;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lmaps/ac/av;)F
    .locals 1

    iget-object v0, p0, Lmaps/y/a;->a:Lmaps/u/a;

    invoke-interface {v0, p1}, Lmaps/u/a;->b(Lmaps/ac/av;)F

    move-result v0

    return v0
.end method

.method public final b(Lmaps/ar/a;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/y/a;->e(Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/y/a;->e:Ljava/util/List;

    return-object v0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/a;->c:Z

    return-void
.end method

.method public final c(Lmaps/ar/a;)Ljava/util/Set;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/y/a;->e(Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/y/a;->g:Ljava/util/Set;

    return-object v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lmaps/y/a;->h:Lmaps/ac/r;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/y/a;->i:Lmaps/ac/ae;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/a;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(Lmaps/ar/a;)Lmaps/ac/r;
    .locals 12

    const-wide/high16 v10, 0x4033000000000000L    # 19.0

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lmaps/y/a;->e(Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/y/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    invoke-virtual {v0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v5

    invoke-virtual {p1}, Lmaps/ar/a;->g()Lmaps/ac/av;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmaps/ac/bd;->a(Lmaps/ac/av;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    iget-object v4, p0, Lmaps/y/a;->j:Lmaps/ax/f;

    invoke-virtual {v4, v0}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lmaps/ar/a;->g()Lmaps/ac/av;

    move-result-object v5

    invoke-virtual {p1}, Lmaps/ar/a;->q()F

    move-result v4

    float-to-double v6, v4

    cmpl-double v4, v6, v10

    if-lez v4, :cond_1

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    sub-double/2addr v6, v10

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    div-double/2addr v2, v6

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v5}, Lmaps/ac/av;->e()D

    move-result-wide v6

    mul-double/2addr v2, v6

    double-to-int v2, v2

    invoke-static {v5, v2}, Lmaps/ac/bd;->a(Lmaps/ac/av;I)Lmaps/ac/bd;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/a;

    invoke-virtual {v0, v6}, Lmaps/ab/a;->a(Lmaps/ac/be;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lmaps/ab/a;->c()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {v3, v5}, Lmaps/ac/av;->d(Lmaps/ac/av;)F

    move-result v3

    if-eqz v1, :cond_2

    cmpg-float v4, v3, v2

    if-gez v4, :cond_5

    :cond_2
    invoke-virtual {v0}, Lmaps/ab/a;->a()Lmaps/ac/r;

    move-result-object v0

    move v1, v3

    :goto_2
    move v2, v1

    move-object v1, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    return-object v1

    :cond_5
    move-object v0, v1

    move v1, v2

    goto :goto_2
.end method
