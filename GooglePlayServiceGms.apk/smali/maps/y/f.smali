.class public Lmaps/y/f;
.super Lmaps/y/g;


# instance fields
.field private d:Z

.field private e:J

.field private f:Lmaps/ac/cw;

.field private g:Lmaps/ac/cw;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;


# direct methods
.method public constructor <init>(Lmaps/ao/b;Lmaps/ao/a;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Lmaps/y/g;-><init>(Lmaps/ao/b;Lmaps/ao/a;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/y/f;->e:J

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->j()D

    move-result-wide v0

    sget-object v2, Lmaps/ao/b;->j:Lmaps/ao/b;

    if-eq p1, v2, :cond_0

    sget-object v2, Lmaps/ao/b;->k:Lmaps/ao/b;

    if-eq p1, v2, :cond_0

    sget-object v2, Lmaps/ao/b;->l:Lmaps/ao/b;

    if-ne p1, v2, :cond_1

    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/y/f;->d:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lmaps/ac/cw;Ljava/util/ArrayList;)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v4, :cond_0

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    invoke-virtual {v0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v1

    invoke-virtual {p0, v1}, Lmaps/ac/cw;->b(Lmaps/ac/be;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p1, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v4, -0x1

    :goto_2
    if-lt v0, v2, :cond_1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method protected a(Lmaps/ar/a;)I
    .locals 4

    invoke-virtual {p1}, Lmaps/ar/a;->p()F

    move-result v0

    iget-object v1, p0, Lmaps/y/f;->c:Lmaps/aj/ak;

    invoke-virtual {p1}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v2

    iget-object v3, p0, Lmaps/y/f;->a:Lmaps/ao/b;

    invoke-virtual {v1, v2, v3}, Lmaps/aj/ak;->a(Lmaps/ac/av;Lmaps/ao/b;)Lmaps/aj/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lmaps/aj/aj;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    float-to-int v0, v0

    goto :goto_0
.end method

.method public final a()J
    .locals 2

    iget-wide v0, p0, Lmaps/y/f;->e:J

    return-wide v0
.end method

.method public final b(Lmaps/ac/av;)F
    .locals 2

    iget-boolean v0, p0, Lmaps/y/f;->d:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lmaps/y/g;->b(Lmaps/ac/av;)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lmaps/y/g;->b(Lmaps/ac/av;)F

    move-result v0

    goto :goto_0
.end method

.method public b(Lmaps/ar/a;)Ljava/util/List;
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v3

    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/ac/cw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/f;->f:Lmaps/ac/cw;

    invoke-virtual {v3, v0}, Lmaps/ac/cw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/f;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/f;->h:Ljava/util/List;

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/y/f;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    invoke-virtual {v0}, Lmaps/ac/bt;->j()Lmaps/ac/cf;

    move-result-object v0

    iget-object v4, p0, Lmaps/y/f;->b:Lmaps/ao/a;

    invoke-virtual {v4}, Lmaps/ao/a;->a()Lmaps/ac/cf;

    move-result-object v4

    invoke-virtual {v0, v4}, Lmaps/ac/cf;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-wide v4, p0, Lmaps/y/f;->e:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lmaps/y/f;->e:J

    invoke-virtual {v3}, Lmaps/ac/cw;->a()Lmaps/ac/cx;

    move-result-object v0

    invoke-virtual {p0, p1}, Lmaps/y/f;->a(Lmaps/ar/a;)I

    move-result v4

    iget-object v5, p0, Lmaps/y/f;->b:Lmaps/ao/a;

    invoke-virtual {v5}, Lmaps/ao/a;->a()Lmaps/ac/cf;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lmaps/ac/bt;->a(Lmaps/ac/cx;ILmaps/ac/cf;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p1}, Lmaps/ar/a;->o()F

    move-result v0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lmaps/ar/a;->n()F

    move-result v0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    if-nez v0, :cond_2

    invoke-static {v3, v4}, Lmaps/y/f;->a(Lmaps/ac/cw;Ljava/util/ArrayList;)V

    :cond_2
    iput-object v4, p0, Lmaps/y/f;->h:Ljava/util/List;

    iput-object v3, p0, Lmaps/y/f;->f:Lmaps/ac/cw;

    iget-object v0, p0, Lmaps/y/f;->h:Ljava/util/List;

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public final c(Lmaps/ar/a;)Ljava/util/List;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v1

    iget-object v0, p0, Lmaps/y/f;->g:Lmaps/ac/cw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->g:Lmaps/ac/cw;

    invoke-virtual {v1, v0}, Lmaps/ac/cw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/f;->i:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lmaps/ac/cw;->a()Lmaps/ac/cx;

    move-result-object v0

    invoke-virtual {p0, p1}, Lmaps/y/f;->a(Lmaps/ar/a;)I

    move-result v2

    invoke-static {v0, v2}, Lmaps/ac/bt;->a(Lmaps/ac/cx;I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, Lmaps/ar/a;->o()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lmaps/ar/a;->n()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    invoke-static {v1, v2}, Lmaps/y/f;->a(Lmaps/ac/cw;Ljava/util/ArrayList;)V

    :cond_1
    iput-object v1, p0, Lmaps/y/f;->g:Lmaps/ac/cw;

    iput-object v2, p0, Lmaps/y/f;->i:Ljava/util/List;

    iget-object v0, p0, Lmaps/y/f;->i:Ljava/util/List;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
