.class public abstract Lmaps/y/g;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/u/a;


# instance fields
.field protected final a:Lmaps/ao/b;

.field protected final b:Lmaps/ao/a;

.field protected final c:Lmaps/aj/ak;


# direct methods
.method private constructor <init>(Lmaps/ao/b;Lmaps/aj/ak;Lmaps/ao/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/y/g;->a:Lmaps/ao/b;

    iput-object p2, p0, Lmaps/y/g;->c:Lmaps/aj/ak;

    iput-object p3, p0, Lmaps/y/g;->b:Lmaps/ao/a;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Null zoom table"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public constructor <init>(Lmaps/ao/b;Lmaps/ao/a;)V
    .locals 1

    invoke-static {}, Lmaps/ax/m;->c()Lmaps/aj/ak;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lmaps/y/g;-><init>(Lmaps/ao/b;Lmaps/aj/ak;Lmaps/ao/a;)V

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/av;)Ljava/util/List;
    .locals 9

    const/4 v1, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1}, Lmaps/y/g;->c(Lmaps/ac/av;)Lmaps/aj/aj;

    move-result-object v5

    move v3, v1

    :goto_0
    const/4 v0, 0x2

    if-gt v3, v0, :cond_2

    invoke-virtual {v5, v3}, Lmaps/aj/aj;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    shl-int v6, v0, v3

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_1

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_0

    new-instance v7, Lmaps/ac/bt;

    iget-object v8, p0, Lmaps/y/g;->b:Lmaps/ao/a;

    invoke-virtual {v8}, Lmaps/ao/a;->a()Lmaps/ac/cf;

    move-result-object v8

    invoke-direct {v7, v3, v2, v0, v8}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    return-object v4
.end method

.method public final a(Lmaps/ac/bt;Lmaps/ac/av;)Lmaps/ac/bt;
    .locals 2

    invoke-virtual {p0, p2}, Lmaps/y/g;->c(Lmaps/ac/av;)Lmaps/aj/aj;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/aj/aj;->a(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, v0}, Lmaps/ac/bt;->a(I)Lmaps/ac/bt;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lmaps/ac/av;)F
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/y/g;->c(Lmaps/ac/av;)Lmaps/aj/aj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aj/aj;->a()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method final c(Lmaps/ac/av;)Lmaps/aj/aj;
    .locals 2

    iget-object v0, p0, Lmaps/y/g;->c:Lmaps/aj/ak;

    iget-object v1, p0, Lmaps/y/g;->a:Lmaps/ao/b;

    invoke-virtual {v0, p1, v1}, Lmaps/aj/ak;->a(Lmaps/ac/av;Lmaps/ao/b;)Lmaps/aj/aj;

    move-result-object v0

    return-object v0
.end method
