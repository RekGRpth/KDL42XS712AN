.class public Lmaps/bs/a;
.super Ljava/lang/Object;


# static fields
.field protected static b:Lmaps/bs/a;

.field protected static final d:Ljava/lang/Object;


# instance fields
.field private a:Lmaps/bt/g;

.field public c:Lmaps/bt/k;

.field protected final e:Lmaps/bs/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lmaps/bs/a;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/bs/a;->c:Lmaps/bt/k;

    iput-object v0, p0, Lmaps/bs/a;->a:Lmaps/bt/g;

    new-instance v0, Lmaps/bs/b;

    invoke-direct {v0}, Lmaps/bs/b;-><init>()V

    iput-object v0, p0, Lmaps/bs/a;->e:Lmaps/bs/b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lmaps/bs/a;->d:Ljava/lang/Object;

    monitor-enter v1

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Lmaps/bt/h;

    invoke-direct {v0}, Lmaps/bt/h;-><init>()V

    :goto_0
    iput-object v0, p0, Lmaps/bs/a;->c:Lmaps/bt/k;

    new-instance v0, Lmaps/bs/b;

    invoke-direct {v0}, Lmaps/bs/b;-><init>()V

    iput-object v0, p0, Lmaps/bs/a;->e:Lmaps/bs/b;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sput-object p0, Lmaps/bs/a;->b:Lmaps/bs/a;

    new-instance v0, Lmaps/bu/a;

    invoke-direct {v0, p1}, Lmaps/bu/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmaps/bs/a;->a:Lmaps/bt/g;

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lmaps/bu/c;

    invoke-direct {v0, p1}, Lmaps/bu/c;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static p()Lmaps/bs/a;
    .locals 1

    sget-object v0, Lmaps/bs/a;->b:Lmaps/bs/a;

    return-object v0
.end method


# virtual methods
.method public i()Lmaps/bs/b;
    .locals 1

    iget-object v0, p0, Lmaps/bs/a;->e:Lmaps/bs/b;

    return-object v0
.end method

.method public final q()Lmaps/bt/k;
    .locals 1

    iget-object v0, p0, Lmaps/bs/a;->c:Lmaps/bt/k;

    return-object v0
.end method

.method public final r()Lmaps/bt/g;
    .locals 1

    iget-object v0, p0, Lmaps/bs/a;->a:Lmaps/bt/g;

    return-object v0
.end method
