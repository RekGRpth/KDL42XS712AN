.class public abstract Lmaps/s/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/s/k;


# instance fields
.field private final a:Lmaps/s/b;

.field private final b:Lmaps/s/d;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/s/b;

    invoke-direct {v0}, Lmaps/s/b;-><init>()V

    iput-object v0, p0, Lmaps/s/a;->a:Lmaps/s/b;

    new-instance v0, Lmaps/s/d;

    invoke-direct {v0}, Lmaps/s/d;-><init>()V

    iput-object v0, p0, Lmaps/s/a;->b:Lmaps/s/d;

    return-void
.end method

.method static final a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;
    .locals 1

    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0, p0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CancellationException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->b:Lmaps/s/d;

    invoke-virtual {v0, p1, p2}, Lmaps/s/d;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method protected a(Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/b;

    invoke-virtual {v0, p1}, Lmaps/s/b;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/s/a;->b:Lmaps/s/d;

    invoke-virtual {v1}, Lmaps/s/d;->a()V

    :cond_0
    return v0
.end method

.method protected a(Ljava/lang/Throwable;)Z
    .locals 2

    iget-object v1, p0, Lmaps/s/a;->a:Lmaps/s/b;

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {v1, v0}, Lmaps/s/b;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/s/a;->b:Lmaps/s/d;

    invoke-virtual {v1}, Lmaps/s/d;->a()V

    :cond_0
    instance-of v1, p1, Ljava/lang/Error;

    if-eqz v1, :cond_1

    check-cast p1, Ljava/lang/Error;

    throw p1

    :cond_1
    return v0
.end method

.method public cancel(Z)Z
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/b;

    invoke-virtual {v0, p1}, Lmaps/s/b;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/s/a;->b:Lmaps/s/d;

    invoke-virtual {v0}, Lmaps/s/d;->a()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/b;

    invoke-virtual {v0}, Lmaps/s/b;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/b;

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/s/b;->a(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isCancelled()Z
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/b;

    invoke-virtual {v0}, Lmaps/s/b;->c()Z

    move-result v0

    return v0
.end method

.method public isDone()Z
    .locals 1

    iget-object v0, p0, Lmaps/s/a;->a:Lmaps/s/b;

    invoke-virtual {v0}, Lmaps/s/b;->b()Z

    move-result v0

    return v0
.end method
