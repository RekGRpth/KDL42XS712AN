.class public final enum Lmaps/v/g;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/v/g;

.field public static final enum b:Lmaps/v/g;

.field public static final enum c:Lmaps/v/g;

.field private static final synthetic d:[Lmaps/v/g;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/v/g;

    const-string v1, "FADE_IN"

    invoke-direct {v0, v1, v2}, Lmaps/v/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/g;->a:Lmaps/v/g;

    new-instance v0, Lmaps/v/g;

    const-string v1, "FADE_OUT"

    invoke-direct {v0, v1, v3}, Lmaps/v/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/g;->b:Lmaps/v/g;

    new-instance v0, Lmaps/v/g;

    const-string v1, "FADE_BETWEEN"

    invoke-direct {v0, v1, v4}, Lmaps/v/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/g;->c:Lmaps/v/g;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/v/g;

    sget-object v1, Lmaps/v/g;->a:Lmaps/v/g;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/v/g;->b:Lmaps/v/g;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/v/g;->c:Lmaps/v/g;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/v/g;->d:[Lmaps/v/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/v/g;
    .locals 1

    const-class v0, Lmaps/v/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/v/g;

    return-object v0
.end method

.method public static values()[Lmaps/v/g;
    .locals 1

    sget-object v0, Lmaps/v/g;->d:[Lmaps/v/g;

    invoke-virtual {v0}, [Lmaps/v/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/v/g;

    return-object v0
.end method
