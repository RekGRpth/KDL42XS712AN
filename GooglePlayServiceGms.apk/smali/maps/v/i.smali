.class public Lmaps/v/i;
.super Lmaps/v/p;


# instance fields
.field a:F

.field private b:F

.field private c:F

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .locals 1

    invoke-direct {p0}, Lmaps/v/p;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/v/i;->d:Z

    invoke-virtual {p0, p1}, Lmaps/v/i;->setInterpolator(Landroid/view/animation/Interpolator;)V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    iget v0, p0, Lmaps/v/i;->c:F

    return v0
.end method

.method public final a(F)V
    .locals 1

    iget-boolean v0, p0, Lmaps/v/i;->d:Z

    if-nez v0, :cond_0

    iput p1, p0, Lmaps/v/i;->b:F

    iput p1, p0, Lmaps/v/i;->c:F

    iput p1, p0, Lmaps/v/i;->a:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/v/i;->d:Z

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lmaps/v/i;->a:F

    iput v0, p0, Lmaps/v/i;->b:F

    iput p1, p0, Lmaps/v/i;->c:F

    goto :goto_0
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/v/i;->a:F

    return v0
.end method

.method public final c(J)V
    .locals 4

    invoke-virtual {p0, p1, p2}, Lmaps/v/i;->a(J)F

    move-result v0

    iget v1, p0, Lmaps/v/i;->b:F

    iget v2, p0, Lmaps/v/i;->c:F

    iget v3, p0, Lmaps/v/i;->b:F

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/v/i;->a:F

    return-void
.end method

.method public isInitialized()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/v/i;->d:Z

    return v0
.end method
