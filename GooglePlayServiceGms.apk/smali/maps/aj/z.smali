.class final Lmaps/aj/z;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:I

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Lmaps/aj/y;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lmaps/aj/z;-><init>()V

    return-void
.end method

.method private c()V
    .locals 3

    const/high16 v2, 0x20000

    const/4 v1, 0x1

    iget-boolean v0, p0, Lmaps/aj/z;->a:Z

    if-nez v0, :cond_1

    iput v2, p0, Lmaps/aj/z;->b:I

    iget v0, p0, Lmaps/aj/z;->b:I

    if-lt v0, v2, :cond_0

    iput-boolean v1, p0, Lmaps/aj/z;->d:Z

    :cond_0
    iput-boolean v1, p0, Lmaps/aj/z;->a:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/aj/z;->c:Z

    if-nez v2, :cond_3

    invoke-direct {p0}, Lmaps/aj/z;->c()V

    const/16 v2, 0x1f01

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    iget v2, p0, Lmaps/aj/z;->b:I

    const/high16 v4, 0x20000

    if-ge v2, v4, :cond_0

    const-string v2, "Q3Dimension MSM7500 "

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v1

    :goto_0
    iput-boolean v2, p0, Lmaps/aj/z;->d:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    :cond_0
    iget-boolean v2, p0, Lmaps/aj/z;->d:Z

    if-eqz v2, :cond_1

    const-string v2, "Adreno"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    iput-boolean v0, p0, Lmaps/aj/z;->e:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/z;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :cond_4
    move v2, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/aj/y;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/aj/y;->a(Lmaps/aj/y;)Z

    iget-object v0, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/aj/z;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/aj/z;->c()V

    iget-boolean v0, p0, Lmaps/aj/z;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lmaps/aj/y;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    if-nez v1, :cond_2

    :cond_0
    iput-object p1, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-direct {p0}, Lmaps/aj/z;->c()V

    iget-boolean v1, p0, Lmaps/aj/z;->d:Z

    if-nez v1, :cond_1

    iget-object v0, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->i()V

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lmaps/aj/y;)V
    .locals 1

    iget-object v0, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/aj/z;->f:Lmaps/aj/y;

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method
