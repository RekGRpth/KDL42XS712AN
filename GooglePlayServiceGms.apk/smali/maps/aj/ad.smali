.class public final Lmaps/aj/ad;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Lmaps/ay/u;

.field private final b:Lmaps/aj/ae;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/util/Map;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lmaps/ay/u;Lmaps/aj/ae;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 5

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/aj/ad;->d:Ljava/util/Map;

    iput-boolean v1, p0, Lmaps/aj/ad;->e:Z

    iput-boolean v1, p0, Lmaps/aj/ad;->f:Z

    iput-object p1, p0, Lmaps/aj/ad;->a:Lmaps/ay/u;

    iput-object p2, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lmaps/aj/af;

    invoke-interface {v1}, Lmaps/aj/af;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/o;

    iget-object v4, p0, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/af;

    invoke-interface {v0}, Lmaps/aj/af;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/o;

    iget-object v4, p0, Lmaps/aj/ad;->d:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-void
.end method

.method public varargs constructor <init>(Lmaps/ay/u;Lmaps/aj/ae;[Lmaps/aj/af;)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lmaps/aj/ad;->d:Ljava/util/Map;

    iput-boolean v0, p0, Lmaps/aj/ad;->e:Z

    iput-boolean v0, p0, Lmaps/aj/ad;->f:Z

    iput-object p1, p0, Lmaps/aj/ad;->a:Lmaps/ay/u;

    iput-object p2, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    array-length v2, p3

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p3, v1

    invoke-interface {v3}, Lmaps/aj/af;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/o;

    iget-object v5, p0, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/o;)Lmaps/aj/af;
    .locals 1

    iget-object v0, p0, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/af;

    return-object v0
.end method

.method public final a()Lmaps/ay/u;
    .locals 1

    iget-object v0, p0, Lmaps/aj/ad;->a:Lmaps/ay/u;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/aj/ad;->e:Z

    return-void
.end method

.method public final b()Lmaps/aj/ae;
    .locals 1

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    return-object v0
.end method

.method public final b(Lmaps/ac/o;)Lmaps/aj/af;
    .locals 1

    iget-object v0, p0, Lmaps/aj/ad;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/af;

    return-object v0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/aj/ad;->f:Z

    return-void
.end method

.method public final c()Z
    .locals 2

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->a:Lmaps/aj/ae;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->c:Lmaps/aj/ae;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->h:Lmaps/aj/ae;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->f:Lmaps/aj/ae;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->i:Lmaps/aj/ae;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 3

    check-cast p1, Lmaps/aj/ad;

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    iget-object v1, p1, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    invoke-virtual {v0, v1}, Lmaps/aj/ae;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lmaps/aj/ad;->a:Lmaps/ay/u;

    iget-object v2, p1, Lmaps/aj/ad;->a:Lmaps/ay/u;

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lmaps/ay/u;->f()Lmaps/ay/v;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ay/v;->a()I

    move-result v0

    invoke-virtual {v2}, Lmaps/ay/u;->f()Lmaps/ay/v;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ay/v;->a()I

    move-result v1

    sub-int/2addr v0, v1

    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/af;

    iget-object v1, p1, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/aj/af;

    invoke-interface {v0, v1}, Lmaps/aj/af;->compareTo(Ljava/lang/Object;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public final d()Z
    .locals 2

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->g:Lmaps/aj/ae;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->b:Lmaps/aj/ae;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    iget-object v0, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->e:Lmaps/aj/ae;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/aj/ad;->e:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/aj/ad;->f:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "overlay"

    iget-object v2, p0, Lmaps/aj/ad;->a:Lmaps/ay/u;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "order"

    iget-object v2, p0, Lmaps/aj/ad;->b:Lmaps/aj/ae;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "isFirstPassForOverlay"

    iget-boolean v2, p0, Lmaps/aj/ad;->e:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    move-result-object v0

    const-string v1, "isLastPassForOverlay"

    iget-boolean v2, p0, Lmaps/aj/ad;->f:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    move-result-object v0

    const-string v1, "overlayRenderTweaks"

    iget-object v2, p0, Lmaps/aj/ad;->c:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "featureRenderTweaks"

    iget-object v2, p0, Lmaps/aj/ad;->d:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
