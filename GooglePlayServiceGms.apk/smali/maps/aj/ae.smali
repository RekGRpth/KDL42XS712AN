.class public final enum Lmaps/aj/ae;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/aj/ae;

.field public static final enum b:Lmaps/aj/ae;

.field public static final enum c:Lmaps/aj/ae;

.field public static final enum d:Lmaps/aj/ae;

.field public static final enum e:Lmaps/aj/ae;

.field public static final enum f:Lmaps/aj/ae;

.field public static final enum g:Lmaps/aj/ae;

.field public static final enum h:Lmaps/aj/ae;

.field public static final enum i:Lmaps/aj/ae;

.field private static final synthetic j:[Lmaps/aj/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "BASE"

    invoke-direct {v0, v1, v3}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->a:Lmaps/aj/ae;

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "DROP_SHADOWS_OUTER"

    invoke-direct {v0, v1, v4}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->b:Lmaps/aj/ae;

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "ELEVATED_COLOR"

    invoke-direct {v0, v1, v5}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->c:Lmaps/aj/ae;

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "UNDERGROUND_MODE_MASK"

    invoke-direct {v0, v1, v6}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->d:Lmaps/aj/ae;

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "UNDERGROUND_STENCIL"

    invoke-direct {v0, v1, v7}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->e:Lmaps/aj/ae;

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "UNDERGROUND_COLOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->f:Lmaps/aj/ae;

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "DROP_SHADOWS_INNER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->g:Lmaps/aj/ae;

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "ANIMATED_ELEVATED_COLOR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->h:Lmaps/aj/ae;

    new-instance v0, Lmaps/aj/ae;

    const-string v1, "DEFAULT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lmaps/aj/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/ae;->i:Lmaps/aj/ae;

    const/16 v0, 0x9

    new-array v0, v0, [Lmaps/aj/ae;

    sget-object v1, Lmaps/aj/ae;->a:Lmaps/aj/ae;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/aj/ae;->b:Lmaps/aj/ae;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/aj/ae;->c:Lmaps/aj/ae;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/aj/ae;->d:Lmaps/aj/ae;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/aj/ae;->e:Lmaps/aj/ae;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/aj/ae;->f:Lmaps/aj/ae;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/aj/ae;->g:Lmaps/aj/ae;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/aj/ae;->h:Lmaps/aj/ae;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lmaps/aj/ae;->i:Lmaps/aj/ae;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/aj/ae;->j:[Lmaps/aj/ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/aj/ae;
    .locals 1

    const-class v0, Lmaps/aj/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/aj/ae;

    return-object v0
.end method

.method public static values()[Lmaps/aj/ae;
    .locals 1

    sget-object v0, Lmaps/aj/ae;->j:[Lmaps/aj/ae;

    invoke-virtual {v0}, [Lmaps/aj/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/aj/ae;

    return-object v0
.end method
