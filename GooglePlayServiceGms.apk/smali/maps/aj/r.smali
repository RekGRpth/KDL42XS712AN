.class public Lmaps/aj/r;
.super Landroid/view/TextureView;

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# static fields
.field private static final h:Lmaps/aj/z;


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;

.field public b:Lmaps/aj/y;

.field public c:Lmaps/ap/f;

.field public d:Lmaps/aj/h;

.field public e:Lmaps/aj/v;

.field public f:Lmaps/aj/w;

.field public g:Z

.field private i:Z

.field private j:Lmaps/aj/n;

.field private k:I

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmaps/aj/z;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/aj/z;-><init>(B)V

    sput-object v0, Lmaps/aj/r;->h:Lmaps/aj/z;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/aj/r;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0, p0}, Lmaps/aj/r;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method

.method static synthetic a(Lmaps/aj/r;)I
    .locals 1

    iget v0, p0, Lmaps/aj/r;->k:I

    return v0
.end method

.method static synthetic b(Lmaps/aj/r;)Lmaps/aj/h;
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->d:Lmaps/aj/h;

    return-object v0
.end method

.method static synthetic c(Lmaps/aj/r;)Lmaps/aj/v;
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->e:Lmaps/aj/v;

    return-object v0
.end method

.method static synthetic d(Lmaps/aj/r;)Lmaps/aj/w;
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->f:Lmaps/aj/w;

    return-object v0
.end method

.method static synthetic e(Lmaps/aj/r;)Lmaps/aj/n;
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->j:Lmaps/aj/n;

    return-object v0
.end method

.method static synthetic f(Lmaps/aj/r;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/aj/r;->g:Z

    return v0
.end method

.method static synthetic g()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method static synthetic g(Lmaps/aj/r;)Lmaps/ap/f;
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->c:Lmaps/ap/f;

    return-object v0
.end method

.method static synthetic h()Lmaps/aj/z;
    .locals 1

    sget-object v0, Lmaps/aj/r;->h:Lmaps/aj/z;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->b()V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iput-boolean p1, p0, Lmaps/aj/r;->l:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lmaps/aj/r;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->g()V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/r;->g:Z

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->e()V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->f()V

    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lmaps/aj/r;->i:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/aj/r;->c:Lmaps/ap/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->a()I

    move-result v0

    :goto_0
    new-instance v2, Lmaps/aj/y;

    iget-object v3, p0, Lmaps/aj/r;->a:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lmaps/aj/y;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v1, v0}, Lmaps/aj/y;->a(I)V

    :cond_1
    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/aj/r;->i:Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lmaps/aj/r;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->g()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/r;->i:Z

    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->c()V

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0, p2, p3}, Lmaps/aj/y;->a(II)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0}, Lmaps/aj/y;->d()V

    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lmaps/aj/r;->b:Lmaps/aj/y;

    invoke-virtual {v0, p2, p3}, Lmaps/aj/y;->a(II)V

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method
