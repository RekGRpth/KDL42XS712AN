.class final Lmaps/aj/k;
.super Ljava/lang/Object;


# instance fields
.field a:Ljavax/microedition/khronos/egl/EGL10;

.field b:Ljavax/microedition/khronos/egl/EGLDisplay;

.field c:Ljavax/microedition/khronos/egl/EGLSurface;

.field d:Ljavax/microedition/khronos/egl/EGLConfig;

.field private e:Ljava/lang/ref/WeakReference;

.field private f:Ljavax/microedition/khronos/egl/EGLContext;


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/aj/k;->e:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public static a(Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private f()V
    .locals 5

    iget-object v0, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    iget-object v0, p0, Lmaps/aj/k;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/d;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lmaps/aj/d;->d(Lmaps/aj/d;)Lmaps/aj/j;

    iget-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-static {v0, v1, v2}, Lmaps/aj/j;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Lmaps/bl/a;->a()V

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v0, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglGetDisplay failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglInitialize failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/aj/k;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/d;

    if-nez v0, :cond_4

    iput-object v4, p0, Lmaps/aj/k;->d:Ljavax/microedition/khronos/egl/EGLConfig;

    iput-object v4, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    :goto_0
    iget-object v0, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v0, v1, :cond_3

    :cond_2
    iput-object v4, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    const-string v0, "createContext"

    iget-object v1, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v1

    invoke-static {v0, v1}, Lmaps/aj/k;->a(Ljava/lang/String;I)V

    :cond_3
    iput-object v4, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-static {}, Lmaps/bl/a;->b()V

    return-void

    :cond_4
    invoke-static {v0}, Lmaps/aj/d;->b(Lmaps/aj/d;)Lmaps/aj/h;

    move-result-object v1

    iget-object v2, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v3}, Lmaps/aj/h;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v1

    iput-object v1, p0, Lmaps/aj/k;->d:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-static {v0}, Lmaps/aj/d;->c(Lmaps/aj/d;)Lmaps/aj/i;

    move-result-object v0

    iget-object v1, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lmaps/aj/k;->d:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v0, v1, v2, v3}, Lmaps/aj/i;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    goto :goto_0
.end method

.method public final b()Z
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "egl not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglDisplay not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/aj/k;->d:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mEglConfig not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-direct {p0}, Lmaps/aj/k;->f()V

    iget-object v0, p0, Lmaps/aj/k;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/d;

    if-eqz v0, :cond_4

    invoke-static {v0}, Lmaps/aj/d;->d(Lmaps/aj/d;)Lmaps/aj/j;

    iget-object v2, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lmaps/aj/k;->d:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-virtual {v0}, Lmaps/aj/d;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-static {v2, v3, v4, v0}, Lmaps/aj/j;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    :goto_0
    iget-object v0, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v0, v2, :cond_5

    :cond_3
    iget-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move v0, v1

    :goto_1
    return v0

    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lmaps/aj/k;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v5, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v2, v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move v0, v1

    goto :goto_1

    :cond_6
    const/4 v0, 0x1

    goto :goto_1
.end method

.method final c()Ljavax/microedition/khronos/opengles/GL;
    .locals 3

    iget-object v0, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v1

    iget-object v0, p0, Lmaps/aj/k;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/d;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lmaps/aj/d;->e(Lmaps/aj/d;)Lmaps/aj/n;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lmaps/aj/d;->e(Lmaps/aj/d;)Lmaps/aj/n;

    move-result-object v0

    invoke-interface {v0}, Lmaps/aj/n;->a()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v0

    :goto_0
    invoke-static {}, Lmaps/aj/d;->g()I

    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Lmaps/aj/k;->f()V

    return-void
.end method

.method public final e()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/aj/k;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/d;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lmaps/aj/d;->c(Lmaps/aj/d;)Lmaps/aj/i;

    move-result-object v0

    iget-object v1, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3}, Lmaps/aj/i;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)V

    :cond_0
    iput-object v4, p0, Lmaps/aj/k;->f:Ljavax/microedition/khronos/egl/EGLContext;

    :cond_1
    iget-object v0, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/aj/k;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    iput-object v4, p0, Lmaps/aj/k;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    :cond_2
    return-void
.end method
