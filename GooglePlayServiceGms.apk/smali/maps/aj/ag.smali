.class public final Lmaps/aj/ag;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lmaps/ac/br;

.field public static final b:Lmaps/aj/ah;

.field public static final c:Lmaps/aj/ah;

.field public static final d:Lmaps/aj/ah;


# instance fields
.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private final g:Landroid/graphics/Path;

.field private h:Lmaps/as/c;

.field private final i:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-object v0, Lmaps/aj/ag;->a:Lmaps/ac/br;

    new-instance v0, Lmaps/aj/ah;

    invoke-direct {v0, v1}, Lmaps/aj/ah;-><init>(B)V

    sput-object v0, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    new-instance v0, Lmaps/aj/ah;

    invoke-direct {v0, v1}, Lmaps/aj/ah;-><init>(B)V

    sput-object v0, Lmaps/aj/ag;->c:Lmaps/aj/ah;

    new-instance v0, Lmaps/aj/ah;

    invoke-direct {v0, v1}, Lmaps/aj/ah;-><init>(B)V

    sput-object v0, Lmaps/aj/ag;->d:Lmaps/aj/ah;

    return-void
.end method

.method public constructor <init>(F)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lmaps/aj/ag;->f:Landroid/graphics/Paint;

    iget-object v0, p0, Lmaps/aj/ag;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lmaps/aj/ag;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lmaps/aj/ag;->g:Landroid/graphics/Path;

    new-instance v0, Lmaps/as/c;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lmaps/as/c;-><init>(I)V

    iput-object v0, p0, Lmaps/aj/ag;->h:Lmaps/as/c;

    const v0, 0x40066666    # 2.1f

    mul-float/2addr v0, p1

    iput v0, p0, Lmaps/aj/ag;->i:F

    return-void
.end method

.method private a(Lmaps/aj/ah;Lmaps/ac/br;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lmaps/aj/ag;->d:Lmaps/aj/ah;

    if-ne p1, v2, :cond_0

    move v1, v0

    :cond_0
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lmaps/ac/br;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    invoke-virtual {p2}, Lmaps/ac/br;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    :goto_1
    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;F)F
    .locals 1

    invoke-direct {p0, p2, p3}, Lmaps/aj/ag;->a(Lmaps/aj/ah;Lmaps/ac/br;)V

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)Lmaps/as/b;
    .locals 8

    new-instance v0, Lmaps/aj/ai;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v0 .. v7}, Lmaps/aj/ai;-><init>(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)V

    iget-object v1, p0, Lmaps/aj/ag;->h:Lmaps/as/c;

    invoke-virtual {v1, v0}, Lmaps/as/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/as/b;->h()V

    :cond_0
    return-object v0
.end method

.method public final a(Lmaps/as/a;Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)Lmaps/as/b;
    .locals 17

    new-instance v3, Lmaps/aj/ai;

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-direct/range {v3 .. v10}, Lmaps/aj/ai;-><init>(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->h:Lmaps/as/c;

    invoke-virtual {v4, v3}, Lmaps/as/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmaps/as/b;

    if-nez v4, :cond_5

    if-nez p7, :cond_0

    if-eqz p8, :cond_6

    :cond_0
    const/4 v9, 0x1

    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lmaps/aj/ag;->a(Lmaps/aj/ah;Lmaps/ac/br;)V

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v4, v4, p5

    float-to-int v4, v4

    int-to-float v8, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    if-eqz v9, :cond_7

    move-object/from16 v0, p0

    iget v4, v0, Lmaps/aj/ag;->i:F

    move v10, v4

    :goto_1
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    invoke-virtual/range {v4 .. v9}, Lmaps/aj/ag;->a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FZ)[F

    move-result-object v4

    const/4 v5, 0x0

    aget v5, v4, v5

    const v6, 0x3f820c4a    # 1.016f

    mul-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v5, v5

    add-int/lit8 v13, v5, 0x1

    const/4 v5, 0x1

    aget v4, v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v14, v4

    const/16 v4, 0x8

    invoke-static {v13, v4}, Lmaps/as/b;->c(II)I

    move-result v5

    const/16 v4, 0x8

    invoke-static {v14, v4}, Lmaps/as/b;->c(II)I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->L()I

    move-result v6

    if-gt v5, v6, :cond_1

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->L()I

    move-result v6

    if-le v4, v6, :cond_b

    :cond_1
    const-string v6, "TextGenerator texture too large"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " because of string "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with size "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lmaps/br/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->L()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->L()I

    move-result v4

    move v6, v5

    move v5, v4

    :goto_2
    const/high16 v4, -0x1000000

    move/from16 v0, p6

    if-eq v0, v4, :cond_2

    const/4 v4, -0x1

    move/from16 v0, p6

    if-ne v0, v4, :cond_8

    :cond_2
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->m()Lmaps/al/k;

    move-result-object v7

    invoke-virtual {v7, v6, v5, v4}, Lmaps/al/k;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v15

    move/from16 v0, p8

    invoke-virtual {v15, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v16, Landroid/graphics/Canvas;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Canvas;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v4, v4

    add-float/2addr v4, v10

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v5, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->f:Landroid/graphics/Paint;

    move/from16 v0, p7

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->f:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v6, v10

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    move/from16 v0, p6

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    if-eqz p7, :cond_9

    const/4 v4, 0x0

    cmpl-float v4, v10, v4

    if-lez v4, :cond_9

    const/4 v4, 0x1

    move v12, v4

    :goto_4
    if-eqz p6, :cond_a

    const/4 v4, 0x1

    move v11, v4

    :goto_5
    float-to-double v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-float v4, v6

    float-to-int v4, v4

    int-to-float v8, v4

    int-to-float v9, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/aj/ag;->g:Landroid/graphics/Path;

    move-object/from16 v5, p2

    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Paint;->getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->g:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/aj/ag;->f:Landroid/graphics/Paint;

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_3
    if-eqz v11, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/aj/ag;->g:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_4
    new-instance v4, Lmaps/as/b;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v5}, Lmaps/as/b;-><init>(Lmaps/as/a;Z)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lmaps/as/b;->c(Z)V

    invoke-virtual {v4, v15, v13, v14}, Lmaps/as/b;->a(Landroid/graphics/Bitmap;II)V

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->recycle()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/aj/ag;->h:Lmaps/as/c;

    invoke-virtual {v5, v3, v4}, Lmaps/as/c;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_5
    invoke-virtual {v4}, Lmaps/as/b;->h()V

    return-object v4

    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v4, 0x0

    move v10, v4

    goto/16 :goto_1

    :cond_8
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto/16 :goto_3

    :cond_9
    const/4 v4, 0x0

    move v12, v4

    goto :goto_4

    :cond_a
    const/4 v4, 0x0

    move v11, v4

    goto :goto_5

    :cond_b
    move v6, v5

    move v5, v4

    goto/16 :goto_2
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lmaps/aj/ag;->h:Lmaps/as/c;

    invoke-virtual {v0}, Lmaps/as/c;->d()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lmaps/aj/ag;->h:Lmaps/as/c;

    invoke-virtual {v0}, Lmaps/as/c;->e()I

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lmaps/aj/ag;->h:Lmaps/as/c;

    invoke-virtual {v0}, Lmaps/as/c;->d()V

    new-instance v0, Lmaps/as/c;

    invoke-direct {v0, p1}, Lmaps/as/c;-><init>(I)V

    iput-object v0, p0, Lmaps/aj/ag;->h:Lmaps/as/c;

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FZ)[F
    .locals 7

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lmaps/aj/ag;->a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FZF)[F

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FZF)[F
    .locals 7

    const/high16 v6, 0x40000000    # 2.0f

    invoke-direct {p0, p2, p3}, Lmaps/aj/ag;->a(Lmaps/aj/ah;Lmaps/ac/br;)V

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    iget v0, v1, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v3, v1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v0, v3

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-float v3, v3

    iget v0, v1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    iget v4, v1, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v0, v4

    iget v4, v1, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float v1, v4, v1

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v4, p6, v4

    mul-float/2addr v4, v3

    if-eqz p5, :cond_0

    const/4 v5, 0x0

    cmpl-float v5, v2, v5

    if-lez v5, :cond_0

    iget v5, p0, Lmaps/aj/ag;->i:F

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    iget v5, p0, Lmaps/aj/ag;->i:F

    add-float/2addr v0, v5

    iget v5, p0, Lmaps/aj/ag;->i:F

    add-float/2addr v1, v5

    :cond_0
    add-float v5, v0, v1

    add-float/2addr v3, v5

    div-float v5, v4, v6

    sub-float/2addr v0, v5

    div-float/2addr v4, v6

    sub-float/2addr v1, v4

    const/4 v4, 0x4

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v2, v4, v5

    const/4 v2, 0x1

    aput v3, v4, v2

    const/4 v2, 0x2

    aput v0, v4, v2

    const/4 v0, 0x3

    aput v1, v4, v0

    return-object v4
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lmaps/aj/ag;->h:Lmaps/as/c;

    iget-object v1, p0, Lmaps/aj/ag;->h:Lmaps/as/c;

    invoke-virtual {v1}, Lmaps/as/c;->e()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    const/16 v2, 0x8

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/as/c;->a(I)V

    return-void
.end method

.method public final b(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;F)[F
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v4, v0, [F

    array-length v0, v4

    if-nez v0, :cond_0

    move-object v0, v4

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p2, p3}, Lmaps/aj/ag;->a(Lmaps/aj/ah;Lmaps/ac/br;)V

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lmaps/aj/ag;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    iget v0, p0, Lmaps/aj/ag;->i:F

    move v2, v0

    move v0, v1

    :goto_1
    array-length v3, v4

    if-ge v0, v3, :cond_1

    aget v3, v4, v0

    add-float/2addr v3, v2

    aput v2, v4, v0

    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_1

    :cond_1
    aget v0, v4, v1

    iget v2, p0, Lmaps/aj/ag;->i:F

    sub-float/2addr v0, v2

    aput v0, v4, v1

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget v1, v4, v0

    iget v2, p0, Lmaps/aj/ag;->i:F

    add-float/2addr v1, v2

    aput v1, v4, v0

    move-object v0, v4

    goto :goto_0
.end method
