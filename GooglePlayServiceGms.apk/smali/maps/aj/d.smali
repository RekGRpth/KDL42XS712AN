.class public Lmaps/aj/d;
.super Landroid/view/SurfaceView;

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final h:Lmaps/aj/m;


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;

.field public b:Lmaps/aj/l;

.field public c:Lmaps/aj/p;

.field public d:Lmaps/aj/h;

.field public e:Lmaps/aj/i;

.field public f:Lmaps/aj/j;

.field public g:Z

.field private i:Z

.field private j:Lmaps/aj/n;

.field private k:I

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmaps/aj/m;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/aj/m;-><init>(B)V

    sput-object v0, Lmaps/aj/d;->h:Lmaps/aj/m;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/aj/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Lmaps/aj/d;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-ge v1, v2, :cond_0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lmaps/aj/d;)I
    .locals 1

    iget v0, p0, Lmaps/aj/d;->k:I

    return v0
.end method

.method static synthetic b(Lmaps/aj/d;)Lmaps/aj/h;
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->d:Lmaps/aj/h;

    return-object v0
.end method

.method static synthetic c(Lmaps/aj/d;)Lmaps/aj/i;
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->e:Lmaps/aj/i;

    return-object v0
.end method

.method static synthetic d(Lmaps/aj/d;)Lmaps/aj/j;
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->f:Lmaps/aj/j;

    return-object v0
.end method

.method static synthetic e(Lmaps/aj/d;)Lmaps/aj/n;
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->j:Lmaps/aj/n;

    return-object v0
.end method

.method static synthetic f(Lmaps/aj/d;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/aj/d;->g:Z

    return v0
.end method

.method static synthetic g()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method static synthetic g(Lmaps/aj/d;)Lmaps/aj/p;
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->c:Lmaps/aj/p;

    return-object v0
.end method

.method static synthetic h()Lmaps/aj/m;
    .locals 1

    sget-object v0, Lmaps/aj/d;->h:Lmaps/aj/m;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->b()V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iput-boolean p1, p0, Lmaps/aj/d;->l:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lmaps/aj/d;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->g()V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/d;->g:Z

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->e()V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->f()V

    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lmaps/aj/d;->i:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/aj/d;->c:Lmaps/aj/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->a()I

    move-result v0

    :goto_0
    new-instance v2, Lmaps/aj/l;

    iget-object v3, p0, Lmaps/aj/d;->a:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lmaps/aj/l;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v1, v0}, Lmaps/aj/l;->a(I)V

    :cond_1
    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/aj/d;->i:Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lmaps/aj/d;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->g()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/d;->i:Z

    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0, p3, p4}, Lmaps/aj/l;->a(II)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->c()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->d()V

    return-void
.end method
