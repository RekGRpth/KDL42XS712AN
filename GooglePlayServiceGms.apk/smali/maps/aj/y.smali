.class public final Lmaps/aj/y;
.super Ljava/lang/Thread;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Ljava/util/ArrayList;

.field private q:Z

.field private r:Lmaps/aj/x;

.field private s:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/aj/y;->p:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lmaps/aj/y;->q:Z

    iput v2, p0, Lmaps/aj/y;->k:I

    iput v2, p0, Lmaps/aj/y;->l:I

    iput-boolean v1, p0, Lmaps/aj/y;->n:Z

    iput v1, p0, Lmaps/aj/y;->m:I

    iput-object p1, p0, Lmaps/aj/y;->s:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic a(Lmaps/aj/y;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/y;->b:Z

    return v0
.end method

.method private j()V
    .locals 1

    iget-boolean v0, p0, Lmaps/aj/y;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/aj/y;->i:Z

    iget-object v0, p0, Lmaps/aj/y;->r:Lmaps/aj/x;

    invoke-virtual {v0}, Lmaps/aj/x;->d()V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 1

    iget-boolean v0, p0, Lmaps/aj/y;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/y;->r:Lmaps/aj/x;

    invoke-virtual {v0}, Lmaps/aj/x;->e()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/aj/y;->h:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/aj/z;->c(Lmaps/aj/y;)V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 18

    new-instance v1, Lmaps/aj/x;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/aj/y;->s:Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v2}, Lmaps/aj/x;-><init>(Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/aj/y;->r:Lmaps/aj/x;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->h:Z

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->i:Z

    const/4 v3, 0x0

    const/4 v12, 0x0

    const/4 v1, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    move-object v14, v3

    move v3, v5

    move v5, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v1

    move/from16 v17, v2

    move-object v2, v4

    move v4, v6

    move/from16 v6, v17

    :goto_0
    :try_start_0
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v15

    monitor-enter v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->a:Z

    if-eqz v1, :cond_0

    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v2

    monitor-enter v2

    :try_start_2
    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->j()V

    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->k()V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    :cond_0
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->p:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    move v2, v6

    move v6, v4

    move-object v4, v1

    move v1, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v5

    move v5, v3

    :goto_2
    monitor-exit v15
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v4, :cond_11

    :try_start_4
    invoke-interface {v4}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    const/4 v4, 0x0

    move v3, v5

    move v5, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v1

    move/from16 v17, v2

    move-object v2, v4

    move v4, v6

    move/from16 v6, v17

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lmaps/aj/y;->d:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lmaps/aj/y;->c:Z

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v13, v0, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->c:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lmaps/aj/y;->c:Z

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lmaps/aj/y;->d:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->notifyAll()V

    move v13, v1

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->j:Z

    if-eqz v1, :cond_2

    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->j()V

    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->k()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->j:Z

    const/4 v5, 0x1

    :cond_2
    if-eqz v9, :cond_3

    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->j()V

    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->k()V

    const/4 v9, 0x0

    :cond_3
    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->i:Z

    if-eqz v1, :cond_4

    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->j()V

    :cond_4
    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->h:Z

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/aj/r;

    if-nez v1, :cond_d

    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_5

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/aj/z;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->k()V

    :cond_6
    if-eqz v13, :cond_7

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/aj/z;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->r:Lmaps/aj/x;

    invoke-virtual {v1}, Lmaps/aj/x;->e()V

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->e:Z

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->g:Z

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->i:Z

    if-eqz v1, :cond_8

    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->j()V

    :cond_8
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->g:Z

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->f:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->e:Z

    if-eqz v1, :cond_a

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->g:Z

    if-eqz v1, :cond_a

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->g:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :cond_a
    if-eqz v6, :cond_b

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->o:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :cond_b
    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->m()Z

    move-result v1

    if-eqz v1, :cond_10

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->h:Z

    if-nez v1, :cond_c

    if-eqz v5, :cond_e

    const/4 v5, 0x0

    :cond_c
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->h:Z

    if-eqz v1, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/aj/y;->i:Z

    if-nez v1, :cond_1c

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->i:Z

    const/4 v11, 0x1

    const/4 v10, 0x1

    const/4 v8, 0x1

    move v1, v8

    move v8, v10

    :goto_6
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lmaps/aj/y;->i:Z

    if-eqz v10, :cond_f

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lmaps/aj/y;->q:Z

    if-eqz v10, :cond_1b

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lmaps/aj/y;->k:I

    move-object/from16 v0, p0

    iget v1, v0, Lmaps/aj/y;->l:I

    const/4 v4, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lmaps/aj/y;->q:Z

    :goto_7
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lmaps/aj/y;->n:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->notifyAll()V

    move v11, v8

    move v8, v4

    move-object v4, v2

    move v2, v6

    move v6, v3

    move/from16 v17, v1

    move v1, v10

    move v10, v9

    move v9, v7

    move v7, v5

    move/from16 v5, v17

    goto/16 :goto_2

    :cond_d
    invoke-static {v1}, Lmaps/aj/r;->f(Lmaps/aj/r;)Z

    move-result v1

    goto/16 :goto_4

    :cond_e
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lmaps/aj/z;->b(Lmaps/aj/y;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v1

    if-eqz v1, :cond_c

    :try_start_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->r:Lmaps/aj/x;

    invoke-virtual {v1}, Lmaps/aj/x;->a()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    const/4 v1, 0x1

    :try_start_7
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->h:Z

    const/4 v12, 0x1

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_5

    :catchall_1
    move-exception v1

    :try_start_8
    monitor-exit v15

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catchall_2
    move-exception v1

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v2

    monitor-enter v2

    :try_start_9
    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->j()V

    invoke-direct/range {p0 .. p0}, Lmaps/aj/y;->k()V

    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    throw v1

    :catch_0
    move-exception v1

    :try_start_a
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lmaps/aj/z;->c(Lmaps/aj/y;)V

    throw v1

    :cond_f
    move v10, v8

    move v8, v1

    :cond_10
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    :cond_11
    if-eqz v1, :cond_1a

    :try_start_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aj/y;->r:Lmaps/aj/x;

    invoke-virtual {v3}, Lmaps/aj/x;->b()Z

    move-result v3

    if-nez v3, :cond_12

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v3

    monitor-enter v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    const/4 v13, 0x1

    :try_start_c
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lmaps/aj/y;->f:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    move v3, v5

    move v5, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v1

    move/from16 v17, v2

    move-object v2, v4

    move v4, v6

    move/from16 v6, v17

    goto/16 :goto_0

    :catchall_3
    move-exception v1

    :try_start_d
    monitor-exit v3

    throw v1

    :cond_12
    const/4 v1, 0x0

    move v3, v1

    :goto_8
    if-eqz v11, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->r:Lmaps/aj/x;

    invoke-virtual {v1}, Lmaps/aj/x;->c()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v1

    check-cast v1, Ljavax/microedition/khronos/opengles/GL10;

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v11

    invoke-virtual {v11, v1}, Lmaps/aj/z;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v11, 0x0

    move-object v13, v1

    :goto_9
    if-eqz v12, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/aj/r;

    if-eqz v1, :cond_13

    invoke-static {v1}, Lmaps/aj/r;->g(Lmaps/aj/r;)Lmaps/ap/f;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/aj/y;->r:Lmaps/aj/x;

    iget-object v12, v12, Lmaps/aj/x;->d:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-virtual {v1, v13}, Lmaps/ap/f;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_13
    const/4 v12, 0x0

    :cond_14
    if-eqz v9, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/aj/r;

    if-eqz v1, :cond_15

    invoke-static {v1}, Lmaps/aj/r;->g(Lmaps/aj/r;)Lmaps/ap/f;

    move-result-object v1

    invoke-virtual {v1, v13, v6, v5}, Lmaps/ap/f;->a(Ljavax/microedition/khronos/opengles/GL10;II)V

    :cond_15
    const/4 v9, 0x0

    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/aj/r;

    if-eqz v1, :cond_17

    invoke-static {v1}, Lmaps/aj/r;->g(Lmaps/aj/r;)Lmaps/ap/f;

    move-result-object v1

    invoke-virtual {v1, v13}, Lmaps/ap/f;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_17
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/aj/y;->r:Lmaps/aj/x;

    iget-object v14, v1, Lmaps/aj/x;->a:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v15, v1, Lmaps/aj/x;->b:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v0, v1, Lmaps/aj/x;->c:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v16, v0

    invoke-interface/range {v14 .. v16}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v14

    if-nez v14, :cond_18

    iget-object v1, v1, Lmaps/aj/x;->a:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v1

    :goto_a
    sparse-switch v1, :sswitch_data_0

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v14

    monitor-enter v14
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    const/4 v1, 0x1

    :try_start_e
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/aj/y;->f:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v14
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :goto_b
    :sswitch_0
    if-eqz v8, :cond_1e

    const/4 v1, 0x1

    :goto_c
    move-object v2, v4

    move-object v14, v13

    move v4, v6

    move v6, v1

    move/from16 v17, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v3

    move v3, v5

    move/from16 v5, v17

    goto/16 :goto_0

    :cond_18
    const/16 v1, 0x3000

    goto :goto_a

    :sswitch_1
    const/4 v10, 0x1

    goto :goto_b

    :catchall_4
    move-exception v1

    :try_start_f
    monitor-exit v14

    throw v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    :catchall_5
    move-exception v1

    monitor-exit v2

    throw v1

    :cond_19
    move-object v13, v14

    goto/16 :goto_9

    :cond_1a
    move v3, v1

    goto/16 :goto_8

    :cond_1b
    move v10, v11

    move/from16 v17, v4

    move v4, v7

    move v7, v1

    move v1, v3

    move/from16 v3, v17

    goto/16 :goto_7

    :cond_1c
    move v1, v8

    move v8, v10

    goto/16 :goto_6

    :cond_1d
    move v13, v1

    goto/16 :goto_3

    :cond_1e
    move v1, v2

    goto :goto_c

    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x300e -> :sswitch_1
    .end sparse-switch
.end method

.method private m()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lmaps/aj/y;->d:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lmaps/aj/y;->e:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lmaps/aj/y;->f:Z

    if-nez v1, :cond_1

    iget v1, p0, Lmaps/aj/y;->k:I

    if-lez v1, :cond_1

    iget v1, p0, Lmaps/aj/y;->l:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lmaps/aj/y;->n:Z

    if-nez v1, :cond_0

    iget v1, p0, Lmaps/aj/y;->m:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lmaps/aj/y;->m:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(I)V
    .locals 2

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lmaps/aj/y;->m:I

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(II)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iput p1, p0, Lmaps/aj/y;->k:I

    iput p2, p0, Lmaps/aj/y;->l:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/y;->q:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/y;->n:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/aj/y;->o:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/aj/y;->b:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lmaps/aj/y;->d:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lmaps/aj/y;->o:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lmaps/aj/y;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/aj/y;->i:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/aj/y;->m()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    :try_start_1
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final b()V
    .locals 2

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/aj/y;->n:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 2

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/aj/y;->e:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/aj/y;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/aj/y;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final d()V
    .locals 2

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/aj/y;->e:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/aj/y;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/aj/y;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final e()V
    .locals 2

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/aj/y;->c:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/aj/y;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/aj/y;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final f()V
    .locals 2

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/aj/y;->c:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/y;->n:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/aj/y;->o:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/aj/y;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/aj/y;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/aj/y;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final g()V
    .locals 2

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/aj/y;->a:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lmaps/aj/y;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final h()Z
    .locals 2

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/aj/y;->b:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/y;->j:Z

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method

.method public final run()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GLThread "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lmaps/aj/y;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/aj/y;->setName(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lmaps/aj/y;->l()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/aj/z;->a(Lmaps/aj/y;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/aj/z;->a(Lmaps/aj/y;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_1
    const-string v1, "MAP"

    invoke-static {v1, v0}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v1

    invoke-virtual {v1, p0}, Lmaps/aj/z;->a(Lmaps/aj/y;)V

    throw v0

    :catch_2
    move-exception v0

    :try_start_2
    const-string v1, "MAP"

    invoke-static {v1, v0}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {}, Lmaps/aj/r;->h()Lmaps/aj/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/aj/z;->a(Lmaps/aj/y;)V

    goto :goto_0
.end method
