.class public final Lmaps/ai/c;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bn/l;


# static fields
.field private static a:Lmaps/ai/c;


# instance fields
.field private final b:Lmaps/bn/d;

.field private final c:Lmaps/bs/b;

.field private final d:Lmaps/ax/f;

.field private final e:Lmaps/ax/f;

.field private volatile f:Lmaps/ag/e;

.field private final g:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/ai/c;->b:Lmaps/bn/d;

    iput-object v0, p0, Lmaps/ai/c;->c:Lmaps/bs/b;

    iput-object v0, p0, Lmaps/ai/c;->d:Lmaps/ax/f;

    iput-object v0, p0, Lmaps/ai/c;->e:Lmaps/ax/f;

    iput-object v0, p0, Lmaps/ai/c;->f:Lmaps/ag/e;

    iput-object v0, p0, Lmaps/ai/c;->g:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private constructor <init>(Lmaps/bn/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ai/c;->b:Lmaps/bn/d;

    iget-object v0, p0, Lmaps/ai/c;->b:Lmaps/bn/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ai/c;->b:Lmaps/bn/d;

    invoke-virtual {v0, p0}, Lmaps/bn/d;->a(Lmaps/bn/l;)V

    :cond_0
    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->i()Lmaps/bs/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/ai/c;->c:Lmaps/bs/b;

    new-instance v0, Lmaps/ax/f;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ai/c;->d:Lmaps/ax/f;

    new-instance v0, Lmaps/ax/f;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ai/c;->e:Lmaps/ax/f;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ai/c;->f:Lmaps/ag/e;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lmaps/ai/c;->g:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method static synthetic a(Lmaps/ai/c;)Lmaps/ag/e;
    .locals 1

    iget-object v0, p0, Lmaps/ai/c;->f:Lmaps/ag/e;

    return-object v0
.end method

.method public static a(Lmaps/bn/d;Ljava/io/File;)Lmaps/ai/c;
    .locals 2

    sget-object v0, Lmaps/ai/c;->a:Lmaps/ai/c;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ai/c;

    invoke-direct {v0, p0}, Lmaps/ai/c;-><init>(Lmaps/bn/d;)V

    sput-object v0, Lmaps/ai/c;->a:Lmaps/ai/c;

    :cond_0
    new-instance v0, Lmaps/ai/d;

    invoke-static {}, Lmaps/ba/i;->a()Lmaps/by/c;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lmaps/ai/d;-><init>(Lmaps/by/c;Ljava/io/File;)V

    invoke-virtual {v0}, Lmaps/ai/d;->e()V

    sget-object v0, Lmaps/ai/c;->a:Lmaps/ai/c;

    return-object v0
.end method

.method static synthetic a(Lmaps/ai/c;Ljava/io/File;)V
    .locals 1

    invoke-static {p1}, Lmaps/ag/e;->a(Ljava/io/File;)Lmaps/ag/e;

    move-result-object v0

    iput-object v0, p0, Lmaps/ai/c;->f:Lmaps/ag/e;

    iget-object v0, p0, Lmaps/ai/c;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public static c()Lmaps/ai/c;
    .locals 1

    sget-object v0, Lmaps/ai/c;->a:Lmaps/ai/c;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lmaps/ai/b;)Lmaps/ai/a;
    .locals 8

    iget-object v2, p0, Lmaps/ai/c;->d:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ai/c;->d:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ai/a;

    if-nez v0, :cond_0

    iget-object v1, p0, Lmaps/ai/c;->f:Lmaps/ag/e;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/ai/c;->f:Lmaps/ag/e;

    invoke-virtual {v0, p1}, Lmaps/ag/e;->a(Ljava/lang/String;)Lmaps/ai/a;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_4

    new-instance v0, Lmaps/ai/a;

    invoke-direct {v0}, Lmaps/ai/a;-><init>()V

    invoke-virtual {v0}, Lmaps/ai/a;->a()V

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/ai/c;->d:Lmaps/ax/f;

    invoke-virtual {v0, p1, v1}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ai/c;->c:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v2

    invoke-virtual {v1}, Lmaps/ai/a;->g()J

    move-result-wide v4

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    cmp-long v0, v4, v2

    if-gez v0, :cond_2

    new-instance v0, Lmaps/bv/a;

    sget-object v4, Lmaps/cm/u;->a:Lmaps/bv/c;

    invoke-direct {v0, v4}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v4, 0x4

    invoke-virtual {v0, v4, p1}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    invoke-virtual {v1}, Lmaps/ai/a;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {v1}, Lmaps/ai/a;->f()J

    move-result-wide v5

    invoke-virtual {v0, v4, v5, v6}, Lmaps/bv/a;->a(IJ)Lmaps/bv/a;

    :cond_1
    new-instance v4, Lmaps/ai/e;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v0, v1, v5}, Lmaps/ai/e;-><init>(Lmaps/ai/c;Lmaps/bv/a;Lmaps/ai/a;B)V

    new-instance v0, Lmaps/z/b;

    const-string v5, "addRequest"

    invoke-direct {v0, v5, v4}, Lmaps/z/b;-><init>(Ljava/lang/String;Lmaps/bn/c;)V

    invoke-static {}, Lmaps/z/a;->a()V

    iget-object v0, p0, Lmaps/ai/c;->b:Lmaps/bn/d;

    invoke-virtual {v0, v4}, Lmaps/bn/d;->c(Lmaps/bn/c;)V

    invoke-virtual {v1, v2, v3}, Lmaps/ai/a;->a(J)V

    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p2, :cond_3

    invoke-virtual {v1}, Lmaps/ai/a;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1, p2}, Lmaps/ai/a;->a(Lmaps/ai/b;)V

    :cond_3
    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/bn/c;)V
    .locals 0

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v1, p0, Lmaps/ai/c;->e:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ai/c;->e:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ai/c;->d:Lmaps/ax/f;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ai/c;->d:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p1, :cond_1

    :goto_0
    iget-object v0, p0, Lmaps/ai/c;->f:Lmaps/ag/e;

    if-nez v0, :cond_0

    :try_start_2
    iget-object v0, p0, Lmaps/ai/c;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ai/c;->f:Lmaps/ag/e;

    invoke-virtual {v0}, Lmaps/ag/e;->a()V

    :cond_1
    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/bn/c;)V
    .locals 1

    instance-of v0, p1, Lmaps/ai/e;

    if-eqz v0, :cond_0

    check-cast p1, Lmaps/ai/e;

    iget-object v0, p1, Lmaps/ai/e;->a:Lmaps/ai/a;

    invoke-virtual {v0}, Lmaps/ai/a;->h()V

    :cond_0
    return-void
.end method
