.class public final Lmaps/ah/d;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmaps/au/ap;


# instance fields
.field private b:Lmaps/aq/a;

.field private final c:Lmaps/ae/y;

.field private volatile d:Lmaps/aq/e;

.field private final e:Lmaps/ax/a;

.field private final f:Ljava/util/List;

.field private g:Lmaps/ao/a;

.field private final h:Lmaps/ax/f;

.field private i:I

.field private j:I

.field private final k:Lmaps/ae/z;

.field private l:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final m:Ljava/util/Set;

.field private final n:Ljava/util/LinkedList;

.field private o:Ljava/util/Map;

.field private p:Ljava/util/Map;

.field private volatile q:I

.field private final r:Lmaps/ah/a;

.field private s:Lmaps/ah/c;

.field private final t:Lmaps/af/d;

.field private final u:Lmaps/af/d;

.field private final v:Lmaps/ah/h;

.field private w:Lmaps/bs/b;

.field private volatile x:Lmaps/as/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/au/av;

    invoke-direct {v0}, Lmaps/au/av;-><init>()V

    sput-object v0, Lmaps/ah/d;->a:Lmaps/au/ap;

    return-void
.end method

.method public constructor <init>(Lmaps/ao/b;Lmaps/ah/a;)V
    .locals 2

    new-instance v0, Lmaps/aq/e;

    invoke-direct {v0, p1}, Lmaps/aq/e;-><init>(Lmaps/ao/b;)V

    sget-object v1, Lmaps/ax/a;->a:Lmaps/ax/a;

    invoke-direct {p0, p1, v0, p2, v1}, Lmaps/ah/d;-><init>(Lmaps/ao/b;Lmaps/aq/e;Lmaps/ah/a;Lmaps/ax/a;)V

    return-void
.end method

.method public constructor <init>(Lmaps/ao/b;Lmaps/aq/e;Lmaps/ah/a;Lmaps/ax/a;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/ax/f;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ah/d;->h:Lmaps/ax/f;

    iput v2, p0, Lmaps/ah/d;->i:I

    iput v2, p0, Lmaps/ah/d;->j:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lmaps/ah/d;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lmaps/ah/d;->m:Ljava/util/Set;

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/ah/d;->o:Ljava/util/Map;

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ah/d;->p:Ljava/util/Map;

    iput v2, p0, Lmaps/ah/d;->q:I

    new-instance v0, Lmaps/ah/f;

    invoke-direct {v0, p0, v2}, Lmaps/ah/f;-><init>(Lmaps/ah/d;B)V

    iput-object v0, p0, Lmaps/ah/d;->t:Lmaps/af/d;

    new-instance v0, Lmaps/ah/g;

    invoke-direct {v0, p0, v2}, Lmaps/ah/g;-><init>(Lmaps/ah/d;B)V

    iput-object v0, p0, Lmaps/ah/d;->u:Lmaps/af/d;

    new-instance v0, Lmaps/ah/h;

    invoke-direct {v0, p0, v2}, Lmaps/ah/h;-><init>(Lmaps/ah/d;B)V

    iput-object v0, p0, Lmaps/ah/d;->v:Lmaps/ah/h;

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->i()Lmaps/bs/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/ah/d;->w:Lmaps/bs/b;

    iput-object p2, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    iput-object v3, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iput-object p4, p0, Lmaps/ah/d;->e:Lmaps/ax/a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ah/d;->f:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    invoke-static {p1}, Lmaps/ae/ab;->a(Lmaps/ao/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/ae/ab;->b(Lmaps/ao/b;)Lmaps/ae/y;

    move-result-object v0

    iput-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    new-instance v0, Lmaps/ah/e;

    invoke-direct {v0, p0}, Lmaps/ah/e;-><init>(Lmaps/ah/d;)V

    iput-object v0, p0, Lmaps/ah/d;->k:Lmaps/ae/z;

    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    iget-object v1, p0, Lmaps/ah/d;->k:Lmaps/ae/z;

    invoke-interface {v0, v1}, Lmaps/ae/y;->a(Lmaps/ae/z;)V

    :goto_0
    iput-object p3, p0, Lmaps/ah/d;->r:Lmaps/ah/a;

    return-void

    :cond_0
    iput-object v3, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    iput-object v3, p0, Lmaps/ah/d;->k:Lmaps/ae/z;

    goto :goto_0
.end method

.method static synthetic a(Lmaps/ah/d;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->m:Ljava/util/Set;

    return-object v0
.end method

.method private a(Lmaps/ac/bt;ILmaps/ac/bs;)Lmaps/au/ap;
    .locals 7

    const/4 v4, 0x0

    iget-object v2, p0, Lmaps/ah/d;->h:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lmaps/ah/d;->h:Lmaps/ax/f;

    invoke-virtual {v1, p1, p1}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ah/d;->g:Lmaps/ao/a;

    invoke-virtual {p1}, Lmaps/ac/bt;->j()Lmaps/ac/cf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ao/a;->a(Lmaps/ac/cf;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, v4

    :cond_0
    :goto_0
    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    :cond_1
    iget-object v1, p0, Lmaps/ah/d;->f:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/ah/d;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lmaps/ac/bt;->j()Lmaps/ac/cf;

    move-result-object v1

    if-eqz v1, :cond_4

    instance-of v1, p3, Lmaps/ac/cs;

    if-eqz v1, :cond_4

    iget-object v5, p0, Lmaps/ah/d;->f:Ljava/util/List;

    monitor-enter v5

    :try_start_1
    iget-object v1, p0, Lmaps/ah/d;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v2, p3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ae/ac;

    const/4 v3, 0x1

    invoke-virtual {v1, p1, v3}, Lmaps/ae/ac;->a(Lmaps/ac/bt;Z)Lmaps/ac/bs;

    move-result-object v3

    if-eqz v3, :cond_2

    move-object v0, v2

    check-cast v0, Lmaps/ac/cs;

    move-object v1, v0

    move-object v0, v3

    check-cast v0, Lmaps/ac/cs;

    move-object v2, v0

    invoke-static {v1, v2}, Lmaps/ac/as;->a(Lmaps/ac/cs;Lmaps/ac/cs;)Lmaps/ac/cs;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-static {}, Lmaps/ae/ac;->d()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v1

    monitor-exit v5

    throw v1

    :cond_3
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object p3, v2

    :cond_4
    iget-object v2, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    if-eqz v2, :cond_8

    if-nez p2, :cond_8

    instance-of v1, p3, Lmaps/ac/cs;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/ah/d;->e:Lmaps/ax/a;

    invoke-static {p3, v1, v2}, Lmaps/au/at;->a(Lmaps/ac/bs;Lmaps/ax/a;Lmaps/as/a;)Lmaps/au/at;

    move-result-object v1

    :goto_2
    if-nez v1, :cond_5

    const/4 v1, 0x2

    if-ne p2, v1, :cond_7

    sget-object v1, Lmaps/ah/d;->a:Lmaps/au/ap;

    :cond_5
    iget-object v3, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v4, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {v3, v2, v4, p1, v1}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Lmaps/ac/bt;Lmaps/au/ap;)V

    goto :goto_0

    :cond_6
    instance-of v1, p3, Lmaps/ac/y;

    if-eqz v1, :cond_8

    invoke-static {p3, v2}, Lmaps/au/k;->a(Lmaps/ac/bs;Lmaps/as/a;)Lmaps/au/k;

    move-result-object v1

    goto :goto_2

    :cond_7
    move-object v1, v4

    goto :goto_0

    :cond_8
    move-object v1, v4

    goto :goto_2
.end method

.method static synthetic a(Lmaps/ah/d;Lmaps/ac/bt;)Lmaps/au/ap;
    .locals 4

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v2, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, p1, v3}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Lmaps/ac/bt;Z)Lmaps/au/ap;

    move-result-object v0

    sget-object v1, Lmaps/ah/d;->a:Lmaps/au/ap;

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ah/d;->w:Lmaps/bs/b;

    invoke-interface {v0}, Lmaps/au/ap;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method static synthetic a(Lmaps/ah/d;Lmaps/ac/bt;ILmaps/ac/bs;)Lmaps/au/ap;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lmaps/ah/d;->a(Lmaps/ac/bt;ILmaps/ac/bs;)Lmaps/au/ap;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/ac/bt;ZLmaps/af/d;)V
    .locals 4

    iget-object v1, p0, Lmaps/ah/d;->h:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ah/d;->h:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->d(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_2

    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    invoke-interface {v0, p1, p3}, Lmaps/ae/y;->c(Lmaps/ac/bt;Lmaps/af/d;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/ah/d;->f:Ljava/util/List;

    if-eqz v0, :cond_4

    sget-object v0, Lmaps/ae/ae;->f:Lmaps/ac/bt;

    if-eq p1, v0, :cond_4

    iget-object v1, p0, Lmaps/ah/d;->f:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ah/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    const/4 v2, 0x1

    invoke-interface {v0, p1, v2}, Lmaps/ae/y;->a(Lmaps/ac/bt;Z)Lmaps/ac/bs;

    :cond_1
    iget-object v0, p0, Lmaps/ah/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ae/ac;

    iget-object v3, p0, Lmaps/ah/d;->v:Lmaps/ah/h;

    invoke-virtual {v0, p1, v3}, Lmaps/ae/ac;->b(Lmaps/ac/bt;Lmaps/af/d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    invoke-interface {v0, p1, p3}, Lmaps/ae/y;->a(Lmaps/ac/bt;Lmaps/af/d;)V

    goto :goto_0

    :cond_3
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    return-void
.end method

.method static synthetic a(Lmaps/ah/d;Lmaps/ac/bt;Lmaps/au/ap;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    sget-object v1, Lmaps/ah/d;->a:Lmaps/au/ap;

    if-eq p2, v1, :cond_3

    :goto_0
    iget-object v1, p0, Lmaps/ah/d;->o:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lmaps/ah/d;->q:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/ah/d;->r:Lmaps/ah/a;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/ah/d;->r:Lmaps/ah/a;

    invoke-virtual {v1}, Lmaps/ah/a;->c()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_0
    move v1, v2

    :goto_1
    iget-object v3, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-ne v4, v2, :cond_5

    iget-object v0, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ah/i;

    invoke-interface {v0, p1, p2, v1}, Lmaps/ah/i;->a(Lmaps/ac/bt;Lmaps/au/ap;Z)V

    :cond_1
    monitor-exit v3

    :cond_2
    return-void

    :cond_3
    const/4 p2, 0x0

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_1

    :cond_5
    new-instance v4, Ljava/util/ArrayList;

    iget-object v2, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v2, v0

    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ah/i;

    invoke-interface {v0, p1, p2, v1}, Lmaps/ah/i;->a(Lmaps/ac/bt;Lmaps/au/ap;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method static synthetic a(Lmaps/ah/d;Lmaps/ac/bt;ZLmaps/af/d;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmaps/ah/d;->a(Lmaps/ac/bt;ZLmaps/af/d;)V

    return-void
.end method

.method private a(Lmaps/ac/bt;Lmaps/au/ap;)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lmaps/ah/d;->w:Lmaps/bs/b;

    invoke-interface {p2}, Lmaps/au/ap;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lmaps/ah/d;->o:Ljava/util/Map;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lmaps/ah/d;->t:Lmaps/af/d;

    invoke-direct {p0, p1, v0, v1}, Lmaps/ah/d;->a(Lmaps/ac/bt;ZLmaps/af/d;)V

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method static synthetic b(Lmaps/ah/d;)Lmaps/aq/e;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    return-object v0
.end method

.method private b(Lmaps/ac/bt;ILmaps/ac/bs;)Lmaps/au/ap;
    .locals 6

    const/4 v1, 0x0

    if-nez p3, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p3

    check-cast v0, Lmaps/ac/cs;

    invoke-virtual {v0}, Lmaps/ac/cs;->p()I

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p3

    check-cast v0, Lmaps/ac/cs;

    invoke-virtual {v0}, Lmaps/ac/cs;->n()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v2, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v3, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, p1, v4}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Lmaps/ac/bt;Z)Lmaps/au/ap;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v2, Lmaps/ah/d;->a:Lmaps/au/ap;

    if-eq v0, v2, :cond_2

    check-cast v0, Lmaps/au/at;

    check-cast p3, Lmaps/ac/cs;

    invoke-virtual {p3}, Lmaps/ac/cs;->n()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lmaps/au/at;->a(J)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lmaps/ah/d;->h:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ah/d;->h:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    iget v0, p0, Lmaps/ah/d;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ah/d;->j:I

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_3
    iget v0, p0, Lmaps/ah/d;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ah/d;->i:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    const/4 v2, 0x1

    invoke-interface {v0, p1, v2}, Lmaps/ae/y;->a(Lmaps/ac/bt;Z)Lmaps/ac/bs;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p1, p2, v0}, Lmaps/ah/d;->a(Lmaps/ac/bt;ILmaps/ac/bs;)Lmaps/au/ap;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lmaps/ah/d;Lmaps/ac/bt;ILmaps/ac/bs;)Lmaps/au/ap;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lmaps/ah/d;->b(Lmaps/ac/bt;ILmaps/ac/bs;)Lmaps/au/ap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lmaps/ah/d;Lmaps/ac/bt;Lmaps/au/ap;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/ah/d;->a(Lmaps/ac/bt;Lmaps/au/ap;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lmaps/ah/d;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->o:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lmaps/ah/d;)Lmaps/bs/b;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->w:Lmaps/bs/b;

    return-object v0
.end method

.method static synthetic e(Lmaps/ah/d;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->p:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lmaps/ah/d;)Lmaps/ah/a;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->r:Lmaps/ah/a;

    return-object v0
.end method

.method static synthetic g(Lmaps/ah/d;)I
    .locals 2

    iget v0, p0, Lmaps/ah/d;->q:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lmaps/ah/d;->q:I

    return v0
.end method

.method static synthetic h(Lmaps/ah/d;)I
    .locals 2

    iget v0, p0, Lmaps/ah/d;->q:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lmaps/ah/d;->q:I

    return v0
.end method

.method static synthetic i(Lmaps/ah/d;)Lmaps/af/d;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->u:Lmaps/af/d;

    return-object v0
.end method

.method static synthetic j(Lmaps/ah/d;)Lmaps/ah/c;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->s:Lmaps/ah/c;

    return-object v0
.end method

.method static synthetic j()Lmaps/au/ap;
    .locals 1

    sget-object v0, Lmaps/ah/d;->a:Lmaps/au/ap;

    return-object v0
.end method

.method static synthetic k(Lmaps/ah/d;)Lmaps/ah/c;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ah/d;->s:Lmaps/ah/c;

    return-object v0
.end method


# virtual methods
.method public final a()Lmaps/ao/b;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    invoke-interface {v0}, Lmaps/ae/y;->k()Lmaps/ao/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lmaps/ac/bt;)Lmaps/au/ap;
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v2, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v3, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {v1, v2, v3, p1, v5}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Lmaps/ac/bt;Z)Lmaps/au/ap;

    move-result-object v1

    sget-object v2, Lmaps/ah/d;->a:Lmaps/au/ap;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz v1, :cond_2

    iget-object v2, p0, Lmaps/ah/d;->w:Lmaps/bs/b;

    invoke-interface {v1}, Lmaps/au/ap;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_2
    iget-object v1, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v2, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v3, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {p1}, Lmaps/ac/bt;->a()Lmaps/ac/bt;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Lmaps/ac/bt;Z)Lmaps/au/ap;

    move-result-object v1

    sget-object v2, Lmaps/ah/d;->a:Lmaps/au/ap;

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_3

    iget-object v2, p0, Lmaps/ah/d;->w:Lmaps/bs/b;

    invoke-interface {v1}, Lmaps/au/ap;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v1, p0, Lmaps/ah/d;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lmaps/m/ck;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    sget-object v3, Lmaps/ah/d;->a:Lmaps/au/ap;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v2, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v3, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {v0, v2, v3, v1}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Ljava/util/List;)V

    return-void
.end method

.method public final a(Lmaps/ah/i;)V
    .locals 2

    iget-object v1, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/ah/d;->n:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ao/a;)V
    .locals 0

    iput-object p1, p0, Lmaps/ah/d;->g:Lmaps/ao/a;

    return-void
.end method

.method public final a(Lmaps/ao/b;)V
    .locals 4

    iget-object v0, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    instance-of v0, v0, Lmaps/ae/ae;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Modifiers not supported on store \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    invoke-interface {v2}, Lmaps/ae/y;->k()Lmaps/ao/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p1, Lmaps/ao/b;->A:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Only modifiers may be added, not "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Lmaps/ae/ab;->a(Lmaps/ao/b;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lmaps/ae/ab;->b(Lmaps/ao/b;)Lmaps/ae/y;

    move-result-object v0

    instance-of v1, v0, Lmaps/ae/ac;

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Modifier store \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lmaps/ae/y;->k()Lmaps/ao/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' must be a vector modifier store"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lmaps/ah/d;->f:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/ah/d;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    monitor-exit v1

    :goto_0
    return-void

    :cond_3
    iget-object v2, p0, Lmaps/ah/d;->k:Lmaps/ae/z;

    invoke-interface {v0, v2}, Lmaps/ae/y;->a(Lmaps/ae/z;)V

    iget-object v2, p0, Lmaps/ah/d;->f:Ljava/util/List;

    check-cast v0, Lmaps/ae/ac;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    iget-object v2, p0, Lmaps/ah/d;->f:Ljava/util/List;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lmaps/ah/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ae/ac;

    invoke-virtual {v0}, Lmaps/ae/ac;->k()Lmaps/ao/b;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown tile store "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v2, p0, Lmaps/ah/d;->m:Ljava/util/Set;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lmaps/ah/d;->m:Ljava/util/Set;

    iget-object v3, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    new-instance v0, Lmaps/aq/e;

    iget-object v2, p0, Lmaps/ah/d;->c:Lmaps/ae/y;

    invoke-interface {v2}, Lmaps/ae/y;->k()Lmaps/ao/b;

    move-result-object v2

    iget-object v3, p0, Lmaps/ah/d;->e:Lmaps/ax/a;

    invoke-direct {v0, v2, v1, v3}, Lmaps/aq/e;-><init>(Lmaps/ao/b;Ljava/util/Set;Lmaps/ax/a;)V

    iput-object v0, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    const-string v0, "GLState should not be null"

    invoke-static {p1, v0}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    new-instance v0, Lmaps/bs/b;

    invoke-direct {v0}, Lmaps/bs/b;-><init>()V

    invoke-static {v0}, Lmaps/aq/a;->a(Lmaps/bs/b;)V

    invoke-static {}, Lmaps/aq/a;->a()Lmaps/aq/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    return-void
.end method

.method public final a(Lmaps/u/a;Lmaps/ac/av;Ljava/util/List;Ljava/util/Set;Z)V
    .locals 7

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v2, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {v0, v1, v2}, Lmaps/aq/a;->d(Lmaps/as/a;Lmaps/aq/e;)V

    iget-object v6, p0, Lmaps/ah/d;->r:Lmaps/ah/a;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lmaps/ah/d;->r:Lmaps/ah/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lmaps/ah/a;->a(Lmaps/u/a;Lmaps/ac/av;Ljava/util/List;Ljava/util/Set;Z)V

    iget-object v0, p0, Lmaps/ah/d;->r:Lmaps/ah/a;

    invoke-virtual {v0}, Lmaps/ah/a;->b()Lmaps/ah/c;

    move-result-object v0

    iget-object v1, p0, Lmaps/ah/d;->s:Lmaps/ah/c;

    if-nez v1, :cond_0

    iget-object v1, v0, Lmaps/ah/c;->a:Lmaps/ac/bt;

    iget-boolean v2, v0, Lmaps/ah/c;->b:Z

    iget-object v3, p0, Lmaps/ah/d;->u:Lmaps/af/d;

    invoke-direct {p0, v1, v2, v3}, Lmaps/ah/d;->a(Lmaps/ac/bt;ZLmaps/af/d;)V

    :cond_0
    iput-object v0, p0, Lmaps/ah/d;->s:Lmaps/ah/c;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    invoke-virtual {v0, p1}, Lmaps/aq/a;->a(Z)V

    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/util/List;)V
    .locals 4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lmaps/m/ck;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    sget-object v3, Lmaps/ah/d;->a:Lmaps/au/ap;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v2, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v3, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {v0, v2, v3, v1}, Lmaps/aq/a;->b(Lmaps/as/a;Lmaps/aq/e;Ljava/util/List;)V

    return-void
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v2, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {v0, v1, v2}, Lmaps/aq/a;->d(Lmaps/as/a;Lmaps/aq/e;)V

    return-void
.end method

.method public final d()V
    .locals 3

    sget-boolean v0, Lmaps/bb/b;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v2, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {v0, v1, v2}, Lmaps/aq/a;->c(Lmaps/as/a;Lmaps/aq/e;)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->r:Lmaps/ah/a;

    invoke-virtual {v0}, Lmaps/ah/a;->a()V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    invoke-virtual {v0}, Lmaps/aq/a;->b()V

    :cond_0
    return-void
.end method

.method public final g()Z
    .locals 5

    iget-object v0, p0, Lmaps/ah/d;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v1, p0, Lmaps/ah/d;->m:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ah/d;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aq/e;

    iget-object v3, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v4, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    invoke-virtual {v3, v4, v0}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/ah/d;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final h()V
    .locals 3

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ah/d;->b:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/ah/d;->x:Lmaps/as/a;

    iget-object v2, p0, Lmaps/ah/d;->d:Lmaps/aq/e;

    invoke-virtual {v0, v1, v2}, Lmaps/aq/a;->b(Lmaps/as/a;Lmaps/aq/e;)V

    invoke-virtual {p0}, Lmaps/ah/d;->g()Z

    :cond_0
    return-void
.end method

.method public final i()Lmaps/bs/b;
    .locals 1

    iget-object v0, p0, Lmaps/ah/d;->w:Lmaps/bs/b;

    return-object v0
.end method
