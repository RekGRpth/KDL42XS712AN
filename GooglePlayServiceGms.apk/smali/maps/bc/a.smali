.class public Lmaps/bc/a;
.super Lmaps/bn/c;


# instance fields
.field private final a:Ljava/util/concurrent/Semaphore;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private volatile e:Lmaps/bc/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/bc/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lmaps/bn/c;-><init>()V

    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lmaps/bc/a;->a:Ljava/util/concurrent/Semaphore;

    iput-object p1, p0, Lmaps/bc/a;->b:Ljava/lang/String;

    iput-object p2, p0, Lmaps/bc/a;->c:Ljava/lang/String;

    iput-object p3, p0, Lmaps/bc/a;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lmaps/bn/k;)Lmaps/bc/b;
    .locals 5

    const/4 v0, 0x0

    invoke-interface {p1, p0}, Lmaps/bn/k;->c(Lmaps/bn/c;)V

    :try_start_0
    iget-object v1, p0, Lmaps/bc/a;->a:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x7530

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    monitor-enter p0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lmaps/bc/a;->e:Lmaps/bc/b;

    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/b;->a:Lmaps/bv/c;

    invoke-direct {v0, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/bc/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    const/4 v1, 0x2

    iget-object v2, p0, Lmaps/bc/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    const/4 v1, 0x3

    iget-object v2, p0, Lmaps/bc/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lmaps/bv/a;->a(Ljava/io/OutputStream;)V

    return-void
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v6, 0x1

    sget-object v0, Lmaps/cm/b;->b:Lmaps/bv/c;

    invoke-static {v0, p1}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/DataInput;)Lmaps/bv/a;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v6, v1}, Lmaps/bv/e;->a(Lmaps/bv/a;II)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lmaps/bc/b;->a()Lmaps/bc/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/bc/a;->e:Lmaps/bc/b;

    :goto_0
    return v6

    :pswitch_0
    invoke-static {v0}, Lmaps/bv/e;->a(Lmaps/bv/a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lmaps/bv/e;->b(Lmaps/bv/a;)J

    move-result-wide v2

    invoke-static {v0}, Lmaps/bv/e;->c(Lmaps/bv/a;)J

    move-result-wide v4

    invoke-static {v1, v2, v3, v4, v5}, Lmaps/bc/b;->a(Ljava/lang/String;JJ)Lmaps/bc/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/bc/a;->e:Lmaps/bc/b;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final af_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x84

    return v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lmaps/bc/a;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void
.end method
