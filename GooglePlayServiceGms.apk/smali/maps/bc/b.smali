.class public final Lmaps/bc/b;
.super Ljava/lang/Object;


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:J


# direct methods
.method private constructor <init>(ZLjava/lang/String;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lmaps/bc/b;->a:Z

    iput-object p2, p0, Lmaps/bc/b;->b:Ljava/lang/String;

    iput-wide p3, p0, Lmaps/bc/b;->c:J

    iput-wide p5, p0, Lmaps/bc/b;->d:J

    return-void
.end method

.method static a()Lmaps/bc/b;
    .locals 7

    const-wide/16 v3, 0x0

    new-instance v0, Lmaps/bc/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lmaps/bc/b;-><init>(ZLjava/lang/String;JJ)V

    return-object v0
.end method

.method static a(Ljava/lang/String;JJ)Lmaps/bc/b;
    .locals 7

    new-instance v0, Lmaps/bc/b;

    const/4 v1, 0x1

    move-object v2, p0

    move-wide v3, p1

    move-wide v5, p3

    invoke-direct/range {v0 .. v6}, Lmaps/bc/b;-><init>(ZLjava/lang/String;JJ)V

    return-object v0
.end method


# virtual methods
.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/bc/b;->a:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/bc/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .locals 2

    iget-wide v0, p0, Lmaps/bc/b;->c:J

    return-wide v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lmaps/bc/b;->d:J

    return-wide v0
.end method
