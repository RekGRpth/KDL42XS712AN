.class public final Lmaps/bp/c;
.super Ljava/lang/Object;


# static fields
.field private static c:I

.field private static d:I

.field private static final e:[Lmaps/bp/c;


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    const/16 v5, 0x16

    sput v0, Lmaps/bp/c;->c:I

    sput v5, Lmaps/bp/c;->d:I

    new-array v1, v5, [Lmaps/bp/c;

    sput-object v1, Lmaps/bp/c;->e:[Lmaps/bp/c;

    const/16 v1, 0x100

    :goto_0
    if-gt v0, v5, :cond_0

    sget-object v2, Lmaps/bp/c;->e:[Lmaps/bp/c;

    add-int/lit8 v3, v0, -0x1

    new-instance v4, Lmaps/bp/c;

    invoke-direct {v4, v0, v1}, Lmaps/bp/c;-><init>(II)V

    aput-object v4, v2, v3

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/bp/c;->b:I

    iput p2, p0, Lmaps/bp/c;->a:I

    return-void
.end method

.method public static a(I)Lmaps/bp/c;
    .locals 2

    const/4 v0, 0x0

    sget v1, Lmaps/bp/c;->c:I

    if-lt p0, v1, :cond_1

    const/16 v1, 0x16

    if-gt p0, v1, :cond_1

    sget v0, Lmaps/bp/c;->d:I

    if-le p0, v0, :cond_0

    sget p0, Lmaps/bp/c;->d:I

    :cond_0
    sget-object v0, Lmaps/bp/c;->e:[Lmaps/bp/c;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lmaps/bp/c;->b:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lmaps/bp/c;->a:I

    return v0
.end method

.method public final b(I)I
    .locals 2

    iget v0, p0, Lmaps/bp/c;->b:I

    const/16 v1, 0x16

    if-ge v0, v1, :cond_0

    iget v0, p0, Lmaps/bp/c;->b:I

    rsub-int/lit8 v0, v0, 0x16

    shl-int v0, p1, v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/bp/c;->b:I

    add-int/lit8 v0, v0, -0x16

    shr-int v0, p1, v0

    goto :goto_0
.end method

.method public final c()Lmaps/bp/c;
    .locals 1

    iget v0, p0, Lmaps/bp/c;->b:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lmaps/bp/c;->a(I)Lmaps/bp/c;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
