.class public final Lmaps/bp/b;
.super Ljava/lang/Object;


# static fields
.field private static final a:I

.field private static b:[B


# instance fields
.field private c:Lmaps/bv/a;

.field private d:Lmaps/bv/a;

.field private e:Lmaps/bv/a;

.field private f:Lmaps/bv/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x100

    invoke-static {v0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x1f

    sput v0, Lmaps/bp/b;->a:I

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/bp/b;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x4ct
        0x54t
        0x49t
        0x50t
        0xat
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(II)I
    .locals 2

    rsub-int/lit8 v0, p1, 0x1e

    sget v1, Lmaps/bp/b;->a:I

    sub-int/2addr v0, v1

    shl-int v0, p0, v0

    return v0
.end method

.method private e()Lmaps/bv/a;
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lmaps/bp/b;->d:Lmaps/bv/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    invoke-virtual {v0, v1}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    invoke-virtual {v0, v1}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/bp/b;->d:Lmaps/bv/a;

    :cond_0
    iget-object v0, p0, Lmaps/bp/b;->d:Lmaps/bv/a;

    return-object v0
.end method


# virtual methods
.method public final a([B)[B
    .locals 7

    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-object v3, Lmaps/bp/b;->b:[B

    array-length v0, p1

    array-length v4, v3

    add-int/lit8 v4, v4, 0x0

    if-ge v0, v4, :cond_2

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    :try_start_0
    sget-object v0, Lmaps/bp/b;->b:[B

    array-length v0, v0

    const/4 v1, 0x4

    new-array v1, v1, [B

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v2, v1

    if-eq v2, v6, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "byte[] must be size 4, there are 4 bytes to an int"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    :goto_1
    const-string v1, "IOException reading map tile info"

    invoke-static {v1, v0}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_2
    return-object p1

    :cond_2
    move v0, v1

    :goto_3
    array-length v4, v3

    if-ge v0, v4, :cond_3

    add-int/lit8 v4, v0, 0x0

    aget-byte v4, p1, v4

    aget-byte v5, v3, v0

    if-ne v4, v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :try_start_1
    aget-byte v2, v1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    const/4 v3, 0x1

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    const/4 v3, 0x2

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    const/4 v3, 0x3

    aget-byte v1, v1, v3

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/lit8 v0, v0, 0x4

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1, v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    if-gez v2, :cond_5

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    :goto_4
    new-instance v1, Lmaps/bv/a;

    sget-object v2, Lmaps/cm/y;->a:Lmaps/bv/c;

    invoke-direct {v1, v2}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    iput-object v1, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    iget-object v1, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    invoke-virtual {v1, v0}, Lmaps/bv/a;->a(Ljava/io/InputStream;)Lmaps/bv/a;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/bp/b;->d:Lmaps/bv/a;

    sget-object v0, Lmaps/bp/b;->b:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    add-int v1, v0, v3

    array-length v0, p1

    sub-int/2addr v0, v1

    new-array v0, v0, [B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v2, 0x0

    :try_start_2
    array-length v3, v0

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-object p1, v0

    goto :goto_2

    :catch_1
    move-exception v1

    move-object p1, v0

    move-object v0, v1

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_4
.end method

.method public final a()[Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/bp/b;->e()Lmaps/bv/a;

    move-result-object v2

    if-nez v2, :cond_1

    new-array v0, v1, [Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {v2, v5}, Lmaps/bv/a;->j(I)I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v5, v1}, Lmaps/bv/a;->d(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final b()[Ljava/lang/String;
    .locals 6

    const/4 v5, 0x2

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/bp/b;->e()Lmaps/bv/a;

    move-result-object v2

    if-nez v2, :cond_1

    new-array v0, v1, [Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {v2, v5}, Lmaps/bv/a;->j(I)I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v5, v1}, Lmaps/bv/a;->d(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x3

    const/4 v0, -0x1

    invoke-direct {p0}, Lmaps/bp/b;->e()Lmaps/bv/a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Lmaps/bv/a;->i(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v3}, Lmaps/bv/a;->d(I)I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "year=0"

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lmaps/br/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 15

    iget-object v0, p0, Lmaps/bp/b;->e:Lmaps/bv/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/bp/b;->e:Lmaps/bv/a;

    :cond_0
    iget-object v4, p0, Lmaps/bp/b;->e:Lmaps/bv/a;

    if-nez v4, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lmaps/bp/b;->f:Lmaps/bv/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/bp/b;->f:Lmaps/bv/a;

    :cond_2
    iget-object v0, p0, Lmaps/bp/b;->f:Lmaps/bv/a;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmaps/bv/e;->a(Lmaps/bv/a;I)I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lmaps/bv/a;->j(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    iget-object v2, p0, Lmaps/bp/b;->c:Lmaps/bv/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lmaps/bv/a;->j(I)I

    move-result v7

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v7, :cond_9

    const/4 v0, 0x1

    invoke-virtual {v4, v0, v2}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v8

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v10

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bv/a;

    const/4 v11, 0x2

    const/4 v12, 0x0

    invoke-virtual {v0, v11, v12}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v0, v12}, Lmaps/bv/a;->d(I)I

    move-result v12

    if-nez v12, :cond_4

    const/4 v12, 0x3

    invoke-virtual {v0, v12}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    const/16 v12, 0x1f

    invoke-virtual {v0, v12}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v12

    invoke-static {v12}, Lmaps/ax/c;->a(Lmaps/bv/a;)Lmaps/ac/av;

    move-result-object v12

    const/16 v13, 0x20

    invoke-static {v0, v13}, Lmaps/bv/e;->a(Lmaps/bv/a;I)I

    move-result v13

    invoke-static {v13, v5}, Lmaps/bp/b;->a(II)I

    move-result v13

    const/16 v14, 0x21

    invoke-static {v0, v14}, Lmaps/bv/e;->a(Lmaps/bv/a;I)I

    move-result v0

    invoke-static {v0, v5}, Lmaps/bp/b;->a(II)I

    move-result v0

    new-instance v14, Lmaps/ac/av;

    div-int/lit8 v13, v13, 0x2

    div-int/lit8 v0, v0, 0x2

    invoke-direct {v14, v13, v0}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v12, v14}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v12, v14}, Lmaps/ac/av;->f(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v12

    invoke-static {v0, v12}, Lmaps/ac/bd;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/bd;

    :cond_4
    new-instance v0, Lmaps/ac/am;

    invoke-direct {v0, v11}, Lmaps/ac/am;-><init>(Ljava/lang/String;)V

    invoke-interface {v10, v11, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_5
    if-eqz v8, :cond_7

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lmaps/bv/a;->j(I)I

    move-result v11

    const/4 v0, 0x0

    move v3, v0

    :goto_4
    if-ge v3, v11, :cond_7

    const/4 v0, 0x1

    invoke-virtual {v8, v0, v3}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v0

    const/4 v12, 0x1

    invoke-virtual {v0, v12}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v10, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/am;

    if-nez v0, :cond_6

    new-instance v0, Lmaps/ac/am;

    invoke-direct {v0, v12}, Lmaps/ac/am;-><init>(Ljava/lang/String;)V

    :cond_6
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_7
    if-eqz v8, :cond_8

    const/4 v0, 0x2

    invoke-virtual {v8, v0}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    :cond_8
    new-instance v0, Lmaps/ac/an;

    invoke-direct {v0}, Lmaps/ac/an;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method
