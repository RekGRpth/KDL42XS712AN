.class abstract Lmaps/l/af;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:I

.field private b:I

.field private c:Lmaps/l/an;

.field private d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field private e:Lmaps/l/am;

.field private f:Lmaps/l/bn;

.field private g:Lmaps/l/bn;

.field private synthetic h:Lmaps/l/m;


# direct methods
.method constructor <init>(Lmaps/l/m;)V
    .locals 1

    iput-object p1, p0, Lmaps/l/af;->h:Lmaps/l/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lmaps/l/m;->c:[Lmaps/l/an;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/l/af;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lmaps/l/af;->b:I

    invoke-direct {p0}, Lmaps/l/af;->b()V

    return-void
.end method

.method private a(Lmaps/l/am;)Z
    .locals 4

    :try_start_0
    iget-object v0, p0, Lmaps/l/af;->h:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v0

    invoke-interface {p1}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lmaps/l/af;->h:Lmaps/l/m;

    invoke-virtual {v3, p1, v0, v1}, Lmaps/l/m;->a(Lmaps/l/am;J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lmaps/l/bn;

    iget-object v3, p0, Lmaps/l/af;->h:Lmaps/l/m;

    invoke-direct {v1, v2, v0}, Lmaps/l/bn;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lmaps/l/af;->f:Lmaps/l/bn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/l/af;->c:Lmaps/l/an;

    invoke-virtual {v0}, Lmaps/l/an;->b()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/af;->c:Lmaps/l/an;

    invoke-virtual {v0}, Lmaps/l/an;->b()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/l/af;->c:Lmaps/l/an;

    invoke-virtual {v1}, Lmaps/l/an;->b()V

    throw v0
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/af;->f:Lmaps/l/bn;

    invoke-direct {p0}, Lmaps/l/af;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lmaps/l/af;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget v0, p0, Lmaps/l/af;->a:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lmaps/l/af;->h:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->c:[Lmaps/l/an;

    iget v1, p0, Lmaps/l/af;->a:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lmaps/l/af;->a:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lmaps/l/af;->c:Lmaps/l/an;

    iget-object v0, p0, Lmaps/l/af;->c:Lmaps/l/an;

    iget v0, v0, Lmaps/l/an;->a:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/af;->c:Lmaps/l/an;

    iget-object v0, v0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lmaps/l/af;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget-object v0, p0, Lmaps/l/af;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/l/af;->b:I

    invoke-direct {p0}, Lmaps/l/af;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    iget-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    :goto_0
    iget-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    invoke-direct {p0, v0}, Lmaps/l/af;->a(Lmaps/l/am;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()Z
    .locals 3

    :cond_0
    iget v0, p0, Lmaps/l/af;->b:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lmaps/l/af;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lmaps/l/af;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lmaps/l/af;->b:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    iput-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/af;->e:Lmaps/l/am;

    invoke-direct {p0, v0}, Lmaps/l/af;->a(Lmaps/l/am;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lmaps/l/af;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()Lmaps/l/bn;
    .locals 1

    iget-object v0, p0, Lmaps/l/af;->f:Lmaps/l/bn;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/l/af;->f:Lmaps/l/bn;

    iput-object v0, p0, Lmaps/l/af;->g:Lmaps/l/bn;

    invoke-direct {p0}, Lmaps/l/af;->b()V

    iget-object v0, p0, Lmaps/l/af;->g:Lmaps/l/bn;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lmaps/l/af;->f:Lmaps/l/bn;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/l/af;->g:Lmaps/l/bn;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    iget-object v0, p0, Lmaps/l/af;->h:Lmaps/l/m;

    iget-object v1, p0, Lmaps/l/af;->g:Lmaps/l/bn;

    invoke-virtual {v1}, Lmaps/l/bn;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/l/m;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/af;->g:Lmaps/l/bn;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
