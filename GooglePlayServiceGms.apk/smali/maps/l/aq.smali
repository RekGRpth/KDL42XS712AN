.class abstract enum Lmaps/l/aq;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/l/aq;

.field public static final enum b:Lmaps/l/aq;

.field private static enum c:Lmaps/l/aq;

.field private static final synthetic d:[Lmaps/l/aq;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/l/ar;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lmaps/l/ar;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/aq;->a:Lmaps/l/aq;

    new-instance v0, Lmaps/l/as;

    const-string v1, "SOFT"

    invoke-direct {v0, v1}, Lmaps/l/as;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/aq;->c:Lmaps/l/aq;

    new-instance v0, Lmaps/l/at;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lmaps/l/at;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/aq;->b:Lmaps/l/aq;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/l/aq;

    const/4 v1, 0x0

    sget-object v2, Lmaps/l/aq;->a:Lmaps/l/aq;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lmaps/l/aq;->c:Lmaps/l/aq;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lmaps/l/aq;->b:Lmaps/l/aq;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/l/aq;->d:[Lmaps/l/aq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/l/aq;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/l/aq;
    .locals 1

    const-class v0, Lmaps/l/aq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/l/aq;

    return-object v0
.end method

.method public static values()[Lmaps/l/aq;
    .locals 1

    sget-object v0, Lmaps/l/aq;->d:[Lmaps/l/aq;

    invoke-virtual {v0}, [Lmaps/l/aq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/l/aq;

    return-object v0
.end method


# virtual methods
.method abstract a()Lmaps/k/b;
.end method

.method abstract a(Lmaps/l/an;Lmaps/l/am;Ljava/lang/Object;)Lmaps/l/ba;
.end method
