.class final enum Lmaps/l/al;
.super Ljava/lang/Enum;

# interfaces
.implements Lmaps/l/am;


# static fields
.field public static final enum a:Lmaps/l/al;

.field private static final synthetic b:[Lmaps/l/al;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/l/al;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lmaps/l/al;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/al;->a:Lmaps/l/al;

    const/4 v0, 0x1

    new-array v0, v0, [Lmaps/l/al;

    const/4 v1, 0x0

    sget-object v2, Lmaps/l/al;->a:Lmaps/l/al;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/l/al;->b:[Lmaps/l/al;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/l/al;
    .locals 1

    const-class v0, Lmaps/l/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    return-object v0
.end method

.method public static values()[Lmaps/l/al;
    .locals 1

    sget-object v0, Lmaps/l/al;->b:[Lmaps/l/al;

    invoke-virtual {v0}, [Lmaps/l/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/l/al;

    return-object v0
.end method


# virtual methods
.method public final a()Lmaps/l/ba;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/l/am;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/l/ba;)V
    .locals 0

    return-void
.end method

.method public final b()Lmaps/l/am;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(J)V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/l/am;)V
    .locals 0

    return-void
.end method

.method public final c()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lmaps/l/am;)V
    .locals 0

    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Lmaps/l/am;)V
    .locals 0

    return-void
.end method

.method public final e()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final f()Lmaps/l/am;
    .locals 0

    return-object p0
.end method

.method public final g()Lmaps/l/am;
    .locals 0

    return-object p0
.end method

.method public final h()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final i()Lmaps/l/am;
    .locals 0

    return-object p0
.end method

.method public final j()Lmaps/l/am;
    .locals 0

    return-object p0
.end method
