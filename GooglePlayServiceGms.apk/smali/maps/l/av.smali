.class final Lmaps/l/av;
.super Lmaps/l/aw;

# interfaces
.implements Lmaps/l/am;


# instance fields
.field private volatile a:J

.field private b:Lmaps/l/am;

.field private c:Lmaps/l/am;

.field private volatile d:J

.field private e:Lmaps/l/am;

.field private f:Lmaps/l/am;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILmaps/l/am;)V
    .locals 3

    const-wide v1, 0x7fffffffffffffffL

    invoke-direct {p0, p1, p2, p3}, Lmaps/l/aw;-><init>(Ljava/lang/Object;ILmaps/l/am;)V

    iput-wide v1, p0, Lmaps/l/av;->a:J

    invoke-static {}, Lmaps/l/m;->k()Lmaps/l/am;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/av;->b:Lmaps/l/am;

    invoke-static {}, Lmaps/l/m;->k()Lmaps/l/am;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/av;->c:Lmaps/l/am;

    iput-wide v1, p0, Lmaps/l/av;->d:J

    invoke-static {}, Lmaps/l/m;->k()Lmaps/l/am;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/av;->e:Lmaps/l/am;

    invoke-static {}, Lmaps/l/m;->k()Lmaps/l/am;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/av;->f:Lmaps/l/am;

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 0

    iput-wide p1, p0, Lmaps/l/av;->a:J

    return-void
.end method

.method public final a(Lmaps/l/am;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/av;->b:Lmaps/l/am;

    return-void
.end method

.method public final b(J)V
    .locals 0

    iput-wide p1, p0, Lmaps/l/av;->d:J

    return-void
.end method

.method public final b(Lmaps/l/am;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/av;->c:Lmaps/l/am;

    return-void
.end method

.method public final c(Lmaps/l/am;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/av;->e:Lmaps/l/am;

    return-void
.end method

.method public final d(Lmaps/l/am;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/av;->f:Lmaps/l/am;

    return-void
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lmaps/l/av;->a:J

    return-wide v0
.end method

.method public final f()Lmaps/l/am;
    .locals 1

    iget-object v0, p0, Lmaps/l/av;->b:Lmaps/l/am;

    return-object v0
.end method

.method public final g()Lmaps/l/am;
    .locals 1

    iget-object v0, p0, Lmaps/l/av;->c:Lmaps/l/am;

    return-object v0
.end method

.method public final h()J
    .locals 2

    iget-wide v0, p0, Lmaps/l/av;->d:J

    return-wide v0
.end method

.method public final i()Lmaps/l/am;
    .locals 1

    iget-object v0, p0, Lmaps/l/av;->e:Lmaps/l/am;

    return-object v0
.end method

.method public final j()Lmaps/l/am;
    .locals 1

    iget-object v0, p0, Lmaps/l/av;->f:Lmaps/l/am;

    return-object v0
.end method
