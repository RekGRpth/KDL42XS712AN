.class final Lmaps/l/an;
.super Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field volatile a:I

.field b:I

.field volatile c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field final d:Ljava/lang/ref/ReferenceQueue;

.field final e:Ljava/lang/ref/ReferenceQueue;

.field private f:Lmaps/l/m;

.field private g:I

.field private h:I

.field private i:J

.field private j:Ljava/util/Queue;

.field private k:Ljava/util/concurrent/atomic/AtomicInteger;

.field private l:Ljava/util/Queue;

.field private m:Ljava/util/Queue;

.field private n:Lmaps/l/c;


# direct methods
.method constructor <init>(Lmaps/l/m;IJLmaps/l/c;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lmaps/l/an;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iput-wide p3, p0, Lmaps/l/an;->i:J

    invoke-static {p5}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/c;

    iput-object v0, p0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-static {p2}, Lmaps/l/an;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    iput v2, p0, Lmaps/l/an;->h:I

    iget-object v2, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v2}, Lmaps/l/m;->b()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lmaps/l/an;->h:I

    int-to-long v2, v2

    iget-wide v4, p0, Lmaps/l/an;->i:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lmaps/l/an;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/l/an;->h:I

    :cond_0
    iput-object v0, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {p1}, Lmaps/l/m;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_0
    iput-object v0, p0, Lmaps/l/an;->d:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {p1}, Lmaps/l/m;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_1
    iput-object v1, p0, Lmaps/l/an;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {p1}, Lmaps/l/m;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_1
    iput-object v0, p0, Lmaps/l/an;->j:Ljava/util/Queue;

    invoke-virtual {p1}, Lmaps/l/m;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lmaps/l/bk;

    invoke-direct {v0}, Lmaps/l/bk;-><init>()V

    :goto_2
    iput-object v0, p0, Lmaps/l/an;->l:Ljava/util/Queue;

    invoke-virtual {p1}, Lmaps/l/m;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lmaps/l/r;

    invoke-direct {v0}, Lmaps/l/r;-><init>()V

    :goto_3
    iput-object v0, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-static {}, Lmaps/l/m;->l()Ljava/util/Queue;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {}, Lmaps/l/m;->l()Ljava/util/Queue;

    move-result-object v0

    goto :goto_2

    :cond_5
    invoke-static {}, Lmaps/l/m;->l()Ljava/util/Queue;

    move-result-object v0

    goto :goto_3
.end method

.method private a(Lmaps/l/am;Ljava/lang/Object;ILjava/lang/Object;JLmaps/l/j;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lmaps/l/am;->h()J

    move-result-wide v0

    sub-long v0, p5, v0

    iget-object v2, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-wide v2, v2, Lmaps/l/m;->h:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-interface {p1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v0

    invoke-interface {v0}, Lmaps/l/ba;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p2, p3, p7}, Lmaps/l/an;->c(Ljava/lang/Object;ILmaps/l/j;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p4, v0

    :cond_0
    return-object p4
.end method

.method private a(Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;)Ljava/lang/Object;
    .locals 3

    invoke-interface {p3}, Lmaps/l/ba;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Recursive load"

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    :try_start_0
    invoke-interface {p3}, Lmaps/l/ba;->e()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lmaps/l/k;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CacheLoader returned null for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/l/k;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-interface {v1}, Lmaps/l/c;->b()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v1, v1, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v1}, Lmaps/k/ae;->a()J

    move-result-wide v1

    invoke-direct {p0, p1, v1, v2}, Lmaps/l/an;->b(Lmaps/l/am;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-interface {v1}, Lmaps/l/c;->b()V

    return-object v0
.end method

.method private static a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    return-object v0
.end method

.method private a(Ljava/lang/Object;IJ)Lmaps/l/am;
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lmaps/l/an;->e(Ljava/lang/Object;I)Lmaps/l/am;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v2, v1, p3, p4}, Lmaps/l/m;->b(Lmaps/l/am;J)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p3, p4}, Lmaps/l/an;->a(J)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;ILmaps/l/am;)Lmaps/l/am;
    .locals 2

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->k:Lmaps/l/u;

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1, p2, p3}, Lmaps/l/u;->a(Lmaps/l/an;Ljava/lang/Object;ILmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/l/am;Lmaps/l/am;)Lmaps/l/am;
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p1}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v1

    invoke-interface {v1}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-interface {v1}, Lmaps/l/ba;->b()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->k:Lmaps/l/u;

    invoke-virtual {v0, p0, p1, p2}, Lmaps/l/u;->a(Lmaps/l/an;Lmaps/l/am;Lmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    iget-object v3, p0, Lmaps/l/an;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v1, v3, v2, v0}, Lmaps/l/ba;->a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lmaps/l/am;)Lmaps/l/ba;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/l/am;->a(Lmaps/l/ba;)V

    goto :goto_0
.end method

.method private a(Lmaps/l/am;Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)Lmaps/l/am;
    .locals 1

    invoke-direct {p0, p3, p4, p5}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V

    iget-object v0, p0, Lmaps/l/an;->l:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    invoke-interface {p4}, Lmaps/l/ba;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p4, v0}, Lmaps/l/ba;->b(Ljava/lang/Object;)V

    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, Lmaps/l/an;->b(Lmaps/l/am;Lmaps/l/am;)Lmaps/l/am;

    move-result-object p1

    goto :goto_0
.end method

.method private a(J)V
    .locals 1

    invoke-virtual {p0}, Lmaps/l/an;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lmaps/l/an;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    throw v0
.end method

.method private a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V
    .locals 2

    iget v0, p0, Lmaps/l/an;->g:I

    invoke-interface {p2}, Lmaps/l/ba;->c()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lmaps/l/an;->g:I

    invoke-virtual {p3}, Lmaps/l/bu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-interface {v0}, Lmaps/l/c;->c()V

    :cond_0
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->i:Ljava/util/Queue;

    sget-object v1, Lmaps/l/m;->n:Ljava/util/Queue;

    if-eq v0, v1, :cond_1

    invoke-interface {p2}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Lmaps/l/cb;

    invoke-direct {v1, p1, v0, p3}, Lmaps/l/cb;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lmaps/l/bu;)V

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->i:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private a(Lmaps/l/am;)V
    .locals 1

    sget-object v0, Lmaps/l/bu;->c:Lmaps/l/bu;

    invoke-direct {p0, p1, v0}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/bu;)V

    iget-object v0, p0, Lmaps/l/an;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lmaps/l/am;Ljava/lang/Object;J)V
    .locals 3

    invoke-interface {p1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v1, v1, Lmaps/l/m;->g:Lmaps/l/ci;

    const/4 v1, 0x1

    const-string v2, "Weights must be non-negative"

    invoke-static {v1, v2}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v1, v1, Lmaps/l/m;->f:Lmaps/l/aq;

    invoke-virtual {v1, p0, p1, p2}, Lmaps/l/aq;->a(Lmaps/l/an;Lmaps/l/am;Ljava/lang/Object;)Lmaps/l/ba;

    move-result-object v1

    invoke-interface {p1, v1}, Lmaps/l/am;->a(Lmaps/l/ba;)V

    invoke-direct {p0}, Lmaps/l/an;->e()V

    iget v1, p0, Lmaps/l/an;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/l/an;->g:I

    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v1}, Lmaps/l/m;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1, p3, p4}, Lmaps/l/am;->a(J)V

    :cond_0
    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v1}, Lmaps/l/m;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1, p3, p4}, Lmaps/l/am;->b(J)V

    :cond_1
    iget-object v1, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lmaps/l/an;->l:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p2}, Lmaps/l/ba;->b(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lmaps/l/am;Lmaps/l/bu;)V
    .locals 2

    invoke-interface {p1}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Lmaps/l/am;->c()I

    invoke-interface {p1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V

    return-void
.end method

.method private a(Ljava/lang/Object;ILmaps/l/ai;)Z
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v3, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2}, Lmaps/l/am;->c()I

    move-result v6

    if-ne v6, p2, :cond_2

    if-eqz v5, :cond_2

    iget-object v6, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v6, v6, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v6, p1, v5}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v5

    if-ne v5, p3, :cond_1

    invoke-virtual {p3}, Lmaps/l/ai;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p3, Lmaps/l/ai;->a:Lmaps/l/ba;

    invoke-interface {v2, v0}, Lmaps/l/am;->a(Lmaps/l/ba;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0, v0, v2}, Lmaps/l/an;->b(Lmaps/l/am;Lmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v0, v1

    goto :goto_2

    :cond_2
    :try_start_2
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/Object;ILmaps/l/ai;Ljava/lang/Object;)Z
    .locals 11

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v5

    invoke-direct {p0, v5, v6}, Lmaps/l/an;->c(J)V

    iget v0, p0, Lmaps/l/an;->a:I

    add-int/lit8 v3, v0, 0x1

    iget v0, p0, Lmaps/l/an;->h:I

    if-le v3, v0, :cond_0

    invoke-direct {p0}, Lmaps/l/an;->g()V

    iget v0, p0, Lmaps/l/an;->a:I

    add-int/lit8 v3, v0, 0x1

    :cond_0
    iget-object v7, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    move-object v4, v0

    :goto_0
    if-eqz v4, :cond_5

    invoke-interface {v4}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v4}, Lmaps/l/am;->c()I

    move-result v10

    if-ne v10, p2, :cond_4

    if-eqz v9, :cond_4

    iget-object v10, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v10, v10, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v10, p1, v9}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v4}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v0

    invoke-interface {v0}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v7

    if-eq p3, v0, :cond_1

    if-nez v7, :cond_3

    sget-object v8, Lmaps/l/m;->m:Lmaps/l/ba;

    if-eq v0, v8, :cond_3

    :cond_1
    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    invoke-virtual {p3}, Lmaps/l/ai;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    if-nez v7, :cond_2

    sget-object v0, Lmaps/l/bu;->c:Lmaps/l/bu;

    :goto_1
    invoke-direct {p0, p1, p3, v0}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V

    add-int/lit8 v0, v3, -0x1

    :goto_2
    invoke-direct {p0, v4, p4, v5, v6}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;J)V

    iput v0, p0, Lmaps/l/an;->a:I

    invoke-direct {p0}, Lmaps/l/an;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v0, v2

    :goto_3
    return v0

    :cond_2
    :try_start_1
    sget-object v0, Lmaps/l/bu;->b:Lmaps/l/bu;

    goto :goto_1

    :cond_3
    new-instance v0, Lmaps/l/bi;

    const/4 v2, 0x0

    invoke-direct {v0, p4, v2}, Lmaps/l/bi;-><init>(Ljava/lang/Object;I)V

    sget-object v2, Lmaps/l/bu;->b:Lmaps/l/bu;

    invoke-direct {p0, p1, v0, v2}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v0, v1

    goto :goto_3

    :cond_4
    :try_start_2
    invoke-interface {v4}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v4

    goto :goto_0

    :cond_5
    iget v1, p0, Lmaps/l/an;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/l/an;->b:I

    invoke-direct {p0, p1, p2, v0}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    invoke-direct {p0, v0, p4, v5, v6}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;J)V

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v3, p0, Lmaps/l/an;->a:I

    invoke-direct {p0}, Lmaps/l/an;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v0, v2

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0

    :cond_6
    move v0, v3

    goto :goto_2
.end method

.method private a(Lmaps/l/am;ILmaps/l/bu;)Z
    .locals 8

    iget v0, p0, Lmaps/l/an;->a:I

    iget-object v6, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v7, p2, v0

    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/am;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    if-ne v2, p1, :cond_0

    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)Lmaps/l/am;

    move-result-object v0

    iget v1, p0, Lmaps/l/an;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6, v7, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/l/an;->a:I

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Ljava/lang/Object;ILmaps/l/j;)Ljava/lang/Object;
    .locals 17

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {p0 .. p0}, Lmaps/l/an;->lock()V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v3, v3, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v3}, Lmaps/k/ae;->a()J

    move-result-wide v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10}, Lmaps/l/an;->c(J)V

    move-object/from16 v0, p0

    iget v3, v0, Lmaps/l/an;->a:I

    add-int/lit8 v11, v3, -0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    and-int v13, p2, v3

    invoke-virtual {v12, v13}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/l/am;

    move-object v5, v3

    :goto_0
    if-eqz v5, :cond_7

    invoke-interface {v5}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v14

    invoke-interface {v5}, Lmaps/l/am;->c()I

    move-result v8

    move/from16 v0, p2

    if-ne v8, v0, :cond_3

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v8, v8, Lmaps/l/m;->d:Lmaps/k/b;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v14}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v5}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v8

    invoke-interface {v8}, Lmaps/l/ba;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    move-object v7, v8

    :goto_1
    if-eqz v6, :cond_6

    new-instance v4, Lmaps/l/ai;

    invoke-direct {v4}, Lmaps/l/ai;-><init>()V

    if-nez v5, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/am;)Lmaps/l/am;

    move-result-object v3

    invoke-interface {v3, v4}, Lmaps/l/am;->a(Lmaps/l/ba;)V

    invoke-virtual {v12, v13, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v16, v4

    move-object v4, v3

    move-object/from16 v3, v16

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lmaps/l/an;->unlock()V

    invoke-direct/range {p0 .. p0}, Lmaps/l/an;->h()V

    if-eqz v6, :cond_5

    :try_start_1
    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v3, v0, v1}, Lmaps/l/ai;->a(Ljava/lang/Object;Lmaps/l/j;)Lmaps/s/k;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3, v5}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/ai;Lmaps/s/k;)Ljava/lang/Object;

    move-result-object v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-interface {v4}, Lmaps/l/c;->b()V

    :goto_3
    return-object v3

    :cond_0
    :try_start_3
    invoke-interface {v8}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    sget-object v6, Lmaps/l/bu;->c:Lmaps/l/bu;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v8, v6}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/l/an;->l:Ljava/util/Queue;

    invoke-interface {v6, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v6, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iput v11, v0, Lmaps/l/an;->a:I

    move v6, v7

    move-object v7, v8

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v15, v5, v9, v10}, Lmaps/l/m;->b(Lmaps/l/am;J)Z

    move-result v15

    if-eqz v15, :cond_2

    sget-object v6, Lmaps/l/bu;->d:Lmaps/l/bu;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v8, v6}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v3

    invoke-virtual/range {p0 .. p0}, Lmaps/l/an;->unlock()V

    invoke-direct/range {p0 .. p0}, Lmaps/l/an;->h()V

    throw v3

    :cond_2
    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v9, v10}, Lmaps/l/an;->c(Lmaps/l/am;J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-interface {v3}, Lmaps/l/c;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-virtual/range {p0 .. p0}, Lmaps/l/an;->unlock()V

    invoke-direct/range {p0 .. p0}, Lmaps/l/an;->h()V

    move-object v3, v6

    goto :goto_3

    :cond_3
    :try_start_5
    invoke-interface {v5}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v5

    goto/16 :goto_0

    :cond_4
    invoke-interface {v5, v4}, Lmaps/l/am;->a(Lmaps/l/ba;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v3, v4

    move-object v4, v5

    goto :goto_2

    :catchall_1
    move-exception v3

    :try_start_6
    monitor-exit v4

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-interface {v4}, Lmaps/l/c;->b()V

    throw v3

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v7}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_3

    :cond_6
    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_2

    :cond_7
    move/from16 v16, v7

    move-object v7, v6

    move/from16 v6, v16

    goto/16 :goto_1
.end method

.method private b(Lmaps/l/am;Lmaps/l/am;)Lmaps/l/am;
    .locals 4

    iget v2, p0, Lmaps/l/an;->a:I

    invoke-interface {p2}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v1

    :goto_0
    if-eq p1, p2, :cond_1

    invoke-direct {p0, p1, v1}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    if-eqz v0, :cond_0

    move v1, v2

    :goto_1
    invoke-interface {p1}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object p1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lmaps/l/an;->a(Lmaps/l/am;)V

    add-int/lit8 v0, v2, -0x1

    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_1

    :cond_1
    iput v2, p0, Lmaps/l/an;->a:I

    return-object v1
.end method

.method private b(J)V
    .locals 3

    invoke-direct {p0}, Lmaps/l/an;->e()V

    :cond_0
    iget-object v0, p0, Lmaps/l/an;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v1, v0, p1, p2}, Lmaps/l/m;->b(Lmaps/l/am;J)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lmaps/l/am;->c()I

    move-result v1

    sget-object v2, Lmaps/l/bu;->d:Lmaps/l/bu;

    invoke-direct {p0, v0, v1, v2}, Lmaps/l/an;->a(Lmaps/l/am;ILmaps/l/bu;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v1, v0, p1, p2}, Lmaps/l/m;->b(Lmaps/l/am;J)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lmaps/l/am;->c()I

    move-result v1

    sget-object v2, Lmaps/l/bu;->d:Lmaps/l/bu;

    invoke-direct {p0, v0, v1, v2}, Lmaps/l/an;->a(Lmaps/l/am;ILmaps/l/bu;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    return-void
.end method

.method private b(Lmaps/l/am;J)V
    .locals 1

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2, p3}, Lmaps/l/am;->a(J)V

    :cond_0
    iget-object v0, p0, Lmaps/l/an;->j:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private c(Ljava/lang/Object;ILmaps/l/j;)Ljava/lang/Object;
    .locals 7

    const/4 v6, 0x0

    invoke-direct {p0, p1, p2}, Lmaps/l/an;->d(Ljava/lang/Object;I)Lmaps/l/ai;

    move-result-object v4

    if-nez v4, :cond_0

    move-object v0, v6

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v4, p1, p3}, Lmaps/l/ai;->a(Ljava/lang/Object;Lmaps/l/j;)Lmaps/s/k;

    move-result-object v5

    new-instance v0, Lmaps/l/ao;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lmaps/l/ao;-><init>(Lmaps/l/an;Ljava/lang/Object;ILmaps/l/ai;Lmaps/s/k;)V

    sget-object v1, Lmaps/l/m;->b:Lmaps/s/m;

    invoke-interface {v5, v0, v1}, Lmaps/s/k;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    invoke-interface {v5}, Lmaps/s/k;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {v5}, Lmaps/s/s;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, v6

    goto :goto_0
.end method

.method private c()V
    .locals 1

    invoke-virtual {p0}, Lmaps/l/an;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Lmaps/l/an;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    throw v0
.end method

.method private c(J)V
    .locals 2

    invoke-virtual {p0}, Lmaps/l/an;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Lmaps/l/an;->d()V

    invoke-direct {p0, p1, p2}, Lmaps/l/an;->b(J)V

    iget-object v0, p0, Lmaps/l/an;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    throw v0
.end method

.method private c(Lmaps/l/am;J)V
    .locals 1

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2, p3}, Lmaps/l/am;->a(J)V

    :cond_0
    iget-object v0, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private d(Ljava/lang/Object;I)Lmaps/l/ai;
    .locals 8

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lmaps/l/an;->c(J)V

    iget-object v4, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_3

    invoke-interface {v1}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v1}, Lmaps/l/am;->c()I

    move-result v7

    if-ne v7, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v7, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v7, v7, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v7, p1, v6}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    invoke-interface {v4}, Lmaps/l/ba;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Lmaps/l/am;->h()J

    move-result-wide v5

    sub-long/2addr v2, v5

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-wide v5, v0, Lmaps/l/m;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v2, v5

    if-gez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    :try_start_1
    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    new-instance v0, Lmaps/l/ai;

    invoke-direct {v0, v4}, Lmaps/l/ai;-><init>(Lmaps/l/ba;)V

    invoke-interface {v1, v0}, Lmaps/l/am;->a(Lmaps/l/ba;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-interface {v1}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v1

    goto :goto_0

    :cond_3
    iget v1, p0, Lmaps/l/an;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/l/an;->b:I

    new-instance v1, Lmaps/l/ai;

    invoke-direct {v1}, Lmaps/l/ai;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    invoke-interface {v0, v1}, Lmaps/l/am;->a(Lmaps/l/ba;)V

    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0
.end method

.method private d()V
    .locals 5

    const/16 v4, 0x10

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lmaps/l/an;->d:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lmaps/l/am;

    iget-object v3, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v3, v0}, Lmaps/l/m;->a(Lmaps/l/am;)V

    add-int/lit8 v0, v1, 0x1

    if-ne v0, v4, :cond_3

    :cond_0
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lmaps/l/an;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lmaps/l/ba;

    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v1, v0}, Lmaps/l/m;->a(Lmaps/l/ba;)V

    add-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    :cond_2
    return-void

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method private e(Ljava/lang/Object;I)Lmaps/l/am;
    .locals 3

    iget-object v0, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    :goto_0
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lmaps/l/am;->c()I

    move-result v1

    if-ne v1, p2, :cond_0

    invoke-interface {v0}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lmaps/l/an;->c()V

    :cond_0
    invoke-interface {v0}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v2, v2, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v2, p1, v1}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private e()V
    .locals 2

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/l/an;->j:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private f()V
    .locals 4

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0}, Lmaps/l/an;->e()V

    :cond_2
    iget v0, p0, Lmaps/l/an;->g:I

    int-to-long v0, v0

    iget-wide v2, p0, Lmaps/l/an;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v2

    invoke-interface {v2}, Lmaps/l/ba;->c()I

    move-result v2

    if-lez v2, :cond_3

    invoke-interface {v0}, Lmaps/l/am;->c()I

    move-result v1

    sget-object v2, Lmaps/l/bu;->e:Lmaps/l/bu;

    invoke-direct {p0, v0, v1, v2}, Lmaps/l/an;->a(Lmaps/l/am;ILmaps/l/bu;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private g()V
    .locals 11

    iget-object v7, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    const/high16 v0, 0x40000000    # 2.0f

    if-lt v8, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v5, p0, Lmaps/l/an;->a:I

    shl-int/lit8 v0, v8, 0x1

    invoke-static {v0}, Lmaps/l/an;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/l/an;->h:I

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_5

    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v2

    invoke-interface {v0}, Lmaps/l/am;->c()I

    move-result v1

    and-int v4, v1, v10

    if-nez v2, :cond_2

    invoke-virtual {v9, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v2, v5

    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v2

    goto :goto_1

    :cond_2
    move-object v1, v0

    :goto_3
    if-eqz v2, :cond_3

    invoke-interface {v2}, Lmaps/l/am;->c()I

    move-result v3

    and-int/2addr v3, v10

    if-eq v3, v4, :cond_6

    move-object v1, v2

    :goto_4
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v2

    move v4, v3

    goto :goto_3

    :cond_3
    invoke-virtual {v9, v4, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v3, v0

    move v2, v5

    :goto_5
    if-eq v3, v1, :cond_1

    invoke-interface {v3}, Lmaps/l/am;->c()I

    move-result v0

    and-int v4, v0, v10

    invoke-virtual {v9, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    invoke-direct {p0, v3, v0}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v9, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v2

    :goto_6
    invoke-interface {v3}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v2

    move-object v3, v2

    move v2, v0

    goto :goto_5

    :cond_4
    invoke-direct {p0, v3}, Lmaps/l/an;->a(Lmaps/l/am;)V

    add-int/lit8 v0, v2, -0x1

    goto :goto_6

    :cond_5
    iput-object v9, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput v5, p0, Lmaps/l/an;->a:I

    goto :goto_0

    :cond_6
    move v3, v4

    goto :goto_4

    :cond_7
    move v2, v5

    goto :goto_2
.end method

.method private h()V
    .locals 1

    invoke-virtual {p0}, Lmaps/l/an;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->m()V

    :cond_0
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8

    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, Lmaps/l/an;->a:I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v1, v1, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v1}, Lmaps/k/ae;->a()J

    move-result-wide v5

    invoke-direct {p0, p1, p2, v5, v6}, Lmaps/l/an;->a(Ljava/lang/Object;IJ)Lmaps/l/am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v2

    invoke-interface {v2}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-direct {p0, v1, v5, v6}, Lmaps/l/an;->b(Lmaps/l/am;J)V

    invoke-interface {v1}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v2

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v7, v0, Lmaps/l/m;->l:Lmaps/l/j;

    move-object v0, p0

    move v3, p2

    invoke-direct/range {v0 .. v7}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;ILjava/lang/Object;JLmaps/l/j;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-direct {p0}, Lmaps/l/an;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    invoke-virtual {p0}, Lmaps/l/an;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 11

    const/4 v6, 0x0

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v7

    invoke-direct {p0, v7, v8}, Lmaps/l/an;->c(J)V

    iget-object v9, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v10, p2, v0

    invoke-virtual {v9, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/am;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/l/am;->c()I

    move-result v0

    if-ne v0, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v0, p1, v3}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    invoke-interface {v4}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-interface {v4}, Lmaps/l/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/l/an;->a:I

    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    sget-object v5, Lmaps/l/bu;->c:Lmaps/l/bu;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)Lmaps/l/am;

    move-result-object v0

    iget v1, p0, Lmaps/l/an;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v9, v10, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/l/an;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move-object v0, v6

    :goto_1
    return-object v0

    :cond_1
    :try_start_1
    iget v1, p0, Lmaps/l/an;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/l/an;->b:I

    sget-object v1, Lmaps/l/bu;->b:Lmaps/l/bu;

    invoke-direct {p0, p1, v4, v1}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V

    invoke-direct {p0, v2, p3, v7, v8}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;J)V

    invoke-direct {p0}, Lmaps/l/an;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move-object v0, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 9

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, Lmaps/l/an;->c(J)V

    iget v0, p0, Lmaps/l/an;->a:I

    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Lmaps/l/an;->h:I

    if-le v0, v2, :cond_0

    invoke-direct {p0}, Lmaps/l/an;->g()V

    iget v0, p0, Lmaps/l/an;->a:I

    :cond_0
    iget-object v5, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v6, p2, v0

    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_5

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v2}, Lmaps/l/am;->c()I

    move-result v8

    if-ne v8, p2, :cond_4

    if-eqz v7, :cond_4

    iget-object v8, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v8, v8, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v8, p1, v7}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v5

    invoke-interface {v5}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    invoke-interface {v5}, Lmaps/l/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/l/bu;->c:Lmaps/l/bu;

    invoke-direct {p0, p1, v5, v0}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V

    invoke-direct {p0, v2, p3, v3, v4}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;J)V

    iget v0, p0, Lmaps/l/an;->a:I

    :goto_1
    iput v0, p0, Lmaps/l/an;->a:I

    invoke-direct {p0}, Lmaps/l/an;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_1
    :try_start_1
    invoke-direct {p0, v2, p3, v3, v4}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;J)V

    iget v0, p0, Lmaps/l/an;->a:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz p4, :cond_3

    invoke-direct {p0, v2, v3, v4}, Lmaps/l/an;->c(Lmaps/l/am;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_2

    :cond_3
    :try_start_2
    iget v1, p0, Lmaps/l/an;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/l/an;->b:I

    sget-object v1, Lmaps/l/bu;->b:Lmaps/l/bu;

    invoke-direct {p0, p1, v5, v1}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V

    invoke-direct {p0, v2, p3, v3, v4}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;J)V

    invoke-direct {p0}, Lmaps/l/an;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_2

    :cond_4
    :try_start_3
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v2

    goto :goto_0

    :cond_5
    iget v2, p0, Lmaps/l/an;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/l/an;->b:I

    invoke-direct {p0, p1, p2, v0}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    invoke-direct {p0, v0, p3, v3, v4}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;J)V

    invoke-virtual {v5, v6, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iget v0, p0, Lmaps/l/an;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->a:I

    invoke-direct {p0}, Lmaps/l/an;->f()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move-object v0, v1

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILmaps/l/ai;Lmaps/s/k;)Ljava/lang/Object;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p4}, Lmaps/s/s;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Lmaps/l/k;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CacheLoader returned null for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lmaps/l/k;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-virtual {p3}, Lmaps/l/ai;->d()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lmaps/l/c;->b(J)V

    invoke-direct {p0, p1, p2, p3}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/ai;)Z

    :cond_0
    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-virtual {p3}, Lmaps/l/ai;->d()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lmaps/l/c;->a(J)V

    invoke-direct {p0, p1, p2, p3, v1}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/ai;Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    iget-object v0, p0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-virtual {p3}, Lmaps/l/ai;->d()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lmaps/l/c;->b(J)V

    invoke-direct {p0, p1, p2, p3}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/ai;)Z

    :cond_2
    return-object v1
.end method

.method final a(Ljava/lang/Object;ILmaps/l/j;)Ljava/lang/Object;
    .locals 8

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget v0, p0, Lmaps/l/an;->a:I

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lmaps/l/an;->e(Ljava/lang/Object;I)Lmaps/l/am;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v5

    invoke-virtual {p0, v1, v5, v6}, Lmaps/l/an;->a(Lmaps/l/am;J)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v1, v5, v6}, Lmaps/l/an;->b(Lmaps/l/am;J)V

    iget-object v0, p0, Lmaps/l/an;->n:Lmaps/l/c;

    invoke-interface {v0}, Lmaps/l/c;->a()V

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;ILjava/lang/Object;JLmaps/l/j;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v0

    invoke-interface {v0}, Lmaps/l/ba;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v1, p1, v0}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-direct {p0, p1, p2, p3}, Lmaps/l/an;->b(Ljava/lang/Object;ILmaps/l/j;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_3
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v2, v0, Ljava/lang/Error;

    if-eqz v2, :cond_2

    new-instance v1, Lmaps/s/c;

    check-cast v0, Ljava/lang/Error;

    invoke-direct {v1, v0}, Lmaps/s/c;-><init>(Ljava/lang/Error;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    throw v0

    :cond_2
    :try_start_4
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_3

    new-instance v1, Lmaps/s/r;

    invoke-direct {v1, v0}, Lmaps/s/r;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method final a(Lmaps/l/am;J)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lmaps/l/an;->c()V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v1

    invoke-interface {v1}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lmaps/l/an;->c()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v2, p1, p2, p3}, Lmaps/l/m;->b(Lmaps/l/am;J)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p2, p3}, Lmaps/l/an;->a(J)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method final a()V
    .locals 5

    const/4 v1, 0x0

    iget v0, p0, Lmaps/l/an;->a:I

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v3, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move v2, v1

    :goto_0
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/am;

    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    invoke-interface {v4}, Lmaps/l/ba;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lmaps/l/bu;->a:Lmaps/l/bu;

    invoke-direct {p0, v0, v4}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/bu;)V

    :cond_0
    invoke-interface {v0}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lmaps/l/an;->d:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_5
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lmaps/l/an;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_7
    iget-object v0, p0, Lmaps/l/an;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lmaps/l/an;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lmaps/l/an;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/l/an;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    :cond_8
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 11

    const/4 v6, 0x0

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v7

    invoke-direct {p0, v7, v8}, Lmaps/l/an;->c(J)V

    iget-object v9, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v10, p2, v0

    invoke-virtual {v9, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/am;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/l/am;->c()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v0, p1, v3}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    invoke-interface {v4}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-interface {v4}, Lmaps/l/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/l/an;->a:I

    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    sget-object v5, Lmaps/l/bu;->c:Lmaps/l/bu;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)Lmaps/l/am;

    move-result-object v0

    iget v1, p0, Lmaps/l/an;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v9, v10, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/l/an;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v0, v6

    :goto_1
    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v1, v1, Lmaps/l/m;->e:Lmaps/k/b;

    invoke-virtual {v1, p3, v0}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    sget-object v0, Lmaps/l/bu;->b:Lmaps/l/bu;

    invoke-direct {p0, p1, v4, v0}, Lmaps/l/an;->a(Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)V

    invoke-direct {p0, v2, p4, v7, v8}, Lmaps/l/an;->a(Lmaps/l/am;Ljava/lang/Object;J)V

    invoke-direct {p0}, Lmaps/l/an;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-direct {p0, v2, v7, v8}, Lmaps/l/an;->c(Lmaps/l/am;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v0, v6

    goto :goto_1

    :cond_3
    :try_start_3
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v0, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILmaps/l/ba;)Z
    .locals 8

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget v1, p0, Lmaps/l/an;->a:I

    iget-object v6, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v7, p2, v1

    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/am;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/l/am;->c()I

    move-result v4

    if-ne v4, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v4, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v4, v4, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v4, p1, v3}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    if-ne v4, p3, :cond_2

    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    sget-object v5, Lmaps/l/bu;->c:Lmaps/l/bu;

    move-object v0, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)Lmaps/l/am;

    move-result-object v0

    iget v1, p0, Lmaps/l/an;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6, v7, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/l/an;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-virtual {p0}, Lmaps/l/an;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/l/an;->h()V

    :cond_0
    const/4 v0, 0x1

    :cond_1
    :goto_1
    return v0

    :cond_2
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-virtual {p0}, Lmaps/l/an;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_1

    :cond_3
    :try_start_1
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-virtual {p0}, Lmaps/l/an;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-virtual {p0}, Lmaps/l/an;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-direct {p0}, Lmaps/l/an;->h()V

    :cond_5
    throw v0
.end method

.method final a(Lmaps/l/am;I)Z
    .locals 8

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget v0, p0, Lmaps/l/an;->a:I

    iget-object v6, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v7, p2, v0

    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/am;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    if-ne v2, p1, :cond_0

    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    sget-object v5, Lmaps/l/bu;->c:Lmaps/l/bu;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)Lmaps/l/am;

    move-result-object v0

    iget v1, p0, Lmaps/l/an;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6, v7, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/l/an;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0
.end method

.method final b()V
    .locals 2

    iget-object v0, p0, Lmaps/l/an;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lmaps/l/an;->c(J)V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    :cond_0
    return-void
.end method

.method final b(Ljava/lang/Object;I)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, Lmaps/l/an;->a:I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v1, v1, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v1}, Lmaps/k/ae;->a()J

    move-result-wide v1

    invoke-direct {p0, p1, p2, v1, v2}, Lmaps/l/an;->a(Ljava/lang/Object;IJ)Lmaps/l/am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    :goto_0
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v1

    invoke-interface {v1}, Lmaps/l/ba;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0}, Lmaps/l/an;->b()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmaps/l/an;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->b()V

    throw v0
.end method

.method final b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 9

    const/4 v6, 0x0

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v0}, Lmaps/k/ae;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lmaps/l/an;->c(J)V

    iget v0, p0, Lmaps/l/an;->a:I

    iget-object v7, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/am;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/l/am;->c()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v0, v0, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v0, p1, v3}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    invoke-interface {v4}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v5, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v5, v5, Lmaps/l/m;->e:Lmaps/k/b;

    invoke-virtual {v5, p3, v0}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lmaps/l/bu;->a:Lmaps/l/bu;

    :goto_1
    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)Lmaps/l/am;

    move-result-object v0

    iget v1, p0, Lmaps/l/an;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/l/an;->a:I

    sget-object v0, Lmaps/l/bu;->a:Lmaps/l/bu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v5, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move v6, v0

    :goto_3
    return v6

    :cond_0
    if-nez v0, :cond_1

    :try_start_1
    invoke-interface {v4}, Lmaps/l/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v5, Lmaps/l/bu;->c:Lmaps/l/bu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_3

    :cond_2
    move v0, v6

    goto :goto_2

    :cond_3
    :try_start_2
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0
.end method

.method final c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 9

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/l/an;->lock()V

    :try_start_0
    iget-object v1, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v1, v1, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v1}, Lmaps/k/ae;->a()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lmaps/l/an;->c(J)V

    iget v1, p0, Lmaps/l/an;->a:I

    iget-object v7, p0, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v8, p2, v1

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/am;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/l/am;->c()I

    move-result v4

    if-ne v4, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v4, p0, Lmaps/l/an;->f:Lmaps/l/m;

    iget-object v4, v4, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v4, p1, v3}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v4

    invoke-interface {v4}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    sget-object v5, Lmaps/l/bu;->a:Lmaps/l/bu;

    :goto_1
    iget v0, p0, Lmaps/l/an;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/l/an;->b:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lmaps/l/an;->a(Lmaps/l/am;Lmaps/l/am;Ljava/lang/Object;Lmaps/l/ba;Lmaps/l/bu;)Lmaps/l/am;

    move-result-object v0

    iget v1, p0, Lmaps/l/an;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/l/an;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    move-object v0, v6

    :goto_2
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v4}, Lmaps/l/ba;->b()Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lmaps/l/bu;->c:Lmaps/l/bu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_2

    :cond_2
    :try_start_2
    invoke-interface {v2}, Lmaps/l/am;->b()Lmaps/l/am;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/l/an;->unlock()V

    invoke-direct {p0}, Lmaps/l/an;->h()V

    throw v0
.end method
