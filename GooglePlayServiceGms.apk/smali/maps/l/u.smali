.class abstract enum Lmaps/l/u;
.super Ljava/lang/Enum;


# static fields
.field private static enum a:Lmaps/l/u;

.field private static enum b:Lmaps/l/u;

.field private static enum c:Lmaps/l/u;

.field private static enum d:Lmaps/l/u;

.field private static enum e:Lmaps/l/u;

.field private static enum f:Lmaps/l/u;

.field private static enum g:Lmaps/l/u;

.field private static enum h:Lmaps/l/u;

.field private static i:[Lmaps/l/u;

.field private static final synthetic j:[Lmaps/l/u;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/l/v;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lmaps/l/v;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/u;->a:Lmaps/l/u;

    new-instance v0, Lmaps/l/w;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1}, Lmaps/l/w;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/u;->b:Lmaps/l/u;

    new-instance v0, Lmaps/l/x;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1}, Lmaps/l/x;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/u;->c:Lmaps/l/u;

    new-instance v0, Lmaps/l/y;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1}, Lmaps/l/y;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/u;->d:Lmaps/l/u;

    new-instance v0, Lmaps/l/z;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lmaps/l/z;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/u;->e:Lmaps/l/u;

    new-instance v0, Lmaps/l/aa;

    const-string v1, "WEAK_ACCESS"

    invoke-direct {v0, v1}, Lmaps/l/aa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/u;->f:Lmaps/l/u;

    new-instance v0, Lmaps/l/ab;

    const-string v1, "WEAK_WRITE"

    invoke-direct {v0, v1}, Lmaps/l/ab;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/u;->g:Lmaps/l/u;

    new-instance v0, Lmaps/l/ac;

    const-string v1, "WEAK_ACCESS_WRITE"

    invoke-direct {v0, v1}, Lmaps/l/ac;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/u;->h:Lmaps/l/u;

    const/16 v0, 0x8

    new-array v0, v0, [Lmaps/l/u;

    sget-object v1, Lmaps/l/u;->a:Lmaps/l/u;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/l/u;->b:Lmaps/l/u;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/l/u;->c:Lmaps/l/u;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/l/u;->d:Lmaps/l/u;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/l/u;->e:Lmaps/l/u;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/l/u;->f:Lmaps/l/u;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/l/u;->g:Lmaps/l/u;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/l/u;->h:Lmaps/l/u;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/l/u;->j:[Lmaps/l/u;

    const/16 v0, 0x8

    new-array v0, v0, [Lmaps/l/u;

    sget-object v1, Lmaps/l/u;->a:Lmaps/l/u;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/l/u;->b:Lmaps/l/u;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/l/u;->c:Lmaps/l/u;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/l/u;->d:Lmaps/l/u;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/l/u;->e:Lmaps/l/u;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/l/u;->f:Lmaps/l/u;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/l/u;->g:Lmaps/l/u;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/l/u;->h:Lmaps/l/u;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/l/u;->i:[Lmaps/l/u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/l/u;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lmaps/l/aq;ZZ)Lmaps/l/u;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lmaps/l/aq;->b:Lmaps/l/aq;

    if-ne p0, v1, :cond_1

    const/4 v1, 0x4

    move v2, v1

    :goto_0
    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :goto_1
    or-int/2addr v1, v2

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    sget-object v1, Lmaps/l/u;->i:[Lmaps/l/u;

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method static a(Lmaps/l/am;Lmaps/l/am;)V
    .locals 2

    invoke-interface {p0}, Lmaps/l/am;->e()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lmaps/l/am;->a(J)V

    invoke-interface {p0}, Lmaps/l/am;->g()Lmaps/l/am;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/l/m;->a(Lmaps/l/am;Lmaps/l/am;)V

    invoke-interface {p0}, Lmaps/l/am;->f()Lmaps/l/am;

    move-result-object v0

    invoke-static {p1, v0}, Lmaps/l/m;->a(Lmaps/l/am;Lmaps/l/am;)V

    invoke-static {p0}, Lmaps/l/m;->b(Lmaps/l/am;)V

    return-void
.end method

.method static b(Lmaps/l/am;Lmaps/l/am;)V
    .locals 2

    invoke-interface {p0}, Lmaps/l/am;->h()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lmaps/l/am;->b(J)V

    invoke-interface {p0}, Lmaps/l/am;->j()Lmaps/l/am;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/l/m;->b(Lmaps/l/am;Lmaps/l/am;)V

    invoke-interface {p0}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v0

    invoke-static {p1, v0}, Lmaps/l/m;->b(Lmaps/l/am;Lmaps/l/am;)V

    invoke-static {p0}, Lmaps/l/m;->c(Lmaps/l/am;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/l/u;
    .locals 1

    const-class v0, Lmaps/l/u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    return-object v0
.end method

.method public static values()[Lmaps/l/u;
    .locals 1

    sget-object v0, Lmaps/l/u;->j:[Lmaps/l/u;

    invoke-virtual {v0}, [Lmaps/l/u;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/l/u;

    return-object v0
.end method


# virtual methods
.method abstract a(Lmaps/l/an;Ljava/lang/Object;ILmaps/l/am;)Lmaps/l/am;
.end method

.method a(Lmaps/l/an;Lmaps/l/am;Lmaps/l/am;)Lmaps/l/am;
    .locals 2

    invoke-interface {p2}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lmaps/l/am;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lmaps/l/u;->a(Lmaps/l/an;Ljava/lang/Object;ILmaps/l/am;)Lmaps/l/am;

    move-result-object v0

    return-object v0
.end method
