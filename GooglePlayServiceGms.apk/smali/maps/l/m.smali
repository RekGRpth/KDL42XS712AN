.class Lmaps/l/m;
.super Ljava/util/AbstractMap;

# interfaces
.implements Ljava/util/concurrent/ConcurrentMap;


# static fields
.field static final a:Ljava/util/logging/Logger;

.field static final b:Lmaps/s/m;

.field static final m:Lmaps/l/ba;

.field static final n:Ljava/util/Queue;


# instance fields
.field final c:[Lmaps/l/an;

.field final d:Lmaps/k/b;

.field final e:Lmaps/k/b;

.field final f:Lmaps/l/aq;

.field final g:Lmaps/l/ci;

.field final h:J

.field final i:Ljava/util/Queue;

.field final j:Lmaps/k/ae;

.field final k:Lmaps/l/u;

.field final l:Lmaps/l/j;

.field private o:I

.field private p:I

.field private q:I

.field private r:Lmaps/l/aq;

.field private s:J

.field private t:J

.field private u:J

.field private v:Lmaps/l/ca;

.field private w:Ljava/util/Set;

.field private x:Ljava/util/Collection;

.field private y:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/l/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lmaps/l/m;->a:Ljava/util/logging/Logger;

    invoke-static {}, Lmaps/s/n;->a()Lmaps/s/m;

    move-result-object v0

    sput-object v0, Lmaps/l/m;->b:Lmaps/s/m;

    new-instance v0, Lmaps/l/n;

    invoke-direct {v0}, Lmaps/l/n;-><init>()V

    sput-object v0, Lmaps/l/m;->m:Lmaps/l/ba;

    new-instance v0, Lmaps/l/o;

    invoke-direct {v0}, Lmaps/l/o;-><init>()V

    sput-object v0, Lmaps/l/m;->n:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(Lmaps/l/d;Lmaps/l/j;)V
    .locals 13

    const-wide/16 v9, 0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    invoke-virtual {p1}, Lmaps/l/d;->e()I

    move-result v0

    const/high16 v3, 0x10000

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lmaps/l/m;->q:I

    invoke-virtual {p1}, Lmaps/l/d;->i()Lmaps/l/aq;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/m;->r:Lmaps/l/aq;

    invoke-static {}, Lmaps/l/d;->j()Lmaps/l/aq;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/m;->f:Lmaps/l/aq;

    invoke-virtual {p1}, Lmaps/l/d;->b()Lmaps/k/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-static {}, Lmaps/l/d;->c()Lmaps/k/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/m;->e:Lmaps/k/b;

    invoke-virtual {p1}, Lmaps/l/d;->f()J

    move-result-wide v3

    iput-wide v3, p0, Lmaps/l/m;->s:J

    invoke-static {}, Lmaps/l/d;->g()Lmaps/l/ci;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/m;->g:Lmaps/l/ci;

    invoke-virtual {p1}, Lmaps/l/d;->l()J

    move-result-wide v3

    iput-wide v3, p0, Lmaps/l/m;->t:J

    invoke-virtual {p1}, Lmaps/l/d;->k()J

    move-result-wide v3

    iput-wide v3, p0, Lmaps/l/m;->u:J

    invoke-virtual {p1}, Lmaps/l/d;->m()J

    move-result-wide v3

    iput-wide v3, p0, Lmaps/l/m;->h:J

    invoke-static {}, Lmaps/l/d;->n()Lmaps/l/ca;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/m;->v:Lmaps/l/ca;

    iget-object v0, p0, Lmaps/l/m;->v:Lmaps/l/ca;

    sget-object v3, Lmaps/l/h;->a:Lmaps/l/h;

    if-ne v0, v3, :cond_5

    sget-object v0, Lmaps/l/m;->n:Ljava/util/Queue;

    :goto_0
    iput-object v0, p0, Lmaps/l/m;->i:Ljava/util/Queue;

    invoke-virtual {p0}, Lmaps/l/m;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/l/m;->o()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    move v0, v2

    :goto_1
    invoke-static {v0}, Lmaps/l/d;->a(Z)Lmaps/k/ae;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/m;->j:Lmaps/k/ae;

    iget-object v4, p0, Lmaps/l/m;->r:Lmaps/l/aq;

    invoke-virtual {p0}, Lmaps/l/m;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lmaps/l/m;->o()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    move v0, v2

    :goto_2
    invoke-direct {p0}, Lmaps/l/m;->n()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lmaps/l/m;->f()Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_2
    move v3, v2

    :goto_3
    invoke-static {v4, v0, v3}, Lmaps/l/u;->a(Lmaps/l/aq;ZZ)Lmaps/l/u;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/m;->k:Lmaps/l/u;

    invoke-virtual {p1}, Lmaps/l/d;->o()Lmaps/k/ab;

    move-result-object v0

    invoke-interface {v0}, Lmaps/k/ab;->a()Ljava/lang/Object;

    iput-object p2, p0, Lmaps/l/m;->l:Lmaps/l/j;

    invoke-virtual {p1}, Lmaps/l/d;->d()I

    move-result v0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0}, Lmaps/l/m;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lmaps/l/m;->b()Z

    move-result v3

    if-nez v3, :cond_3

    iget-wide v3, p0, Lmaps/l/m;->s:J

    long-to-int v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_3
    move v3, v2

    move v4, v1

    :goto_4
    iget v5, p0, Lmaps/l/m;->q:I

    if-ge v3, v5, :cond_9

    invoke-virtual {p0}, Lmaps/l/m;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    mul-int/lit8 v5, v3, 0x14

    int-to-long v5, v5

    iget-wide v7, p0, Lmaps/l/m;->s:J

    cmp-long v5, v5, v7

    if-gtz v5, :cond_9

    :cond_4
    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    move v3, v1

    goto :goto_3

    :cond_9
    rsub-int/lit8 v4, v4, 0x20

    iput v4, p0, Lmaps/l/m;->p:I

    add-int/lit8 v4, v3, -0x1

    iput v4, p0, Lmaps/l/m;->o:I

    new-array v4, v3, [Lmaps/l/an;

    iput-object v4, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    div-int v4, v0, v3

    mul-int v5, v4, v3

    if-ge v5, v0, :cond_e

    add-int/lit8 v0, v4, 0x1

    :goto_5
    move v6, v2

    :goto_6
    if-ge v6, v0, :cond_a

    shl-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_6

    :cond_a
    invoke-virtual {p0}, Lmaps/l/m;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-wide v4, p0, Lmaps/l/m;->s:J

    int-to-long v7, v3

    div-long/2addr v4, v7

    add-long/2addr v4, v9

    iget-wide v7, p0, Lmaps/l/m;->s:J

    int-to-long v2, v3

    rem-long/2addr v7, v2

    move v3, v1

    move-wide v0, v4

    :goto_7
    iget-object v2, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    array-length v2, v2

    if-ge v3, v2, :cond_c

    int-to-long v4, v3

    cmp-long v2, v4, v7

    if-nez v2, :cond_d

    sub-long v1, v0, v9

    :goto_8
    iget-object v4, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    invoke-virtual {p1}, Lmaps/l/d;->o()Lmaps/k/ab;

    move-result-object v0

    invoke-interface {v0}, Lmaps/k/ab;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/c;

    invoke-direct {p0, v6, v1, v2, v0}, Lmaps/l/m;->a(IJLmaps/l/c;)Lmaps/l/an;

    move-result-object v0

    aput-object v0, v4, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-wide v11, v1

    move-wide v0, v11

    goto :goto_7

    :cond_b
    :goto_9
    iget-object v0, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    array-length v0, v0

    if-ge v1, v0, :cond_c

    iget-object v2, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    const-wide/16 v3, -0x1

    invoke-virtual {p1}, Lmaps/l/d;->o()Lmaps/k/ab;

    move-result-object v0

    invoke-interface {v0}, Lmaps/k/ab;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/c;

    invoke-direct {p0, v6, v3, v4, v0}, Lmaps/l/m;->a(IJLmaps/l/c;)Lmaps/l/an;

    move-result-object v0

    aput-object v0, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_c
    return-void

    :cond_d
    move-wide v1, v0

    goto :goto_8

    :cond_e
    move v0, v4

    goto :goto_5
.end method

.method private a(I)Lmaps/l/an;
    .locals 3

    iget-object v0, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    iget v1, p0, Lmaps/l/m;->p:I

    ushr-int v1, p1, v1

    iget v2, p0, Lmaps/l/m;->o:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method private a(IJLmaps/l/c;)Lmaps/l/an;
    .locals 6

    new-instance v0, Lmaps/l/an;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lmaps/l/an;-><init>(Lmaps/l/m;IJLmaps/l/c;)V

    return-object v0
.end method

.method static a(Lmaps/l/am;Lmaps/l/am;)V
    .locals 0

    invoke-interface {p0, p1}, Lmaps/l/am;->a(Lmaps/l/am;)V

    invoke-interface {p1, p0}, Lmaps/l/am;->b(Lmaps/l/am;)V

    return-void
.end method

.method private b(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lmaps/l/m;->d:Lmaps/k/b;

    invoke-virtual {v0, p1}, Lmaps/k/b;->a(Ljava/lang/Object;)I

    move-result v0

    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method static b(Lmaps/l/am;)V
    .locals 1

    sget-object v0, Lmaps/l/al;->a:Lmaps/l/al;

    invoke-interface {p0, v0}, Lmaps/l/am;->a(Lmaps/l/am;)V

    invoke-interface {p0, v0}, Lmaps/l/am;->b(Lmaps/l/am;)V

    return-void
.end method

.method static b(Lmaps/l/am;Lmaps/l/am;)V
    .locals 0

    invoke-interface {p0, p1}, Lmaps/l/am;->c(Lmaps/l/am;)V

    invoke-interface {p1, p0}, Lmaps/l/am;->d(Lmaps/l/am;)V

    return-void
.end method

.method static c(Lmaps/l/am;)V
    .locals 1

    sget-object v0, Lmaps/l/al;->a:Lmaps/l/al;

    invoke-interface {p0, v0}, Lmaps/l/am;->c(Lmaps/l/am;)V

    invoke-interface {p0, v0}, Lmaps/l/am;->d(Lmaps/l/am;)V

    return-void
.end method

.method static j()Lmaps/l/ba;
    .locals 1

    sget-object v0, Lmaps/l/m;->m:Lmaps/l/ba;

    return-object v0
.end method

.method static k()Lmaps/l/am;
    .locals 1

    sget-object v0, Lmaps/l/al;->a:Lmaps/l/al;

    return-object v0
.end method

.method static l()Ljava/util/Queue;
    .locals 1

    sget-object v0, Lmaps/l/m;->n:Ljava/util/Queue;

    return-object v0
.end method

.method private n()Z
    .locals 4

    iget-wide v0, p0, Lmaps/l/m;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Z
    .locals 4

    iget-wide v0, p0, Lmaps/l/m;->t:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lmaps/l/m;->l:Lmaps/l/j;

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v1

    invoke-direct {p0, v1}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v0}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/j;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final a(Lmaps/l/am;J)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Lmaps/l/am;->a()Lmaps/l/ba;

    move-result-object v1

    invoke-interface {v1}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lmaps/l/m;->b(Lmaps/l/am;J)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method final a(Lmaps/l/am;)V
    .locals 2

    invoke-interface {p1}, Lmaps/l/am;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lmaps/l/an;->a(Lmaps/l/am;I)Z

    return-void
.end method

.method final a(Lmaps/l/ba;)V
    .locals 3

    invoke-interface {p1}, Lmaps/l/ba;->f()Lmaps/l/am;

    move-result-object v0

    invoke-interface {v0}, Lmaps/l/am;->c()I

    move-result v1

    invoke-direct {p0, v1}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v2

    invoke-interface {v0}, Lmaps/l/am;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lmaps/l/an;->a(Ljava/lang/Object;ILmaps/l/ba;)Z

    return-void
.end method

.method final a()Z
    .locals 4

    iget-wide v0, p0, Lmaps/l/m;->s:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()Z
    .locals 2

    iget-object v0, p0, Lmaps/l/m;->g:Lmaps/l/ci;

    sget-object v1, Lmaps/l/i;->a:Lmaps/l/i;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Lmaps/l/am;J)Z
    .locals 5

    const/4 v0, 0x1

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lmaps/l/m;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Lmaps/l/am;->e()J

    move-result-wide v1

    sub-long v1, p2, v1

    iget-wide v3, p0, Lmaps/l/m;->t:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lmaps/l/m;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Lmaps/l/am;->h()J

    move-result-wide v1

    sub-long v1, p2, v1

    iget-wide v3, p0, Lmaps/l/m;->u:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Z
    .locals 4

    iget-wide v0, p0, Lmaps/l/m;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 4

    iget-object v1, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lmaps/l/an;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lmaps/l/an;->b(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 19

    if-nez p1, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/l/m;->j:Lmaps/k/ae;

    invoke-virtual {v3}, Lmaps/k/ae;->a()J

    move-result-wide v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/l/m;->c:[Lmaps/l/an;

    const-wide/16 v6, -0x1

    const/4 v3, 0x0

    move v8, v3

    move-wide v9, v6

    :goto_1
    const/4 v3, 0x3

    if-ge v8, v3, :cond_5

    const-wide/16 v4, 0x0

    array-length v14, v13

    const/4 v3, 0x0

    move-wide v6, v4

    move v5, v3

    :goto_2
    if-ge v5, v14, :cond_4

    aget-object v15, v13, v5

    iget v3, v15, Lmaps/l/an;->a:I

    iget-object v0, v15, Lmaps/l/an;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-object/from16 v16, v0

    const/4 v3, 0x0

    move v4, v3

    :goto_3
    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v3

    if-ge v4, v3, :cond_3

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/l/am;

    :goto_4
    if-eqz v3, :cond_2

    invoke-virtual {v15, v3, v11, v12}, Lmaps/l/an;->a(Lmaps/l/am;J)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/l/m;->e:Lmaps/k/b;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lmaps/k/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Lmaps/l/am;->b()Lmaps/l/am;

    move-result-object v3

    goto :goto_4

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    :cond_3
    iget v3, v15, Lmaps/l/an;->b:I

    int-to-long v3, v3

    add-long/2addr v6, v3

    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    :cond_4
    cmp-long v3, v6, v9

    if-eqz v3, :cond_5

    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move-wide v9, v6

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method

.method final d()Z
    .locals 1

    invoke-direct {p0}, Lmaps/l/m;->o()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/l/m;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()Z
    .locals 1

    invoke-direct {p0}, Lmaps/l/m;->n()Z

    move-result v0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/l/m;->y:Ljava/util/Set;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/l/ae;

    invoke-direct {v0, p0, p0}, Lmaps/l/ae;-><init>(Lmaps/l/m;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lmaps/l/m;->y:Ljava/util/Set;

    goto :goto_0
.end method

.method final f()Z
    .locals 1

    invoke-direct {p0}, Lmaps/l/m;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/l/m;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final g()Z
    .locals 1

    invoke-direct {p0}, Lmaps/l/m;->o()Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lmaps/l/an;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method final h()Z
    .locals 2

    iget-object v0, p0, Lmaps/l/m;->r:Lmaps/l/aq;

    sget-object v1, Lmaps/l/aq;->a:Lmaps/l/aq;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final i()Z
    .locals 2

    iget-object v0, p0, Lmaps/l/m;->f:Lmaps/l/aq;

    sget-object v1, Lmaps/l/aq;->a:Lmaps/l/aq;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 9

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    iget-object v6, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    move v0, v1

    move-wide v2, v4

    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    aget-object v7, v6, v0

    iget v7, v7, Lmaps/l/an;->a:I

    if-eqz v7, :cond_1

    :cond_0
    :goto_1
    return v1

    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lmaps/l/an;->b:I

    int-to-long v7, v7

    add-long/2addr v2, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    aget-object v7, v6, v0

    iget v7, v7, Lmaps/l/an;->a:I

    if-nez v7, :cond_0

    aget-object v7, v6, v0

    iget v7, v7, Lmaps/l/an;->b:I

    int-to-long v7, v7

    sub-long/2addr v2, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/l/m;->w:Ljava/util/Set;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/l/ah;

    invoke-direct {v0, p0, p0}, Lmaps/l/ah;-><init>(Lmaps/l/m;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lmaps/l/m;->w:Ljava/util/Set;

    goto :goto_0
.end method

.method final m()V
    .locals 4

    :goto_0
    iget-object v0, p0, Lmaps/l/m;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/cb;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/l/m;->v:Lmaps/l/ca;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lmaps/l/m;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lmaps/l/an;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lmaps/l/m;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lmaps/l/an;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lmaps/l/an;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lmaps/l/an;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lmaps/l/an;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lmaps/l/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/l/m;->a(I)Lmaps/l/an;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lmaps/l/an;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 6

    iget-object v3, p0, Lmaps/l/m;->c:[Lmaps/l/an;

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_0

    aget-object v4, v3, v2

    iget v4, v4, Lmaps/l/an;->a:I

    int-to-long v4, v4

    add-long/2addr v0, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v0, v1}, Lmaps/p/a;->a(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lmaps/l/m;->x:Ljava/util/Collection;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/l/bb;

    invoke-direct {v0, p0, p0}, Lmaps/l/bb;-><init>(Lmaps/l/m;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lmaps/l/m;->x:Ljava/util/Collection;

    goto :goto_0
.end method
