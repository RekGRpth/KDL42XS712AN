.class final Lmaps/l/bk;
.super Ljava/util/AbstractQueue;


# instance fields
.field final a:Lmaps/l/am;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    new-instance v0, Lmaps/l/bl;

    invoke-direct {v0}, Lmaps/l/bl;-><init>()V

    iput-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    return-void
.end method

.method private a()Lmaps/l/am;
    .locals 2

    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    if-eq v0, v1, :cond_0

    invoke-interface {v0}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v1

    invoke-static {v0}, Lmaps/l/m;->c(Lmaps/l/am;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    iget-object v1, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-interface {v0, v1}, Lmaps/l/am;->c(Lmaps/l/am;)V

    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    iget-object v1, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-interface {v0, v1}, Lmaps/l/am;->d(Lmaps/l/am;)V

    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lmaps/l/am;

    invoke-interface {p1}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v0

    sget-object v1, Lmaps/l/al;->a:Lmaps/l/al;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lmaps/l/bm;

    invoke-direct {p0}, Lmaps/l/bk;->a()Lmaps/l/am;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lmaps/l/bm;-><init>(Lmaps/l/bk;Lmaps/l/am;)V

    return-object v0
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lmaps/l/am;

    invoke-interface {p1}, Lmaps/l/am;->j()Lmaps/l/am;

    move-result-object v0

    invoke-interface {p1}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/l/m;->b(Lmaps/l/am;Lmaps/l/am;)V

    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->j()Lmaps/l/am;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/l/m;->b(Lmaps/l/am;Lmaps/l/am;)V

    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-static {p1, v0}, Lmaps/l/m;->b(Lmaps/l/am;Lmaps/l/am;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lmaps/l/bk;->a()Lmaps/l/am;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v0

    iget-object v1, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lmaps/l/bk;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lmaps/l/am;

    invoke-interface {p1}, Lmaps/l/am;->j()Lmaps/l/am;

    move-result-object v0

    invoke-interface {p1}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/l/m;->b(Lmaps/l/am;Lmaps/l/am;)V

    invoke-static {p1}, Lmaps/l/m;->c(Lmaps/l/am;)V

    sget-object v0, Lmaps/l/al;->a:Lmaps/l/al;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    invoke-interface {v0}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lmaps/l/bk;->a:Lmaps/l/am;

    if-eq v0, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0}, Lmaps/l/am;->i()Lmaps/l/am;

    move-result-object v0

    goto :goto_0

    :cond_0
    return v1
.end method
