.class public abstract enum Lmaps/l/bu;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/l/bu;

.field public static final enum b:Lmaps/l/bu;

.field public static final enum c:Lmaps/l/bu;

.field public static final enum d:Lmaps/l/bu;

.field public static final enum e:Lmaps/l/bu;

.field private static final synthetic f:[Lmaps/l/bu;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/l/bv;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1}, Lmaps/l/bv;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/bu;->a:Lmaps/l/bu;

    new-instance v0, Lmaps/l/bw;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1}, Lmaps/l/bw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/bu;->b:Lmaps/l/bu;

    new-instance v0, Lmaps/l/bx;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1}, Lmaps/l/bx;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/bu;->c:Lmaps/l/bu;

    new-instance v0, Lmaps/l/by;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1}, Lmaps/l/by;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/bu;->d:Lmaps/l/bu;

    new-instance v0, Lmaps/l/bz;

    const-string v1, "SIZE"

    invoke-direct {v0, v1}, Lmaps/l/bz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/l/bu;->e:Lmaps/l/bu;

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/l/bu;

    const/4 v1, 0x0

    sget-object v2, Lmaps/l/bu;->a:Lmaps/l/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lmaps/l/bu;->b:Lmaps/l/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lmaps/l/bu;->c:Lmaps/l/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lmaps/l/bu;->d:Lmaps/l/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lmaps/l/bu;->e:Lmaps/l/bu;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/l/bu;->f:[Lmaps/l/bu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/l/bu;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/l/bu;
    .locals 1

    const-class v0, Lmaps/l/bu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/l/bu;

    return-object v0
.end method

.method public static values()[Lmaps/l/bu;
    .locals 1

    sget-object v0, Lmaps/l/bu;->f:[Lmaps/l/bu;

    invoke-virtual {v0}, [Lmaps/l/bu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/l/bu;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
