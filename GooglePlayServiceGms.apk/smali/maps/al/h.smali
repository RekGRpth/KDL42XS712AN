.class public final Lmaps/al/h;
.super Ljava/lang/Object;


# static fields
.field private static final a:F

.field private static final t:Ljava/lang/ThreadLocal;

.field private static final u:[I

.field private static final v:[I

.field private static final w:[I

.field private static final x:[I

.field private static final y:[[I


# instance fields
.field private final b:Lmaps/ac/av;

.field private final c:Lmaps/ac/av;

.field private final d:Lmaps/ac/av;

.field private final e:Lmaps/ac/av;

.field private final f:Lmaps/ac/av;

.field private final g:Lmaps/ac/av;

.field private final h:Lmaps/ac/av;

.field private final i:Lmaps/ac/av;

.field private final j:Lmaps/ac/av;

.field private final k:Lmaps/ac/av;

.field private final l:Lmaps/ac/cm;

.field private final m:Lmaps/ac/cm;

.field private final n:Lmaps/ac/cm;

.field private final o:Lmaps/ac/cm;

.field private final p:Lmaps/ac/cm;

.field private final q:Lmaps/ac/cm;

.field private final r:Lmaps/ac/cm;

.field private final s:Lmaps/ac/cm;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v2, 0x8

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lmaps/al/h;->a:F

    new-instance v0, Lmaps/al/i;

    invoke-direct {v0}, Lmaps/al/i;-><init>()V

    sput-object v0, Lmaps/al/h;->t:Ljava/lang/ThreadLocal;

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/al/h;->u:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lmaps/al/h;->v:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lmaps/al/h;->w:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lmaps/al/h;->x:[I

    const/16 v0, 0x10

    new-array v0, v0, [[I

    sput-object v0, Lmaps/al/h;->y:[[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x10000
        0x10000
        0x10000
        0x0
        0x8000
        0x10000
        0x8000
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x8000
        0x10000
        0x8000
        0x0
        0x10000
        0x10000
        0x10000
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x4000
        0x10000
        0x4000
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x4000
        0x10000
        0x4000
        0x8000
        0x4000
        0x0
        0x4000
        0x10000
        0x4000
    .end array-data
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->b:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->c:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->d:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->e:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->f:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->g:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->h:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->i:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->j:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->k:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/cm;

    invoke-direct {v0}, Lmaps/ac/cm;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->l:Lmaps/ac/cm;

    new-instance v0, Lmaps/ac/cm;

    invoke-direct {v0}, Lmaps/ac/cm;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->m:Lmaps/ac/cm;

    new-instance v0, Lmaps/ac/cm;

    invoke-direct {v0}, Lmaps/ac/cm;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->n:Lmaps/ac/cm;

    new-instance v0, Lmaps/ac/cm;

    invoke-direct {v0}, Lmaps/ac/cm;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->o:Lmaps/ac/cm;

    new-instance v0, Lmaps/ac/cm;

    invoke-direct {v0}, Lmaps/ac/cm;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->p:Lmaps/ac/cm;

    new-instance v0, Lmaps/ac/cm;

    invoke-direct {v0}, Lmaps/ac/cm;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->q:Lmaps/ac/cm;

    new-instance v0, Lmaps/ac/cm;

    invoke-direct {v0}, Lmaps/ac/cm;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->r:Lmaps/ac/cm;

    new-instance v0, Lmaps/ac/cm;

    invoke-direct {v0}, Lmaps/ac/cm;-><init>()V

    iput-object v0, p0, Lmaps/al/h;->s:Lmaps/ac/cm;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lmaps/al/h;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)I
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static a(Lmaps/ac/az;)I
    .locals 2

    invoke-virtual {p0}, Lmaps/ac/az;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x8

    goto :goto_0
.end method

.method public static a()Lmaps/al/h;
    .locals 1

    sget-object v0, Lmaps/al/h;->t:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/h;

    return-object v0
.end method

.method public static a(II[ILmaps/at/j;)V
    .locals 11

    const v10, 0x8000

    const/16 v9, 0xa

    const/high16 v8, 0x10000

    const/4 v3, 0x1

    const/4 v1, 0x0

    add-int/lit8 v0, p0, -0x1

    mul-int/lit8 v0, v0, 0x5

    invoke-interface {p3}, Lmaps/at/j;->g()I

    move-result v2

    add-int/2addr v0, v2

    invoke-interface {p3, v0}, Lmaps/at/j;->c(I)V

    sget-object v0, Lmaps/al/h;->y:[[I

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    sget-object v4, Lmaps/al/h;->y:[[I

    shl-int v5, v3, p1

    mul-int/lit8 v0, v5, 0x5

    mul-int/lit8 v0, v0, 0x2

    new-array v6, v0, [I

    div-int v0, v10, v5

    move v2, v0

    move v0, v1

    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_0

    aput v1, v6, v0

    add-int/lit8 v7, v0, 0x1

    aput v2, v6, v7

    add-int/lit8 v7, v0, 0x2

    aput v8, v6, v7

    add-int/lit8 v7, v0, 0x3

    aput v2, v6, v7

    add-int/lit8 v7, v0, 0x4

    aput v8, v6, v7

    add-int/lit8 v7, v0, 0x5

    aput v2, v6, v7

    add-int/lit8 v7, v0, 0x6

    aput v1, v6, v7

    add-int/lit8 v7, v0, 0x7

    aput v2, v6, v7

    add-int/lit8 v7, v0, 0x8

    aput v10, v6, v7

    add-int/lit8 v7, v0, 0x9

    aput v2, v6, v7

    div-int v7, v8, v5

    add-int/2addr v2, v7

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    :cond_0
    aput-object v6, v4, p1

    :cond_1
    sget-object v0, Lmaps/al/h;->y:[[I

    aget-object v2, v0, p1

    if-eqz p2, :cond_2

    array-length v0, p2

    if-ne v0, v3, :cond_4

    :cond_2
    if-nez p2, :cond_3

    :goto_1
    if-ge v3, p0, :cond_5

    mul-int/lit8 v0, v1, 0x5

    mul-int/lit8 v0, v0, 0x2

    invoke-interface {p3, v2, v0, v9}, Lmaps/at/j;->a([III)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    aget v1, p2, v1

    goto :goto_1

    :cond_4
    move v0, v3

    :goto_2
    if-ge v0, p0, :cond_5

    aget v1, p2, v0

    mul-int/lit8 v1, v1, 0x5

    mul-int/lit8 v1, v1, 0x2

    invoke-interface {p3, v2, v1, v9}, Lmaps/at/j;->a([III)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    return-void
.end method

.method public static b(Ljava/util/List;)I
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0}, Lmaps/ac/az;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v2, 0x1

    :goto_1
    mul-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x6

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public static b(Lmaps/ac/az;)I
    .locals 2

    invoke-virtual {p0}, Lmaps/ac/az;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0xc

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v1, v0, 0x2

    mul-int/lit8 v1, v1, 0x6

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lmaps/ac/az;FLmaps/ac/av;ILmaps/at/o;Lmaps/at/j;Lmaps/at/e;ZZ)I
    .locals 22

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, p2, v2

    if-gez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmaps/ac/az;->b()I

    move-result v10

    const/4 v2, 0x2

    if-ne v10, v2, :cond_5

    invoke-interface/range {p5 .. p5}, Lmaps/at/o;->a()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/al/h;->b:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/al/h;->c:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/al/h;->d:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/al/h;->e:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/al/h;->f:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/al/h;->g:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/al/h;->h:Lmaps/ac/av;

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v10, v1, v3}, Lmaps/ac/az;->a(ILmaps/ac/av;Lmaps/ac/av;)V

    const/4 v10, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v10, v1, v4}, Lmaps/ac/az;->a(ILmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v4, v3, v5}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move/from16 v0, p2

    invoke-static {v5, v0, v6}, Lmaps/ac/ax;->a(Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-static {v6, v7}, Lmaps/ac/ax;->a(Lmaps/ac/av;Lmaps/ac/av;)V

    if-eqz p8, :cond_1

    invoke-static {v3, v7, v3}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    :cond_1
    if-eqz p9, :cond_2

    invoke-static {v4, v7, v4}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    :cond_2
    invoke-static {v3, v6, v9}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v7, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v9, v1, v7}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    invoke-static {v3, v6, v9}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v7, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v9, v1, v7}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    invoke-static {v3, v4, v8}, Lmaps/ac/ax;->e(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v8, v6, v9}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v9, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    invoke-static {v8, v6, v9}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v9, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    invoke-static {v4, v6, v9}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v9, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    invoke-static {v4, v6, v9}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v9, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    const/high16 v3, -0x41800000    # -0.25f

    invoke-virtual {v5}, Lmaps/ac/av;->i()F

    move-result v4

    div-float v4, v4, p2

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    const/high16 v4, 0x47800000    # 65536.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    if-eqz p8, :cond_3

    const/4 v4, 0x0

    const/high16 v5, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, Lmaps/at/j;->a(II)V

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, Lmaps/at/j;->a(II)V

    :goto_1
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/at/j;->a(II)V

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/at/j;->a(II)V

    if-eqz p9, :cond_4

    const/4 v3, 0x0

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v4}, Lmaps/at/j;->a(II)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v4}, Lmaps/at/j;->a(II)V

    :goto_2
    add-int/lit8 v3, v2, 0x1

    add-int/lit8 v4, v2, 0x2

    add-int/lit8 v5, v2, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v3, v4, v5}, Lmaps/at/e;->a(IIII)V

    add-int/lit8 v3, v2, 0x2

    add-int/lit8 v4, v2, 0x3

    add-int/lit8 v5, v2, 0x4

    add-int/lit8 v2, v2, 0x5

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v4, v5, v2}, Lmaps/at/e;->a(IIII)V

    const/4 v2, 0x6

    goto/16 :goto_0

    :cond_3
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/at/j;->a(II)V

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/at/j;->a(II)V

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/at/j;->a(II)V

    const/high16 v4, 0x10000

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, Lmaps/at/j;->a(II)V

    goto :goto_2

    :cond_5
    const/4 v2, 0x2

    if-ge v10, v2, :cond_6

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/al/h;->b:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/al/h;->c:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/al/h;->d:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/al/h;->e:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/al/h;->f:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->g:Lmaps/ac/av;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->h:Lmaps/ac/av;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/al/h;->i:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->j:Lmaps/ac/av;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/al/h;->k:Lmaps/ac/av;

    invoke-interface/range {p5 .. p5}, Lmaps/at/o;->a()I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1, v11}, Lmaps/ac/az;->a(ILmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1, v12}, Lmaps/ac/az;->a(ILmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v12, v11, v14}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move/from16 v0, p2

    move-object/from16 v1, v16

    invoke-static {v14, v0, v1}, Lmaps/ac/ax;->a(Lmaps/ac/av;FLmaps/ac/av;)V

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lmaps/ac/ax;->a(Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v11, v5, v11}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, v16

    invoke-static {v11, v0, v4}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    move-object/from16 v0, v16

    invoke-static {v11, v0, v4}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    invoke-static {v11, v5, v11}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, v16

    invoke-static {v11, v0, v4}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    move-object/from16 v0, v16

    invoke-static {v11, v0, v4}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    sget-object v3, Lmaps/al/h;->u:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Lmaps/at/j;->a([I)V

    const/4 v8, 0x4

    if-eqz p8, :cond_7

    add-int/lit8 v3, v2, 0x1

    add-int/lit8 v6, v2, 0x2

    add-int/lit8 v7, v2, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v3, v6, v7}, Lmaps/at/e;->a(IIII)V

    :goto_3
    add-int/lit8 v3, v2, 0x2

    add-int/lit8 v6, v2, 0x3

    add-int/lit8 v7, v2, 0x4

    add-int/lit8 v9, v2, 0x5

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v6, v7, v9}, Lmaps/at/e;->a(IIII)V

    add-int/lit8 v7, v2, 0x4

    mul-float v19, p2, p2

    const/4 v2, 0x1

    move v9, v2

    :goto_4
    add-int/lit8 v2, v10, -0x1

    if-ge v9, v2, :cond_b

    add-int/lit8 v2, v9, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1, v13}, Lmaps/ac/az;->a(ILmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v13, v12, v15}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move/from16 v0, p2

    move-object/from16 v1, v17

    invoke-static {v15, v0, v1}, Lmaps/ac/ax;->a(Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-static {v14, v15}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;)J

    move-result-wide v2

    const-wide/16 v20, 0x0

    cmp-long v2, v2, v20

    if-lez v2, :cond_8

    const/4 v2, 0x1

    :goto_5
    const/4 v6, 0x1

    invoke-static/range {v16 .. v18}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static/range {v17 .. v18}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;)F

    move-result v3

    const/high16 v20, 0x3f800000    # 1.0f

    cmpl-float v20, v3, v20

    if-lez v20, :cond_e

    invoke-static {v14, v15}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;)F

    move-result v20

    const/16 v21, 0x0

    cmpl-float v20, v20, v21

    if-ltz v20, :cond_e

    div-float v3, v19, v3

    move-object/from16 v0, v18

    move-object/from16 v1, v18

    invoke-static {v0, v3, v1}, Lmaps/ac/av;->a(Lmaps/ac/av;FLmaps/ac/av;)V

    move-object/from16 v0, v18

    invoke-static {v12, v0, v4}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, v18

    invoke-static {v12, v0, v5}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    if-eqz v2, :cond_9

    move-object v3, v4

    :goto_6
    invoke-static {v12, v11, v3}, Lmaps/ac/av;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)F

    move-result v20

    const/high16 v21, 0x3f000000    # 0.5f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_e

    invoke-static {v12, v13, v3}, Lmaps/ac/av;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)F

    move-result v3

    const/high16 v20, 0x3f000000    # 0.5f

    cmpg-float v3, v3, v20

    if-gez v3, :cond_e

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v5, v1, v3}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    add-int/lit8 v8, v8, 0x2

    sget-object v3, Lmaps/al/h;->w:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Lmaps/at/j;->a([I)V

    add-int/lit8 v3, v7, 0x1

    add-int/lit8 v6, v7, 0x2

    add-int/lit8 v20, v7, 0x3

    move-object/from16 v0, p7

    move/from16 v1, v20

    invoke-interface {v0, v7, v3, v6, v1}, Lmaps/at/e;->a(IIII)V

    add-int/lit8 v6, v7, 0x2

    const/4 v3, 0x0

    move v7, v3

    move v3, v6

    move v6, v8

    :goto_7
    if-eqz v7, :cond_d

    move-object/from16 v0, v16

    invoke-static {v12, v0, v4}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v7, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v7}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    move-object/from16 v0, v16

    invoke-static {v12, v0, v4}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v7, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v7}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    const/4 v7, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v12, v1, v7}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    move-object/from16 v0, v17

    invoke-static {v12, v0, v4}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v7, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v7}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    move-object/from16 v0, v17

    invoke-static {v12, v0, v4}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v7, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v7}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    sget-object v7, Lmaps/al/h;->x:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v7}, Lmaps/at/j;->a([I)V

    add-int/lit8 v6, v6, 0x5

    if-eqz v2, :cond_a

    add-int/lit8 v2, v3, 0x2

    add-int/lit8 v7, v3, 0x1

    add-int/lit8 v8, v3, 0x4

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v7, v8}, Lmaps/at/e;->a(III)V

    :goto_8
    add-int/lit8 v2, v3, 0x3

    add-int/lit8 v7, v3, 0x4

    add-int/lit8 v8, v3, 0x5

    add-int/lit8 v20, v3, 0x6

    move-object/from16 v0, p7

    move/from16 v1, v20

    invoke-interface {v0, v2, v7, v8, v1}, Lmaps/at/e;->a(IIII)V

    add-int/lit8 v2, v3, 0x5

    move v3, v6

    :goto_9
    invoke-virtual/range {v16 .. v17}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    invoke-virtual {v14, v15}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    invoke-virtual {v11, v12}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    invoke-virtual {v12, v13}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    add-int/lit8 v6, v9, 0x1

    move v9, v6

    move v7, v2

    move v8, v3

    goto/16 :goto_4

    :cond_7
    add-int/lit8 v3, v2, 0x2

    add-int/lit8 v6, v2, 0x2

    add-int/lit8 v7, v2, 0x2

    add-int/lit8 v9, v2, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v6, v7, v9}, Lmaps/at/e;->a(IIII)V

    goto/16 :goto_3

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_9
    move-object v3, v5

    goto/16 :goto_6

    :cond_a
    add-int/lit8 v2, v3, 0x0

    add-int/lit8 v7, v3, 0x2

    add-int/lit8 v8, v3, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v7, v8}, Lmaps/at/e;->a(III)V

    goto :goto_8

    :cond_b
    move-object/from16 v0, v17

    invoke-static {v13, v0, v4}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    move-object/from16 v0, v17

    invoke-static {v13, v0, v4}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Lmaps/ac/ax;->a(Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v13, v5, v13}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, v17

    invoke-static {v13, v0, v4}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    move-object/from16 v0, v17

    invoke-static {v13, v0, v4}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-interface {v0, v4, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;IB)V

    sget-object v2, Lmaps/al/h;->v:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Lmaps/at/j;->a([I)V

    add-int/lit8 v2, v8, 0x4

    if-eqz p9, :cond_c

    add-int/lit8 v3, v7, 0x1

    add-int/lit8 v4, v7, 0x2

    add-int/lit8 v5, v7, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v7, v3, v4, v5}, Lmaps/at/e;->a(IIII)V

    goto/16 :goto_0

    :cond_c
    move-object/from16 v0, p7

    invoke-interface {v0, v7, v7, v7, v7}, Lmaps/at/e;->a(IIII)V

    goto/16 :goto_0

    :cond_d
    move v2, v3

    move v3, v6

    goto/16 :goto_9

    :cond_e
    move v3, v7

    move v7, v6

    move v6, v8

    goto/16 :goto_7
.end method

.method public final a(Lmaps/ac/az;FFLmaps/ac/av;IIILmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V
    .locals 36

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/az;->b()I

    move-result v18

    const/4 v3, 0x1

    move/from16 v0, v18

    if-gt v0, v3, :cond_1

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v19, v18, -0x1

    invoke-interface/range {p8 .. p8}, Lmaps/at/o;->a()I

    move-result v7

    mul-int/lit8 v3, v18, 0x5

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/az;->e()Z

    move-result v20

    add-int v4, v7, v3

    move-object/from16 v0, p8

    invoke-interface {v0, v4}, Lmaps/at/o;->a(I)V

    if-eqz p10, :cond_2

    invoke-interface/range {p10 .. p10}, Lmaps/at/j;->g()I

    move-result v4

    add-int/2addr v3, v4

    move-object/from16 v0, p10

    invoke-interface {v0, v3}, Lmaps/at/j;->c(I)V

    :cond_2
    invoke-interface/range {p9 .. p9}, Lmaps/at/e;->b()I

    move-result v3

    mul-int/lit8 v4, v19, 0x3

    mul-int/lit8 v4, v4, 0x6

    add-int/2addr v3, v4

    move-object/from16 v0, p9

    invoke-interface {v0, v3}, Lmaps/at/e;->b(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->b:Lmaps/ac/av;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->c:Lmaps/ac/av;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->d:Lmaps/ac/av;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->e:Lmaps/ac/av;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->f:Lmaps/ac/av;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->g:Lmaps/ac/av;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->h:Lmaps/ac/av;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->l:Lmaps/ac/cm;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->m:Lmaps/ac/cm;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->n:Lmaps/ac/cm;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->o:Lmaps/ac/cm;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->p:Lmaps/ac/cm;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/al/h;->q:Lmaps/ac/cm;

    move-object/from16 v33, v0

    const/4 v6, -0x1

    const/4 v5, -0x1

    const/4 v9, -0x1

    const/4 v4, -0x1

    const/4 v10, -0x1

    add-float v3, p2, p3

    move/from16 v0, p6

    int-to-float v8, v0

    mul-float v8, v8, p3

    move/from16 v0, p7

    int-to-float v11, v0

    mul-float v11, v11, p2

    add-float/2addr v8, v11

    div-float v3, v8, v3

    float-to-int v0, v3

    move/from16 v34, v0

    const/4 v3, 0x0

    move v11, v3

    move v12, v10

    move v13, v4

    move v14, v9

    move v4, v7

    :goto_0
    move/from16 v0, v18

    if-ge v11, v0, :cond_0

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v22

    invoke-virtual {v0, v11, v1, v2}, Lmaps/ac/az;->a(ILmaps/ac/av;Lmaps/ac/av;)V

    add-int/lit8 v7, v11, -0x1

    add-int/lit8 v3, v11, 0x1

    if-eqz v20, :cond_14

    if-gez v7, :cond_3

    add-int/lit8 v7, v18, -0x2

    :cond_3
    move/from16 v0, v18

    if-lt v3, v0, :cond_14

    const/4 v3, 0x1

    move/from16 v35, v3

    move v3, v7

    move/from16 v7, v35

    :goto_1
    if-ltz v3, :cond_8

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v21

    invoke-virtual {v0, v3, v1, v2}, Lmaps/ac/az;->a(ILmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/al/h;->r:Lmaps/ac/cm;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/cm;

    move-result-object v3

    move-object v8, v3

    :goto_2
    move/from16 v0, v18

    if-ge v7, v0, :cond_9

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v23

    invoke-virtual {v0, v7, v1, v2}, Lmaps/ac/az;->a(ILmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/al/h;->s:Lmaps/ac/cm;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v3, v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/cm;

    move-result-object v3

    move-object v7, v3

    :goto_3
    const/4 v3, 0x1

    if-eqz v8, :cond_b

    if-eqz v7, :cond_b

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v9

    move-object/from16 v0, v29

    invoke-virtual {v9, v0}, Lmaps/ac/cm;->b(Lmaps/ac/cm;)Lmaps/ac/cm;

    invoke-virtual/range {v30 .. v30}, Lmaps/ac/cm;->e()Z

    move-result v9

    if-eqz v9, :cond_a

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cm;->d()Lmaps/ac/cm;

    move/from16 v17, v3

    :goto_4
    if-eqz v17, :cond_d

    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v3

    move/from16 v0, p2

    neg-float v7, v0

    invoke-virtual {v3, v7}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v25

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    if-eqz p10, :cond_4

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    :cond_4
    add-int/lit8 v3, v4, 0x1

    add-int/lit8 v7, v3, 0x1

    move v8, v4

    move v9, v3

    move v10, v7

    move v7, v6

    move v6, v5

    move v5, v3

    move v3, v4

    :goto_5
    add-int/lit8 v15, v10, 0x1

    move-object/from16 v0, p8

    move-object/from16 v1, v22

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    if-eqz p10, :cond_5

    const/16 v16, 0x0

    move-object/from16 v0, p10

    move/from16 v1, v34

    move/from16 v2, v16

    invoke-interface {v0, v1, v2}, Lmaps/at/j;->a(II)V

    :cond_5
    if-eqz v20, :cond_13

    move/from16 v0, v19

    if-ne v11, v0, :cond_13

    const/16 v16, 0x1

    :goto_6
    if-nez v17, :cond_6

    if-nez v16, :cond_6

    move-object/from16 v0, p9

    invoke-interface {v0, v7, v10, v3}, Lmaps/at/e;->a(III)V

    move-object/from16 v0, p9

    invoke-interface {v0, v10, v6, v3}, Lmaps/at/e;->a(III)V

    :cond_6
    if-lez v11, :cond_7

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v12, v10}, Lmaps/at/e;->a(III)V

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v14, v10}, Lmaps/at/e;->a(III)V

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v10, v8}, Lmaps/at/e;->a(III)V

    move-object/from16 v0, p9

    invoke-interface {v0, v10, v14, v5}, Lmaps/at/e;->a(III)V

    :cond_7
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    move v12, v10

    move v13, v4

    move v14, v9

    move v5, v6

    move v6, v7

    move v4, v15

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    move-object v8, v3

    goto/16 :goto_2

    :cond_9
    const/4 v3, 0x0

    move-object v7, v3

    goto/16 :goto_3

    :cond_a
    const/4 v3, 0x0

    move/from16 v17, v3

    goto/16 :goto_4

    :cond_b
    if-eqz v8, :cond_c

    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cm;->d()Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move/from16 v17, v3

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cm;->d()Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cm;->a()Lmaps/ac/cm;

    move/from16 v17, v3

    goto/16 :goto_4

    :cond_d
    invoke-virtual/range {v30 .. v30}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    invoke-virtual {v8, v7}, Lmaps/ac/cm;->d(Lmaps/ac/cm;)Z

    move-result v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->d()Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    invoke-virtual/range {v30 .. v31}, Lmaps/ac/cm;->c(Lmaps/ac/cm;)F

    move-result v6

    if-eqz v5, :cond_10

    move/from16 v3, p2

    :goto_7
    neg-float v9, v6

    div-float/2addr v3, v9

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v9

    invoke-virtual {v9, v3}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    invoke-virtual {v8}, Lmaps/ac/cm;->b()F

    move-result v9

    invoke-virtual {v7}, Lmaps/ac/cm;->b()F

    move-result v10

    move-object/from16 v0, v33

    invoke-virtual {v0, v8}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v15

    invoke-virtual {v15}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-result-object v15

    move-object/from16 v0, v32

    invoke-virtual {v0, v15}, Lmaps/ac/cm;->c(Lmaps/ac/cm;)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-result-object v16

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->c(Lmaps/ac/cm;)F

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v16

    div-float/2addr v9, v15

    div-float v10, v10, v16

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    const/high16 v10, 0x3f800000    # 1.0f

    cmpg-float v10, v9, v10

    if-gez v10, :cond_e

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v6, v9}, Ljava/lang/Math;->max(FF)F

    move-result v6

    mul-float/2addr v3, v6

    :cond_e
    if-eqz v5, :cond_11

    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v5

    move/from16 v0, p3

    neg-float v6, v0

    invoke-virtual {v5, v6}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v5

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-static {v0, v5, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v5

    neg-float v3, v3

    invoke-virtual {v5, v3}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->d()Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-result-object v3

    move/from16 v0, p3

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, v33

    invoke-virtual {v0, v8}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->d()Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v25

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v26

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v27

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    if-eqz p10, :cond_f

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    :cond_f
    add-int/lit8 v3, v4, 0x1

    add-int/lit8 v6, v3, 0x1

    add-int/lit8 v5, v6, 0x1

    add-int/lit8 v7, v5, 0x1

    move v8, v3

    move v9, v6

    move v10, v7

    move v7, v6

    move v6, v5

    move/from16 v35, v3

    move v3, v4

    move/from16 v4, v35

    goto/16 :goto_5

    :cond_10
    move/from16 v3, p3

    goto/16 :goto_7

    :cond_11
    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v5

    invoke-virtual {v5, v3}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v3

    move/from16 v0, p2

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, v33

    invoke-virtual {v0, v8}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->d()Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-result-object v3

    move/from16 v0, p2

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Lmaps/ac/cm;->a(Lmaps/ac/cm;)Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->d()Lmaps/ac/cm;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cm;->c()Lmaps/ac/cm;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lmaps/ac/cm;->a(F)Lmaps/ac/cm;

    move-result-object v3

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-static {v0, v3, v1}, Lmaps/ac/cm;->a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;

    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v25

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v26

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p8

    move-object/from16 v1, v27

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    if-eqz p10, :cond_12

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, Lmaps/at/j;->a(II)V

    :cond_12
    add-int/lit8 v5, v4, 0x1

    add-int/lit8 v6, v5, 0x1

    add-int/lit8 v3, v6, 0x1

    add-int/lit8 v7, v3, 0x1

    move v8, v6

    move v9, v5

    move v10, v7

    move v7, v6

    move v6, v3

    move/from16 v35, v3

    move v3, v4

    move/from16 v4, v35

    goto/16 :goto_5

    :cond_13
    const/16 v16, 0x0

    goto/16 :goto_6

    :cond_14
    move/from16 v35, v3

    move v3, v7

    move/from16 v7, v35

    goto/16 :goto_1
.end method

.method public final a(Lmaps/ac/az;FLmaps/ac/av;IFLmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V
    .locals 15

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/az;->b()I

    move-result v6

    add-int/lit8 v7, v6, -0x1

    invoke-interface/range {p6 .. p6}, Lmaps/at/o;->a()I

    move-result v8

    mul-int/lit8 v2, v7, 0x4

    iget-object v5, p0, Lmaps/al/h;->b:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/al/h;->c:Lmaps/ac/av;

    iget-object v9, p0, Lmaps/al/h;->d:Lmaps/ac/av;

    iget-object v10, p0, Lmaps/al/h;->e:Lmaps/ac/av;

    iget-object v11, p0, Lmaps/al/h;->f:Lmaps/ac/av;

    invoke-interface/range {p6 .. p6}, Lmaps/at/o;->a()I

    move-result v4

    add-int/2addr v4, v2

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Lmaps/at/o;->a(I)V

    if-eqz p8, :cond_0

    invoke-interface/range {p8 .. p8}, Lmaps/at/j;->g()I

    move-result v4

    add-int/2addr v2, v4

    move-object/from16 v0, p8

    invoke-interface {v0, v2}, Lmaps/at/j;->c(I)V

    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move-object/from16 v0, p3

    invoke-static {v5, v0, v5}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v4, 0x0

    const/4 v2, 0x1

    move v14, v2

    move v2, v4

    move-object v4, v3

    move v3, v14

    :goto_0
    if-ge v3, v6, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move-object/from16 v0, p3

    invoke-static {v4, v0, v4}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v4, v5, v9}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move/from16 v0, p2

    neg-float v12, v0

    invoke-static {v9, v12, v10}, Lmaps/ac/ax;->a(Lmaps/ac/av;FLmaps/ac/av;)V

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v5, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    invoke-static {v5, v10, v11}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v11, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    invoke-static {v4, v10, v11}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v11, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v4, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    if-eqz p8, :cond_1

    invoke-virtual {v9}, Lmaps/ac/av;->i()F

    move-result v12

    move/from16 v0, p4

    int-to-float v13, v0

    div-float/2addr v12, v13

    mul-float v12, v12, p5

    const/4 v13, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v13, v2}, Lmaps/at/j;->a(FF)V

    const/high16 v13, 0x3f800000    # 1.0f

    move-object/from16 v0, p8

    invoke-interface {v0, v13, v2}, Lmaps/at/j;->a(FF)V

    add-float/2addr v2, v12

    const/high16 v12, 0x3f800000    # 1.0f

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v2}, Lmaps/at/j;->a(FF)V

    const/4 v12, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v2}, Lmaps/at/j;->a(FF)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    move-object v14, v5

    move-object v5, v4

    move-object v4, v14

    goto :goto_0

    :cond_2
    mul-int/lit8 v2, v7, 0x2

    add-int/lit8 v3, v7, -0x1

    invoke-interface/range {p7 .. p7}, Lmaps/at/e;->b()I

    move-result v6

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v2, v6

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Lmaps/at/e;->b(I)V

    iget-object v6, p0, Lmaps/al/h;->d:Lmaps/ac/av;

    iget-object v7, p0, Lmaps/al/h;->e:Lmaps/ac/av;

    iget-object v9, p0, Lmaps/al/h;->f:Lmaps/ac/av;

    const/4 v2, 0x0

    :goto_1
    mul-int/lit8 v10, v2, 0x4

    add-int/2addr v10, v8

    const/4 v11, 0x0

    cmpl-float v11, p2, v11

    if-lez v11, :cond_4

    add-int/lit8 v11, v10, 0x1

    add-int/lit8 v12, v10, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, Lmaps/at/e;->a(III)V

    add-int/lit8 v11, v10, 0x2

    add-int/lit8 v12, v10, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, Lmaps/at/e;->a(III)V

    :goto_2
    if-eq v2, v3, :cond_6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    add-int/lit8 v11, v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v4}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    add-int/lit8 v11, v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v6}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-static {v4, v5, v7}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v6, v4, v9}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v7, v9}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;)J

    move-result-wide v11

    long-to-float v11, v11

    mul-float v11, v11, p2

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_3

    add-int/lit8 v11, v10, 0x4

    const/4 v12, 0x0

    cmpl-float v12, p2, v12

    if-lez v12, :cond_5

    add-int/lit8 v12, v10, 0x3

    add-int/lit8 v10, v10, 0x2

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v12, v10, v11}, Lmaps/at/e;->a(III)V

    :cond_3
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v11, v10, 0x2

    add-int/lit8 v12, v10, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, Lmaps/at/e;->a(III)V

    add-int/lit8 v11, v10, 0x3

    add-int/lit8 v12, v10, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, Lmaps/at/e;->a(III)V

    goto :goto_2

    :cond_5
    add-int/lit8 v12, v10, 0x2

    add-int/lit8 v10, v10, 0x3

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v12, v10, v11}, Lmaps/at/e;->a(III)V

    goto :goto_3

    :cond_6
    return-void
.end method

.method public final a(Lmaps/ac/az;FZLmaps/ac/av;IFLmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V
    .locals 16

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/az;->b()I

    move-result v7

    add-int/lit8 v8, v7, -0x1

    invoke-interface/range {p7 .. p7}, Lmaps/at/o;->a()I

    move-result v9

    if-gtz v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_5

    const/4 v2, 0x5

    :goto_1
    mul-int v10, v2, v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/al/h;->b:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/al/h;->c:Lmaps/ac/av;

    invoke-interface/range {p7 .. p7}, Lmaps/at/o;->a()I

    move-result v3

    add-int/2addr v3, v10

    move-object/from16 v0, p7

    invoke-interface {v0, v3}, Lmaps/at/o;->a(I)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/al/h;->d:Lmaps/ac/av;

    if-eqz p9, :cond_2

    invoke-interface/range {p9 .. p9}, Lmaps/at/j;->g()I

    move-result v3

    add-int/2addr v3, v10

    move-object/from16 v0, p9

    invoke-interface {v0, v3}, Lmaps/at/j;->c(I)V

    :cond_2
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move-object/from16 v0, p4

    invoke-static {v5, v0, v5}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    const/4 v4, 0x0

    const/4 v3, 0x1

    move v15, v3

    move v3, v4

    move v4, v15

    :goto_2
    if-ge v4, v7, :cond_6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move-object/from16 v0, p4

    invoke-static {v6, v0, v6}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/al/h;->d:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/al/h;->e:Lmaps/ac/av;

    invoke-static {v6, v5, v12}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move/from16 v0, p2

    invoke-static {v12, v0, v13}, Lmaps/ac/ax;->a(Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-static {v5, v13, v12}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p7

    move/from16 v1, p5

    invoke-interface {v0, v12, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    invoke-static {v5, v13, v12}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p7

    move/from16 v1, p5

    invoke-interface {v0, v12, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    invoke-static {v6, v13, v12}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p7

    move/from16 v1, p5

    invoke-interface {v0, v12, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    invoke-static {v6, v13, v12}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p7

    move/from16 v1, p5

    invoke-interface {v0, v12, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    if-eqz p3, :cond_3

    move-object/from16 v0, p7

    move/from16 v1, p5

    invoke-interface {v0, v6, v1}, Lmaps/at/o;->a(Lmaps/ac/av;I)V

    :cond_3
    if-eqz p9, :cond_4

    invoke-static {v6, v5, v11}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-virtual {v11}, Lmaps/ac/av;->i()F

    move-result v12

    move/from16 v0, p5

    int-to-float v13, v0

    div-float/2addr v12, v13

    mul-float v12, v12, p6

    const/4 v13, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v3}, Lmaps/at/j;->a(FF)V

    const/high16 v13, 0x3f800000    # 1.0f

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v3}, Lmaps/at/j;->a(FF)V

    add-float/2addr v3, v12

    const/high16 v12, 0x3f800000    # 1.0f

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v3}, Lmaps/at/j;->a(FF)V

    const/4 v12, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v3}, Lmaps/at/j;->a(FF)V

    if-eqz p3, :cond_4

    const/high16 v12, 0x3f000000    # 0.5f

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v3}, Lmaps/at/j;->a(FF)V

    :cond_4
    add-int/lit8 v4, v4, 0x1

    move-object v15, v6

    move-object v6, v5

    move-object v5, v15

    goto/16 :goto_2

    :cond_5
    const/4 v2, 0x4

    goto/16 :goto_1

    :cond_6
    if-eqz p8, :cond_0

    add-int v3, v9, v10

    const/16 v4, 0x7fff

    if-le v3, v4, :cond_7

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " required, but we can only store 32767"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/al/h;->d:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/al/h;->e:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/al/h;->f:Lmaps/ac/av;

    mul-int/lit8 v11, v8, 0x2

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/az;->e()Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x0

    :goto_3
    sub-int v3, v8, v3

    if-eqz p3, :cond_9

    invoke-interface/range {p8 .. p8}, Lmaps/at/e;->b()I

    move-result v12

    add-int/2addr v3, v11

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v3, v12

    move-object/from16 v0, p8

    invoke-interface {v0, v3}, Lmaps/at/e;->b(I)V

    :goto_4
    const/4 v3, 0x0

    :goto_5
    if-ge v3, v8, :cond_a

    mul-int v11, v3, v2

    add-int/2addr v11, v9

    add-int/lit8 v12, v11, 0x1

    add-int/lit8 v13, v11, 0x2

    move-object/from16 v0, p8

    invoke-interface {v0, v11, v12, v13}, Lmaps/at/e;->a(III)V

    add-int/lit8 v12, v11, 0x2

    add-int/lit8 v13, v11, 0x3

    move-object/from16 v0, p8

    invoke-interface {v0, v11, v12, v13}, Lmaps/at/e;->a(III)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_8
    const/4 v3, 0x1

    goto :goto_3

    :cond_9
    invoke-interface/range {p8 .. p8}, Lmaps/at/e;->b()I

    move-result v3

    mul-int/lit8 v11, v11, 0x3

    add-int/2addr v3, v11

    move-object/from16 v0, p8

    invoke-interface {v0, v3}, Lmaps/at/e;->b(I)V

    goto :goto_4

    :cond_a
    if-eqz p3, :cond_0

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    add-int/lit8 v2, v8, -0x1

    if-ge v3, v2, :cond_d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    add-int/lit8 v2, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    add-int/lit8 v2, v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-static {v6, v5, v7}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v4, v6, v10}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v7, v10}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;)J

    move-result-wide v11

    const-wide/16 v13, 0x0

    cmp-long v2, v11, v13

    if-lez v2, :cond_b

    const/4 v2, 0x1

    :goto_7
    mul-int/lit8 v11, v3, 0x5

    add-int/2addr v11, v9

    add-int/lit8 v12, v11, 0x5

    if-eqz v2, :cond_c

    add-int/lit8 v2, v11, 0x2

    add-int/lit8 v12, v12, 0x1

    add-int/lit8 v11, v11, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v2, v12, v11}, Lmaps/at/e;->a(III)V

    :goto_8
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    :cond_b
    const/4 v2, 0x0

    goto :goto_7

    :cond_c
    add-int/lit8 v2, v11, 0x3

    add-int/lit8 v11, v11, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v2, v11, v12}, Lmaps/at/e;->a(III)V

    goto :goto_8

    :cond_d
    invoke-virtual/range {p1 .. p1}, Lmaps/ac/az;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, v8, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-static {v6, v5, v7}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v4, v6, v10}, Lmaps/ac/ax;->d(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v7, v10}, Lmaps/ac/ax;->c(Lmaps/ac/av;Lmaps/ac/av;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_e

    const/4 v2, 0x1

    :goto_9
    add-int/lit8 v3, v8, -0x1

    mul-int/lit8 v3, v3, 0x5

    add-int/2addr v3, v9

    if-eqz v2, :cond_f

    add-int/lit8 v2, v3, 0x2

    add-int/lit8 v4, v9, 0x1

    add-int/lit8 v3, v3, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v2, v4, v3}, Lmaps/at/e;->a(III)V

    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x0

    goto :goto_9

    :cond_f
    add-int/lit8 v2, v3, 0x3

    add-int/lit8 v3, v3, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v2, v3, v9}, Lmaps/at/e;->a(III)V

    goto/16 :goto_0
.end method
