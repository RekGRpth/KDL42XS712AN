.class public final Lmaps/al/p;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lmaps/ac/cj;[B)Ljava/util/List;
    .locals 6

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lmaps/ac/cj;->a()I

    move-result v2

    if-ge v0, v2, :cond_3

    new-instance v2, Lmaps/ac/av;

    invoke-direct {v2}, Lmaps/ac/av;-><init>()V

    new-instance v3, Lmaps/ac/av;

    invoke-direct {v3}, Lmaps/ac/av;-><init>()V

    new-instance v4, Lmaps/ac/av;

    invoke-direct {v4}, Lmaps/ac/av;-><init>()V

    invoke-virtual {p0, v0, v2, v3, v4}, Lmaps/ac/cj;->a(ILmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    aget-byte v5, p1, v0

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    invoke-static {v1, v2, v3}, Lmaps/al/p;->a(Ljava/util/Map;Lmaps/ac/av;Lmaps/ac/av;)V

    :cond_0
    aget-byte v5, p1, v0

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_1

    invoke-static {v1, v3, v4}, Lmaps/al/p;->a(Ljava/util/Map;Lmaps/ac/av;Lmaps/ac/av;)V

    :cond_1
    aget-byte v3, p1, v0

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_2

    invoke-static {v1, v4, v2}, Lmaps/al/p;->a(Ljava/util/Map;Lmaps/ac/av;Lmaps/ac/av;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/dk;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v1}, Lmaps/m/ck;->a(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/q;

    invoke-virtual {v0}, Lmaps/al/q;->c()Lmaps/ac/az;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    return-object v1
.end method

.method private static a(Ljava/util/Map;Lmaps/ac/av;Lmaps/ac/av;)V
    .locals 4

    new-instance v2, Lmaps/al/q;

    invoke-direct {v2, p1, p2}, Lmaps/al/q;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/q;

    invoke-interface {p0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/al/q;

    if-eqz v0, :cond_0

    invoke-virtual {v2, v0}, Lmaps/al/q;->a(Lmaps/al/q;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lmaps/al/q;->a()Lmaps/ac/av;

    move-result-object v3

    invoke-interface {p0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lmaps/al/q;->b()Lmaps/ac/av;

    move-result-object v3

    invoke-interface {p0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-eqz v1, :cond_1

    if-eq v1, v0, :cond_1

    invoke-virtual {v2, v1}, Lmaps/al/q;->a(Lmaps/al/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lmaps/al/q;->a()Lmaps/ac/av;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lmaps/al/q;->b()Lmaps/ac/av;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v2}, Lmaps/al/q;->a()Lmaps/ac/av;

    move-result-object v0

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Lmaps/al/q;->b()Lmaps/ac/av;

    move-result-object v0

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
