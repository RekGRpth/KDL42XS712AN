.class public final Lmaps/bo/v;
.super Lmaps/bo/w;


# direct methods
.method public constructor <init>(Lmaps/bo/n;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/bo/w;-><init>(Lmaps/bo/n;)V

    return-void
.end method


# virtual methods
.method protected final a(F)F
    .locals 4

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method protected final a(Lmaps/bo/j;I)F
    .locals 1

    invoke-virtual {p1, p2}, Lmaps/bo/j;->a(I)F

    move-result v0

    return v0
.end method

.method protected final b(Lmaps/bo/j;I)F
    .locals 1

    invoke-virtual {p1, p2}, Lmaps/bo/j;->b(I)F

    move-result v0

    return v0
.end method

.method protected final b(Lmaps/bo/k;)Z
    .locals 2

    const-string v0, "t"

    const/16 v1, 0x63

    invoke-static {v1, v0}, Lmaps/br/g;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lmaps/bo/v;->a:Lmaps/bo/n;

    invoke-interface {v0, p1}, Lmaps/bo/n;->b(Lmaps/bo/k;)Z

    move-result v0

    return v0
.end method

.method protected final d(Lmaps/bo/k;)V
    .locals 1

    iget-object v0, p0, Lmaps/bo/v;->a:Lmaps/bo/n;

    invoke-interface {v0, p1}, Lmaps/bo/n;->c(Lmaps/bo/k;)V

    return-void
.end method

.method protected final f(Lmaps/bo/k;)Z
    .locals 1

    iget-object v0, p0, Lmaps/bo/v;->a:Lmaps/bo/n;

    invoke-interface {v0, p1}, Lmaps/bo/n;->a(Lmaps/bo/k;)Z

    move-result v0

    return v0
.end method
