.class public final enum Lmaps/bo/f;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/bo/f;

.field public static final enum b:Lmaps/bo/f;

.field public static final enum c:Lmaps/bo/f;

.field private static final synthetic d:[Lmaps/bo/f;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/bo/f;

    const-string v1, "NO"

    invoke-direct {v0, v1, v2}, Lmaps/bo/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/bo/f;->a:Lmaps/bo/f;

    new-instance v0, Lmaps/bo/f;

    const-string v1, "MAYBE"

    invoke-direct {v0, v1, v3}, Lmaps/bo/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/bo/f;->b:Lmaps/bo/f;

    new-instance v0, Lmaps/bo/f;

    const-string v1, "YES"

    invoke-direct {v0, v1, v4}, Lmaps/bo/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/bo/f;->c:Lmaps/bo/f;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/bo/f;

    sget-object v1, Lmaps/bo/f;->a:Lmaps/bo/f;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/bo/f;->b:Lmaps/bo/f;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/bo/f;->c:Lmaps/bo/f;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/bo/f;->d:[Lmaps/bo/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/bo/f;
    .locals 1

    const-class v0, Lmaps/bo/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/bo/f;

    return-object v0
.end method

.method public static values()[Lmaps/bo/f;
    .locals 1

    sget-object v0, Lmaps/bo/f;->d:[Lmaps/bo/f;

    invoke-virtual {v0}, [Lmaps/bo/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/bo/f;

    return-object v0
.end method
