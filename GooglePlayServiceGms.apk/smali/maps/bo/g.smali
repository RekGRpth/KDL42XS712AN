.class public final Lmaps/bo/g;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bo/n;


# instance fields
.field private a:Lmaps/bo/m;

.field private b:Lmaps/bo/k;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lmaps/bo/m;)V
    .locals 1

    iput-object p2, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v0, Lmaps/bo/k;

    invoke-direct {v0, p1, p0}, Lmaps/bo/k;-><init>(Landroid/content/Context;Lmaps/bo/n;)V

    iput-object v0, p0, Lmaps/bo/g;->b:Lmaps/bo/k;

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->b:Lmaps/bo/k;

    invoke-virtual {v0, p1}, Lmaps/bo/k;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(Lmaps/bo/k;)Z
    .locals 3

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/d;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lmaps/bo/d;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/u;)Z

    move-result v0

    return v0
.end method

.method public final a(Lmaps/bo/k;Z)Z
    .locals 3

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/c;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lmaps/bo/c;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/q;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Lmaps/bo/k;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/d;

    invoke-direct {v1, v2, p1}, Lmaps/bo/d;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/u;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lmaps/bo/g;->c:Z

    :cond_0
    return v0
.end method

.method public final b(Lmaps/bo/k;Z)Z
    .locals 3

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v2, Lmaps/bo/c;

    invoke-direct {v2, v0, p1}, Lmaps/bo/c;-><init>(ILmaps/bo/k;)V

    invoke-interface {v1, v2}, Lmaps/bo/m;->a(Lmaps/bo/q;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(Lmaps/bo/k;)V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bo/g;->c:Z

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/d;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lmaps/bo/d;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/u;)Z

    return-void
.end method

.method public final c(Lmaps/bo/k;Z)V
    .locals 3

    if-eqz p2, :cond_0

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/c;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p1}, Lmaps/bo/c;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/q;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/c;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lmaps/bo/c;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/q;)Z

    goto :goto_0
.end method

.method public final d(Lmaps/bo/k;)Z
    .locals 3

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/b;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lmaps/bo/b;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/o;)Z

    move-result v0

    return v0
.end method

.method public final e(Lmaps/bo/k;)Z
    .locals 3

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/b;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lmaps/bo/b;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/o;)Z

    move-result v0

    return v0
.end method

.method public final f(Lmaps/bo/k;)V
    .locals 3

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/b;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lmaps/bo/b;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/o;)Z

    return-void
.end method

.method public final g(Lmaps/bo/k;)Z
    .locals 3

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/s;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lmaps/bo/s;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/o;)Z

    move-result v0

    return v0
.end method

.method public final h(Lmaps/bo/k;)Z
    .locals 3

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/s;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lmaps/bo/s;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/o;)Z

    move-result v0

    return v0
.end method

.method public final i(Lmaps/bo/k;)V
    .locals 3

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    new-instance v1, Lmaps/bo/s;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lmaps/bo/s;-><init>(ILmaps/bo/k;)V

    invoke-interface {v0, v1}, Lmaps/bo/m;->a(Lmaps/bo/o;)Z

    return-void
.end method

.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1}, Lmaps/bo/m;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1}, Lmaps/bo/m;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1}, Lmaps/bo/m;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1, p2, p3, p4}, Lmaps/bo/m;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1}, Lmaps/bo/m;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/bo/g;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1, p2, p3, p4}, Lmaps/bo/m;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onShowPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1}, Lmaps/bo/m;->onShowPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1}, Lmaps/bo/m;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/bo/g;->a:Lmaps/bo/m;

    invoke-interface {v0, p1}, Lmaps/bo/m;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
