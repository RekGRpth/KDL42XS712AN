.class public final enum Lmaps/au/aj;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/au/aj;

.field public static final enum b:Lmaps/au/aj;

.field public static final enum c:Lmaps/au/aj;

.field public static final enum d:Lmaps/au/aj;

.field public static final enum e:Lmaps/au/aj;

.field public static final enum f:Lmaps/au/aj;

.field public static final enum g:Lmaps/au/aj;

.field public static final enum h:Lmaps/au/aj;

.field public static final enum i:Lmaps/au/aj;

.field private static final synthetic j:[Lmaps/au/aj;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/au/aj;

    const-string v1, "AT_CENTER"

    invoke-direct {v0, v1, v3}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->a:Lmaps/au/aj;

    new-instance v0, Lmaps/au/aj;

    const-string v1, "ABOVE_CENTER"

    invoke-direct {v0, v1, v4}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->b:Lmaps/au/aj;

    new-instance v0, Lmaps/au/aj;

    const-string v1, "RIGHT_OF_CENTER"

    invoke-direct {v0, v1, v5}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->c:Lmaps/au/aj;

    new-instance v0, Lmaps/au/aj;

    const-string v1, "BELOW_CENTER"

    invoke-direct {v0, v1, v6}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->d:Lmaps/au/aj;

    new-instance v0, Lmaps/au/aj;

    const-string v1, "LEFT_OF_CENTER"

    invoke-direct {v0, v1, v7}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->e:Lmaps/au/aj;

    new-instance v0, Lmaps/au/aj;

    const-string v1, "BOTTOM_RIGHT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->f:Lmaps/au/aj;

    new-instance v0, Lmaps/au/aj;

    const-string v1, "BOTTOM_LEFT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->g:Lmaps/au/aj;

    new-instance v0, Lmaps/au/aj;

    const-string v1, "TOP_RIGHT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->h:Lmaps/au/aj;

    new-instance v0, Lmaps/au/aj;

    const-string v1, "TOP_LEFT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lmaps/au/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/aj;->i:Lmaps/au/aj;

    const/16 v0, 0x9

    new-array v0, v0, [Lmaps/au/aj;

    sget-object v1, Lmaps/au/aj;->a:Lmaps/au/aj;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/au/aj;->b:Lmaps/au/aj;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/au/aj;->c:Lmaps/au/aj;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/au/aj;->d:Lmaps/au/aj;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/au/aj;->e:Lmaps/au/aj;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/au/aj;->f:Lmaps/au/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/au/aj;->g:Lmaps/au/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/au/aj;->h:Lmaps/au/aj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lmaps/au/aj;->i:Lmaps/au/aj;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/au/aj;->j:[Lmaps/au/aj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lmaps/au/aj;
    .locals 2

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    sget-object v0, Lmaps/au/aj;->a:Lmaps/au/aj;

    :goto_0
    return-object v0

    :pswitch_2
    sget-object v0, Lmaps/au/aj;->e:Lmaps/au/aj;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lmaps/au/aj;->c:Lmaps/au/aj;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lmaps/au/aj;->b:Lmaps/au/aj;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lmaps/au/aj;->i:Lmaps/au/aj;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lmaps/au/aj;->h:Lmaps/au/aj;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lmaps/au/aj;->d:Lmaps/au/aj;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lmaps/au/aj;->g:Lmaps/au/aj;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lmaps/au/aj;->f:Lmaps/au/aj;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/au/aj;
    .locals 1

    const-class v0, Lmaps/au/aj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/au/aj;

    return-object v0
.end method

.method public static values()[Lmaps/au/aj;
    .locals 1

    sget-object v0, Lmaps/au/aj;->j:[Lmaps/au/aj;

    invoke-virtual {v0}, [Lmaps/au/aj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/au/aj;

    return-object v0
.end method
