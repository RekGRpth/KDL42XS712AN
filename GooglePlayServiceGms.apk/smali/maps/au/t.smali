.class public final Lmaps/au/t;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/au/u;


# instance fields
.field private final a:Lmaps/aj/ag;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lmaps/ac/bl;

.field private final e:Lmaps/aj/ah;

.field private final f:F

.field private final g:F

.field private final h:F

.field private final i:F

.field private final j:I


# direct methods
.method constructor <init>(Lmaps/aj/ag;Ljava/lang/String;ILmaps/ac/bl;Lmaps/aj/ah;)V
    .locals 8

    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/au/t;->a:Lmaps/aj/ag;

    iput-object p2, p0, Lmaps/au/t;->b:Ljava/lang/String;

    iput p3, p0, Lmaps/au/t;->c:I

    iput-object p4, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {p4}, Lmaps/ac/bl;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4}, Lmaps/ac/bl;->j()Lmaps/ac/bq;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bq;->b()I

    move-result v0

    :goto_0
    iput v0, p0, Lmaps/au/t;->j:I

    invoke-virtual {p4}, Lmaps/ac/bl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/br;->g()F

    move-result v6

    :cond_0
    iput-object p5, p0, Lmaps/au/t;->e:Lmaps/aj/ah;

    iget-object v0, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    invoke-virtual {v0}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v3

    :goto_1
    int-to-float v4, p3

    move-object v0, p1

    move-object v1, p2

    move-object v2, p5

    invoke-virtual/range {v0 .. v6}, Lmaps/aj/ag;->a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FZF)[F

    move-result-object v0

    sget-object v1, Lmaps/aj/ag;->c:Lmaps/aj/ah;

    if-ne p5, v1, :cond_3

    aget v1, v0, v7

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v1, v2

    iput v1, p0, Lmaps/au/t;->f:F

    :goto_2
    aget v1, v0, v5

    iput v1, p0, Lmaps/au/t;->g:F

    const/4 v1, 0x2

    aget v1, v0, v1

    iput v1, p0, Lmaps/au/t;->h:F

    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, Lmaps/au/t;->i:F

    return-void

    :cond_1
    move v0, v7

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    aget v1, v0, v7

    iput v1, p0, Lmaps/au/t;->f:F

    goto :goto_2
.end method


# virtual methods
.method public final a()F
    .locals 1

    iget v0, p0, Lmaps/au/t;->f:F

    return v0
.end method

.method public final a(Lmaps/ap/b;)Lmaps/as/b;
    .locals 8

    iget-object v0, p0, Lmaps/au/t;->a:Lmaps/aj/ag;

    iget-object v1, p0, Lmaps/au/t;->b:Ljava/lang/String;

    iget-object v2, p0, Lmaps/au/t;->e:Lmaps/aj/ah;

    iget-object v3, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    invoke-virtual {v3}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v3

    :goto_0
    iget v4, p0, Lmaps/au/t;->c:I

    int-to-float v4, v4

    iget-object v5, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    invoke-static {v5, p1}, Lmaps/au/m;->a(Lmaps/ac/bl;Lmaps/ap/b;)I

    move-result v5

    iget-object v6, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    invoke-static {v6, p1}, Lmaps/au/m;->b(Lmaps/ac/bl;Lmaps/ap/b;)I

    move-result v6

    iget v7, p0, Lmaps/au/t;->j:I

    invoke-virtual/range {v0 .. v7}, Lmaps/aj/ag;->a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)Lmaps/as/b;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(Lmaps/as/a;Lmaps/ap/b;)Lmaps/as/b;
    .locals 9

    iget-object v0, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    invoke-static {v0, p2}, Lmaps/au/m;->b(Lmaps/ac/bl;Lmaps/ap/b;)I

    move-result v7

    iget-object v0, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    invoke-static {v0, p2}, Lmaps/au/m;->a(Lmaps/ac/bl;Lmaps/ap/b;)I

    move-result v6

    iget v0, p0, Lmaps/au/t;->j:I

    if-eqz v0, :cond_1

    const/4 v7, 0x0

    sget-object v0, Lmaps/ap/b;->b:Lmaps/ap/b;

    if-eq p2, v0, :cond_0

    sget-object v0, Lmaps/ap/b;->c:Lmaps/ap/b;

    if-ne p2, v0, :cond_1

    :cond_0
    iget v0, p0, Lmaps/au/t;->j:I

    invoke-static {v0}, Lmaps/au/m;->b(I)I

    move-result v6

    :cond_1
    iget-object v0, p0, Lmaps/au/t;->a:Lmaps/aj/ag;

    iget-object v2, p0, Lmaps/au/t;->b:Ljava/lang/String;

    iget-object v3, p0, Lmaps/au/t;->e:Lmaps/aj/ah;

    iget-object v1, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/au/t;->d:Lmaps/ac/bl;

    invoke-virtual {v1}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v4

    :goto_0
    iget v1, p0, Lmaps/au/t;->c:I

    int-to-float v5, v1

    iget v8, p0, Lmaps/au/t;->j:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lmaps/aj/ag;->a(Lmaps/as/a;Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)Lmaps/as/b;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/au/t;->g:F

    return v0
.end method

.method public final c()F
    .locals 1

    iget v0, p0, Lmaps/au/t;->h:F

    return v0
.end method

.method public final d()F
    .locals 1

    iget v0, p0, Lmaps/au/t;->i:F

    return v0
.end method

.method public final e()F
    .locals 2

    iget v0, p0, Lmaps/au/t;->g:F

    iget v1, p0, Lmaps/au/t;->h:F

    sub-float/2addr v0, v1

    iget v1, p0, Lmaps/au/t;->i:F

    sub-float/2addr v0, v1

    return v0
.end method
