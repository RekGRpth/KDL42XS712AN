.class public final Lmaps/au/x;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/ArrayList;

.field private final b:Lmaps/ac/bt;

.field private final c:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lmaps/ac/bt;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/au/x;->a:Ljava/util/ArrayList;

    iput-object p1, p0, Lmaps/au/x;->b:Lmaps/ac/bt;

    iput-object p2, p0, Lmaps/au/x;->c:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/cu;)V
    .locals 4

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Lmaps/ac/cu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lmaps/ac/cu;->a()Lmaps/ac/n;

    move-result-object v0

    instance-of v2, v0, Lmaps/ac/aj;

    if-eqz v2, :cond_1

    check-cast v0, Lmaps/ac/aj;

    if-eqz v1, :cond_0

    invoke-static {v1}, Lmaps/au/z;->a(Lmaps/au/z;)Lmaps/ac/az;

    move-result-object v2

    invoke-virtual {v0}, Lmaps/ac/aj;->b()Lmaps/ac/az;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/ac/az;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Lmaps/au/z;->a(Lmaps/ac/aj;)V

    move-object v0, v1

    :goto_1
    invoke-interface {p1}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    move-object v1, v0

    goto :goto_0

    :cond_0
    new-instance v1, Lmaps/au/z;

    iget-object v2, p0, Lmaps/au/x;->c:[Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lmaps/au/z;-><init>(Lmaps/ac/aj;[Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/au/x;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final a(Lmaps/ac/n;)V
    .locals 3

    instance-of v0, p1, Lmaps/ac/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/x;->a:Ljava/util/ArrayList;

    new-instance v1, Lmaps/au/z;

    check-cast p1, Lmaps/ac/aj;

    iget-object v2, p0, Lmaps/au/x;->c:[Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Lmaps/au/z;-><init>(Lmaps/ac/aj;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final a(I)[Lmaps/au/w;
    .locals 18

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/x;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v17, Lmaps/au/y;

    invoke-direct/range {v17 .. v17}, Lmaps/au/y;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lmaps/au/y;

    invoke-direct {v3}, Lmaps/au/y;-><init>()V

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    move-object v13, v2

    move-object v14, v6

    move-object v2, v1

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lmaps/au/z;

    invoke-virtual {v12}, Lmaps/au/z;->c()Lmaps/ac/o;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v8, Lmaps/au/y;

    invoke-direct {v8}, Lmaps/au/y;-><init>()V

    invoke-virtual {v12}, Lmaps/au/z;->a()Lmaps/ac/az;

    move-result-object v1

    invoke-static {v1, v8}, Lmaps/au/w;->a(Lmaps/ac/az;Lmaps/au/y;)Z

    new-instance v6, Lmaps/au/w;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/au/x;->b:Lmaps/ac/bt;

    invoke-static {v12}, Lmaps/m/ay;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v9

    invoke-virtual {v12}, Lmaps/au/z;->b()Ljava/util/Set;

    move-result-object v10

    move/from16 v11, p1

    invoke-direct/range {v6 .. v11}, Lmaps/au/w;-><init>(Lmaps/ac/bt;Lmaps/au/y;Ljava/util/List;Ljava/util/Set;I)V

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v12}, Lmaps/au/z;->e()I

    move-result v1

    const/4 v6, 0x1

    if-le v1, v6, :cond_4

    new-instance v7, Lmaps/au/ab;

    invoke-direct {v7, v12}, Lmaps/au/ab;-><init>(Lmaps/au/z;)V

    if-eqz v2, :cond_3

    invoke-virtual {v7, v2}, Lmaps/au/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v12}, Lmaps/au/z;->a()Lmaps/ac/az;

    move-result-object v2

    invoke-static {v2, v3}, Lmaps/au/w;->a(Lmaps/ac/az;Lmaps/au/y;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    new-instance v1, Lmaps/au/w;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/x;->b:Lmaps/ac/bt;

    move/from16 v6, p1

    invoke-direct/range {v1 .. v6}, Lmaps/au/w;-><init>(Lmaps/ac/bt;Lmaps/au/y;Ljava/util/List;Ljava/util/Set;I)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lmaps/au/y;->a()V

    invoke-virtual {v12}, Lmaps/au/z;->a()Lmaps/ac/az;

    move-result-object v1

    invoke-static {v1, v3}, Lmaps/au/w;->a(Lmaps/ac/az;Lmaps/au/y;)Z

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    :cond_2
    invoke-interface {v4, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12}, Lmaps/au/z;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object v2, v7

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    invoke-virtual {v12}, Lmaps/au/z;->a()Lmaps/ac/az;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-static {v1, v0}, Lmaps/au/w;->a(Lmaps/ac/az;Lmaps/au/y;)Z

    move-result v1

    if-nez v1, :cond_9

    new-instance v6, Lmaps/au/w;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/au/x;->b:Lmaps/ac/bt;

    move-object/from16 v8, v17

    move-object v9, v14

    move-object v10, v13

    move/from16 v11, p1

    invoke-direct/range {v6 .. v11}, Lmaps/au/w;-><init>(Lmaps/ac/bt;Lmaps/au/y;Ljava/util/List;Ljava/util/Set;I)V

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {v17 .. v17}, Lmaps/au/y;->a()V

    invoke-virtual {v12}, Lmaps/au/z;->a()Lmaps/ac/az;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-static {v1, v0}, Lmaps/au/w;->a(Lmaps/ac/az;Lmaps/au/y;)Z

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :goto_2
    invoke-interface {v6, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12}, Lmaps/au/z;->b()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object v13, v1

    move-object v14, v6

    goto/16 :goto_0

    :cond_5
    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    new-instance v6, Lmaps/au/w;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/au/x;->b:Lmaps/ac/bt;

    move-object/from16 v8, v17

    move-object v9, v14

    move-object v10, v13

    move/from16 v11, p1

    invoke-direct/range {v6 .. v11}, Lmaps/au/w;-><init>(Lmaps/ac/bt;Lmaps/au/y;Ljava/util/List;Ljava/util/Set;I)V

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    new-instance v1, Lmaps/au/w;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/x;->b:Lmaps/ac/bt;

    move/from16 v6, p1

    invoke-direct/range {v1 .. v6}, Lmaps/au/w;-><init>(Lmaps/ac/bt;Lmaps/au/y;Ljava/util/List;Ljava/util/Set;I)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x0

    :goto_3
    return-object v1

    :cond_8
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lmaps/au/w;

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/au/w;

    goto :goto_3

    :cond_9
    move-object v1, v13

    move-object v6, v14

    goto :goto_2
.end method
