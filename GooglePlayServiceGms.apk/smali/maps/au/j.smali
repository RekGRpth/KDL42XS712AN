.class public final Lmaps/au/j;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/au/ap;


# instance fields
.field private final a:Lmaps/ac/bt;

.field private final b:Lmaps/ac/bd;

.field private final c:Lmaps/at/i;


# direct methods
.method public constructor <init>(Lmaps/ac/bt;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/at/i;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lmaps/at/i;-><init>(I)V

    iput-object v0, p0, Lmaps/au/j;->c:Lmaps/at/i;

    iput-object p1, p0, Lmaps/au/j;->a:Lmaps/ac/bt;

    iget-object v0, p0, Lmaps/au/j;->a:Lmaps/ac/bt;

    invoke-virtual {v0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/j;->b:Lmaps/ac/bd;

    const/high16 v0, 0x10000

    mul-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x20

    iget-object v1, p0, Lmaps/au/j;->c:Lmaps/at/i;

    invoke-virtual {v1, v2, v2}, Lmaps/at/i;->a(II)V

    iget-object v1, p0, Lmaps/au/j;->c:Lmaps/at/i;

    invoke-virtual {v1, v2, v0}, Lmaps/at/i;->a(II)V

    iget-object v1, p0, Lmaps/au/j;->c:Lmaps/at/i;

    invoke-virtual {v1, v0, v2}, Lmaps/at/i;->a(II)V

    iget-object v1, p0, Lmaps/au/j;->c:Lmaps/at/i;

    invoke-virtual {v1, v0, v0}, Lmaps/at/i;->a(II)V

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ar/a;Lmaps/ap/b;)I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final a()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/au/j;->a:Lmaps/ac/bt;

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/am/b;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/as/a;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 4

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lmaps/au/j;->b:Lmaps/ac/bd;

    invoke-virtual {v1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v1

    iget-object v2, p0, Lmaps/au/j;->b:Lmaps/ac/bd;

    invoke-virtual {v2}, Lmaps/ac/bd;->f()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    iget-object v1, p0, Lmaps/au/j;->c:Lmaps/at/i;

    invoke-virtual {v1, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method public final a(Lmaps/au/m;)V
    .locals 0

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/aj/ac;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/as/a;)V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 4

    const/4 v2, 0x1

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-le v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x303

    invoke-interface {v0, v2, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p1}, Lmaps/as/a;->r()V

    iget-object v1, p1, Lmaps/as/a;->d:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    invoke-virtual {p1}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0
.end method

.method public final c()Lmaps/ao/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .locals 0

    return-void
.end method

.method public final h()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final j()I
    .locals 1

    iget-object v0, p0, Lmaps/au/j;->c:Lmaps/at/i;

    invoke-virtual {v0}, Lmaps/at/i;->b()I

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    iget-object v0, p0, Lmaps/au/j;->c:Lmaps/at/i;

    invoke-virtual {v0}, Lmaps/at/i;->c()I

    move-result v0

    return v0
.end method

.method public final l()Lmaps/au/m;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
