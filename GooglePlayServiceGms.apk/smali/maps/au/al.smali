.class public final Lmaps/au/al;
.super Lmaps/au/i;


# static fields
.field private static final h:Ljava/util/Map;


# instance fields
.field private volatile b:Lmaps/as/b;

.field private c:Lmaps/at/i;

.field private final d:[B

.field private e:Lmaps/v/e;

.field private f:Z

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lmaps/au/al;->h:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>([BLjava/util/Set;)V
    .locals 2

    invoke-direct {p0, p2}, Lmaps/au/i;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, Lmaps/au/al;->d:[B

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/au/al;->f:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/au/al;->g:J

    return-void
.end method

.method public static a([B)Lmaps/au/al;
    .locals 2

    new-instance v0, Lmaps/au/al;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-direct {v0, p0, v1}, Lmaps/au/al;-><init>([BLjava/util/Set;)V

    return-object v0
.end method

.method public static a([Ljava/lang/String;Lmaps/ac/cu;)Lmaps/au/al;
    .locals 7

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bc;

    invoke-virtual {v0}, Lmaps/ac/bc;->i()[I

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    if-ltz v5, :cond_0

    array-length v6, p0

    if-ge v5, v6, :cond_0

    aget-object v5, p0, v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Lmaps/au/al;

    invoke-virtual {v0}, Lmaps/ac/bc;->b()[B

    move-result-object v0

    invoke-direct {v1, v0, v2}, Lmaps/au/al;-><init>([BLjava/util/Set;)V

    return-object v1
.end method

.method public static c(Lmaps/as/a;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    iget-object v0, p0, Lmaps/as/a;->d:Lmaps/at/n;

    invoke-virtual {v0, p0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    return-void
.end method

.method private d(Lmaps/as/a;)Landroid/graphics/Bitmap;
    .locals 3

    const/4 v1, 0x1

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    :try_start_0
    invoke-virtual {p1}, Lmaps/as/a;->m()Lmaps/al/k;

    move-result-object v1

    iget-object v2, p0, Lmaps/au/al;->d:[B

    invoke-virtual {v1, v2, v0}, Lmaps/al/k;->a([BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/au/al;->b:Lmaps/as/b;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmaps/as/b;->k()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    return v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/al;->b:Lmaps/as/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/al;->b:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/au/al;->b:Lmaps/as/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/au/al;->f:Z

    :cond_0
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 11

    const/4 v9, 0x0

    const/high16 v6, 0x10000

    const/4 v10, 0x0

    iget-object v7, p0, Lmaps/au/al;->b:Lmaps/as/b;

    if-nez v7, :cond_5

    iget-boolean v0, p0, Lmaps/au/al;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->h()V

    invoke-direct {p0, p1}, Lmaps/au/al;->d(Lmaps/as/a;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_5

    new-instance v1, Lmaps/as/b;

    invoke-direct {v1, p1}, Lmaps/as/b;-><init>(Lmaps/as/a;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lmaps/as/b;->c(Z)V

    invoke-virtual {v1, v2}, Lmaps/as/b;->b(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lmaps/au/al;->b:Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->e()F

    move-result v3

    sget-object v0, Lmaps/au/al;->h:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/au/al;->h:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/at/i;

    :goto_1
    iput-object v0, p0, Lmaps/au/al;->c:Lmaps/at/i;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    :goto_2
    if-nez v0, :cond_2

    :goto_3
    return-void

    :cond_0
    const/16 v0, 0x4e20

    invoke-virtual {p1, v0}, Lmaps/as/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, p1}, Lmaps/au/al;->d(Lmaps/as/a;)Landroid/graphics/Bitmap;

    move-result-object v8

    new-instance v0, Lmaps/v/e;

    iget-wide v1, p0, Lmaps/au/al;->g:J

    const-wide/16 v3, 0xfa

    sget-object v5, Lmaps/v/g;->a:Lmaps/v/g;

    invoke-direct/range {v0 .. v5}, Lmaps/v/e;-><init>(JJLmaps/v/g;)V

    iput-object v0, p0, Lmaps/au/al;->e:Lmaps/v/e;

    move-object v2, v8

    goto :goto_0

    :cond_1
    new-instance v0, Lmaps/at/i;

    const/16 v4, 0x8

    invoke-direct {v0, v4}, Lmaps/at/i;-><init>(I)V

    const/high16 v4, 0x47800000    # 65536.0f

    mul-float/2addr v4, v3

    float-to-int v4, v4

    invoke-virtual {v0, v10, v10}, Lmaps/at/i;->a(II)V

    invoke-virtual {v0, v10, v4}, Lmaps/at/i;->a(II)V

    invoke-virtual {v0, v4, v10}, Lmaps/at/i;->a(II)V

    invoke-virtual {v0, v4, v4}, Lmaps/at/i;->a(II)V

    sget-object v4, Lmaps/au/al;->h:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    iget-object v2, p0, Lmaps/au/al;->c:Lmaps/at/i;

    invoke-virtual {v2, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    invoke-virtual {v0, v1}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/au/al;->e:Lmaps/v/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/au/al;->e:Lmaps/v/e;

    invoke-virtual {v0, p1}, Lmaps/v/e;->a(Lmaps/as/a;)I

    move-result v0

    if-ne v0, v6, :cond_3

    iput-object v9, p0, Lmaps/au/al;->e:Lmaps/v/e;

    iput-boolean v10, p0, Lmaps/au/al;->f:Z

    :cond_3
    :goto_4
    invoke-interface {v1, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/4 v0, 0x5

    const/4 v2, 0x4

    invoke-interface {v1, v0, v10, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    goto :goto_3

    :cond_4
    move v0, v6

    goto :goto_4

    :cond_5
    move-object v0, v7

    goto :goto_2

    :cond_6
    move-object v2, v9

    goto/16 :goto_0
.end method

.method public final b()I
    .locals 2

    const/16 v0, 0x60

    iget-object v1, p0, Lmaps/au/al;->d:[B

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/au/al;->d:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x10

    add-int/lit8 v0, v0, 0x60

    :cond_0
    return v0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/au/al;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/al;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/au/al;->f:Z

    return v0
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/au/al;->f:Z

    return-void
.end method
