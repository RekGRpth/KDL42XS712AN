.class public Lmaps/au/ah;
.super Lmaps/au/m;


# static fields
.field private static final G:[Lmaps/au/ak;

.field private static final H:[Lmaps/au/ak;


# instance fields
.field private A:I

.field private B:Lmaps/v/e;

.field private C:Z

.field private D:F

.field private final E:F

.field private final F:[F

.field protected a:Lmaps/ac/a;

.field protected b:Lmaps/au/o;

.field protected c:Lmaps/au/o;

.field protected d:Lmaps/au/ak;

.field protected e:F

.field protected f:F

.field protected g:F

.field protected h:F

.field private q:Lmaps/ac/a;

.field private r:Lmaps/ac/t;

.field private s:Lmaps/ac/t;

.field private t:Lmaps/at/n;

.field private final u:Ljava/lang/String;

.field private final v:Z

.field private w:Z

.field private x:F

.field private y:Z

.field private final z:[Lmaps/au/ak;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    new-array v0, v4, [Lmaps/au/ak;

    sput-object v0, Lmaps/au/ah;->G:[Lmaps/au/ak;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/au/ak;

    new-instance v1, Lmaps/au/ak;

    sget-object v2, Lmaps/au/aj;->d:Lmaps/au/aj;

    sget-object v3, Lmaps/au/s;->a:Lmaps/au/s;

    invoke-direct {v1, v2, v3}, Lmaps/au/ak;-><init>(Lmaps/au/aj;Lmaps/au/s;)V

    aput-object v1, v0, v4

    const/4 v1, 0x1

    new-instance v2, Lmaps/au/ak;

    sget-object v3, Lmaps/au/aj;->b:Lmaps/au/aj;

    sget-object v4, Lmaps/au/s;->a:Lmaps/au/s;

    invoke-direct {v2, v3, v4}, Lmaps/au/ak;-><init>(Lmaps/au/aj;Lmaps/au/s;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lmaps/au/ak;

    sget-object v3, Lmaps/au/aj;->e:Lmaps/au/aj;

    sget-object v4, Lmaps/au/s;->c:Lmaps/au/s;

    invoke-direct {v2, v3, v4}, Lmaps/au/ak;-><init>(Lmaps/au/aj;Lmaps/au/s;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lmaps/au/ak;

    sget-object v3, Lmaps/au/aj;->c:Lmaps/au/aj;

    sget-object v4, Lmaps/au/s;->b:Lmaps/au/s;

    invoke-direct {v2, v3, v4}, Lmaps/au/ak;-><init>(Lmaps/au/aj;Lmaps/au/s;)V

    aput-object v2, v0, v1

    sput-object v0, Lmaps/au/ah;->H:[Lmaps/au/ak;

    return-void
.end method

.method constructor <init>(Lmaps/ac/n;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/a;Lmaps/ac/a;FFZZLmaps/au/o;Lmaps/au/o;[Lmaps/au/ak;Z)V
    .locals 10

    invoke-interface {p1}, Lmaps/ac/n;->d()Lmaps/ac/bl;

    move-result-object v4

    invoke-interface {p1}, Lmaps/ac/n;->f()I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v8, p8

    move/from16 v9, p13

    invoke-direct/range {v1 .. v9}, Lmaps/au/m;-><init>(Lmaps/ac/n;Lmaps/am/b;Lmaps/ac/bl;FFIZZ)V

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lmaps/au/ah;->x:F

    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, Lmaps/au/ah;->F:[F

    iput-object p3, p0, Lmaps/au/ah;->u:Ljava/lang/String;

    iput-object p4, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    iput-object p5, p0, Lmaps/au/ah;->q:Lmaps/ac/a;

    move-object/from16 v0, p10

    iput-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    move-object/from16 v0, p11

    iput-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    move/from16 v0, p9

    iput-boolean v0, p0, Lmaps/au/ah;->v:Z

    iget-object v1, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    if-nez v1, :cond_0

    sget-object p12, Lmaps/au/ah;->G:[Lmaps/au/ak;

    :cond_0
    move-object/from16 v0, p12

    iput-object v0, p0, Lmaps/au/ah;->z:[Lmaps/au/ak;

    const/4 v1, 0x0

    iput v1, p0, Lmaps/au/ah;->A:I

    iget-object v1, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/au/ah;->z:[Lmaps/au/ak;

    iget v2, p0, Lmaps/au/ah;->A:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v1, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    iget-object v2, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v2, v2, Lmaps/au/ak;->b:Lmaps/au/s;

    invoke-virtual {v1, v2}, Lmaps/au/o;->a(Lmaps/au/s;)V

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/au/ah;->y:Z

    const/4 v1, 0x0

    if-eqz p10, :cond_2

    const/4 v1, 0x0

    invoke-virtual/range {p10 .. p10}, Lmaps/au/o;->a()F

    move-result v2

    invoke-virtual/range {p10 .. p10}, Lmaps/au/o;->b()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    :cond_2
    if-eqz p11, :cond_3

    invoke-virtual/range {p11 .. p11}, Lmaps/au/o;->a()F

    move-result v2

    invoke-virtual/range {p11 .. p11}, Lmaps/au/o;->b()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    :cond_3
    iput v1, p0, Lmaps/au/ah;->E:F

    return-void
.end method

.method private a(Lmaps/ar/a;)F
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    iget-boolean v0, p0, Lmaps/au/ah;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lmaps/ar/a;->a(Lmaps/ac/av;Z)F

    move-result v0

    invoke-virtual {p1, v2, v0}, Lmaps/ar/a;->a(FF)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lmaps/ar/a;->m()F

    move-result v0

    invoke-virtual {p1, v2, v0}, Lmaps/ar/a;->a(FF)F

    move-result v0

    goto :goto_0
.end method

.method static a(Lmaps/ac/bl;Lmaps/av/a;F)I
    .locals 3

    iget v0, p1, Lmaps/av/a;->e:F

    iget v1, p1, Lmaps/av/a;->f:I

    iget v2, p1, Lmaps/av/a;->g:I

    invoke-static {p0, v0, v1, v2, p2}, Lmaps/au/m;->a(Lmaps/ac/bl;FIIF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static a(Lmaps/ac/aw;Lmaps/am/b;ZLmaps/ar/a;Lmaps/as/c;Lmaps/aj/ag;Lmaps/av/a;)Lmaps/au/ah;
    .locals 27

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->n()Lmaps/ac/ag;

    move-result-object v1

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, Lmaps/au/o;->a(Lmaps/ac/ag;Lmaps/ac/n;Lmaps/ar/a;Lmaps/as/c;Lmaps/aj/ag;Lmaps/av/a;)Lmaps/au/o;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {v10}, Lmaps/au/o;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    const/4 v11, 0x0

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->o()Lmaps/ac/ag;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->o()Lmaps/ac/ag;

    move-result-object v1

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, Lmaps/au/o;->a(Lmaps/ac/ag;Lmaps/ac/n;Lmaps/ar/a;Lmaps/as/c;Lmaps/aj/ag;Lmaps/av/a;)Lmaps/au/o;

    move-result-object v11

    if-nez v11, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v11}, Lmaps/au/o;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v11, 0x0

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->p()[Lmaps/ac/c;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_7

    if-eqz v11, :cond_7

    sget-object v12, Lmaps/au/ah;->H:[Lmaps/au/ak;

    :cond_4
    if-eqz v11, :cond_5

    array-length v1, v12

    const/4 v2, 0x1

    if-ne v1, v2, :cond_8

    :cond_5
    const/4 v9, 0x1

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->m()[Lmaps/ac/a;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v5, v1, v2

    invoke-virtual {v5}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->n()Lmaps/ac/ag;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/ag;->b()I

    move-result v4

    if-ge v1, v4, :cond_9

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->n()Lmaps/ac/ag;

    move-result-object v4

    invoke-virtual {v4, v1}, Lmaps/ac/ag;->a(I)Lmaps/ac/ah;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/ah;->b()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v4}, Lmaps/ac/ah;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->p()[Lmaps/ac/c;

    move-result-object v1

    array-length v1, v1

    new-array v12, v1, [Lmaps/au/ak;

    const/4 v1, 0x0

    :goto_3
    array-length v2, v12

    if-ge v1, v2, :cond_4

    new-instance v2, Lmaps/au/ak;

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->p()[Lmaps/ac/c;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lmaps/ac/c;->d()I

    move-result v3

    invoke-static {v3}, Lmaps/au/aj;->a(I)Lmaps/au/aj;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->p()[Lmaps/ac/c;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lmaps/ac/c;->a()I

    move-result v4

    invoke-static {v4}, Lmaps/au/s;->a(I)Lmaps/au/s;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lmaps/au/ak;-><init>(Lmaps/au/aj;Lmaps/au/s;)V

    aput-object v2, v12, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_8
    const/4 v9, 0x0

    goto :goto_1

    :cond_9
    const/4 v1, 0x0

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->o()Lmaps/ac/ag;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/ag;->b()I

    move-result v4

    if-ge v1, v4, :cond_b

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->o()Lmaps/ac/ag;

    move-result-object v4

    invoke-virtual {v4, v1}, Lmaps/ac/ag;->a(I)Lmaps/ac/ah;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/ah;->b()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v4}, Lmaps/ac/ah;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->a()Lmaps/ac/o;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->a()Lmaps/ac/o;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/o;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_e

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    :goto_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->q()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_f

    move-object/from16 v0, p6

    iget-boolean v1, v0, Lmaps/av/a;->p:Z

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    :goto_6
    if-eqz v1, :cond_10

    new-instance v1, Lmaps/au/l;

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->k()F

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->l()F

    move-result v7

    move-object/from16 v0, p6

    iget-boolean v13, v0, Lmaps/av/a;->q:Z

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v8, p2

    invoke-direct/range {v1 .. v13}, Lmaps/au/l;-><init>(Lmaps/ac/aw;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/a;FFZZLmaps/au/o;Lmaps/au/o;[Lmaps/au/ak;Z)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->b()Lmaps/ac/bt;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v1

    const/16 v4, 0xd

    if-le v1, v4, :cond_d

    invoke-virtual {v2}, Lmaps/ac/av;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_f
    const/4 v1, 0x0

    goto :goto_6

    :cond_10
    new-instance v13, Lmaps/au/ah;

    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->k()F

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/aw;->l()F

    move-result v20

    move-object/from16 v0, p6

    iget-boolean v0, v0, Lmaps/av/a;->q:Z

    move/from16 v26, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v4

    move-object/from16 v17, v5

    move/from16 v21, p2

    move/from16 v22, v9

    move-object/from16 v23, v10

    move-object/from16 v24, v11

    move-object/from16 v25, v12

    invoke-direct/range {v13 .. v26}, Lmaps/au/ah;-><init>(Lmaps/ac/n;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/a;Lmaps/ac/a;FFZZLmaps/au/o;Lmaps/au/o;[Lmaps/au/ak;Z)V

    move-object v1, v13

    goto/16 :goto_0
.end method

.method public static a(Lmaps/ac/bg;ILmaps/am/b;Lmaps/ac/av;Lmaps/ac/av;ZLmaps/av/a;Lmaps/ar/a;Lmaps/as/c;)Lmaps/au/ah;
    .locals 17

    invoke-virtual/range {p0 .. p1}, Lmaps/ac/bg;->a(I)Lmaps/ac/ag;

    move-result-object v1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1}, Lmaps/ac/ag;->b()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-virtual {v1, v2}, Lmaps/ac/ag;->a(I)Lmaps/ac/ah;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/ah;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x0

    :goto_1
    return-object v1

    :cond_0
    invoke-virtual {v3}, Lmaps/ac/ah;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lmaps/ac/ah;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, Lmaps/au/o;->a(Lmaps/ac/ag;Lmaps/ac/n;Lmaps/ar/a;Lmaps/as/c;Lmaps/aj/ag;Lmaps/av/a;)Lmaps/au/o;

    move-result-object v12

    if-nez v12, :cond_4

    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    new-instance v16, Lmaps/au/ah;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v1, Lmaps/ac/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v8}, Lmaps/ac/a;-><init>(Lmaps/ac/av;IFLmaps/ac/av;FFF)V

    if-nez p4, :cond_5

    const/4 v7, 0x0

    :goto_2
    const/high16 v8, -0x40800000    # -1.0f

    const/high16 v9, -0x40800000    # -1.0f

    const/4 v11, 0x0

    const/4 v13, 0x0

    sget-object v14, Lmaps/au/ah;->G:[Lmaps/au/ak;

    move-object/from16 v0, p6

    iget-boolean v15, v0, Lmaps/av/a;->q:Z

    move-object/from16 v2, v16

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-object v5, v10

    move-object v6, v1

    move/from16 v10, p5

    invoke-direct/range {v2 .. v15}, Lmaps/au/ah;-><init>(Lmaps/ac/n;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/a;Lmaps/ac/a;FFZZLmaps/au/o;Lmaps/au/o;[Lmaps/au/ak;Z)V

    move-object/from16 v1, v16

    goto :goto_1

    :cond_5
    new-instance v2, Lmaps/ac/a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p4

    invoke-direct/range {v2 .. v9}, Lmaps/ac/a;-><init>(Lmaps/ac/av;IFLmaps/ac/av;FFF)V

    move-object v7, v2

    goto :goto_2
.end method

.method public static a(Lmaps/au/ap;Lmaps/aj/ag;Lmaps/ar/a;)Lmaps/au/ah;
    .locals 20

    invoke-interface/range {p0 .. p0}, Lmaps/au/ap;->c()Lmaps/ao/b;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/ao/b;->f()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface/range {p0 .. p0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v8

    invoke-virtual {v8}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v1

    invoke-virtual {v7, v1}, Lmaps/ao/b;->a(Lmaps/ac/bd;)Lmaps/ac/av;

    move-result-object v9

    invoke-virtual {v7}, Lmaps/ao/b;->g()Lmaps/au/ak;

    move-result-object v10

    invoke-virtual {v7}, Lmaps/ao/b;->h()Lmaps/ac/bl;

    move-result-object v5

    const/4 v1, 0x1

    new-array v14, v1, [Lmaps/au/ak;

    const/4 v1, 0x0

    aput-object v10, v14, v1

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v15, Lmaps/av/a;->t:Lmaps/av/a;

    new-instance v13, Lmaps/au/r;

    const/high16 v1, 0x40800000    # 4.0f

    invoke-direct {v13, v1}, Lmaps/au/r;-><init>(F)V

    invoke-virtual/range {p2 .. p2}, Lmaps/ar/a;->k()F

    move-result v1

    invoke-static {v5, v15, v1}, Lmaps/au/ah;->a(Lmaps/ac/bl;Lmaps/av/a;F)I

    move-result v4

    new-instance v1, Lmaps/au/t;

    invoke-virtual {v8}, Lmaps/ac/bt;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/av/a;->d:Lmaps/aj/ah;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/au/t;-><init>(Lmaps/aj/ag;Ljava/lang/String;ILmaps/ac/bl;Lmaps/aj/ah;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/au/t;

    invoke-virtual {v8}, Lmaps/ac/bt;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/av/a;->d:Lmaps/aj/ah;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/au/t;-><init>(Lmaps/aj/ag;Ljava/lang/String;ILmaps/ac/bl;Lmaps/aj/ah;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/au/t;

    invoke-virtual {v8}, Lmaps/ac/bt;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/av/a;->d:Lmaps/aj/ah;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/au/t;-><init>(Lmaps/aj/ag;Ljava/lang/String;ILmaps/ac/bl;Lmaps/aj/ah;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/au/t;

    invoke-virtual {v7}, Lmaps/ao/b;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/av/a;->d:Lmaps/aj/ah;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/au/t;-><init>(Lmaps/aj/ag;Ljava/lang/String;ILmaps/ac/bl;Lmaps/aj/ah;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p0}, Lmaps/au/ap;->b()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmaps/au/t;

    invoke-interface/range {p0 .. p0}, Lmaps/au/ap;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, Lmaps/av/a;->d:Lmaps/aj/ah;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lmaps/au/t;-><init>(Lmaps/aj/ag;Ljava/lang/String;ILmaps/ac/bl;Lmaps/aj/ah;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v13, Lmaps/au/o;

    iget-object v1, v10, Lmaps/au/ak;->b:Lmaps/au/s;

    sget-object v2, Lmaps/au/v;->c:Lmaps/au/v;

    invoke-direct {v13, v11, v1, v2}, Lmaps/au/o;-><init>(Ljava/util/ArrayList;Lmaps/au/s;Lmaps/au/v;)V

    new-instance v16, Lmaps/au/ah;

    new-instance v17, Lmaps/ac/l;

    const/4 v1, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Lmaps/ac/l;-><init>(Lmaps/ac/bl;)V

    const/16 v18, 0x0

    const/16 v19, 0x0

    new-instance v1, Lmaps/ac/a;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, v9

    invoke-direct/range {v1 .. v8}, Lmaps/ac/a;-><init>(Lmaps/ac/av;IFLmaps/ac/av;FFF)V

    const/4 v7, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    const/high16 v9, -0x40800000    # -1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-boolean v15, v15, Lmaps/av/a;->q:Z

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    move-object/from16 v4, v18

    move-object/from16 v5, v19

    move-object v6, v1

    invoke-direct/range {v2 .. v15}, Lmaps/au/ah;-><init>(Lmaps/ac/n;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/a;Lmaps/ac/a;FFZZLmaps/au/o;Lmaps/au/o;[Lmaps/au/ak;Z)V

    const/4 v1, 0x1

    move-object/from16 v0, v16

    iput-boolean v1, v0, Lmaps/au/ah;->w:Z

    move-object/from16 v1, v16

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lmaps/as/a;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/au/m;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v0, p1}, Lmaps/au/o;->a(Lmaps/as/a;)V

    :cond_0
    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v0, p1}, Lmaps/au/o;->a(Lmaps/as/a;)V

    :cond_1
    iget-object v0, p0, Lmaps/au/ah;->t:Lmaps/at/n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/ah;->t:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    :cond_2
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 10

    const/4 v5, 0x1

    const/high16 v4, 0x43b40000    # 360.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v1, 0x0

    iget-boolean v0, p0, Lmaps/au/ah;->y:Z

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    iget-boolean v0, p0, Lmaps/au/ah;->m:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/v/e;

    sget-object v2, Lmaps/v/g;->a:Lmaps/v/g;

    invoke-direct {v0, v2}, Lmaps/v/e;-><init>(Lmaps/v/g;)V

    iput-object v0, p0, Lmaps/au/ah;->B:Lmaps/v/e;

    :cond_0
    iput-boolean v5, p0, Lmaps/au/ah;->y:Z

    :cond_1
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v7

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {p2}, Lmaps/ar/a;->h()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v2}, Lmaps/ac/a;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v2}, Lmaps/ac/a;->c()F

    move-result v2

    invoke-virtual {p2}, Lmaps/ar/a;->n()F

    move-result v3

    sub-float/2addr v2, v3

    cmpl-float v2, v2, v1

    if-nez v2, :cond_3

    :cond_2
    invoke-virtual {p2}, Lmaps/ar/a;->o()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_3

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v0

    iget-object v2, p0, Lmaps/au/ah;->F:[F

    invoke-virtual {p2, v0, v2}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    iget-object v0, p0, Lmaps/au/ah;->F:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lmaps/au/ah;->F:[F

    aget v2, v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v0

    :cond_3
    iget v2, p0, Lmaps/au/ah;->x:F

    invoke-static {p1, p2, v0, v2}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    iget-object v0, p0, Lmaps/au/ah;->B:Lmaps/v/e;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lmaps/au/ah;->B:Lmaps/v/e;

    invoke-virtual {v0, p1}, Lmaps/v/e;->a(Lmaps/as/a;)I

    move-result v0

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_4

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/au/ah;->B:Lmaps/v/e;

    :cond_4
    :goto_0
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->c()F

    move-result v0

    invoke-virtual {p2}, Lmaps/ar/a;->n()F

    move-result v2

    sub-float/2addr v0, v2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_5

    add-float/2addr v0, v4

    :cond_5
    iget-object v2, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v2}, Lmaps/ac/a;->c()F

    move-result v2

    iget-boolean v3, p0, Lmaps/au/ah;->w:Z

    if-nez v3, :cond_d

    const/high16 v3, 0x42b40000    # 90.0f

    cmpl-float v3, v0, v3

    if-lez v3, :cond_d

    const/high16 v3, 0x43870000    # 270.0f

    cmpg-float v0, v0, v3

    if-gez v0, :cond_d

    const/high16 v0, 0x43340000    # 180.0f

    add-float/2addr v0, v2

    :goto_1
    cmpl-float v2, v0, v4

    if-ltz v2, :cond_6

    sub-float/2addr v0, v4

    :cond_6
    const/high16 v2, -0x40800000    # -1.0f

    invoke-interface {v7, v0, v1, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v0, -0x3d4c0000    # -90.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v7, v0, v2, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :goto_2
    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->a()F

    move-result v4

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->b()F

    move-result v3

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->a()F

    move-result v0

    div-float v2, v0, v9

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->b()F

    move-result v0

    div-float/2addr v0, v9

    neg-float v5, v2

    neg-float v8, v0

    invoke-interface {v7, v5, v1, v8}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v5, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v5, p1, p2, p3}, Lmaps/au/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    :goto_3
    iget-object v5, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    if-eqz v5, :cond_9

    sget-object v5, Lmaps/au/ai;->a:[I

    iget-object v8, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v8, v8, Lmaps/au/ak;->a:Lmaps/au/aj;

    invoke-virtual {v8}, Lmaps/au/aj;->ordinal()I

    move-result v8

    aget v5, v5, v8

    packed-switch v5, :pswitch_data_0

    move v3, v1

    move v0, v1

    :goto_4
    iget-object v5, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v5, v5, Lmaps/au/ak;->a:Lmaps/au/aj;

    sget-object v8, Lmaps/au/aj;->d:Lmaps/au/aj;

    if-eq v5, v8, :cond_7

    iget-object v5, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v5, v5, Lmaps/au/ak;->a:Lmaps/au/aj;

    sget-object v8, Lmaps/au/aj;->b:Lmaps/au/aj;

    if-ne v5, v8, :cond_8

    :cond_7
    sget-object v5, Lmaps/au/ai;->b:[I

    iget-object v8, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v8, v8, Lmaps/au/ak;->b:Lmaps/au/s;

    invoke-virtual {v8}, Lmaps/au/s;->ordinal()I

    move-result v8

    aget v5, v5, v8

    packed-switch v5, :pswitch_data_1

    :cond_8
    :goto_5
    invoke-interface {v7, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/au/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    :cond_9
    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void

    :cond_a
    iget v0, p0, Lmaps/au/ah;->p:I

    goto/16 :goto_0

    :cond_b
    invoke-static {v7, p2}, Lmaps/ap/o;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/ar/a;)V

    goto :goto_2

    :pswitch_0
    iget-object v3, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v3}, Lmaps/au/o;->a()F

    move-result v3

    div-float/2addr v3, v9

    sub-float v5, v2, v3

    iget-object v3, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v3}, Lmaps/au/o;->b()F

    move-result v3

    div-float/2addr v3, v9

    sub-float v3, v0, v3

    move v0, v5

    goto :goto_4

    :pswitch_1
    iget-object v3, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v3}, Lmaps/au/o;->b()F

    move-result v3

    div-float/2addr v3, v9

    sub-float v3, v0, v3

    move v0, v4

    goto :goto_4

    :pswitch_2
    iget-object v3, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v3}, Lmaps/au/o;->a()F

    move-result v3

    neg-float v5, v3

    iget-object v3, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v3}, Lmaps/au/o;->b()F

    move-result v3

    div-float/2addr v3, v9

    sub-float v3, v0, v3

    move v0, v5

    goto :goto_4

    :pswitch_3
    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->b()F

    move-result v0

    neg-float v3, v0

    move v0, v1

    goto :goto_4

    :pswitch_4
    move v0, v1

    goto :goto_4

    :pswitch_5
    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->b()F

    move-result v0

    neg-float v3, v0

    move v0, v4

    goto :goto_4

    :pswitch_6
    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->a()F

    move-result v0

    neg-float v0, v0

    iget-object v3, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v3}, Lmaps/au/o;->b()F

    move-result v3

    neg-float v3, v3

    goto/16 :goto_4

    :pswitch_7
    move v0, v4

    goto/16 :goto_4

    :pswitch_8
    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->a()F

    move-result v0

    neg-float v0, v0

    goto/16 :goto_4

    :pswitch_9
    const/high16 v0, -0x3ee00000    # -10.0f

    goto :goto_5

    :pswitch_a
    const/high16 v0, 0x41200000    # 10.0f

    add-float/2addr v0, v4

    iget-object v2, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v2}, Lmaps/au/o;->a()F

    move-result v2

    sub-float/2addr v0, v2

    goto/16 :goto_5

    :pswitch_b
    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->a()F

    move-result v0

    div-float/2addr v0, v9

    sub-float v0, v2, v0

    goto/16 :goto_5

    :cond_c
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_3

    :cond_d
    move v0, v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final a(Lmaps/ac/cy;)Z
    .locals 1

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmaps/ac/cy;->a(Lmaps/ac/av;)Z

    move-result v0

    return v0
.end method

.method public final a(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lmaps/au/ah;->A:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lmaps/au/ah;->z:[Lmaps/au/ak;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v1, p0, Lmaps/au/ah;->z:[Lmaps/au/ak;

    iget v2, p0, Lmaps/au/ah;->A:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/au/ah;->A:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v1, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    iget-object v2, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v2, v2, Lmaps/au/ak;->b:Lmaps/au/s;

    invoke-virtual {v1, v2}, Lmaps/au/o;->a(Lmaps/au/s;)V

    iput-object v4, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    invoke-virtual {p0, p1, p2}, Lmaps/au/ah;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lmaps/au/ah;->q:Lmaps/ac/a;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmaps/au/ah;->q:Lmaps/ac/a;

    iput-object v2, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    iput-object v4, p0, Lmaps/au/ah;->q:Lmaps/ac/a;

    iget-object v2, p0, Lmaps/au/ah;->z:[Lmaps/au/ak;

    array-length v2, v2

    if-le v2, v0, :cond_1

    iput v1, p0, Lmaps/au/ah;->A:I

    iget-object v2, p0, Lmaps/au/ah;->z:[Lmaps/au/ak;

    aget-object v1, v2, v1

    iput-object v1, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v1, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    iget-object v2, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v2, v2, Lmaps/au/ak;->b:Lmaps/au/s;

    invoke-virtual {v1, v2}, Lmaps/au/o;->a(Lmaps/au/s;)V

    :cond_1
    iput-object v4, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    invoke-virtual {p0, p1, p2}, Lmaps/au/ah;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final aj_()F
    .locals 1

    iget v0, p0, Lmaps/au/ah;->E:F

    return v0
.end method

.method public final ao_()Z
    .locals 1

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/au/ah;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/au/m;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v0, p1}, Lmaps/au/o;->b(Lmaps/as/a;)V

    :cond_0
    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v0, p1}, Lmaps/au/o;->b(Lmaps/as/a;)V

    :cond_1
    iget-object v0, p0, Lmaps/au/ah;->t:Lmaps/at/n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/ah;->t:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    :cond_2
    return-void
.end method

.method public b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 12

    const/high16 v11, 0x42b40000    # 90.0f

    const/4 v3, 0x0

    const/high16 v10, 0x43870000    # 270.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lmaps/au/m;->n:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmaps/au/ah;->o:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/au/ah;->x:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    invoke-direct {p0, p1}, Lmaps/au/ah;->a(Lmaps/ar/a;)F

    move-result v0

    iget v3, p0, Lmaps/au/ah;->x:F

    div-float/2addr v0, v3

    invoke-static {v0}, Lmaps/au/ah;->a(F)I

    move-result v3

    iput v3, p0, Lmaps/au/ah;->p:I

    const/high16 v3, 0x3e800000    # 0.25f

    cmpl-float v3, v0, v3

    if-ltz v3, :cond_1

    const/high16 v3, 0x40000000    # 2.0f

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/au/ah;->p:I

    invoke-virtual {p1}, Lmaps/ar/a;->o()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lmaps/ar/a;->n()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lmaps/ar/a;->p()F

    move-result v3

    iget-object v4, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lmaps/au/ah;->C:Z

    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    iget v4, p0, Lmaps/au/ah;->D:F

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_0

    :cond_3
    iget-object v4, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v4}, Lmaps/ac/a;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    iget v4, p0, Lmaps/au/ah;->D:F

    cmpl-float v4, v3, v4

    if-nez v4, :cond_5

    iget-object v0, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    iput-object v0, p0, Lmaps/au/ah;->s:Lmaps/ac/t;

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iput-boolean v0, p0, Lmaps/au/ah;->C:Z

    iput v3, p0, Lmaps/au/ah;->D:F

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->a()F

    move-result v0

    float-to-int v0, v0

    shr-int/lit8 v3, v0, 0x1

    iget-object v0, p0, Lmaps/au/ah;->b:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->b()F

    move-result v0

    float-to-int v0, v0

    shr-int/lit8 v0, v0, 0x1

    :goto_2
    iget-object v4, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    if-nez v4, :cond_9

    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->e:F

    int-to-float v3, v3

    iput v3, p0, Lmaps/au/ah;->f:F

    neg-int v3, v0

    int-to-float v3, v3

    iput v3, p0, Lmaps/au/ah;->g:F

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->h:F

    :cond_6
    :goto_3
    sget-object v0, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/l;

    iget-object v0, v0, Lmaps/al/l;->a:Lmaps/ac/av;

    invoke-virtual {p1}, Lmaps/ar/a;->r()Lmaps/ac/av;

    move-result-object v3

    iget-object v4, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v4}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-direct {p0, p1}, Lmaps/au/ah;->a(Lmaps/ar/a;)F

    move-result v0

    iput v0, p0, Lmaps/au/ah;->x:F

    iget-boolean v0, p0, Lmaps/au/ah;->v:Z

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lmaps/ar/a;->m()F

    move-result v0

    iget-object v3, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v3}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {p1, v3, v1}, Lmaps/ar/a;->a(Lmaps/ac/av;Z)F

    move-result v3

    div-float/2addr v0, v3

    iget v3, p0, Lmaps/au/ah;->e:F

    mul-float/2addr v3, v0

    iput v3, p0, Lmaps/au/ah;->e:F

    iget v3, p0, Lmaps/au/ah;->f:F

    mul-float/2addr v3, v0

    iput v3, p0, Lmaps/au/ah;->f:F

    iget v3, p0, Lmaps/au/ah;->g:F

    mul-float/2addr v3, v0

    iput v3, p0, Lmaps/au/ah;->g:F

    iget v3, p0, Lmaps/au/ah;->h:F

    mul-float/2addr v0, v3

    iput v0, p0, Lmaps/au/ah;->h:F

    :cond_7
    iget-object v0, p0, Lmaps/au/ah;->k:Lmaps/am/b;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/au/ah;->k:Lmaps/am/b;

    instance-of v0, v0, Lmaps/am/a;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/au/ah;->k:Lmaps/am/b;

    check-cast v0, Lmaps/am/a;

    invoke-virtual {v0}, Lmaps/am/a;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    instance-of v3, v0, Lmaps/ac/r;

    if-eqz v3, :cond_8

    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    move-result-object v3

    check-cast v0, Lmaps/ac/r;

    invoke-virtual {v3, v0}, Lmaps/ab/q;->e(Lmaps/ac/r;)Lmaps/ab/k;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v3, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v3}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v3

    iget-object v4, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v4}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v4

    invoke-virtual {v0, p1, v4}, Lmaps/ab/k;->a(Lmaps/ar/a;Lmaps/ac/av;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v3, v0}, Lmaps/ac/av;->b(I)V

    :cond_8
    iget-object v0, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    iput-object v0, p0, Lmaps/au/ah;->s:Lmaps/ac/t;

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->a()Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x0

    :goto_4
    iput-object v0, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    :goto_5
    iget-object v0, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    if-nez v0, :cond_0

    move v1, v2

    goto/16 :goto_0

    :cond_9
    iget-object v4, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v4}, Lmaps/au/o;->a()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v7, v4, 0x1

    iget-object v4, p0, Lmaps/au/ah;->c:Lmaps/au/o;

    invoke-virtual {v4}, Lmaps/au/o;->b()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v5, v4, 0x1

    if-le v3, v7, :cond_b

    move v6, v3

    :goto_6
    if-le v0, v5, :cond_c

    move v4, v0

    :goto_7
    sget-object v8, Lmaps/au/ai;->a:[I

    iget-object v9, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v9, v9, Lmaps/au/ak;->a:Lmaps/au/aj;

    invoke-virtual {v9}, Lmaps/au/aj;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    neg-int v0, v6

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->e:F

    int-to-float v0, v6

    iput v0, p0, Lmaps/au/ah;->f:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->g:F

    int-to-float v0, v4

    iput v0, p0, Lmaps/au/ah;->h:F

    :goto_8
    iget-object v0, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v0, v0, Lmaps/au/ak;->a:Lmaps/au/aj;

    sget-object v4, Lmaps/au/aj;->d:Lmaps/au/aj;

    if-eq v0, v4, :cond_a

    iget-object v0, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v0, v0, Lmaps/au/ak;->a:Lmaps/au/aj;

    sget-object v4, Lmaps/au/aj;->b:Lmaps/au/aj;

    if-ne v0, v4, :cond_6

    :cond_a
    sget-object v0, Lmaps/au/ai;->b:[I

    iget-object v4, p0, Lmaps/au/ah;->d:Lmaps/au/ak;

    iget-object v4, v4, Lmaps/au/ak;->b:Lmaps/au/s;

    invoke-virtual {v4}, Lmaps/au/s;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_3

    :pswitch_0
    neg-int v0, v3

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->e:F

    iget v0, p0, Lmaps/au/ah;->e:F

    mul-int/lit8 v4, v7, 0x2

    int-to-float v4, v4

    add-float/2addr v0, v4

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/au/ah;->f:F

    goto/16 :goto_3

    :cond_b
    move v6, v7

    goto :goto_6

    :cond_c
    move v4, v5

    goto :goto_7

    :pswitch_1
    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->g:F

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->h:F

    goto :goto_8

    :pswitch_2
    neg-int v0, v3

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->e:F

    mul-int/lit8 v0, v7, 0x2

    add-int/2addr v0, v3

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->f:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->g:F

    int-to-float v0, v4

    iput v0, p0, Lmaps/au/ah;->h:F

    goto :goto_8

    :pswitch_3
    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->g:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->h:F

    goto :goto_8

    :pswitch_4
    neg-int v0, v3

    mul-int/lit8 v5, v7, 0x2

    sub-int/2addr v0, v5

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->e:F

    int-to-float v0, v3

    iput v0, p0, Lmaps/au/ah;->f:F

    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->g:F

    int-to-float v0, v4

    iput v0, p0, Lmaps/au/ah;->h:F

    goto :goto_8

    :pswitch_5
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->e:F

    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->f:F

    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->g:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->h:F

    goto/16 :goto_8

    :pswitch_6
    neg-int v4, v3

    mul-int/lit8 v8, v7, 0x2

    sub-int/2addr v4, v8

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->e:F

    int-to-float v4, v3

    iput v4, p0, Lmaps/au/ah;->f:F

    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->g:F

    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->h:F

    goto/16 :goto_8

    :pswitch_7
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->e:F

    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v4, v3

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->f:F

    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->g:F

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->h:F

    goto/16 :goto_8

    :pswitch_8
    neg-int v4, v3

    mul-int/lit8 v8, v7, 0x2

    sub-int/2addr v4, v8

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->e:F

    int-to-float v4, v3

    iput v4, p0, Lmaps/au/ah;->f:F

    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, Lmaps/au/ah;->g:F

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->h:F

    goto/16 :goto_8

    :pswitch_9
    add-int/lit8 v0, v3, 0xa

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->f:F

    iget v0, p0, Lmaps/au/ah;->f:F

    mul-int/lit8 v4, v7, 0x2

    int-to-float v4, v4

    sub-float/2addr v0, v4

    neg-int v3, v3

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lmaps/au/ah;->e:F

    goto/16 :goto_3

    :pswitch_a
    neg-int v0, v6

    int-to-float v0, v0

    iput v0, p0, Lmaps/au/ah;->e:F

    int-to-float v0, v6

    iput v0, p0, Lmaps/au/ah;->f:F

    goto/16 :goto_3

    :cond_d
    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->c()F

    move-result v0

    cmpg-float v3, v0, v10

    if-gez v3, :cond_e

    add-float/2addr v0, v11

    move v4, v0

    :goto_9
    cmpg-float v0, v4, v10

    if-gez v0, :cond_f

    add-float v0, v4, v11

    move v3, v0

    :goto_a
    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Lmaps/ar/a;->a(Lmaps/ac/av;Z)F

    move-result v5

    sget-object v0, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/l;

    iget-object v6, v0, Lmaps/al/l;->a:Lmaps/ac/av;

    iget v7, p0, Lmaps/au/ah;->e:F

    invoke-virtual {p1, v7, v5}, Lmaps/ar/a;->a(FF)F

    move-result v7

    invoke-virtual {v6, v4, v7}, Lmaps/ac/av;->a(FF)V

    iget-object v7, v0, Lmaps/al/l;->b:Lmaps/ac/av;

    iget v8, p0, Lmaps/au/ah;->f:F

    invoke-virtual {p1, v8, v5}, Lmaps/ar/a;->a(FF)F

    move-result v8

    invoke-virtual {v7, v4, v8}, Lmaps/ac/av;->a(FF)V

    iget-object v4, v0, Lmaps/al/l;->c:Lmaps/ac/av;

    iget v8, p0, Lmaps/au/ah;->g:F

    invoke-virtual {p1, v8, v5}, Lmaps/ar/a;->a(FF)F

    move-result v8

    invoke-virtual {v4, v3, v8}, Lmaps/ac/av;->a(FF)V

    iget-object v8, v0, Lmaps/al/l;->d:Lmaps/ac/av;

    iget v9, p0, Lmaps/au/ah;->h:F

    invoke-virtual {p1, v9, v5}, Lmaps/ar/a;->a(FF)F

    move-result v5

    invoke-virtual {v8, v3, v5}, Lmaps/ac/av;->a(FF)V

    iget-object v3, v0, Lmaps/al/l;->e:Lmaps/ac/av;

    iget-object v5, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v5}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v5

    invoke-static {v5, v4, v3}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v0, v0, Lmaps/al/l;->f:Lmaps/ac/av;

    iget-object v4, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v4}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v4

    invoke-static {v4, v8, v0}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-virtual {v3, v6}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v4

    invoke-virtual {v3, v7}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {v0, v6}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v5

    invoke-virtual {v0, v7}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    invoke-static {v5, v0, v4, v3}, Lmaps/ac/t;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/t;

    move-result-object v0

    goto/16 :goto_4

    :cond_e
    sub-float/2addr v0, v10

    move v4, v0

    goto :goto_9

    :cond_f
    sub-float v0, v4, v10

    move v3, v0

    goto :goto_a

    :cond_10
    iget-object v0, p0, Lmaps/au/ah;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v0

    iget-object v3, p0, Lmaps/au/ah;->F:[F

    invoke-virtual {p1, v0, v3}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    iget-object v0, p0, Lmaps/au/ah;->F:[F

    aget v0, v0, v2

    iget-object v3, p0, Lmaps/au/ah;->F:[F

    aget v3, v3, v1

    iget v4, p0, Lmaps/au/ah;->e:F

    add-float/2addr v4, v0

    iget v5, p0, Lmaps/au/ah;->f:F

    add-float/2addr v0, v5

    iget v5, p0, Lmaps/au/ah;->g:F

    add-float/2addr v5, v3

    iget v6, p0, Lmaps/au/ah;->h:F

    add-float/2addr v3, v6

    invoke-virtual {p1, v4, v0, v5, v3}, Lmaps/ar/a;->a(FFFF)Lmaps/ac/t;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    goto/16 :goto_5

    :cond_11
    move v0, v2

    move v3, v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final c()Lmaps/ac/be;
    .locals 1

    iget-object v0, p0, Lmaps/au/ah;->r:Lmaps/ac/t;

    return-object v0
.end method
