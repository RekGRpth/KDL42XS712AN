.class public final Lmaps/au/af;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;
.implements Lmaps/ay/b;


# static fields
.field private static F:[F

.field private static G:Lmaps/ac/av;


# instance fields
.field private final A:I

.field private final B:I

.field private final C:I

.field private D:Z

.field private E:Lmaps/ac/cw;

.field private H:Lmaps/an/t;

.field private I:Lmaps/an/t;

.field private final J:Lmaps/ac/av;

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private a:Lmaps/ac/av;

.field private b:Landroid/graphics/Bitmap;

.field private final c:Landroid/graphics/Bitmap;

.field private d:I

.field private e:I

.field private final f:Ljava/lang/String;

.field private g:Lmaps/ay/r;

.field private h:Lmaps/as/b;

.field private i:Lmaps/as/b;

.field private j:Lmaps/at/i;

.field private k:Lmaps/at/i;

.field private l:F

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private final s:Z

.field private t:Z

.field private u:F

.field private v:F

.field private final w:F

.field private x:I

.field private y:I

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, Lmaps/au/af;->F:[F

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    sput-object v0, Lmaps/au/af;->G:Lmaps/ac/av;

    return-void
.end method

.method public constructor <init>(Lmaps/ac/av;Landroid/graphics/Bitmap;IILjava/lang/String;)V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lmaps/au/af;->r:Z

    iput-boolean v1, p0, Lmaps/au/af;->t:Z

    const/4 v0, 0x0

    iput v0, p0, Lmaps/au/af;->u:F

    iput v3, p0, Lmaps/au/af;->v:F

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/af;->J:Lmaps/ac/av;

    iput-object p1, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    iput-object p2, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    iput p3, p0, Lmaps/au/af;->d:I

    iput p4, p0, Lmaps/au/af;->e:I

    iput-object p5, p0, Lmaps/au/af;->f:Ljava/lang/String;

    iput-boolean v1, p0, Lmaps/au/af;->p:Z

    iput-boolean v2, p0, Lmaps/au/af;->s:Z

    iget-boolean v0, p0, Lmaps/au/af;->s:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->k()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->f()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x43700000    # 240.0f

    div-float/2addr v0, v1

    iput v0, p0, Lmaps/au/af;->w:F

    :goto_0
    iget v0, p0, Lmaps/au/af;->d:I

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->x:I

    iget v0, p0, Lmaps/au/af;->d:I

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0, v2}, Lmaps/au/af;->b(II)V

    iget-object v0, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->z:I

    iget-object v0, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->A:I

    :goto_1
    iget-object v0, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->B:I

    iget-object v0, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->C:I

    :goto_2
    iget v0, p0, Lmaps/au/af;->e:I

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->y:I

    return-void

    :cond_0
    iput v3, p0, Lmaps/au/af;->w:F

    goto :goto_0

    :cond_1
    iput v2, p0, Lmaps/au/af;->z:I

    iput v2, p0, Lmaps/au/af;->A:I

    goto :goto_1

    :cond_2
    iput v2, p0, Lmaps/au/af;->B:I

    iput v2, p0, Lmaps/au/af;->C:I

    goto :goto_2
.end method

.method private a(Lmaps/as/a;Landroid/graphics/Bitmap;)Lmaps/as/b;
    .locals 2

    iget-object v0, p0, Lmaps/au/af;->g:Lmaps/ay/r;

    invoke-virtual {v0}, Lmaps/ay/r;->j()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/as/b;->h()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/as/b;

    invoke-direct {v0, p1}, Lmaps/as/b;-><init>(Lmaps/as/a;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/as/b;->c(Z)V

    invoke-virtual {v0, p2}, Lmaps/as/b;->b(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lmaps/au/af;->g:Lmaps/ay/r;

    invoke-virtual {v1}, Lmaps/ay/r;->j()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(Lmaps/as/b;)Lmaps/at/i;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lmaps/at/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lmaps/at/i;-><init>(I)V

    invoke-virtual {p0}, Lmaps/as/b;->e()F

    move-result v1

    invoke-virtual {p0}, Lmaps/as/b;->f()F

    move-result v2

    invoke-virtual {v0, v3, v3}, Lmaps/at/i;->a(FF)V

    invoke-virtual {v0, v3, v2}, Lmaps/at/i;->a(FF)V

    invoke-virtual {v0, v1, v3}, Lmaps/at/i;->a(FF)V

    invoke-virtual {v0, v1, v2}, Lmaps/at/i;->a(FF)V

    return-object v0
.end method

.method private a(Lmaps/an/t;II)V
    .locals 4

    int-to-float v0, p2

    int-to-float v1, p3

    iget-object v2, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1, v2, v0, v1, v3}, Lmaps/an/t;->a(Lmaps/ac/av;FFF)V

    return-void
.end method

.method private b(I)I
    .locals 2

    iget-boolean v0, p0, Lmaps/au/af;->s:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->k()Z

    move-result v0

    if-nez v0, :cond_0

    int-to-float v0, p1

    iget v1, p0, Lmaps/au/af;->w:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p1

    :cond_0
    return p1
.end method

.method private static b(Lmaps/as/a;Landroid/graphics/Bitmap;)Lmaps/as/b;
    .locals 2

    new-instance v0, Lmaps/as/b;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lmaps/as/b;-><init>(Lmaps/as/a;Z)V

    invoke-virtual {v0, p1}, Lmaps/as/b;->b(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public static g()Lmaps/au/ag;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(FFLmaps/ar/a;)I
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    invoke-virtual {p3, v0}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v1

    iget-boolean v0, p0, Lmaps/au/af;->t:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/au/af;->u:F

    :goto_0
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v0, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    const/4 v3, 0x0

    aget v3, v1, v3

    int-to-float v3, v3

    sub-float v3, p1, v3

    const/4 v4, 0x1

    aget v4, v1, v4

    int-to-float v4, v4

    sub-float v4, p2, v4

    neg-float v4, v4

    mul-float v5, v3, v2

    mul-float v6, v4, v0

    sub-float/2addr v5, v6

    mul-float/2addr v0, v3

    mul-float/2addr v2, v4

    add-float/2addr v0, v2

    const/4 v2, 0x0

    aget v2, v1, v2

    int-to-float v2, v2

    add-float p1, v2, v5

    const/4 v2, 0x1

    aget v2, v1, v2

    int-to-float v2, v2

    sub-float p2, v2, v0

    :cond_0
    const/4 v0, 0x0

    aget v0, v1, v0

    iget v2, p0, Lmaps/au/af;->x:I

    sub-int/2addr v0, v2

    iget v2, p0, Lmaps/au/af;->z:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    const/4 v2, 0x1

    aget v1, v1, v2

    iget v2, p0, Lmaps/au/af;->y:I

    sub-int/2addr v1, v2

    iget v2, p0, Lmaps/au/af;->A:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v0, v0

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iget v2, p0, Lmaps/au/af;->z:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v1, v1

    sub-float v1, p2, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-int v1, v1

    iget v2, p0, Lmaps/au/af;->A:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget v0, p0, Lmaps/au/af;->u:F

    invoke-virtual {p3}, Lmaps/ar/a;->n()F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    sub-float/2addr v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(F)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/au/af;->u:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    invoke-virtual {v0, p1}, Lmaps/an/t;->a(I)V

    :cond_0
    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    invoke-virtual {v0, p1}, Lmaps/an/t;->a(I)V

    :cond_1
    return-void
.end method

.method public final a(II)V
    .locals 1

    iput p1, p0, Lmaps/au/af;->d:I

    iput p2, p0, Lmaps/au/af;->e:I

    iget v0, p0, Lmaps/au/af;->d:I

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->x:I

    iget v0, p0, Lmaps/au/af;->e:I

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->y:I

    return-void
.end method

.method public final declared-synchronized a(Lmaps/ac/av;)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iput-object p1, p0, Lmaps/au/af;->a:Lmaps/ac/av;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    if-nez v0, :cond_1

    iget v0, p0, Lmaps/au/af;->x:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/au/af;->z:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lmaps/au/af;->y:I

    int-to-float v1, v1

    iget v2, p0, Lmaps/au/af;->A:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/16 v2, 0x14

    new-array v2, v2, [F

    neg-float v3, v0

    aput v3, v2, v7

    aput v1, v2, v9

    const/4 v3, 0x2

    aput v5, v2, v3

    const/4 v3, 0x3

    aput v5, v2, v3

    aput v5, v2, v8

    const/4 v3, 0x5

    neg-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x6

    sub-float v4, v1, v6

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    const/16 v3, 0xa

    sub-float v4, v6, v0

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v1, v2, v3

    const/16 v3, 0xc

    aput v5, v2, v3

    const/16 v3, 0xd

    aput v6, v2, v3

    const/16 v3, 0xe

    aput v5, v2, v3

    const/16 v3, 0xf

    sub-float v0, v6, v0

    aput v0, v2, v3

    const/16 v0, 0x10

    sub-float/2addr v1, v6

    aput v1, v2, v0

    const/16 v0, 0x11

    aput v5, v2, v0

    const/16 v0, 0x12

    aput v6, v2, v0

    const/16 v0, 0x13

    aput v6, v2, v0

    new-instance v0, Lmaps/an/t;

    sget-object v1, Lmaps/ay/v;->o:Lmaps/ay/v;

    invoke-direct {v0, v1}, Lmaps/an/t;-><init>(Lmaps/ay/v;)V

    iput-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Marker for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/au/af;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/au/af;->h:Lmaps/as/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lmaps/au/af;->b(Lmaps/as/a;Landroid/graphics/Bitmap;)Lmaps/as/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/af;->h:Lmaps/as/b;

    :cond_0
    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    iget-object v1, p0, Lmaps/au/af;->h:Lmaps/as/b;

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Lmaps/an/q;)V

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    new-instance v1, Lmaps/an/f;

    invoke-direct {v1, v7}, Lmaps/an/f;-><init>(B)V

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Lmaps/an/q;)V

    new-array v0, v8, [F

    fill-array-data v0, :array_0

    new-instance v1, Lmaps/an/ah;

    invoke-direct {v1, v0}, Lmaps/an/ah;-><init>([F)V

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Lmaps/an/q;)V

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    new-instance v1, Lmaps/an/y;

    invoke-direct {v1, v2}, Lmaps/an/y;-><init>([F)V

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Lmaps/an/ap;)V

    invoke-virtual {p1}, Lmaps/as/a;->n()Lmaps/an/m;

    move-result-object v0

    iget-object v1, p0, Lmaps/au/af;->H:Lmaps/an/t;

    invoke-virtual {v0, v1}, Lmaps/an/m;->a(Lmaps/an/k;)V

    :cond_1
    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget v0, p0, Lmaps/au/af;->x:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/au/af;->B:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lmaps/au/af;->y:I

    int-to-float v1, v1

    iget v2, p0, Lmaps/au/af;->C:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/16 v2, 0x14

    new-array v2, v2, [F

    neg-float v3, v0

    aput v3, v2, v7

    aput v1, v2, v9

    const/4 v3, 0x2

    aput v5, v2, v3

    const/4 v3, 0x3

    aput v5, v2, v3

    aput v5, v2, v8

    const/4 v3, 0x5

    neg-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x6

    sub-float v4, v1, v6

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    const/16 v3, 0xa

    sub-float v4, v6, v0

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v1, v2, v3

    const/16 v3, 0xc

    aput v5, v2, v3

    const/16 v3, 0xd

    aput v6, v2, v3

    const/16 v3, 0xe

    aput v5, v2, v3

    const/16 v3, 0xf

    sub-float v0, v6, v0

    aput v0, v2, v3

    const/16 v0, 0x10

    sub-float/2addr v1, v6

    aput v1, v2, v0

    const/16 v0, 0x11

    aput v5, v2, v0

    const/16 v0, 0x12

    aput v6, v2, v0

    const/16 v0, 0x13

    aput v6, v2, v0

    new-instance v0, Lmaps/an/t;

    sget-object v1, Lmaps/ay/v;->n:Lmaps/ay/v;

    invoke-direct {v0, v1}, Lmaps/an/t;-><init>(Lmaps/ay/v;)V

    iput-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Marker shadow for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/au/af;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/au/af;->i:Lmaps/as/b;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lmaps/au/af;->b(Lmaps/as/a;Landroid/graphics/Bitmap;)Lmaps/as/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/af;->i:Lmaps/as/b;

    :cond_2
    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    iget-object v1, p0, Lmaps/au/af;->i:Lmaps/as/b;

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Lmaps/an/q;)V

    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    new-instance v1, Lmaps/an/f;

    invoke-direct {v1, v7}, Lmaps/an/f;-><init>(B)V

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Lmaps/an/q;)V

    new-array v0, v8, [F

    fill-array-data v0, :array_1

    new-instance v1, Lmaps/an/ah;

    invoke-direct {v1, v0}, Lmaps/an/ah;-><init>([F)V

    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Lmaps/an/q;)V

    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    new-instance v1, Lmaps/an/y;

    invoke-direct {v1, v2}, Lmaps/an/y;-><init>([F)V

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(Lmaps/an/ap;)V

    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    const/high16 v1, -0x3dcc0000    # -45.0f

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(F)V

    invoke-virtual {p1}, Lmaps/as/a;->n()Lmaps/an/m;

    move-result-object v0

    iget-object v1, p0, Lmaps/au/af;->I:Lmaps/an/t;

    invoke-virtual {v0, v1}, Lmaps/an/m;->a(Lmaps/an/k;)V

    :cond_3
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 10

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lmaps/au/af;->g:Lmaps/ay/r;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/au/af;->l:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/au/af;->r:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-enter p0

    :try_start_0
    iget v3, p0, Lmaps/au/af;->u:F

    iget-boolean v4, p0, Lmaps/au/af;->t:Z

    iget v5, p0, Lmaps/au/af;->v:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    iget-object v0, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lmaps/au/af;->i:Lmaps/as/b;

    if-nez v2, :cond_2

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v0}, Lmaps/au/af;->a(Lmaps/as/a;Landroid/graphics/Bitmap;)Lmaps/as/b;

    move-result-object v2

    iput-object v2, p0, Lmaps/au/af;->i:Lmaps/as/b;

    iget-object v2, p0, Lmaps/au/af;->i:Lmaps/as/b;

    invoke-static {v2}, Lmaps/au/af;->a(Lmaps/as/b;)Lmaps/at/i;

    move-result-object v2

    iput-object v2, p0, Lmaps/au/af;->k:Lmaps/at/i;

    :cond_2
    :goto_1
    sget-object v2, Lmaps/au/af;->G:Lmaps/ac/av;

    iget-object v7, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    invoke-virtual {v2, v7}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    invoke-virtual {p2}, Lmaps/ar/a;->h()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p2}, Lmaps/ar/a;->o()F

    move-result v2

    const/4 v7, 0x0

    cmpl-float v2, v2, v7

    if-nez v2, :cond_7

    if-eqz v4, :cond_6

    const/high16 v2, 0x42b40000    # 90.0f

    rem-float v2, v3, v2

    const/4 v7, 0x0

    cmpl-float v2, v2, v7

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_3

    sget-object v2, Lmaps/au/af;->G:Lmaps/ac/av;

    sget-object v7, Lmaps/au/af;->F:[F

    invoke-virtual {p2, v2, v7}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    sget-object v2, Lmaps/au/af;->F:[F

    const/4 v7, 0x0

    aget v2, v2, v7

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    sget-object v7, Lmaps/au/af;->F:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {p2, v2, v7}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v2

    sput-object v2, Lmaps/au/af;->G:Lmaps/ac/av;

    :cond_3
    iget-object v2, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    if-nez v2, :cond_8

    const-string v0, "UI"

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Null point for ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lmaps/au/af;->F:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lmaps/au/af;->F:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "); camera="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    iget-object v2, p0, Lmaps/au/af;->h:Lmaps/as/b;

    if-nez v2, :cond_2

    invoke-direct {p0, p1, v1}, Lmaps/au/af;->a(Lmaps/as/a;Landroid/graphics/Bitmap;)Lmaps/as/b;

    move-result-object v2

    iput-object v2, p0, Lmaps/au/af;->h:Lmaps/as/b;

    iget-object v2, p0, Lmaps/au/af;->h:Lmaps/as/b;

    invoke-static {v2}, Lmaps/au/af;->a(Lmaps/as/b;)Lmaps/at/i;

    move-result-object v2

    iput-object v2, p0, Lmaps/au/af;->j:Lmaps/at/i;

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    :cond_6
    invoke-virtual {p2}, Lmaps/ar/a;->n()F

    move-result v2

    sub-float/2addr v2, v3

    const/high16 v7, 0x42b40000    # 90.0f

    rem-float/2addr v2, v7

    const/4 v7, 0x0

    cmpl-float v2, v2, v7

    if-nez v2, :cond_7

    const/4 v2, 0x1

    goto/16 :goto_2

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_8
    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    monitor-enter p0

    :try_start_1
    sget-object v2, Lmaps/au/af;->G:Lmaps/ac/av;

    iget v7, p0, Lmaps/au/af;->l:F

    invoke-static {p1, p2, v2, v7}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v2

    if-nez v2, :cond_9

    const/high16 v1, -0x2d0000

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x10000

    invoke-interface {v6, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatex(IIII)V

    const/high16 v1, -0x5a0000

    const/high16 v2, 0x10000

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v6, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatex(IIII)V

    iget-object v1, p0, Lmaps/au/af;->k:Lmaps/at/i;

    invoke-virtual {v1, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/af;->i:Lmaps/as/b;

    invoke-virtual {v1, v6}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget v2, p0, Lmaps/au/af;->B:I

    iget v1, p0, Lmaps/au/af;->C:I

    :goto_3
    shl-int/lit8 v2, v2, 0x10

    shl-int/lit8 v1, v1, 0x10

    iget v3, p0, Lmaps/au/af;->d:I

    shl-int/lit8 v3, v3, 0x10

    neg-int v3, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/2addr v3, v4

    const/high16 v4, -0x10000

    iget v5, p0, Lmaps/au/af;->e:I

    shl-int/lit8 v5, v5, 0x10

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int v0, v5, v0

    add-int/2addr v0, v4

    const/high16 v4, 0x10000

    invoke-interface {v6, v2, v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    const/4 v1, 0x0

    invoke-interface {v6, v3, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatex(III)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {v6, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_9
    if-eqz v4, :cond_a

    invoke-static {v6, p2}, Lmaps/ap/o;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/ar/a;)V

    :goto_4
    const/4 v0, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    invoke-interface {v6, v3, v0, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget-object v0, p0, Lmaps/au/af;->j:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/af;->h:Lmaps/as/b;

    invoke-virtual {v0, v6}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget v2, p0, Lmaps/au/af;->z:I

    iget v0, p0, Lmaps/au/af;->A:I

    const/4 v3, 0x1

    const/16 v4, 0x303

    invoke-interface {v6, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v7, 0x2100

    invoke-interface {v6, v3, v4, v7}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-interface {v6, v5, v5, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_3

    :cond_a
    const/high16 v0, -0x3d4c0000    # -90.0f

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-interface {v6, v0, v2, v4, v7}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    goto :goto_4
.end method

.method public final a(Lmaps/ay/r;)V
    .locals 0

    iput-object p1, p0, Lmaps/au/af;->g:Lmaps/ay/r;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/au/af;->q:Z

    return-void
.end method

.method public final declared-synchronized a(Lmaps/ar/a;)Z
    .locals 11

    const/4 v10, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/au/af;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return v10

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    :try_start_1
    invoke-virtual {p1}, Lmaps/ar/a;->m()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmaps/ar/a;->a(FF)F

    move-result v0

    iput v0, p0, Lmaps/au/af;->l:F

    iget-object v0, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    sget-object v1, Lmaps/au/af;->F:[F

    invoke-virtual {p1, v0, v1}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    sget-object v0, Lmaps/au/af;->F:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    const/high16 v1, 0x47800000    # 65536.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lmaps/au/af;->m:I

    iget v0, p0, Lmaps/au/af;->u:F

    neg-float v0, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    iget v2, p0, Lmaps/au/af;->u:F

    neg-float v2, v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    iget v4, p0, Lmaps/au/af;->M:I

    iget v5, p0, Lmaps/au/af;->x:I

    sub-int/2addr v4, v5

    iget v5, p0, Lmaps/au/af;->N:I

    iget v6, p0, Lmaps/au/af;->y:I

    sub-int/2addr v5, v6

    neg-int v5, v5

    int-to-double v6, v4

    mul-double/2addr v6, v0

    int-to-double v8, v5

    mul-double/2addr v8, v2

    sub-double/2addr v6, v8

    int-to-double v8, v4

    mul-double/2addr v2, v8

    int-to-double v4, v5

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    iget-boolean v2, p0, Lmaps/au/af;->t:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lmaps/au/af;->o()Lmaps/ac/av;

    move-result-object v2

    double-to-int v3, v6

    double-to-int v0, v0

    iget-object v1, p0, Lmaps/au/af;->J:Lmaps/ac/av;

    invoke-static {p1, v2, v3, v0, v1}, Lmaps/ay/o;->a(Lmaps/ar/a;Lmaps/ac/av;IILmaps/ac/av;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-virtual {p1}, Lmaps/ar/a;->m()F

    iget-object v2, p0, Lmaps/au/af;->J:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v3

    iget v4, p0, Lmaps/au/af;->l:F

    float-to-double v4, v4

    mul-double/2addr v4, v6

    double-to-int v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    invoke-virtual {v4}, Lmaps/ac/av;->g()I

    move-result v4

    iget v5, p0, Lmaps/au/af;->l:F

    float-to-double v5, v5

    mul-double/2addr v0, v5

    double-to-int v0, v0

    add-int/2addr v0, v4

    iget-object v1, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    invoke-virtual {v1}, Lmaps/ac/av;->h()I

    move-result v1

    invoke-virtual {v2, v3, v0, v1}, Lmaps/ac/av;->a(III)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final ap_()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/au/af;->p:Z

    return v0
.end method

.method public final declared-synchronized b(F)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/au/af;->v:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(II)V
    .locals 1

    iput p1, p0, Lmaps/au/af;->K:I

    iput p2, p0, Lmaps/au/af;->L:I

    iget v0, p0, Lmaps/au/af;->K:I

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->M:I

    iget v0, p0, Lmaps/au/af;->L:I

    invoke-direct {p0, v0}, Lmaps/au/af;->b(I)I

    move-result v0

    iput v0, p0, Lmaps/au/af;->N:I

    return-void
.end method

.method public final b(Lmaps/as/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->n()Lmaps/an/m;

    move-result-object v0

    iget-object v1, p0, Lmaps/au/af;->H:Lmaps/an/t;

    invoke-virtual {v0, v1}, Lmaps/an/m;->b(Lmaps/an/k;)V

    :cond_0
    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lmaps/as/a;->n()Lmaps/an/m;

    move-result-object v0

    iget-object v1, p0, Lmaps/au/af;->I:Lmaps/an/t;

    invoke-virtual {v0, v1}, Lmaps/an/m;->b(Lmaps/an/k;)V

    :cond_1
    return-void
.end method

.method public final declared-synchronized b(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/au/af;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/au/af;->q:Z

    return v0
.end method

.method public final declared-synchronized b(FFLmaps/ar/a;)Z
    .locals 2

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    invoke-virtual {p0, p1, p2, p3}, Lmaps/au/af;->a(FFLmaps/ar/a;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lmaps/ar/a;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/au/af;->r:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v2

    iget-object v3, p0, Lmaps/au/af;->E:Lmaps/ac/cw;

    invoke-virtual {v2, v3}, Lmaps/ac/cw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v0, p0, Lmaps/au/af;->D:Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lmaps/au/af;->a:Lmaps/ac/av;

    invoke-virtual {p1, v2}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v2

    const/4 v3, 0x0

    aget v3, v2, v3

    iget v4, p0, Lmaps/au/af;->x:I

    sub-int v4, v3, v4

    iget v3, p0, Lmaps/au/af;->z:I

    add-int/2addr v3, v4

    const/4 v5, 0x1

    aget v2, v2, v5

    iget v5, p0, Lmaps/au/af;->y:I

    sub-int v5, v2, v5

    iget v2, p0, Lmaps/au/af;->A:I

    add-int/2addr v2, v5

    iget-object v6, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_3

    iget v6, p0, Lmaps/au/af;->B:I

    add-int/2addr v6, v4

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v6, p0, Lmaps/au/af;->C:I

    add-int/2addr v6, v5

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_3
    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v6

    if-ge v4, v6, :cond_4

    if-ltz v3, :cond_4

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v3

    if-ge v5, v3, :cond_4

    if-ltz v2, :cond_4

    move v0, v1

    :cond_4
    iput-boolean v0, p0, Lmaps/au/af;->D:Z

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/af;->E:Lmaps/ac/cw;

    iget-boolean v0, p0, Lmaps/au/af;->D:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/au/af;->u:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/au/af;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lmaps/au/af;

    iget v0, p0, Lmaps/au/af;->m:I

    iget v1, p1, Lmaps/au/af;->m:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/au/af;->m:I

    iget v1, p1, Lmaps/au/af;->m:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/au/af;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/au/af;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/au/af;->v:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()Lmaps/ay/r;
    .locals 1

    iget-object v0, p0, Lmaps/au/af;->g:Lmaps/ay/r;

    return-object v0
.end method

.method public final i()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lmaps/au/af;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final j()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lmaps/au/af;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final k()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public final l()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/au/af;->n:Z

    iget-boolean v0, p0, Lmaps/au/af;->o:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lmaps/au/af;->o:Z

    iget-object v0, p0, Lmaps/au/af;->g:Lmaps/ay/r;

    invoke-virtual {v0, p0}, Lmaps/ay/r;->c(Lmaps/au/af;)V

    :cond_0
    return-void
.end method

.method public final m()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/au/af;->n:Z

    return-void
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/au/af;->n:Z

    return v0
.end method

.method public final declared-synchronized o()Lmaps/ac/av;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/au/af;->a:Lmaps/ac/av;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final p()Lmaps/ac/ad;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final q()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/au/af;->J:Lmaps/ac/av;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/au/af;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final s()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/au/af;->o:Z

    return-void
.end method

.method public final t()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/au/af;->h:Lmaps/as/b;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/au/af;->h:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    iget-object v0, p0, Lmaps/au/af;->h:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->j()I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/au/af;->h:Lmaps/as/b;

    :cond_0
    return v0
.end method

.method public final u()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/au/af;->i:Lmaps/as/b;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/au/af;->i:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    iget-object v0, p0, Lmaps/au/af;->i:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->j()I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/au/af;->i:Lmaps/as/b;

    :cond_0
    return v0
.end method

.method public final v()V
    .locals 3

    iget-object v0, p0, Lmaps/au/af;->H:Lmaps/an/t;

    iget v1, p0, Lmaps/au/af;->z:I

    iget v2, p0, Lmaps/au/af;->A:I

    invoke-direct {p0, v0, v1, v2}, Lmaps/au/af;->a(Lmaps/an/t;II)V

    iget-object v0, p0, Lmaps/au/af;->I:Lmaps/an/t;

    iget v1, p0, Lmaps/au/af;->B:I

    iget v2, p0, Lmaps/au/af;->C:I

    invoke-direct {p0, v0, v1, v2}, Lmaps/au/af;->a(Lmaps/an/t;II)V

    return-void
.end method
