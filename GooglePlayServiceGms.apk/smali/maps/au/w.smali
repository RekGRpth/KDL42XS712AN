.class public final Lmaps/au/w;
.super Lmaps/au/i;


# static fields
.field private static b:F

.field private static final o:Ljava/util/Map;


# instance fields
.field private final c:Lmaps/ac/bt;

.field private final d:Ljava/util/List;

.field private final e:Lmaps/at/n;

.field private final f:Lmaps/at/d;

.field private final g:Lmaps/at/i;

.field private h:Lmaps/as/b;

.field private final i:I

.field private final j:I

.field private final k:Z

.field private final l:F

.field private m:Lmaps/ac/o;

.field private final n:Lmaps/au/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lmaps/au/w;->b:F

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lmaps/au/w;->o:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lmaps/ac/bt;Lmaps/au/y;Ljava/util/List;Ljava/util/Set;)V
    .locals 13

    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lmaps/au/i;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, Lmaps/au/w;->c:Lmaps/ac/bt;

    new-instance v1, Lmaps/at/p;

    iget v2, p2, Lmaps/au/y;->a:I

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lmaps/at/p;-><init>(IB)V

    iput-object v1, p0, Lmaps/au/w;->e:Lmaps/at/n;

    new-instance v1, Lmaps/at/f;

    iget v2, p2, Lmaps/au/y;->b:I

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lmaps/at/f;-><init>(IB)V

    iput-object v1, p0, Lmaps/au/w;->f:Lmaps/at/d;

    new-instance v1, Lmaps/at/k;

    iget v2, p2, Lmaps/au/y;->a:I

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lmaps/at/k;-><init>(IB)V

    iput-object v1, p0, Lmaps/au/w;->g:Lmaps/at/i;

    move-object/from16 v0, p3

    iput-object v0, p0, Lmaps/au/w;->d:Ljava/util/List;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v1, p0, Lmaps/au/w;->d:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/au/z;

    invoke-virtual {v1}, Lmaps/au/z;->e()I

    move-result v5

    const/4 v1, 0x1

    if-le v5, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lmaps/au/w;->k:Z

    iget-boolean v1, p0, Lmaps/au/w;->k:Z

    if-eqz v1, :cond_1

    div-int/lit8 v1, v5, 0x10

    int-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    mul-int/lit8 v3, v1, 0x2

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v1, p0, Lmaps/au/w;->d:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/au/z;

    invoke-virtual {v1}, Lmaps/au/z;->d()F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    add-int/lit8 v3, v3, 0x0

    move v1, v4

    :goto_1
    const/4 v4, 0x1

    shl-int/2addr v4, v1

    if-ge v4, v3, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lmaps/au/w;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/au/z;

    invoke-virtual {v1}, Lmaps/au/z;->e()I

    move-result v6

    invoke-virtual {v1}, Lmaps/au/z;->d()F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    add-int v2, v3, v6

    move v3, v2

    move v2, v1

    goto :goto_2

    :cond_2
    iput v1, p0, Lmaps/au/w;->i:I

    sget v1, Lmaps/au/w;->b:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Landroid/util/FloatMath;->ceil(F)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    add-float/2addr v1, v2

    float-to-int v2, v1

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lmaps/as/b;->c(II)I

    move-result v2

    iput v2, p0, Lmaps/au/w;->j:I

    iget v2, p0, Lmaps/au/w;->j:I

    int-to-float v2, v2

    div-float/2addr v2, v1

    iput v2, p0, Lmaps/au/w;->l:F

    new-instance v2, Lmaps/au/ab;

    iget-object v3, p0, Lmaps/au/w;->d:Ljava/util/List;

    iget v4, p0, Lmaps/au/w;->l:F

    iget v5, p0, Lmaps/au/w;->i:I

    iget-boolean v6, p0, Lmaps/au/w;->k:Z

    invoke-direct {v2, v3, v4, v5, v6}, Lmaps/au/ab;-><init>(Ljava/util/List;FIZ)V

    iput-object v2, p0, Lmaps/au/w;->n:Lmaps/au/ab;

    invoke-virtual {p1}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bd;->f()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x43800000    # 256.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v3, v1, v2

    invoke-static {}, Lmaps/al/h;->a()Lmaps/al/h;

    move-result-object v1

    const/4 v2, 0x0

    move v11, v2

    :goto_3
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    if-ge v11, v2, :cond_5

    invoke-virtual {p1}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lmaps/au/z;

    invoke-virtual {v4}, Lmaps/au/z;->a()Lmaps/ac/az;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/az;->b()I

    move-result v12

    const/4 v5, 0x2

    if-lt v12, v5, :cond_3

    invoke-virtual {v4}, Lmaps/au/z;->c()Lmaps/ac/o;

    move-result-object v5

    iput-object v5, p0, Lmaps/au/w;->m:Lmaps/ac/o;

    invoke-virtual {v6}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v5

    invoke-virtual {v6}, Lmaps/ac/bd;->f()I

    move-result v6

    iget-boolean v7, p0, Lmaps/au/w;->k:Z

    if-nez v7, :cond_4

    const/4 v4, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v8, p0, Lmaps/au/w;->e:Lmaps/at/n;

    iget-object v9, p0, Lmaps/au/w;->f:Lmaps/at/d;

    const/4 v10, 0x0

    invoke-virtual/range {v1 .. v10}, Lmaps/al/h;->a(Lmaps/ac/az;FZLmaps/ac/av;IFLmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V

    iget v2, p0, Lmaps/au/w;->i:I

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v11, v4, v5

    iget-object v5, p0, Lmaps/au/w;->g:Lmaps/at/i;

    invoke-static {v12, v2, v4, v5}, Lmaps/al/h;->a(II[ILmaps/at/j;)V

    :cond_3
    :goto_4
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_3

    :cond_4
    invoke-virtual {v4}, Lmaps/au/z;->e()I

    move-result v4

    const/high16 v7, 0x45000000    # 2048.0f

    int-to-float v4, v4

    div-float/2addr v7, v4

    const/4 v4, 0x1

    iget-object v8, p0, Lmaps/au/w;->e:Lmaps/at/n;

    iget-object v9, p0, Lmaps/au/w;->f:Lmaps/at/d;

    iget-object v10, p0, Lmaps/au/w;->g:Lmaps/at/i;

    invoke-virtual/range {v1 .. v10}, Lmaps/al/h;->a(Lmaps/ac/az;FZLmaps/ac/av;IFLmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lmaps/au/w;->c:Lmaps/ac/bt;

    return-void

    :cond_6
    move v1, v4

    goto/16 :goto_1
.end method

.method synthetic constructor <init>(Lmaps/ac/bt;Lmaps/au/y;Ljava/util/List;Ljava/util/Set;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lmaps/au/w;-><init>(Lmaps/ac/bt;Lmaps/au/y;Ljava/util/List;Ljava/util/Set;)V

    return-void
.end method

.method private static declared-synchronized a(Lmaps/as/a;Lmaps/au/ab;)Lmaps/as/b;
    .locals 2

    const-class v1, Lmaps/au/w;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/au/w;->o:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(F)V
    .locals 0

    sput p0, Lmaps/au/w;->b:F

    return-void
.end method

.method private static a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V
    .locals 18

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    move/from16 v0, p6

    int-to-float v2, v0

    div-float/2addr v1, v2

    const/high16 v2, 0x41800000    # 16.0f

    mul-float v12, v1, v2

    const/4 v1, 0x0

    move v7, v1

    :goto_0
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v7, v1, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/au/aa;

    iget v2, v1, Lmaps/au/aa;->a:F

    mul-float v2, v2, p5

    add-float v13, p3, v2

    iget v2, v1, Lmaps/au/aa;->b:F

    mul-float v2, v2, p5

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float v14, v2, v3

    iget v2, v1, Lmaps/au/aa;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v15, v1, Lmaps/au/aa;->d:[I

    if-nez v15, :cond_2

    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_1

    sub-float v2, v13, v14

    const/high16 v1, 0x3f000000    # 0.5f

    add-float v3, p4, v1

    add-float v4, v13, v14

    const/high16 v1, 0x3f000000    # 0.5f

    add-float v5, p4, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    :goto_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_0

    :cond_1
    sub-float v2, v13, v14

    const/4 v3, 0x0

    add-float v4, v13, v14

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v5, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    const/4 v2, 0x1

    move/from16 v4, p4

    :goto_2
    move/from16 v0, p6

    if-ge v3, v0, :cond_0

    const/4 v1, 0x0

    move v8, v1

    move v1, v2

    move v2, v3

    move v3, v4

    :goto_3
    array-length v4, v15

    rem-int/lit8 v4, v4, 0x2

    if-gt v8, v4, :cond_6

    array-length v0, v15

    move/from16 v16, v0

    const/4 v4, 0x0

    move v11, v4

    move v9, v1

    move v10, v2

    :goto_4
    move/from16 v0, v16

    if-ge v11, v0, :cond_5

    aget v17, v15, v11

    move/from16 v0, v17

    int-to-float v1, v0

    const/high16 v2, 0x41800000    # 16.0f

    div-float/2addr v1, v2

    mul-float/2addr v1, v12

    add-float v5, v3, v1

    if-eqz v9, :cond_3

    sub-float v2, v13, v14

    add-float v4, v13, v14

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    if-nez v9, :cond_4

    const/4 v1, 0x1

    :goto_5
    add-int v10, v10, v17

    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move v9, v1

    move v3, v5

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    goto :goto_5

    :cond_5
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v2, v10

    move v1, v9

    goto :goto_3

    :cond_6
    move v4, v3

    move v3, v2

    move v2, v1

    goto :goto_2

    :cond_7
    return-void
.end method

.method private static declared-synchronized a(Lmaps/as/a;Lmaps/au/ab;Lmaps/as/b;)V
    .locals 3

    const-class v1, Lmaps/au/w;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/au/w;->o:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    sget-object v2, Lmaps/au/w;->o:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Lmaps/ac/az;Lmaps/au/y;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/ac/az;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-gtz v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    mul-int/lit8 v3, v2, 0x5

    iget v4, p1, Lmaps/au/y;->a:I

    if-lez v4, :cond_2

    iget v4, p1, Lmaps/au/y;->a:I

    add-int/2addr v4, v3

    const/16 v5, 0x4000

    if-gt v4, v5, :cond_0

    :cond_2
    iget v4, p1, Lmaps/au/y;->a:I

    add-int/2addr v3, v4

    iput v3, p1, Lmaps/au/y;->a:I

    iget v3, p1, Lmaps/au/y;->b:I

    mul-int/lit8 v2, v2, 0x3

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x3

    invoke-virtual {p0}, Lmaps/ac/az;->e()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x3

    :cond_3
    add-int/2addr v0, v2

    add-int/2addr v0, v3

    iput v0, p1, Lmaps/au/y;->b:I

    move v0, v1

    goto :goto_0
.end method

.method private static declared-synchronized b(Lmaps/as/a;Lmaps/au/ab;)V
    .locals 2

    const-class v1, Lmaps/au/w;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/au/w;->o:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c(Lmaps/as/a;)V
    .locals 5

    const/high16 v4, 0x10000

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method

.method private d(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->j()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/au/w;->n:Lmaps/au/ab;

    invoke-static {p1, v0}, Lmaps/au/w;->b(Lmaps/as/a;Lmaps/au/ab;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lmaps/au/w;->e:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/au/w;->f:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/w;->g:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/w;->h:Lmaps/as/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/au/w;->h:Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->k()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/au/w;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/w;->e:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/w;->f:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/w;->g:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 13

    invoke-virtual/range {p3 .. p3}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aj/ad;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/au/w;->m:Lmaps/ac/o;

    if-eqz v0, :cond_2

    invoke-virtual/range {p3 .. p3}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/au/w;->m:Lmaps/ac/o;

    invoke-virtual {v0, v1}, Lmaps/aj/ad;->b(Lmaps/ac/o;)Lmaps/aj/af;

    move-result-object v0

    if-eqz v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v1

    iget-object v2, p0, Lmaps/au/w;->c:Lmaps/ac/bt;

    invoke-virtual {v2}, Lmaps/ac/bt;->b()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v10

    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    if-nez v0, :cond_5

    iget-object v0, p0, Lmaps/au/w;->n:Lmaps/au/ab;

    invoke-static {p1, v0}, Lmaps/au/w;->a(Lmaps/as/a;Lmaps/au/ab;)Lmaps/as/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    if-nez v0, :cond_7

    iget v0, p0, Lmaps/au/w;->j:I

    const/4 v1, 0x1

    iget v2, p0, Lmaps/au/w;->i:I

    shl-int/2addr v1, v2

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    iget-object v12, p0, Lmaps/au/w;->d:Ljava/util/List;

    iget v5, p0, Lmaps/au/w;->l:F

    iget-boolean v0, p0, Lmaps/au/w;->k:Z

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v8, v0

    :goto_1
    const/4 v0, 0x0

    move v9, v0

    :goto_2
    if-ge v9, v8, :cond_4

    invoke-interface {v12, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lmaps/au/z;

    invoke-virtual {v7}, Lmaps/au/z;->e()I

    move-result v6

    int-to-float v4, v9

    invoke-virtual {v7}, Lmaps/au/z;->f()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static/range {v0 .. v6}, Lmaps/au/w;->a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V

    invoke-virtual {v7}, Lmaps/au/z;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static/range {v0 .. v6}, Lmaps/au/w;->a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_2

    :cond_3
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    move v8, v0

    goto :goto_1

    :cond_4
    new-instance v0, Lmaps/as/b;

    invoke-direct {v0, p1}, Lmaps/as/b;-><init>(Lmaps/as/a;)V

    iput-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    iget-object v0, p0, Lmaps/au/w;->n:Lmaps/au/ab;

    iget-object v1, p0, Lmaps/au/w;->h:Lmaps/as/b;

    invoke-static {p1, v0, v1}, Lmaps/au/w;->a(Lmaps/as/a;Lmaps/au/ab;Lmaps/as/b;)V

    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/as/b;->c(Z)V

    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/as/b;->b(Z)V

    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    invoke-virtual {v0, v11}, Lmaps/as/b;->b(Landroid/graphics/Bitmap;)V

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    :goto_3
    if-eqz v10, :cond_6

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    int-to-float v0, v10

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v2, v0

    iget-boolean v0, p0, Lmaps/au/w;->k:Z

    if-eqz v0, :cond_8

    int-to-float v0, v10

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v0, v3

    :goto_4
    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/high16 v1, -0x41000000    # -0.5f

    const/high16 v2, -0x41000000    # -0.5f

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :cond_6
    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/au/w;->e:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/w;->g:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/w;->f:Lmaps/at/d;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    if-eqz v10, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lmaps/au/w;->h:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->h()V

    goto/16 :goto_3

    :cond_8
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_4
.end method

.method public final b()I
    .locals 3

    const/16 v0, 0x260

    iget-object v1, p0, Lmaps/au/w;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/z;

    invoke-virtual {v0}, Lmaps/au/z;->h()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/au/w;->e:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->d()I

    move-result v0

    iget-object v2, p0, Lmaps/au/w;->f:Lmaps/at/d;

    invoke-virtual {v2}, Lmaps/at/d;->d()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/au/w;->g:Lmaps/at/i;

    invoke-virtual {v2}, Lmaps/at/i;->c()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/au/w;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/w;->e:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/w;->f:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/w;->g:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    return-void
.end method
