.class public final enum Lmaps/au/v;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/au/v;

.field public static final enum b:Lmaps/au/v;

.field public static final enum c:Lmaps/au/v;

.field private static final synthetic d:[Lmaps/au/v;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/au/v;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v2}, Lmaps/au/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/v;->a:Lmaps/au/v;

    new-instance v0, Lmaps/au/v;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, Lmaps/au/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/v;->b:Lmaps/au/v;

    new-instance v0, Lmaps/au/v;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, Lmaps/au/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/au/v;->c:Lmaps/au/v;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/au/v;

    sget-object v1, Lmaps/au/v;->a:Lmaps/au/v;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/au/v;->b:Lmaps/au/v;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/au/v;->c:Lmaps/au/v;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/au/v;->d:[Lmaps/au/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lmaps/au/v;
    .locals 2

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown alignment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, Lmaps/au/v;->a:Lmaps/au/v;

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lmaps/au/v;->b:Lmaps/au/v;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lmaps/au/v;->c:Lmaps/au/v;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/au/v;
    .locals 1

    const-class v0, Lmaps/au/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/au/v;

    return-object v0
.end method

.method public static values()[Lmaps/au/v;
    .locals 1

    sget-object v0, Lmaps/au/v;->d:[Lmaps/au/v;

    invoke-virtual {v0}, [Lmaps/au/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/au/v;

    return-object v0
.end method
