.class public final Lmaps/au/at;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/au/ap;


# static fields
.field private static K:[F

.field public static final a:Lmaps/ax/a;

.field private static final b:Lmaps/au/d;

.field private static final c:Lmaps/au/d;

.field private static final d:Lmaps/au/d;

.field private static final e:Lmaps/au/d;

.field private static final f:Lmaps/au/d;

.field private static final g:Lmaps/au/d;


# instance fields
.field private A:Lmaps/ao/b;

.field private B:I

.field private C:[F

.field private D:J

.field private volatile E:I

.field private F:Lmaps/am/b;

.field private G:Ljava/lang/Boolean;

.field private H:J

.field private I:J

.field private J:J

.field private L:Z

.field private h:Lmaps/al/m;

.field private i:Lmaps/al/m;

.field private final j:[F

.field private k:[Lmaps/au/al;

.field private l:[Lmaps/au/i;

.field private m:[Lmaps/au/w;

.field private n:[[Lmaps/au/am;

.field private o:[Lmaps/au/i;

.field private p:[Lmaps/au/w;

.field private q:[Lmaps/au/e;

.field private r:[Lmaps/au/ar;

.field private s:Lmaps/au/m;

.field private t:Ljava/util/ArrayList;

.field private u:Ljava/util/Set;

.field private final v:Lmaps/ac/bt;

.field private final w:Lmaps/ac/bd;

.field private final x:Ljava/util/HashSet;

.field private y:I

.field private z:Lmaps/aj/aj;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const v14, -0x33000001    # -1.3421772E8f

    const/high16 v11, -0x80000000

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    new-instance v0, Lmaps/au/d;

    const/high16 v2, 0x42f00000    # 120.0f

    const/high16 v4, -0x3dc00000    # -48.0f

    const/high16 v5, 0x42400000    # 48.0f

    const v7, 0x6fffffff

    invoke-direct/range {v0 .. v7}, Lmaps/au/d;-><init>(FFZFFZI)V

    sput-object v0, Lmaps/au/at;->b:Lmaps/au/d;

    new-instance v0, Lmaps/au/d;

    const/high16 v2, 0x42700000    # 60.0f

    const/high16 v4, -0x3e400000    # -24.0f

    const/high16 v5, 0x41c00000    # 24.0f

    const v7, 0x6fffffff

    invoke-direct/range {v0 .. v7}, Lmaps/au/d;-><init>(FFZFFZI)V

    sput-object v0, Lmaps/au/at;->d:Lmaps/au/d;

    new-instance v0, Lmaps/au/d;

    const/high16 v2, 0x41f00000    # 30.0f

    const/high16 v4, -0x3f400000    # -6.0f

    const/high16 v5, 0x40c00000    # 6.0f

    const v7, 0x6fffffff

    invoke-direct/range {v0 .. v7}, Lmaps/au/d;-><init>(FFZFFZI)V

    sput-object v0, Lmaps/au/at;->f:Lmaps/au/d;

    new-instance v7, Lmaps/au/d;

    const/high16 v8, 0x42400000    # 48.0f

    const/high16 v9, 0x42f00000    # 120.0f

    move v10, v6

    move v12, v1

    move v13, v3

    invoke-direct/range {v7 .. v14}, Lmaps/au/d;-><init>(FFZFFZI)V

    sput-object v7, Lmaps/au/at;->c:Lmaps/au/d;

    new-instance v7, Lmaps/au/d;

    const/high16 v8, 0x41c00000    # 24.0f

    const/high16 v9, 0x42700000    # 60.0f

    move v10, v6

    move v12, v1

    move v13, v3

    invoke-direct/range {v7 .. v14}, Lmaps/au/d;-><init>(FFZFFZI)V

    sput-object v7, Lmaps/au/at;->e:Lmaps/au/d;

    new-instance v7, Lmaps/au/d;

    const/high16 v8, 0x41800000    # 16.0f

    const/high16 v9, 0x42200000    # 40.0f

    move v10, v6

    move v12, v1

    move v13, v3

    invoke-direct/range {v7 .. v14}, Lmaps/au/d;-><init>(FFZFFZI)V

    sput-object v7, Lmaps/au/at;->g:Lmaps/au/d;

    sget-object v0, Lmaps/ax/a;->a:Lmaps/ax/a;

    sput-object v0, Lmaps/au/at;->a:Lmaps/ax/a;

    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, Lmaps/au/at;->K:[F

    return-void
.end method

.method private constructor <init>(Lmaps/ac/bt;)V
    .locals 7

    const-wide/16 v5, 0x0

    const-wide/16 v3, -0x1

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lmaps/au/at;->h:Lmaps/al/m;

    iput-object v1, p0, Lmaps/au/at;->i:Lmaps/al/m;

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/au/at;->j:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/au/at;->C:[F

    iput-wide v5, p0, Lmaps/au/at;->D:J

    const/4 v0, -0x1

    iput v0, p0, Lmaps/au/at;->E:I

    iput-object v1, p0, Lmaps/au/at;->F:Lmaps/am/b;

    iput-object v1, p0, Lmaps/au/at;->G:Ljava/lang/Boolean;

    iput-wide v3, p0, Lmaps/au/at;->H:J

    iput-wide v3, p0, Lmaps/au/at;->I:J

    iput-wide v5, p0, Lmaps/au/at;->J:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/au/at;->L:Z

    iput-object p1, p0, Lmaps/au/at;->v:Lmaps/ac/bt;

    iget-object v0, p0, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-virtual {v0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/at;->w:Lmaps/ac/bd;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/au/at;->x:Ljava/util/HashSet;

    return-void
.end method

.method public static a(Lmaps/ac/bs;Lmaps/ax/a;Lmaps/as/a;)Lmaps/au/at;
    .locals 25

    invoke-interface/range {p0 .. p0}, Lmaps/ac/bs;->a()Lmaps/ac/bt;

    move-result-object v1

    new-instance v15, Lmaps/au/at;

    invoke-direct {v15, v1}, Lmaps/au/at;-><init>(Lmaps/ac/bt;)V

    move-object/from16 v0, p0

    instance-of v1, v0, Lmaps/ac/cs;

    if-eqz v1, :cond_1c

    check-cast p0, Lmaps/ac/cs;

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->o()I

    move-result v1

    iput v1, v15, Lmaps/au/at;->y:I

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->i()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    iget-object v5, v15, Lmaps/au/at;->x:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->b()Lmaps/ao/b;

    move-result-object v1

    iput-object v1, v15, Lmaps/au/at;->A:Lmaps/ao/b;

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->e()I

    move-result v1

    iput v1, v15, Lmaps/au/at;->B:I

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->j()[Ljava/lang/String;

    move-result-object v2

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lmaps/au/x;

    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-direct {v4, v1, v2}, Lmaps/au/x;-><init>(Lmaps/ac/bt;[Ljava/lang/String;)V

    new-instance v9, Lmaps/au/x;

    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-direct {v9, v1, v2}, Lmaps/au/x;-><init>(Lmaps/ac/bt;[Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->a()Lmaps/ac/bt;

    move-result-object v1

    sget-object v3, Lmaps/ac/bx;->d:Lmaps/ac/bx;

    invoke-virtual {v1, v3}, Lmaps/ac/bt;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v1

    check-cast v1, Lmaps/ac/ae;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lmaps/ac/ae;->b()Lmaps/ac/r;

    move-result-object v1

    invoke-static {}, Lmaps/ae/n;->c()Lmaps/ae/n;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3, v1}, Lmaps/ae/n;->a(Lmaps/ac/r;)Lmaps/ac/z;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3, v1}, Lmaps/ac/z;->a(Lmaps/ac/r;)Lmaps/ac/aa;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lmaps/ac/aa;->e()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->a()Lmaps/ac/bt;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x41940000    # 18.5f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    sget-object v5, Lmaps/au/at;->f:Lmaps/au/d;

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->l()Lmaps/ac/cu;

    move-result-object v3

    const/4 v1, 0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    move-wide v12, v6

    move v14, v1

    :goto_2
    invoke-interface {v3}, Lmaps/ac/cu;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {v3}, Lmaps/ac/cu;->a()Lmaps/ac/n;

    move-result-object v1

    invoke-interface {v1}, Lmaps/ac/n;->e()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lmaps/ax/a;->a(I)Z

    move-result v6

    if-nez v6, :cond_7

    invoke-interface {v3}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    goto :goto_2

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->a()Lmaps/ac/bt;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x41840000    # 16.5f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    sget-object v5, Lmaps/au/at;->d:Lmaps/au/d;

    goto :goto_1

    :cond_2
    sget-object v5, Lmaps/au/at;->b:Lmaps/au/d;

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Lmaps/ac/aa;->e()I

    move-result v1

    if-gez v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->a()Lmaps/ac/bt;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x41940000    # 18.5f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_4

    sget-object v5, Lmaps/au/at;->g:Lmaps/au/d;

    goto :goto_1

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->a()Lmaps/ac/bt;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x41840000    # 16.5f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    sget-object v5, Lmaps/au/at;->e:Lmaps/au/d;

    goto :goto_1

    :cond_5
    sget-object v5, Lmaps/au/at;->c:Lmaps/au/d;

    goto :goto_1

    :cond_6
    const/4 v5, 0x0

    goto :goto_1

    :cond_7
    invoke-interface {v1}, Lmaps/ac/n;->e()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    invoke-interface {v3}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    move v1, v14

    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v10, v6, v12

    const-wide/16 v23, 0xa

    cmp-long v8, v10, v23

    if-lez v8, :cond_1d

    invoke-static {}, Ljava/lang/Thread;->yield()V

    :goto_4
    move-wide v12, v6

    move v14, v1

    goto :goto_2

    :pswitch_1
    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    iget-object v6, v15, Lmaps/au/at;->i:Lmaps/al/m;

    iget-object v6, v15, Lmaps/au/at;->h:Lmaps/al/m;

    move-object/from16 v0, p2

    invoke-static {v1, v2, v3, v0}, Lmaps/au/am;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/as/a;)Lmaps/au/am;

    move-result-object v1

    if-eqz v14, :cond_8

    invoke-virtual {v1}, Lmaps/au/am;->c()Z

    move-result v6

    if-eqz v6, :cond_8

    const/4 v14, 0x0

    :cond_8
    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto :goto_3

    :pswitch_2
    if-eqz v14, :cond_9

    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    move-object/from16 v6, p2

    invoke-static/range {v1 .. v6}, Lmaps/au/a;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/au/x;Lmaps/au/d;Lmaps/as/a;)Lmaps/au/a;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto :goto_3

    :cond_9
    iget-object v6, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    move-object v7, v2

    move-object v8, v3

    move-object v10, v5

    move-object/from16 v11, p2

    invoke-static/range {v6 .. v11}, Lmaps/au/a;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/au/x;Lmaps/au/d;Lmaps/as/a;)Lmaps/au/a;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto :goto_3

    :pswitch_3
    if-eqz v14, :cond_a

    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    move-object/from16 v0, p2

    invoke-static {v1, v2, v3, v0}, Lmaps/au/ae;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/as/a;)Lmaps/au/ae;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto :goto_3

    :cond_a
    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    move-object/from16 v0, p2

    invoke-static {v1, v2, v3, v0}, Lmaps/au/ae;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/as/a;)Lmaps/au/ae;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto :goto_3

    :pswitch_4
    check-cast v1, Lmaps/ac/aj;

    invoke-static {v1}, Lmaps/au/ae;->a(Lmaps/ac/aj;)Z

    move-result v1

    if-eqz v1, :cond_c

    if-eqz v14, :cond_b

    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    move-object/from16 v0, p2

    invoke-static {v1, v2, v3, v0}, Lmaps/au/ae;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/as/a;)Lmaps/au/ae;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto/16 :goto_3

    :cond_b
    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    move-object/from16 v0, p2

    invoke-static {v1, v2, v3, v0}, Lmaps/au/ae;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/as/a;)Lmaps/au/ae;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto/16 :goto_3

    :cond_c
    if-eqz v14, :cond_d

    invoke-virtual {v4, v3}, Lmaps/au/x;->a(Lmaps/ac/cu;)V

    move v1, v14

    goto/16 :goto_3

    :cond_d
    invoke-virtual {v9, v3}, Lmaps/au/x;->a(Lmaps/ac/cu;)V

    move v1, v14

    goto/16 :goto_3

    :pswitch_5
    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-static {v1, v2, v3}, Lmaps/au/e;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;)Lmaps/au/e;

    move-result-object v1

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto/16 :goto_3

    :pswitch_6
    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-static {v2, v3}, Lmaps/au/al;->a([Ljava/lang/String;Lmaps/ac/cu;)Lmaps/au/al;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto/16 :goto_3

    :pswitch_7
    iget-object v1, v15, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-static {v1, v2, v3}, Lmaps/au/ar;->a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;)Lmaps/au/ar;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v14

    goto/16 :goto_3

    :cond_e
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_f

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lmaps/au/al;

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/au/al;

    iput-object v1, v15, Lmaps/au/at;->k:[Lmaps/au/al;

    :cond_f
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_10

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lmaps/au/i;

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/au/i;

    iput-object v1, v15, Lmaps/au/at;->l:[Lmaps/au/i;

    :cond_10
    const/4 v1, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_11

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [[Lmaps/au/am;

    iput-object v1, v15, Lmaps/au/at;->n:[[Lmaps/au/am;

    const/4 v1, 0x0

    move v3, v1

    :goto_5
    iget-object v1, v15, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v1, v1

    if-ge v3, v1, :cond_11

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v5, v15, Lmaps/au/at;->n:[[Lmaps/au/am;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lmaps/au/am;

    invoke-interface {v1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/au/am;

    aput-object v1, v5, v3

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5

    :cond_11
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_12

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lmaps/au/i;

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/au/i;

    iput-object v1, v15, Lmaps/au/at;->o:[Lmaps/au/i;

    :cond_12
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_13

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lmaps/au/e;

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/au/e;

    iput-object v1, v15, Lmaps/au/at;->q:[Lmaps/au/e;

    :cond_13
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_14

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lmaps/au/ar;

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/au/ar;

    iput-object v1, v15, Lmaps/au/at;->r:[Lmaps/au/ar;

    :cond_14
    const/4 v1, 0x3

    invoke-virtual {v4, v1}, Lmaps/au/x;->a(I)[Lmaps/au/w;

    move-result-object v1

    iput-object v1, v15, Lmaps/au/at;->m:[Lmaps/au/w;

    iget-object v1, v15, Lmaps/au/at;->m:[Lmaps/au/w;

    const/16 v1, 0xa

    invoke-virtual {v9, v1}, Lmaps/au/x;->a(I)[Lmaps/au/w;

    move-result-object v1

    iput-object v1, v15, Lmaps/au/at;->p:[Lmaps/au/w;

    iget-object v1, v15, Lmaps/au/at;->p:[Lmaps/au/w;

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->k()I

    move-result v6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v15, Lmaps/au/at;->t:Ljava/util/ArrayList;

    const/4 v1, 0x0

    move v5, v1

    :goto_6
    if-ge v5, v6, :cond_1a

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lmaps/ac/cs;->a(I)Lmaps/ac/n;

    move-result-object v3

    invoke-interface {v3}, Lmaps/ac/n;->e()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_15
    :goto_7
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_6

    :sswitch_0
    invoke-direct {v15, v3}, Lmaps/au/at;->a(Lmaps/ac/n;)V

    move-object v1, v3

    check-cast v1, Lmaps/ac/aw;

    iget-object v4, v15, Lmaps/au/at;->z:Lmaps/aj/aj;

    if-nez v4, :cond_16

    invoke-static {}, Lmaps/ax/m;->c()Lmaps/aj/ak;

    move-result-object v4

    iget-object v7, v15, Lmaps/au/at;->w:Lmaps/ac/bd;

    invoke-virtual {v7}, Lmaps/ac/bd;->e()Lmaps/ac/av;

    move-result-object v7

    iget-object v8, v15, Lmaps/au/at;->A:Lmaps/ao/b;

    invoke-virtual {v4, v7, v8}, Lmaps/aj/ak;->a(Lmaps/ac/av;Lmaps/ao/b;)Lmaps/aj/aj;

    move-result-object v4

    iput-object v4, v15, Lmaps/au/at;->z:Lmaps/aj/aj;

    :cond_16
    iget-object v4, v15, Lmaps/au/at;->z:Lmaps/aj/aj;

    invoke-virtual {v1}, Lmaps/ac/aw;->g()I

    move-result v7

    invoke-virtual {v4, v7}, Lmaps/aj/aj;->d(I)F

    move-result v4

    const/4 v7, 0x0

    cmpg-float v7, v4, v7

    if-gez v7, :cond_17

    invoke-virtual {v1}, Lmaps/ac/aw;->g()I

    move-result v4

    int-to-float v4, v4

    :cond_17
    invoke-virtual {v1, v4}, Lmaps/ac/aw;->a(F)V

    iget-object v4, v15, Lmaps/au/at;->z:Lmaps/aj/aj;

    invoke-virtual {v1}, Lmaps/ac/aw;->h()I

    move-result v7

    invoke-virtual {v4, v7}, Lmaps/aj/aj;->e(I)F

    move-result v4

    const/4 v7, 0x0

    cmpg-float v7, v4, v7

    if-gez v7, :cond_18

    const/16 v4, 0x16

    invoke-virtual {v1}, Lmaps/ac/aw;->h()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    :cond_18
    invoke-virtual {v1, v4}, Lmaps/ac/aw;->b(F)V

    invoke-interface {v3}, Lmaps/ac/n;->i()[I

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_8
    if-ge v1, v4, :cond_15

    aget v7, v3, v1

    if-ltz v7, :cond_19

    array-length v8, v2

    if-ge v7, v8, :cond_19

    iget-object v8, v15, Lmaps/au/at;->x:Ljava/util/HashSet;

    aget-object v7, v2, v7

    invoke-virtual {v8, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :sswitch_1
    move-object v1, v3

    check-cast v1, Lmaps/ac/aj;

    invoke-virtual {v1}, Lmaps/ac/aj;->c()I

    move-result v1

    if-lez v1, :cond_15

    invoke-direct {v15, v3}, Lmaps/au/at;->a(Lmaps/ac/n;)V

    goto :goto_7

    :sswitch_2
    move-object v1, v3

    check-cast v1, Lmaps/ac/bg;

    invoke-virtual {v1}, Lmaps/ac/bg;->c()I

    move-result v1

    if-lez v1, :cond_15

    invoke-direct {v15, v3}, Lmaps/au/at;->a(Lmaps/ac/n;)V

    goto/16 :goto_7

    :cond_1a
    iget-object v1, v15, Lmaps/au/at;->t:Ljava/util/ArrayList;

    new-instance v2, Lmaps/au/au;

    invoke-direct {v2}, Lmaps/au/au;-><init>()V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object/from16 v0, p0

    instance-of v1, v0, Lmaps/ac/as;

    if-eqz v1, :cond_1b

    move-object/from16 v1, p0

    check-cast v1, Lmaps/ac/as;

    invoke-virtual {v1}, Lmaps/ac/as;->m()Ljava/util/Set;

    move-result-object v1

    iput-object v1, v15, Lmaps/au/at;->u:Ljava/util/Set;

    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lmaps/ac/cs;->n()J

    move-result-wide v1

    invoke-virtual {v15, v1, v2}, Lmaps/au/at;->a(J)V

    :cond_1c
    return-object v15

    :cond_1d
    move-wide v6, v12

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_7
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Lmaps/ac/n;)V
    .locals 3

    iget-object v0, p0, Lmaps/au/at;->t:Ljava/util/ArrayList;

    new-instance v1, Lmaps/am/c;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lmaps/am/c;-><init>(Lmaps/ac/n;Lmaps/am/b;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ar/a;Lmaps/ap/b;)I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    iget-object v1, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v1, :cond_3

    invoke-static {p1, p2}, Lmaps/au/am;->a(Lmaps/ar/a;Lmaps/ap/b;)I

    move-result v1

    or-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    if-eqz v1, :cond_4

    or-int/lit16 v0, v0, 0x200

    :cond_4
    iget-object v1, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    if-eqz v1, :cond_5

    or-int/lit16 v0, v0, 0x400

    :cond_5
    iget-object v1, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Lmaps/ar/a;->o()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_6

    or-int/lit16 v0, v0, 0x800

    :cond_6
    or-int/lit16 v0, v0, 0x1000

    :cond_7
    iget-object v1, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    if-eqz v1, :cond_8

    or-int/lit16 v0, v0, 0x2000

    :cond_8
    sget-boolean v1, Lmaps/ap/f;->a:Z

    if-eqz v1, :cond_9

    const v1, 0x8000

    or-int/2addr v0, v1

    :cond_9
    return v0
.end method

.method public final a()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/au/at;->v:Lmaps/ac/bt;

    return-object v0
.end method

.method public final a(J)V
    .locals 4

    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lmaps/au/at;->H:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    iget-wide v0, p0, Lmaps/au/at;->H:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    :cond_2
    iput-wide p1, p0, Lmaps/au/at;->H:J

    const-wide/32 v0, 0xea60

    add-long/2addr v0, p1

    iput-wide v0, p0, Lmaps/au/at;->I:J

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/au/at;->x:Ljava/util/HashSet;

    invoke-interface {p1, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/au/al;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/au/i;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/au/w;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v1, :cond_4

    iget-object v3, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7}, Lmaps/au/am;->e()Ljava/util/Set;

    move-result-object v7

    invoke-interface {p1, v7}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/au/i;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v1, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    if-eqz v1, :cond_6

    iget-object v2, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/au/w;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    iget-object v1, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    if-eqz v1, :cond_7

    iget-object v2, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/au/e;->e()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    iget-object v1, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    array-length v2, v1

    :goto_8
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lmaps/au/ar;->e()Ljava/util/Set;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_8
    return-void
.end method

.method public final a(Lmaps/am/b;)V
    .locals 4

    iget-object v0, p0, Lmaps/au/at;->F:Lmaps/am/b;

    invoke-static {p1, v0}, Lmaps/am/b;->a(Lmaps/am/b;Lmaps/am/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lmaps/au/at;->F:Lmaps/am/b;

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lmaps/au/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/au/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/am/c;

    invoke-virtual {v0}, Lmaps/am/c;->a()Lmaps/ac/n;

    move-result-object v0

    iget-object v2, p0, Lmaps/au/at;->t:Ljava/util/ArrayList;

    new-instance v3, Lmaps/am/c;

    invoke-direct {v3, v0, p1}, Lmaps/am/c;-><init>(Lmaps/ac/n;Lmaps/am/b;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lmaps/au/at;->E:I

    goto :goto_0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/au/al;->a(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lmaps/au/h;->a(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/au/w;->a(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v1, :cond_4

    iget-object v3, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7, p1}, Lmaps/au/am;->a(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lmaps/au/h;->a(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v1, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    if-eqz v1, :cond_6

    iget-object v2, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/au/w;->a(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    iget-object v1, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    if-eqz v1, :cond_7

    iget-object v2, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/au/e;->a(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    iget-object v1, p0, Lmaps/au/at;->s:Lmaps/au/m;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/au/at;->s:Lmaps/au/m;

    invoke-virtual {v1, p1}, Lmaps/au/m;->a(Lmaps/as/a;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/au/at;->s:Lmaps/au/m;

    :cond_8
    iget-object v1, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    array-length v2, v1

    :goto_8
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lmaps/au/ar;->a(Lmaps/as/a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 11

    invoke-virtual {p1}, Lmaps/as/a;->J()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p1}, Lmaps/as/a;->K()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p2}, Lmaps/ar/a;->e()J

    move-result-wide v3

    iget-wide v5, p0, Lmaps/au/at;->D:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lmaps/ar/a;->e()J

    move-result-wide v3

    iput-wide v3, p0, Lmaps/au/at;->D:J

    iget-object v1, p0, Lmaps/au/at;->w:Lmaps/ac/bd;

    invoke-virtual {v1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {p2}, Lmaps/ar/a;->h()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p2}, Lmaps/ar/a;->o()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    invoke-virtual {p2}, Lmaps/ar/a;->n()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v3

    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    sget-object v3, Lmaps/au/at;->K:[F

    invoke-virtual {p2, v1, v3}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    sget-object v1, Lmaps/au/at;->K:[F

    const/4 v3, 0x0

    aget v1, v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    sget-object v3, Lmaps/au/at;->K:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p2, v1, v3}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v1

    :cond_0
    iget-object v3, p0, Lmaps/au/at;->w:Lmaps/ac/bd;

    invoke-virtual {v3}, Lmaps/ac/bd;->f()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lmaps/au/at;->C:[F

    invoke-static {p1, p2, v1, v3, v4}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F[F)V

    :cond_1
    iget-object v1, p0, Lmaps/au/at;->C:[F

    invoke-static {v2, v1}, Lmaps/ap/o;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    iget-object v0, p0, Lmaps/au/at;->G:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lmaps/as/a;->I()Z

    move-result v0

    if-eqz v0, :cond_11

    sget-boolean v0, Lmaps/bb/b;->a:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    if-nez v0, :cond_10

    iget-object v0, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-nez v0, :cond_10

    iget-object v0, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    if-nez v0, :cond_10

    iget-object v0, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    if-nez v0, :cond_10

    iget-object v0, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    if-nez v0, :cond_10

    iget-object v0, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    if-nez v0, :cond_10

    iget-object v0, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/at;->G:Ljava/lang/Boolean;

    :cond_3
    :goto_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    array-length v3, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/au/al;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :pswitch_2
    iget-object v0, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    array-length v3, v1

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v3, :cond_5

    aget-object v4, v1, v0

    invoke-interface {v4, p1, p2, p3}, Lmaps/au/h;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    iget-object v0, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v6, :cond_6

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :pswitch_3
    iget-object v0, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    array-length v3, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/au/w;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :pswitch_4
    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_9
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v6, :cond_7

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :pswitch_5
    if-eqz v0, :cond_c

    iget-object v0, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v0, :cond_c

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v0

    invoke-static {p2, v0}, Lmaps/au/am;->a(Lmaps/ar/a;Lmaps/ap/b;)I

    move-result v3

    new-instance v4, Lmaps/ap/c;

    const/4 v1, 0x4

    invoke-direct {v4, v0, v1}, Lmaps/ap/c;-><init>(Lmaps/ap/b;I)V

    new-instance v5, Lmaps/ap/c;

    const/4 v1, 0x6

    invoke-direct {v5, v0, v1}, Lmaps/ap/c;-><init>(Lmaps/ap/b;I)V

    iget-object v6, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v7, v6

    const/4 v0, 0x0

    :goto_b
    if-ge v0, v7, :cond_2

    aget-object v8, v6, v0

    and-int/lit8 v1, v3, 0x10

    if-eqz v1, :cond_8

    invoke-virtual {p0, p1, p2, v4}, Lmaps/au/at;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    array-length v9, v8

    const/4 v1, 0x0

    :goto_c
    if-ge v1, v9, :cond_8

    aget-object v10, v8, v1

    invoke-virtual {v10, p1, p2, v4}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    :cond_8
    and-int/lit8 v1, v3, 0x20

    if-eqz v1, :cond_9

    invoke-virtual {p0, p1, p2, p3}, Lmaps/au/at;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    array-length v9, v8

    const/4 v1, 0x0

    :goto_d
    if-ge v1, v9, :cond_9

    aget-object v10, v8, v1

    invoke-virtual {v10, p1, p2, p3}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_9
    and-int/lit8 v1, v3, 0x40

    if-eqz v1, :cond_a

    invoke-virtual {p1}, Lmaps/as/a;->q()V

    invoke-virtual {p0, p1, p2, v5}, Lmaps/au/at;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    array-length v9, v8

    const/4 v1, 0x0

    :goto_e
    if-ge v1, v9, :cond_a

    aget-object v10, v8, v1

    invoke-virtual {v10, p1, p2, v5}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    :cond_a
    and-int/lit16 v1, v3, 0x180

    if-eqz v1, :cond_b

    invoke-virtual {p1}, Lmaps/as/a;->p()V

    invoke-virtual {p0, p1, p2, p3}, Lmaps/au/at;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    array-length v9, v8

    const/4 v1, 0x0

    :goto_f
    if-ge v1, v9, :cond_b

    aget-object v10, v8, v1

    invoke-virtual {v10, p1, p2, p3}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_c
    iget-object v0, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_10
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_11
    if-ge v0, v6, :cond_d

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    :pswitch_6
    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_12
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_13
    if-ge v0, v6, :cond_e

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    :cond_e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    :pswitch_7
    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_14
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_15
    if-ge v0, v6, :cond_f

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, Lmaps/au/am;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    :cond_f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    :pswitch_8
    iget-object v0, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    array-length v3, v1

    const/4 v0, 0x0

    :goto_16
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    invoke-interface {v4, p1, p2, p3}, Lmaps/au/h;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    :pswitch_9
    iget-object v0, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    array-length v3, v1

    const/4 v0, 0x0

    :goto_17
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/au/w;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    :pswitch_a
    iget-object v0, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    array-length v3, v1

    const/4 v0, 0x0

    :goto_18
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/au/e;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    :pswitch_b
    iget-object v0, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    array-length v3, v1

    const/4 v0, 0x0

    :goto_19
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, Lmaps/au/ar;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    :pswitch_c
    sget-object v0, Lmaps/au/aq;->a:Lmaps/au/aq;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/au/aq;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    goto/16 :goto_1

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_11
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lmaps/au/at;->G:Ljava/lang/Boolean;

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
    .end packed-switch
.end method

.method public final a(Lmaps/au/m;)V
    .locals 0

    iput-object p1, p0, Lmaps/au/at;->s:Lmaps/au/m;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lmaps/au/at;->L:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lmaps/au/at;->L:Z

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)Z
    .locals 1

    iget-object v0, p0, Lmaps/au/at;->u:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/at;->u:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/au/at;->u:Ljava/util/Set;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lmaps/aj/ac;)Z
    .locals 1

    iget-object v0, p0, Lmaps/au/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/au/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmaps/aj/ac;->a(Ljava/util/Iterator;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lmaps/au/at;->B:I

    return v0
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/as/a;)V
    .locals 8

    const/4 v0, 0x0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, p0, Lmaps/au/at;->G:Ljava/lang/Boolean;

    iget-object v1, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/au/al;->b(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lmaps/au/h;->b(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/au/w;->b(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v1, :cond_4

    iget-object v3, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7, p1}, Lmaps/au/am;->b(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, Lmaps/au/h;->b(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v1, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    if-eqz v1, :cond_6

    iget-object v2, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/au/w;->b(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    iget-object v1, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    if-eqz v1, :cond_7

    iget-object v2, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lmaps/au/e;->b(Lmaps/as/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    iget-object v1, p0, Lmaps/au/at;->s:Lmaps/au/m;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/au/at;->s:Lmaps/au/m;

    invoke-virtual {v1, p1}, Lmaps/au/m;->b(Lmaps/as/a;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/au/at;->s:Lmaps/au/m;

    :cond_8
    iget-object v1, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    array-length v2, v1

    :goto_8
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lmaps/au/ar;->b(Lmaps/as/a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    return-void
.end method

.method public final b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 3

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v0

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {p1}, Lmaps/au/al;->c(Lmaps/as/a;)V

    goto :goto_0

    :pswitch_2
    invoke-static {p1, p3}, Lmaps/au/a;->a(Lmaps/as/a;Lmaps/ap/c;)V

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lmaps/au/w;->c(Lmaps/as/a;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v0

    iget-object v1, p0, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v1

    invoke-static {p1, v0, v1}, Lmaps/au/am;->a(Lmaps/as/a;FI)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v1

    iget-object v2, p0, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-virtual {v2}, Lmaps/ac/bt;->b()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, Lmaps/au/am;->a(Lmaps/as/a;FILmaps/ap/b;)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    invoke-static {p1}, Lmaps/au/am;->c(Lmaps/as/a;)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v0

    iget-object v1, p0, Lmaps/au/at;->v:Lmaps/ac/bt;

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v1

    invoke-static {p1, v0, v1}, Lmaps/au/am;->b(Lmaps/as/a;FI)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    invoke-static {p1}, Lmaps/au/am;->d(Lmaps/as/a;)V

    goto :goto_0

    :pswitch_9
    invoke-static {p1, p3}, Lmaps/au/a;->a(Lmaps/as/a;Lmaps/ap/c;)V

    goto :goto_0

    :pswitch_a
    invoke-static {p1}, Lmaps/au/w;->c(Lmaps/as/a;)V

    goto :goto_0

    :pswitch_b
    invoke-static {p1, v1}, Lmaps/au/e;->a(Lmaps/as/a;I)V

    goto :goto_0

    :pswitch_c
    invoke-static {p1, v1}, Lmaps/au/e;->a(Lmaps/as/a;I)V

    goto :goto_0

    :pswitch_d
    invoke-static {p1}, Lmaps/au/ar;->c(Lmaps/as/a;)V

    goto :goto_0

    :pswitch_e
    invoke-static {p1, p3}, Lmaps/au/aq;->a(Lmaps/as/a;Lmaps/ap/c;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public final c()Lmaps/ao/b;
    .locals 1

    iget-object v0, p0, Lmaps/au/at;->A:Lmaps/ao/b;

    return-object v0
.end method

.method public final d()Z
    .locals 4

    iget-wide v0, p0, Lmaps/au/at;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/au/at;->H:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    iget-wide v0, p0, Lmaps/au/at;->I:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/au/at;->I:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/au/al;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    iget-object v0, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lmaps/au/al;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lmaps/au/at;->y:I

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lmaps/au/at;->G:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/at;->G:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/al;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    :cond_1
    iget-object v2, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    if-eqz v2, :cond_2

    iget-object v4, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/w;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v2, :cond_4

    iget-object v4, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v5, v4

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_3

    aget-object v8, v6, v0

    invoke-virtual {v8}, Lmaps/au/am;->a()I

    move-result v8

    add-int/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    if-eqz v2, :cond_5

    iget-object v4, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    array-length v5, v4

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_5

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/w;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_4

    :cond_5
    iget-object v2, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    if-eqz v2, :cond_6

    iget-object v4, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    array-length v5, v4

    move v2, v1

    :goto_5
    if-ge v2, v5, :cond_6

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/e;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    if-eqz v2, :cond_7

    iget-object v4, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    array-length v5, v4

    move v2, v1

    :goto_6
    if-ge v2, v5, :cond_7

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/ar;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6

    :cond_7
    iget-object v2, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    if-eqz v2, :cond_8

    iget-object v4, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    array-length v5, v4

    move v2, v1

    :goto_7
    if-ge v2, v5, :cond_8

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/i;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_7

    :cond_8
    iget-object v2, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    array-length v3, v2

    :goto_8
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/au/i;->a()I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_9
    return v0
.end method

.method public final k()I
    .locals 9

    const/4 v1, 0x0

    const/16 v0, 0x100

    iget-object v2, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    if-eqz v2, :cond_0

    const/16 v0, 0x110

    iget-object v4, p0, Lmaps/au/at;->k:[Lmaps/au/al;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/al;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    if-eqz v2, :cond_1

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/au/at;->m:[Lmaps/au/w;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/w;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    if-eqz v2, :cond_3

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/au/at;->n:[[Lmaps/au/am;

    array-length v5, v4

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_2

    aget-object v8, v6, v0

    invoke-virtual {v8}, Lmaps/au/am;->b()I

    move-result v8

    add-int/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    if-eqz v2, :cond_4

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/au/at;->p:[Lmaps/au/w;

    array-length v5, v4

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_4

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/w;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_4

    :cond_4
    iget-object v2, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    if-eqz v2, :cond_5

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/au/at;->q:[Lmaps/au/e;

    array-length v5, v4

    move v2, v1

    :goto_5
    if-ge v2, v5, :cond_5

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/e;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    :cond_5
    iget-object v2, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    if-eqz v2, :cond_6

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/au/at;->r:[Lmaps/au/ar;

    array-length v5, v4

    move v2, v1

    :goto_6
    if-ge v2, v5, :cond_6

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/ar;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6

    :cond_6
    iget-object v2, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    if-eqz v2, :cond_7

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, Lmaps/au/at;->l:[Lmaps/au/i;

    array-length v5, v4

    move v2, v1

    :goto_7
    if-ge v2, v5, :cond_7

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lmaps/au/i;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_7

    :cond_7
    iget-object v2, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    if-eqz v2, :cond_8

    add-int/lit8 v0, v0, 0x10

    iget-object v3, p0, Lmaps/au/at;->o:[Lmaps/au/i;

    array-length v4, v3

    :goto_8
    if-ge v1, v4, :cond_8

    aget-object v2, v3, v1

    invoke-virtual {v2}, Lmaps/au/i;->b()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_8

    :cond_8
    move v1, v0

    iget v2, p0, Lmaps/au/at;->E:I

    const/4 v0, -0x1

    if-ne v2, v0, :cond_a

    const/16 v0, 0x18

    iget-object v2, p0, Lmaps/au/at;->t:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/am/c;

    invoke-virtual {v0}, Lmaps/am/c;->d()I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    goto :goto_9

    :cond_9
    iput v2, p0, Lmaps/au/at;->E:I

    :cond_a
    add-int v0, v1, v2

    return v0
.end method

.method public final l()Lmaps/au/m;
    .locals 1

    iget-object v0, p0, Lmaps/au/at;->s:Lmaps/au/m;

    return-object v0
.end method
