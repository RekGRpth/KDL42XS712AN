.class public final Lmaps/au/am;
.super Lmaps/au/i;


# static fields
.field private static final b:[F

.field private static final c:F


# instance fields
.field private final A:Ljava/util/ArrayList;

.field private final B:Ljava/util/ArrayList;

.field private C:Z

.field private D:F

.field private E:F

.field private final d:Lmaps/ac/bt;

.field private final e:[F

.field private final f:Lmaps/al/j;

.field private final g:Lmaps/al/j;

.field private final h:Lmaps/at/n;

.field private final i:Lmaps/at/i;

.field private final j:Lmaps/at/a;

.field private final k:Lmaps/at/a;

.field private final l:Lmaps/at/d;

.field private final m:Lmaps/at/a;

.field private final n:Lmaps/at/n;

.field private final o:Lmaps/at/i;

.field private final p:Lmaps/at/d;

.field private final q:Lmaps/at/a;

.field private final r:Lmaps/at/n;

.field private final s:Lmaps/al/c;

.field private final t:Lmaps/at/d;

.field private final u:Lmaps/at/d;

.field private final v:Lmaps/at/n;

.field private final w:Lmaps/at/i;

.field private final x:Lmaps/at/d;

.field private y:Lmaps/v/e;

.field private final z:Lmaps/ac/av;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/au/am;->b:[F

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lmaps/au/am;->c:F

    return-void

    nop

    :array_0
    .array-data 4
        0x3f37b7b8
        0x3f37b7b8
        0x3f65e5e6
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private constructor <init>(Lmaps/ac/bt;Lmaps/au/an;Ljava/util/HashSet;Lmaps/as/a;I)V
    .locals 7

    const/high16 v6, -0x40800000    # -1.0f

    const/high16 v5, 0x437f0000    # 255.0f

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p3}, Lmaps/au/i;-><init>(Ljava/util/Set;)V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/am;->z:Lmaps/ac/av;

    iput-boolean v4, p0, Lmaps/au/am;->C:Z

    iput-object p1, p0, Lmaps/au/am;->d:Lmaps/ac/bt;

    const/4 v0, 0x4

    new-array v0, v0, [F

    ushr-int/lit8 v1, p5, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-float v1, v1

    div-float/2addr v1, v5

    aput v1, v0, v4

    const/4 v1, 0x1

    ushr-int/lit8 v2, p5, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-float v2, v2

    div-float/2addr v2, v5

    aput v2, v0, v1

    const/4 v1, 0x2

    and-int/lit16 v2, p5, 0xff

    int-to-float v2, v2

    div-float/2addr v2, v5

    aput v2, v0, v1

    const/4 v1, 0x3

    ushr-int/lit8 v2, p5, 0x18

    and-int/lit16 v2, v2, 0xff

    int-to-float v2, v2

    div-float/2addr v2, v5

    aput v2, v0, v1

    iput-object v0, p0, Lmaps/au/am;->e:[F

    iput-object v3, p0, Lmaps/au/am;->f:Lmaps/al/j;

    iput-object v3, p0, Lmaps/au/am;->g:Lmaps/al/j;

    new-instance v0, Lmaps/at/p;

    iget v1, p2, Lmaps/au/an;->a:I

    invoke-direct {v0, v1, v4}, Lmaps/at/p;-><init>(IB)V

    iput-object v0, p0, Lmaps/au/am;->h:Lmaps/at/n;

    new-instance v0, Lmaps/at/k;

    iget v1, p2, Lmaps/au/an;->a:I

    invoke-direct {v0, v1, v4}, Lmaps/at/k;-><init>(IB)V

    iput-object v0, p0, Lmaps/au/am;->i:Lmaps/at/i;

    new-instance v0, Lmaps/at/c;

    iget v1, p2, Lmaps/au/an;->a:I

    invoke-direct {v0, v1, v4}, Lmaps/at/c;-><init>(IB)V

    iput-object v0, p0, Lmaps/au/am;->k:Lmaps/at/a;

    new-instance v0, Lmaps/at/c;

    iget v1, p2, Lmaps/au/an;->a:I

    invoke-direct {v0, v1, v4}, Lmaps/at/c;-><init>(IB)V

    iput-object v0, p0, Lmaps/au/am;->j:Lmaps/at/a;

    new-instance v0, Lmaps/at/f;

    iget v1, p2, Lmaps/au/an;->b:I

    invoke-direct {v0, v1, v4}, Lmaps/at/f;-><init>(IB)V

    iput-object v0, p0, Lmaps/au/am;->l:Lmaps/at/d;

    iget-object v0, p2, Lmaps/au/an;->i:Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_0

    new-instance v0, Lmaps/at/c;

    iget v1, p2, Lmaps/au/an;->a:I

    invoke-direct {v0, v1}, Lmaps/at/c;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->m:Lmaps/at/a;

    :goto_0
    iget v0, p2, Lmaps/au/an;->c:I

    if-lez v0, :cond_1

    new-instance v0, Lmaps/at/p;

    iget v1, p2, Lmaps/au/an;->c:I

    invoke-direct {v0, v1}, Lmaps/at/p;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->r:Lmaps/at/n;

    new-instance v0, Lmaps/al/c;

    iget v1, p2, Lmaps/au/an;->c:I

    invoke-virtual {p4}, Lmaps/as/a;->H()Lmaps/al/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/al/c;-><init>(ILmaps/al/a;)V

    iput-object v0, p0, Lmaps/au/am;->s:Lmaps/al/c;

    new-instance v0, Lmaps/at/f;

    iget v1, p2, Lmaps/au/an;->d:I

    invoke-direct {v0, v1}, Lmaps/at/f;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->t:Lmaps/at/d;

    new-instance v0, Lmaps/at/f;

    iget v1, p2, Lmaps/au/an;->b:I

    invoke-direct {v0, v1}, Lmaps/at/f;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->u:Lmaps/at/d;

    :goto_1
    iget v0, p2, Lmaps/au/an;->e:I

    if-lez v0, :cond_2

    new-instance v0, Lmaps/at/p;

    iget v1, p2, Lmaps/au/an;->e:I

    invoke-direct {v0, v1}, Lmaps/at/p;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->v:Lmaps/at/n;

    new-instance v0, Lmaps/at/k;

    iget v1, p2, Lmaps/au/an;->e:I

    invoke-direct {v0, v1}, Lmaps/at/k;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->w:Lmaps/at/i;

    new-instance v0, Lmaps/at/f;

    iget v1, p2, Lmaps/au/an;->f:I

    invoke-direct {v0, v1}, Lmaps/at/f;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->x:Lmaps/at/d;

    :goto_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/au/am;->A:Ljava/util/ArrayList;

    iput v6, p0, Lmaps/au/am;->D:F

    iget v0, p2, Lmaps/au/an;->g:I

    if-lez v0, :cond_3

    new-instance v0, Lmaps/at/p;

    iget v1, p2, Lmaps/au/an;->g:I

    invoke-direct {v0, v1}, Lmaps/at/p;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->n:Lmaps/at/n;

    new-instance v0, Lmaps/at/k;

    iget v1, p2, Lmaps/au/an;->g:I

    invoke-direct {v0, v1}, Lmaps/at/k;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->o:Lmaps/at/i;

    new-instance v0, Lmaps/at/f;

    iget v1, p2, Lmaps/au/an;->h:I

    invoke-direct {v0, v1}, Lmaps/at/f;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->p:Lmaps/at/d;

    new-instance v0, Lmaps/at/c;

    iget v1, p2, Lmaps/au/an;->g:I

    invoke-direct {v0, v1}, Lmaps/at/c;-><init>(I)V

    iput-object v0, p0, Lmaps/au/am;->q:Lmaps/at/a;

    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/au/am;->B:Ljava/util/ArrayList;

    iput v6, p0, Lmaps/au/am;->E:F

    return-void

    :cond_0
    iput-object v3, p0, Lmaps/au/am;->m:Lmaps/at/a;

    goto/16 :goto_0

    :cond_1
    iput-object v3, p0, Lmaps/au/am;->r:Lmaps/at/n;

    iput-object v3, p0, Lmaps/au/am;->s:Lmaps/al/c;

    iput-object v3, p0, Lmaps/au/am;->t:Lmaps/at/d;

    iput-object v3, p0, Lmaps/au/am;->u:Lmaps/at/d;

    goto :goto_1

    :cond_2
    iput-object v3, p0, Lmaps/au/am;->v:Lmaps/at/n;

    iput-object v3, p0, Lmaps/au/am;->w:Lmaps/at/i;

    iput-object v3, p0, Lmaps/au/am;->x:Lmaps/at/d;

    goto :goto_2

    :cond_3
    iput-object v3, p0, Lmaps/au/am;->n:Lmaps/at/n;

    iput-object v3, p0, Lmaps/au/am;->o:Lmaps/at/i;

    iput-object v3, p0, Lmaps/au/am;->p:Lmaps/at/d;

    iput-object v3, p0, Lmaps/au/am;->q:Lmaps/at/a;

    goto :goto_3
.end method

.method private static a(F)F
    .locals 3

    float-to-int v1, p0

    int-to-float v0, v1

    sub-float v0, p0, v0

    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    sget v0, Lmaps/au/am;->c:F

    :goto_0
    const/4 v2, 0x1

    rsub-int/lit8 v1, v1, 0x1e

    shl-int v1, v2, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x43800000    # 256.0f

    div-float/2addr v0, v1

    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method private a(FI)F
    .locals 3

    iget-object v0, p0, Lmaps/au/am;->d:Lmaps/ac/bt;

    invoke-virtual {v0}, Lmaps/ac/bt;->b()I

    move-result v0

    const/16 v1, 0xe

    if-le v0, v1, :cond_0

    const/high16 v0, 0x3f000000    # 0.5f

    :goto_0
    int-to-float v1, p2

    mul-float/2addr v1, p1

    const/high16 v2, 0x43800000    # 256.0f

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0

    :cond_0
    const v0, 0x3e99999a    # 0.3f

    goto :goto_0
.end method

.method private static a(Lmaps/ac/bl;)F
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/ac/bl;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lmaps/ac/bl;->b()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bk;->c()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Lmaps/ar/a;Lmaps/ap/b;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/ar/a;->p()F

    move-result v1

    sget-object v2, Lmaps/ap/b;->b:Lmaps/ap/b;

    if-ne p1, v2, :cond_0

    const/high16 v2, 0x418c0000    # 17.5f

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_1

    :cond_0
    const/16 v0, 0x20

    :cond_1
    sget-object v2, Lmaps/ap/b;->a:Lmaps/ap/b;

    if-ne p1, v2, :cond_2

    or-int/lit8 v0, v0, 0x14

    :cond_2
    const/high16 v2, 0x41780000    # 15.5f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_4

    sget-object v1, Lmaps/ap/b;->a:Lmaps/ap/b;

    if-eq p1, v1, :cond_3

    sget-object v1, Lmaps/ap/b;->c:Lmaps/ap/b;

    if-ne p1, v1, :cond_4

    :cond_3
    or-int/lit8 v0, v0, 0x40

    :cond_4
    or-int/lit16 v0, v0, 0x180

    return v0
.end method

.method public static a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/as/a;)Lmaps/au/am;
    .locals 23

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v19

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    const/16 v2, 0x200

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/bt;->b()I

    move-result v7

    new-instance v4, Lmaps/au/an;

    invoke-direct {v4}, Lmaps/au/an;-><init>()V

    new-instance v20, Lmaps/au/ao;

    invoke-direct/range {v20 .. v20}, Lmaps/au/ao;-><init>()V

    const/4 v2, -0x1

    move v6, v2

    :goto_0
    invoke-interface/range {p2 .. p2}, Lmaps/ac/cu;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {p2 .. p2}, Lmaps/ac/cu;->a()Lmaps/ac/n;

    move-result-object v3

    instance-of v2, v3, Lmaps/ac/bg;

    if-eqz v2, :cond_4

    move-object v2, v3

    check-cast v2, Lmaps/ac/bg;

    invoke-virtual {v2}, Lmaps/ac/bg;->h()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v2}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/bl;->k()Lmaps/ac/bk;

    move-result-object v9

    if-nez v9, :cond_2

    const v6, -0x48481b

    :cond_0
    :goto_1
    move-object/from16 v0, v20

    invoke-static {v2, v0}, Lmaps/au/am;->a(Lmaps/ac/bg;Lmaps/au/ao;)V

    move-object/from16 v0, v20

    invoke-static {v7, v2, v0, v4}, Lmaps/au/am;->a(ILmaps/ac/bg;Lmaps/au/ao;Lmaps/au/an;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v3}, Lmaps/ac/n;->i()[I

    move-result-object v9

    array-length v10, v9

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v10, :cond_c

    aget v11, v9, v3

    if-ltz v11, :cond_1

    move-object/from16 v0, p1

    array-length v12, v0

    if-ge v11, v12, :cond_1

    aget-object v11, p1, v11

    invoke-virtual {v5, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v9, -0x1

    if-ne v6, v9, :cond_3

    invoke-virtual {v2}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/ac/bl;->k()Lmaps/ac/bk;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/ac/bk;->b()I

    move-result v6

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/bl;->k()Lmaps/ac/bk;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/bk;->b()I

    move-result v9

    if-eq v6, v9, :cond_0

    :cond_4
    move v7, v6

    new-instance v2, Lmaps/au/am;

    move-object/from16 v3, p0

    move-object/from16 v6, p3

    invoke-direct/range {v2 .. v7}, Lmaps/au/am;-><init>(Lmaps/ac/bt;Lmaps/au/an;Ljava/util/HashSet;Lmaps/as/a;I)V

    invoke-static {}, Lmaps/al/h;->a()Lmaps/al/h;

    move-result-object v3

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_5
    :goto_3
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v18, v4

    check-cast v18, Lmaps/ac/bg;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lmaps/au/am;->a(Lmaps/ac/bg;Lmaps/au/ao;)V

    move-object/from16 v0, v20

    iget-boolean v4, v0, Lmaps/au/ao;->a:Z

    if-eqz v4, :cond_5

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v18 .. v18}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/bl;->b()I

    move-result v5

    if-lez v5, :cond_6

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lmaps/ac/bk;->f()Z

    move-result v11

    invoke-virtual {v4}, Lmaps/ac/bk;->g()Z

    move-result v12

    :cond_6
    invoke-virtual/range {v19 .. v19}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v6

    invoke-virtual/range {v19 .. v19}, Lmaps/ac/bd;->f()I

    move-result v7

    invoke-virtual/range {v18 .. v18}, Lmaps/ac/bg;->b()Lmaps/ac/az;

    move-result-object v4

    move-object/from16 v0, v20

    iget v5, v0, Lmaps/au/ao;->c:F

    invoke-direct {v2, v5, v7}, Lmaps/au/am;->a(FI)F

    move-result v5

    move-object/from16 v0, v20

    iget v8, v0, Lmaps/au/ao;->e:I

    if-nez v8, :cond_7

    move-object/from16 v0, v20

    iget v8, v0, Lmaps/au/ao;->d:I

    if-nez v8, :cond_7

    move-object/from16 v0, v20

    iget v8, v0, Lmaps/au/ao;->f:I

    if-eqz v8, :cond_9

    :cond_7
    iget-object v8, v2, Lmaps/au/am;->h:Lmaps/at/n;

    invoke-virtual {v8}, Lmaps/at/n;->a()I

    move-result v13

    iget-object v8, v2, Lmaps/au/am;->l:Lmaps/at/d;

    invoke-virtual {v8}, Lmaps/at/d;->b()I

    move-result v14

    iget-object v8, v2, Lmaps/au/am;->h:Lmaps/at/n;

    iget-object v9, v2, Lmaps/au/am;->i:Lmaps/at/i;

    iget-object v10, v2, Lmaps/au/am;->l:Lmaps/at/d;

    invoke-virtual/range {v3 .. v12}, Lmaps/al/h;->a(Lmaps/ac/az;FLmaps/ac/av;ILmaps/at/o;Lmaps/at/j;Lmaps/at/e;ZZ)I

    iget-object v8, v2, Lmaps/au/am;->h:Lmaps/at/n;

    invoke-virtual {v8}, Lmaps/at/n;->a()I

    move-result v8

    sub-int/2addr v8, v13

    iget-object v9, v2, Lmaps/au/am;->j:Lmaps/at/a;

    move-object/from16 v0, v20

    iget v10, v0, Lmaps/au/ao;->e:I

    invoke-virtual {v9, v10, v8}, Lmaps/at/a;->b(II)V

    iget-object v9, v2, Lmaps/au/am;->k:Lmaps/at/a;

    move-object/from16 v0, v20

    iget v10, v0, Lmaps/au/ao;->d:I

    invoke-virtual {v9, v10, v8}, Lmaps/at/a;->b(II)V

    iget-object v9, v2, Lmaps/au/am;->m:Lmaps/at/a;

    if-eqz v9, :cond_8

    move-object/from16 v0, v20

    iget v9, v0, Lmaps/au/ao;->f:I

    if-eqz v9, :cond_8

    iget-object v9, v2, Lmaps/au/am;->m:Lmaps/at/a;

    move-object/from16 v0, v20

    iget v10, v0, Lmaps/au/ao;->f:I

    invoke-virtual {v9, v10, v8}, Lmaps/at/a;->b(II)V

    :cond_8
    iget-object v8, v2, Lmaps/au/am;->r:Lmaps/at/n;

    if-eqz v8, :cond_9

    move-object/from16 v0, v20

    iget-boolean v8, v0, Lmaps/au/ao;->b:Z

    if-eqz v8, :cond_d

    move-object/from16 v0, v20

    iget v8, v0, Lmaps/au/ao;->d:I

    invoke-direct {v2, v4, v6, v7, v8}, Lmaps/au/am;->a(Lmaps/ac/az;Lmaps/ac/av;II)V

    :cond_9
    :goto_4
    iget-object v8, v2, Lmaps/au/am;->d:Lmaps/ac/bt;

    invoke-virtual {v8}, Lmaps/ac/bt;->b()I

    move-result v8

    move-object/from16 v0, v18

    invoke-static {v8, v0}, Lmaps/au/am;->a(ILmaps/ac/bg;)Z

    move-result v8

    if-eqz v8, :cond_a

    const/4 v11, 0x0

    const/high16 v14, 0x3f800000    # 1.0f

    iget-object v15, v2, Lmaps/au/am;->v:Lmaps/at/n;

    iget-object v0, v2, Lmaps/au/am;->x:Lmaps/at/d;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object v8, v3

    move-object v9, v4

    move v10, v5

    move-object v12, v6

    move v13, v7

    invoke-virtual/range {v8 .. v17}, Lmaps/al/h;->a(Lmaps/ac/az;FZLmaps/ac/av;IFLmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V

    iget-object v8, v2, Lmaps/au/am;->A:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    move-object/from16 v0, v20

    iget v8, v0, Lmaps/au/ao;->g:I

    if-eqz v8, :cond_b

    iget-object v8, v2, Lmaps/au/am;->n:Lmaps/at/n;

    invoke-virtual {v8}, Lmaps/at/n;->a()I

    move-result v22

    const/4 v11, 0x0

    const/high16 v14, 0x3f800000    # 1.0f

    iget-object v15, v2, Lmaps/au/am;->n:Lmaps/at/n;

    iget-object v0, v2, Lmaps/au/am;->p:Lmaps/at/d;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object v8, v3

    move-object v9, v4

    move v10, v5

    move-object v12, v6

    move v13, v7

    invoke-virtual/range {v8 .. v17}, Lmaps/al/h;->a(Lmaps/ac/az;FZLmaps/ac/av;IFLmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V

    iget-object v4, v2, Lmaps/au/am;->n:Lmaps/at/n;

    invoke-virtual {v4}, Lmaps/at/n;->a()I

    move-result v4

    sub-int v4, v4, v22

    iget-object v5, v2, Lmaps/au/am;->q:Lmaps/at/a;

    move-object/from16 v0, v20

    iget v6, v0, Lmaps/au/ao;->g:I

    invoke-virtual {v5, v6, v4}, Lmaps/at/a;->b(II)V

    iget-object v4, v2, Lmaps/au/am;->B:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    invoke-virtual/range {v18 .. v18}, Lmaps/ac/bg;->m()Z

    move-result v4

    iput-boolean v4, v2, Lmaps/au/am;->C:Z

    goto/16 :goto_3

    :cond_c
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p2 .. p2}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    goto/16 :goto_0

    :cond_d
    iget-object v8, v2, Lmaps/au/am;->u:Lmaps/at/d;

    iget-object v9, v2, Lmaps/au/am;->l:Lmaps/at/d;

    iget-object v10, v2, Lmaps/au/am;->l:Lmaps/at/d;

    invoke-virtual {v10}, Lmaps/at/d;->b()I

    move-result v10

    sub-int/2addr v10, v14

    invoke-virtual {v8, v9, v14, v10}, Lmaps/at/d;->a(Lmaps/at/d;II)V

    goto :goto_4

    :cond_e
    return-object v2
.end method

.method private a(Lmaps/ac/az;Lmaps/ac/av;II)V
    .locals 6

    iget-object v0, p0, Lmaps/au/am;->r:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->a()I

    move-result v1

    invoke-virtual {p1}, Lmaps/ac/az;->b()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lmaps/au/am;->z:Lmaps/ac/av;

    invoke-virtual {p1, v0, v3}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    iget-object v3, p0, Lmaps/au/am;->z:Lmaps/ac/av;

    iget-object v4, p0, Lmaps/au/am;->z:Lmaps/ac/av;

    invoke-static {v3, p2, v4}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v3, p0, Lmaps/au/am;->r:Lmaps/at/n;

    iget-object v4, p0, Lmaps/au/am;->z:Lmaps/ac/av;

    invoke-virtual {v3, v4, p3}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    if-lez v0, :cond_0

    iget-object v3, p0, Lmaps/au/am;->t:Lmaps/at/d;

    add-int v4, v1, v0

    add-int/lit8 v4, v4, -0x1

    int-to-short v4, v4

    add-int v5, v1, v0

    int-to-short v5, v5

    invoke-virtual {v3, v4, v5}, Lmaps/at/d;->a(SS)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/au/am;->s:Lmaps/al/c;

    invoke-virtual {v0, p4, v2}, Lmaps/al/c;->a(II)V

    return-void
.end method

.method private static a(Lmaps/ac/bg;Lmaps/au/ao;)V
    .locals 9

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/high16 v8, 0x42700000    # 60.0f

    const/4 v1, 0x2

    const/4 v5, 0x0

    invoke-virtual {p0}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v6

    invoke-static {v6}, Lmaps/au/am;->a(Lmaps/ac/bl;)F

    move-result v0

    iput v0, p1, Lmaps/au/ao;->c:F

    iput v5, p1, Lmaps/au/ao;->d:I

    iput v5, p1, Lmaps/au/ao;->e:I

    iput v5, p1, Lmaps/au/ao;->f:I

    invoke-virtual {v6}, Lmaps/ac/bl;->b()I

    move-result v0

    if-lt v0, v1, :cond_3

    invoke-virtual {v6, v5}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bk;->b()I

    move-result v0

    iput v0, p1, Lmaps/au/ao;->d:I

    invoke-virtual {v6, v4}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bk;->b()I

    move-result v0

    iput v0, p1, Lmaps/au/ao;->e:I

    invoke-virtual {v6}, Lmaps/ac/bl;->b()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v6}, Lmaps/ac/bl;->b()I

    move-result v2

    if-ge v0, v2, :cond_13

    invoke-virtual {v6, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bk;->d()[I

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_2

    :goto_1
    invoke-virtual {v6, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bk;->b()I

    move-result v0

    iput v0, p1, Lmaps/au/ao;->f:I

    :cond_0
    :goto_2
    iput v5, p1, Lmaps/au/ao;->g:I

    invoke-virtual {v6}, Lmaps/ac/bl;->b()I

    move-result v0

    if-le v0, v1, :cond_5

    move v0, v1

    move v2, v3

    :goto_3
    invoke-virtual {v6}, Lmaps/ac/bl;->b()I

    move-result v7

    if-ge v0, v7, :cond_5

    invoke-virtual {v6, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/ac/bk;->d()[I

    move-result-object v7

    array-length v7, v7

    if-lez v7, :cond_4

    invoke-virtual {v6, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bk;->c()F

    move-result v2

    invoke-virtual {v6, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/ac/bk;->b()I

    move-result v7

    iput v7, p1, Lmaps/au/ao;->g:I

    :cond_1
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, Lmaps/ac/bl;->b()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v6, v5}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bk;->b()I

    move-result v0

    iput v0, p1, Lmaps/au/ao;->e:I

    goto :goto_2

    :cond_4
    iget v7, p1, Lmaps/au/ao;->g:I

    if-eqz v7, :cond_1

    invoke-virtual {v6, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/ac/bk;->c()F

    move-result v7

    cmpl-float v7, v7, v2

    if-lez v7, :cond_1

    iput v5, p1, Lmaps/au/ao;->g:I

    goto :goto_4

    :cond_5
    iget v0, p1, Lmaps/au/ao;->g:I

    if-eqz v0, :cond_6

    iput v5, p1, Lmaps/au/ao;->f:I

    :cond_6
    iget v0, p1, Lmaps/au/ao;->f:I

    if-eqz v0, :cond_8

    iget v0, p1, Lmaps/au/ao;->e:I

    invoke-static {v0}, Lmaps/al/d;->a(I)I

    move-result v0

    iget v2, p1, Lmaps/au/ao;->d:I

    invoke-static {v2}, Lmaps/al/d;->a(I)I

    move-result v2

    if-lt v0, v2, :cond_7

    iget v0, p1, Lmaps/au/ao;->c:F

    const/high16 v2, 0x41100000    # 9.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_8

    :cond_7
    iget v0, p1, Lmaps/au/ao;->f:I

    iput v0, p1, Lmaps/au/ao;->e:I

    :cond_8
    invoke-virtual {p0}, Lmaps/ac/bg;->b()Lmaps/ac/az;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v0

    if-lt v0, v1, :cond_a

    iget v0, p1, Lmaps/au/ao;->c:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_a

    iget v0, p1, Lmaps/au/ao;->e:I

    if-nez v0, :cond_9

    iget v0, p1, Lmaps/au/ao;->d:I

    if-nez v0, :cond_9

    iget v0, p1, Lmaps/au/ao;->f:I

    if-nez v0, :cond_9

    iget v0, p1, Lmaps/au/ao;->g:I

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lmaps/ac/bg;->k()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    move v0, v4

    :goto_5
    iput-boolean v0, p1, Lmaps/au/ao;->a:Z

    invoke-virtual {p0}, Lmaps/ac/bg;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    iget v0, p1, Lmaps/au/ao;->g:I

    if-nez v0, :cond_12

    iget v0, p1, Lmaps/au/ao;->f:I

    if-nez v0, :cond_12

    iget v0, p1, Lmaps/au/ao;->e:I

    shr-int/lit8 v1, v0, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-float v1, v1

    shr-int/lit8 v2, v0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-float v2, v2

    and-int/lit16 v0, v0, 0xff

    int-to-float v0, v0

    cmpl-float v3, v1, v2

    if-nez v3, :cond_b

    cmpl-float v3, v2, v0

    if-nez v3, :cond_b

    const/high16 v0, -0x40800000    # -1.0f

    :goto_6
    const/high16 v1, 0x42a00000    # 80.0f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_11

    const/high16 v1, 0x43200000    # 160.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_11

    move v0, v4

    :goto_7
    if-nez v0, :cond_12

    :goto_8
    iput-boolean v4, p1, Lmaps/au/ao;->b:Z

    return-void

    :cond_a
    move v0, v5

    goto :goto_5

    :cond_b
    cmpl-float v3, v1, v2

    if-ltz v3, :cond_c

    cmpl-float v3, v2, v0

    if-ltz v3, :cond_c

    sub-float/2addr v2, v0

    mul-float/2addr v2, v8

    sub-float v0, v1, v0

    div-float v0, v2, v0

    goto :goto_6

    :cond_c
    cmpl-float v3, v2, v1

    if-lez v3, :cond_d

    cmpl-float v3, v1, v0

    if-ltz v3, :cond_d

    const/high16 v3, 0x40000000    # 2.0f

    sub-float/2addr v1, v0

    sub-float v0, v2, v0

    div-float v0, v1, v0

    sub-float v0, v3, v0

    mul-float/2addr v0, v8

    goto :goto_6

    :cond_d
    cmpl-float v3, v2, v0

    if-ltz v3, :cond_e

    cmpl-float v3, v0, v1

    if-lez v3, :cond_e

    const/high16 v3, 0x40000000    # 2.0f

    sub-float/2addr v0, v1

    sub-float v1, v2, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v3

    mul-float/2addr v0, v8

    goto :goto_6

    :cond_e
    cmpl-float v3, v0, v2

    if-lez v3, :cond_f

    cmpl-float v3, v2, v1

    if-lez v3, :cond_f

    const/high16 v3, 0x40800000    # 4.0f

    sub-float/2addr v2, v1

    sub-float/2addr v0, v1

    div-float v0, v2, v0

    sub-float v0, v3, v0

    mul-float/2addr v0, v8

    goto :goto_6

    :cond_f
    cmpl-float v3, v0, v1

    if-lez v3, :cond_10

    cmpl-float v3, v1, v2

    if-ltz v3, :cond_10

    const/high16 v3, 0x40800000    # 4.0f

    sub-float/2addr v1, v2

    sub-float/2addr v0, v2

    div-float v0, v1, v0

    add-float/2addr v0, v3

    mul-float/2addr v0, v8

    goto :goto_6

    :cond_10
    const/high16 v3, 0x40c00000    # 6.0f

    sub-float/2addr v0, v2

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    sub-float v0, v3, v0

    mul-float/2addr v0, v8

    goto :goto_6

    :cond_11
    move v0, v5

    goto :goto_7

    :cond_12
    move v4, v5

    goto :goto_8

    :cond_13
    move v0, v1

    goto/16 :goto_1
.end method

.method public static a(Lmaps/as/a;FI)V
    .locals 4

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    const/16 v0, 0x302

    const/16 v2, 0x303

    invoke-interface {v1, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v0, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/as/a;->p()V

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    int-to-float v0, p2

    sub-float v0, p1, v0

    const/high16 v2, 0x40800000    # 4.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p0}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void

    :cond_0
    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/high16 v2, 0x40100000    # 2.25f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_3

    const/16 v0, 0x11

    if-lt p2, v0, :cond_3

    const/4 v0, 0x5

    goto :goto_0

    :cond_3
    const/16 v0, 0x16

    goto :goto_0
.end method

.method public static a(Lmaps/as/a;FILmaps/ap/b;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    const/16 v0, 0x302

    const/16 v2, 0x303

    invoke-interface {v1, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v0, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/as/a;->p()V

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    sget-object v0, Lmaps/ap/b;->b:Lmaps/ap/b;

    if-ne p3, v0, :cond_1

    const/high16 v0, 0x41800000    # 16.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x7

    :goto_0
    invoke-virtual {p0}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void

    :cond_0
    const/16 v0, 0x15

    goto :goto_0

    :cond_1
    int-to-float v0, p2

    sub-float v0, p1, v0

    const/high16 v2, 0x40800000    # 4.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const/high16 v2, 0x40100000    # 2.25f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_4

    const/4 v0, 0x3

    goto :goto_0

    :cond_4
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_5

    const/16 v0, 0x11

    if-lt p2, v0, :cond_5

    const/4 v0, 0x4

    goto :goto_0

    :cond_5
    const/4 v0, 0x6

    goto :goto_0
.end method

.method private static a(ILmaps/ac/bg;)Z
    .locals 1

    const/16 v0, 0xe

    if-lt p0, v0, :cond_0

    invoke-virtual {p1}, Lmaps/ac/bg;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(ILmaps/ac/bg;Lmaps/au/ao;Lmaps/au/an;)Z
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/ac/bg;->b()Lmaps/ac/az;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/az;->b()I

    move-result v4

    add-int/lit8 v5, v4, -0x1

    iget-boolean v0, p2, Lmaps/au/ao;->a:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {v3}, Lmaps/al/h;->a(Lmaps/ac/az;)I

    move-result v6

    iget v0, p3, Lmaps/au/an;->a:I

    if-lez v0, :cond_2

    iget v0, p3, Lmaps/au/an;->a:I

    add-int/2addr v0, v6

    const/16 v7, 0x4000

    if-le v0, v7, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v7

    iget v0, p2, Lmaps/au/ao;->f:I

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v8, p3, Lmaps/au/an;->i:Ljava/lang/Boolean;

    if-nez v8, :cond_6

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p3, Lmaps/au/an;->i:Ljava/lang/Boolean;

    :cond_3
    iget v0, p2, Lmaps/au/ao;->g:I

    if-eqz v0, :cond_7

    :goto_2
    invoke-virtual {v7}, Lmaps/ac/bl;->b()I

    move-result v0

    if-ge v2, v0, :cond_7

    invoke-virtual {v7, v2}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bk;->d()[I

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_4

    mul-int/lit8 v0, v5, 0x4

    mul-int/lit8 v8, v5, 0x2

    iget v9, p3, Lmaps/au/an;->g:I

    add-int/2addr v0, v9

    iput v0, p3, Lmaps/au/an;->g:I

    iget v0, p3, Lmaps/au/an;->h:I

    mul-int/lit8 v8, v8, 0x3

    add-int/2addr v0, v8

    iput v0, p3, Lmaps/au/an;->h:I

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iget-object v8, p3, Lmaps/au/an;->i:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eq v8, v0, :cond_3

    move v1, v2

    goto :goto_0

    :cond_7
    iget v0, p3, Lmaps/au/an;->a:I

    add-int/2addr v0, v6

    iput v0, p3, Lmaps/au/an;->a:I

    iget v0, p3, Lmaps/au/an;->b:I

    invoke-static {v3}, Lmaps/al/h;->b(Lmaps/ac/az;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p3, Lmaps/au/an;->b:I

    iget-boolean v0, p2, Lmaps/au/ao;->b:Z

    if-eqz v0, :cond_8

    iget v0, p3, Lmaps/au/an;->c:I

    add-int/2addr v0, v4

    iput v0, p3, Lmaps/au/an;->c:I

    iget v0, p3, Lmaps/au/an;->d:I

    mul-int/lit8 v2, v5, 0x2

    add-int/2addr v0, v2

    iput v0, p3, Lmaps/au/an;->d:I

    :cond_8
    invoke-static {p0, p1}, Lmaps/au/am;->a(ILmaps/ac/bg;)Z

    move-result v0

    if-eqz v0, :cond_0

    mul-int/lit8 v0, v5, 0x4

    mul-int/lit8 v2, v5, 0x2

    iget v3, p3, Lmaps/au/an;->e:I

    add-int/2addr v0, v3

    iput v0, p3, Lmaps/au/an;->e:I

    iget v0, p3, Lmaps/au/an;->f:I

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v0, v2

    iput v0, p3, Lmaps/au/an;->f:I

    goto/16 :goto_0
.end method

.method public static b(Lmaps/as/a;FI)V
    .locals 4

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    const/16 v0, 0x302

    const/16 v2, 0x303

    invoke-interface {v1, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v0, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/as/a;->p()V

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    int-to-float v0, p2

    sub-float v0, p1, v0

    const/high16 v2, 0x40800000    # 4.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_0

    const/16 v0, 0x19

    :goto_0
    invoke-virtual {p0}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void

    :cond_0
    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_1

    const/16 v0, 0x1a

    goto :goto_0

    :cond_1
    const/high16 v2, 0x40100000    # 2.25f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_2

    const/16 v0, 0x1b

    goto :goto_0

    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_3

    const/16 v0, 0x11

    if-lt p2, v0, :cond_3

    const/16 v0, 0x1c

    goto :goto_0

    :cond_3
    const/16 v0, 0x1d

    goto :goto_0
.end method

.method public static c(Lmaps/as/a;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    invoke-virtual {p0}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method public static d(Lmaps/as/a;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/as/a;->p()V

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    invoke-virtual {p0}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lmaps/au/am;->h:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/au/am;->i:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/am;->j:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/am;->k:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/am;->l:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/am;->m:Lmaps/at/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/au/am;->m:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lmaps/au/am;->r:Lmaps/at/n;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/au/am;->r:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lmaps/au/am;->s:Lmaps/al/c;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/au/am;->s:Lmaps/al/c;

    invoke-virtual {v1}, Lmaps/al/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lmaps/au/am;->t:Lmaps/at/d;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/au/am;->t:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lmaps/au/am;->u:Lmaps/at/d;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/au/am;->u:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lmaps/au/am;->v:Lmaps/at/n;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/au/am;->v:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lmaps/au/am;->w:Lmaps/at/i;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/au/am;->w:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lmaps/au/am;->x:Lmaps/at/d;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmaps/au/am;->x:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lmaps/au/am;->n:Lmaps/at/n;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/au/am;->n:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lmaps/au/am;->o:Lmaps/at/i;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/au/am;->o:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lmaps/au/am;->p:Lmaps/at/d;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lmaps/au/am;->p:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lmaps/au/am;->q:Lmaps/at/a;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lmaps/au/am;->q:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    return v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/am;->h:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->i:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->k:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->j:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->l:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->m:Lmaps/at/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/am;->m:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->a(Lmaps/as/a;)V

    :cond_0
    iget-object v0, p0, Lmaps/au/am;->r:Lmaps/at/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/am;->r:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->s:Lmaps/al/c;

    invoke-virtual {v0, p1}, Lmaps/al/c;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->t:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->u:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    :cond_1
    iget-object v0, p0, Lmaps/au/am;->v:Lmaps/at/n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/am;->v:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->w:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->x:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    :cond_2
    iget-object v0, p0, Lmaps/au/am;->n:Lmaps/at/n;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/au/am;->n:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->o:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->p:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->q:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->a(Lmaps/as/a;)V

    :cond_3
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 16

    invoke-virtual/range {p2 .. p2}, Lmaps/ar/a;->p()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->d:Lmaps/ac/bt;

    invoke-virtual {v2}, Lmaps/ac/bt;->b()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual/range {p3 .. p3}, Lmaps/ap/c;->b()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->r:Lmaps/at/n;

    if-eqz v2, :cond_0

    const/high16 v2, 0x3e800000    # 0.25f

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->r:Lmaps/at/n;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->s:Lmaps/al/c;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/al/c;->a(Lmaps/as/a;)V

    invoke-static/range {p1 .. p1}, Lmaps/al/a;->c(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->t:Lmaps/at/d;

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    invoke-static/range {p1 .. p1}, Lmaps/al/a;->d(Lmaps/as/a;)V

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->i:Lmaps/at/i;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/i;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->h:Lmaps/at/n;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->k:Lmaps/at/a;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/a;->c(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->u:Lmaps/at/d;

    if-eqz v2, :cond_1

    const/high16 v2, 0x3e800000    # 0.25f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->l:Lmaps/at/d;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->u:Lmaps/at/d;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->i:Lmaps/at/i;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/i;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->h:Lmaps/at/n;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->j:Lmaps/at/a;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/a;->c(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->u:Lmaps/at/d;

    if-eqz v2, :cond_3

    const/high16 v2, 0x3e800000    # 0.25f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->l:Lmaps/at/d;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->u:Lmaps/at/d;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto/16 :goto_0

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->v:Lmaps/at/n;

    if-eqz v1, :cond_0

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-virtual/range {p2 .. p2}, Lmaps/ar/a;->p()F

    move-result v2

    invoke-static {v2}, Lmaps/au/am;->a(F)F

    move-result v2

    mul-float v6, v1, v2

    float-to-double v1, v6

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-lez v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lmaps/au/am;->D:F

    cmpl-float v1, v6, v1

    if-eqz v1, :cond_a

    move-object/from16 v0, p0

    iput v6, v0, Lmaps/au/am;->D:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->w:Lmaps/at/i;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/i;->a(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/au/am;->w:Lmaps/at/i;

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->A:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/bg;

    const/high16 v2, 0x41800000    # 16.0f

    mul-float/2addr v2, v6

    const/high16 v3, 0x3f800000    # 1.0f

    div-float v9, v3, v6

    const/high16 v3, 0x3f800000    # 1.0f

    div-float v10, v3, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->d:Lmaps/ac/bt;

    invoke-virtual {v2}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bd;->f()I

    move-result v2

    invoke-virtual {v1}, Lmaps/ac/bg;->b()Lmaps/ac/az;

    move-result-object v11

    invoke-virtual {v11}, Lmaps/ac/az;->b()I

    move-result v3

    add-int/lit8 v12, v3, -0x1

    invoke-virtual {v1}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v3

    invoke-static {v3}, Lmaps/au/am;->a(Lmaps/ac/bl;)F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lmaps/au/am;->a(FI)F

    move-result v2

    mul-float v13, v4, v2

    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v12, :cond_5

    invoke-virtual {v11, v5}, Lmaps/ac/az;->b(I)F

    move-result v14

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    mul-float/2addr v14, v10

    const/high16 v15, 0x3f000000    # 0.5f

    cmpl-float v15, v14, v15

    if-lez v15, :cond_7

    mul-float v3, v13, v9

    const/high16 v4, 0x47000000    # 32768.0f

    mul-float/2addr v3, v4

    float-to-int v4, v3

    const/high16 v3, 0x47800000    # 65536.0f

    mul-float/2addr v3, v14

    float-to-int v3, v3

    float-to-int v15, v14

    int-to-float v15, v15

    sub-float/2addr v14, v15

    const/high16 v15, 0x3e000000    # 0.125f

    cmpl-float v15, v14, v15

    if-lez v15, :cond_6

    const/high16 v15, 0x3ec00000    # 0.375f

    cmpg-float v14, v14, v15

    if-gez v14, :cond_6

    const v2, 0xa000

    :cond_6
    :goto_2
    const v14, 0x8000

    sub-int/2addr v14, v4

    const v15, 0x8000

    add-int/2addr v4, v15

    invoke-virtual {v1}, Lmaps/ac/bg;->g()Z

    move-result v15

    if-eqz v15, :cond_8

    add-int v15, v3, v2

    invoke-interface {v7, v4, v15}, Lmaps/at/j;->a(II)V

    add-int/2addr v3, v2

    invoke-interface {v7, v14, v3}, Lmaps/at/j;->a(II)V

    invoke-interface {v7, v14, v2}, Lmaps/at/j;->a(II)V

    invoke-interface {v7, v4, v2}, Lmaps/at/j;->a(II)V

    :goto_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    :cond_7
    const v2, 0xc000

    goto :goto_2

    :cond_8
    invoke-interface {v7, v14, v2}, Lmaps/at/j;->a(II)V

    invoke-interface {v7, v4, v2}, Lmaps/at/j;->a(II)V

    add-int v15, v3, v2

    invoke-interface {v7, v4, v15}, Lmaps/at/j;->a(II)V

    add-int/2addr v2, v3

    invoke-interface {v7, v14, v2}, Lmaps/at/j;->a(II)V

    goto :goto_3

    :cond_9
    new-instance v1, Lmaps/v/e;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f4

    sget-object v6, Lmaps/v/g;->c:Lmaps/v/g;

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-direct/range {v1 .. v8}, Lmaps/v/e;-><init>(JJLmaps/v/g;II)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/au/am;->y:Lmaps/v/e;

    :cond_a
    const/high16 v1, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->y:Lmaps/v/e;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->y:Lmaps/v/e;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/v/e;->a(Lmaps/as/a;)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    const v2, 0x3f7d70a4    # 0.99f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_b

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/au/am;->y:Lmaps/v/e;

    :cond_b
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/am;->e:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/au/am;->e:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/am;->e:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/au/am;->e:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    mul-float/2addr v1, v6

    invoke-interface {v2, v3, v4, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->w:Lmaps/at/i;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/i;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->v:Lmaps/at/n;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->x:Lmaps/at/d;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto/16 :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->m:Lmaps/at/a;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->i:Lmaps/at/i;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/i;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->h:Lmaps/at/n;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->m:Lmaps/at/a;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lmaps/at/a;->c(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/am;->u:Lmaps/at/d;

    if-eqz v2, :cond_c

    const/high16 v2, 0x3e800000    # 0.25f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->l:Lmaps/at/d;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto/16 :goto_0

    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->u:Lmaps/at/d;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto/16 :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->n:Lmaps/at/n;

    if-eqz v1, :cond_0

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-virtual/range {p2 .. p2}, Lmaps/ar/a;->p()F

    move-result v2

    invoke-static {v2}, Lmaps/au/am;->a(F)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-double v2, v1

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-lez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/au/am;->E:F

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iput v1, v0, Lmaps/au/am;->E:F

    const/high16 v2, 0x41800000    # 16.0f

    mul-float/2addr v2, v1

    const/high16 v3, 0x3f800000    # 1.0f

    div-float/2addr v3, v1

    const/high16 v1, 0x3f800000    # 1.0f

    div-float v2, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->o:Lmaps/at/i;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/i;->a(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->B:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->d:Lmaps/ac/bt;

    invoke-virtual {v1}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bd;->f()I

    move-result v5

    :cond_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/bg;

    invoke-virtual {v1}, Lmaps/ac/bg;->b()Lmaps/ac/az;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/ac/az;->b()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v1}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v1

    invoke-static {v1}, Lmaps/au/am;->a(Lmaps/ac/bl;)F

    move-result v1

    const/high16 v8, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v5}, Lmaps/au/am;->a(FI)F

    move-result v1

    mul-float/2addr v8, v1

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v7, :cond_e

    invoke-virtual {v6, v1}, Lmaps/ac/az;->b(I)F

    move-result v9

    mul-float/2addr v9, v2

    mul-float v10, v8, v3

    const/high16 v11, 0x47000000    # 32768.0f

    mul-float/2addr v10, v11

    float-to-int v10, v10

    const/high16 v11, 0x47800000    # 65536.0f

    mul-float/2addr v9, v11

    float-to-int v9, v9

    const v11, 0x8000

    sub-int/2addr v11, v10

    const v12, 0x8000

    add-int/2addr v10, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/au/am;->o:Lmaps/at/i;

    invoke-virtual {v12, v10, v9}, Lmaps/at/i;->a(II)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/au/am;->o:Lmaps/at/i;

    invoke-virtual {v12, v11, v9}, Lmaps/at/i;->a(II)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/au/am;->o:Lmaps/at/i;

    const/4 v12, 0x0

    invoke-virtual {v9, v11, v12}, Lmaps/at/i;->a(II)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/au/am;->o:Lmaps/at/i;

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lmaps/at/i;->a(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->o:Lmaps/at/i;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/i;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->n:Lmaps/at/n;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->q:Lmaps/at/a;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/a;->c(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/am;->p:Lmaps/at/d;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final b()I
    .locals 3

    iget-object v0, p0, Lmaps/au/am;->h:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x1d0

    iget-object v1, p0, Lmaps/au/am;->i:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/am;->j:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/am;->k:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/am;->l:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/am;->m:Lmaps/at/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/au/am;->m:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lmaps/au/am;->r:Lmaps/at/n;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/au/am;->r:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lmaps/au/am;->s:Lmaps/al/c;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/au/am;->s:Lmaps/al/c;

    invoke-virtual {v1}, Lmaps/al/c;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lmaps/au/am;->t:Lmaps/at/d;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/au/am;->t:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lmaps/au/am;->u:Lmaps/at/d;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/au/am;->u:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lmaps/au/am;->v:Lmaps/at/n;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/au/am;->v:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lmaps/au/am;->w:Lmaps/at/i;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/au/am;->w:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lmaps/au/am;->x:Lmaps/at/d;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmaps/au/am;->x:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lmaps/au/am;->n:Lmaps/at/n;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/au/am;->n:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lmaps/au/am;->o:Lmaps/at/i;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/au/am;->o:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lmaps/au/am;->p:Lmaps/at/d;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lmaps/au/am;->p:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lmaps/au/am;->q:Lmaps/at/a;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lmaps/au/am;->q:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    add-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lmaps/au/am;->A:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bg;

    invoke-virtual {v0}, Lmaps/ac/bg;->j()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_c
    add-int/lit8 v0, v1, 0x18

    iget-object v1, p0, Lmaps/au/am;->B:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bg;

    invoke-virtual {v0}, Lmaps/ac/bg;->j()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_d
    return v1
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/am;->h:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->i:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->k:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->j:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->l:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->m:Lmaps/at/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/am;->m:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->b(Lmaps/as/a;)V

    :cond_0
    iget-object v0, p0, Lmaps/au/am;->r:Lmaps/at/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/am;->r:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->s:Lmaps/al/c;

    invoke-virtual {v0, p1}, Lmaps/al/c;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->t:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->u:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    :cond_1
    iget-object v0, p0, Lmaps/au/am;->v:Lmaps/at/n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/am;->v:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->w:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->x:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    :cond_2
    iget-object v0, p0, Lmaps/au/am;->n:Lmaps/at/n;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/au/am;->n:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->o:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->p:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/am;->q:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->b(Lmaps/as/a;)V

    :cond_3
    return-void
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lmaps/au/am;->h:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
