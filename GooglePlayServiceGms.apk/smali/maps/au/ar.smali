.class public final Lmaps/au/ar;
.super Lmaps/au/i;


# static fields
.field private static final b:[I

.field private static final c:I

.field private static volatile d:Z

.field private static final j:F


# instance fields
.field private final e:Lmaps/ac/bt;

.field private final f:Lmaps/at/n;

.field private final g:Lmaps/at/a;

.field private final h:Lmaps/at/i;

.field private final i:Lmaps/at/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/au/ar;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x64

    sput v0, Lmaps/au/ar;->c:I

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/au/ar;->d:Z

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lmaps/au/ar;->j:F

    return-void

    nop

    :array_0
    .array-data 4
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
    .end array-data
.end method

.method private constructor <init>(Lmaps/ac/bt;Lmaps/au/as;Ljava/util/Set;)V
    .locals 2

    invoke-direct {p0, p3}, Lmaps/au/i;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, Lmaps/au/ar;->e:Lmaps/ac/bt;

    new-instance v0, Lmaps/at/n;

    iget v1, p2, Lmaps/au/as;->a:I

    invoke-direct {v0, v1}, Lmaps/at/n;-><init>(I)V

    iput-object v0, p0, Lmaps/au/ar;->f:Lmaps/at/n;

    new-instance v0, Lmaps/at/a;

    iget v1, p2, Lmaps/au/as;->a:I

    invoke-direct {v0, v1}, Lmaps/at/a;-><init>(I)V

    iput-object v0, p0, Lmaps/au/ar;->g:Lmaps/at/a;

    new-instance v0, Lmaps/at/i;

    iget v1, p2, Lmaps/au/as;->a:I

    invoke-direct {v0, v1}, Lmaps/at/i;-><init>(I)V

    iput-object v0, p0, Lmaps/au/ar;->h:Lmaps/at/i;

    new-instance v0, Lmaps/at/d;

    iget v1, p2, Lmaps/au/as;->b:I

    invoke-direct {v0, v1}, Lmaps/at/d;-><init>(I)V

    iput-object v0, p0, Lmaps/au/ar;->i:Lmaps/at/d;

    return-void
.end method

.method public static a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;)Lmaps/au/ar;
    .locals 13

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v3, Lmaps/au/as;

    const/4 v0, 0x0

    invoke-direct {v3, v0}, Lmaps/au/as;-><init>(B)V

    invoke-interface {p2}, Lmaps/ac/cu;->b()V

    :cond_0
    invoke-interface {p2}, Lmaps/ac/cu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    instance-of v1, v0, Lmaps/ac/cg;

    if-eqz v1, :cond_3

    move-object v1, v0

    check-cast v1, Lmaps/ac/cg;

    invoke-virtual {v1}, Lmaps/ac/cg;->b()Lmaps/ac/az;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/az;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v4, v1, 0x4

    iget v5, v3, Lmaps/au/as;->a:I

    add-int/2addr v5, v4

    const/16 v6, 0x1000

    if-le v5, v6, :cond_2

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    invoke-interface {v0}, Lmaps/ac/n;->i()[I

    move-result-object v1

    array-length v4, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_0

    aget v5, v1, v0

    if-ltz v5, :cond_1

    array-length v6, p1

    if-ge v5, v6, :cond_1

    aget-object v5, p1, v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v5, v3, Lmaps/au/as;->a:I

    add-int/2addr v4, v5

    iput v4, v3, Lmaps/au/as;->a:I

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v1, v1, -0x1

    iget v5, v3, Lmaps/au/as;->b:I

    add-int/2addr v1, v4

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v1, v5

    iput v1, v3, Lmaps/au/as;->b:I

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    invoke-interface {p2}, Lmaps/ac/cu;->c()V

    new-instance v9, Lmaps/au/ar;

    invoke-direct {v9, p0, v3, v2}, Lmaps/au/ar;-><init>(Lmaps/ac/bt;Lmaps/au/as;Ljava/util/Set;)V

    invoke-virtual {p0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v10

    invoke-static {}, Lmaps/al/h;->a()Lmaps/al/h;

    move-result-object v0

    :goto_2
    invoke-interface {p2}, Lmaps/ac/cu;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p2}, Lmaps/ac/cu;->a()Lmaps/ac/n;

    move-result-object v1

    instance-of v2, v1, Lmaps/ac/cg;

    if-eqz v2, :cond_6

    move-object v5, v1

    check-cast v5, Lmaps/ac/cg;

    invoke-virtual {v5}, Lmaps/ac/cg;->b()Lmaps/ac/az;

    move-result-object v1

    invoke-virtual {v10}, Lmaps/ac/bd;->f()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x43800000    # 256.0f

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lmaps/ac/az;->b(F)Lmaps/ac/az;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/az;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, v9, Lmaps/au/ar;->f:Lmaps/at/n;

    invoke-virtual {v3}, Lmaps/at/n;->a()I

    move-result v3

    mul-int/lit8 v6, v2, 0x4

    add-int v7, v3, v6

    invoke-virtual {v10}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {v10}, Lmaps/ac/bd;->f()I

    move-result v4

    invoke-virtual {v5}, Lmaps/ac/cg;->d()Lmaps/ac/bl;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bl;->b()I

    move-result v8

    if-lez v8, :cond_5

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v8

    invoke-virtual {v8}, Lmaps/ac/bk;->c()F

    move-result v8

    invoke-virtual {v2}, Lmaps/ac/bl;->b()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v2, v11}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bk;->b()I

    move-result v11

    int-to-float v2, v4

    mul-float/2addr v2, v8

    const/high16 v12, 0x3fa00000    # 1.25f

    mul-float/2addr v2, v12

    const/high16 v12, 0x43800000    # 256.0f

    div-float/2addr v2, v12

    invoke-virtual {v5}, Lmaps/ac/cg;->c()Z

    move-result v12

    if-nez v12, :cond_4

    neg-float v2, v2

    :cond_4
    iget-object v12, v9, Lmaps/au/ar;->g:Lmaps/at/a;

    invoke-virtual {v12, v7}, Lmaps/at/a;->a(I)V

    iget-object v7, v9, Lmaps/au/ar;->g:Lmaps/at/a;

    invoke-virtual {v7, v11, v6}, Lmaps/at/a;->b(II)V

    invoke-virtual {v5}, Lmaps/ac/cg;->g()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    const/high16 v5, 0x42a00000    # 80.0f

    div-float/2addr v5, v8

    :goto_3
    iget-object v6, v9, Lmaps/au/ar;->f:Lmaps/at/n;

    iget-object v7, v9, Lmaps/au/ar;->i:Lmaps/at/d;

    iget-object v8, v9, Lmaps/au/ar;->h:Lmaps/at/i;

    invoke-virtual/range {v0 .. v8}, Lmaps/al/h;->a(Lmaps/ac/az;FLmaps/ac/av;IFLmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V

    :cond_5
    invoke-interface {p2}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    goto/16 :goto_2

    :pswitch_0
    const/high16 v5, 0x43b40000    # 360.0f

    div-float/2addr v5, v8

    goto :goto_3

    :pswitch_1
    const/high16 v5, 0x43700000    # 240.0f

    div-float/2addr v5, v8

    goto :goto_3

    :pswitch_2
    const/high16 v5, 0x43200000    # 160.0f

    div-float/2addr v5, v8

    goto :goto_3

    :cond_6
    return-object v9

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static c(Lmaps/as/a;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lmaps/au/ar;->f:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/au/ar;->g:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/ar;->h:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/ar;->i:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/ar;->f:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ar;->g:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ar;->h:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ar;->i:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 8

    const/16 v7, 0x1702

    const/16 v6, 0x1700

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/high16 v3, 0x10000

    iget-object v0, p0, Lmaps/au/ar;->i:Lmaps/at/d;

    invoke-virtual {v0}, Lmaps/at/d;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/au/ar;->f:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ar;->h:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v0

    iget-object v1, p0, Lmaps/au/ar;->e:Lmaps/ac/bt;

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    sget v2, Lmaps/au/ar;->j:F

    invoke-interface {v1, v2, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :cond_2
    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v1

    sget-object v2, Lmaps/ap/b;->a:Lmaps/ap/b;

    if-eq v1, v2, :cond_3

    sget-object v2, Lmaps/ap/b;->d:Lmaps/ap/b;

    if-ne v1, v2, :cond_4

    :cond_3
    invoke-virtual {p1}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v3, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v1, p0, Lmaps/au/ar;->i:Lmaps/at/d;

    invoke-virtual {v1, p1, v5}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    :cond_4
    invoke-virtual {p1}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, Lmaps/as/a;->p()V

    iget-object v1, p0, Lmaps/au/ar;->g:Lmaps/at/a;

    invoke-virtual {v1, p1}, Lmaps/at/a;->c(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/ar;->i:Lmaps/at/d;

    invoke-virtual {v1, p1, v5}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v6}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public final b()I
    .locals 2

    iget-object v0, p0, Lmaps/au/ar;->f:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0xb8

    iget-object v1, p0, Lmaps/au/ar;->g:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/ar;->h:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/ar;->i:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/ar;->f:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ar;->g:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ar;->h:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ar;->i:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    return-void
.end method
