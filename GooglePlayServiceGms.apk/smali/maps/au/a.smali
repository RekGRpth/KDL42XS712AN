.class public final Lmaps/au/a;
.super Lmaps/au/i;


# static fields
.field private static final b:[I

.field private static c:I

.field private static final k:Lmaps/ac/bl;

.field private static final l:Lmaps/ac/bl;

.field private static final m:Ljava/lang/ThreadLocal;


# instance fields
.field private final d:Lmaps/at/n;

.field private final e:Lmaps/at/n;

.field private final f:Lmaps/al/c;

.field private final g:Lmaps/al/c;

.field private h:Lmaps/an/y;

.field private i:Lmaps/an/y;

.field private final j:Lmaps/au/c;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x1

    const/4 v1, -0x1

    const v10, -0x45749f

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/au/a;->b:[I

    const/16 v0, 0x4000

    sput v0, Lmaps/au/a;->c:I

    new-instance v0, Lmaps/ac/bl;

    const/4 v2, 0x2

    new-array v3, v9, [I

    new-array v4, v11, [Lmaps/ac/bk;

    new-instance v6, Lmaps/ac/bk;

    const/high16 v7, 0x40000000    # 2.0f

    new-array v8, v9, [I

    invoke-direct {v6, v10, v7, v8, v9}, Lmaps/ac/bk;-><init>(IF[II)V

    aput-object v6, v4, v9

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/ac/bl;-><init>(II[I[Lmaps/ac/bk;Lmaps/ac/br;Lmaps/ac/bq;Lmaps/ac/bk;Lmaps/ac/x;)V

    sput-object v0, Lmaps/au/a;->k:Lmaps/ac/bl;

    new-instance v0, Lmaps/ac/bl;

    const/4 v2, 0x2

    new-array v3, v9, [I

    new-array v4, v11, [Lmaps/ac/bk;

    new-instance v6, Lmaps/ac/bk;

    const/high16 v7, 0x3fc00000    # 1.5f

    new-array v8, v9, [I

    invoke-direct {v6, v10, v7, v8, v9}, Lmaps/ac/bk;-><init>(IF[II)V

    aput-object v6, v4, v9

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/ac/bl;-><init>(II[I[Lmaps/ac/bk;Lmaps/ac/br;Lmaps/ac/bq;Lmaps/ac/bk;Lmaps/ac/x;)V

    sput-object v0, Lmaps/au/a;->l:Lmaps/ac/bl;

    new-instance v0, Lmaps/au/b;

    invoke-direct {v0}, Lmaps/au/b;-><init>()V

    sput-object v0, Lmaps/au/a;->m:Ljava/lang/ThreadLocal;

    return-void

    :array_0
    .array-data 4
        0x0
        0x2
        0x2
        0x4
        0x2
        0x4
        0x4
        0x6
    .end array-data
.end method

.method private constructor <init>(IILjava/util/Set;Lmaps/au/c;Lmaps/as/a;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p3}, Lmaps/au/i;-><init>(Ljava/util/Set;)V

    iput-object v0, p0, Lmaps/au/a;->i:Lmaps/an/y;

    iput-object v0, p0, Lmaps/au/a;->h:Lmaps/an/y;

    new-instance v0, Lmaps/at/p;

    invoke-direct {v0, p1, v1}, Lmaps/at/p;-><init>(IB)V

    iput-object v0, p0, Lmaps/au/a;->e:Lmaps/at/n;

    new-instance v0, Lmaps/at/p;

    invoke-direct {v0, p2, v1}, Lmaps/at/p;-><init>(IB)V

    iput-object v0, p0, Lmaps/au/a;->d:Lmaps/at/n;

    new-instance v0, Lmaps/al/c;

    invoke-virtual {p5}, Lmaps/as/a;->H()Lmaps/al/a;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lmaps/al/c;-><init>(ILmaps/al/a;)V

    iput-object v0, p0, Lmaps/au/a;->g:Lmaps/al/c;

    new-instance v0, Lmaps/al/c;

    invoke-virtual {p5}, Lmaps/as/a;->H()Lmaps/al/a;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lmaps/al/c;-><init>(ILmaps/al/a;)V

    iput-object v0, p0, Lmaps/au/a;->f:Lmaps/al/c;

    iput-object p4, p0, Lmaps/au/a;->j:Lmaps/au/c;

    return-void
.end method

.method static a(Lmaps/ac/f;)I
    .locals 1

    invoke-virtual {p0}, Lmaps/ac/f;->d()Lmaps/ac/bl;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bl;->c()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lmaps/ac/f;->b()Lmaps/ac/cj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/cj;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method public static a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;Lmaps/au/x;Lmaps/au/d;Lmaps/as/a;)Lmaps/au/a;
    .locals 13

    invoke-virtual {p0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    if-eqz p4, :cond_e

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v6, v1

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {p2}, Lmaps/ac/cu;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p2}, Lmaps/ac/cu;->a()Lmaps/ac/n;

    move-result-object v5

    instance-of v1, v5, Lmaps/ac/f;

    if-eqz v1, :cond_9

    move-object v1, v5

    check-cast v1, Lmaps/ac/f;

    invoke-static {v1}, Lmaps/au/a;->a(Lmaps/ac/f;)I

    move-result v9

    invoke-static {v1}, Lmaps/au/a;->c(Lmaps/ac/f;)I

    move-result v10

    sget v11, Lmaps/au/a;->c:I

    if-gt v9, v11, :cond_0

    sget v11, Lmaps/au/a;->c:I

    if-le v10, v11, :cond_1

    :cond_0
    invoke-interface {p2}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_1
    add-int v11, v2, v9

    sget v12, Lmaps/au/a;->c:I

    if-gt v11, v12, :cond_9

    add-int v11, v3, v10

    sget v12, Lmaps/au/a;->c:I

    if-gt v11, v12, :cond_9

    add-int/2addr v2, v9

    add-int/2addr v3, v10

    invoke-interface {v5}, Lmaps/ac/n;->i()[I

    move-result-object v9

    array-length v10, v9

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v10, :cond_3

    aget v11, v9, v5

    if-ltz v11, :cond_2

    array-length v12, p1

    if-ge v11, v12, :cond_2

    aget-object v11, p1, v11

    invoke-virtual {v4, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_8

    invoke-virtual {v1}, Lmaps/ac/f;->l()Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x1

    :goto_3
    invoke-static {v1}, Lmaps/au/a;->b(Lmaps/ac/f;)Z

    move-result v9

    if-nez v5, :cond_4

    if-nez v9, :cond_7

    :cond_4
    invoke-virtual {v1}, Lmaps/ac/f;->c()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-virtual {v1}, Lmaps/ac/f;->b()Lmaps/ac/cj;

    move-result-object v10

    invoke-virtual {v1}, Lmaps/ac/f;->g()[B

    move-result-object v11

    invoke-static {v10, v11}, Lmaps/al/p;->a(Lmaps/ac/cj;[B)Ljava/util/List;

    move-result-object v10

    if-eqz p3, :cond_5

    if-nez v9, :cond_5

    const/4 v9, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v1, v10, v9}, Lmaps/au/a;->a(Lmaps/au/x;Lmaps/ac/f;Ljava/util/List;Lmaps/ac/bt;)V

    :cond_5
    if-eqz p3, :cond_6

    invoke-virtual {v1}, Lmaps/ac/f;->l()Z

    move-result v9

    if-eqz v9, :cond_6

    move-object/from16 v0, p3

    invoke-static {v0, v1, v10, p0}, Lmaps/au/a;->a(Lmaps/au/x;Lmaps/ac/f;Ljava/util/List;Lmaps/ac/bt;)V

    :cond_6
    if-eqz v5, :cond_7

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_7
    invoke-interface {p2}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    goto/16 :goto_1

    :cond_8
    const/4 v5, 0x0

    goto :goto_3

    :cond_9
    const/4 v5, 0x0

    if-eqz v6, :cond_c

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_a
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/f;

    invoke-virtual {v1}, Lmaps/ac/f;->l()Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_b
    new-instance v5, Lmaps/au/c;

    move-object/from16 v0, p4

    invoke-direct {v5, v7, v6, v9, v0}, Lmaps/au/c;-><init>(Lmaps/ac/bd;Ljava/util/List;Ljava/util/List;Lmaps/au/d;)V

    :cond_c
    new-instance v1, Lmaps/au/a;

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lmaps/au/a;-><init>(IILjava/util/Set;Lmaps/au/c;Lmaps/as/a;)V

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ac/f;

    invoke-direct {v1, v7, v2}, Lmaps/au/a;->a(Lmaps/ac/bd;Lmaps/ac/f;)V

    goto :goto_5

    :cond_d
    return-object v1

    :cond_e
    move-object v6, v1

    goto/16 :goto_0
.end method

.method private a(Lmaps/ac/bd;Lmaps/ac/f;)V
    .locals 17

    invoke-virtual/range {p2 .. p2}, Lmaps/ac/f;->d()Lmaps/ac/bl;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lmaps/ac/f;->b()Lmaps/ac/cj;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/cj;->a()I

    move-result v12

    if-nez v12, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v4}, Lmaps/ac/bl;->c()I

    move-result v2

    if-lez v2, :cond_7

    const/4 v2, 0x1

    move v7, v2

    :goto_1
    invoke-static/range {p2 .. p2}, Lmaps/au/a;->b(Lmaps/ac/f;)Z

    move-result v13

    if-nez v7, :cond_2

    if-eqz v13, :cond_0

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/bd;->f()I

    move-result v14

    if-eqz v7, :cond_8

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lmaps/ac/bl;->a(I)I

    move-result v2

    move v11, v2

    :goto_2
    if-eqz v13, :cond_9

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bk;->b()I

    move-result v2

    move v8, v2

    :goto_3
    invoke-virtual/range {p2 .. p2}, Lmaps/ac/f;->g()[B

    move-result-object v15

    const/4 v10, 0x0

    const/4 v9, 0x0

    sget-object v2, Lmaps/au/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/ac/av;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    sget-object v2, Lmaps/au/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/ac/av;

    const/4 v5, 0x1

    aget-object v5, v2, v5

    sget-object v2, Lmaps/au/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmaps/ac/av;

    const/4 v6, 0x2

    aget-object v6, v2, v6

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v12, :cond_a

    invoke-virtual/range {v1 .. v6}, Lmaps/ac/cj;->a(ILmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->e:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->e:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->e:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    add-int/lit8 v10, v10, 0x3

    :cond_3
    if-eqz v13, :cond_6

    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x1

    if-eqz v16, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->d:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->d:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    add-int/lit8 v9, v9, 0x2

    :cond_4
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x2

    if-eqz v16, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->d:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->d:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    add-int/lit8 v9, v9, 0x2

    :cond_5
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x4

    if-eqz v16, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->d:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/au/a;->d:Lmaps/at/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    add-int/lit8 v9, v9, 0x2

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_7
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/a;->g:Lmaps/al/c;

    invoke-virtual {v1, v11, v10}, Lmaps/al/c;->a(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/a;->f:Lmaps/al/c;

    invoke-virtual {v1, v8, v9}, Lmaps/al/c;->a(II)V

    goto/16 :goto_0
.end method

.method public static a(Lmaps/as/a;Lmaps/ap/c;)V
    .locals 5

    const/high16 v4, 0x10000

    invoke-virtual {p1}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aj/ad;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/as/a;->r()V

    :cond_0
    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method

.method private static a(Lmaps/au/x;Lmaps/ac/f;Ljava/util/List;Lmaps/ac/bt;)V
    .locals 11

    if-eqz p3, :cond_2

    sget-object v0, Lmaps/ac/bx;->d:Lmaps/ac/bx;

    invoke-virtual {p3, v0}, Lmaps/ac/bt;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v0

    check-cast v0, Lmaps/ac/ae;

    invoke-virtual {v0}, Lmaps/ac/ae;->b()Lmaps/ac/r;

    move-result-object v1

    invoke-virtual {p3}, Lmaps/ac/bt;->b()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x41900000    # 18.0f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    sget-object v0, Lmaps/au/a;->k:Lmaps/ac/bl;

    :goto_0
    move-object v4, v0

    :goto_1
    invoke-virtual {p1}, Lmaps/ac/f;->b()Lmaps/ac/cj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/cj;->a()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lmaps/ac/bl;->b()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/ac/f;->c()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lmaps/au/a;->l:Lmaps/ac/bl;

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/ac/f;->a()Lmaps/ac/o;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/ac/f;->d()Lmaps/ac/bl;

    move-result-object v4

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lmaps/ac/f;->f()I

    move-result v6

    invoke-virtual {p1}, Lmaps/ac/f;->h()I

    invoke-virtual {p1}, Lmaps/ac/f;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lmaps/ac/f;->i()[I

    move-result-object v8

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ac/az;

    new-instance v0, Lmaps/ac/aj;

    const/4 v3, 0x0

    const/4 v7, 0x0

    if-eqz v1, :cond_4

    const/4 v9, 0x1

    :goto_3
    invoke-direct/range {v0 .. v9}, Lmaps/ac/aj;-><init>(Lmaps/ac/o;Lmaps/ac/az;[Lmaps/ac/ag;Lmaps/ac/bl;Ljava/lang/String;IF[IZ)V

    invoke-virtual {p0, v0}, Lmaps/au/x;->a(Lmaps/ac/n;)V

    goto :goto_2

    :cond_4
    const/4 v9, 0x0

    goto :goto_3
.end method

.method private static b(Lmaps/ac/f;)Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/ac/f;->d()Lmaps/ac/bl;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bl;->b()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ac/f;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    invoke-virtual {v2}, Lmaps/ac/bl;->b()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bk;->c()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    invoke-virtual {v2, v0}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bk;->e()Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static c(Lmaps/ac/f;)I
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, Lmaps/au/a;->b(Lmaps/ac/f;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lmaps/ac/f;->g()[B

    move-result-object v2

    move v1, v0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-byte v3, v2, v0

    and-int/lit8 v3, v3, 0x7

    sget-object v4, Lmaps/au/a;->b:[I

    aget v3, v4, v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/ThreadLocal;
    .locals 1

    sget-object v0, Lmaps/au/a;->m:Ljava/lang/ThreadLocal;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/a;->f:Lmaps/al/c;

    invoke-virtual {v1}, Lmaps/al/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/a;->g:Lmaps/al/c;

    invoke-virtual {v1}, Lmaps/al/c;->a()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    invoke-virtual {v0}, Lmaps/au/c;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/a;->g:Lmaps/al/c;

    invoke-virtual {v0, p1}, Lmaps/al/c;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/a;->f:Lmaps/al/c;

    invoke-virtual {v0, p1}, Lmaps/al/c;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    invoke-virtual {v0, p1}, Lmaps/au/c;->b(Lmaps/as/a;)V

    :cond_0
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p3}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/aj/ad;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p1}, Lmaps/al/a;->c(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->a()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/a;->g:Lmaps/al/c;

    invoke-virtual {v1, p1}, Lmaps/al/c;->a(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->a()I

    move-result v1

    invoke-interface {v0, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_0
    iget-object v1, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->a()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/a;->f:Lmaps/al/c;

    invoke-virtual {v1, p1}, Lmaps/al/c;->a(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->a()I

    move-result v1

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_1
    invoke-static {p1}, Lmaps/al/a;->d(Lmaps/as/a;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {v1}, Lmaps/aj/ad;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v1, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->a()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->a()I

    move-result v1

    invoke-interface {v0, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_4
    iget-object v1, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->a()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->a()I

    move-result v1

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lmaps/aj/ad;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    invoke-virtual {v0, p1}, Lmaps/au/c;->a(Lmaps/as/a;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    iget-object v0, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x9c

    iget-object v1, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v1}, Lmaps/at/n;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/a;->f:Lmaps/al/c;

    invoke-virtual {v1}, Lmaps/al/c;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/a;->g:Lmaps/al/c;

    invoke-virtual {v1}, Lmaps/al/c;->b()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    invoke-virtual {v0}, Lmaps/au/c;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/a;->e:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/a;->d:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/a;->g:Lmaps/al/c;

    invoke-virtual {v0, p1}, Lmaps/al/c;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/a;->f:Lmaps/al/c;

    invoke-virtual {v0, p1}, Lmaps/al/c;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/a;->j:Lmaps/au/c;

    invoke-virtual {v0, p1}, Lmaps/au/c;->c(Lmaps/as/a;)V

    :cond_0
    return-void
.end method
