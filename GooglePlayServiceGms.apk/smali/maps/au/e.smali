.class public final Lmaps/au/e;
.super Lmaps/au/i;


# static fields
.field private static final b:Lmaps/ac/av;

.field private static final c:Ljava/util/Comparator;


# instance fields
.field private final d:Lmaps/at/n;

.field private final e:Lmaps/at/a;

.field private f:Lmaps/v/o;

.field private final g:Lmaps/at/d;

.field private final h:Lmaps/at/d;

.field private final i:Lmaps/at/e;

.field private final j:Lmaps/ac/av;

.field private final k:Lmaps/ac/av;

.field private final l:Lmaps/ac/av;

.field private final m:Lmaps/ac/av;

.field private final n:Lmaps/ac/av;

.field private final o:Lmaps/ac/av;

.field private final p:Lmaps/ac/av;

.field private final q:Lmaps/ac/av;

.field private final r:Lmaps/ac/av;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const v2, 0xb504

    new-instance v0, Lmaps/ac/av;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lmaps/ac/av;-><init>(III)V

    sput-object v0, Lmaps/au/e;->b:Lmaps/ac/av;

    new-instance v0, Lmaps/au/f;

    invoke-direct {v0}, Lmaps/au/f;-><init>()V

    sput-object v0, Lmaps/au/e;->c:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(Lmaps/au/g;Ljava/util/Set;)V
    .locals 3

    invoke-direct {p0, p2}, Lmaps/au/i;-><init>(Ljava/util/Set;)V

    new-instance v0, Lmaps/v/o;

    invoke-direct {v0}, Lmaps/v/o;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->f:Lmaps/v/o;

    new-instance v0, Lmaps/at/p;

    iget v1, p1, Lmaps/au/g;->a:I

    invoke-direct {v0, v1}, Lmaps/at/p;-><init>(I)V

    iput-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    new-instance v0, Lmaps/at/c;

    iget v1, p1, Lmaps/au/g;->a:I

    invoke-direct {v0, v1}, Lmaps/at/c;-><init>(I)V

    iput-object v0, p0, Lmaps/au/e;->e:Lmaps/at/a;

    new-instance v0, Lmaps/at/f;

    iget v1, p1, Lmaps/au/g;->c:I

    iget v2, p1, Lmaps/au/g;->b:I

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Lmaps/at/f;-><init>(I)V

    iput-object v0, p0, Lmaps/au/e;->g:Lmaps/at/d;

    new-instance v0, Lmaps/at/f;

    iget v1, p1, Lmaps/au/g;->d:I

    invoke-direct {v0, v1}, Lmaps/at/f;-><init>(I)V

    iput-object v0, p0, Lmaps/au/e;->h:Lmaps/at/d;

    new-instance v0, Lmaps/at/d;

    iget v1, p1, Lmaps/au/g;->c:I

    invoke-direct {v0, v1}, Lmaps/at/d;-><init>(I)V

    iput-object v0, p0, Lmaps/au/e;->i:Lmaps/at/e;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->m:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->n:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->o:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->p:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->q:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/e;->r:Lmaps/ac/av;

    return-void
.end method

.method private static a(I)I
    .locals 2

    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    const/16 v1, 0xa0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const v1, 0xffffff

    and-int/2addr v1, p0

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(Lmaps/ac/bt;[Ljava/lang/String;Lmaps/ac/cu;)Lmaps/au/e;
    .locals 13

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {}, Lmaps/ap/p;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ab/q;->j()Lmaps/ab/e;

    move-result-object v0

    move-object v2, v0

    :goto_0
    new-instance v7, Ljava/util/ArrayList;

    const/16 v0, 0x80

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v8, Lmaps/au/g;

    invoke-direct {v8}, Lmaps/au/g;-><init>()V

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    :goto_1
    invoke-interface {p2}, Lmaps/ac/cu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p2}, Lmaps/ac/cu;->a()Lmaps/ac/n;

    move-result-object v1

    instance-of v0, v1, Lmaps/ac/g;

    if-eqz v0, :cond_6

    move-object v0, v1

    check-cast v0, Lmaps/ac/g;

    invoke-virtual {v0}, Lmaps/ac/g;->b()Lmaps/ac/cj;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/ac/cj;->a()I

    move-result v6

    invoke-virtual {v0}, Lmaps/ac/g;->c()I

    move-result v10

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v11, v10, 0x4

    iget v12, v8, Lmaps/au/g;->a:I

    add-int/2addr v12, v6

    add-int/2addr v11, v12

    const/16 v12, 0x4000

    if-le v11, v12, :cond_1

    iget v12, v8, Lmaps/au/g;->a:I

    if-lez v12, :cond_1

    move v6, v4

    :goto_2
    if-eqz v6, :cond_6

    invoke-interface {v1}, Lmaps/ac/n;->i()[I

    move-result-object v6

    array-length v10, v6

    move v1, v4

    :goto_3
    if-ge v1, v10, :cond_2

    aget v11, v6, v1

    if-ltz v11, :cond_0

    array-length v12, p1

    if-ge v11, v12, :cond_0

    aget-object v11, p1, v11

    invoke-virtual {v9, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_1
    iput v11, v8, Lmaps/au/g;->a:I

    iget v11, v8, Lmaps/au/g;->b:I

    add-int/2addr v6, v11

    iput v6, v8, Lmaps/au/g;->b:I

    iget v6, v8, Lmaps/au/g;->c:I

    mul-int/lit8 v11, v10, 0x6

    add-int/2addr v6, v11

    iput v6, v8, Lmaps/au/g;->c:I

    iget v6, v8, Lmaps/au/g;->d:I

    mul-int/lit8 v10, v10, 0x2

    add-int/2addr v6, v10

    iput v6, v8, Lmaps/au/g;->d:I

    move v6, v5

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Lmaps/ac/g;->g()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lmaps/ac/g;->a()Lmaps/ac/o;

    move-result-object v1

    invoke-interface {v2, v1}, Lmaps/ab/e;->a(Lmaps/ac/o;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    move v1, v5

    :goto_4
    if-nez v1, :cond_4

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-interface {p2}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_4

    :cond_6
    sget-object v0, Lmaps/au/e;->c:Ljava/util/Comparator;

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {p0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/av;->b()D

    move-result-wide v5

    invoke-static {v5, v6}, Lmaps/ac/av;->a(D)D

    move-result-wide v5

    double-to-float v2, v5

    new-instance v5, Lmaps/au/e;

    invoke-direct {v5, v8, v9}, Lmaps/au/e;-><init>(Lmaps/au/g;Ljava/util/Set;)V

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/g;

    invoke-direct {v5, v1, v0, v2}, Lmaps/au/e;->a(Lmaps/ac/bd;Lmaps/ac/g;F)V

    goto :goto_5

    :cond_7
    iget-object v1, v5, Lmaps/au/e;->g:Lmaps/at/d;

    iget-object v0, v5, Lmaps/au/e;->i:Lmaps/at/e;

    check-cast v0, Lmaps/at/d;

    iget-object v2, v5, Lmaps/au/e;->i:Lmaps/at/e;

    invoke-interface {v2}, Lmaps/at/e;->b()I

    move-result v2

    invoke-virtual {v1, v0, v4, v2}, Lmaps/at/d;->a(Lmaps/at/d;II)V

    iget-object v0, v5, Lmaps/au/e;->i:Lmaps/at/e;

    check-cast v0, Lmaps/at/d;

    invoke-virtual {v0, v3}, Lmaps/at/d;->a(Lmaps/as/a;)V

    return-object v5

    :cond_8
    move-object v2, v3

    goto/16 :goto_0
.end method

.method private a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;II)V
    .locals 7

    iget-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->a()I

    move-result v0

    iget-object v1, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v1, p1, p5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v1, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v1, p3, p5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v1, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v1, p2, p5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v1, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v1, p4, p5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v1, p0, Lmaps/au/e;->i:Lmaps/at/e;

    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v4, v0, 0x2

    invoke-interface {v1, v2, v0, v3, v4}, Lmaps/at/e;->a(IIII)V

    iget-object v1, p0, Lmaps/au/e;->h:Lmaps/at/d;

    add-int/lit8 v2, v0, 0x1

    int-to-short v2, v2

    add-int/lit8 v0, v0, 0x3

    int-to-short v0, v0

    invoke-virtual {v1, v2, v0}, Lmaps/at/d;->a(SS)V

    iget-object v0, p0, Lmaps/au/e;->r:Lmaps/ac/av;

    invoke-static {p2, p1, v0}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->e:Lmaps/at/a;

    iget-object v0, p0, Lmaps/au/e;->r:Lmaps/ac/av;

    const/high16 v2, -0x1000000

    and-int/2addr v2, p6

    shr-int/lit8 v3, p6, 0x10

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v4, p6, 0x8

    and-int/lit16 v4, v4, 0xff

    and-int/lit16 v5, p6, 0xff

    sget-object v6, Lmaps/au/e;->b:Lmaps/ac/av;

    invoke-static {v0, v6}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;)F

    move-result v6

    invoke-virtual {v0}, Lmaps/ac/av;->i()F

    move-result v0

    div-float v0, v6, v0

    float-to-int v0, v0

    if-gez v0, :cond_0

    neg-int v0, v0

    :cond_0
    mul-int/lit16 v0, v0, 0x4ccc

    shr-int/lit8 v0, v0, 0x10

    const v6, 0xb333

    add-int/2addr v0, v6

    mul-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0x10

    mul-int/2addr v4, v0

    shr-int/lit8 v4, v4, 0x10

    mul-int/2addr v0, v5

    shr-int/lit8 v0, v0, 0x10

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    shl-int/lit8 v3, v4, 0x8

    or-int/2addr v2, v3

    or-int/2addr v0, v2

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Lmaps/at/a;->b(II)V

    return-void
.end method

.method private a(Lmaps/ac/bd;Lmaps/ac/g;F)V
    .locals 13

    invoke-virtual {p2}, Lmaps/ac/g;->d()Lmaps/ac/bl;

    move-result-object v0

    invoke-virtual {p2}, Lmaps/ac/g;->b()Lmaps/ac/cj;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/cj;->a()I

    move-result v10

    invoke-virtual {v0}, Lmaps/ac/bl;->c()I

    move-result v1

    if-eqz v10, :cond_0

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v11

    invoke-virtual {p1}, Lmaps/ac/bd;->f()I

    move-result v5

    invoke-virtual {p2}, Lmaps/ac/g;->h()I

    move-result v2

    invoke-virtual {p2}, Lmaps/ac/g;->k()I

    move-result v12

    iget-object v3, p0, Lmaps/au/e;->p:Lmaps/ac/av;

    const/4 v4, 0x0

    const/4 v6, 0x0

    int-to-float v2, v2

    mul-float v2, v2, p3

    float-to-int v2, v2

    invoke-virtual {v3, v4, v6, v2}, Lmaps/ac/av;->a(III)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lmaps/ac/bl;->a(I)I

    move-result v2

    invoke-static {v2}, Lmaps/au/e;->a(I)I

    move-result v6

    const/4 v2, 0x1

    if-le v1, v2, :cond_6

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/ac/bl;->a(I)I

    move-result v0

    invoke-static {v0}, Lmaps/au/e;->a(I)I

    move-result v0

    move v7, v0

    :goto_0
    const/4 v0, 0x0

    move v8, v0

    :goto_1
    if-ge v8, v10, :cond_0

    iget-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->a()I

    move-result v0

    iget-object v1, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    invoke-virtual {v9, v8, v1, v2, v3}, Lmaps/ac/cj;->a(ILmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    invoke-static {v1, v11, v2}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    invoke-static {v1, v11, v2}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    invoke-static {v1, v11, v2}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    if-eqz v12, :cond_2

    iget-object v1, p0, Lmaps/au/e;->q:Lmaps/ac/av;

    const/4 v2, 0x0

    const/4 v3, 0x0

    int-to-float v4, v12

    mul-float v4, v4, p3

    float-to-int v4, v4

    invoke-virtual {v1, v2, v3, v4}, Lmaps/ac/av;->a(III)V

    iget-object v1, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->q:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    invoke-static {v1, v2, v3}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->q:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    invoke-static {v1, v2, v3}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->q:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    invoke-static {v1, v2, v3}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    :cond_2
    iget-object v1, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->p:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->m:Lmaps/ac/av;

    invoke-static {v1, v2, v3}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->p:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->n:Lmaps/ac/av;

    invoke-static {v1, v2, v3}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->p:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->o:Lmaps/ac/av;

    invoke-static {v1, v2, v3}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/au/e;->d:Lmaps/at/n;

    iget-object v2, p0, Lmaps/au/e;->m:Lmaps/ac/av;

    invoke-virtual {v1, v2, v5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v1, p0, Lmaps/au/e;->d:Lmaps/at/n;

    iget-object v2, p0, Lmaps/au/e;->n:Lmaps/ac/av;

    invoke-virtual {v1, v2, v5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v1, p0, Lmaps/au/e;->d:Lmaps/at/n;

    iget-object v2, p0, Lmaps/au/e;->o:Lmaps/ac/av;

    invoke-virtual {v1, v2, v5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v1, p0, Lmaps/au/e;->e:Lmaps/at/a;

    const/4 v2, 0x3

    invoke-virtual {v1, v7, v2}, Lmaps/at/a;->b(II)V

    iget-object v1, p0, Lmaps/au/e;->g:Lmaps/at/d;

    int-to-short v2, v0

    add-int/lit8 v3, v0, 0x1

    int-to-short v3, v3

    add-int/lit8 v0, v0, 0x2

    int-to-short v0, v0

    invoke-virtual {v1, v2, v3, v0}, Lmaps/at/d;->a(SSS)V

    const/4 v0, 0x0

    invoke-virtual {p2, v8, v0}, Lmaps/ac/g;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->m:Lmaps/ac/av;

    iget-object v4, p0, Lmaps/au/e;->n:Lmaps/ac/av;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/au/e;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;II)V

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p2, v8, v0}, Lmaps/ac/g;->a(II)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lmaps/au/e;->k:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->n:Lmaps/ac/av;

    iget-object v4, p0, Lmaps/au/e;->o:Lmaps/ac/av;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/au/e;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;II)V

    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p2, v8, v0}, Lmaps/ac/g;->a(II)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lmaps/au/e;->l:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/au/e;->j:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/au/e;->o:Lmaps/ac/av;

    iget-object v4, p0, Lmaps/au/e;->m:Lmaps/ac/av;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/au/e;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;II)V

    :cond_5
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_1

    :cond_6
    shr-int/lit8 v0, v6, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, v6, 0x8

    and-int/lit16 v1, v1, 0xff

    and-int/lit16 v2, v6, 0xff

    add-int/lit16 v0, v0, 0x2fd

    shr-int/lit8 v0, v0, 0x2

    add-int/lit16 v1, v1, 0x2fd

    shr-int/lit8 v1, v1, 0x2

    add-int/lit16 v2, v2, 0x2fd

    shr-int/lit8 v2, v2, 0x2

    const/high16 v3, -0x1000000

    and-int/2addr v3, v6

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v3

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v2

    move v7, v0

    goto/16 :goto_0
.end method

.method public static a(Lmaps/as/a;I)V
    .locals 4

    const/high16 v3, 0x10000

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/as/a;->v()V

    invoke-virtual {p0}, Lmaps/as/a;->w()V

    invoke-interface {v0, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glPolygonOffsetx(II)V

    const/16 v1, 0xb

    if-ne p1, v1, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x201

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0xc

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lmaps/as/a;->p()V

    const/16 v1, 0x203

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-interface {v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/au/e;->e:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/e;->g:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/e;->h:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->c()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lmaps/au/e;->i:Lmaps/at/e;

    check-cast v0, Lmaps/at/d;

    invoke-virtual {v0}, Lmaps/at/d;->c()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/e;->e:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/e;->g:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/e;->h:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 5

    const/4 v4, 0x4

    const/high16 v1, 0x10000

    iget-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->a()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/e;->e:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/e;->f:Lmaps/v/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/au/e;->f:Lmaps/v/o;

    invoke-virtual {v0, p1}, Lmaps/v/o;->a(Lmaps/as/a;)I

    move-result v0

    if-ne v0, v1, :cond_2

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/au/e;->f:Lmaps/v/o;

    :goto_1
    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_4

    iget-object v0, p0, Lmaps/au/e;->g:Lmaps/at/d;

    invoke-virtual {v0, p1, v4}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lmaps/au/e;->g:Lmaps/at/d;

    invoke-virtual {v2, p1, v4}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/au/e;->h:Lmaps/at/d;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    iget-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x160

    iget-object v1, p0, Lmaps/au/e;->e:Lmaps/at/a;

    invoke-virtual {v1}, Lmaps/at/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/e;->g:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/au/e;->h:Lmaps/at/d;

    invoke-virtual {v1}, Lmaps/at/d;->d()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lmaps/au/e;->i:Lmaps/at/e;

    check-cast v0, Lmaps/at/d;

    invoke-virtual {v0}, Lmaps/at/d;->d()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/e;->d:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/e;->e:Lmaps/at/a;

    invoke-virtual {v0, p1}, Lmaps/at/a;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/e;->g:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/e;->h:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    return-void
.end method
