.class public final Lmaps/i/b;
.super Ljava/lang/Object;


# direct methods
.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 12

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lmaps/m/ck;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v8

    :goto_0
    return-object v0

    :cond_0
    iget-wide v1, v7, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v3, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    neg-double v3, v3

    cmpl-double v1, v1, v3

    if-nez v1, :cond_2

    iget-wide v1, v7, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v3, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    sub-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    const-wide v3, 0x4066800000000000L    # 180.0

    cmpl-double v1, v1, v3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_3

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    const-wide/16 v2, 0x0

    iget-wide v4, v7, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v10, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    add-double/2addr v4, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v10

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    move-object v0, v1

    :goto_2
    invoke-virtual {v9, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :goto_3
    invoke-virtual {v9}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v9}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v1, v7, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v3, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    iget-wide v3, v7, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v5, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    sub-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    const-wide v5, 0x4076800000000000L    # 360.0

    sub-double/2addr v5, v3

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(DD)D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    const-wide/high16 v3, 0x4010000000000000L    # 4.0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v9}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/gms/maps/model/LatLng;

    goto :goto_3

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    invoke-static {v7}, Lmaps/i/e;->a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/i/e;

    move-result-object v5

    invoke-static {v0}, Lmaps/i/e;->a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/i/e;

    move-result-object v6

    new-instance v0, Lmaps/i/e;

    iget-wide v1, v5, Lmaps/i/e;->a:D

    iget-wide v3, v6, Lmaps/i/e;->a:D

    add-double/2addr v1, v3

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    iget-wide v3, v5, Lmaps/i/e;->b:D

    iget-wide v10, v6, Lmaps/i/e;->b:D

    add-double/2addr v3, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v10

    iget-wide v10, v5, Lmaps/i/e;->c:D

    iget-wide v5, v6, Lmaps/i/e;->c:D

    add-double/2addr v5, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v5, v10

    invoke-direct/range {v0 .. v6}, Lmaps/i/e;-><init>(DDD)V

    iget-wide v1, v0, Lmaps/i/e;->a:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-nez v1, :cond_4

    iget-wide v1, v0, Lmaps/i/e;->b:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-nez v1, :cond_4

    iget-wide v1, v0, Lmaps/i/e;->c:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/ArithmeticException;

    invoke-direct {v0}, Ljava/lang/ArithmeticException;-><init>()V

    throw v0

    :cond_4
    iget-wide v1, v0, Lmaps/i/e;->c:D

    iget-wide v3, v0, Lmaps/i/e;->a:D

    iget-wide v5, v0, Lmaps/i/e;->a:D

    mul-double/2addr v3, v5

    iget-wide v5, v0, Lmaps/i/e;->b:D

    iget-wide v10, v0, Lmaps/i/e;->b:D

    mul-double/2addr v5, v10

    add-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    iget-wide v1, v0, Lmaps/i/e;->b:D

    const-wide/16 v5, 0x0

    cmpl-double v1, v1, v5

    if-nez v1, :cond_5

    iget-wide v1, v0, Lmaps/i/e;->a:D

    const-wide/16 v5, 0x0

    cmpl-double v1, v1, v5

    if-nez v1, :cond_5

    const-wide/16 v0, 0x0

    :goto_4
    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v3, v4}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v3

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    move-object v0, v2

    goto/16 :goto_2

    :cond_5
    iget-wide v1, v0, Lmaps/i/e;->b:D

    iget-wide v5, v0, Lmaps/i/e;->a:D

    invoke-static {v1, v2, v5, v6}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    goto :goto_4

    :cond_6
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v8

    goto/16 :goto_0
.end method
