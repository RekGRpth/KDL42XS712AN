.class public final Lmaps/am/c;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ac/n;

.field private final b:Lmaps/am/b;


# direct methods
.method public constructor <init>(Lmaps/ac/n;Lmaps/am/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/am/c;->a:Lmaps/ac/n;

    iput-object p2, p0, Lmaps/am/c;->b:Lmaps/am/b;

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/n;
    .locals 1

    iget-object v0, p0, Lmaps/am/c;->a:Lmaps/ac/n;

    return-object v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lmaps/am/c;->a:Lmaps/ac/n;

    invoke-interface {v0}, Lmaps/ac/n;->f()I

    move-result v0

    return v0
.end method

.method public final c()Lmaps/am/b;
    .locals 1

    iget-object v0, p0, Lmaps/am/c;->b:Lmaps/am/b;

    return-object v0
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lmaps/am/c;->a:Lmaps/ac/n;

    invoke-interface {v0}, Lmaps/ac/n;->j()I

    move-result v0

    add-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/am/c;

    iget-object v2, p0, Lmaps/am/c;->a:Lmaps/ac/n;

    iget-object v3, p1, Lmaps/am/c;->a:Lmaps/ac/n;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lmaps/am/c;->b:Lmaps/am/b;

    iget-object v3, p1, Lmaps/am/c;->b:Lmaps/am/b;

    invoke-static {v2, v3}, Lmaps/am/b;->a(Lmaps/am/b;Lmaps/am/b;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lmaps/am/c;->a:Lmaps/ac/n;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lmaps/am/c;->b:Lmaps/am/b;

    if-eqz v1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/am/c;->b:Lmaps/am/b;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method
