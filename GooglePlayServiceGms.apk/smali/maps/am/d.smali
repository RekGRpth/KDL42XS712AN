.class public final Lmaps/am/d;
.super Ljava/lang/Object;


# static fields
.field private static a:Lmaps/am/b;


# instance fields
.field private final A:Ljava/util/List;

.field private B:Ljava/util/Iterator;

.field private final C:Ljava/util/Comparator;

.field private D:Lmaps/n/a;

.field private E:I

.field private final F:Ljava/util/Map;

.field private final b:Lmaps/aj/ag;

.field private volatile c:Lmaps/av/a;

.field private final d:Lmaps/as/c;

.field private e:Lmaps/ac/h;

.field private f:Lmaps/ar/a;

.field private final g:Lmaps/as/a;

.field private h:F

.field private i:Lmaps/ac/cy;

.field private j:Lmaps/aj/ac;

.field private k:Ljava/util/Iterator;

.field private l:Ljava/util/ArrayList;

.field private m:I

.field private n:Ljava/util/ArrayList;

.field private o:I

.field private final p:Ljava/util/Map;

.field private q:I

.field private r:F

.field private s:I

.field private t:I

.field private u:Z

.field private v:Z

.field private volatile w:Z

.field private x:Z

.field private y:Z

.field private z:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/am/b;

    invoke-direct {v0}, Lmaps/am/b;-><init>()V

    sput-object v0, Lmaps/am/d;->a:Lmaps/am/b;

    return-void
.end method

.method public constructor <init>(Lmaps/av/a;Lmaps/as/a;Landroid/content/res/Resources;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/am/d;->p:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/am/d;->A:Ljava/util/List;

    new-instance v0, Lmaps/am/e;

    invoke-direct {v0, v1}, Lmaps/am/e;-><init>(B)V

    iput-object v0, p0, Lmaps/am/d;->C:Ljava/util/Comparator;

    iput v1, p0, Lmaps/am/d;->E:I

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/am/d;->F:Ljava/util/Map;

    new-instance v0, Lmaps/aj/ag;

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lmaps/aj/ag;-><init>(F)V

    iput-object v0, p0, Lmaps/am/d;->b:Lmaps/aj/ag;

    iput-object p1, p0, Lmaps/am/d;->c:Lmaps/av/a;

    iput-object p2, p0, Lmaps/am/d;->g:Lmaps/as/a;

    new-instance v0, Lmaps/as/c;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lmaps/as/c;-><init>(I)V

    iput-object v0, p0, Lmaps/am/d;->d:Lmaps/as/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    iput v2, p0, Lmaps/am/d;->m:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    iput v2, p0, Lmaps/am/d;->o:I

    return-void
.end method

.method private static a(Lmaps/ac/ag;Lmaps/ac/bl;)I
    .locals 6

    const/4 v1, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v1

    :cond_0
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lmaps/ac/bl;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/br;->f()I

    move-result v0

    :goto_1
    move v2, v1

    :goto_2
    invoke-virtual {p0}, Lmaps/ac/ag;->b()I

    move-result v3

    if-ge v1, v3, :cond_5

    invoke-virtual {p0, v1}, Lmaps/ac/ag;->a(I)Lmaps/ac/ah;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/ah;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v4}, Lmaps/ac/ah;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v4}, Lmaps/ac/ah;->j()Lmaps/ac/bl;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bl;->e()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v4}, Lmaps/ac/ah;->j()Lmaps/ac/bl;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/br;->f()I

    move-result v3

    :goto_3
    invoke-virtual {v4}, Lmaps/ac/ah;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    mul-int/2addr v3, v5

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {v4}, Lmaps/ac/ah;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v2, v2, 0x8

    :cond_2
    invoke-virtual {v4}, Lmaps/ac/ah;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    int-to-float v2, v2

    invoke-virtual {v4}, Lmaps/ac/ah;->k()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/16 v0, 0xa

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    move v3, v0

    goto :goto_3
.end method

.method static a(Lmaps/ac/n;)I
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Lmaps/ac/n;->e()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v0

    :cond_0
    :goto_0
    return v1

    :pswitch_1
    check-cast p0, Lmaps/ac/bg;

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lmaps/ac/bg;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lmaps/ac/bg;->a(I)Lmaps/ac/ag;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/ac/bg;->d()Lmaps/ac/bl;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/am/d;->a(Lmaps/ac/ag;Lmaps/ac/bl;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_2
    check-cast p0, Lmaps/ac/aw;

    invoke-virtual {p0}, Lmaps/ac/aw;->n()Lmaps/ac/ag;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ac/aw;->d()Lmaps/ac/bl;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/am/d;->a(Lmaps/ac/ag;Lmaps/ac/bl;)I

    move-result v0

    invoke-virtual {p0}, Lmaps/ac/aw;->o()Lmaps/ac/ag;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/ac/aw;->d()Lmaps/ac/bl;

    move-result-object v2

    invoke-static {v1, v2}, Lmaps/am/d;->a(Lmaps/ac/ag;Lmaps/ac/bl;)I

    move-result v1

    add-int/2addr v1, v0

    goto :goto_0

    :pswitch_3
    check-cast p0, Lmaps/ac/aj;

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lmaps/ac/aj;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lmaps/ac/aj;->a(I)Lmaps/ac/ag;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/ac/aj;->d()Lmaps/ac/bl;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/am/d;->a(Lmaps/ac/ag;Lmaps/ac/bl;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_4
    move v1, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lmaps/ac/az;)Lmaps/ac/az;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lmaps/am/d;->e:Lmaps/ac/h;

    invoke-virtual {v0, p1, v5}, Lmaps/ac/h;->a(Lmaps/ac/az;Ljava/util/List;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-ne v6, v1, :cond_1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    goto :goto_0

    :cond_1
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->d()F

    move-result v2

    move v4, v1

    move-object v3, v0

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->d()F

    move-result v1

    cmpl-float v0, v1, v2

    if-lez v0, :cond_3

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    move v7, v1

    move-object v1, v0

    move v0, v7

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v3

    goto :goto_0

    :cond_3
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method private a(Lmaps/ac/n;Lmaps/am/b;ZZ)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lmaps/am/d;->a(Lmaps/am/b;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p1

    instance-of v2, v0, Lmaps/ac/bg;

    if-eqz v2, :cond_6

    move-object/from16 v2, p1

    check-cast v2, Lmaps/ac/bg;

    invoke-virtual {v2}, Lmaps/ac/bg;->b()Lmaps/ac/az;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/az;->a()Lmaps/ac/bd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/am/d;->i:Lmaps/ac/cy;

    invoke-virtual {v5, v4}, Lmaps/ac/cy;->b(Lmaps/ac/be;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lmaps/am/d;->a(Lmaps/ac/az;)Lmaps/ac/az;

    move-result-object v16

    if-eqz v16, :cond_0

    invoke-virtual/range {v16 .. v16}, Lmaps/ac/az;->d()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v4}, Lmaps/ar/a;->v()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v5}, Lmaps/ar/a;->k()F

    move-result v5

    mul-float/2addr v4, v5

    const/high16 v5, 0x42200000    # 40.0f

    mul-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v2}, Lmaps/ac/bg;->c()I

    move-result v4

    if-ge v3, v4, :cond_0

    const v4, 0x3f333333    # 0.7f

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lmaps/ac/az;->a(F)Lmaps/ac/av;

    move-result-object v5

    const v4, 0x3e99999a    # 0.3f

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lmaps/ac/az;->a(F)Lmaps/ac/av;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/am/d;->c:Lmaps/av/a;

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/am/d;->d:Lmaps/as/c;

    move-object/from16 v4, p2

    move/from16 v7, p3

    invoke-static/range {v2 .. v10}, Lmaps/au/ah;->a(Lmaps/ac/bg;ILmaps/am/b;Lmaps/ac/av;Lmaps/ac/av;ZLmaps/av/a;Lmaps/ar/a;Lmaps/as/c;)Lmaps/au/ah;

    move-result-object v4

    if-nez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget v4, v4, Lmaps/av/a;->c:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v5}, Lmaps/ar/a;->p()F

    move-result v5

    const/high16 v6, 0x41680000    # 14.5f

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    cmpl-float v6, v5, v6

    if-lez v6, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget v4, v4, Lmaps/av/a;->b:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lmaps/am/d;->u:Z

    if-nez v6, :cond_2

    add-float/2addr v4, v5

    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v5}, Lmaps/ar/a;->k()F

    move-result v5

    mul-float v10, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget-boolean v4, v4, Lmaps/av/a;->o:Z

    if-eqz v4, :cond_5

    const/16 v8, 0xa

    :goto_2
    invoke-virtual {v2, v3}, Lmaps/ac/bg;->a(I)Lmaps/ac/ag;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget v11, v4, Lmaps/av/a;->l:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget-object v12, v4, Lmaps/av/a;->a:Lmaps/aj/ah;

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/am/d;->b:Lmaps/aj/ag;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget-boolean v15, v4, Lmaps/av/a;->q:Z

    move-object v4, v2

    move-object/from16 v5, p2

    move-object/from16 v7, v16

    move/from16 v9, p3

    invoke-static/range {v4 .. v15}, Lmaps/au/ac;->a(Lmaps/ac/bg;Lmaps/am/b;Lmaps/ac/ag;Lmaps/ac/az;IZFFLmaps/aj/ah;Lmaps/ar/a;Lmaps/aj/ag;Z)Lmaps/au/ac;

    move-result-object v4

    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lmaps/am/d;->a(Lmaps/au/m;)Z

    move-result v5

    if-eqz v5, :cond_4

    if-eqz p4, :cond_4

    invoke-virtual {v4}, Lmaps/au/m;->n()V

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_5
    const/4 v8, 0x0

    goto :goto_2

    :cond_6
    move-object/from16 v0, p1

    instance-of v2, v0, Lmaps/ac/aj;

    if-eqz v2, :cond_8

    move-object/from16 v2, p1

    check-cast v2, Lmaps/ac/aj;

    invoke-virtual {v2}, Lmaps/ac/aj;->c()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Lmaps/ac/aj;->b()Lmaps/ac/az;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/az;->a()Lmaps/ac/bd;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/am/d;->i:Lmaps/ac/cy;

    invoke-virtual {v5, v4}, Lmaps/ac/cy;->b(Lmaps/ac/be;)Z

    move-result v4

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lmaps/am/d;->a(Lmaps/ac/az;)Lmaps/ac/az;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v2}, Lmaps/ac/aj;->d()Lmaps/ac/bl;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget v4, v4, Lmaps/av/a;->i:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget v6, v6, Lmaps/av/a;->j:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget v7, v7, Lmaps/av/a;->k:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v8}, Lmaps/ar/a;->k()F

    move-result v8

    invoke-static {v3, v4, v6, v7, v8}, Lmaps/au/m;->a(Lmaps/ac/bl;FIIF)F

    move-result v7

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lmaps/ac/aj;->a(I)Lmaps/ac/ag;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget-object v8, v3, Lmaps/av/a;->h:Lmaps/aj/ah;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget v9, v3, Lmaps/av/a;->l:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/am/d;->b:Lmaps/aj/ag;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/am/d;->c:Lmaps/av/a;

    iget-boolean v12, v3, Lmaps/av/a;->q:Z

    move-object/from16 v3, p2

    move/from16 v6, p3

    invoke-static/range {v2 .. v12}, Lmaps/au/ac;->a(Lmaps/ac/aj;Lmaps/am/b;Lmaps/ac/ag;Lmaps/ac/az;ZFLmaps/aj/ah;FLmaps/ar/a;Lmaps/aj/ag;Z)Lmaps/au/ac;

    move-result-object v2

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lmaps/am/d;->a(Lmaps/au/m;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {v2}, Lmaps/au/m;->n()V

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    move-object/from16 v0, p1

    instance-of v2, v0, Lmaps/ac/aw;

    if-eqz v2, :cond_0

    move-object/from16 v2, p1

    check-cast v2, Lmaps/ac/aw;

    invoke-virtual {v2}, Lmaps/ac/aw;->m()[Lmaps/ac/a;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/am/d;->i:Lmaps/ac/cy;

    invoke-virtual {v4, v3}, Lmaps/ac/cy;->a(Lmaps/ac/av;)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lmaps/am/d;->b(Lmaps/am/b;)Lmaps/ac/j;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-interface {v4, v3}, Lmaps/ac/j;->a(Lmaps/ac/av;)Z

    move-result v3

    :goto_4
    if-nez v3, :cond_b

    const/4 v3, 0x1

    :goto_5
    if-eqz v3, :cond_d

    invoke-virtual {v2}, Lmaps/ac/aw;->k()F

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lmaps/am/d;->h:F

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_9

    invoke-virtual {v2}, Lmaps/ac/aw;->l()F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_c

    invoke-virtual {v2}, Lmaps/ac/aw;->l()F

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lmaps/am/d;->h:F

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_c

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lmaps/am/d;->a(Lmaps/ac/aw;)Z

    move-result v3

    if-nez v3, :cond_c

    :cond_9
    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lmaps/am/d;->a(Lmaps/au/m;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {v2}, Lmaps/au/m;->n()V

    goto/16 :goto_0

    :cond_a
    const/4 v3, 0x0

    goto :goto_4

    :cond_b
    const/4 v3, 0x0

    goto :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/am/d;->f:Lmaps/ar/a;

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/am/d;->d:Lmaps/as/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/am/d;->b:Lmaps/aj/ag;

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/am/d;->c:Lmaps/av/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/am/d;->D:Lmaps/n/a;

    move-object/from16 v3, p2

    move/from16 v4, p3

    invoke-static/range {v2 .. v8}, Lmaps/au/ah;->a(Lmaps/ac/aw;Lmaps/am/b;ZLmaps/ar/a;Lmaps/as/c;Lmaps/aj/ag;Lmaps/av/a;)Lmaps/au/ah;

    move-result-object v2

    goto :goto_6

    :cond_d
    const/4 v2, 0x0

    goto :goto_6
.end method

.method private a(J)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x1

    :try_start_0
    iget-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot run labeler loop until all previous labels have either been copied into new label table or destroyed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x100

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "#:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lmaps/am/d;->E:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lmaps/am/d;->E:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " T:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " E:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " numL:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "Labeler.runLabeler"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/aw/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_0
    return v0

    :cond_0
    :goto_1
    :try_start_1
    iget-object v0, p0, Lmaps/am/d;->k:Ljava/util/Iterator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/am/d;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/am/d;->s:I

    iget v1, p0, Lmaps/am/d;->q:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lmaps/am/d;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-direct {p0, v0, v1, v4, v5}, Lmaps/am/d;->a(Lmaps/ac/n;Lmaps/am/b;ZZ)V

    goto :goto_1

    :cond_1
    move v4, v2

    :goto_2
    iget-object v0, p0, Lmaps/am/d;->B:Ljava/util/Iterator;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/am/d;->j:Lmaps/aj/ac;

    invoke-virtual {v0}, Lmaps/aj/ac;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_2
    iget v0, p0, Lmaps/am/d;->s:I

    iget v1, p0, Lmaps/am/d;->q:I

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lmaps/am/d;->j:Lmaps/aj/ac;

    invoke-virtual {v0}, Lmaps/aj/ac;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    iget v1, p0, Lmaps/am/d;->m:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    invoke-virtual {v0}, Lmaps/au/m;->d()I

    move-result v0

    iget-object v1, p0, Lmaps/am/d;->j:Lmaps/aj/ac;

    invoke-virtual {v1}, Lmaps/aj/ac;->b()Lmaps/am/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/am/c;->b()I

    move-result v1

    if-gt v0, v1, :cond_c

    :cond_3
    if-lez v4, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-ltz v0, :cond_4

    move v0, v3

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lmaps/am/d;->B:Ljava/util/Iterator;

    if-nez v0, :cond_6

    iget-object v0, p0, Lmaps/am/d;->j:Lmaps/aj/ac;

    invoke-virtual {v0}, Lmaps/aj/ac;->a()Lmaps/am/c;

    move-result-object v0

    iget-object v1, p0, Lmaps/am/d;->A:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    iget-object v1, p0, Lmaps/am/d;->j:Lmaps/aj/ac;

    invoke-virtual {v1}, Lmaps/aj/ac;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/am/d;->j:Lmaps/aj/ac;

    invoke-virtual {v1}, Lmaps/aj/ac;->b()Lmaps/am/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/am/c;->b()I

    move-result v1

    invoke-virtual {v0}, Lmaps/am/c;->b()I

    move-result v5

    if-ne v1, v5, :cond_5

    iget-object v1, p0, Lmaps/am/d;->j:Lmaps/aj/ac;

    invoke-virtual {v1}, Lmaps/aj/ac;->a()Lmaps/am/c;

    move-result-object v1

    iget-object v5, p0, Lmaps/am/d;->A:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lmaps/am/d;->A:Ljava/util/List;

    iget-object v1, p0, Lmaps/am/d;->C:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lmaps/am/d;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iput-object v0, p0, Lmaps/am/d;->B:Ljava/util/Iterator;

    :cond_6
    move v0, v4

    :goto_4
    iget-object v1, p0, Lmaps/am/d;->B:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    add-int/lit8 v4, v0, 0x1

    if-lez v0, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-ltz v0, :cond_7

    move v0, v3

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lmaps/am/d;->B:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/am/c;

    iget v1, p0, Lmaps/am/d;->s:I

    iget v5, p0, Lmaps/am/d;->q:I

    if-lt v1, v5, :cond_8

    iget-object v1, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    iget v5, p0, Lmaps/am/d;->m:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/au/m;

    invoke-virtual {v1}, Lmaps/au/m;->d()I

    move-result v1

    invoke-virtual {v0}, Lmaps/am/c;->b()I

    move-result v5

    if-ge v1, v5, :cond_a

    :cond_8
    invoke-virtual {v0}, Lmaps/am/c;->a()Lmaps/ac/n;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/am/c;->c()Lmaps/am/b;

    move-result-object v0

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, v1, v0, v5, v6}, Lmaps/am/d;->a(Lmaps/ac/n;Lmaps/am/b;ZZ)V

    iget v0, p0, Lmaps/am/d;->s:I

    iget v1, p0, Lmaps/am/d;->q:I

    if-le v0, v1, :cond_9

    iget v0, p0, Lmaps/am/d;->m:I

    invoke-direct {p0, v0}, Lmaps/am/d;->b(I)V

    :cond_9
    move v0, v4

    goto :goto_4

    :cond_a
    move v0, v4

    :cond_b
    iget-object v1, p0, Lmaps/am/d;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/am/d;->B:Ljava/util/Iterator;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move v4, v0

    goto/16 :goto_2

    :cond_c
    move v0, v2

    goto/16 :goto_0
.end method

.method private a(Lmaps/ac/aw;)Z
    .locals 2

    invoke-virtual {p1}, Lmaps/ac/aw;->b()Lmaps/ac/bt;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bt;->b()I

    move-result v0

    iget v1, p0, Lmaps/am/d;->t:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lmaps/am/d;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lmaps/am/b;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/am/d;->z:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lmaps/au/m;)Z
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_5

    iget-object v0, p0, Lmaps/am/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/au/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/am/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/au/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    if-eqz v0, :cond_4

    instance-of v1, v0, Lmaps/au/ah;

    if-eqz v1, :cond_3

    instance-of v1, p1, Lmaps/au/ah;

    if-eqz v1, :cond_3

    move-object v1, p1

    check-cast v1, Lmaps/au/ah;

    invoke-virtual {v1}, Lmaps/au/ah;->ao_()Z

    move-result v1

    if-eqz v1, :cond_3

    check-cast v0, Lmaps/au/ah;

    invoke-virtual {v0}, Lmaps/au/ah;->ao_()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v3

    :goto_0
    if-eqz v0, :cond_4

    invoke-direct {p0, v4}, Lmaps/am/d;->b(I)V

    :cond_0
    iget-object v0, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    iget-object v1, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {p1, v0, v1}, Lmaps/au/m;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    invoke-direct {p0, p1}, Lmaps/am/d;->e(Lmaps/au/m;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lmaps/am/d;->b(Lmaps/au/m;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    move v0, v3

    :goto_1
    if-eqz v0, :cond_8

    iget-object v1, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    iget-object v4, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {p1, v1, v4}, Lmaps/au/m;->a(Lmaps/ar/a;Lmaps/as/a;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-direct {p0, p1}, Lmaps/am/d;->e(Lmaps/au/m;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lmaps/am/d;->b(Lmaps/au/m;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {p1, v0}, Lmaps/au/m;->b(Lmaps/as/a;)V

    :cond_5
    :goto_2
    return v2

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {p1, v0}, Lmaps/au/m;->b(Lmaps/as/a;)V

    goto :goto_2

    :cond_9
    invoke-direct {p0, p1}, Lmaps/am/d;->c(Lmaps/au/m;)V

    move v2, v3

    goto :goto_2
.end method

.method private b(Lmaps/am/b;)Lmaps/ac/j;
    .locals 2

    iget-object v0, p0, Lmaps/am/d;->F:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez p1, :cond_2

    sget-object p1, Lmaps/am/d;->a:Lmaps/am/b;

    :cond_2
    iget-object v0, p0, Lmaps/am/d;->F:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/j;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/am/d;->F:Ljava/util/Map;

    sget-object v1, Lmaps/am/d;->a:Lmaps/am/b;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/j;

    goto :goto_0
.end method

.method private b(I)V
    .locals 4

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    invoke-direct {p0, v0}, Lmaps/am/d;->d(Lmaps/au/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lmaps/am/d;->s:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lmaps/am/d;->s:I

    :cond_0
    iget-object v1, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v0, v1}, Lmaps/au/m;->b(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/am/d;->p:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/au/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lmaps/am/d;->m:I

    if-ne p1, v0, :cond_2

    const v1, 0x7fffffff

    const/4 v0, -0x1

    iput v0, p0, Lmaps/am/d;->m:I

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmaps/au/m;->d()I

    move-result v3

    if-ge v3, v2, :cond_1

    invoke-direct {p0, v0}, Lmaps/am/d;->d(Lmaps/au/m;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lmaps/au/m;->d()I

    move-result v2

    iput v1, p0, Lmaps/am/d;->m:I

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private b(Lmaps/au/m;)Z
    .locals 12

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/au/m;->d()I

    move-result v6

    invoke-virtual {p1}, Lmaps/au/m;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Lmaps/au/m;->c()Lmaps/ac/be;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/ac/be;->a()Lmaps/ac/bd;

    move-result-object v8

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v5, v3

    :goto_1
    if-ge v5, v9, :cond_8

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/au/m;->j()Lmaps/ac/n;

    move-result-object v4

    invoke-interface {v4}, Lmaps/ac/n;->a()Lmaps/ac/o;

    move-result-object v4

    invoke-virtual {v0}, Lmaps/au/m;->j()Lmaps/ac/n;

    move-result-object v10

    invoke-interface {v10}, Lmaps/ac/n;->a()Lmaps/ac/o;

    move-result-object v10

    if-eqz v4, :cond_2

    if-eqz v10, :cond_2

    instance-of v11, v4, Lmaps/ac/p;

    if-eqz v11, :cond_2

    instance-of v11, v10, Lmaps/ac/p;

    if-eqz v11, :cond_2

    sget-object v11, Lmaps/ac/o;->a:Lmaps/ac/o;

    invoke-virtual {v11, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {v4, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v2

    :goto_2
    if-eqz v4, :cond_5

    invoke-virtual {p1}, Lmaps/au/m;->h()F

    move-result v4

    invoke-virtual {v0}, Lmaps/au/m;->h()F

    move-result v10

    cmpl-float v4, v4, v10

    if-lez v4, :cond_3

    invoke-direct {p0, v5}, Lmaps/am/d;->b(I)V

    :cond_0
    :goto_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v4, v3

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lmaps/au/m;->h()F

    move-result v4

    invoke-virtual {v0}, Lmaps/au/m;->h()F

    move-result v10

    cmpg-float v4, v4, v10

    if-gez v4, :cond_5

    :cond_4
    :goto_4
    return v2

    :cond_5
    invoke-virtual {v0}, Lmaps/au/m;->c()Lmaps/ac/be;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/be;->a()Lmaps/ac/bd;

    move-result-object v10

    invoke-virtual {v10, v8}, Lmaps/ac/bd;->a(Lmaps/ac/be;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v4, v7}, Lmaps/ac/be;->a(Lmaps/ac/be;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lmaps/au/m;->k()Z

    move-result v4

    if-eqz v4, :cond_7

    move v4, v2

    :goto_5
    if-gt v1, v4, :cond_6

    if-ne v1, v4, :cond_4

    invoke-virtual {v0}, Lmaps/au/m;->d()I

    move-result v0

    if-le v6, v0, :cond_4

    :cond_6
    invoke-direct {p0, v5}, Lmaps/am/d;->b(I)V

    goto :goto_3

    :cond_7
    move v4, v3

    goto :goto_5

    :cond_8
    move v2, v3

    goto :goto_4
.end method

.method private c(Lmaps/au/m;)V
    .locals 3

    invoke-direct {p0, p1}, Lmaps/am/d;->d(Lmaps/au/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/am/d;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/am/d;->s:I

    iget v0, p0, Lmaps/am/d;->m:I

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Lmaps/au/m;->d()I

    move-result v1

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    iget v2, p0, Lmaps/am/d;->m:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    invoke-virtual {v0}, Lmaps/au/m;->d()I

    move-result v0

    if-ge v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lmaps/am/d;->m:I

    :cond_1
    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/am/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/au/m;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private d(Lmaps/au/m;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/au/m;->aj_()F

    move-result v1

    iget v2, p0, Lmaps/am/d;->r:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lmaps/au/m;->ak_()Lmaps/am/b;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lmaps/am/b;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e(Lmaps/au/m;)Z
    .locals 2

    invoke-virtual {p1}, Lmaps/au/m;->ak_()Lmaps/am/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/am/d;->b(Lmaps/am/b;)Lmaps/ac/j;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/au/m;->c()Lmaps/ac/be;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/ac/j;->a(Lmaps/ac/be;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 5

    const/4 v2, 0x0

    iget-boolean v0, p0, Lmaps/am/d;->w:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v0, v4}, Lmaps/au/m;->b(Lmaps/as/a;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/am/d;->m:I

    iput-boolean v2, p0, Lmaps/am/d;->w:Z

    iput-boolean v2, p0, Lmaps/am/d;->x:Z

    iput-boolean v2, p0, Lmaps/am/d;->y:Z

    :cond_2
    iget-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    iput-object v1, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    iput-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    iget v0, p0, Lmaps/am/d;->o:I

    iget v1, p0, Lmaps/am/d;->m:I

    iput v1, p0, Lmaps/am/d;->o:I

    iput v0, p0, Lmaps/am/d;->m:I

    iput v2, p0, Lmaps/am/d;->s:I

    iget-object v0, p0, Lmaps/am/d;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method


# virtual methods
.method public final a(Lmaps/au/ap;Lmaps/ar/a;)Lmaps/au/m;
    .locals 1

    iget-object v0, p0, Lmaps/am/d;->b:Lmaps/aj/ag;

    invoke-static {p1, v0, p2}, Lmaps/au/ah;->a(Lmaps/au/ap;Lmaps/aj/ag;Lmaps/ar/a;)Lmaps/au/ah;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    iget-object v2, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v0, v2}, Lmaps/au/m;->b(Lmaps/as/a;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/am/d;->b:Lmaps/aj/ag;

    invoke-virtual {v0}, Lmaps/aj/ag;->a()V

    iget-object v0, p0, Lmaps/am/d;->d:Lmaps/as/c;

    invoke-virtual {v0}, Lmaps/as/c;->d()V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmaps/au/m;->a(I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Lmaps/ac/cy;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/am/d;->f()V

    invoke-virtual {p1}, Lmaps/ac/cy;->a()Lmaps/ac/cx;

    move-result-object v3

    iget-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    iget-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    if-eqz v0, :cond_0

    iget-object v5, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    iget-object v6, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v0, v5, v6}, Lmaps/au/m;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lmaps/au/m;->c()Lmaps/ac/be;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/ac/be;->a()Lmaps/ac/bd;

    move-result-object v5

    invoke-virtual {v3, v5}, Lmaps/ac/cx;->b(Lmaps/ac/be;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, v0}, Lmaps/am/d;->c(Lmaps/au/m;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v0, v5}, Lmaps/au/m;->b(Lmaps/as/a;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/am/d;->o:I

    iput-boolean v7, p0, Lmaps/am/d;->x:Z

    iput-boolean v7, p0, Lmaps/am/d;->y:Z

    iput-boolean v2, p0, Lmaps/am/d;->v:Z

    return-void
.end method

.method public final a(Lmaps/ar/a;Lmaps/ac/cy;ILmaps/aj/ac;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;Lmaps/ao/b;)V
    .locals 10

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, 0x14

    add-long v4, v1, v3

    iput-object p1, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/am/d;->k:Ljava/util/Iterator;

    iput-object p4, p0, Lmaps/am/d;->j:Lmaps/aj/ac;

    iput-object p2, p0, Lmaps/am/d;->i:Lmaps/ac/cy;

    iput p3, p0, Lmaps/am/d;->t:I

    move-object/from16 v0, p6

    iput-object v0, p0, Lmaps/am/d;->z:Ljava/util/Set;

    new-instance v1, Lmaps/ac/h;

    invoke-virtual {p2}, Lmaps/ac/cy;->c()Lmaps/ac/be;

    move-result-object v2

    invoke-direct {v1, v2}, Lmaps/ac/h;-><init>(Lmaps/ac/be;)V

    iput-object v1, p0, Lmaps/am/d;->e:Lmaps/ac/h;

    iget-object v6, p0, Lmaps/am/d;->F:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->clear()V

    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->size()I

    move-result v2

    sget-object v1, Lmaps/am/d;->a:Lmaps/am/b;

    new-instance v3, Lmaps/ac/k;

    invoke-direct {v3, v2}, Lmaps/ac/k;-><init>(I)V

    invoke-interface {v6, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/am/b;

    new-instance v7, Lmaps/ac/k;

    add-int/lit8 v8, v2, -0x1

    invoke-direct {v7, v8}, Lmaps/ac/k;-><init>(I)V

    invoke-interface {v6, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lmaps/am/b;

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lmaps/ac/j;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/am/b;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/k;

    invoke-virtual {v1, v3}, Lmaps/ac/k;->a(Lmaps/ac/j;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lmaps/ar/a;->k()F

    move-result v1

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v2

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v1, v1

    div-float v1, v2, v1

    const v2, 0x48435000    # 200000.0f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_7

    const v2, 0x48435000    # 200000.0f

    sub-float/2addr v1, v2

    const v2, 0x38d1b717    # 1.0E-4f

    mul-float/2addr v1, v2

    const/high16 v2, 0x42300000    # 44.0f

    add-float/2addr v1, v2

    :goto_2
    float-to-int v1, v1

    iget v2, p0, Lmaps/am/d;->q:I

    if-eq v1, v2, :cond_4

    iput v1, p0, Lmaps/am/d;->q:I

    iget-object v2, p0, Lmaps/am/d;->b:Lmaps/aj/ag;

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v2, v1}, Lmaps/aj/ag;->a(I)V

    :cond_4
    iget-object v1, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v1}, Lmaps/ar/a;->k()F

    move-result v1

    iget-object v2, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v2}, Lmaps/ar/a;->k()F

    move-result v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x43480000    # 200.0f

    mul-float/2addr v1, v2

    iput v1, p0, Lmaps/am/d;->r:F

    invoke-direct {p0}, Lmaps/am/d;->f()V

    iget-object v1, p0, Lmaps/am/d;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/am/d;->B:Ljava/util/Iterator;

    iget-object v1, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v1}, Lmaps/ar/a;->q()F

    move-result v1

    iput v1, p0, Lmaps/am/d;->h:F

    iget v1, p0, Lmaps/am/d;->t:I

    invoke-static {}, Lmaps/ax/m;->c()Lmaps/aj/ak;

    move-result-object v2

    iget-object v3, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    invoke-virtual {v3}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v2, v3, v0}, Lmaps/aj/ak;->a(Lmaps/ac/av;Lmaps/ao/b;)Lmaps/aj/aj;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/aj/aj;->b()I

    move-result v2

    if-ge v1, v2, :cond_8

    const/4 v1, 0x1

    :goto_3
    iput-boolean v1, p0, Lmaps/am/d;->u:Z

    const/4 v1, 0x0

    move v3, v1

    :goto_4
    if-ge v3, v6, :cond_11

    iget-object v1, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lmaps/au/m;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lmaps/au/m;->j()Lmaps/ac/n;

    move-result-object v1

    invoke-interface {v1}, Lmaps/ac/n;->a()Lmaps/ac/o;

    move-result-object v1

    invoke-interface {p5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget v1, p0, Lmaps/am/d;->h:F

    invoke-virtual {v2}, Lmaps/au/m;->h()F

    move-result v8

    cmpg-float v1, v1, v8

    if-gez v1, :cond_9

    const/4 v1, 0x0

    :goto_5
    if-nez v1, :cond_d

    :cond_5
    iget-object v1, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v2, v1}, Lmaps/au/m;->b(Lmaps/as/a;)V

    :cond_6
    :goto_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    :cond_7
    const v2, 0x3966afcd    # 2.2E-4f

    mul-float/2addr v1, v2

    goto/16 :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    :cond_9
    invoke-virtual {v2}, Lmaps/au/m;->ak_()Lmaps/am/b;

    move-result-object v1

    invoke-direct {p0, v1}, Lmaps/am/d;->a(Lmaps/am/b;)Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x0

    goto :goto_5

    :cond_a
    invoke-virtual {v2}, Lmaps/au/m;->j()Lmaps/ac/n;

    move-result-object v1

    instance-of v1, v1, Lmaps/ac/aw;

    if-eqz v1, :cond_b

    invoke-virtual {v2}, Lmaps/au/m;->j()Lmaps/ac/n;

    move-result-object v1

    check-cast v1, Lmaps/ac/aw;

    invoke-direct {p0, v1}, Lmaps/am/d;->a(Lmaps/ac/aw;)Z

    move-result v1

    if-nez v1, :cond_c

    :cond_b
    invoke-virtual {v2}, Lmaps/au/m;->al_()F

    move-result v1

    const/4 v8, 0x0

    cmpl-float v1, v1, v8

    if-lez v1, :cond_c

    iget v1, p0, Lmaps/am/d;->h:F

    invoke-virtual {v2}, Lmaps/au/m;->al_()F

    move-result v8

    cmpl-float v1, v1, v8

    if-ltz v1, :cond_c

    const/4 v1, 0x0

    goto :goto_5

    :cond_c
    const/4 v1, 0x1

    goto :goto_5

    :cond_d
    iget-object v1, p0, Lmaps/am/d;->f:Lmaps/ar/a;

    iget-object v8, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v2, v1, v8}, Lmaps/au/m;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v1, p0, Lmaps/am/d;->c:Lmaps/av/a;

    iget-boolean v1, v1, Lmaps/av/a;->r:Z

    if-eqz v1, :cond_e

    iget-object v1, p0, Lmaps/am/d;->i:Lmaps/ac/cy;

    invoke-virtual {v2}, Lmaps/au/m;->c()Lmaps/ac/be;

    move-result-object v8

    invoke-virtual {v8}, Lmaps/ac/be;->a()Lmaps/ac/bd;

    move-result-object v8

    invoke-virtual {v1, v8}, Lmaps/ac/cy;->b(Lmaps/ac/be;)Z

    move-result v1

    :goto_7
    if-eqz v1, :cond_10

    invoke-direct {p0, v2}, Lmaps/am/d;->e(Lmaps/au/m;)Z

    move-result v1

    if-nez v1, :cond_10

    invoke-direct {p0, v2}, Lmaps/am/d;->b(Lmaps/au/m;)Z

    move-result v1

    if-nez v1, :cond_f

    invoke-direct {p0, v2}, Lmaps/am/d;->c(Lmaps/au/m;)V

    goto :goto_6

    :cond_e
    iget-object v1, p0, Lmaps/am/d;->i:Lmaps/ac/cy;

    invoke-virtual {v2, v1}, Lmaps/au/m;->a(Lmaps/ac/cy;)Z

    move-result v1

    goto :goto_7

    :cond_f
    iget-object v1, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v2, v1}, Lmaps/au/m;->b(Lmaps/as/a;)V

    goto :goto_6

    :cond_10
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v2, v1}, Lmaps/au/m;->b(Lmaps/as/a;)V

    goto/16 :goto_6

    :cond_11
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_8
    if-ge v2, v3, :cond_12

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/au/m;

    invoke-virtual {v1}, Lmaps/au/m;->j()Lmaps/ac/n;

    move-result-object v6

    invoke-virtual {v1}, Lmaps/au/m;->ak_()Lmaps/am/b;

    move-result-object v8

    invoke-virtual {v1}, Lmaps/au/m;->k()Z

    move-result v1

    const/4 v9, 0x0

    invoke-direct {p0, v6, v8, v1, v9}, Lmaps/am/d;->a(Lmaps/ac/n;Lmaps/am/b;ZZ)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_8

    :cond_12
    iget-object v1, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    const/4 v1, -0x1

    iput v1, p0, Lmaps/am/d;->o:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/am/d;->x:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/am/d;->y:Z

    invoke-direct {p0, v4, v5}, Lmaps/am/d;->a(J)Z

    move-result v1

    iput-boolean v1, p0, Lmaps/am/d;->v:Z

    return-void
.end method

.method public final a(Lmaps/av/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/am/d;->c:Lmaps/av/a;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lmaps/am/d;->c:Lmaps/av/a;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/am/d;->w:Z

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/am/d;->b:Lmaps/aj/ag;

    invoke-virtual {v0}, Lmaps/aj/ag;->a()V

    iget-object v0, p0, Lmaps/am/d;->d:Lmaps/as/c;

    invoke-virtual {v0}, Lmaps/as/c;->d()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/am/d;->b:Lmaps/aj/ag;

    invoke-virtual {v0}, Lmaps/aj/ag;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/am/d;->w:Z

    return-void
.end method

.method public final c()V
    .locals 8

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v3, 0x14

    add-long/2addr v3, v0

    iget-boolean v0, p0, Lmaps/am/d;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/am/d;->y:Z

    if-eqz v0, :cond_5

    :cond_0
    iget-boolean v5, p0, Lmaps/am/d;->y:Z

    invoke-direct {p0}, Lmaps/am/d;->f()V

    iget-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_4

    iget-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    if-eqz v0, :cond_2

    if-eqz v5, :cond_1

    iget-object v7, p0, Lmaps/am/d;->i:Lmaps/ac/cy;

    invoke-virtual {v0, v7}, Lmaps/au/m;->a(Lmaps/ac/cy;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-direct {p0, v0}, Lmaps/am/d;->e(Lmaps/au/m;)Z

    move-result v7

    if-nez v7, :cond_3

    :cond_1
    invoke-direct {p0, v0}, Lmaps/am/d;->b(Lmaps/au/m;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-direct {p0, v0}, Lmaps/am/d;->c(Lmaps/au/m;)V

    :cond_2
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lmaps/am/d;->g:Lmaps/as/a;

    invoke-virtual {v0, v7}, Lmaps/au/m;->b(Lmaps/as/a;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lmaps/am/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/am/d;->o:I

    iput-boolean v2, p0, Lmaps/am/d;->x:Z

    iput-boolean v2, p0, Lmaps/am/d;->y:Z

    :cond_5
    invoke-direct {p0, v3, v4}, Lmaps/am/d;->a(J)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/am/d;->v:Z

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/am/d;->v:Z

    return v0
.end method

.method public final e()Lmaps/am/f;
    .locals 2

    new-instance v0, Lmaps/am/f;

    iget-object v1, p0, Lmaps/am/d;->l:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lmaps/am/f;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method
