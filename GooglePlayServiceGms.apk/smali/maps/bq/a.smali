.class public final Lmaps/bq/a;
.super Landroid/widget/ListView;

# interfaces
.implements Lmaps/ab/s;


# instance fields
.field private a:I

.field private b:Lmaps/ac/z;

.field private c:Lmaps/ab/q;

.field private d:I

.field private e:Lmaps/bq/g;

.field private final f:Ljava/util/Set;

.field private final g:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmaps/bq/a;-><init>(Landroid/content/Context;Landroid/content/res/Resources;B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;B)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lmaps/bq/a;->a:I

    iput v1, p0, Lmaps/bq/a;->d:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/bq/a;->f:Ljava/util/Set;

    iput-object p2, p0, Lmaps/bq/a;->g:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic a(Lmaps/bq/a;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lmaps/bq/a;->g:Landroid/content/res/Resources;

    return-object v0
.end method

.method private a()V
    .locals 2

    iget v0, p0, Lmaps/bq/a;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lmaps/bq/a;->a:I

    invoke-virtual {p0, v0}, Lmaps/bq/a;->smoothScrollToPosition(I)V

    :cond_0
    return-void
.end method

.method private a(Lmaps/ac/z;Lmaps/ac/ad;)V
    .locals 9

    const-wide/16 v7, 0x1f4

    const/4 v1, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v0, -0x1

    iget-object v2, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    invoke-static {p1, v2}, Lmaps/bq/a;->b(Lmaps/ac/z;Lmaps/ac/z;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lmaps/bq/a;->clearAnimation()V

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    iput v0, p0, Lmaps/bq/a;->d:I

    iput v0, p0, Lmaps/bq/a;->a:I

    if-eqz p1, :cond_0

    invoke-static {p1}, Lmaps/bq/a;->a(Lmaps/ac/z;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object p1, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    invoke-virtual {p0, v1}, Lmaps/bq/a;->setVisibility(I)V

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    invoke-virtual {v2, v7, v8}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v3, Lmaps/bq/b;

    invoke-direct {v3, p0}, Lmaps/bq/b;-><init>(Lmaps/bq/a;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v2}, Lmaps/bq/a;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v2, Lmaps/bq/g;

    invoke-virtual {p0}, Lmaps/bq/a;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    invoke-direct {v2, p0, v3, v4}, Lmaps/bq/g;-><init>(Lmaps/bq/a;Landroid/content/Context;Lmaps/ac/z;)V

    iput-object v2, p0, Lmaps/bq/a;->e:Lmaps/bq/g;

    iget-object v2, p0, Lmaps/bq/a;->e:Lmaps/bq/g;

    invoke-virtual {p0, v2}, Lmaps/bq/a;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    if-eqz v2, :cond_0

    iget v2, p0, Lmaps/bq/a;->d:I

    if-eq v0, v2, :cond_0

    iput v0, p0, Lmaps/bq/a;->d:I

    iget-object v2, p0, Lmaps/bq/a;->e:Lmaps/bq/g;

    invoke-virtual {v2}, Lmaps/bq/g;->notifyDataSetChanged()V

    :cond_0
    iget-object v2, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lmaps/bq/a;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lmaps/bq/a;->setVisibility(I)V

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v6, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v2, v7, v8}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v3, Lmaps/bq/c;

    invoke-direct {v3, p0}, Lmaps/bq/c;-><init>(Lmaps/bq/a;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v2}, Lmaps/bq/a;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    iget-object v2, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    if-nez v2, :cond_2

    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    if-nez v2, :cond_4

    :cond_3
    :goto_1
    invoke-virtual {p0, v0}, Lmaps/bq/a;->a(I)V

    invoke-direct {p0}, Lmaps/bq/a;->a()V

    goto :goto_0

    :cond_4
    if-nez p2, :cond_6

    invoke-virtual {v2}, Lmaps/ac/z;->d()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_5
    :goto_2
    if-ltz v1, :cond_3

    move v0, v1

    goto :goto_1

    :cond_6
    invoke-virtual {v2, p2}, Lmaps/ac/z;->b(Lmaps/ac/ad;)I

    move-result v1

    if-ltz v1, :cond_5

    invoke-virtual {v2}, Lmaps/ac/z;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    move v1, v0

    goto :goto_2
.end method

.method static synthetic a(Lmaps/bq/a;Lmaps/ac/z;Lmaps/ac/ad;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/bq/a;->a(Lmaps/ac/z;Lmaps/ac/ad;)V

    return-void
.end method

.method private static a(Lmaps/ac/z;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_1

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lmaps/ac/z;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lmaps/ac/z;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v3, v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_1
.end method

.method static synthetic a(Lmaps/ac/z;Lmaps/ac/z;)Z
    .locals 1

    invoke-static {p0, p1}, Lmaps/bq/a;->b(Lmaps/ac/z;Lmaps/ac/z;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lmaps/bq/a;)I
    .locals 1

    iget v0, p0, Lmaps/bq/a;->a:I

    return v0
.end method

.method private static b(Lmaps/ac/z;Lmaps/ac/z;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lmaps/bq/a;)I
    .locals 1

    iget v0, p0, Lmaps/bq/a;->d:I

    return v0
.end method

.method static synthetic d(Lmaps/bq/a;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/bq/a;->f:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lmaps/bq/a;)V
    .locals 0

    invoke-direct {p0}, Lmaps/bq/a;->a()V

    return-void
.end method

.method static synthetic f(Lmaps/bq/a;)Lmaps/ac/z;
    .locals 1

    iget-object v0, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 10

    iget v0, p0, Lmaps/bq/a;->a:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lmaps/bq/a;->a:I

    iget-object v0, p0, Lmaps/bq/a;->e:Lmaps/bq/g;

    invoke-virtual {v0}, Lmaps/bq/g;->notifyDataSetChanged()V

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/bq/a;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bq/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/bq/h;->a()Lmaps/ac/aa;

    move-result-object v2

    iget-object v3, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    if-nez v2, :cond_2

    const-string v0, "none"

    move-object v1, v0

    :goto_1
    if-nez v3, :cond_3

    const-string v0, "none"

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "?sa=T&oi=m_map:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lmaps/br/f;->a:Lmaps/br/f;

    invoke-virtual {v4}, Lmaps/br/f;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x68

    const-string v5, "s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "l="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v7

    const/4 v1, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "b="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    const/4 v1, 0x2

    if-nez v3, :cond_4

    const/4 v0, 0x0

    :goto_3
    aput-object v0, v6, v1

    invoke-static {v6}, Lmaps/br/g;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/bq/a;->c:Lmaps/ab/q;

    if-eqz v0, :cond_0

    if-nez v2, :cond_5

    iget-object v0, p0, Lmaps/bq/a;->c:Lmaps/ab/q;

    iget-object v1, p0, Lmaps/bq/a;->b:Lmaps/ac/z;

    invoke-virtual {v0, v1}, Lmaps/ab/q;->a(Lmaps/ac/z;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lmaps/ac/aa;->b()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/r;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/r;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "u="

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lmaps/bq/a;->c:Lmaps/ab/q;

    invoke-virtual {v2}, Lmaps/ac/aa;->a()Lmaps/ac/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ab/q;->a(Lmaps/ac/ad;)V

    goto/16 :goto_0
.end method

.method public final a(Lmaps/ab/q;)V
    .locals 1

    new-instance v0, Lmaps/bq/d;

    invoke-direct {v0, p0, p1}, Lmaps/bq/d;-><init>(Lmaps/bq/a;Lmaps/ab/q;)V

    invoke-virtual {p0, v0}, Lmaps/bq/a;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lmaps/ab/q;Lmaps/ac/z;)V
    .locals 1

    new-instance v0, Lmaps/bq/e;

    invoke-direct {v0, p0, p1, p2}, Lmaps/bq/e;-><init>(Lmaps/bq/a;Lmaps/ab/q;Lmaps/ac/z;)V

    invoke-virtual {p0, v0}, Lmaps/bq/a;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Lmaps/ab/q;)V
    .locals 1

    iget-object v0, p0, Lmaps/bq/a;->c:Lmaps/ab/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/a;->c:Lmaps/ab/q;

    invoke-virtual {v0, p0}, Lmaps/ab/q;->b(Lmaps/ab/s;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lmaps/bq/a;->a(Lmaps/ab/q;)V

    invoke-virtual {p1, p0}, Lmaps/ab/q;->a(Lmaps/ab/s;)V

    :cond_1
    iput-object p1, p0, Lmaps/bq/a;->c:Lmaps/ab/q;

    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method final c(Lmaps/ab/q;)V
    .locals 12

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/ab/q;->c()Lmaps/ac/z;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmaps/ab/q;->b(Lmaps/ac/r;)Lmaps/ac/ae;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lmaps/ac/ae;->c()Lmaps/ac/ad;

    move-result-object v0

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "?sa=T&oi=m_map:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lmaps/br/f;->a:Lmaps/br/f;

    invoke-virtual {v3}, Lmaps/br/f;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    if-nez v0, :cond_0

    const-string v2, "0"

    :goto_1
    invoke-static {v4}, Lmaps/bq/a;->a(Lmaps/ac/z;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "1"

    :goto_2
    const/16 v6, 0x68

    const-string v7, "f"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "b="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v11

    invoke-virtual {v11}, Lmaps/ac/r;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "p="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v9

    const/4 v3, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "v="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v3

    const/4 v2, 0x3

    if-nez v5, :cond_2

    :goto_3
    aput-object v1, v8, v2

    invoke-static {v8}, Lmaps/br/g;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v7, v1}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_4
    invoke-direct {p0, v4, v0}, Lmaps/bq/a;->a(Lmaps/ac/z;Lmaps/ac/ad;)V

    return-void

    :cond_0
    const-string v2, "1"

    goto :goto_1

    :cond_1
    const-string v3, "0"

    goto :goto_2

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "u="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_4
.end method

.method protected final onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    new-instance v0, Lmaps/bq/f;

    invoke-direct {v0, p0}, Lmaps/bq/f;-><init>(Lmaps/bq/a;)V

    invoke-virtual {p0, v0}, Lmaps/bq/a;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
