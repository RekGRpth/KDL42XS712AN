.class public final Lmaps/ba/f;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmaps/ba/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/ba/f;

    invoke-direct {v0}, Lmaps/ba/f;-><init>()V

    sput-object v0, Lmaps/ba/f;->a:Lmaps/ba/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lmaps/ba/f;
    .locals 1

    sget-object v0, Lmaps/ba/f;->a:Lmaps/ba/f;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)Z
    .locals 1

    invoke-static {p0}, Lmaps/bd/a;->a(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method public static b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static c()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lmaps/bs/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/api"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d()Z
    .locals 1

    invoke-static {}, Lmaps/bb/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmaps/bd/a;->b(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public static e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static f()Z
    .locals 1

    invoke-static {}, Lmaps/be/g;->a()Lmaps/be/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/be/c;->a()Z

    move-result v0

    return v0
.end method

.method public static g()Z
    .locals 1

    invoke-static {}, Lmaps/ax/m;->a()Lmaps/ax/k;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ax/m;->a()Lmaps/ax/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ax/k;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h()V
    .locals 0

    return-void
.end method

.method public static i()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
