.class public final Lmaps/c/c;
.super Ljava/lang/Object;


# static fields
.field private static a:J

.field private static final b:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final c:Lmaps/ab/q;

.field private final d:J

.field private final e:Lmaps/ab/s;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Lmaps/h/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/16 v0, -0x1

    sput-wide v0, Lmaps/c/c;->a:J

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lmaps/c/c;->b:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method private constructor <init>(Lmaps/ab/q;Ljava/util/concurrent/Executor;Lmaps/h/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lmaps/c/c;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/c/c;->d:J

    new-instance v0, Lmaps/c/d;

    invoke-direct {v0, p0}, Lmaps/c/d;-><init>(Lmaps/c/c;)V

    iput-object v0, p0, Lmaps/c/c;->e:Lmaps/ab/s;

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/q;

    iput-object v0, p0, Lmaps/c/c;->c:Lmaps/ab/q;

    invoke-static {p2}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lmaps/c/c;->f:Ljava/util/concurrent/Executor;

    invoke-static {p3}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/h/a;

    iput-object v0, p0, Lmaps/c/c;->g:Lmaps/h/a;

    return-void
.end method

.method public static a(Lmaps/ab/q;Ljava/util/concurrent/Executor;Lmaps/h/a;)Lmaps/c/c;
    .locals 3

    new-instance v0, Lmaps/c/c;

    invoke-direct {v0, p0, p1, p2}, Lmaps/c/c;-><init>(Lmaps/ab/q;Ljava/util/concurrent/Executor;Lmaps/h/a;)V

    iget-object v1, v0, Lmaps/c/c;->c:Lmaps/ab/q;

    iget-object v2, v0, Lmaps/c/c;->e:Lmaps/ab/s;

    invoke-virtual {v1, v2}, Lmaps/ab/q;->a(Lmaps/ab/s;)V

    return-object v0
.end method

.method static synthetic f()Lmaps/d/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    sget-wide v0, Lmaps/c/c;->a:J

    iget-wide v2, p0, Lmaps/c/c;->d:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    sget-wide v0, Lmaps/c/c;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    iget-wide v0, p0, Lmaps/c/c;->d:J

    sput-wide v0, Lmaps/c/c;->a:J

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    sget-wide v0, Lmaps/c/c;->a:J

    iget-wide v2, p0, Lmaps/c/c;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    sput-wide v0, Lmaps/c/c;->a:J

    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/c/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/c/c;->c:Lmaps/ab/q;

    invoke-virtual {v0}, Lmaps/ab/q;->d()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 4

    iget-wide v0, p0, Lmaps/c/c;->d:J

    sget-wide v2, Lmaps/c/c;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lmaps/ab/q;
    .locals 1

    iget-object v0, p0, Lmaps/c/c;->c:Lmaps/ab/q;

    return-object v0
.end method
