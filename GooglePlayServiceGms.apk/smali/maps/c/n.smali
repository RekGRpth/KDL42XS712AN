.class public final Lmaps/c/n;
.super Lmaps/ay/bg;

# interfaces
.implements Lmaps/c/m;


# instance fields
.field private l:Leui;

.field private m:Leuo;

.field private n:Lmaps/ay/bh;

.field private o:Lmaps/i/c;

.field private p:Lmaps/e/bj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lmaps/ay/bg;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    new-instance v0, Lmaps/c/o;

    invoke-direct {v0, p0}, Lmaps/c/o;-><init>(Lmaps/c/n;)V

    iput-object v0, p0, Lmaps/c/n;->n:Lmaps/ay/bh;

    new-instance v0, Lmaps/i/c;

    new-instance v1, Lmaps/c/p;

    invoke-direct {v1, p0}, Lmaps/c/p;-><init>(Lmaps/c/n;)V

    invoke-direct {v0, v1}, Lmaps/i/c;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lmaps/c/n;->o:Lmaps/i/c;

    return-void
.end method

.method static synthetic a(Lmaps/c/n;)Leuo;
    .locals 1

    iget-object v0, p0, Lmaps/c/n;->m:Leuo;

    return-object v0
.end method

.method static synthetic b(Lmaps/c/n;)Leui;
    .locals 1

    iget-object v0, p0, Lmaps/c/n;->l:Leui;

    return-object v0
.end method

.method static synthetic c(Lmaps/c/n;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lmaps/ay/bg;->a(ZZ)V

    return-void
.end method

.method private w()V
    .locals 1

    iget-object v0, p0, Lmaps/c/n;->l:Leui;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/c/n;->m:Leuo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/c/n;->n:Lmaps/ay/bh;

    iput-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    goto :goto_0
.end method


# virtual methods
.method public final a(Leui;)V
    .locals 0

    iput-object p1, p0, Lmaps/c/n;->l:Leui;

    invoke-direct {p0}, Lmaps/c/n;->w()V

    return-void
.end method

.method public final a(Leuo;)V
    .locals 0

    iput-object p1, p0, Lmaps/c/n;->m:Leuo;

    invoke-direct {p0}, Lmaps/c/n;->w()V

    return-void
.end method

.method public final a(Lmaps/e/bj;)V
    .locals 1

    iput-object p1, p0, Lmaps/c/n;->p:Lmaps/e/bj;

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->a(Lmaps/ap/e;)V

    :cond_0
    iput-object p1, p0, Lmaps/ay/bg;->k:Lmaps/ap/e;

    return-void
.end method

.method public final canScrollHorizontally(I)Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0}, Lmaps/ay/bc;->a()Z

    move-result v0

    return v0
.end method

.method public final canScrollVertically(I)Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0}, Lmaps/ay/bc;->a()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lmaps/c/n;->p:Lmaps/e/bj;

    invoke-virtual {v0}, Lmaps/e/bj;->e()V

    iget-object v0, p0, Lmaps/c/n;->o:Lmaps/i/c;

    invoke-virtual {v0}, Lmaps/i/c;->a()V

    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Lmaps/ay/bg;->onSizeChanged(IIII)V

    return-void
.end method
