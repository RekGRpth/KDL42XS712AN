.class public final Lmaps/c/l;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Lmaps/c/m;

.field private final j:Lmaps/ay/au;


# direct methods
.method public constructor <init>(Lmaps/ay/au;Lmaps/c/m;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/c/l;->a:I

    iput v0, p0, Lmaps/c/l;->b:I

    iput v0, p0, Lmaps/c/l;->c:I

    iput v0, p0, Lmaps/c/l;->d:I

    iput v0, p0, Lmaps/c/l;->e:I

    iput v0, p0, Lmaps/c/l;->f:I

    iput v0, p0, Lmaps/c/l;->g:I

    iput v0, p0, Lmaps/c/l;->h:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/c/l;->i:Lmaps/c/m;

    iput-object p1, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    iput-object p2, p0, Lmaps/c/l;->i:Lmaps/c/m;

    return-void
.end method

.method private a(Lmaps/ar/c;I)V
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lmaps/c/l;->a(Lmaps/ar/c;II)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lmaps/c/l;->i:Lmaps/c/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/c/l;->i:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->getWidth()I

    move-result v0

    iget v1, p0, Lmaps/c/l;->a:I

    sub-int/2addr v0, v1

    iget v1, p0, Lmaps/c/l;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, Lmaps/c/l;->e:I

    iget-object v0, p0, Lmaps/c/l;->i:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->getHeight()I

    move-result v0

    iget v1, p0, Lmaps/c/l;->b:I

    sub-int/2addr v0, v1

    iget v1, p0, Lmaps/c/l;->d:I

    sub-int/2addr v0, v1

    iput v0, p0, Lmaps/c/l;->f:I

    iget v0, p0, Lmaps/c/l;->a:I

    iget v1, p0, Lmaps/c/l;->e:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/c/l;->g:I

    iget v0, p0, Lmaps/c/l;->b:I

    iget v1, p0, Lmaps/c/l;->f:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/c/l;->h:I

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(FFFI)F
    .locals 1

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/ay/au;->a(FFFI)F

    move-result v0

    return v0
.end method

.method public final a(FI)F
    .locals 2

    invoke-direct {p0}, Lmaps/c/l;->h()V

    iget v0, p0, Lmaps/c/l;->g:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/c/l;->h:I

    int-to-float v1, v1

    invoke-virtual {p0, p1, v0, v1, p2}, Lmaps/c/l;->a(FFFI)F

    move-result v0

    return v0
.end method

.method public final a()I
    .locals 2

    iget v0, p0, Lmaps/c/l;->a:I

    iget v1, p0, Lmaps/c/l;->c:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(Lmaps/ar/b;F)Lmaps/ar/b;
    .locals 8

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-direct {p0}, Lmaps/c/l;->h()V

    iget-object v0, p0, Lmaps/c/l;->i:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->m()Lmaps/ar/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ar/a;->p()F

    move-result v1

    invoke-virtual {p1}, Lmaps/ar/b;->b()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v6, v7, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, p2

    iget-object v2, p0, Lmaps/c/l;->i:Lmaps/c/m;

    invoke-interface {v2}, Lmaps/c/m;->getWidth()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v6

    iget v4, p0, Lmaps/c/l;->g:I

    int-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v2, v1

    iget-object v3, p0, Lmaps/c/l;->i:Lmaps/c/m;

    invoke-interface {v3}, Lmaps/c/m;->getHeight()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v3, v6

    iget v5, p0, Lmaps/c/l;->h:I

    int-to-double v5, v5

    sub-double/2addr v3, v5

    double-to-float v3, v3

    mul-float/2addr v1, v3

    invoke-virtual {v0}, Lmaps/ar/a;->v()F

    move-result v3

    mul-float/2addr v2, v3

    neg-float v1, v1

    invoke-virtual {v0}, Lmaps/ar/a;->v()F

    move-result v3

    mul-float/2addr v1, v3

    invoke-virtual {v0}, Lmaps/ar/a;->o()F

    move-result v3

    const v4, 0x3c8efa35

    mul-float/2addr v3, v4

    invoke-static {v3}, Landroid/util/FloatMath;->cos(F)F

    move-result v3

    div-float/2addr v1, v3

    invoke-virtual {v0}, Lmaps/ar/a;->s()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {v0}, Lmaps/ar/a;->t()Lmaps/ac/av;

    move-result-object v0

    new-instance v4, Lmaps/ac/av;

    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v5

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v3

    invoke-direct {v4, v5, v3}, Lmaps/ac/av;-><init>(II)V

    new-instance v3, Lmaps/ac/av;

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v5

    invoke-virtual {v0}, Lmaps/ac/av;->g()I

    move-result v0

    invoke-direct {v3, v5, v0}, Lmaps/ac/av;-><init>(II)V

    invoke-static {v4, v2, v4}, Lmaps/ac/av;->b(Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-static {v3, v1, v3}, Lmaps/ac/av;->b(Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-virtual {p1}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/ar/b;->b()F

    move-result v2

    invoke-virtual {v0}, Lmaps/ac/av;->h()I

    move-result v5

    invoke-virtual {v0, v4}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v1

    invoke-static {v1, v3, v1}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-virtual {v1, v5}, Lmaps/ac/av;->b(I)V

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {p1}, Lmaps/ar/b;->d()F

    move-result v3

    invoke-virtual {p1}, Lmaps/ar/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    return-object v0
.end method

.method public final a(IIII)V
    .locals 0

    iput p1, p0, Lmaps/c/l;->a:I

    iput p2, p0, Lmaps/c/l;->b:I

    iput p3, p0, Lmaps/c/l;->c:I

    iput p4, p0, Lmaps/c/l;->d:I

    return-void
.end method

.method public final a(Lmaps/ac/av;)V
    .locals 6

    iget-object v0, p0, Lmaps/c/l;->i:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->m()Lmaps/ar/a;

    move-result-object v1

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {v1}, Lmaps/ar/a;->p()F

    move-result v2

    invoke-virtual {v1}, Lmaps/ar/a;->o()F

    move-result v3

    invoke-virtual {v1}, Lmaps/ar/a;->n()F

    move-result v4

    const/4 v5, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    const/16 v1, 0x12c

    invoke-direct {p0, v0, v1}, Lmaps/c/l;->a(Lmaps/ar/c;I)V

    return-void
.end method

.method public final a(Lmaps/ap/d;)V
    .locals 1

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0, p1}, Lmaps/ay/au;->a(Lmaps/ap/d;)V

    return-void
.end method

.method public final a(Lmaps/ar/c;II)V
    .locals 2

    iget-object v0, p0, Lmaps/c/l;->i:Lmaps/c/m;

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/c/l;->a:I

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/c/l;->c:I

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/c/l;->b:I

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/c/l;->d:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    invoke-interface {p1}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1}, Lmaps/c/l;->a(Lmaps/ar/b;F)Lmaps/ar/b;

    move-result-object v0

    iget-object v1, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v1, v0, p2, p3}, Lmaps/ay/au;->a(Lmaps/ar/c;II)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/au;->a(Lmaps/ar/c;II)V

    goto :goto_1
.end method

.method public final a(Lmaps/ay/az;)V
    .locals 1

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0, p1}, Lmaps/ay/au;->a(Lmaps/ay/az;)V

    return-void
.end method

.method public final b(Lmaps/ac/av;)F
    .locals 1

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0, p1}, Lmaps/ay/au;->a(Lmaps/ac/av;)F

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 2

    iget v0, p0, Lmaps/c/l;->b:I

    iget v1, p0, Lmaps/c/l;->d:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(FI)V
    .locals 6

    invoke-direct {p0}, Lmaps/c/l;->h()V

    invoke-virtual {p0}, Lmaps/c/l;->c()Lmaps/ar/b;

    move-result-object v2

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {v2}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v2}, Lmaps/ar/b;->d()F

    move-result v3

    invoke-virtual {v2}, Lmaps/ar/b;->e()F

    move-result v4

    invoke-virtual {v2}, Lmaps/ar/b;->f()F

    move-result v5

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    invoke-direct {p0, v0, p2}, Lmaps/c/l;->a(Lmaps/ar/c;I)V

    return-void
.end method

.method public final c()Lmaps/ar/b;
    .locals 2

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v0

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0, v0, v1}, Lmaps/c/l;->a(Lmaps/ar/b;F)Lmaps/ar/b;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->b()V

    return-void
.end method

.method public final e()F
    .locals 1

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->a()F

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0, v1, v1}, Lmaps/ay/au;->a(FF)V

    return-void
.end method

.method public final g()F
    .locals 1

    iget-object v0, p0, Lmaps/c/l;->j:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->e()F

    move-result v0

    return v0
.end method
