.class public final Lmaps/bf/a;
.super Lmaps/bs/a;


# static fields
.field public static final a:Ljava/lang/String;

.field private static f:Ljava/lang/String;

.field private static g:Ljava/lang/String;

.field private static h:[Ljava/lang/String;

.field private static volatile i:Ljava/lang/Boolean;

.field private static o:Ljava/lang/Thread;


# instance fields
.field private j:Landroid/content/Context;

.field private k:I

.field private l:F

.field private final m:F

.field private final n:F

.field private volatile p:Z

.field private q:Lmaps/bf/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lmaps/bs/c;->c:Ljava/lang/String;

    sput-object v0, Lmaps/bf/a;->a:Ljava/lang/String;

    sget-object v0, Lmaps/bs/c;->a:Ljava/lang/String;

    sput-object v0, Lmaps/bf/a;->g:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lmaps/bs/a;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bf/a;->p:Z

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not in a unit or feature test"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lmaps/bh/a;

    invoke-direct {v0}, Lmaps/bh/a;-><init>()V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/bf/a;-><init>(Landroid/content/Context;B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 11

    const/4 v3, 0x1

    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lmaps/bs/a;-><init>(Landroid/content/Context;)V

    iput-boolean v2, p0, Lmaps/bf/a;->p:Z

    iput-object p1, p0, Lmaps/bf/a;->j:Landroid/content/Context;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Lmaps/bf/a;->o:Ljava/lang/Thread;

    iget-object v0, p0, Lmaps/bf/a;->q:Lmaps/bf/b;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/bf/b;

    invoke-direct {v0}, Lmaps/bf/b;-><init>()V

    iput-object v0, p0, Lmaps/bf/a;->q:Lmaps/bf/b;

    :cond_0
    const-string v0, "maps_client_id"

    invoke-direct {p0, v0}, Lmaps/bf/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lmaps/bm/a;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v0, "Web"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/bf/a;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "unknown"

    sput-object v0, Lmaps/bf/a;->f:Ljava/lang/String;

    :cond_1
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, p0, Lmaps/bf/a;->k:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lmaps/bf/a;->l:F

    :goto_1
    iget v0, p0, Lmaps/bf/a;->k:I

    int-to-float v0, v0

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v4, v1, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    div-float/2addr v4, v0

    float-to-double v4, v4

    cmpl-double v4, v4, v6

    if-gtz v4, :cond_2

    iget v4, v1, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    div-float/2addr v4, v0

    float-to-double v4, v4

    cmpl-double v4, v4, v6

    if-lez v4, :cond_7

    :cond_2
    iput v0, p0, Lmaps/bf/a;->m:F

    iput v0, p0, Lmaps/bf/a;->n:F

    :goto_2
    new-instance v0, Lmaps/bh/b;

    invoke-direct {v0}, Lmaps/bh/b;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lmaps/bf/a;->q:Lmaps/bf/b;

    invoke-virtual {v0, v5}, Lmaps/bf/b;->b(Ljava/lang/String;)V

    iget-object v6, p0, Lmaps/bf/a;->q:Lmaps/bf/b;

    sget-object v0, Lmaps/bf/a;->h:[Ljava/lang/String;

    if-nez v0, :cond_d

    sget-object v0, Lmaps/bf/a;->g:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lmaps/bm/a;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v1, v4, :cond_9

    move v1, v3

    :goto_3
    if-nez v1, :cond_c

    new-instance v7, Ljava/util/ArrayList;

    array-length v1, v0

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v8, v0

    move v4, v2

    :goto_4
    if-ge v4, v8, :cond_b

    aget-object v9, v0, v4

    invoke-static {v9}, Lmaps/bf/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmaps/bf/b;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v10, "ar"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "fa"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "iw"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_3
    move v1, v3

    :goto_5
    if-nez v1, :cond_4

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_4

    :cond_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_6
    const/16 v0, 0xa0

    iput v0, p0, Lmaps/bf/a;->k:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmaps/bf/a;->l:F

    goto/16 :goto_1

    :cond_7
    iget v0, v1, Landroid/util/DisplayMetrics;->xdpi:F

    iput v0, p0, Lmaps/bf/a;->m:F

    iget v0, v1, Landroid/util/DisplayMetrics;->ydpi:F

    iput v0, p0, Lmaps/bf/a;->n:F

    goto :goto_2

    :cond_8
    iput v0, p0, Lmaps/bf/a;->m:F

    iput v0, p0, Lmaps/bf/a;->n:F

    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_3

    :cond_a
    move v1, v2

    goto :goto_5

    :cond_b
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :cond_c
    sput-object v0, Lmaps/bf/a;->h:[Ljava/lang/String;

    :cond_d
    sget-object v0, Lmaps/bf/a;->h:[Ljava/lang/String;

    invoke-static {v5, v0}, Lmaps/bf/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5}, Lmaps/bf/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lmaps/bf/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/bm/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-static {v1}, Lmaps/bm/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_e
    invoke-virtual {v6, v0}, Lmaps/bf/b;->c(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lmaps/bf/a;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.google.settings/partner"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "name=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v6

    :goto_2
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method

.method public static a()Lmaps/bf/a;
    .locals 1

    sget-object v0, Lmaps/bf/a;->b:Lmaps/bs/a;

    check-cast v0, Lmaps/bf/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lmaps/bf/a;
    .locals 2

    sget-object v1, Lmaps/bf/a;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/bf/a;->b:Lmaps/bs/a;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/bf/a;

    invoke-direct {v0, p0}, Lmaps/bf/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lmaps/bf/a;->b:Lmaps/bs/a;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lmaps/bf/a;->b:Lmaps/bs/a;

    check-cast v0, Lmaps/bf/a;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b()Ljava/lang/String;
    .locals 2

    const-class v1, Lmaps/bf/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/bf/a;->b:Lmaps/bs/a;

    check-cast v0, Lmaps/bf/a;

    iget-object v0, v0, Lmaps/bf/a;->q:Lmaps/bf/b;

    invoke-virtual {v0}, Lmaps/bf/b;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lmaps/bf/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public static d()Ljava/lang/String;
    .locals 1

    sget-object v0, Lmaps/bs/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static o()Ljava/lang/String;
    .locals 7

    const/16 v6, 0x5f

    const/16 v5, 0x2d

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "android:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final e()Lmaps/bs/d;
    .locals 2

    new-instance v0, Lmaps/bs/d;

    iget-object v1, p0, Lmaps/bs/a;->c:Lmaps/bt/k;

    invoke-direct {v0, v1}, Lmaps/bs/d;-><init>(Lmaps/bt/k;)V

    return-object v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/bf/a;->k:I

    return v0
.end method

.method public final g()F
    .locals 1

    iget v0, p0, Lmaps/bf/a;->m:F

    return v0
.end method

.method public final h()F
    .locals 1

    iget v0, p0, Lmaps/bf/a;->n:F

    return v0
.end method

.method public final i()Lmaps/bs/b;
    .locals 1

    iget-object v0, p0, Lmaps/bf/a;->e:Lmaps/bs/b;

    return-object v0
.end method

.method public final j()D
    .locals 2

    iget v0, p0, Lmaps/bf/a;->l:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lmaps/bf/a;->k:I

    const/16 v1, 0xc8

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 4

    const-wide/high16 v0, 0x4034000000000000L    # 20.0

    iget v2, p0, Lmaps/bf/a;->l:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Lmaps/bz/d;->a(D)I

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    sget-object v0, Lmaps/bf/a;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bf/a;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lmaps/bf/a;->i:Ljava/lang/Boolean;

    :cond_0
    sget-object v0, Lmaps/bf/a;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final n()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lmaps/bf/a;->j:Landroid/content/Context;

    return-object v0
.end method
