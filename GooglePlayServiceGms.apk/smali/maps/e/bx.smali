.class final Lmaps/e/bx;
.super Ljava/lang/Object;


# static fields
.field private static a:Lmaps/e/bx;

.field private static final b:Ljava/lang/Object;


# instance fields
.field private final c:Lmaps/bn/k;

.field private d:Lmaps/e/bz;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lmaps/e/bx;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lmaps/bn/k;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/e/bx;->c:Lmaps/bn/k;

    return-void
.end method

.method static a()Lmaps/e/bx;
    .locals 3

    sget-object v1, Lmaps/e/bx;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/e/bx;->a:Lmaps/e/bx;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/e/bx;

    invoke-static {}, Lmaps/bn/d;->a()Lmaps/bn/d;

    move-result-object v2

    invoke-direct {v0, v2}, Lmaps/e/bx;-><init>(Lmaps/bn/k;)V

    sput-object v0, Lmaps/e/bx;->a:Lmaps/e/bx;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lmaps/e/bx;->a:Lmaps/e/bx;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lmaps/e/bx;)Lmaps/e/bz;
    .locals 1

    iget-object v0, p0, Lmaps/e/bx;->d:Lmaps/e/bz;

    return-object v0
.end method

.method static a(Lmaps/bv/a;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v3}, Lmaps/bv/a;->j(I)I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v3, v1}, Lmaps/bv/a;->e(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lmaps/e/bx;)Lmaps/bn/k;
    .locals 1

    iget-object v0, p0, Lmaps/e/bx;->c:Lmaps/bn/k;

    return-object v0
.end method

.method private declared-synchronized c()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bx;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/bx;->e:Z

    iget-object v0, p0, Lmaps/e/bx;->c:Lmaps/bn/k;

    new-instance v1, Lmaps/e/by;

    invoke-direct {v1, p0}, Lmaps/e/by;-><init>(Lmaps/e/bx;)V

    invoke-interface {v0, v1}, Lmaps/bn/k;->c(Lmaps/bn/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lmaps/e/bx;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/e/bx;->e:Z

    return v0
.end method


# virtual methods
.method final a(Lmaps/e/bz;)V
    .locals 1

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bz;

    iput-object v0, p0, Lmaps/e/bx;->d:Lmaps/e/bz;

    return-void
.end method

.method final declared-synchronized b()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bx;->d:Lmaps/e/bz;

    sget-object v1, Lmaps/cm/b;->c:Lmaps/bv/c;

    invoke-virtual {v0, v1}, Lmaps/e/bz;->a(Lmaps/bv/c;)Lmaps/bv/a;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/b;->c:Lmaps/bv/c;

    invoke-direct {v0, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    :cond_0
    new-instance v1, Lmaps/bv/a;

    sget-object v2, Lmaps/cm/b;->d:Lmaps/bv/c;

    invoke-direct {v1, v2}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lmaps/bv/a;->a(II)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    iget-object v1, p0, Lmaps/e/bx;->d:Lmaps/e/bz;

    invoke-virtual {v1, v0}, Lmaps/e/bz;->a(Lmaps/bv/a;)V

    invoke-direct {p0}, Lmaps/e/bx;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
