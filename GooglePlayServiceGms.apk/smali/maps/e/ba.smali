.class final Lmaps/e/ba;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Z

.field private synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(ZLandroid/content/Context;)V
    .locals 0

    iput-boolean p1, p0, Lmaps/e/ba;->a:Z

    iput-object p2, p0, Lmaps/e/ba;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    iget-boolean v0, p0, Lmaps/e/ba;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/e/bd;

    invoke-direct {v0}, Lmaps/e/bd;-><init>()V

    iget-object v1, p0, Lmaps/e/ba;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lmaps/e/bd;->a(Landroid/content/Context;)V

    const/4 v0, 0x4

    const-string v1, "Failed to contact Google servers. Another attempt will be made when connectivity is established."

    invoke-static {v0, v1}, Lmaps/i/f;->a(ILjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Failed to load map. Error contacting Google servers. This is probably an authentication issue (but could be due to network errors)."

    const/4 v1, 0x6

    invoke-static {v1, v0}, Lmaps/i/f;->a(ILjava/lang/String;)V

    goto :goto_0
.end method
