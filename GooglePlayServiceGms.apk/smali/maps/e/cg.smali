.class public final Lmaps/e/cg;
.super Lewy;

# interfaces
.implements Lmaps/e/bq;
.implements Lmaps/e/ce;


# static fields
.field private static final a:Lcom/google/android/gms/maps/model/TileOverlayOptions;

.field private static final o:Lmaps/u/c;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Z

.field private final e:Lmaps/e/cd;

.field private final f:Ljava/util/ArrayList;

.field private final g:Lmaps/e/ch;

.field private h:Lmaps/u/a;

.field private volatile i:Lmaps/ap/n;

.field private j:Z

.field private volatile k:Z

.field private l:Z

.field private volatile m:Z

.field private final n:Lmaps/u/c;

.field private final p:Ljava/util/Set;

.field private final q:Lmaps/ao/a;

.field private r:Ljava/lang/ref/WeakReference;

.field private s:Z

.field private t:F

.field private u:Z

.field private final v:Ljava/lang/String;

.field private final w:Lmaps/e/bs;

.field private final x:Lmaps/h/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/model/TileOverlayOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/TileOverlayOptions;-><init>()V

    sput-object v0, Lmaps/e/cg;->a:Lcom/google/android/gms/maps/model/TileOverlayOptions;

    new-instance v0, Lmaps/u/b;

    invoke-direct {v0}, Lmaps/u/b;-><init>()V

    sput-object v0, Lmaps/e/cg;->o:Lmaps/u/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lmaps/e/bs;Lmaps/h/a;Lmaps/e/cd;Lmaps/u/c;ILcom/google/android/gms/maps/model/TileOverlayOptions;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lewy;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    new-instance v0, Lmaps/e/ch;

    invoke-direct {v0}, Lmaps/e/ch;-><init>()V

    iput-object v0, p0, Lmaps/e/cg;->g:Lmaps/e/ch;

    iput-boolean v1, p0, Lmaps/e/cg;->m:Z

    invoke-static {}, Lmaps/m/dk;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/cg;->p:Ljava/util/Set;

    iput-object p1, p0, Lmaps/e/cg;->v:Ljava/lang/String;

    iput-object p2, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    iput-object p3, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    iput-object p4, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    iput-object p5, p0, Lmaps/e/cg;->n:Lmaps/u/c;

    iput p6, p0, Lmaps/e/cg;->b:I

    const/16 v0, 0x14c

    iput v0, p0, Lmaps/e/cg;->c:I

    iput-boolean v1, p0, Lmaps/e/cg;->d:Z

    invoke-virtual {p7}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->e()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/cg;->s:Z

    invoke-virtual {p7}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->d()F

    move-result v0

    iput v0, p0, Lmaps/e/cg;->t:F

    invoke-virtual {p7}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->f()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/cg;->u:Z

    new-instance v0, Lmaps/ao/a;

    invoke-direct {v0}, Lmaps/ao/a;-><init>()V

    iput-object v0, p0, Lmaps/e/cg;->q:Lmaps/ao/a;

    invoke-virtual {p7}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->e()Z

    move-result v0

    sget-object v1, Lmaps/e/cg;->a:Lcom/google/android/gms/maps/model/TileOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->e()Z

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->af:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_0
    invoke-virtual {p7}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->d()F

    move-result v0

    sget-object v1, Lmaps/e/cg;->a:Lcom/google/android/gms/maps/model/TileOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ae:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_1
    invoke-virtual {p7}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->f()Z

    move-result v0

    sget-object v1, Lmaps/e/cg;->a:Lcom/google/android/gms/maps/model/TileOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->f()Z

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ag:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_2
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/maps/model/TileOverlayOptions;Landroid/content/res/Resources;Lmaps/e/bs;Ljava/util/concurrent/ScheduledExecutorService;Lmaps/h/a;)Lmaps/e/cg;
    .locals 8

    const/16 v0, 0x14c

    invoke-static {p2, v0}, Lmaps/ay/ap;->a(Landroid/content/res/Resources;I)I

    move-result v6

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->c()Lewd;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "TileOverlay.Options must specify a TileProvider"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    new-instance v4, Lmaps/e/cd;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/TileOverlayOptions;->c()Lewd;

    move-result-object v0

    invoke-direct {v4, v0, p0, p4}, Lmaps/e/cd;-><init>(Lewd;Ljava/lang/String;Ljava/util/concurrent/ScheduledExecutorService;)V

    new-instance v0, Lmaps/e/cg;

    sget-object v5, Lmaps/e/cg;->o:Lmaps/u/c;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p5

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lmaps/e/cg;-><init>(Ljava/lang/String;Lmaps/e/bs;Lmaps/h/a;Lmaps/e/cd;Lmaps/u/c;ILcom/google/android/gms/maps/model/TileOverlayOptions;)V

    invoke-virtual {v4, v0}, Lmaps/e/cd;->a(Lmaps/e/ce;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ad:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->a(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ae:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->b(Lmaps/e/bq;)V

    iput p1, p0, Lmaps/e/cg;->t:F

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->c(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ar/a;Lmaps/as/a;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    iput-boolean v2, p0, Lmaps/e/cg;->m:Z

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/cg;->s:Z

    if-nez v0, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0

    iput-boolean v1, p0, Lmaps/e/cg;->l:Z

    iget-object v0, p0, Lmaps/e/cg;->h:Lmaps/u/a;

    invoke-interface {v0, p1}, Lmaps/u/a;->b(Lmaps/ar/a;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lmaps/e/cg;->g:Lmaps/e/ch;

    invoke-virtual {p1}, Lmaps/ar/a;->g()Lmaps/ac/av;

    move-result-object v4

    invoke-virtual {v0, v4}, Lmaps/e/ch;->a(Lmaps/ac/av;)V

    iget-object v0, p0, Lmaps/e/cg;->g:Lmaps/e/ch;

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_1
    iget-object v0, p0, Lmaps/e/cg;->p:Ljava/util/Set;

    iget-object v4, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-interface {v0, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0}, Lmaps/e/cd;->a()V

    iget-boolean v4, p0, Lmaps/e/cg;->j:Z

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    if-eqz v4, :cond_4

    iget-object v6, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v6, v0}, Lmaps/e/cd;->a(Lmaps/ac/bt;)Lmaps/au/ap;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_3

    iget-object v6, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lmaps/e/cg;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    iget-object v6, p0, Lmaps/e/cg;->p:Ljava/util/Set;

    invoke-interface {v6, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-interface {v0, v1}, Lmaps/au/ap;->a(Z)V

    :cond_2
    iget-object v0, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    iget-object v0, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v6, p0, Lmaps/e/cg;->b:I

    if-eq v0, v6, :cond_5

    :cond_3
    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0}, Lmaps/e/cd;->b()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    iget-object v6, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v6, v0}, Lmaps/e/cd;->b(Lmaps/ac/bt;)Lmaps/au/ap;

    move-result-object v0

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lmaps/e/cg;->m:Z

    iget-boolean v0, p0, Lmaps/e/cg;->j:Z

    iput-boolean v0, p0, Lmaps/e/cg;->k:Z

    iget-object v0, p0, Lmaps/e/cg;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    invoke-interface {v0, v2}, Lmaps/au/ap;->a(Z)V

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lmaps/e/cg;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto/16 :goto_0
.end method

.method public final a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 5

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/e/cg;->r:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0, p1}, Lmaps/e/cd;->a(Lmaps/as/a;)V

    iput-object p2, p0, Lmaps/e/cg;->i:Lmaps/ap/n;

    iget-object v0, p0, Lmaps/e/cg;->h:Lmaps/u/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/cg;->n:Lmaps/u/c;

    sget-object v1, Lmaps/ao/b;->d:Lmaps/ao/b;

    iget v2, p0, Lmaps/e/cg;->c:I

    iget-boolean v3, p0, Lmaps/e/cg;->d:Z

    iget-object v4, p0, Lmaps/e/cg;->q:Lmaps/ao/a;

    invoke-interface {v0, v1, v2, v3, v4}, Lmaps/u/c;->a(Lmaps/ao/b;IZLmaps/ao/a;)Lmaps/u/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/cg;->h:Lmaps/u/a;

    :cond_0
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 5

    const/4 v4, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/cg;->s:Z

    if-nez v0, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit p0

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v1, Lmaps/ap/c;

    invoke-direct {v1, p3}, Lmaps/ap/c;-><init>(Lmaps/ap/c;)V

    iget-boolean v0, p0, Lmaps/e/cg;->k:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmaps/e/cg;->j:Z

    if-nez v0, :cond_2

    invoke-virtual {p0, p2, p1}, Lmaps/e/cg;->a(Lmaps/ar/a;Lmaps/as/a;)V

    :cond_2
    iget-boolean v0, p0, Lmaps/e/cg;->l:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    iget-object v2, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lmaps/e/cd;->a(Ljava/util/List;)V

    :cond_3
    iget-object v0, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p1}, Lmaps/as/a;->B()V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lmaps/ap/c;->a(I)V

    iget-object v0, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    invoke-interface {v0, p1, p2, v1}, Lmaps/au/ap;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    iget-object v0, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    iget-boolean v3, p0, Lmaps/e/cg;->u:Z

    if-nez v3, :cond_4

    invoke-interface {v0}, Lmaps/au/ap;->g()V

    :cond_4
    invoke-interface {v0, p1, p2, v1}, Lmaps/au/ap;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    invoke-virtual {p1}, Lmaps/as/a;->C()V

    :cond_6
    iget-boolean v0, p0, Lmaps/e/cg;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    iget-object v1, p0, Lmaps/e/cg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lmaps/e/cd;->b(Ljava/util/List;)V

    iput-boolean v4, p0, Lmaps/e/cg;->l:Z

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->af:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/e/cg;->s:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lewx;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/cg;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0, p1}, Lmaps/e/cd;->a(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/cg;->k:Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ac:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0}, Lmaps/e/cd;->e()V

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void
.end method

.method public final c(I)V
    .locals 1

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/e/cg;->j:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 3

    iget-object v0, p0, Lmaps/e/cg;->i:Lmaps/ap/n;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/ap/n;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/cg;->s:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final e_(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/cg;->x:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ag:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/e/cg;->u:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/cg;->u:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()F
    .locals 1

    iget-object v0, p0, Lmaps/e/cg;->w:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget v0, p0, Lmaps/e/cg;->t:F

    return v0
.end method

.method public final l()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/cg;->r:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, Lmaps/e/cg;->i:Lmaps/ap/n;

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0}, Lmaps/e/cd;->c()V

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0}, Lmaps/e/cd;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/cg;->k:Z

    return-void
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0}, Lmaps/e/cd;->f()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/cg;->k:Z

    return-void
.end method

.method public final declared-synchronized n()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/cg;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/e/cg;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized o()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lmaps/e/cg;->i:Lmaps/ap/n;

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    invoke-virtual {v0}, Lmaps/e/cd;->c()V

    iget-object v0, p0, Lmaps/e/cg;->e:Lmaps/e/cd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/e/cd;->a(Lmaps/e/ce;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lmaps/e/cg;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
