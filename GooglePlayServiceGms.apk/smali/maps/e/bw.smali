.class final Lmaps/e/bw;
.super Leve;


# instance fields
.field private final a:Lmaps/ar/a;

.field private final b:Lmaps/h/a;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Lmaps/ar/a;Lmaps/h/a;IIII)V
    .locals 0

    invoke-direct {p0}, Leve;-><init>()V

    iput-object p1, p0, Lmaps/e/bw;->a:Lmaps/ar/a;

    iput p3, p0, Lmaps/e/bw;->c:I

    iput p4, p0, Lmaps/e/bw;->d:I

    iput p5, p0, Lmaps/e/bw;->e:I

    iput p6, p0, Lmaps/e/bw;->f:I

    iput-object p2, p0, Lmaps/e/bw;->b:Lmaps/h/a;

    return-void
.end method


# virtual methods
.method public final a(Lcrv;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 3

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    iget-object v1, p0, Lmaps/e/bw;->b:Lmaps/h/a;

    sget-object v2, Lmaps/h/b;->bk:Lmaps/h/b;

    invoke-interface {v1, v2}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/bw;->a:Lmaps/ar/a;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/maps/model/VisibleRegion;
    .locals 6

    iget-object v0, p0, Lmaps/e/bw;->b:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->bl:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bw;->a:Lmaps/ar/a;

    iget v1, p0, Lmaps/e/bw;->c:I

    iget v2, p0, Lmaps/e/bw;->d:I

    iget v3, p0, Lmaps/e/bw;->e:I

    iget v4, p0, Lmaps/e/bw;->f:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lmaps/ar/a;->a(IIII)Lmaps/ac/cw;

    move-result-object v5

    new-instance v0, Lcom/google/android/gms/maps/model/VisibleRegion;

    invoke-virtual {v5}, Lmaps/ac/cw;->d()Lmaps/ac/av;

    move-result-object v1

    invoke-static {v1}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v5}, Lmaps/ac/cw;->e()Lmaps/ac/av;

    move-result-object v2

    invoke-static {v2}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-virtual {v5}, Lmaps/ac/cw;->g()Lmaps/ac/av;

    move-result-object v3

    invoke-static {v3}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    invoke-virtual {v5}, Lmaps/ac/cw;->f()Lmaps/ac/av;

    move-result-object v4

    invoke-static {v4}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    invoke-virtual {v5}, Lmaps/ac/cw;->a()Lmaps/ac/cx;

    move-result-object v5

    invoke-static {v5}, Lmaps/i/a;->a(Lmaps/ac/cx;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/maps/model/VisibleRegion;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)Lcrv;
    .locals 4

    iget-object v0, p0, Lmaps/e/bw;->b:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->bm:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bw;->a:Lmaps/ar/a;

    invoke-static {p1}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    invoke-static {v1}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "camera"

    iget-object v2, p0, Lmaps/e/bw;->a:Lmaps/ar/a;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
