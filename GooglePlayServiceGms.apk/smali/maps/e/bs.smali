.class final Lmaps/e/bs;
.super Lmaps/ay/u;

# interfaces
.implements Lmaps/e/br;


# static fields
.field private static final b:Lmaps/ay/v;


# instance fields
.field final a:Lmaps/i/g;

.field private final c:Lmaps/c/m;

.field private final d:Ljava/util/Map;

.field private final e:Lmaps/e/n;

.field private final f:Ljava/util/SortedSet;

.field private g:I

.field private final h:Ljava/util/concurrent/ScheduledExecutorService;

.field private final i:Lmaps/h/a;

.field private j:Lmaps/ap/n;

.field private k:Lmaps/as/a;

.field private l:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lmaps/ay/v;->e:Lmaps/ay/v;

    sput-object v0, Lmaps/e/bs;->b:Lmaps/ay/v;

    return-void
.end method

.method private constructor <init>(Lmaps/c/m;Lmaps/e/n;Lmaps/i/g;Ljava/util/concurrent/ScheduledExecutorService;Lmaps/h/a;)V
    .locals 2

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bs;->d:Ljava/util/Map;

    new-instance v0, Lmaps/e/bt;

    invoke-direct {v0}, Lmaps/e/bt;-><init>()V

    new-instance v1, Ljava/util/TreeSet;

    invoke-static {v0}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v1, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    iput-object p1, p0, Lmaps/e/bs;->c:Lmaps/c/m;

    iput-object p2, p0, Lmaps/e/bs;->e:Lmaps/e/n;

    iput-object p3, p0, Lmaps/e/bs;->a:Lmaps/i/g;

    iput-object p4, p0, Lmaps/e/bs;->h:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p5, p0, Lmaps/e/bs;->i:Lmaps/h/a;

    return-void
.end method

.method public static a(Lmaps/c/m;Lmaps/h/a;)Lmaps/e/br;
    .locals 6

    invoke-static {}, Lmaps/i/g;->a()Lmaps/i/g;

    move-result-object v3

    invoke-interface {p0}, Lmaps/c/m;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmaps/e/n;->a(Landroid/content/Context;)Lmaps/e/n;

    move-result-object v2

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    new-instance v0, Lmaps/e/bs;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lmaps/e/bs;-><init>(Lmaps/c/m;Lmaps/e/n;Lmaps/i/g;Ljava/util/concurrent/ScheduledExecutorService;Lmaps/h/a;)V

    return-object v0
.end method

.method private b(Lcom/google/android/gms/maps/model/CircleOptions;)Lmaps/e/ag;
    .locals 4

    iget-object v0, p0, Lmaps/e/bs;->a:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    monitor-enter p0

    :try_start_0
    new-instance v0, Lmaps/e/ag;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ci"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmaps/e/bs;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/e/bs;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/bs;->i:Lmaps/h/a;

    invoke-direct {v0, v1, p1, p0, v2}, Lmaps/e/ag;-><init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/CircleOptions;Lmaps/e/bs;Lmaps/h/a;)V

    invoke-virtual {v0}, Lmaps/e/ag;->k()V

    invoke-direct {p0, v0}, Lmaps/e/bs;->d(Lmaps/e/bq;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/e/bs;->j()V

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lmaps/e/ax;
    .locals 6

    iget-object v0, p0, Lmaps/e/bs;->a:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    monitor-enter p0

    :try_start_0
    new-instance v0, Lmaps/e/ax;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "go"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmaps/e/bs;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/e/bs;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/bs;->e:Lmaps/e/n;

    iget-object v5, p0, Lmaps/e/bs;->i:Lmaps/h/a;

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lmaps/e/ax;-><init>(Ljava/lang/String;Lmaps/e/n;Lcom/google/android/gms/maps/model/GroundOverlayOptions;Lmaps/e/bs;Lmaps/h/a;)V

    invoke-direct {p0, v0}, Lmaps/e/bs;->d(Lmaps/e/bq;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/e/bs;->j()V

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lcom/google/android/gms/maps/model/PolygonOptions;)Lmaps/e/bu;
    .locals 4

    iget-object v0, p0, Lmaps/e/bs;->a:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    monitor-enter p0

    :try_start_0
    new-instance v1, Lmaps/e/bu;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "pg"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmaps/e/bs;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/e/bs;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lmaps/e/bs;->i:Lmaps/h/a;

    invoke-direct {v1, v0, p1, p0, v2}, Lmaps/e/bu;-><init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/PolygonOptions;Lmaps/e/bs;Lmaps/h/a;)V

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Lmaps/e/bu;->p()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-direct {p0, v1}, Lmaps/e/bs;->d(Lmaps/e/bq;)V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {p0}, Lmaps/e/bs;->j()V

    return-object v1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lcom/google/android/gms/maps/model/PolylineOptions;)Lmaps/e/bv;
    .locals 4

    iget-object v0, p0, Lmaps/e/bs;->a:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    monitor-enter p0

    :try_start_0
    new-instance v1, Lmaps/e/bv;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "pl"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmaps/e/bs;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/e/bs;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lmaps/e/bs;->i:Lmaps/h/a;

    invoke-direct {v1, v0, p1, p0, v2}, Lmaps/e/bv;-><init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/PolylineOptions;Lmaps/e/bs;Lmaps/h/a;)V

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Lmaps/e/bv;->j()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-direct {p0, v1}, Lmaps/e/bs;->d(Lmaps/e/bq;)V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {p0}, Lmaps/e/bs;->j()V

    return-object v1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lmaps/e/cg;
    .locals 6

    iget-object v0, p0, Lmaps/e/bs;->a:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "to"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lmaps/e/bs;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/e/bs;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/bs;->c:Lmaps/c/m;

    invoke-interface {v1}, Lmaps/c/m;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Lmaps/e/bs;->h:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v5, p0, Lmaps/e/bs;->i:Lmaps/h/a;

    move-object v1, p1

    move-object v3, p0

    invoke-static/range {v0 .. v5}, Lmaps/e/cg;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/TileOverlayOptions;Landroid/content/res/Resources;Lmaps/e/bs;Ljava/util/concurrent/ScheduledExecutorService;Lmaps/h/a;)Lmaps/e/cg;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/e/bs;->d(Lmaps/e/bq;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/e/bs;->j()V

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d(Lmaps/e/bq;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->d:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/e/bq;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/e/bs;->k:Lmaps/as/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/bs;->k:Lmaps/as/a;

    iget-object v1, p0, Lmaps/e/bs;->j:Lmaps/ap/n;

    invoke-interface {p1, v0, v1}, Lmaps/e/bq;->a(Lmaps/as/a;Lmaps/ap/n;)V

    :cond_0
    iget-wide v0, p0, Lmaps/e/bs;->l:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/maps/model/CircleOptions;)Lewi;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/e/bs;->b(Lcom/google/android/gms/maps/model/CircleOptions;)Lmaps/e/ag;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lewl;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/e/bs;->b(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lmaps/e/ax;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/maps/model/PolygonOptions;)Lewr;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/e/bs;->b(Lcom/google/android/gms/maps/model/PolygonOptions;)Lmaps/e/bu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lewu;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/e/bs;->b(Lcom/google/android/gms/maps/model/PolylineOptions;)Lmaps/e/bv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lewx;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/e/bs;->b(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lmaps/e/cg;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->a:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    invoke-interface {v0}, Lmaps/e/bq;->o()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->clear()V

    iget-object v0, p0, Lmaps/e/bs;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {p0}, Lmaps/e/bs;->j()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    invoke-interface {v0, p1}, Lmaps/e/bq;->c(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/e/bs;->k:Lmaps/as/a;

    iput-object p2, p0, Lmaps/e/bs;->j:Lmaps/ap/n;

    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    iget-object v2, p0, Lmaps/e/bs;->k:Lmaps/as/a;

    iget-object v3, p0, Lmaps/e/bs;->j:Lmaps/ap/n;

    invoke-interface {v0, v2, v3}, Lmaps/e/bq;->a(Lmaps/as/a;Lmaps/ap/n;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    invoke-virtual {p1}, Lmaps/as/a;->B()V

    invoke-interface {v0, p1, p2, p3}, Lmaps/e/bq;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    invoke-virtual {p1}, Lmaps/as/a;->C()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Lmaps/e/bq;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->d:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/e/bq;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/e/bs;->d:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/e/bq;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lmaps/e/bq;->o()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    invoke-interface {v0, p1}, Lmaps/e/bq;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(FFLmaps/ac/av;Lmaps/ar/a;)Z
    .locals 3

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    const/4 v1, 0x0

    new-array v1, v1, [Lmaps/e/bq;

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized ar_()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    invoke-interface {v0}, Lmaps/e/bq;->n()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Lmaps/ay/u;
    .locals 0

    return-object p0
.end method

.method final b(Lmaps/e/bq;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final declared-synchronized b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    invoke-interface {v0, p1, p2}, Lmaps/e/bq;->a(Lmaps/ar/a;Lmaps/as/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0
.end method

.method public final declared-synchronized c(Lmaps/as/a;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    invoke-interface {v0}, Lmaps/e/bq;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method final c(Lmaps/e/bq;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method final d()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bs;->a:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    return-void
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    sget-object v0, Lmaps/e/bs;->b:Lmaps/ay/v;

    return-object v0
.end method

.method public final declared-synchronized i()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bs;->f:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bq;

    invoke-interface {v0}, Lmaps/e/bq;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method final j()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bs;->c:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->f()V

    return-void
.end method
