.class final Lmaps/e/bc;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bn/l;


# instance fields
.field private a:Landroid/content/SharedPreferences;

.field private b:Lmaps/e/bb;

.field private c:Landroid/content/Context;

.field private d:J


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Lmaps/e/bb;Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/e/bc;->d:J

    iput-object p1, p0, Lmaps/e/bc;->a:Landroid/content/SharedPreferences;

    iput-object p2, p0, Lmaps/e/bc;->b:Lmaps/e/bb;

    iput-object p3, p0, Lmaps/e/bc;->c:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/bn/c;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/bn/c;)V
    .locals 6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/e/bc;->d:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lmaps/e/bc;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "LAST_FETCH_PERSISTENT_TAG"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iput-wide v0, p0, Lmaps/e/bc;->d:J

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 6

    iget-object v0, p0, Lmaps/e/bc;->c:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/e/ay;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lmaps/be/g;->b()Lmaps/be/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/be/f;->a()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lmaps/e/bc;->a:Landroid/content/SharedPreferences;

    const-string v5, "LAST_FETCH_PERSISTENT_TAG"

    invoke-interface {v4, v5, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lmaps/e/bc;->b:Lmaps/e/bb;

    invoke-static {}, Lmaps/ap/p;->e()V

    goto :goto_0
.end method
