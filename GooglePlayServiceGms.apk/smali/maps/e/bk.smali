.class public final Lmaps/e/bk;
.super Letx;


# instance fields
.field private a:Lmaps/e/al;

.field private final b:Lcom/google/android/gms/maps/GoogleMapOptions;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 1

    invoke-direct {p0}, Letx;-><init>()V

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lmaps/e/bk;->c:Landroid/content/Context;

    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, Lmaps/e/bk;->b:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void

    :cond_0
    new-instance p2, Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {p2}, Lcom/google/android/gms/maps/GoogleMapOptions;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Letk;
    .locals 1

    iget-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/e/bk;->c:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/e/bk;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lmaps/i/f;->a(Landroid/app/Activity;)Z

    move-result v0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/e/bk;->c:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v2, p0, Lmaps/e/bk;->b:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {v0, v2, v1}, Lmaps/e/al;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;Z)Lmaps/e/al;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    iget-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    invoke-virtual {v0, p1}, Lmaps/e/al;->a(Landroid/os/Bundle;)V

    return-void

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->w()V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    invoke-virtual {v0, p1}, Lmaps/e/al;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->x()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->v()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->y()V

    return-void
.end method

.method public final f()Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/bk;->a:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->z()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method
