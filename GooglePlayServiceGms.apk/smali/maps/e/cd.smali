.class public final Lmaps/e/cd;
.super Ljava/lang/Object;


# static fields
.field private static b:Lmaps/au/ap;


# instance fields
.field final a:Ljava/util/Set;

.field private final c:Lmaps/aq/e;

.field private final d:Lewd;

.field private final e:Ljava/util/concurrent/ScheduledExecutorService;

.field private final f:Ljava/util/Set;

.field private volatile g:Lmaps/as/a;

.field private final h:Lmaps/aq/a;

.field private volatile i:Lmaps/e/ce;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/au/av;

    invoke-direct {v0}, Lmaps/au/av;-><init>()V

    sput-object v0, Lmaps/e/cd;->b:Lmaps/au/ap;

    return-void
.end method

.method public constructor <init>(Lewd;Ljava/lang/String;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/cd;->f:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/cd;->a:Ljava/util/Set;

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/e/cd;->d:Lewd;

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lmaps/e/cd;->a(I)Lmaps/ax/a;

    move-result-object v0

    new-instance v1, Lmaps/aq/e;

    sget-object v2, Lmaps/ao/b;->t:Lmaps/ao/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Lmaps/aq/e;-><init>(Lmaps/ao/b;Ljava/util/Set;Lmaps/ax/a;)V

    iput-object v1, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    iput-object p3, p0, Lmaps/e/cd;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lmaps/bs/b;

    invoke-direct {v0}, Lmaps/bs/b;-><init>()V

    invoke-static {v0}, Lmaps/aq/a;->a(Lmaps/bs/b;)V

    invoke-static {}, Lmaps/aq/a;->a()Lmaps/aq/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    return-void
.end method

.method static synthetic a(Lmaps/e/cd;)Lewd;
    .locals 1

    iget-object v0, p0, Lmaps/e/cd;->d:Lewd;

    return-object v0
.end method

.method private a(Lmaps/ac/bt;Z)Lmaps/au/ap;
    .locals 3

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    iget-object v2, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    invoke-virtual {v0, v1, v2, p1, p2}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Lmaps/ac/bt;Z)Lmaps/au/ap;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(I)Lmaps/ax/a;
    .locals 6

    const/16 v5, 0x20

    const/4 v3, 0x0

    new-array v4, v5, [I

    move v2, v3

    move v1, v3

    :goto_0
    if-ge v2, v5, :cond_0

    const/4 v0, 0x1

    shl-int/2addr v0, v2

    and-int/2addr v0, p0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    aput v2, v4, v1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    new-array v0, v1, [I

    invoke-static {v4, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0}, Lmaps/ax/a;->a([I)Lmaps/ax/a;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a(Lmaps/ac/bt;Lmaps/au/ap;Lmaps/as/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    invoke-virtual {v0, p3, v1, p1, p2}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Lmaps/ac/bt;Lmaps/au/ap;)V

    :cond_0
    iget-object v0, p0, Lmaps/e/cd;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lmaps/e/cd;->i:Lmaps/e/ce;

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lmaps/e/ce;->c(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/e/cd;Lmaps/ac/bt;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    sget-object v1, Lmaps/e/cd;->b:Lmaps/au/ap;

    invoke-direct {p0, p1, v1, v0}, Lmaps/e/cd;->a(Lmaps/ac/bt;Lmaps/au/ap;Lmaps/as/a;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/e/cd;Lmaps/ac/bt;Lmaps/ac/y;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    iget-object v1, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-static {p2, v1}, Lmaps/au/k;->a(Lmaps/ac/bs;Lmaps/as/a;)Lmaps/au/k;

    move-result-object v0

    :cond_0
    invoke-direct {p0, p1, v0, v1}, Lmaps/e/cd;->a(Lmaps/ac/bt;Lmaps/au/ap;Lmaps/as/a;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/e/cd;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    iget-object v0, p0, Lmaps/e/cd;->e:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method


# virtual methods
.method public final a(Lmaps/ac/bt;)Lmaps/au/ap;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/e/cd;->a(Lmaps/ac/bt;Z)Lmaps/au/ap;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    iget-object v2, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    invoke-virtual {v0, v1, v2}, Lmaps/aq/a;->d(Lmaps/as/a;Lmaps/aq/e;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    sget-object v3, Lmaps/e/cd;->b:Lmaps/au/ap;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    iget-object v2, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    iget-object v3, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    invoke-virtual {v0, v2, v3, v1}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;Ljava/util/List;)V

    return-void
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    const-string v0, "state must not be null."

    invoke-static {p1, v0}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    return-void
.end method

.method public final a(Lmaps/e/ce;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/e/cd;->i:Lmaps/e/ce;

    if-nez v2, :cond_1

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->a(Z)V

    :goto_1
    iput-object p1, p0, Lmaps/e/cd;->i:Lmaps/e/ce;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    :goto_2
    invoke-static {v0}, Lmaps/k/o;->a(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    invoke-virtual {v0, p1}, Lmaps/aq/a;->a(Z)V

    return-void
.end method

.method public final b(Lmaps/ac/bt;)Lmaps/au/ap;
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmaps/e/cd;->a(Lmaps/ac/bt;Z)Lmaps/au/ap;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/e/cd;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/cd;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lmaps/e/cf;

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-direct {v1, p0, p1, v2}, Lmaps/e/cf;-><init>(Lmaps/e/cd;Lmaps/ac/bt;Ljava/util/Random;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    sget-boolean v0, Lmaps/bb/b;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    iget-object v2, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    invoke-virtual {v0, v1, v2}, Lmaps/aq/a;->c(Lmaps/as/a;Lmaps/aq/e;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/util/List;)V
    .locals 4

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    sget-object v3, Lmaps/e/cd;->b:Lmaps/au/ap;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    iget-object v2, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    iget-object v3, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    invoke-virtual {v0, v2, v3, v1}, Lmaps/aq/a;->b(Lmaps/as/a;Lmaps/aq/e;Ljava/util/List;)V

    return-void
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    iget-object v1, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    iget-object v2, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    invoke-virtual {v0, v1, v2}, Lmaps/aq/a;->b(Lmaps/as/a;Lmaps/aq/e;)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 3

    iget-object v1, p0, Lmaps/e/cd;->a:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/cd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/cf;

    invoke-virtual {v0}, Lmaps/e/cf;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/e/cd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lmaps/e/cd;->g:Lmaps/as/a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    iget-object v2, p0, Lmaps/e/cd;->c:Lmaps/aq/e;

    invoke-virtual {v1, v0, v2}, Lmaps/aq/a;->a(Lmaps/as/a;Lmaps/aq/e;)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lmaps/e/cd;->h:Lmaps/aq/a;

    invoke-virtual {v0}, Lmaps/aq/a;->b()V

    return-void
.end method
