.class public final Lmaps/e/bg;
.super Letu;


# instance fields
.field private final a:Lmaps/e/bi;

.field private b:Lmaps/e/al;

.field private c:Lcom/google/android/gms/maps/GoogleMapOptions;


# direct methods
.method private constructor <init>(Lmaps/e/bi;)V
    .locals 1

    invoke-direct {p0}, Letu;-><init>()V

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bi;

    iput-object v0, p0, Lmaps/e/bg;->a:Lmaps/e/bi;

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lmaps/e/bg;
    .locals 3

    invoke-static {p0}, Lmaps/i/f;->a(Landroid/app/Activity;)Z

    move-result v0

    new-instance v1, Lmaps/e/bg;

    new-instance v2, Lmaps/e/bh;

    invoke-direct {v2, v0}, Lmaps/e/bh;-><init>(Z)V

    invoke-direct {v1, v2}, Lmaps/e/bg;-><init>(Lmaps/e/bi;)V

    return-object v1
.end method


# virtual methods
.method public final a(Lcrv;Lcrv;Landroid/os/Bundle;)Lcrv;
    .locals 3

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v1, p0, Lmaps/e/bg;->a:Lmaps/e/bi;

    iget-object v2, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v1, v0, v2}, Lmaps/e/bi;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;)Lmaps/e/al;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0, p3}, Lmaps/e/al;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->z()Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->z()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Letk;
    .locals 1

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-nez v0, :cond_0

    const-string v0, "MapOptions"

    invoke-static {p1, v0}, Levm;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    iput-object v0, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    :cond_0
    iget-object v0, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;-><init>()V

    iput-object v0, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    :cond_1
    return-void
.end method

.method public final a(Lcrv;Lcom/google/android/gms/maps/GoogleMapOptions;Landroid/os/Bundle;)V
    .locals 0

    iput-object p2, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->w()V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-eqz v0, :cond_0

    const-string v0, "MapOptions"

    iget-object v1, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {p1, v0, v1}, Levm;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0, p1}, Lmaps/e/al;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->x()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->v()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->v()V

    iput-object v1, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    :cond_0
    iput-object v1, p0, Lmaps/e/bg;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    invoke-virtual {v0}, Lmaps/e/al;->y()V

    return-void
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bg;->b:Lmaps/e/al;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
