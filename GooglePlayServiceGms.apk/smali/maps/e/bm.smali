.class public final Lmaps/e/bm;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ay/d;
.implements Lmaps/ay/p;
.implements Lmaps/ay/t;
.implements Lmaps/e/bl;


# static fields
.field private static final a:Lcom/google/android/gms/maps/model/MarkerOptions;


# instance fields
.field private final b:Lmaps/c/l;

.field private final c:Lmaps/c/m;

.field private final d:Lmaps/ay/r;

.field private final e:Lmaps/ay/o;

.field private final f:Ljava/util/Map;

.field private final g:Lmaps/e/n;

.field private final h:Landroid/view/ViewGroup;

.field private final i:Lmaps/g/b;

.field private final j:Lmaps/i/g;

.field private k:Leur;

.field private l:Leuu;

.field private m:Leuc;

.field private n:Letn;

.field private o:I

.field private final p:Lmaps/h/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    sput-object v0, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    return-void
.end method

.method private constructor <init>(Lmaps/c/m;Lmaps/c/l;Lmaps/ay/r;Lmaps/ay/o;Landroid/view/ViewGroup;Lmaps/g/b;Lmaps/e/n;Lmaps/i/g;Lmaps/h/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bm;->f:Ljava/util/Map;

    iput-object p1, p0, Lmaps/e/bm;->c:Lmaps/c/m;

    iput-object p2, p0, Lmaps/e/bm;->b:Lmaps/c/l;

    iput-object p3, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    iput-object p4, p0, Lmaps/e/bm;->e:Lmaps/ay/o;

    iput-object p5, p0, Lmaps/e/bm;->h:Landroid/view/ViewGroup;

    iput-object p6, p0, Lmaps/e/bm;->i:Lmaps/g/b;

    iput-object p7, p0, Lmaps/e/bm;->g:Lmaps/e/n;

    iput-object p8, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    iput-object p9, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    iget-object v0, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    invoke-virtual {v0, p0}, Lmaps/ay/r;->a(Lmaps/ay/d;)V

    iget-object v0, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    invoke-virtual {v0, p0}, Lmaps/ay/r;->a(Lmaps/ay/t;)V

    invoke-virtual {p4, p0}, Lmaps/ay/o;->a(Lmaps/ay/p;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Lmaps/e/f;FFFFZZZFF)Lmaps/au/af;
    .locals 7

    iget-object v1, p0, Lmaps/e/bm;->g:Lmaps/e/n;

    invoke-virtual {v1, p3}, Lmaps/e/n;->a(Lmaps/e/f;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v1, Lmaps/au/af;

    invoke-static {p2}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lmaps/au/af;-><init>(Lmaps/ac/av;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    invoke-virtual {v1, p8}, Lmaps/au/af;->a(Z)V

    move/from16 v0, p9

    invoke-virtual {v1, v0}, Lmaps/au/af;->c(Z)V

    if-nez p10, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v1, v2}, Lmaps/au/af;->b(Z)V

    move/from16 v0, p11

    invoke-virtual {v1, v0}, Lmaps/au/af;->a(F)V

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p7

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lmaps/au/af;->b(II)V

    move/from16 v0, p12

    invoke-virtual {v1, v0}, Lmaps/au/af;->b(F)V

    return-object v1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/e/bm;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Lmaps/e/f;FFFFZZZFF)Lmaps/au/af;
    .locals 1

    invoke-direct/range {p0 .. p12}, Lmaps/e/bm;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Lmaps/e/f;FFFFZZZFF)Lmaps/au/af;

    move-result-object v0

    return-object v0
.end method

.method static a(Lmaps/c/m;Lmaps/c/l;Landroid/content/Context;Landroid/content/res/Resources;Lmaps/h/a;)Lmaps/e/bm;
    .locals 10

    const/4 v1, -0x2

    new-instance v5, Landroid/widget/LinearLayout;

    invoke-direct {v5, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    sget v0, Lmaps/b/e;->R:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p2}, Lmaps/g/b;->a(Landroid/content/Context;)Lmaps/g/b;

    move-result-object v6

    invoke-static {}, Lmaps/i/g;->a()Lmaps/i/g;

    move-result-object v8

    sget-object v0, Lmaps/ay/v;->o:Lmaps/ay/v;

    invoke-interface {p0, v0}, Lmaps/c/m;->a(Lmaps/ay/v;)Lmaps/ay/r;

    move-result-object v3

    invoke-interface {p0}, Lmaps/c/m;->v()Lmaps/ap/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v4

    invoke-static {p2}, Lmaps/e/n;->a(Landroid/content/Context;)Lmaps/e/n;

    move-result-object v7

    new-instance v0, Lmaps/e/bm;

    move-object v1, p0

    move-object v2, p1

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, Lmaps/e/bm;-><init>(Lmaps/c/m;Lmaps/c/l;Lmaps/ay/r;Lmaps/ay/o;Landroid/view/ViewGroup;Lmaps/g/b;Lmaps/e/n;Lmaps/i/g;Lmaps/h/a;)V

    return-object v0
.end method

.method static synthetic a(Lmaps/e/bm;)Lmaps/i/g;
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    return-object v0
.end method

.method static synthetic a(Lmaps/e/bm;Lmaps/e/bn;)V
    .locals 3

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->c()V

    iget-object v0, p0, Lmaps/e/bm;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/e/bn;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bn;

    if-ne p1, v0, :cond_0

    iget-object v1, p0, Lmaps/e/bm;->f:Ljava/util/Map;

    invoke-static {v0}, Lmaps/e/bn;->b(Lmaps/e/bn;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/e/bm;->g:Lmaps/e/n;

    invoke-static {v0}, Lmaps/e/bn;->c(Lmaps/e/bn;)Lmaps/e/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/e/n;->b(Lmaps/e/f;)V

    iget-object v1, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    invoke-static {v0}, Lmaps/e/bn;->a(Lmaps/e/bn;)Lmaps/au/af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/ay/r;->b(Lmaps/au/af;)V

    :cond_0
    return-void
.end method

.method private a(Lmaps/e/bn;)V
    .locals 4

    const/4 v1, 0x0

    invoke-static {p1}, Lmaps/e/bn;->a(Lmaps/e/bn;)Lmaps/au/af;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/af;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lmaps/e/bm;->n:Letn;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/e/bm;->n:Letn;

    invoke-interface {v0, p1}, Letn;->a(Lewo;)Lcrv;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-nez v0, :cond_3

    :try_start_1
    iget-object v0, p0, Lmaps/e/bm;->n:Letn;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/e/bm;->n:Letn;

    invoke-interface {v0, p1}, Letn;->b(Lewo;)Lcrv;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    if-nez v0, :cond_2

    invoke-static {p1}, Lmaps/e/bn;->d(Lmaps/e/bn;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmaps/k/aa;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/bm;->i:Lmaps/g/b;

    invoke-virtual {p1}, Lmaps/e/bn;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/g/b;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/e/bm;->i:Lmaps/g/b;

    invoke-virtual {p1}, Lmaps/e/bn;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/g/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/e/bm;->i:Lmaps/g/b;

    invoke-virtual {v0}, Lmaps/g/b;->a()Landroid/view/View;

    move-result-object v0

    :cond_2
    iget-object v1, p0, Lmaps/e/bm;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lmaps/e/bm;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lmaps/e/bm;->h:Landroid/view/ViewGroup;

    :cond_3
    iget-object v1, p0, Lmaps/e/bm;->c:Lmaps/c/m;

    invoke-static {p1}, Lmaps/e/bn;->a(Lmaps/e/bn;)Lmaps/au/af;

    move-result-object v2

    new-instance v3, Lmaps/ay/e;

    invoke-direct {v3, v0}, Lmaps/ay/e;-><init>(Landroid/view/View;)V

    invoke-interface {v1, v2, v3}, Lmaps/c/m;->a(Lmaps/ay/b;Lmaps/ay/e;)V

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_5
    move-object v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method static synthetic b(Lmaps/e/bm;)V
    .locals 0

    invoke-direct {p0}, Lmaps/e/bm;->c()V

    return-void
.end method

.method static synthetic b(Lmaps/e/bm;Lmaps/e/bn;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/e/bm;->a(Lmaps/e/bn;)V

    return-void
.end method

.method private b(Lmaps/e/bn;)Z
    .locals 2

    iget-object v1, p0, Lmaps/e/bm;->e:Lmaps/ay/o;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, Lmaps/e/bn;->a(Lmaps/e/bn;)Lmaps/au/af;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/af;->n()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Lmaps/ay/b;)Lmaps/e/bn;
    .locals 2

    instance-of v0, p1, Lmaps/au/af;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    check-cast p1, Lmaps/au/af;

    iget-object v0, p0, Lmaps/e/bm;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/au/af;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bn;

    goto :goto_0
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->c:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->f()V

    return-void
.end method

.method static synthetic c(Lmaps/e/bm;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    invoke-virtual {v0}, Lmaps/ay/r;->d()V

    return-void
.end method

.method static synthetic c(Lmaps/e/bm;Lmaps/e/bn;)V
    .locals 2

    invoke-static {p1}, Lmaps/e/bn;->a(Lmaps/e/bn;)Lmaps/au/af;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/af;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/e/bm;->e:Lmaps/ay/o;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, Lmaps/e/bn;->a(Lmaps/e/bn;)Lmaps/au/af;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/af;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/bm;->c:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->q()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d(Lmaps/e/bm;)Lmaps/ay/r;
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    return-object v0
.end method

.method static synthetic d(Lmaps/e/bm;Lmaps/e/bn;)Z
    .locals 1

    invoke-direct {p0, p1}, Lmaps/e/bm;->b(Lmaps/e/bn;)Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lmaps/e/bm;)Lmaps/e/n;
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->g:Lmaps/e/n;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lewo;
    .locals 13

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_0
    const-string v1, "no position in marker options"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "m"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lmaps/e/bm;->o:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/e/bm;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->f()Levo;

    move-result-object v0

    invoke-static {v0}, Lmaps/e/f;->a(Levo;)Lmaps/e/f;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->g()F

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->h()F

    move-result v5

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->m()F

    move-result v6

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->n()F

    move-result v7

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->i()Z

    move-result v8

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->j()Z

    move-result v9

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->k()Z

    move-result v10

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->l()F

    move-result v11

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->o()F

    move-result v12

    move-object v0, p0

    invoke-direct/range {v0 .. v12}, Lmaps/e/bm;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Lmaps/e/f;FFFFZZZFF)Lmaps/au/af;

    move-result-object v7

    new-instance v4, Lmaps/e/bn;

    iget-object v9, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    move-object v5, v1

    move-object v6, v3

    move-object v8, p0

    invoke-direct/range {v4 .. v9}, Lmaps/e/bn;-><init>(Ljava/lang/String;Lmaps/e/f;Lmaps/au/af;Lmaps/e/bm;Lmaps/h/a;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lmaps/e/bn;->a(Lmaps/e/bn;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lmaps/e/bn;->b(Lmaps/e/bn;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->g()F

    move-result v0

    invoke-static {v4, v0}, Lmaps/e/bn;->a(Lmaps/e/bn;F)F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->h()F

    move-result v0

    invoke-static {v4, v0}, Lmaps/e/bn;->b(Lmaps/e/bn;F)F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->m()F

    move-result v0

    invoke-static {v4, v0}, Lmaps/e/bn;->c(Lmaps/e/bn;F)F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->n()F

    move-result v0

    invoke-static {v4, v0}, Lmaps/e/bn;->d(Lmaps/e/bn;F)F

    iget-object v0, p0, Lmaps/e/bm;->f:Ljava/util/Map;

    invoke-virtual {v4}, Lmaps/e/bn;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    invoke-static {v4}, Lmaps/e/bn;->a(Lmaps/e/bn;)Lmaps/au/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ay/r;->a(Lmaps/au/af;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->g()F

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->g()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->h()F

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->h()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->h:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->m()F

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->m()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->n()F

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->n()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->m:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->f()Levo;

    move-result-object v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->f()Levo;

    move-result-object v1

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->g:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->d()Ljava/lang/String;

    move-result-object v1

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->e:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->e()Ljava/lang/String;

    move-result-object v1

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->f:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->i()Z

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->i()Z

    move-result v1

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->i:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_7
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->j()Z

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->j()Z

    move-result v1

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->j:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->k()Z

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->k()Z

    move-result v1

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->k:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->l()F

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->l()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_a

    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->l:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->o()F

    move-result v0

    sget-object v1, Lmaps/e/bm;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->o()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmaps/e/bm;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->n:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_b
    invoke-direct {p0}, Lmaps/e/bm;->c()V

    return-object v4

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 3

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bm;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/bn;

    iget-object v2, p0, Lmaps/e/bm;->g:Lmaps/e/n;

    invoke-static {v0}, Lmaps/e/bn;->c(Lmaps/e/bn;)Lmaps/e/f;

    move-result-object v0

    invoke-virtual {v2, v0}, Lmaps/e/n;->b(Lmaps/e/f;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/e/bm;->e:Lmaps/ay/o;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/e/bm;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    invoke-virtual {v0}, Lmaps/ay/r;->b()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Letn;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iput-object p1, p0, Lmaps/e/bm;->n:Letn;

    return-void
.end method

.method public final a(Leuc;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iput-object p1, p0, Lmaps/e/bm;->m:Leuc;

    return-void
.end method

.method public final a(Leur;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iput-object p1, p0, Lmaps/e/bm;->k:Leur;

    return-void
.end method

.method public final a(Leuu;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iput-object p1, p0, Lmaps/e/bm;->l:Leuu;

    return-void
.end method

.method public final a(Lmaps/au/af;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    invoke-direct {p0, p1}, Lmaps/e/bm;->c(Lmaps/ay/b;)Lmaps/e/bn;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lmaps/e/bm;->l:Leuu;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/e/bm;->l:Leuu;

    invoke-interface {v1, v0}, Leuu;->a(Lewo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lmaps/ay/b;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    invoke-direct {p0, p1}, Lmaps/e/bm;->c(Lmaps/ay/b;)Lmaps/e/bn;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v1, p0, Lmaps/e/bm;->k:Leur;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/e/bm;->k:Leur;

    invoke-interface {v1, v0}, Leur;->a(Lewo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-direct {p0, v0}, Lmaps/e/bm;->a(Lmaps/e/bn;)V

    iget-object v1, p0, Lmaps/e/bm;->b:Lmaps/c/l;

    invoke-static {v0}, Lmaps/e/bn;->a(Lmaps/e/bn;)Lmaps/au/af;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/af;->o()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/c/l;->a(Lmaps/ac/av;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b()Lmaps/ay/u;
    .locals 1

    iget-object v0, p0, Lmaps/e/bm;->d:Lmaps/ay/r;

    return-object v0
.end method

.method public final b(Lmaps/au/af;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    invoke-direct {p0, p1}, Lmaps/e/bm;->c(Lmaps/ay/b;)Lmaps/e/bn;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lmaps/e/bm;->l:Leuu;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/e/bm;->l:Leuu;

    invoke-interface {v1, v0}, Leuu;->b(Lewo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b(Lmaps/ay/b;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bm;->m:Leuc;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lmaps/e/bm;->c(Lmaps/ay/b;)Lmaps/e/bn;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/e/bm;->m:Leuc;

    invoke-interface {v1, v0}, Leuc;->a(Lewo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c(Lmaps/au/af;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bm;->j:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    invoke-direct {p0, p1}, Lmaps/e/bm;->c(Lmaps/ay/b;)Lmaps/e/bn;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lmaps/e/bm;->l:Leuu;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/e/bm;->l:Leuu;

    invoke-interface {v1, v0}, Leuu;->c(Lewo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
