.class final Lmaps/e/aj;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/e/ah;


# instance fields
.field private final a:Lmaps/bs/b;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;

.field private final d:Z

.field private e:Z

.field private f:Z

.field private g:J


# direct methods
.method constructor <init>(Lmaps/bs/b;Ljava/lang/String;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/aj;->c:Ljava/util/Map;

    iput-object p1, p0, Lmaps/e/aj;->a:Lmaps/bs/b;

    iput-object p2, p0, Lmaps/e/aj;->b:Ljava/lang/String;

    iput-boolean p3, p0, Lmaps/e/aj;->d:Z

    return-void
.end method

.method static synthetic a(Lmaps/e/aj;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/aj;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lmaps/e/aj;)J
    .locals 2

    iget-wide v0, p0, Lmaps/e/aj;->g:J

    return-wide v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lmaps/e/ai;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/aj;->e:Z

    const-string v1, "Action with name %s not started"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lmaps/e/aj;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lmaps/k/o;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lmaps/e/ai;

    invoke-direct {v0, p1}, Lmaps/e/ai;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/e/aj;->a:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v1

    iput-wide v1, v0, Lmaps/e/ai;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/e/aj;->e:Z

    if-nez v2, :cond_0

    :goto_0
    const-string v1, "Action with name %s already started"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lmaps/e/aj;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/e/aj;->a:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/e/aj;->g:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/aj;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/e/ai;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/e/aj;->e:Z

    const-string v3, "Action with name %s not started"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lmaps/e/aj;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lmaps/k/o;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lmaps/e/aj;->c:Ljava/util/Map;

    iget-object v3, p1, Lmaps/e/ai;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p1, :cond_1

    :goto_0
    const-string v1, "This event with name %s already ended"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lmaps/e/ai;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lmaps/k/o;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v0, p0, Lmaps/e/aj;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lmaps/e/aj;->c:Ljava/util/Map;

    iget-object v1, p1, Lmaps/e/ai;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/aj;->a:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iput-wide v0, p1, Lmaps/e/ai;->c:J

    iget-object v0, p0, Lmaps/e/aj;->c:Ljava/util/Map;

    iget-object v1, p1, Lmaps/e/ai;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lmaps/e/aj;->e:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lmaps/e/aj;->f:Z

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->b(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/aj;->f:Z

    iget-object v0, p0, Lmaps/e/aj;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/aj;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-boolean v1, p0, Lmaps/e/aj;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    :goto_1
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v1, Lmaps/e/ak;

    invoke-direct {v1, p0, v0}, Lmaps/e/ak;-><init>(Lmaps/e/aj;Ljava/util/List;)V

    invoke-virtual {v1}, Lmaps/e/ak;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
