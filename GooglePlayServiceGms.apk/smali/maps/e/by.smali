.class final Lmaps/e/by;
.super Lmaps/bn/c;


# instance fields
.field private a:I

.field private synthetic b:Lmaps/e/bx;


# direct methods
.method constructor <init>(Lmaps/e/bx;)V
    .locals 0

    iput-object p1, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    invoke-direct {p0}, Lmaps/bn/c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/DataOutput;)V
    .locals 3

    iget-object v1, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    invoke-static {v0}, Lmaps/e/bx;->a(Lmaps/e/bx;)Lmaps/e/bz;

    move-result-object v0

    sget-object v2, Lmaps/cm/b;->c:Lmaps/bv/c;

    invoke-virtual {v0, v2}, Lmaps/e/bz;->a(Lmaps/bv/c;)Lmaps/bv/a;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmaps/bv/a;->j(I)I

    move-result v2

    iput v2, p0, Lmaps/e/by;->a:I

    invoke-static {p1, v0}, Lmaps/bv/e;->a(Ljava/io/DataOutput;Lmaps/bv/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 3

    const/4 v2, 0x6

    const/4 v1, 0x1

    sget-object v0, Lmaps/cm/b;->e:Lmaps/bv/c;

    invoke-static {v0, p1}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/DataInput;)Lmaps/bv/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/bv/a;->d(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v1

    :pswitch_1
    const-string v0, "This application has been blocked by the Google Maps API. This might be because of an incorrectly registered key."

    invoke-static {v2, v0}, Lmaps/i/f;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    invoke-static {v0}, Lmaps/e/bx;->b(Lmaps/e/bx;)Lmaps/bn/k;

    move-result-object v0

    invoke-interface {v0}, Lmaps/bn/k;->p()V

    goto :goto_0

    :pswitch_2
    const-string v0, "This application has exceeded its quota for the Google Maps API."

    invoke-static {v2, v0}, Lmaps/i/f;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    invoke-static {v0}, Lmaps/e/bx;->b(Lmaps/e/bx;)Lmaps/bn/k;

    move-result-object v0

    invoke-interface {v0}, Lmaps/bn/k;->p()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x93

    return v0
.end method

.method public final d()V
    .locals 3

    iget-object v1, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    invoke-static {v0}, Lmaps/e/bx;->a(Lmaps/e/bx;)Lmaps/e/bz;

    move-result-object v0

    sget-object v2, Lmaps/cm/b;->c:Lmaps/bv/c;

    invoke-virtual {v0, v2}, Lmaps/e/bz;->a(Lmaps/bv/c;)Lmaps/bv/a;

    move-result-object v0

    iget v2, p0, Lmaps/e/by;->a:I

    invoke-static {v0, v2}, Lmaps/e/bx;->a(Lmaps/bv/a;I)V

    iget-object v2, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    invoke-static {v2}, Lmaps/e/bx;->a(Lmaps/e/bx;)Lmaps/e/bz;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/e/bz;->a(Lmaps/bv/a;)V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/e/by;->a:I

    iget-object v0, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    invoke-static {v0}, Lmaps/e/bx;->c(Lmaps/e/bx;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()V
    .locals 2

    iget-object v1, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lmaps/e/by;->a:I

    iget-object v0, p0, Lmaps/e/by;->b:Lmaps/e/bx;

    invoke-static {v0}, Lmaps/e/bx;->c(Lmaps/e/bx;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
