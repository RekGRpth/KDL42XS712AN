.class public final Lmaps/e/al;
.super Letl;

# interfaces
.implements Levj;


# static fields
.field private static synthetic L:Z

.field private static a:Lcom/google/android/gms/maps/model/CameraPosition;


# instance fields
.field private final A:Lmaps/ay/h;

.field private B:Levj;

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private final G:Lmaps/g/f;

.field private H:Lmaps/e/aw;

.field private I:Z

.field private J:Z

.field private final K:Lmaps/e/ca;

.field private final b:Lmaps/e/p;

.field private final c:Lmaps/c/l;

.field private final d:Lmaps/c/m;

.field private final e:Lmaps/e/bl;

.field private final f:Lmaps/e/br;

.field private final g:Lmaps/f/a;

.field private final h:Lmaps/e/bo;

.field private final i:Lmaps/e/av;

.field private final j:Lmaps/g/d;

.field private final k:Lmaps/g/a;

.field private final l:Lmaps/e/at;

.field private final m:Lmaps/e/bj;

.field private final n:Lmaps/i/g;

.field private final o:Landroid/view/View;

.field private final p:Lmaps/h/a;

.field private final q:Lcom/google/android/gms/maps/GoogleMapOptions;

.field private final r:Lmaps/e/ah;

.field private final s:Lmaps/ap/t;

.field private final t:Landroid/content/res/Resources;

.field private final u:Lmaps/c/c;

.field private v:Lmaps/ay/u;

.field private w:Lmaps/ay/u;

.field private x:Lmaps/ay/u;

.field private y:I

.field private z:Lmaps/c/h;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/16 v1, 0x0

    const-class v0, Lmaps/e/al;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/e/al;->L:Z

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, v1, v2, v1, v2}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0}, Lcom/google/android/gms/maps/model/CameraPosition;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    sput-object v0, Lmaps/e/al;->a:Lcom/google/android/gms/maps/model/CameraPosition;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/view/View;Lmaps/c/m;Lmaps/c/l;Lmaps/e/bl;Lmaps/f/a;Lmaps/e/bo;Lmaps/e/br;Lmaps/e/av;Lmaps/e/p;Lmaps/g/d;Lmaps/g/a;Lmaps/e/at;Lmaps/e/bj;Lmaps/i/g;Lmaps/h/a;Lcom/google/android/gms/maps/GoogleMapOptions;Lmaps/e/ah;Landroid/content/res/Resources;Lmaps/ap/t;Lmaps/c/c;Lmaps/e/ca;Lmaps/ay/h;)V
    .locals 2

    invoke-direct {p0}, Letl;-><init>()V

    const/4 v1, 0x1

    iput v1, p0, Lmaps/e/al;->y:I

    const/4 v1, 0x0

    iput v1, p0, Lmaps/e/al;->C:I

    const/4 v1, 0x0

    iput v1, p0, Lmaps/e/al;->D:I

    const/4 v1, 0x0

    iput v1, p0, Lmaps/e/al;->E:I

    const/4 v1, 0x0

    iput v1, p0, Lmaps/e/al;->F:I

    new-instance v1, Lmaps/e/am;

    invoke-direct {v1, p0}, Lmaps/e/am;-><init>(Lmaps/e/al;)V

    iput-object v1, p0, Lmaps/e/al;->G:Lmaps/g/f;

    iput-object p1, p0, Lmaps/e/al;->o:Landroid/view/View;

    iput-object p2, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iput-object p3, p0, Lmaps/e/al;->c:Lmaps/c/l;

    iput-object p4, p0, Lmaps/e/al;->e:Lmaps/e/bl;

    iput-object p5, p0, Lmaps/e/al;->g:Lmaps/f/a;

    iput-object p6, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    iput-object p7, p0, Lmaps/e/al;->f:Lmaps/e/br;

    iput-object p8, p0, Lmaps/e/al;->i:Lmaps/e/av;

    iput-object p9, p0, Lmaps/e/al;->b:Lmaps/e/p;

    iput-object p10, p0, Lmaps/e/al;->j:Lmaps/g/d;

    iput-object p11, p0, Lmaps/e/al;->k:Lmaps/g/a;

    iput-object p12, p0, Lmaps/e/al;->l:Lmaps/e/at;

    iput-object p13, p0, Lmaps/e/al;->m:Lmaps/e/bj;

    move-object/from16 v0, p14

    iput-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    move-object/from16 v0, p15

    iput-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    move-object/from16 v0, p16

    iput-object v0, p0, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    move-object/from16 v0, p17

    iput-object v0, p0, Lmaps/e/al;->r:Lmaps/e/ah;

    move-object/from16 v0, p19

    iput-object v0, p0, Lmaps/e/al;->s:Lmaps/ap/t;

    move-object/from16 v0, p18

    iput-object v0, p0, Lmaps/e/al;->t:Landroid/content/res/Resources;

    move-object/from16 v0, p20

    iput-object v0, p0, Lmaps/e/al;->u:Lmaps/c/c;

    move-object/from16 v0, p21

    iput-object v0, p0, Lmaps/e/al;->K:Lmaps/e/ca;

    move-object/from16 v0, p22

    iput-object v0, p0, Lmaps/e/al;->A:Lmaps/ay/h;

    return-void
.end method

.method private B()F
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->c:Lmaps/c/l;

    iget-object v1, p0, Lmaps/e/al;->c:Lmaps/c/l;

    invoke-virtual {v1}, Lmaps/c/l;->c()Lmaps/ar/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/c/l;->b(Lmaps/ac/av;)F

    move-result v0

    return v0
.end method

.method private C()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->u:Lmaps/c/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/al;->u:Lmaps/c/c;

    invoke-virtual {v0}, Lmaps/c/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;Z)Lmaps/e/al;
    .locals 34

    const-string v11, ""

    invoke-static/range {p1 .. p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lmaps/be/g;->d()Lmaps/be/b;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lmaps/be/b;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    new-instance v26, Lmaps/e/aj;

    new-instance v4, Lmaps/bs/b;

    invoke-direct {v4}, Lmaps/bs/b;-><init>()V

    const-string v5, "map_start_up"

    move-object/from16 v0, v26

    invoke-direct {v0, v4, v5, v3}, Lmaps/e/aj;-><init>(Lmaps/bs/b;Ljava/lang/String;Z)V

    invoke-interface/range {v26 .. v26}, Lmaps/e/ah;->a()V

    const-string v3, "init"

    move-object/from16 v0, v26

    invoke-interface {v0, v3}, Lmaps/e/ah;->a(Ljava/lang/String;)Lmaps/e/ai;

    move-result-object v32

    const-string v3, "map_load"

    move-object/from16 v0, v26

    invoke-interface {v0, v3}, Lmaps/e/ah;->a(Ljava/lang/String;)Lmaps/e/ai;

    move-result-object v12

    invoke-static {}, Lmaps/e/be;->a()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lmaps/e/ay;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v14

    new-instance v10, Landroid/widget/FrameLayout;

    invoke-direct {v10, v14}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lmaps/h/c;->b()Lmaps/h/a;

    move-result-object v8

    invoke-static {}, Lmaps/e/bx;->a()Lmaps/e/bx;

    move-result-object v33

    new-instance v4, Lmaps/e/bz;

    invoke-virtual {v14}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "com.google.android.gms.maps._m_u"

    invoke-direct {v4, v5, v6}, Lmaps/e/bz;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Lmaps/e/bx;->a(Lmaps/e/bz;)V

    new-instance v17, Lmaps/ay/au;

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lmaps/ay/au;-><init>(Landroid/content/res/Resources;)V

    invoke-static/range {p1 .. p1}, Lmaps/e/al;->a(Lcom/google/android/gms/maps/GoogleMapOptions;)Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    move v6, v4

    :goto_1
    if-eqz p2, :cond_6

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_6

    invoke-static {}, Lmaps/bb/a;->c()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-static {}, Lmaps/be/g;->c()Lmaps/be/l;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/be/l;->d()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {}, Lmaps/be/g;->c()Lmaps/be/l;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/be/l;->e()Z

    move-result v4

    move v9, v4

    :goto_2
    if-eqz v9, :cond_7

    new-instance v5, Lmaps/c/i;

    invoke-direct {v5, v14, v3}, Lmaps/c/i;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-virtual {v5}, Lmaps/c/i;->c()V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lmaps/c/i;->a(Lmaps/ay/au;)V

    invoke-virtual {v5, v6}, Lmaps/c/i;->a(Z)V

    :cond_0
    :goto_3
    invoke-virtual {v10, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    new-instance v4, Lmaps/c/l;

    move-object/from16 v0, v17

    invoke-direct {v4, v0, v5}, Lmaps/c/l;-><init>(Lmaps/ay/au;Lmaps/c/m;)V

    invoke-virtual {v4}, Lmaps/c/l;->d()V

    new-instance v22, Landroid/os/Handler;

    invoke-direct/range {v22 .. v22}, Landroid/os/Handler;-><init>()V

    new-instance v21, Lmaps/e/an;

    invoke-direct/range {v21 .. v22}, Lmaps/e/an;-><init>(Landroid/os/Handler;)V

    move-object/from16 v0, v21

    invoke-static {v5, v4, v0}, Lmaps/e/q;->a(Lmaps/c/m;Lmaps/c/l;Ljava/util/concurrent/Executor;)Lmaps/e/q;

    move-result-object v18

    new-instance v19, Lmaps/g/d;

    move-object/from16 v0, v19

    invoke-direct {v0, v14, v3}, Lmaps/g/d;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-virtual/range {v19 .. v19}, Lmaps/g/d;->a()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v10, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    new-instance v20, Lmaps/g/a;

    move-object/from16 v0, v20

    invoke-direct {v0, v14, v3}, Lmaps/g/a;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-virtual/range {v20 .. v20}, Lmaps/g/a;->a()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v10, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    invoke-static {v5, v4, v14, v3, v8}, Lmaps/e/bm;->a(Lmaps/c/m;Lmaps/c/l;Landroid/content/Context;Landroid/content/res/Resources;Lmaps/h/a;)Lmaps/e/bm;

    move-result-object v13

    invoke-static {v5, v8}, Lmaps/e/bs;->a(Lmaps/c/m;Lmaps/h/a;)Lmaps/e/br;

    move-result-object v16

    invoke-static {v14}, Lmaps/f/b;->a(Landroid/content/Context;)Lmaps/f/b;

    move-result-object v7

    invoke-virtual/range {v20 .. v20}, Lmaps/g/a;->c()Lmaps/g/c;

    move-result-object v6

    invoke-static/range {v3 .. v8}, Lmaps/e/bp;->a(Landroid/content/res/Resources;Lmaps/c/l;Lmaps/c/m;Lmaps/g/c;Letq;Lmaps/h/a;)Lmaps/e/bp;

    move-result-object v15

    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    move-result-object v6

    if-eqz v6, :cond_8

    move-object/from16 v0, v21

    invoke-static {v6, v0, v8}, Lmaps/c/c;->a(Lmaps/ab/q;Ljava/util/concurrent/Executor;Lmaps/h/a;)Lmaps/c/c;

    move-result-object v29

    :goto_4
    new-instance v28, Lmaps/ap/t;

    invoke-static {}, Lmaps/bn/d;->a()Lmaps/bn/d;

    move-result-object v21

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lmaps/ap/t;-><init>(Lmaps/bn/k;)V

    new-instance v21, Lmaps/e/ao;

    move-object/from16 v0, v21

    invoke-direct {v0, v7}, Lmaps/e/ao;-><init>(Lmaps/f/b;)V

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lmaps/ap/t;->a(Lmaps/ap/v;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lmaps/ay/au;->a(Lmaps/ap/t;)V

    new-instance v30, Lmaps/e/ca;

    invoke-virtual/range {v19 .. v19}, Lmaps/g/d;->a()Landroid/view/View;

    move-result-object v17

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v5, v9, v2}, Lmaps/e/ca;-><init>(Landroid/view/View;Lmaps/c/m;ZLandroid/os/Handler;)V

    new-instance v17, Lmaps/e/ap;

    move-object/from16 v0, v17

    invoke-direct {v0, v11, v6}, Lmaps/e/ap;-><init>(Ljava/lang/String;Lmaps/ab/q;)V

    invoke-static {v11}, Lmaps/k/aa;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    sget-object v6, Lmaps/ao/b;->a:Lmaps/ao/b;

    invoke-interface {v5}, Lmaps/c/m;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-interface {v0, v6, v9}, Lmaps/e/av;->a(Lmaps/ao/b;Landroid/content/res/Resources;)Lmaps/ay/u;

    move-result-object v6

    check-cast v6, Lmaps/ay/ap;

    invoke-interface {v5, v6}, Lmaps/c/m;->a(Lmaps/ay/ap;)V

    sget-object v6, Lmaps/ao/b;->j:Lmaps/ao/b;

    invoke-static {v6, v14, v3}, Lmaps/ap/p;->a(Lmaps/ao/b;Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/ae/y;

    sget-object v6, Lmaps/ao/b;->m:Lmaps/ao/b;

    invoke-static {v6, v14, v3}, Lmaps/ap/p;->a(Lmaps/ao/b;Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/ae/y;

    :cond_1
    new-instance v21, Lmaps/e/at;

    invoke-direct/range {v21 .. v21}, Lmaps/e/at;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-static {v5, v0, v1, v12}, Lmaps/e/bj;->a(Lmaps/c/m;Landroid/os/Handler;Lmaps/e/ah;Lmaps/e/ai;)Lmaps/e/bj;

    move-result-object v22

    invoke-static {}, Lmaps/i/g;->a()Lmaps/i/g;

    move-result-object v23

    sget-object v6, Lmaps/ay/k;->a:Lmaps/ay/k;

    new-instance v31, Lmaps/ay/h;

    move-object/from16 v0, v31

    invoke-direct {v0, v3, v6}, Lmaps/ay/h;-><init>(Landroid/content/res/Resources;Lmaps/ay/k;)V

    new-instance v9, Lmaps/e/al;

    move-object v11, v5

    move-object v12, v4

    move-object v14, v7

    move-object/from16 v24, v8

    move-object/from16 v25, p1

    move-object/from16 v27, v3

    invoke-direct/range {v9 .. v31}, Lmaps/e/al;-><init>(Landroid/view/View;Lmaps/c/m;Lmaps/c/l;Lmaps/e/bl;Lmaps/f/a;Lmaps/e/bo;Lmaps/e/br;Lmaps/e/av;Lmaps/e/p;Lmaps/g/d;Lmaps/g/a;Lmaps/e/at;Lmaps/e/bj;Lmaps/i/g;Lmaps/h/a;Lcom/google/android/gms/maps/GoogleMapOptions;Lmaps/e/ah;Landroid/content/res/Resources;Lmaps/ap/t;Lmaps/c/c;Lmaps/e/ca;Lmaps/ay/h;)V

    iget-object v3, v9, Lmaps/e/al;->o:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    iget-object v3, v9, Lmaps/e/al;->t:Landroid/content/res/Resources;

    invoke-static {v3}, Lmaps/ba/f;->a(Landroid/content/res/Resources;)Z

    move-result v3

    iget-object v4, v9, Lmaps/e/al;->d:Lmaps/c/m;

    if-eqz v3, :cond_9

    sget-object v3, Lmaps/av/a;->u:Lmaps/av/a;

    :goto_5
    invoke-interface {v4, v3}, Lmaps/c/m;->a(Lmaps/av/a;)V

    iget-object v3, v9, Lmaps/e/al;->A:Lmaps/ay/h;

    invoke-virtual {v3}, Lmaps/ay/h;->b()V

    iget-object v3, v9, Lmaps/e/al;->A:Lmaps/ay/h;

    new-instance v4, Lmaps/e/aq;

    invoke-direct {v4, v9}, Lmaps/e/aq;-><init>(Lmaps/e/al;)V

    invoke-virtual {v3, v4}, Lmaps/ay/h;->a(Lmaps/ay/w;)V

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->o()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_a

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->o()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v9, v3}, Lmaps/e/al;->f(Z)V

    :goto_6
    iget-object v3, v9, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v4, v9, Lmaps/e/al;->i:Lmaps/e/av;

    sget-object v5, Lmaps/ao/b;->o:Lmaps/ao/b;

    iget-object v6, v9, Lmaps/e/al;->t:Landroid/content/res/Resources;

    invoke-interface {v4, v5, v6}, Lmaps/e/av;->a(Lmaps/ao/b;Landroid/content/res/Resources;)Lmaps/ay/u;

    move-result-object v4

    invoke-interface {v3, v4}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    iget-object v3, v9, Lmaps/e/al;->i:Lmaps/e/av;

    iget-object v4, v9, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v4}, Lmaps/c/m;->v()Lmaps/ap/f;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ap/f;->h()Lmaps/ay/ap;

    move-result-object v4

    iget-object v5, v9, Lmaps/e/al;->t:Landroid/content/res/Resources;

    invoke-interface {v3, v4, v5}, Lmaps/e/av;->a(Lmaps/ay/ap;Landroid/content/res/Resources;)Lmaps/ay/u;

    move-result-object v3

    iput-object v3, v9, Lmaps/e/al;->w:Lmaps/ay/u;

    iget-object v3, v9, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v4, v9, Lmaps/e/al;->w:Lmaps/ay/u;

    invoke-interface {v3, v4}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    const/4 v3, 0x1

    invoke-direct {v9, v3}, Lmaps/e/al;->m(Z)V

    iget-object v3, v9, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v4, v9, Lmaps/e/al;->e:Lmaps/e/bl;

    invoke-interface {v4}, Lmaps/e/bl;->b()Lmaps/ay/u;

    move-result-object v4

    invoke-interface {v3, v4}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    iget-object v3, v9, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v4, v9, Lmaps/e/al;->f:Lmaps/e/br;

    invoke-interface {v4}, Lmaps/e/br;->b()Lmaps/ay/u;

    move-result-object v4

    invoke-interface {v3, v4}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->n()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->n()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v9, v3}, Lmaps/e/al;->e(Z)V

    :goto_7
    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->l()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->l()I

    move-result v3

    invoke-virtual {v9, v3}, Lmaps/e/al;->a(I)V

    :cond_2
    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->q()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_c

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->q()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v9, v3}, Lmaps/e/al;->i(Z)V

    :goto_8
    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->p()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_d

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->p()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v9, v3}, Lmaps/e/al;->h(Z)V

    :goto_9
    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->r()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_e

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->r()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v9, v3}, Lmaps/e/al;->j(Z)V

    :goto_a
    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->s()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_f

    iget-object v3, v9, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMapOptions;->s()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v9, v3}, Lmaps/e/al;->k(Z)V

    :goto_b
    const/4 v3, 0x1

    invoke-direct {v9, v3}, Lmaps/e/al;->p(Z)V

    sget-object v3, Lmaps/h/b;->a:Lmaps/h/b;

    invoke-interface {v8, v3}, Lmaps/h/a;->a(Lmaps/h/b;)V

    invoke-virtual/range {v33 .. v33}, Lmaps/e/bx;->b()V

    move-object/from16 v0, v26

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lmaps/e/ah;->a(Lmaps/e/ai;)V

    return-object v9

    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_1

    :cond_5
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_6

    const/4 v4, 0x1

    move v9, v4

    goto/16 :goto_2

    :cond_6
    const/4 v4, 0x0

    move v9, v4

    goto/16 :goto_2

    :cond_7
    new-instance v5, Lmaps/c/n;

    invoke-direct {v5, v14, v3}, Lmaps/c/n;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-virtual {v5}, Lmaps/c/n;->c()V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lmaps/c/n;->a(Lmaps/ay/au;)V

    invoke-virtual {v5, v6}, Lmaps/c/n;->a(Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/maps/GoogleMapOptions;->j()Ljava/lang/Boolean;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/maps/GoogleMapOptions;->j()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v4}, Lmaps/c/n;->setZOrderOnTop(Z)V

    goto/16 :goto_3

    :cond_8
    const/16 v29, 0x0

    goto/16 :goto_4

    :cond_9
    sget-object v3, Lmaps/av/a;->t:Lmaps/av/a;

    goto/16 :goto_5

    :cond_a
    const/4 v3, 0x1

    invoke-direct {v9, v3}, Lmaps/e/al;->o(Z)V

    goto/16 :goto_6

    :cond_b
    const/4 v3, 0x1

    invoke-direct {v9, v3}, Lmaps/e/al;->n(Z)V

    goto/16 :goto_7

    :cond_c
    const/4 v3, 0x1

    invoke-direct {v9, v3}, Lmaps/e/al;->r(Z)V

    goto/16 :goto_8

    :cond_d
    const/4 v3, 0x1

    invoke-direct {v9, v3}, Lmaps/e/al;->q(Z)V

    goto/16 :goto_9

    :cond_e
    const/4 v3, 0x1

    invoke-direct {v9, v3}, Lmaps/e/al;->s(Z)V

    goto/16 :goto_a

    :cond_f
    const/4 v3, 0x1

    invoke-direct {v9, v3}, Lmaps/e/al;->t(Z)V

    goto :goto_b
.end method

.method static synthetic a(Lmaps/e/al;)Lmaps/h/a;
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    return-object v0
.end method

.method static synthetic a(Lmaps/ay/ap;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lmaps/k/aa;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ac/e;

    invoke-direct {v0}, Lmaps/ac/e;-><init>()V

    invoke-virtual {v0, p1}, Lmaps/ac/e;->a(Ljava/lang/String;)Lmaps/ac/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/e;->a()Lmaps/ac/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ay/ap;->a(Lmaps/ac/bw;)Z

    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/gms/maps/GoogleMapOptions;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/maps/GoogleMapOptions;->k()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/maps/GoogleMapOptions;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/e/al;)Lmaps/c/l;
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->c:Lmaps/c/l;

    return-object v0
.end method

.method static synthetic c(Lmaps/e/al;)F
    .locals 1

    invoke-direct {p0}, Lmaps/e/al;->B()F

    move-result v0

    return v0
.end method

.method static synthetic d(Lmaps/e/al;)F
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->c:Lmaps/c/l;

    invoke-virtual {v0}, Lmaps/c/l;->e()F

    move-result v0

    return v0
.end method

.method static synthetic e(Lmaps/e/al;)Lmaps/e/ca;
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->K:Lmaps/e/ca;

    return-object v0
.end method

.method private m(Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/e/al;->u:Lmaps/c/c;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/e/al;->C()Z

    move-result v0

    if-eq v0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/e/al;->u:Lmaps/c/c;

    invoke-virtual {v0}, Lmaps/c/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/al;->k:Lmaps/g/a;

    invoke-virtual {v0}, Lmaps/g/a;->d()Lmaps/c/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmaps/c/a;->a(I)V

    iget-object v0, p0, Lmaps/e/al;->k:Lmaps/g/a;

    invoke-virtual {v0}, Lmaps/g/a;->d()Lmaps/c/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/al;->u:Lmaps/c/c;

    invoke-virtual {v0, v1}, Lmaps/c/a;->a(Lmaps/c/c;)V

    iget-object v0, p0, Lmaps/e/al;->i:Lmaps/e/av;

    iget-object v1, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v1}, Lmaps/c/m;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/al;->t:Landroid/content/res/Resources;

    invoke-interface {v0, v1, v2}, Lmaps/e/av;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/c/h;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/al;->z:Lmaps/c/h;

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/al;->z:Lmaps/c/h;

    invoke-virtual {v1}, Lmaps/c/h;->a()Lmaps/ay/u;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    iget-object v0, p0, Lmaps/e/al;->s:Lmaps/ap/t;

    new-instance v1, Lmaps/e/au;

    iget-object v2, p0, Lmaps/e/al;->u:Lmaps/c/c;

    invoke-direct {v1, v2, v3}, Lmaps/e/au;-><init>(Lmaps/c/c;B)V

    invoke-virtual {v0, v1}, Lmaps/ap/t;->a(Lmaps/ap/u;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/e/al;->s:Lmaps/ap/t;

    invoke-virtual {v0, v2}, Lmaps/ap/t;->a(Lmaps/ap/u;)V

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/al;->z:Lmaps/c/h;

    invoke-virtual {v1}, Lmaps/c/h;->a()Lmaps/ay/u;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/c/m;->b(Lmaps/ay/u;)V

    iget-object v0, p0, Lmaps/e/al;->z:Lmaps/c/h;

    invoke-virtual {v0}, Lmaps/c/h;->b()V

    iput-object v2, p0, Lmaps/e/al;->z:Lmaps/c/h;

    iget-object v0, p0, Lmaps/e/al;->k:Lmaps/g/a;

    invoke-virtual {v0}, Lmaps/g/a;->d()Lmaps/c/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/c/a;->a(Lmaps/c/c;)V

    iget-object v0, p0, Lmaps/e/al;->k:Lmaps/g/a;

    invoke-virtual {v0}, Lmaps/g/a;->d()Lmaps/c/a;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lmaps/c/a;->a(I)V

    iget-object v0, p0, Lmaps/e/al;->u:Lmaps/c/c;

    invoke-virtual {v0}, Lmaps/c/c;->b()V

    goto :goto_0
.end method

.method private n(Z)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lmaps/e/al;->I:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/e/al;->I:Z

    iget-object v0, p0, Lmaps/e/al;->k:Lmaps/g/a;

    invoke-virtual {v0}, Lmaps/g/a;->b()Lmaps/g/e;

    move-result-object v0

    if-eqz p1, :cond_1

    new-instance v1, Lmaps/e/aw;

    iget-object v2, p0, Lmaps/e/al;->c:Lmaps/c/l;

    invoke-direct {v1, v2, v0}, Lmaps/e/aw;-><init>(Lmaps/c/l;Lmaps/g/e;)V

    iput-object v1, p0, Lmaps/e/al;->H:Lmaps/e/aw;

    iget-object v1, p0, Lmaps/e/al;->H:Lmaps/e/aw;

    invoke-virtual {p0}, Lmaps/e/al;->a()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/e/aw;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    iget-object v1, p0, Lmaps/e/al;->b:Lmaps/e/p;

    iget-object v2, p0, Lmaps/e/al;->H:Lmaps/e/aw;

    invoke-interface {v1, v2}, Lmaps/e/p;->a(Letz;)V

    iget-object v1, p0, Lmaps/e/al;->G:Lmaps/g/f;

    invoke-virtual {v0, v1}, Lmaps/g/e;->a(Lmaps/g/f;)V

    :goto_0
    invoke-virtual {v0, p1}, Lmaps/g/e;->a(Z)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0, v3}, Lmaps/g/e;->a(Lmaps/g/f;)V

    iget-object v1, p0, Lmaps/e/al;->b:Lmaps/e/p;

    iget-object v2, p0, Lmaps/e/al;->H:Lmaps/e/aw;

    invoke-interface {v1, v2}, Lmaps/e/p;->b(Letz;)V

    iput-object v3, p0, Lmaps/e/al;->H:Lmaps/e/aw;

    goto :goto_0
.end method

.method private o(Z)V
    .locals 2

    iget-boolean v0, p0, Lmaps/e/al;->J:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/e/al;->J:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/al;->A:Lmaps/ay/h;

    invoke-interface {v0, v1}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/al;->A:Lmaps/ay/h;

    invoke-interface {v0, v1}, Lmaps/c/m;->b(Lmaps/ay/u;)V

    goto :goto_0
.end method

.method private p(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0, p1}, Lmaps/e/bo;->a(Z)V

    return-void
.end method

.method private q(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0, p1}, Lmaps/c/m;->b(Z)V

    return-void
.end method

.method private r(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0, p1}, Lmaps/c/m;->c(Z)V

    return-void
.end method

.method private s(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0, p1}, Lmaps/c/m;->d(Z)V

    return-void
.end method

.method private t(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0, p1}, Lmaps/c/m;->e(Z)V

    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {v0}, Lmaps/e/al;->a(Lcom/google/android/gms/maps/GoogleMapOptions;)Z

    move-result v0

    return v0
.end method

.method public final a()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->b:Lmaps/e/p;

    invoke-interface {v0}, Lmaps/e/p;->c()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/CircleOptions;)Lewi;
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->J:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->f:Lmaps/e/br;

    invoke-interface {v0, p1}, Lmaps/e/br;->a(Lcom/google/android/gms/maps/model/CircleOptions;)Lewi;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lewl;
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->S:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->f:Lmaps/e/br;

    invoke-interface {v0, p1}, Lmaps/e/br;->a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lewl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lewo;
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->b:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->e:Lmaps/e/bl;

    invoke-interface {v0, p1}, Lmaps/e/bl;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lewo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/PolygonOptions;)Lewr;
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->z:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->f:Lmaps/e/br;

    invoke-interface {v0, p1}, Lmaps/e/br;->a(Lcom/google/android/gms/maps/model/PolygonOptions;)Lewr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lewu;
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->r:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->f:Lmaps/e/br;

    invoke-interface {v0, p1}, Lmaps/e/br;->a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lewu;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lewx;
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ab:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->f:Lmaps/e/br;

    invoke-interface {v0, p1}, Lmaps/e/br;->a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lewx;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v3, Lmaps/h/b;->ay:Lmaps/h/b;

    invoke-interface {v0, v3}, Lmaps/h/a;->b(Lmaps/h/b;)V

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    sget-object v3, Lmaps/ao/b;->a:Lmaps/ao/b;

    sget-object v0, Lmaps/ap/b;->a:Lmaps/ap/b;

    move-object v4, v3

    move-object v3, v0

    move v0, v1

    :goto_0
    iget-object v6, p0, Lmaps/e/al;->x:Lmaps/ay/u;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v7, p0, Lmaps/e/al;->x:Lmaps/ay/u;

    invoke-interface {v6, v7}, Lmaps/c/m;->b(Lmaps/ay/u;)V

    iput-object v5, p0, Lmaps/e/al;->x:Lmaps/ay/u;

    :cond_0
    if-eqz v4, :cond_1

    sget-object v5, Lmaps/ao/b;->a:Lmaps/ao/b;

    if-eq v4, v5, :cond_1

    iget-object v5, p0, Lmaps/e/al;->i:Lmaps/e/av;

    iget-object v6, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v6}, Lmaps/c/m;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Lmaps/e/av;->b(Lmaps/ao/b;Landroid/content/res/Resources;)Lmaps/ay/u;

    move-result-object v5

    iput-object v5, p0, Lmaps/e/al;->x:Lmaps/ay/u;

    iget-object v5, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v6, p0, Lmaps/e/al;->x:Lmaps/ay/u;

    invoke-interface {v5, v6}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    :cond_1
    iget-object v5, p0, Lmaps/e/al;->d:Lmaps/c/m;

    if-eqz v4, :cond_2

    :goto_1
    invoke-interface {v5, v2}, Lmaps/c/m;->f(Z)V

    iget-object v1, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v1, v3}, Lmaps/c/m;->a(Lmaps/ap/b;)V

    iget-object v1, p0, Lmaps/e/al;->j:Lmaps/g/d;

    invoke-virtual {v1, v0}, Lmaps/g/d;->a(Z)V

    iput p1, p0, Lmaps/e/al;->y:I

    return-void

    :pswitch_1
    sget-object v0, Lmaps/ap/b;->e:Lmaps/ap/b;

    move-object v3, v0

    move-object v4, v5

    move v0, v1

    goto :goto_0

    :pswitch_2
    sget-object v3, Lmaps/ao/b;->d:Lmaps/ao/b;

    sget-object v0, Lmaps/ap/b;->e:Lmaps/ap/b;

    move-object v4, v3

    move-object v3, v0

    move v0, v2

    goto :goto_0

    :pswitch_3
    sget-object v3, Lmaps/ao/b;->d:Lmaps/ao/b;

    sget-object v0, Lmaps/ap/b;->b:Lmaps/ap/b;

    move-object v4, v3

    move-object v3, v0

    move v0, v2

    goto :goto_0

    :pswitch_4
    sget-object v3, Lmaps/ao/b;->e:Lmaps/ao/b;

    sget-object v0, Lmaps/ap/b;->d:Lmaps/ap/b;

    move-object v4, v3

    move-object v3, v0

    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final a(IIII)V
    .locals 3

    const/4 v0, 0x0

    if-gez p1, :cond_0

    move p1, v0

    :cond_0
    if-gez p3, :cond_1

    move p3, v0

    :cond_1
    if-gez p2, :cond_2

    move p2, v0

    :cond_2
    if-gez p4, :cond_3

    move p4, v0

    :cond_3
    iput p1, p0, Lmaps/e/al;->C:I

    iput p2, p0, Lmaps/e/al;->D:I

    iput p3, p0, Lmaps/e/al;->E:I

    iput p4, p0, Lmaps/e/al;->F:I

    iget-object v1, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v1}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v2, Lmaps/h/b;->bs:Lmaps/h/b;

    invoke-interface {v1, v2}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/al;->c:Lmaps/c/l;

    invoke-virtual {v1, p1, p2, p3, p4}, Lmaps/c/l;->a(IIII)V

    iget-object v1, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v1, p3, p4}, Lmaps/c/m;->a(II)V

    iget-object v1, p0, Lmaps/e/al;->k:Lmaps/g/a;

    invoke-virtual {v1, p1, p2, p3, p4}, Lmaps/g/a;->a(IIII)V

    iget-object v1, p0, Lmaps/e/al;->j:Lmaps/g/d;

    invoke-virtual {v1, p1, p2, p3, p4}, Lmaps/g/d;->a(IIII)V

    iget-object v1, p0, Lmaps/e/al;->A:Lmaps/ay/h;

    invoke-virtual {v1, p1, p2, p3}, Lmaps/ay/h;->a(III)V

    iget-object v1, p0, Lmaps/e/al;->d:Lmaps/c/m;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v0}, Lmaps/c/m;->a(ZZ)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/e/al;->r:Lmaps/e/ah;

    const-string v1, "on_create"

    invoke-interface {v0, v1}, Lmaps/e/ah;->a(Ljava/lang/String;)Lmaps/e/ai;

    move-result-object v1

    const-string v0, "camera"

    invoke-static {p1, v0}, Levm;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/CameraPosition;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->m()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->m()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    :cond_0
    :goto_0
    iget-object v2, p0, Lmaps/e/al;->c:Lmaps/c/l;

    invoke-static {v0}, Lmaps/i/a;->a(Lcom/google/android/gms/maps/model/CameraPosition;)Lmaps/ar/b;

    move-result-object v0

    invoke-virtual {v2, v0, v3, v3}, Lmaps/c/l;->a(Lmaps/ar/c;II)V

    iget-object v0, p0, Lmaps/e/al;->r:Lmaps/e/ah;

    invoke-interface {v0, v1}, Lmaps/e/ah;->a(Lmaps/e/ai;)V

    return-void

    :cond_1
    sget-object v0, Lmaps/e/al;->a:Lcom/google/android/gms/maps/model/CameraPosition;

    goto :goto_0
.end method

.method public final a(Lcrv;)V
    .locals 5

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ak:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/af;

    iget-object v1, p0, Lmaps/e/al;->b:Lmaps/e/p;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lmaps/e/al;->p:Lmaps/h/a;

    invoke-interface {v1, v0, v2, v3, v4}, Lmaps/e/p;->a(Lmaps/e/af;ILete;Lmaps/h/a;)V

    return-void
.end method

.method public final a(Lcrv;ILete;)V
    .locals 3

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aj:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/af;

    if-lez p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "durationMs must be positive"

    invoke-static {v1, v2}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lmaps/e/al;->b:Lmaps/e/p;

    iget-object v2, p0, Lmaps/e/al;->p:Lmaps/h/a;

    invoke-interface {v1, v0, p2, p3, v2}, Lmaps/e/p;->a(Lmaps/e/af;ILete;Lmaps/h/a;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Lcrv;Lete;)V
    .locals 4

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ai:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/af;

    iget-object v1, p0, Lmaps/e/al;->b:Lmaps/e/p;

    const/4 v2, -0x1

    iget-object v3, p0, Lmaps/e/al;->p:Lmaps/h/a;

    invoke-interface {v1, v0, v2, p2, v3}, Lmaps/e/p;->a(Lmaps/e/af;ILete;Lmaps/h/a;)V

    return-void
.end method

.method public final a(Letn;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->q:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->e:Lmaps/e/bl;

    invoke-interface {v0, p1}, Lmaps/e/bl;->a(Letn;)V

    return-void
.end method

.method public final a(Letq;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aG:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :goto_0
    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0, p1}, Lmaps/e/bo;->a(Letq;)V

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aF:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    goto :goto_0
.end method

.method public final a(Letz;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aK:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->b:Lmaps/e/p;

    invoke-interface {v0, p1}, Lmaps/e/p;->c(Letz;)V

    return-void
.end method

.method public final a(Leuc;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aJ:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->e:Lmaps/e/bl;

    invoke-interface {v0, p1}, Lmaps/e/bl;->a(Leuc;)V

    return-void
.end method

.method public final a(Leui;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aL:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0, p1}, Lmaps/c/m;->a(Leui;)V

    return-void
.end method

.method public final a(Leul;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aP:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->m:Lmaps/e/bj;

    invoke-virtual {v0, p1}, Lmaps/e/bj;->a(Leul;)V

    return-void
.end method

.method public final a(Leuo;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aM:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0, p1}, Lmaps/c/m;->a(Leuo;)V

    return-void
.end method

.method public final a(Leur;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aN:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->e:Lmaps/e/bl;

    invoke-interface {v0, p1}, Lmaps/e/bl;->a(Leur;)V

    return-void
.end method

.method public final a(Leuu;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aO:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->e:Lmaps/e/bl;

    invoke-interface {v0, p1}, Lmaps/e/bl;->a(Leuu;)V

    return-void
.end method

.method public final a(Leux;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aI:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0, p1}, Lmaps/e/bo;->a(Leux;)V

    return-void
.end method

.method public final a(Leva;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aH:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0, p1}, Lmaps/e/bo;->a(Leva;)V

    return-void
.end method

.method public final a(Levg;Lcrv;)V
    .locals 3

    const-string v0, "Callback method is null."

    invoke-static {p1, v0}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    invoke-static {p2}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v2, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-nez v0, :cond_1

    sget-object v1, Lmaps/h/b;->bq:Lmaps/h/b;

    :goto_1
    invoke-interface {v2, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lmaps/e/as;

    invoke-direct {v2, p0, v0, p1}, Lmaps/e/as;-><init>(Lmaps/e/al;Landroid/graphics/Bitmap;Levg;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v1, Lmaps/h/b;->br:Lmaps/h/b;

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aA:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->v:Lmaps/ay/u;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/al;->i:Lmaps/e/av;

    iget-object v1, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v1}, Lmaps/c/m;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/e/av;->a(Landroid/content/res/Resources;)Lmaps/ay/u;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/al;->v:Lmaps/ay/u;

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/al;->v:Lmaps/ay/u;

    invoke-interface {v0, v1}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    :cond_0
    :goto_0
    sget-boolean v0, Lmaps/e/al;->L:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/e/al;->v:Lmaps/ay/u;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eq p1, v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->az:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->v:Lmaps/ay/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/al;->v:Lmaps/ay/u;

    invoke-interface {v0, v1}, Lmaps/c/m;->b(Lmaps/ay/u;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/al;->v:Lmaps/ay/u;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final b()F
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    invoke-direct {p0}, Lmaps/e/al;->B()F

    move-result v0

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "MapOptions"

    iget-object v1, p0, Lmaps/e/al;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {p1, v0, v1}, Levm;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "camera"

    iget-object v1, p0, Lmaps/e/al;->b:Lmaps/e/p;

    invoke-interface {v1}, Lmaps/e/p;->c()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    invoke-static {p1, v0, v1}, Levm;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public final b(Lcrv;)V
    .locals 5

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ah:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/af;

    iget-object v1, p0, Lmaps/e/al;->b:Lmaps/e/p;

    const/4 v2, -0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lmaps/e/al;->p:Lmaps/h/a;

    invoke-interface {v1, v0, v2, v3, v4}, Lmaps/e/p;->a(Lmaps/e/af;ILete;Lmaps/h/a;)V

    return-void
.end method

.method public final b(Z)Z
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->bn:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->m(Z)V

    invoke-direct {p0}, Lmaps/e/al;->C()Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lmaps/h/b;->bo:Lmaps/h/b;

    goto :goto_0
.end method

.method public final c()F
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->c:Lmaps/c/l;

    invoke-virtual {v0}, Lmaps/c/l;->e()F

    move-result v0

    return v0
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aC:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0}, Lmaps/e/bo;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aB:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0}, Lmaps/e/bo;->b()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->al:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->b:Lmaps/e/p;

    invoke-interface {v0}, Lmaps/e/p;->b()V

    return-void
.end method

.method public final d(Z)V
    .locals 3

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aE:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->w:Lmaps/ay/u;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/al;->i:Lmaps/e/av;

    iget-object v1, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v1}, Lmaps/c/m;->v()Lmaps/ap/f;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ap/f;->h()Lmaps/ay/ap;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/al;->t:Landroid/content/res/Resources;

    invoke-interface {v0, v1, v2}, Lmaps/e/av;->a(Lmaps/ay/ap;Landroid/content/res/Resources;)Lmaps/ay/u;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/al;->w:Lmaps/ay/u;

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/al;->w:Lmaps/ay/u;

    invoke-interface {v0, v1}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    :cond_0
    :goto_0
    sget-boolean v0, Lmaps/e/al;->L:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/e/al;->w:Lmaps/ay/u;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eq p1, v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aD:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->w:Lmaps/ay/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/al;->w:Lmaps/ay/u;

    invoke-interface {v0, v1}, Lmaps/c/m;->b(Lmaps/ay/u;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/al;->w:Lmaps/ay/u;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->ax:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/al;->e:Lmaps/e/bl;

    invoke-interface {v0}, Lmaps/e/bl;->a()V

    iget-object v0, p0, Lmaps/e/al;->f:Lmaps/e/br;

    invoke-interface {v0}, Lmaps/e/br;->a()V

    return-void
.end method

.method public final e(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->aW:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->n(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/h/b;->aT:Lmaps/h/b;

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget v0, p0, Lmaps/e/al;->y:I

    return v0
.end method

.method public final f(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->aU:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->o(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/h/b;->aR:Lmaps/h/b;

    goto :goto_0
.end method

.method public final g(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->aV:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->p(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/h/b;->aS:Lmaps/h/b;

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->v:Lmaps/ay/u;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->ba:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->q(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/h/b;->bb:Lmaps/h/b;

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    invoke-direct {p0}, Lmaps/e/al;->C()Z

    move-result v0

    return v0
.end method

.method public final i(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->bc:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->r(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/h/b;->bd:Lmaps/h/b;

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0}, Lmaps/e/bo;->c()Z

    move-result v0

    return v0
.end method

.method public final j()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0}, Lmaps/e/bo;->e()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final j(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->bg:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->s(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/h/b;->bh:Lmaps/h/b;

    goto :goto_0
.end method

.method public final k()Levj;
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->B:Levj;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/e/ar;

    invoke-direct {v0, p0}, Lmaps/e/ar;-><init>(Lmaps/e/al;)V

    iput-object v0, p0, Lmaps/e/al;->B:Levj;

    :cond_0
    iget-object v0, p0, Lmaps/e/al;->B:Levj;

    return-object v0
.end method

.method public final k(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->be:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->t(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/h/b;->bf:Lmaps/h/b;

    goto :goto_0
.end method

.method public final l()Levd;
    .locals 7

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    new-instance v0, Lmaps/e/bw;

    iget-object v1, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v1}, Lmaps/c/m;->m()Lmaps/ar/a;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/al;->p:Lmaps/h/a;

    iget v3, p0, Lmaps/e/al;->C:I

    iget v4, p0, Lmaps/e/al;->D:I

    iget v5, p0, Lmaps/e/al;->E:I

    iget v6, p0, Lmaps/e/al;->F:I

    invoke-direct/range {v0 .. v6}, Lmaps/e/bw;-><init>(Lmaps/ar/a;Lmaps/h/a;IIII)V

    return-object v0
.end method

.method public final l(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v1, p0, Lmaps/e/al;->p:Lmaps/h/a;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/h/b;->bi:Lmaps/h/b;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-direct {p0, p1}, Lmaps/e/al;->q(Z)V

    invoke-direct {p0, p1}, Lmaps/e/al;->r(Z)V

    invoke-direct {p0, p1}, Lmaps/e/al;->s(Z)V

    invoke-direct {p0, p1}, Lmaps/e/al;->t(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/h/b;->bj:Lmaps/h/b;

    goto :goto_0
.end method

.method public final m()Lcrv;
    .locals 1

    invoke-static {p0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->w:Lmaps/ay/u;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/al;->I:Z

    return v0
.end method

.method public final p()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/al;->J:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->h:Lmaps/e/bo;

    invoke-interface {v0}, Lmaps/e/bo;->d()Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->r()Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->s()Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->t()Z

    move-result v0

    return v0
.end method

.method public final u()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->u()Z

    move-result v0

    return v0
.end method

.method public final v()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/e/al;->p:Lmaps/h/a;

    invoke-interface {v0}, Lmaps/h/a;->a()V

    invoke-direct {p0, v1}, Lmaps/e/al;->m(Z)V

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0, v1}, Lmaps/c/m;->a(Z)V

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->j()V

    return-void
.end method

.method public final w()V
    .locals 2

    iget-object v0, p0, Lmaps/e/al;->r:Lmaps/e/ah;

    const-string v1, "on_resume"

    invoke-interface {v0, v1}, Lmaps/e/ah;->a(Ljava/lang/String;)Lmaps/e/ai;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/al;->l:Lmaps/e/at;

    invoke-static {}, Lmaps/e/at;->a()V

    iget-object v1, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v1}, Lmaps/c/m;->e()V

    iget-object v1, p0, Lmaps/e/al;->g:Lmaps/f/a;

    invoke-interface {v1}, Lmaps/f/a;->b()V

    iget-object v1, p0, Lmaps/e/al;->r:Lmaps/e/ah;

    invoke-interface {v1, v0}, Lmaps/e/ah;->a(Lmaps/e/ai;)V

    return-void
.end method

.method public final x()V
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->g:Lmaps/f/a;

    invoke-interface {v0}, Lmaps/f/a;->c()V

    iget-object v0, p0, Lmaps/e/al;->l:Lmaps/e/at;

    invoke-static {}, Lmaps/e/at;->b()V

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->d()V

    return-void
.end method

.method public final y()V
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->d:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->k()V

    return-void
.end method

.method public final z()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmaps/e/al;->n:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/al;->o:Landroid/view/View;

    return-object v0
.end method
