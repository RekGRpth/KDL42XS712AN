.class public final Lmaps/e/bn;
.super Lewp;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lmaps/e/f;

.field private c:Lmaps/au/af;

.field private final d:Lmaps/e/bm;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:F

.field private h:F

.field private final i:Lmaps/h/a;

.field private j:F

.field private k:F


# direct methods
.method constructor <init>(Ljava/lang/String;Lmaps/e/f;Lmaps/au/af;Lmaps/e/bm;Lmaps/h/a;)V
    .locals 0

    invoke-direct {p0}, Lewp;-><init>()V

    iput-object p1, p0, Lmaps/e/bn;->a:Ljava/lang/String;

    iput-object p2, p0, Lmaps/e/bn;->b:Lmaps/e/f;

    iput-object p3, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    iput-object p4, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    iput-object p5, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    return-void
.end method

.method static synthetic a(Lmaps/e/bn;F)F
    .locals 0

    iput p1, p0, Lmaps/e/bn;->g:F

    return p1
.end method

.method static synthetic a(Lmaps/e/bn;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lmaps/e/bn;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lmaps/e/bn;)Lmaps/au/af;
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    return-object v0
.end method

.method static synthetic b(Lmaps/e/bn;F)F
    .locals 0

    iput p1, p0, Lmaps/e/bn;->h:F

    return p1
.end method

.method static synthetic b(Lmaps/e/bn;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lmaps/e/bn;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lmaps/e/bn;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lmaps/e/bn;F)F
    .locals 0

    iput p1, p0, Lmaps/e/bn;->j:F

    return p1
.end method

.method static synthetic c(Lmaps/e/bn;)Lmaps/e/f;
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->b:Lmaps/e/f;

    return-object v0
.end method

.method static synthetic d(Lmaps/e/bn;F)F
    .locals 0

    iput p1, p0, Lmaps/e/bn;->k:F

    return p1
.end method

.method static synthetic d(Lmaps/e/bn;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->c:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0, p0}, Lmaps/e/bm;->a(Lmaps/e/bm;Lmaps/e/bn;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->l:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0, p1}, Lmaps/au/af;->a(F)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final a(FF)V
    .locals 3

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->h:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iput p1, p0, Lmaps/e/bn;->g:F

    iput p2, p0, Lmaps/e/bn;->h:F

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    iget-object v1, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v1}, Lmaps/au/af;->i()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v2}, Lmaps/au/af;->i()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lmaps/au/af;->a(II)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->d:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-static {p1}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/au/af;->a(Lmaps/ac/av;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->c(Lmaps/e/bm;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final a(Lcrv;)V
    .locals 13

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->g:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->d(Lmaps/e/bm;)Lmaps/ay/r;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0, v1}, Lmaps/ay/r;->b(Lmaps/au/af;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->e(Lmaps/e/bm;)Lmaps/e/n;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/bn;->b:Lmaps/e/f;

    invoke-virtual {v0, v1}, Lmaps/e/n;->b(Lmaps/e/f;)V

    invoke-static {p1}, Lmaps/e/f;->a(Lcrv;)Lmaps/e/f;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bn;->b:Lmaps/e/f;

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    iget-object v1, p0, Lmaps/e/bn;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lmaps/e/bn;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget-object v3, p0, Lmaps/e/bn;->b:Lmaps/e/f;

    iget v4, p0, Lmaps/e/bn;->g:F

    iget v5, p0, Lmaps/e/bn;->h:F

    iget v6, p0, Lmaps/e/bn;->j:F

    iget v7, p0, Lmaps/e/bn;->k:F

    invoke-virtual {p0}, Lmaps/e/bn;->f()Z

    move-result v8

    invoke-virtual {p0}, Lmaps/e/bn;->j()Z

    move-result v9

    invoke-virtual {p0}, Lmaps/e/bn;->l()Z

    move-result v10

    invoke-virtual {p0}, Lmaps/e/bn;->m()F

    move-result v11

    invoke-virtual {p0}, Lmaps/e/bn;->n()F

    move-result v12

    invoke-static/range {v0 .. v12}, Lmaps/e/bm;->a(Lmaps/e/bm;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Lmaps/e/f;FFFFZZZFF)Lmaps/au/af;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->d(Lmaps/e/bm;)Lmaps/ay/r;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0, v1}, Lmaps/ay/r;->a(Lmaps/au/af;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->e:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iput-object p1, p0, Lmaps/e/bn;->e:Ljava/lang/String;

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->i:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0, p1}, Lmaps/au/af;->a(Z)V

    return-void
.end method

.method public final a(Lewo;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->n:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0, p1}, Lmaps/au/af;->b(F)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final b(FF)V
    .locals 3

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->m:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iput p1, p0, Lmaps/e/bn;->j:F

    iput p2, p0, Lmaps/e/bn;->k:F

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    iget-object v1, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v1}, Lmaps/au/af;->i()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v2}, Lmaps/au/af;->i()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lmaps/au/af;->b(II)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->f:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iput-object p1, p0, Lmaps/e/bn;->f:Ljava/lang/String;

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->j:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0, p0}, Lmaps/e/bm;->c(Lmaps/e/bm;Lmaps/e/bn;)V

    :cond_0
    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0, p1}, Lmaps/au/af;->c(Z)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->c(Lmaps/e/bm;)V

    return-void
.end method

.method public final c()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->o()Lmaps/ac/av;

    move-result-object v0

    invoke-static {v0}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->k:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lmaps/au/af;->b(Z)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->b()Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->o:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0, p0}, Lmaps/e/bm;->b(Lmaps/e/bm;Lmaps/e/bn;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->p:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0, p0}, Lmaps/e/bm;->c(Lmaps/e/bm;Lmaps/e/bn;)V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->b(Lmaps/e/bm;)V

    return-void
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0, p0}, Lmaps/e/bm;->d(Lmaps/e/bm;Lmaps/e/bn;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->e()Z

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()F
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->c()F

    move-result v0

    return v0
.end method

.method public final n()F
    .locals 1

    iget-object v0, p0, Lmaps/e/bn;->d:Lmaps/e/bm;

    invoke-static {v0}, Lmaps/e/bm;->a(Lmaps/e/bm;)Lmaps/i/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/bn;->c:Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->f()F

    move-result v0

    return v0
.end method
