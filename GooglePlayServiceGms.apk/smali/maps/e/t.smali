.class public final Lmaps/e/t;
.super Ljava/lang/Object;


# static fields
.field private static final a:D

.field private static final b:Lmaps/e/af;

.field private static final c:Lmaps/e/af;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    sput-wide v0, Lmaps/e/t;->a:D

    new-instance v0, Lmaps/e/u;

    invoke-direct {v0}, Lmaps/e/u;-><init>()V

    sput-object v0, Lmaps/e/t;->b:Lmaps/e/af;

    new-instance v0, Lmaps/e/x;

    invoke-direct {v0}, Lmaps/e/x;-><init>()V

    sput-object v0, Lmaps/e/t;->c:Lmaps/e/af;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lmaps/e/af;
    .locals 1

    sget-object v0, Lmaps/e/t;->b:Lmaps/e/af;

    return-object v0
.end method

.method public static a(F)Lmaps/e/af;
    .locals 1

    new-instance v0, Lmaps/e/z;

    invoke-direct {v0, p0}, Lmaps/e/z;-><init>(F)V

    return-object v0
.end method

.method public static a(FF)Lmaps/e/af;
    .locals 1

    new-instance v0, Lmaps/e/y;

    invoke-direct {v0, p0, p1}, Lmaps/e/y;-><init>(FF)V

    return-object v0
.end method

.method public static a(FII)Lmaps/e/af;
    .locals 1

    new-instance v0, Lmaps/e/ab;

    invoke-direct {v0, p0, p1, p2}, Lmaps/e/ab;-><init>(FII)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/CameraPosition;)Lmaps/e/af;
    .locals 1

    new-instance v0, Lmaps/e/ac;

    invoke-direct {v0, p0}, Lmaps/e/ac;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/e/af;
    .locals 1

    new-instance v0, Lmaps/e/ad;

    invoke-direct {v0, p0}, Lmaps/e/ad;-><init>(Lcom/google/android/gms/maps/model/LatLng;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;F)Lmaps/e/af;
    .locals 1

    new-instance v0, Lmaps/e/ae;

    invoke-direct {v0, p0, p1}, Lmaps/e/ae;-><init>(Lcom/google/android/gms/maps/model/LatLng;F)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lmaps/e/af;
    .locals 1

    new-instance v0, Lmaps/e/v;

    invoke-direct {v0, p0, p1}, Lmaps/e/v;-><init>(Lcom/google/android/gms/maps/model/LatLngBounds;I)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lmaps/e/af;
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Map size should not be 0"

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    new-instance v0, Lmaps/e/w;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/e/w;-><init>(Lcom/google/android/gms/maps/model/LatLngBounds;III)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/ar/b;Lmaps/c/l;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lmaps/e/t;->b(Lmaps/ar/b;Lmaps/c/l;I)V

    return-void
.end method

.method static synthetic a(Lmaps/c/m;Lmaps/c/l;ILcom/google/android/gms/maps/model/LatLngBounds;III)V
    .locals 12

    move/from16 v0, p4

    int-to-double v1, v0

    move/from16 v0, p5

    int-to-double v3, v0

    mul-int/lit8 v5, p6, 0x2

    int-to-double v5, v5

    sub-double v5, v1, v5

    mul-int/lit8 v1, p6, 0x2

    int-to-double v1, v1

    sub-double v2, v3, v1

    const-wide/16 v7, 0x0

    cmpl-double v1, v5, v7

    if-lez v1, :cond_0

    const-wide/16 v7, 0x0

    cmpl-double v1, v2, v7

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v4, "View size is too small after padding"

    invoke-static {v1, v4}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lmaps/c/l;->a()I

    move-result v1

    int-to-double v7, v1

    sub-double v4, v5, v7

    invoke-virtual {p1}, Lmaps/c/l;->b()I

    move-result v1

    int-to-double v6, v1

    sub-double/2addr v2, v6

    invoke-interface {p0}, Lmaps/c/m;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v1

    const-wide/high16 v8, 0x4070000000000000L    # 256.0

    mul-double/2addr v6, v8

    iget-object v1, p3, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v8

    iget-object v1, p3, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v9

    invoke-virtual {v8}, Lmaps/ac/av;->f()I

    move-result v1

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v10

    if-ge v1, v10, :cond_1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v10

    sub-int/2addr v1, v10

    invoke-virtual {v8}, Lmaps/ac/av;->f()I

    move-result v10

    add-int/2addr v1, v10

    :goto_1
    invoke-virtual {v8}, Lmaps/ac/av;->g()I

    move-result v8

    invoke-virtual {v9}, Lmaps/ac/av;->g()I

    move-result v10

    sub-int/2addr v8, v10

    int-to-double v10, v1

    mul-double/2addr v10, v6

    div-double v4, v10, v4

    int-to-double v10, v8

    mul-double/2addr v6, v10

    div-double v2, v6, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    const-wide/high16 v4, 0x403e000000000000L    # 30.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    sget-wide v6, Lmaps/e/t;->a:D

    mul-double/2addr v2, v6

    sub-double v3, v4, v2

    new-instance v2, Lmaps/ac/av;

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v5

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v5

    const/high16 v5, 0x40000000    # 2.0f

    rem-int/2addr v1, v5

    invoke-virtual {v9}, Lmaps/ac/av;->g()I

    move-result v5

    div-int/lit8 v6, v8, 0x2

    add-int/2addr v5, v6

    invoke-direct {v2, v1, v5}, Lmaps/ac/av;-><init>(II)V

    new-instance v1, Lmaps/ar/b;

    double-to-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    invoke-static {v1, p1, p2}, Lmaps/e/t;->b(Lmaps/ar/b;Lmaps/c/l;I)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_1
    invoke-virtual {v8}, Lmaps/ac/av;->f()I

    move-result v1

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v10

    sub-int/2addr v1, v10

    goto :goto_1
.end method

.method public static b()Lmaps/e/af;
    .locals 1

    sget-object v0, Lmaps/e/t;->c:Lmaps/e/af;

    return-object v0
.end method

.method public static b(F)Lmaps/e/af;
    .locals 1

    new-instance v0, Lmaps/e/aa;

    invoke-direct {v0, p0}, Lmaps/e/aa;-><init>(F)V

    return-object v0
.end method

.method private static b(Lmaps/ar/b;Lmaps/c/l;I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-nez p2, :cond_0

    invoke-virtual {p1, p0, v1, v1}, Lmaps/c/l;->a(Lmaps/ar/c;II)V

    :goto_0
    return-void

    :cond_0
    if-ne p2, v0, :cond_1

    invoke-virtual {p1, p0, v0, v0}, Lmaps/c/l;->a(Lmaps/ar/c;II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p0, p2, p2}, Lmaps/c/l;->a(Lmaps/ar/c;II)V

    goto :goto_0
.end method
