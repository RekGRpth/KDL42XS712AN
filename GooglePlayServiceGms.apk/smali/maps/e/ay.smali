.class public final Lmaps/e/ay;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Landroid/content/Context;

.field private static d:Lmaps/e/bc;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.google.android.geo.API_KEY"

    aput-object v2, v0, v1

    const-string v1, "com.google.android.maps.v2.API_KEY"

    aput-object v1, v0, v3

    sput-object v0, Lmaps/e/ay;->a:[Ljava/lang/String;

    aget-object v0, v0, v3

    sput-object v0, Lmaps/e/ay;->b:Ljava/lang/String;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    const-class v1, Lmaps/e/ay;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/e/bb;

    invoke-direct {v0}, Lmaps/e/bb;-><init>()V

    invoke-static {p0, v0}, Lmaps/e/ay;->a(Landroid/content/Context;Lmaps/e/bb;)Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(Landroid/content/Context;Lmaps/e/bb;)Landroid/content/Context;
    .locals 10

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-class v8, Lmaps/e/ay;

    monitor-enter v8

    :try_start_0
    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v8

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lmaps/k/o;->a(Z)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lmaps/e/be;->b(Landroid/content/res/Resources;)V

    new-instance v0, Lmaps/e/az;

    invoke-direct {v0, p0}, Lmaps/e/az;-><init>(Landroid/content/Context;)V

    sput-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    invoke-static {v0}, Lmaps/e/ci;->a(Landroid/content/Context;)V

    new-instance v0, Lmaps/e/bc;

    sget-object v1, Lmaps/e/ay;->c:Landroid/content/Context;

    const-string v2, "MapviewInitializerPreferences"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    sget-object v2, Lmaps/e/ay;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1, v2}, Lmaps/e/bc;-><init>(Landroid/content/SharedPreferences;Lmaps/e/bb;Landroid/content/Context;)V

    sput-object v0, Lmaps/e/ay;->d:Lmaps/e/bc;

    new-instance v5, Lmaps/bb/c;

    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    invoke-direct {v5, v0}, Lmaps/bb/c;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x6

    new-array v2, v0, [Lmaps/ao/b;

    const/4 v0, 0x0

    sget-object v1, Lmaps/ao/b;->a:Lmaps/ao/b;

    aput-object v1, v2, v0

    const/4 v0, 0x1

    sget-object v1, Lmaps/ao/b;->d:Lmaps/ao/b;

    aput-object v1, v2, v0

    const/4 v0, 0x2

    sget-object v1, Lmaps/ao/b;->f:Lmaps/ao/b;

    aput-object v1, v2, v0

    const/4 v0, 0x3

    sget-object v1, Lmaps/ao/b;->e:Lmaps/ao/b;

    aput-object v1, v2, v0

    const/4 v0, 0x4

    sget-object v1, Lmaps/ao/b;->o:Lmaps/ao/b;

    aput-object v1, v2, v0

    const/4 v0, 0x5

    sget-object v1, Lmaps/ao/b;->n:Lmaps/ao/b;

    aput-object v1, v2, v0

    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    invoke-static {}, Lmaps/e/be;->a()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lmaps/e/ay;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;)Lmaps/bn/d;

    move-result-object v9

    sget-object v0, Lmaps/e/ay;->d:Lmaps/e/bc;

    invoke-virtual {v9, v0}, Lmaps/bn/d;->a(Lmaps/bn/l;)V

    invoke-static {v9}, Lmaps/be/g;->a(Lmaps/bn/d;)V

    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    invoke-static {}, Lmaps/e/be;->a()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lmaps/b/g;->a:I

    invoke-static/range {v0 .. v5}, Lmaps/ap/p;->a(Landroid/content/Context;Landroid/content/res/Resources;[Lmaps/ao/b;Ljava/lang/String;ILmaps/br/k;)V

    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v1

    if-eqz v1, :cond_5

    array-length v0, v1

    if-lez v0, :cond_5

    array-length v2, v1

    move v0, v7

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    iget-object v4, v3, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    if-nez v4, :cond_4

    iget v3, v3, Landroid/content/pm/FeatureInfo;->reqGlEsVersion:I

    const/high16 v4, 0x20000

    if-lt v3, v4, :cond_4

    move v0, v6

    :goto_2
    if-nez v0, :cond_1

    invoke-static {}, Lmaps/bb/a;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    invoke-static {v0}, Lmaps/i/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lmaps/bb/a;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v9, v1}, Lmaps/e/ay;->a(Landroid/content/Context;Lmaps/bn/d;Z)V

    :goto_3
    invoke-static {}, Lmaps/ba/i;->a()Lmaps/by/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/by/c;->b()V

    sget-object v0, Lmaps/e/ay;->d:Lmaps/e/bc;

    invoke-virtual {v0}, Lmaps/e/bc;->c()V

    invoke-static {}, Lmaps/be/g;->d()Lmaps/be/b;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lmaps/be/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lmaps/b/h;->a:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    sget-object v0, Lmaps/e/ay;->c:Landroid/content/Context;

    goto/16 :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v7

    goto :goto_2

    :cond_6
    invoke-virtual {v9}, Lmaps/bn/d;->p()V

    const-string v0, "Google Play services is missing."

    const/4 v1, 0x6

    invoke-static {v1, v0}, Lmaps/i/f;->a(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    :cond_7
    :try_start_2
    invoke-virtual {v9}, Lmaps/bn/d;->p()V

    const-string v0, "Google Maps Android API v2 only supports devices with OpenGL ES 2.0 and above"

    const/4 v1, 0x6

    invoke-static {v1, v0}, Lmaps/i/f;->a(ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private static a(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_2

    sget-object v3, Lmaps/e/ay;->a:[Ljava/lang/String;

    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    iget-object v6, p0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The API key can only be specified once. It is recommended that you use the meta-data tag with the name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lmaps/e/ay;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in the <application> element of AndroidManifest.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "API key not found.  Check that <meta-data android:name=\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lmaps/e/ay;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" android:value=\"your API key\"/> is in the <application> element of AndroidManifest.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v1
.end method

.method private static a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;)Lmaps/bn/d;
    .locals 4

    invoke-static {p0}, Lmaps/bf/a;->a(Landroid/content/Context;)Lmaps/bf/a;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-string v1, "2.8.0"

    invoke-static {p0, p1, p2, v1}, Lmaps/ap/p;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Lmaps/bn/d;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/bn/d;->s()V

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    invoke-static {}, Lmaps/bf/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmaps/bn/d;->b(Ljava/lang/String;)V

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/bn/d;->c(Ljava/lang/String;)V

    const-string v2, "SYSTEM"

    invoke-virtual {v1, v2}, Lmaps/bn/d;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Lmaps/bn/d;->t()V

    invoke-virtual {v1}, Lmaps/bn/d;->f()V

    invoke-virtual {v1}, Lmaps/bn/d;->f()V

    invoke-static {v0}, Lmaps/e/ay;->a(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lmaps/e/a;->a(Landroid/content/Context;Ljava/lang/String;Lmaps/bn/d;)Lmaps/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/a;->c()V

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method static synthetic a(Landroid/content/Context;Lmaps/bn/d;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lmaps/e/ay;->a(Landroid/content/Context;Lmaps/bn/d;Z)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lmaps/bn/d;Z)V
    .locals 1

    invoke-static {}, Lmaps/ax/m;->e()V

    new-instance v0, Lmaps/e/ba;

    invoke-direct {v0, p2, p0}, Lmaps/e/ba;-><init>(ZLandroid/content/Context;)V

    invoke-static {p0, p1, v0}, Lmaps/ax/m;->a(Landroid/content/Context;Lmaps/bn/d;Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Z)Z
    .locals 3

    const-string v0, "android.permission.ACCESS_NETWORK_STATE"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    :cond_0
    :goto_0
    return p1

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method
