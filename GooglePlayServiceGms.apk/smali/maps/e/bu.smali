.class final Lmaps/e/bu;
.super Lews;

# interfaces
.implements Lmaps/e/bq;


# static fields
.field private static final a:Lcom/google/android/gms/maps/model/PolygonOptions;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lmaps/e/bs;

.field private d:F

.field private e:I

.field private f:I

.field private final g:Ljava/util/List;

.field private final h:Ljava/util/List;

.field private i:Lmaps/ac/az;

.field private j:Ljava/util/List;

.field private k:Lmaps/ay/x;

.field private l:F

.field private m:Z

.field private n:Z

.field private final o:Lmaps/h/a;

.field private p:Lmaps/as/a;

.field private q:Lmaps/ap/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/PolygonOptions;-><init>()V

    sput-object v0, Lmaps/e/bu;->a:Lcom/google/android/gms/maps/model/PolygonOptions;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/PolygonOptions;Lmaps/e/bs;Lmaps/h/a;)V
    .locals 3

    invoke-direct {p0}, Lews;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->e()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lmaps/e/bu;->b:Ljava/lang/String;

    iput-object p3, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    iput-object p4, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->e()F

    move-result v0

    iput v0, p0, Lmaps/e/bu;->d:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->f()I

    move-result v0

    iput v0, p0, Lmaps/e/bu;->e:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->g()I

    move-result v0

    iput v0, p0, Lmaps/e/bu;->f:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->h()F

    move-result v0

    iput v0, p0, Lmaps/e/bu;->l:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->i()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/bu;->m:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->j()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/bu;->n:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->c()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bu;->g:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/bu;->h:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lmaps/e/bu;->h:Ljava/util/List;

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->g()I

    move-result v0

    sget-object v1, Lmaps/e/bu;->a:Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolygonOptions;->g()I

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->F:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->f()I

    move-result v0

    sget-object v1, Lmaps/e/bu;->a:Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolygonOptions;->f()I

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->E:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->e()F

    move-result v0

    sget-object v1, Lmaps/e/bu;->a:Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolygonOptions;->e()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->D:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->j()Z

    move-result v0

    sget-object v1, Lmaps/e/bu;->a:Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolygonOptions;->j()Z

    move-result v1

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->I:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_5
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->d()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lmaps/e/bu;->a:Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolygonOptions;->d()Ljava/util/List;

    move-result-object v1

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->C:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_6
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->i()Z

    move-result v0

    sget-object v1, Lmaps/e/bu;->a:Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolygonOptions;->i()Z

    move-result v1

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->H:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_7
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->h()F

    move-result v0

    sget-object v1, Lmaps/e/bu;->a:Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolygonOptions;->h()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->G:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_8
    return-void
.end method

.method private static c(Ljava/util/List;)V
    .locals 3

    const/4 v2, 0x0

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private static d(Ljava/util/List;)Lmaps/ac/az;
    .locals 3

    new-instance v1, Lmaps/ac/bb;

    invoke-direct {v1}, Lmaps/ac/bb;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/az;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ac/az;->c(I)Lmaps/ac/az;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/az;->f()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lmaps/ac/az;->h()Lmaps/ac/az;

    move-result-object v0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->A:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->a(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->D:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/bu;->d:F

    iget-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    float-to-int v1, p1

    invoke-virtual {v0, v1}, Lmaps/ay/x;->d(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->E:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/bu;->e:I

    iget-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    invoke-static {p1}, Lmaps/i/a;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/x;->b(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->B:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bu;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/e/bu;->g:Ljava/util/List;

    invoke-static {v0, p1}, Lmaps/m/bz;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    invoke-virtual {p0}, Lmaps/e/bu;->p()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/ar/a;Lmaps/as/a;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bu;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    invoke-virtual {v0, p1, p2}, Lmaps/ay/x;->b(Lmaps/ar/a;Lmaps/as/a;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/e/bu;->p:Lmaps/as/a;

    iput-object p2, p0, Lmaps/e/bu;->q:Lmaps/ap/n;

    iget-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    invoke-virtual {v0, p1, p2}, Lmaps/ay/x;->a(Lmaps/as/a;Lmaps/ap/n;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bu;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/x;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->H:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/e/bu;->m:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lewr;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/bu;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->G:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->b(Lmaps/e/bq;)V

    iput p1, p0, Lmaps/e/bu;->l:F

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->c(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->F:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/bu;->f:I

    iget-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    invoke-static {p1}, Lmaps/i/a;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/x;->c(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->C:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bu;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lmaps/e/bu;->h:Ljava/util/List;

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lmaps/e/bu;->p()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bu;->g:Ljava/util/List;

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public final c_(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bu;->o:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->I:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bu;->n:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/e/bu;->n:Z

    invoke-virtual {p0}, Lmaps/e/bu;->p()V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/util/List;
    .locals 3

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bu;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public final e()F
    .locals 1

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/bu;->d:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/bu;->e:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/bu;->f:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()F
    .locals 1

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget v0, p0, Lmaps/e/bu;->l:F

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bu;->m:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bu;->n:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final k()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 0

    return-void
.end method

.method public final m()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized n()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bu;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    invoke-virtual {v0}, Lmaps/ay/x;->ar_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final o()V
    .locals 0

    return-void
.end method

.method final p()V
    .locals 6

    iget-object v0, p0, Lmaps/e/bu;->g:Ljava/util/List;

    invoke-static {v0}, Lmaps/e/bu;->c(Ljava/util/List;)V

    iget-boolean v0, p0, Lmaps/e/bu;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/bu;->g:Ljava/util/List;

    invoke-static {v0}, Lmaps/i/b;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lmaps/e/bu;->d(Ljava/util/List;)Lmaps/ac/az;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bu;->i:Lmaps/ac/az;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/bu;->j:Ljava/util/List;

    iget-object v0, p0, Lmaps/e/bu;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lmaps/e/bu;->c(Ljava/util/List;)V

    iget-object v2, p0, Lmaps/e/bu;->j:Ljava/util/List;

    iget-boolean v3, p0, Lmaps/e/bu;->n:Z

    if-eqz v3, :cond_0

    invoke-static {v0}, Lmaps/i/b;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lmaps/e/bu;->d(Ljava/util/List;)Lmaps/ac/az;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmaps/e/bu;->g:Ljava/util/List;

    goto :goto_0

    :cond_2
    new-instance v0, Lmaps/ay/x;

    iget-object v1, p0, Lmaps/e/bu;->i:Lmaps/ac/az;

    iget-object v2, p0, Lmaps/e/bu;->j:Ljava/util/List;

    iget v3, p0, Lmaps/e/bu;->d:F

    float-to-int v3, v3

    iget v4, p0, Lmaps/e/bu;->e:I

    invoke-static {v4}, Lmaps/i/a;->a(I)I

    move-result v4

    iget v5, p0, Lmaps/e/bu;->f:I

    invoke-static {v5}, Lmaps/i/a;->a(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/ay/x;-><init>(Lmaps/ac/az;Ljava/util/List;III)V

    iput-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    iget-object v0, p0, Lmaps/e/bu;->p:Lmaps/as/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/e/bu;->q:Lmaps/ap/n;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/e/bu;->k:Lmaps/ay/x;

    iget-object v1, p0, Lmaps/e/bu;->p:Lmaps/as/a;

    iget-object v2, p0, Lmaps/e/bu;->q:Lmaps/ap/n;

    invoke-virtual {v0, v1, v2}, Lmaps/ay/x;->a(Lmaps/as/a;Lmaps/ap/n;)V

    :cond_3
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lmaps/e/bu;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "points"

    iget-object v2, p0, Lmaps/e/bu;->g:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "holes"

    iget-object v2, p0, Lmaps/e/bu;->h:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "strokeWidth"

    iget v2, p0, Lmaps/e/bu;->d:F

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;F)Lmaps/k/k;

    move-result-object v0

    const-string v1, "strokeColor"

    iget v2, p0, Lmaps/e/bu;->e:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    const-string v1, "fillColor"

    iget v2, p0, Lmaps/e/bu;->f:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
