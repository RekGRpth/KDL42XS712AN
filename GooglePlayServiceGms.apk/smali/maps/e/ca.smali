.class public final Lmaps/e/ca;
.super Ljava/lang/Object;


# instance fields
.field private final a:Z

.field private final b:Lmaps/c/m;

.field private final c:Landroid/os/Handler;

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lmaps/c/m;ZLandroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmaps/e/ca;->b:Lmaps/c/m;

    iput-boolean p3, p0, Lmaps/e/ca;->a:Z

    iput-object p4, p0, Lmaps/e/ca;->c:Landroid/os/Handler;

    iput-object p1, p0, Lmaps/e/ca;->d:Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lmaps/e/ca;)Lmaps/c/m;
    .locals 1

    iget-object v0, p0, Lmaps/e/ca;->b:Lmaps/c/m;

    return-object v0
.end method

.method static synthetic a(Levg;Landroid/graphics/Bitmap;)V
    .locals 2

    :try_start_0
    invoke-static {}, Lmaps/e/ci;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0, p1}, Levg;->a(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    invoke-interface {p0, v0}, Levg;->a(Lcrv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method static synthetic a(Lmaps/e/ca;Landroid/graphics/Bitmap;)V
    .locals 2

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lmaps/e/ca;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/graphics/Bitmap;Levg;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/ca;->b:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->getWidth()I

    move-result v0

    iget-object v1, p0, Lmaps/e/ca;->b:Lmaps/c/m;

    invoke-interface {v1}, Lmaps/c/m;->getHeight()I

    move-result v1

    if-nez p1, :cond_1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lmaps/e/ca;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/e/ca;->c:Landroid/os/Handler;

    new-instance v1, Lmaps/e/cc;

    invoke-direct {v1, p0, p1, p2}, Lmaps/e/cc;-><init>(Lmaps/e/ca;Landroid/graphics/Bitmap;Levg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, v1, :cond_0

    :cond_2
    const/4 v2, 0x5

    const-string v3, "The Bitmap provided in the snapshot() method does not match the map\'s dimensions, hence another Bitmap is allocated with the right dimensions. If you think this is due to the fact that the map was resized, you can ignore this message. Otherwise, you should check the dimensions of the Bitmap passed to the method."

    invoke-static {v2, v3}, Lmaps/i/f;->a(ILjava/lang/String;)V

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/e/ca;->b:Lmaps/c/m;

    invoke-interface {v0, p1}, Lmaps/c/m;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lmaps/aw/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/ca;->c:Landroid/os/Handler;

    new-instance v2, Lmaps/e/cb;

    invoke-direct {v2, p0, v0, p2}, Lmaps/e/cb;-><init>(Lmaps/e/ca;Landroid/graphics/Bitmap;Levg;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
