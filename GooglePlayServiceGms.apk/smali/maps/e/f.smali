.class public abstract Lmaps/e/f;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmaps/e/k;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/e/k;

    sget v1, Lmaps/b/e;->M:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmaps/e/k;-><init>(IB)V

    sput-object v0, Lmaps/e/f;->a:Lmaps/e/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lmaps/e/f;-><init>()V

    return-void
.end method

.method static a(Lcrv;)Lmaps/e/f;
    .locals 1

    if-nez p0, :cond_0

    sget-object v0, Lmaps/e/f;->a:Lmaps/e/k;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/f;

    goto :goto_0
.end method

.method static a(Levo;)Lmaps/e/f;
    .locals 1

    if-nez p0, :cond_0

    sget-object v0, Lmaps/e/f;->a:Lmaps/e/k;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Levo;->a:Lcrv;

    invoke-static {v0}, Lmaps/e/f;->a(Lcrv;)Lmaps/e/f;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lmaps/e/g;
    .locals 2

    new-instance v0, Lmaps/e/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/e/g;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method

.method public static a(F)Lmaps/e/h;
    .locals 2

    new-instance v0, Lmaps/e/h;

    sget-object v1, Lmaps/e/f;->a:Lmaps/e/k;

    invoke-direct {v0, v1, p0}, Lmaps/e/h;-><init>(Lmaps/e/f;F)V

    return-object v0
.end method

.method public static a(Landroid/graphics/Bitmap;)Lmaps/e/j;
    .locals 2

    new-instance v0, Lmaps/e/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/e/j;-><init>(Landroid/graphics/Bitmap;B)V

    return-object v0
.end method

.method public static a()Lmaps/e/k;
    .locals 1

    sget-object v0, Lmaps/e/f;->a:Lmaps/e/k;

    return-object v0
.end method

.method public static a(I)Lmaps/e/m;
    .locals 2

    new-instance v0, Lmaps/e/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/e/m;-><init>(IB)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lmaps/e/i;
    .locals 2

    new-instance v0, Lmaps/e/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/e/i;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lmaps/e/l;
    .locals 2

    new-instance v0, Lmaps/e/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/e/l;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/graphics/Bitmap;
.end method
