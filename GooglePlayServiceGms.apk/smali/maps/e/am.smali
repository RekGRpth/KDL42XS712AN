.class final Lmaps/e/am;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/g/f;


# instance fields
.field private synthetic a:Lmaps/e/al;


# direct methods
.method constructor <init>(Lmaps/e/al;)V
    .locals 0

    iput-object p1, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(F)F
    .locals 2

    iget-object v0, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-static {v0}, Lmaps/e/al;->c(Lmaps/e/al;)F

    move-result v0

    iget-object v1, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-static {v1}, Lmaps/e/al;->d(Lmaps/e/al;)F

    move-result v1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-static {v0}, Lmaps/e/al;->a(Lmaps/e/al;)Lmaps/h/a;

    move-result-object v0

    sget-object v1, Lmaps/h/b;->aY:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-static {v0}, Lmaps/e/al;->b(Lmaps/e/al;)Lmaps/c/l;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-static {v1}, Lmaps/e/al;->b(Lmaps/e/al;)Lmaps/c/l;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/c/l;->g()F

    move-result v1

    invoke-direct {p0, v1}, Lmaps/e/am;->a(F)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Lmaps/c/l;->b(FI)V

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-static {v0}, Lmaps/e/al;->a(Lmaps/e/al;)Lmaps/h/a;

    move-result-object v0

    sget-object v1, Lmaps/h/b;->aZ:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-static {v0}, Lmaps/e/al;->b(Lmaps/e/al;)Lmaps/c/l;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/am;->a:Lmaps/e/al;

    invoke-static {v1}, Lmaps/e/al;->b(Lmaps/e/al;)Lmaps/c/l;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/c/l;->g()F

    move-result v1

    invoke-direct {p0, v1}, Lmaps/e/am;->a(F)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Lmaps/c/l;->b(FI)V

    return-void
.end method
