.class final Lmaps/e/ag;
.super Lewj;

# interfaces
.implements Lmaps/e/bq;


# static fields
.field private static final a:Lcom/google/android/gms/maps/model/CircleOptions;

.field private static o:Ljava/util/List;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lmaps/e/bs;

.field private d:F

.field private e:I

.field private f:I

.field private g:Lcom/google/android/gms/maps/model/LatLng;

.field private h:D

.field private i:Lmaps/ay/x;

.field private j:F

.field private k:Z

.field private final l:Lmaps/h/a;

.field private m:Lmaps/as/a;

.field private n:Lmaps/ap/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/CircleOptions;-><init>()V

    sput-object v0, Lmaps/e/ag;->a:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lmaps/e/ag;->o:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/CircleOptions;Lmaps/e/bs;Lmaps/h/a;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lewj;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->d()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_5

    move v0, v1

    :goto_0
    const-string v3, "line width is negative"

    invoke-static {v0, v3}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->c()D

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmpl-double v0, v3, v5

    if-ltz v0, :cond_6

    :goto_1
    const-string v0, "radius is negative"

    invoke-static {v1, v0}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lmaps/e/ag;->b:Ljava/lang/String;

    iput-object p3, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    iput-object p4, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->d()F

    move-result v0

    iput v0, p0, Lmaps/e/ag;->d:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->e()I

    move-result v0

    iput v0, p0, Lmaps/e/ag;->e:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->f()I

    move-result v0

    iput v0, p0, Lmaps/e/ag;->f:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->g()F

    move-result v0

    iput v0, p0, Lmaps/e/ag;->j:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->h()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/ag;->k:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->c()D

    move-result-wide v0

    iput-wide v0, p0, Lmaps/e/ag;->h:D

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/ag;->g:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->f()I

    move-result v0

    sget-object v1, Lmaps/e/ag;->a:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->f()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->P:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->e()I

    move-result v0

    sget-object v1, Lmaps/e/ag;->a:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->e()I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->O:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->d()F

    move-result v0

    sget-object v1, Lmaps/e/ag;->a:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->N:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->h()Z

    move-result v0

    sget-object v1, Lmaps/e/ag;->a:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->h()Z

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->R:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/CircleOptions;->g()F

    move-result v0

    sget-object v1, Lmaps/e/ag;->a:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->g()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->Q:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->K:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->a(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(D)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->M:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "radius is negative"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lmaps/e/ag;->h:D

    invoke-virtual {p0}, Lmaps/e/ag;->k()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->N:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/ag;->d:F

    iget-object v0, p0, Lmaps/e/ag;->i:Lmaps/ay/x;

    float-to-int v1, p1

    invoke-virtual {v0, v1}, Lmaps/ay/x;->d(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->O:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/ag;->e:I

    iget-object v0, p0, Lmaps/e/ag;->i:Lmaps/ay/x;

    invoke-static {p1}, Lmaps/i/a;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/x;->b(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->L:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/e/ag;->g:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0}, Lmaps/e/ag;->k()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/ar/a;Lmaps/as/a;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/ag;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/e/ag;->i:Lmaps/ay/x;

    invoke-virtual {v0, p1, p2}, Lmaps/ay/x;->b(Lmaps/ar/a;Lmaps/as/a;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/e/ag;->m:Lmaps/as/a;

    iput-object p2, p0, Lmaps/e/ag;->n:Lmaps/ap/n;

    iget-object v0, p0, Lmaps/e/ag;->i:Lmaps/ay/x;

    invoke-virtual {v0, p1, p2}, Lmaps/ay/x;->a(Lmaps/as/a;Lmaps/ap/n;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/ag;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/e/ag;->i:Lmaps/ay/x;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/x;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->R:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/e/ag;->k:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lewi;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/ag;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->Q:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->b(Lmaps/e/bq;)V

    iput p1, p0, Lmaps/e/ag;->j:F

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->c(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ag;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->P:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/ag;->f:I

    iget-object v0, p0, Lmaps/e/ag;->i:Lmaps/ay/x;

    invoke-static {p1}, Lmaps/i/a;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/x;->c(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Z)V
    .locals 0

    return-void
.end method

.method public final c()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/ag;->g:Lcom/google/android/gms/maps/model/LatLng;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public final d()D
    .locals 2

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lmaps/e/ag;->h:D

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()F
    .locals 1

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/ag;->d:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/ag;->e:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/ag;->f:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()F
    .locals 1

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget v0, p0, Lmaps/e/ag;->j:F

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/ag;->k:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method final k()V
    .locals 22

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/e/ag;->g:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lmaps/e/ag;->h:D

    const/16 v5, 0x3e8

    new-array v5, v5, [I

    iget-wide v6, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    iget-wide v8, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    const-wide v10, 0x41584db040000000L    # 6371009.0

    div-double v2, v3, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    const/16 v2, 0x1f4

    if-ge v4, v2, :cond_2

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    int-to-double v0, v4

    move-wide/from16 v16, v0

    mul-double v2, v2, v16

    const-wide v16, 0x407f300000000000L    # 499.0

    div-double v2, v2, v16

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double v18, v6, v10

    mul-double v20, v14, v12

    mul-double v16, v16, v20

    add-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->asin(D)D

    move-result-wide v18

    mul-double/2addr v2, v12

    mul-double/2addr v2, v14

    mul-double v16, v16, v6

    sub-double v16, v10, v16

    move-wide/from16 v0, v16

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    add-double/2addr v2, v8

    :goto_1
    const-wide v16, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpg-double v16, v2, v16

    if-gez v16, :cond_0

    const-wide v16, 0x401921fb54442d18L    # 6.283185307179586

    add-double v2, v2, v16

    goto :goto_1

    :cond_0
    :goto_2
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    cmpl-double v16, v2, v16

    if-lez v16, :cond_1

    const-wide v16, 0x401921fb54442d18L    # 6.283185307179586

    sub-double v2, v2, v16

    goto :goto_2

    :cond_1
    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    mul-double v16, v16, v18

    const-wide v18, 0x3fe921fb54442d18L    # 0.7853981633974483

    add-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->tan(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->log(D)D

    move-result-wide v16

    rsub-int v0, v4, 0x1f4

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    mul-int/lit8 v18, v18, 0x2

    const-wide v19, 0x41a45f306dc9c883L    # 1.708913188941079E8

    mul-double v2, v2, v19

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v5, v18

    rsub-int v2, v4, 0x1f4

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    const-wide v18, 0x41a45f306dc9c883L    # 1.708913188941079E8

    mul-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->round(D)J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v3, v0

    aput v3, v5, v2

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    :cond_2
    invoke-static {v5}, Lmaps/ac/az;->a([I)Lmaps/ac/az;

    move-result-object v3

    new-instance v2, Lmaps/ay/x;

    sget-object v4, Lmaps/e/ag;->o:Ljava/util/List;

    move-object/from16 v0, p0

    iget v5, v0, Lmaps/e/ag;->d:F

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lmaps/e/ag;->e:I

    invoke-static {v6}, Lmaps/i/a;->a(I)I

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lmaps/e/ag;->f:I

    invoke-static {v7}, Lmaps/i/a;->a(I)I

    move-result v7

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lmaps/ay/x;-><init>(Lmaps/ac/az;Ljava/util/List;IIIZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/e/ag;->i:Lmaps/ay/x;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/e/ag;->m:Lmaps/as/a;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/e/ag;->n:Lmaps/ap/n;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/e/ag;->i:Lmaps/ay/x;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/e/ag;->m:Lmaps/as/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/e/ag;->n:Lmaps/ap/n;

    invoke-virtual {v2, v3, v4}, Lmaps/ay/x;->a(Lmaps/as/a;Lmaps/ap/n;)V

    :cond_3
    return-void
.end method

.method public final l()V
    .locals 0

    return-void
.end method

.method public final m()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized n()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/ag;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/ag;->i:Lmaps/ay/x;

    invoke-virtual {v0}, Lmaps/ay/x;->ar_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final o()V
    .locals 0

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lmaps/e/ag;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "center"

    iget-object v2, p0, Lmaps/e/ag;->g:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "radius"

    iget-wide v2, p0, Lmaps/e/ag;->h:D

    invoke-virtual {v0, v1, v2, v3}, Lmaps/k/k;->a(Ljava/lang/String;D)Lmaps/k/k;

    move-result-object v0

    const-string v1, "strokeWidth"

    iget v2, p0, Lmaps/e/ag;->d:F

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;F)Lmaps/k/k;

    move-result-object v0

    const-string v1, "strokeColor"

    iget v2, p0, Lmaps/e/ag;->e:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    const-string v1, "fillColor"

    iget v2, p0, Lmaps/e/ag;->f:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
