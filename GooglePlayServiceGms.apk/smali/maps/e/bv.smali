.class final Lmaps/e/bv;
.super Lewv;

# interfaces
.implements Lmaps/e/bq;


# static fields
.field private static final a:Lcom/google/android/gms/maps/model/PolylineOptions;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lmaps/e/bs;

.field private d:F

.field private e:I

.field private final f:Ljava/util/List;

.field private final g:Ljava/util/List;

.field private final h:Ljava/util/List;

.field private i:F

.field private j:Z

.field private k:Z

.field private final l:Lmaps/h/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    sput-object v0, Lmaps/e/bv;->a:Lcom/google/android/gms/maps/model/PolylineOptions;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/PolylineOptions;Lmaps/e/bs;Lmaps/h/a;)V
    .locals 2

    invoke-direct {p0}, Lewv;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/bv;->g:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/bv;->h:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->c()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lmaps/e/bv;->b:Ljava/lang/String;

    iput-object p3, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    iput-object p4, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->c()F

    move-result v0

    iput v0, p0, Lmaps/e/bv;->d:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->d()I

    move-result v0

    iput v0, p0, Lmaps/e/bv;->e:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->e()F

    move-result v0

    iput v0, p0, Lmaps/e/bv;->i:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->f()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/bv;->j:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->g()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/bv;->k:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bv;->f:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->d()I

    move-result v0

    sget-object v1, Lmaps/e/bv;->a:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->v:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->c()F

    move-result v0

    sget-object v1, Lmaps/e/bv;->a:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->c()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->u:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->g()Z

    move-result v0

    sget-object v1, Lmaps/e/bv;->a:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->g()Z

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->y:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->f()Z

    move-result v0

    sget-object v1, Lmaps/e/bv;->a:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->f()Z

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->x:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolylineOptions;->e()F

    move-result v0

    sget-object v1, Lmaps/e/bv;->a:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->e()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->w:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_4
    return-void

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static a(Ljava/util/List;FILjava/util/List;)V
    .locals 4

    invoke-interface {p3}, Ljava/util/List;->clear()V

    invoke-static {p2}, Lmaps/i/a;->a(I)I

    move-result v1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v3

    if-lez v3, :cond_0

    new-instance v3, Lmaps/ay/q;

    invoke-direct {v3, v0, p1, v1}, Lmaps/ay/q;-><init>(Lmaps/ac/az;FI)V

    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->s:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->a(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->u:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/bv;->d:F

    iget-object v0, p0, Lmaps/e/bv;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/q;

    invoke-virtual {v0, p1}, Lmaps/ay/q;->a(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void
.end method

.method public final a(I)V
    .locals 3

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->v:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/bv;->e:I

    iget-object v0, p0, Lmaps/e/bv;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/q;

    invoke-static {p1}, Lmaps/i/a;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lmaps/ay/q;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->t:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bv;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/e/bv;->f:Ljava/util/List;

    invoke-static {v0, p1}, Lmaps/m/bz;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    invoke-virtual {p0}, Lmaps/e/bv;->j()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/ar/a;Lmaps/as/a;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bv;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/e/bv;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/q;

    invoke-virtual {v0, p1, p2}, Lmaps/ay/q;->b(Lmaps/ar/a;Lmaps/as/a;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 0

    return-void
.end method

.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bv;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/e/bv;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/q;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/q;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->x:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/e/bv;->j:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lewu;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/bv;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->w:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->b(Lmaps/e/bq;)V

    iput p1, p0, Lmaps/e/bv;->i:F

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->c(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Z)V
    .locals 0

    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bv;->f:Ljava/util/List;

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public final d()F
    .locals 1

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/bv;->d:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d_(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/bv;->l:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->y:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bv;->k:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/e/bv;->k:Z

    invoke-virtual {p0}, Lmaps/e/bv;->j()V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/bv;->e:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bv;->j:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bv;->k:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()F
    .locals 1

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget v0, p0, Lmaps/e/bv;->i:F

    return v0
.end method

.method public final i()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method final declared-synchronized j()V
    .locals 14

    const/high16 v13, 0x40000000    # 2.0f

    const/high16 v12, 0x20000000

    const v11, 0x1fffffff

    const/high16 v10, -0x20000000

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bv;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/bv;->f:Ljava/util/List;

    invoke-static {v0}, Lmaps/i/b;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    :goto_0
    iget-object v4, p0, Lmaps/e/bv;->g:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    new-instance v1, Lmaps/ac/bb;

    invoke-direct {v1}, Lmaps/ac/bb;-><init>()V

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v3

    if-eqz v2, :cond_0

    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v0

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v6

    sub-int/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v12, :cond_2

    :cond_0
    invoke-virtual {v1, v3}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    move-object v0, v1

    :goto_2
    move-object v2, v3

    move-object v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmaps/e/bv;->f:Ljava/util/List;

    move-object v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v0

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v6

    sub-int/2addr v0, v6

    if-le v0, v12, :cond_3

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v0

    invoke-virtual {v2}, Lmaps/ac/av;->g()I

    move-result v6

    sub-int/2addr v0, v6

    int-to-float v0, v0

    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v6

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v6, v13

    int-to-float v6, v6

    div-float/2addr v0, v6

    new-instance v6, Lmaps/ac/av;

    const/high16 v7, -0x20000000

    invoke-virtual {v2}, Lmaps/ac/av;->g()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v2

    sub-int v2, v10, v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    add-float/2addr v2, v8

    float-to-int v2, v2

    invoke-direct {v6, v7, v2}, Lmaps/ac/av;-><init>(II)V

    new-instance v2, Lmaps/ac/av;

    const v7, 0x1fffffff

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v9

    sub-int v9, v12, v9

    int-to-float v9, v9

    mul-float/2addr v0, v9

    add-float/2addr v0, v8

    float-to-int v0, v0

    invoke-direct {v2, v7, v0}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v1, v6}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v1}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/ac/bb;

    invoke-direct {v0}, Lmaps/ac/bb;-><init>()V

    invoke-virtual {v0, v2}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v0, v3}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_1
    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v0

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v6

    sub-int/2addr v0, v6

    if-ge v0, v10, :cond_4

    invoke-virtual {v2}, Lmaps/ac/av;->g()I

    move-result v0

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v6

    sub-int/2addr v0, v6

    int-to-float v0, v0

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v6

    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v6, v13

    int-to-float v6, v6

    div-float/2addr v0, v6

    new-instance v6, Lmaps/ac/av;

    const v7, 0x1fffffff

    invoke-virtual {v2}, Lmaps/ac/av;->g()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v2

    sub-int v2, v11, v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    add-float/2addr v2, v8

    float-to-int v2, v2

    invoke-direct {v6, v7, v2}, Lmaps/ac/av;-><init>(II)V

    new-instance v2, Lmaps/ac/av;

    const/high16 v7, -0x20000000

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v9

    sub-int v9, v10, v9

    int-to-float v9, v9

    mul-float/2addr v0, v9

    add-float/2addr v0, v8

    float-to-int v0, v0

    invoke-direct {v2, v7, v0}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v1, v6}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v1}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/ac/bb;

    invoke-direct {v0}, Lmaps/ac/bb;-><init>()V

    invoke-virtual {v0, v2}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v0, v3}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    goto/16 :goto_2

    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    invoke-virtual {v1}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/e/bv;->g:Ljava/util/List;

    iget v1, p0, Lmaps/e/bv;->d:F

    iget v2, p0, Lmaps/e/bv;->e:I

    iget-object v3, p0, Lmaps/e/bv;->h:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lmaps/e/bv;->a(Ljava/util/List;FILjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public final l()V
    .locals 0

    return-void
.end method

.method public final m()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized n()Z
    .locals 3

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bv;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/e/bv;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/q;

    invoke-virtual {v0}, Lmaps/ay/q;->ar_()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final o()V
    .locals 0

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lmaps/e/bv;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "points"

    iget-object v2, p0, Lmaps/e/bv;->f:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "width"

    iget v2, p0, Lmaps/e/bv;->d:F

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;F)Lmaps/k/k;

    move-result-object v0

    const-string v1, "color"

    iget v2, p0, Lmaps/e/bv;->e:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
