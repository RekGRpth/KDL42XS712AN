.class public Lmaps/e/bz;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/e/bz;->b:Landroid/content/Context;

    iput-object p2, p0, Lmaps/e/bz;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lmaps/bv/c;)Lmaps/bv/a;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bz;->b:Landroid/content/Context;

    iget-object v2, p0, Lmaps/e/bz;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_1
    new-instance v0, Lmaps/bv/a;

    invoke-direct {v0, p1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v2}, Lmaps/bt/i;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/bv/a;->a([B)Lmaps/bv/a;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    invoke-static {v2}, Lmaps/bt/i;->b(Ljava/io/InputStream;)V

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    :try_start_3
    iget-object v2, p0, Lmaps/e/bz;->b:Landroid/content/Context;

    iget-object v3, p0, Lmaps/e/bz;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v1}, Lmaps/bt/i;->b(Ljava/io/InputStream;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, Lmaps/bt/i;->b(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_2

    :catch_2
    move-exception v1

    move-object v1, v2

    goto :goto_2

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lmaps/bv/a;)V
    .locals 5

    if-nez p1, :cond_0

    iget-object v0, p0, Lmaps/e/bz;->b:Landroid/content/Context;

    iget-object v1, p0, Lmaps/e/bz;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lmaps/e/bz;->b:Landroid/content/Context;

    iget-object v2, p0, Lmaps/e/bz;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-virtual {p1}, Lmaps/bv/a;->c()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v0}, Lmaps/bt/i;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    iget-object v1, p0, Lmaps/e/bz;->b:Landroid/content/Context;

    iget-object v2, p0, Lmaps/e/bz;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v0}, Lmaps/bt/i;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    invoke-static {v1}, Lmaps/bt/i;->a(Ljava/io/OutputStream;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1
.end method
