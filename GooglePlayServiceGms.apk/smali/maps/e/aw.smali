.class final Lmaps/e/aw;
.super Leua;


# instance fields
.field private final a:Lmaps/g/e;

.field private final b:Lmaps/c/l;


# direct methods
.method constructor <init>(Lmaps/c/l;Lmaps/g/e;)V
    .locals 0

    invoke-direct {p0}, Leua;-><init>()V

    iput-object p2, p0, Lmaps/e/aw;->a:Lmaps/g/e;

    iput-object p1, p0, Lmaps/e/aw;->b:Lmaps/c/l;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/e/aw;->a:Lmaps/g/e;

    iget v0, p1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    iget-object v4, p0, Lmaps/e/aw;->b:Lmaps/c/l;

    iget-object v5, p1, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v5}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/c/l;->b(Lmaps/ac/av;)F

    move-result v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lmaps/g/e;->b(Z)V

    iget-object v0, p0, Lmaps/e/aw;->a:Lmaps/g/e;

    iget v3, p1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    iget-object v4, p0, Lmaps/e/aw;->b:Lmaps/c/l;

    invoke-virtual {v4}, Lmaps/c/l;->e()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lmaps/g/e;->c(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
