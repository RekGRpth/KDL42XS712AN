.class public final Lmaps/e/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;
.implements Lmaps/bn/l;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Lmaps/bn/d;

.field private final e:Lmaps/bs/b;

.field private final f:Ljava/util/concurrent/ScheduledExecutorService;

.field private g:Ljava/util/concurrent/Future;

.field private final h:Ljava/util/Random;

.field private i:I

.field private volatile j:Lesx;

.field private k:J

.field private l:Z

.field private final m:Ljava/lang/Runnable;

.field private final n:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/e/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/e/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lmaps/bn/d;Lmaps/bs/b;Ljava/util/Random;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/e/a;->l:Z

    new-instance v0, Lmaps/e/b;

    invoke-direct {v0, p0}, Lmaps/e/b;-><init>(Lmaps/e/a;)V

    iput-object v0, p0, Lmaps/e/a;->m:Ljava/lang/Runnable;

    new-instance v0, Lmaps/e/c;

    invoke-direct {v0, p0}, Lmaps/e/c;-><init>(Lmaps/e/a;)V

    iput-object v0, p0, Lmaps/e/a;->n:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lmaps/e/a;->b:Landroid/content/Context;

    iput-object p2, p0, Lmaps/e/a;->c:Ljava/lang/String;

    iput-object p3, p0, Lmaps/e/a;->d:Lmaps/bn/d;

    iput-object p4, p0, Lmaps/e/a;->e:Lmaps/bs/b;

    iput-object p5, p0, Lmaps/e/a;->h:Ljava/util/Random;

    iput-object p6, p0, Lmaps/e/a;->f:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method

.method private a(Ljava/io/FileInputStream;)Landroid/util/Pair;
    .locals 7

    const/4 v0, 0x0

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lmaps/e/a;->c:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    :goto_0
    return-object v0

    :catch_0
    move-exception v5

    :cond_0
    :try_start_3
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v2

    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_6
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :goto_1
    throw v0

    :catch_4
    move-exception v1

    goto :goto_0

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static synthetic a(Lmaps/e/a;Lesx;)Lesx;
    .locals 0

    iput-object p1, p0, Lmaps/e/a;->j:Lesx;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lmaps/bn/d;)Lmaps/e/a;
    .locals 7

    new-instance v4, Lmaps/bs/b;

    invoke-direct {v4}, Lmaps/bs/b;-><init>()V

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    new-instance v0, Lmaps/e/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lmaps/e/a;-><init>(Landroid/content/Context;Ljava/lang/String;Lmaps/bn/d;Lmaps/bs/b;Ljava/util/Random;Ljava/util/concurrent/ScheduledExecutorService;)V

    return-object v0
.end method

.method private a(J)V
    .locals 2

    iget-object v0, p0, Lmaps/e/a;->g:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/a;->g:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/a;->g:Ljava/util/concurrent/Future;

    invoke-virtual {p0}, Lmaps/e/a;->run()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/e/a;->f:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p0, p1, p2, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/a;->g:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;J)V
    .locals 3

    if-nez p1, :cond_1

    iget-object v0, p0, Lmaps/e/a;->b:Landroid/content/Context;

    const-string v1, "_m_t"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lmaps/e/a;->b:Landroid/content/Context;

    const-string v1, "_m_t"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_1
    invoke-virtual {v1, p2, p3}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/e/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_1
    throw v0

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/e/a;)V
    .locals 0

    invoke-direct {p0}, Lmaps/e/a;->f()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 4

    const/4 v1, 0x1

    const-string v0, "android.permission.ACCESS_NETWORK_STATE"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-virtual {p0, v0, v2, v3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private declared-synchronized b(Ljava/lang/String;J)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/a;->d:Lmaps/bn/d;

    invoke-virtual {v0, p1}, Lmaps/bn/d;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/e/a;->d:Lmaps/bn/d;

    invoke-virtual {v0, p0}, Lmaps/bn/d;->a(Lmaps/bn/l;)V

    iget-object v0, p0, Lmaps/e/a;->d:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->g()V

    iget-object v0, p0, Lmaps/e/a;->d:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->g()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/e/a;->i:I

    const-wide/32 v0, 0x493e0

    sub-long v0, p2, v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    invoke-direct {p0, v0, v1}, Lmaps/e/a;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lmaps/e/a;)V
    .locals 10

    const/4 v4, -0x1

    invoke-static {}, Lmaps/i/g;->d()V

    :try_start_0
    iget-object v0, p0, Lmaps/e/a;->j:Lesx;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "PACKAGE_NAME"

    iget-object v3, p0, Lmaps/e/a;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "API_KEY"

    iget-object v3, p0, Lmaps/e/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lesx;->a(Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lmaps/e/a;->j:Lesx;

    iget-object v1, p0, Lmaps/e/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lmaps/e/a;->n:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_0
    const-string v1, "ERROR_CODE"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getShort(Ljava/lang/String;S)S

    move-result v1

    if-eq v1, v4, :cond_0

    packed-switch v1, :pswitch_data_0

    invoke-direct {p0}, Lmaps/e/a;->d()V

    invoke-direct {p0}, Lmaps/e/a;->g()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-direct {p0}, Lmaps/e/a;->g()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lmaps/e/a;->j:Lesx;

    iget-object v0, p0, Lmaps/e/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lmaps/e/a;->n:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, Lmaps/e/a;->j:Lesx;

    iget-object v1, p0, Lmaps/e/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lmaps/e/a;->n:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_2
    throw v0

    :pswitch_0
    iget-object v0, p0, Lmaps/e/a;->h:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    const-wide v2, 0x40b3880000000000L    # 5000.0

    mul-double/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    const-wide v4, 0x40c3880000000000L    # 10000.0

    const-wide v6, 0x3ff999999999999aL    # 1.6

    iget v8, p0, Lmaps/e/a;->i:I

    int-to-double v8, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v0, v4

    double-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lmaps/e/a;->a(J)V

    iget v0, p0, Lmaps/e/a;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/e/a;->i:I

    goto :goto_1

    :cond_0
    const-string v1, "API_TOKEN"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v1, "VALIDITY_DURATION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "VALIDITY_DURATION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, Lmaps/e/a;->e:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v0

    add-long/2addr v0, v2

    :goto_3
    invoke-direct {p0, v4, v2, v3}, Lmaps/e/a;->b(Ljava/lang/String;J)V

    invoke-direct {p0, v4, v0, v1}, Lmaps/e/a;->a(Ljava/lang/String;J)V

    goto :goto_1

    :cond_1
    const-string v1, "EXPIRY_TIME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lmaps/e/a;->e:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v2

    sub-long v2, v0, v2

    goto :goto_3

    :cond_2
    invoke-direct {p0}, Lmaps/e/a;->g()V

    goto/16 :goto_1

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic c(Lmaps/e/a;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lmaps/e/a;->m:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Lmaps/e/a;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    iget-object v0, p0, Lmaps/e/a;->f:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method private d()V
    .locals 6

    const/4 v5, 0x6

    iget-boolean v0, p0, Lmaps/e/a;->l:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "Authorization failure.  Please see https://developers.google.com/maps/documentation/android/start for how to correctly set up the map."

    invoke-static {v5, v0}, Lmaps/i/f;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lmaps/e/a;->c:Ljava/lang/String;

    iget-object v1, p0, Lmaps/e/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/a;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2, v1}, Lmaps/bc/c;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ensure that the following correspond to what is in the API Console: Package Name: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", API Key: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Certificate Fingerprint: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lmaps/i/f;->a(ILjava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/a;->l:Z

    goto :goto_0
.end method

.method private e()Landroid/util/Pair;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lmaps/e/a;->b:Landroid/content/Context;

    const-string v1, "_m_t"

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/e/a;->a(Ljava/io/FileInputStream;)Landroid/util/Pair;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms"

    const-string v2, "com.google.android.gms.maps.auth.ApiTokenService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lmaps/e/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lmaps/e/a;->n:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/e/a;->g()V

    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lmaps/e/a;->g:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/a;->g:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/a;->e:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/e/a;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v2, p0, Lmaps/e/a;->d:Lmaps/bn/d;

    invoke-virtual {v2}, Lmaps/bn/d;->e()Z

    move-result v2

    if-nez v2, :cond_0

    iput-wide v0, p0, Lmaps/e/a;->k:J

    iget-object v0, p0, Lmaps/e/a;->d:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->f()V

    iget-object v0, p0, Lmaps/e/a;->d:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->f()V

    const/4 v0, 0x0

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lmaps/e/a;->a(Ljava/lang/String;J)V

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/e/a;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/bn/c;)V
    .locals 0

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/e/a;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lmaps/bn/c;)V
    .locals 0

    return-void
.end method

.method public final declared-synchronized c()V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/e/a;->e()Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_0

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/e/a;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lmaps/e/a;->e:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x493e0

    cmp-long v3, v1, v3

    if-gtz v3, :cond_1

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/e/a;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-direct {p0, v0, v1, v2}, Lmaps/e/a;->b(Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized run()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lmaps/e/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/e/a;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lmaps/e/d;

    invoke-direct {v0, p0}, Lmaps/e/d;-><init>(Lmaps/e/a;)V

    iget-object v1, p0, Lmaps/e/a;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lmaps/e/d;->a(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
