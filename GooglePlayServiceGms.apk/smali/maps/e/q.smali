.class final Lmaps/e/q;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ap/d;
.implements Lmaps/ay/az;
.implements Lmaps/e/p;


# static fields
.field private static synthetic h:Z


# instance fields
.field private final a:Lmaps/c/l;

.field private final b:Lmaps/c/m;

.field private final c:Ljava/util/concurrent/Executor;

.field private d:Lete;

.field private e:Letz;

.field private f:Ljava/util/Collection;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/e/q;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/e/q;->h:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lmaps/c/m;Lmaps/c/l;Ljava/util/concurrent/Executor;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/e/q;->b:Lmaps/c/m;

    iput-object p2, p0, Lmaps/e/q;->a:Lmaps/c/l;

    iput-object p3, p0, Lmaps/e/q;->c:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic a(Lmaps/e/q;)Lmaps/c/l;
    .locals 1

    iget-object v0, p0, Lmaps/e/q;->a:Lmaps/c/l;

    return-object v0
.end method

.method public static a(Lmaps/c/m;Lmaps/c/l;Ljava/util/concurrent/Executor;)Lmaps/e/q;
    .locals 1

    new-instance v0, Lmaps/e/q;

    invoke-direct {v0, p0, p1, p2}, Lmaps/e/q;-><init>(Lmaps/c/m;Lmaps/c/l;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method static synthetic a(Lmaps/e/q;Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Letz;

    :try_start_0
    invoke-interface {v0, p1}, Letz;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lmaps/e/q;->d:Lete;

    if-eqz v0, :cond_1

    :try_start_1
    invoke-direct {p0}, Lmaps/e/q;->e()Lete;

    move-result-object v0

    invoke-interface {v0}, Lete;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    iget-object v0, p0, Lmaps/e/q;->e:Letz;

    if-eqz v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lmaps/e/q;->e:Letz;

    invoke-interface {v0, p1}, Letz;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    return-void

    :catch_1
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method private d()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/e/q;->d:Lete;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/q;->e:Letz;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lmaps/e/q;->a:Lmaps/c/l;

    if-eqz v0, :cond_2

    move-object v1, p0

    :goto_1
    invoke-virtual {v3, v1}, Lmaps/c/l;->a(Lmaps/ap/d;)V

    iget-object v1, p0, Lmaps/e/q;->a:Lmaps/c/l;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v1, p0}, Lmaps/c/l;->a(Lmaps/ay/az;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_1

    :cond_3
    move-object p0, v2

    goto :goto_2
.end method

.method private e()Lete;
    .locals 2

    iget-object v0, p0, Lmaps/e/q;->d:Lete;

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/e/q;->d:Lete;

    invoke-direct {p0}, Lmaps/e/q;->d()V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lmaps/e/q;->d:Lete;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/e/q;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/e/q;->g:I

    :try_start_0
    invoke-direct {p0}, Lmaps/e/q;->e()Lete;

    move-result-object v0

    invoke-interface {v0}, Lete;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lmaps/e/q;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/e/q;->g:I

    :cond_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    iget v1, p0, Lmaps/e/q;->g:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lmaps/e/q;->g:I

    throw v0
.end method

.method public final a(Letz;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    :cond_0
    iget-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lmaps/e/q;->d()V

    return-void
.end method

.method public final a(Lmaps/ar/b;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/q;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lmaps/e/r;

    invoke-direct {v1, p0, p1}, Lmaps/e/r;-><init>(Lmaps/e/q;Lmaps/ar/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lmaps/e/af;ILete;Lmaps/h/a;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Callback supplied with instantaneous camera movement"

    invoke-static {v0, v3}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    iget v0, p0, Lmaps/e/q;->g:I

    if-nez v0, :cond_2

    :goto_1
    const-string v0, "Camera moved during a cancellation"

    invoke-static {v2, v0}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/e/q;->b:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/q;->a:Lmaps/c/l;

    invoke-virtual {p1, v0, v1, p2, p4}, Lmaps/e/af;->a(Lmaps/c/m;Lmaps/c/l;ILmaps/h/a;)V

    sget-boolean v0, Lmaps/e/q;->h:Z

    if-nez v0, :cond_3

    iget v0, p0, Lmaps/e/q;->g:I

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    iput-object p3, p0, Lmaps/e/q;->d:Lete;

    invoke-direct {p0}, Lmaps/e/q;->d()V

    return-void
.end method

.method public final b()V
    .locals 2

    iget v0, p0, Lmaps/e/q;->g:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Camera stopped during a cancellation"

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/e/q;->a:Lmaps/c/l;

    invoke-virtual {v0}, Lmaps/c/l;->f()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Letz;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/q;->f:Ljava/util/Collection;

    :cond_0
    invoke-direct {p0}, Lmaps/e/q;->d()V

    return-void
.end method

.method public final c()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 1

    iget-object v0, p0, Lmaps/e/q;->a:Lmaps/c/l;

    invoke-virtual {v0}, Lmaps/c/l;->c()Lmaps/ar/b;

    move-result-object v0

    invoke-static {v0}, Lmaps/i/a;->a(Lmaps/ar/b;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public final c(Letz;)V
    .locals 0

    iput-object p1, p0, Lmaps/e/q;->e:Letz;

    invoke-direct {p0}, Lmaps/e/q;->d()V

    return-void
.end method
