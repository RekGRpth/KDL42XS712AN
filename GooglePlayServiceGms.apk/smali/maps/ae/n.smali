.class public final Lmaps/ae/n;
.super Lmaps/ax/b;

# interfaces
.implements Lmaps/bn/l;


# static fields
.field private static a:Lmaps/ae/n;


# instance fields
.field private final b:Lmaps/bs/b;

.field private final c:Lmaps/bn/d;

.field private final d:Lmaps/ag/t;

.field private final e:Ljava/io/File;

.field private f:Z

.field private g:Landroid/os/Handler;

.field private final h:Ljava/util/Map;

.field private i:Z


# direct methods
.method private constructor <init>(Lmaps/bn/d;Ljava/io/File;Ljava/util/Locale;Lmaps/bs/b;)V
    .locals 2

    const-string v0, "ibs"

    invoke-direct {p0, v0}, Lmaps/ax/b;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lmaps/ae/n;->c:Lmaps/bn/d;

    iput-object p4, p0, Lmaps/ae/n;->b:Lmaps/bs/b;

    new-instance v0, Lmaps/ag/t;

    iget-object v1, p0, Lmaps/ae/n;->b:Lmaps/bs/b;

    invoke-direct {v0, p3, v1}, Lmaps/ag/t;-><init>(Ljava/util/Locale;Lmaps/bs/b;)V

    iput-object v0, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    iput-object p2, p0, Lmaps/ae/n;->e:Ljava/io/File;

    invoke-static {}, Lmaps/m/co;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ae/n;->h:Ljava/util/Map;

    return-void
.end method

.method public static a(Lmaps/bn/d;Ljava/io/File;Ljava/util/Locale;Lmaps/bs/b;)Lmaps/ae/n;
    .locals 1

    sget-object v0, Lmaps/ae/n;->a:Lmaps/ae/n;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ae/n;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/ae/n;-><init>(Lmaps/bn/d;Ljava/io/File;Ljava/util/Locale;Lmaps/bs/b;)V

    sput-object v0, Lmaps/ae/n;->a:Lmaps/ae/n;

    :cond_0
    sget-object v0, Lmaps/ae/n;->a:Lmaps/ae/n;

    return-object v0
.end method

.method static synthetic a(Lmaps/ae/n;)V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ae/n;->i:Z

    iget-object v0, p0, Lmaps/ae/n;->c:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->f()V

    :try_start_0
    iget-object v0, p0, Lmaps/ae/n;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/af/c;

    invoke-virtual {v0}, Lmaps/af/c;->ah_()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/ae/n;->c:Lmaps/bn/d;

    invoke-virtual {v2, v0}, Lmaps/bn/d;->c(Lmaps/bn/c;)V

    invoke-virtual {v0}, Lmaps/af/c;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ae/n;->c:Lmaps/bn/d;

    invoke-virtual {v1}, Lmaps/bn/d;->g()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/ae/n;->c:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->g()V

    return-void
.end method

.method static synthetic a(Lmaps/ae/n;Lmaps/ae/p;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v1, p1, Lmaps/ae/p;->a:Lmaps/ac/r;

    iget-object v2, p1, Lmaps/ae/p;->b:Lmaps/af/b;

    iget-object v0, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-virtual {v0, v1}, Lmaps/ag/t;->a(Lmaps/ac/r;)Lmaps/ac/z;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    iget-object v3, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-static {v0}, Lmaps/ag/t;->a(Lmaps/ac/z;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-interface {v2, v1, v3, v4}, Lmaps/af/b;->a(Lmaps/ac/r;ILmaps/ac/z;)V

    :cond_0
    :goto_0
    iget-object v3, p0, Lmaps/ae/n;->b:Lmaps/bs/b;

    invoke-virtual {v0}, Lmaps/ac/z;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    iget-object v0, p0, Lmaps/ae/n;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/af/c;

    if-nez v0, :cond_2

    new-instance v0, Lmaps/af/c;

    invoke-direct {v0, v1}, Lmaps/af/c;-><init>(Lmaps/ac/r;)V

    iget-object v3, p0, Lmaps/ae/n;->h:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v0, v2}, Lmaps/af/c;->a(Lmaps/af/b;)V

    :cond_3
    invoke-virtual {v0}, Lmaps/af/c;->ah_()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lmaps/ae/n;->i:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v5, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iput-boolean v5, p0, Lmaps/ae/n;->i:Z

    :cond_4
    return-void

    :cond_5
    const/4 v3, 0x0

    invoke-interface {v2, v1, v3, v0}, Lmaps/af/b;->a(Lmaps/ac/r;ILmaps/ac/z;)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/ae/n;Lmaps/af/c;)V
    .locals 3

    iget-object v0, p0, Lmaps/ae/n;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/af/c;->a()Lmaps/ac/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/af/c;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-virtual {p1}, Lmaps/af/c;->a()Lmaps/ac/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ag/t;->c(Lmaps/ac/r;)V

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Lmaps/af/c;->a(Lmaps/ac/z;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/af/c;->e()Lmaps/bv/a;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-virtual {p1}, Lmaps/af/c;->a()Lmaps/ac/r;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lmaps/ag/t;->a(Lmaps/ac/r;Lmaps/bv/a;)Lmaps/ac/z;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/ae/n;Lmaps/af/c;)V
    .locals 2

    iget-object v0, p0, Lmaps/ae/n;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/af/c;->a()Lmaps/ac/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmaps/af/c;->a(Lmaps/ac/z;)V

    return-void
.end method

.method public static c()Lmaps/ae/n;
    .locals 1

    sget-object v0, Lmaps/ae/n;->a:Lmaps/ae/n;

    return-object v0
.end method

.method private h()V
    .locals 1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-boolean v0, p0, Lmaps/ae/n;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lmaps/ac/r;)Lmaps/ac/z;
    .locals 2

    iget-object v0, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-virtual {v0, p1}, Lmaps/ag/t;->b(Lmaps/ac/r;)Lmaps/ac/z;

    move-result-object v0

    iget-object v1, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-static {v0}, Lmaps/ag/t;->a(Lmaps/ac/z;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/ac/r;Lmaps/af/b;)V
    .locals 4

    iget-object v0, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    const/4 v2, 0x0

    new-instance v3, Lmaps/ae/p;

    invoke-direct {v3, p1, p2}, Lmaps/ae/p;-><init>(Lmaps/ac/r;Lmaps/af/b;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Lmaps/bn/c;)V
    .locals 3

    invoke-virtual {p1}, Lmaps/bn/c;->b()I

    move-result v0

    const/16 v1, 0x76

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/bn/c;)V
    .locals 3

    invoke-virtual {p1}, Lmaps/bn/c;->b()I

    move-result v0

    const/16 v1, 0x76

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public final b(Lmaps/ac/r;)Z
    .locals 2

    iget-object v0, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-virtual {v0, p1}, Lmaps/ag/t;->b(Lmaps/ac/r;)Lmaps/ac/z;

    move-result-object v0

    iget-object v1, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-static {v0}, Lmaps/ag/t;->a(Lmaps/ac/z;)Z

    move-result v0

    return v0
.end method

.method public final c(Lmaps/ac/r;)Lmaps/ac/aa;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/ae/n;->a(Lmaps/ac/r;)Lmaps/ac/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmaps/ac/z;->a(Lmaps/ac/r;)Lmaps/ac/aa;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    invoke-virtual {p0}, Lmaps/ae/n;->start()V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    iget-object v0, p0, Lmaps/ae/n;->c:Lmaps/bn/d;

    invoke-virtual {v0, p0}, Lmaps/bn/d;->a(Lmaps/bn/l;)V

    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final e()V
    .locals 4

    :try_start_0
    invoke-static {}, Lmaps/ap/p;->d()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lmaps/ae/o;

    invoke-direct {v0, p0}, Lmaps/ae/o;-><init>(Lmaps/ae/n;)V

    iput-object v0, p0, Lmaps/ae/n;->g:Landroid/os/Handler;

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lmaps/aw/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    iget-object v1, p0, Lmaps/ae/n;->e:Ljava/io/File;

    invoke-virtual {v0, v1}, Lmaps/ag/t;->a(Ljava/io/File;)V

    :cond_0
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lmaps/ae/n;->f:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/ae/n;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not set thread priority: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()V
    .locals 1

    invoke-direct {p0}, Lmaps/ae/n;->h()V

    iget-object v0, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-virtual {v0}, Lmaps/ag/t;->a()V

    return-void
.end method

.method public final g()V
    .locals 1

    invoke-direct {p0}, Lmaps/ae/n;->h()V

    iget-object v0, p0, Lmaps/ae/n;->d:Lmaps/ag/t;

    invoke-virtual {v0}, Lmaps/ag/t;->b()V

    return-void
.end method
