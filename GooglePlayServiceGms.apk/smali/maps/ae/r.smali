.class final Lmaps/ae/r;
.super Lmaps/ae/h;


# instance fields
.field private a:[Lmaps/bv/a;

.field private b:Lmaps/bs/b;


# direct methods
.method constructor <init>(Lmaps/bs/b;)V
    .locals 1

    invoke-direct {p0}, Lmaps/ae/h;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [Lmaps/bv/a;

    iput-object v0, p0, Lmaps/ae/r;->a:[Lmaps/bv/a;

    iput-object p1, p0, Lmaps/ae/r;->b:Lmaps/bs/b;

    return-void
.end method

.method private f()Lmaps/bv/a;
    .locals 11

    const/16 v7, 0x16

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x2

    const/4 v1, 0x0

    new-instance v2, Lmaps/bv/a;

    sget-object v0, Lmaps/cm/m;->a:Lmaps/bv/c;

    invoke-direct {v2, v0}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/16 v0, 0x80

    invoke-virtual {v2, v9, v0}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    iget-object v0, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v0, v0, v1

    iget-object v0, v0, Lmaps/ae/k;->a:Lmaps/ac/bt;

    check-cast v0, Lmaps/ae/s;

    invoke-virtual {v0}, Lmaps/ae/s;->k()Lmaps/ac/ai;

    move-result-object v0

    invoke-virtual {v2, v8}, Lmaps/bv/a;->a(I)Lmaps/bv/a;

    move-result-object v3

    const/16 v4, 0x15

    invoke-virtual {v0}, Lmaps/ac/ai;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    invoke-virtual {v0}, Lmaps/ac/ai;->b()[Ljava/lang/String;

    move-result-object v4

    move v0, v1

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    invoke-virtual {v3, v7}, Lmaps/bv/a;->a(I)Lmaps/bv/a;

    move-result-object v5

    aget-object v6, v4, v0

    invoke-virtual {v5, v9, v6}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    add-int/lit8 v6, v0, 0x1

    aget-object v6, v4, v6

    invoke-virtual {v5, v8, v6}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    invoke-virtual {v3, v7, v5}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    invoke-virtual {v2, v8, v3}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move v0, v1

    :goto_1
    iget v3, p0, Lmaps/ae/h;->d:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v3, v3, v0

    iget-object v3, v3, Lmaps/ae/k;->a:Lmaps/ac/bt;

    new-instance v4, Lmaps/bv/a;

    sget-object v5, Lmaps/cm/y;->e:Lmaps/bv/c;

    invoke-direct {v4, v5}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/16 v5, 0x8

    invoke-virtual {v4, v9, v5}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/16 v5, 0x1e

    invoke-virtual {v3}, Lmaps/ac/bt;->c()I

    move-result v6

    invoke-virtual {v3}, Lmaps/ac/bt;->d()I

    move-result v7

    invoke-virtual {v3}, Lmaps/ac/bt;->b()I

    move-result v3

    invoke-static {v6, v7, v3}, Lmaps/ac/ck;->a(III)J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Lmaps/bv/a;->a(IJ)Lmaps/bv/a;

    invoke-virtual {v4, v8, v1}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    invoke-virtual {v4, v10, v1}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v3, 0x4

    invoke-virtual {v4, v3, v1}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    invoke-virtual {v2, v10, v4}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-object v2
.end method


# virtual methods
.method public final a(I)Lmaps/ac/bs;
    .locals 9

    const/4 v8, 0x3

    iget-object v0, p0, Lmaps/ae/r;->a:[Lmaps/bv/a;

    aget-object v2, v0, p1

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v4, Lmaps/ac/bp;

    invoke-direct {v4}, Lmaps/ac/bp;-><init>()V

    iget-object v0, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v0, v0, p1

    iget-object v0, v0, Lmaps/ae/k;->a:Lmaps/ac/bt;

    check-cast v0, Lmaps/ae/s;

    invoke-virtual {v2, v8}, Lmaps/bv/a;->j(I)I

    move-result v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v8, v1}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v6

    invoke-static {}, Lmaps/ac/bl;->a()Lmaps/ac/bl;

    move-result-object v7

    invoke-static {v6, v7, v0}, Lmaps/ae/q;->a(Lmaps/bv/a;Lmaps/ac/bl;Lmaps/ac/bt;)Lmaps/ac/n;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v7, 0x14

    if-eq v6, v7, :cond_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lmaps/ac/n;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/ac/n;

    invoke-virtual {v0}, Lmaps/ae/s;->k()Lmaps/ac/ai;

    move-result-object v5

    const-wide/16 v2, -0x1

    invoke-virtual {v5}, Lmaps/ac/ai;->c()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v2, p0, Lmaps/ae/r;->b:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v2

    invoke-virtual {v5}, Lmaps/ac/ai;->d()J

    move-result-wide v5

    add-long/2addr v2, v5

    :cond_3
    new-instance v5, Lmaps/ac/ct;

    invoke-direct {v5}, Lmaps/ac/ct;-><init>()V

    invoke-virtual {v5, v4}, Lmaps/ac/ct;->a(Lmaps/ac/bp;)Lmaps/ac/ct;

    move-result-object v4

    invoke-virtual {v4, v0}, Lmaps/ac/ct;->a(Lmaps/ac/bt;)Lmaps/ac/ct;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->a([Lmaps/ac/n;)Lmaps/ac/ct;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lmaps/ac/ct;->a(J)Lmaps/ac/ct;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/ct;->a()Lmaps/ac/cs;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/io/DataOutput;)V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {p0}, Lmaps/ae/r;->f()Lmaps/bv/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/bv/a;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    return-void
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    sget-object v0, Lmaps/cm/m;->b:Lmaps/bv/c;

    invoke-static {v0, p1}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/DataInput;)Lmaps/bv/a;

    move-result-object v1

    invoke-virtual {v1, v6}, Lmaps/bv/a;->j(I)I

    move-result v2

    iget v0, p0, Lmaps/ae/h;->d:I

    if-eq v2, v0, :cond_1

    :cond_0
    return v5

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lmaps/ae/r;->a:[Lmaps/bv/a;

    invoke-virtual {v1, v6, v0}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected final a(Lmaps/ae/k;)Z
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lmaps/ae/h;->d:I

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmaps/ae/k;->a:Lmaps/ac/bt;

    check-cast v0, Lmaps/ae/s;

    iget-object v1, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v1, v1, v2

    iget-object v1, v1, Lmaps/ae/k;->a:Lmaps/ac/bt;

    check-cast v1, Lmaps/ae/s;

    invoke-virtual {v0, v1}, Lmaps/ae/s;->a(Lmaps/ae/s;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x24

    return v0
.end method
