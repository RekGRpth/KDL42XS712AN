.class final Lmaps/ae/w;
.super Lmaps/ae/b;


# direct methods
.method constructor <init>(Lmaps/ae/v;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ae/b;-><init>(Lmaps/ae/a;)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/ae/w;->b:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lmaps/ae/w;->b:[[B

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/ae/w;->b:[[B

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lmaps/ad/b;->a([BI)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    return v0

    :catch_0
    move-exception v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected final a(I)Lmaps/ac/bs;
    .locals 7

    iget-object v0, p0, Lmaps/ae/w;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v0, v0, p1

    iget-object v0, v0, Lmaps/ae/k;->a:Lmaps/ac/bt;

    iget-object v1, p0, Lmaps/ae/w;->b:[[B

    aget-object v1, v1, p1

    const/4 v2, 0x0

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/bf/a;->i()Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v3

    const-wide/32 v5, 0x48190800

    add-long/2addr v3, v5

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/ad/b;->a(Lmaps/ac/bt;[BIJ)Lmaps/ad/b;

    move-result-object v0

    goto :goto_0
.end method

.method protected final b(I)[B
    .locals 1

    iget-object v0, p0, Lmaps/ae/w;->b:[[B

    aget-object v0, v0, p1

    return-object v0
.end method
