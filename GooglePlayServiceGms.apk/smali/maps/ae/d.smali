.class public abstract Lmaps/ae/d;
.super Lmaps/ax/b;

# interfaces
.implements Lmaps/ae/t;
.implements Lmaps/ae/y;
.implements Lmaps/bn/l;


# instance fields
.field private volatile a:Lmaps/ae/i;

.field private final b:Ljava/util/concurrent/locks/ReentrantLock;

.field protected c:Lmaps/ae/aa;

.field protected d:Lmaps/bs/b;

.field protected final e:Lmaps/ao/b;

.field private final g:Lmaps/bn/k;

.field private h:Landroid/os/Handler;

.field private i:Z

.field private j:Lmaps/ae/h;

.field private final k:Ljava/util/List;

.field private final l:Lmaps/ax/f;

.field private final m:Ljava/util/Map;

.field private final n:I

.field private volatile o:I

.field private volatile p:I

.field private volatile q:I

.field private r:Z

.field private s:Lmaps/ba/h;

.field private final t:Ljava/util/ArrayList;

.field private volatile u:Z

.field private final v:Lmaps/af/d;


# direct methods
.method protected constructor <init>(Lmaps/bn/k;Lmaps/ao/b;Ljava/lang/String;Lmaps/ag/aj;Lmaps/ag/f;IZILjava/util/Locale;Ljava/io/File;)V
    .locals 7

    invoke-direct {p0, p3}, Lmaps/ax/b;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lmaps/ae/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/ae/d;->k:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/ae/d;->m:Ljava/util/Map;

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->i()Lmaps/bs/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/ae/d;->d:Lmaps/bs/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ae/d;->r:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ae/d;->u:Z

    new-instance v0, Lmaps/ae/e;

    invoke-direct {v0, p0}, Lmaps/ae/e;-><init>(Lmaps/ae/d;)V

    iput-object v0, p0, Lmaps/ae/d;->v:Lmaps/af/d;

    iput-object p2, p0, Lmaps/ae/d;->e:Lmaps/ao/b;

    new-instance v0, Lmaps/ae/aa;

    invoke-virtual {p0}, Lmaps/ae/d;->getName()Ljava/lang/String;

    move-result-object v1

    move-object v2, p4

    move-object v3, p5

    move v4, p7

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    invoke-direct/range {v0 .. v6}, Lmaps/ae/aa;-><init>(Ljava/lang/String;Lmaps/ag/aj;Lmaps/ag/f;ZLjava/util/Locale;Ljava/io/File;)V

    iput-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iput p6, p0, Lmaps/ae/d;->n:I

    iput-object p1, p0, Lmaps/ae/d;->g:Lmaps/bn/k;

    invoke-virtual {p0}, Lmaps/ae/d;->f()Lmaps/ae/h;

    move-result-object v0

    iput-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    iget-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-static {v0, p0}, Lmaps/ae/h;->a(Lmaps/ae/h;Lmaps/ae/d;)Lmaps/ae/d;

    new-instance v0, Lmaps/ae/f;

    invoke-direct {v0, p0, p8}, Lmaps/ae/f;-><init>(Lmaps/ae/d;I)V

    iput-object v0, p0, Lmaps/ae/d;->l:Lmaps/ax/f;

    return-void
.end method

.method private a(Lmaps/ae/k;ZZ)Landroid/util/Pair;
    .locals 7

    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p1, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-virtual {p0}, Lmaps/ae/d;->k()Lmaps/ao/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ac/bt;->a(Lmaps/ao/b;)Lmaps/ac/bt;

    move-result-object v2

    iget-object v1, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v1, v1, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v1, v1, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v1, v2}, Lmaps/ag/aj;->c(Lmaps/ac/bt;)Lmaps/ac/bs;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v5, p0, Lmaps/ae/d;->d:Lmaps/bs/b;

    invoke-interface {v1}, Lmaps/ac/bs;->d()Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v2, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v2, v1}, Lmaps/ag/aj;->a(Lmaps/ac/bs;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1, v0}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    move v1, v3

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    iget-boolean v2, p1, Lmaps/ae/k;->c:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    invoke-virtual {v2}, Lmaps/ba/h;->b()V

    :cond_1
    invoke-direct {p0, v1, p1, p3}, Lmaps/ae/d;->a(Lmaps/ac/bs;Lmaps/ae/k;Z)Lmaps/ae/k;

    move-result-object v2

    if-nez p3, :cond_3

    iget-boolean v5, p1, Lmaps/ae/k;->c:Z

    if-eqz v5, :cond_2

    :goto_2
    invoke-virtual {p0, p1, v4, v0}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    move-object v0, v2

    move v1, v3

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v2

    move v1, v4

    goto :goto_0

    :cond_4
    if-eqz p2, :cond_b

    iget-object v1, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v1}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-boolean v5, p1, Lmaps/ae/k;->c:Z

    if-eqz v5, :cond_5

    invoke-interface {v1, v2}, Lmaps/ag/f;->b(Lmaps/ac/bt;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0, p1, v4, v0}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_5
    invoke-interface {v1, v2}, Lmaps/ag/f;->c(Lmaps/ac/bt;)Lmaps/ac/bs;

    move-result-object v5

    if-eqz v5, :cond_b

    iget-object v6, p0, Lmaps/ae/d;->d:Lmaps/bs/b;

    invoke-interface {v5}, Lmaps/ac/bs;->d()Z

    move-result v6

    if-nez v6, :cond_b

    invoke-interface {v1, v5}, Lmaps/ag/f;->a(Lmaps/ac/bs;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-boolean v1, p1, Lmaps/ae/k;->c:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    invoke-virtual {v1}, Lmaps/ba/h;->d()V

    :cond_6
    invoke-direct {p0, p1, v2}, Lmaps/ae/d;->a(Lmaps/ae/k;Lmaps/ac/bt;)V

    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_7
    iget-boolean v0, p1, Lmaps/ae/k;->c:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    invoke-virtual {v0}, Lmaps/ba/h;->c()V

    :cond_8
    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v0, v0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v0, v0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v0, v2, v5}, Lmaps/ag/aj;->a(Lmaps/ac/bt;Lmaps/ac/bs;)V

    :cond_9
    invoke-direct {p0, v5, p1, p3}, Lmaps/ae/d;->a(Lmaps/ac/bs;Lmaps/ae/k;Z)Lmaps/ae/k;

    move-result-object v0

    if-nez p3, :cond_a

    invoke-virtual {p0, p1, v4, v5}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    goto :goto_3

    :cond_a
    move v3, v4

    goto :goto_3

    :cond_b
    iget-boolean v1, p1, Lmaps/ae/k;->c:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    invoke-virtual {v1}, Lmaps/ba/h;->d()V

    :cond_c
    const/4 v1, -0x1

    iput v1, p1, Lmaps/ae/k;->i:I

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private a(Lmaps/ac/bs;Lmaps/ae/k;Z)Lmaps/ae/k;
    .locals 12

    const/4 v9, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v0}, Lmaps/ae/aa;->d()I

    move-result v1

    const/4 v0, 0x0

    if-eq v1, v9, :cond_1

    invoke-interface {p1}, Lmaps/ac/bs;->c()I

    move-result v2

    if-eq v1, v2, :cond_1

    move v1, v5

    move v7, v9

    :goto_0
    if-eqz v1, :cond_0

    new-instance v0, Lmaps/ae/k;

    iget-object v1, p0, Lmaps/ae/d;->e:Lmaps/ao/b;

    iget-object v2, p2, Lmaps/ae/k;->a:Lmaps/ac/bt;

    iget-object v3, p0, Lmaps/ae/d;->e:Lmaps/ao/b;

    invoke-virtual {v2, v3}, Lmaps/ac/bt;->a(Lmaps/ao/b;)Lmaps/ac/bt;

    move-result-object v2

    iget-object v3, p0, Lmaps/ae/d;->v:Lmaps/af/d;

    sget-object v4, Lmaps/ae/c;->b:Lmaps/ae/c;

    move v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/ae/k;-><init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;Lmaps/ae/c;ZZIZ)V

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lmaps/ac/bs;->a()Lmaps/ac/bt;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v2

    iget-object v1, p0, Lmaps/ae/d;->g:Lmaps/bn/k;

    invoke-interface {v1}, Lmaps/bn/k;->r()J

    move-result-wide v3

    const-wide/16 v10, 0x64

    rem-long/2addr v3, v10

    const-wide/16 v10, 0x8

    cmp-long v1, v3, v10

    if-nez v1, :cond_0

    if-eq v7, v9, :cond_3

    move v1, v5

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "v="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "d="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lmaps/ac/cs;->r()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "z="

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x6d

    const-string v7, "u"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    aput-object v1, v8, v6

    aput-object v3, v8, v5

    const/4 v1, 0x2

    aput-object v2, v8, v1

    invoke-static {v8}, Lmaps/br/g;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v7, v1}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p2, Lmaps/ae/k;->c:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lmaps/ae/d;->d:Lmaps/bs/b;

    invoke-interface {p1}, Lmaps/ac/bs;->f()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p3, :cond_4

    :cond_2
    invoke-interface {p1}, Lmaps/ac/bs;->e()I

    move-result v7

    move v1, v5

    goto/16 :goto_0

    :cond_3
    move v1, v6

    goto :goto_1

    :cond_4
    move v1, v6

    move v7, v9

    goto/16 :goto_0
.end method

.method static synthetic a(Lmaps/ae/d;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ae/d;->i:Z

    invoke-direct {p0}, Lmaps/ae/d;->d()V

    return-void
.end method

.method static synthetic a(Lmaps/ae/d;Lmaps/ac/bs;)V
    .locals 4

    iget-object v2, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    monitor-enter v2

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ae/z;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmaps/ae/z;->a(Lmaps/ac/bs;)V

    move v0, v1

    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic a(Lmaps/ae/d;Lmaps/ac/bu;Lmaps/ae/c;Lmaps/af/d;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lmaps/ae/u;->a(Lmaps/ae/c;Z)I

    move-result v0

    iget-object v1, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v1}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v1

    :goto_0
    invoke-interface {p1}, Lmaps/ac/bu;->a()Lmaps/ac/bt;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_0

    invoke-interface {v1, v2, p3, v0}, Lmaps/ag/f;->a(Lmaps/ac/bt;Lmaps/af/d;I)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    invoke-interface {p3, v2, v4, v3}, Lmaps/af/d;->a(Lmaps/ac/bt;ILmaps/ac/bs;)V

    goto :goto_0

    :cond_1
    iput-boolean v4, p0, Lmaps/ae/d;->u:Z

    invoke-direct {p0}, Lmaps/ae/d;->g()V

    return-void
.end method

.method static synthetic a(Lmaps/ae/d;Lmaps/ae/h;)V
    .locals 16

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/ae/d;->r:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/ae/d;->r:Z

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ae/d;->l:Lmaps/ax/f;

    invoke-virtual {v1}, Lmaps/ax/f;->e()I

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ae/d;->l:Lmaps/ax/f;

    invoke-virtual {v1}, Lmaps/ax/f;->f()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ae/k;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lmaps/ae/d;->b(Lmaps/ae/k;)V

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmaps/ae/h;->a()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v2}, Lmaps/ae/aa;->d()I

    move-result v2

    if-eq v1, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v2, v1}, Lmaps/ae/aa;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct/range {p0 .. p0}, Lmaps/ae/d;->c()V

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ae/d;->k:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v1}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lmaps/ae/h;->c()I

    move-result v11

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    move v7, v4

    move v8, v5

    move v9, v6

    move v4, v1

    move v5, v2

    move v1, v3

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lmaps/ae/h;->c()I

    move-result v2

    if-ge v4, v2, :cond_14

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lmaps/ae/h;->c(I)Lmaps/ae/k;

    move-result-object v12

    iget-object v2, v12, Lmaps/ae/k;->a:Lmaps/ac/bt;

    iget-object v3, v12, Lmaps/ae/k;->h:Lmaps/ao/b;

    invoke-virtual {v2, v3}, Lmaps/ac/bt;->a(Lmaps/ao/b;)Lmaps/ac/bt;

    move-result-object v13

    iget v2, v12, Lmaps/ae/k;->i:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_19

    add-int/lit8 v6, v1, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ae/d;->m:Ljava/util/Map;

    invoke-interface {v1, v13}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget v1, v0, Lmaps/ae/d;->o:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lmaps/ae/d;->o:I

    :try_start_0
    iget-boolean v1, v12, Lmaps/ae/k;->c:Z

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget v1, v0, Lmaps/ae/d;->q:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lmaps/ae/d;->q:I

    :goto_3
    const/4 v1, 0x0

    if-eqz v10, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lmaps/ae/h;->b(I)[B

    move-result-object v2

    if-eqz v2, :cond_2

    array-length v1, v2

    new-array v1, v1, [B

    const/4 v3, 0x0

    const/4 v14, 0x0

    array-length v15, v2

    invoke-static {v2, v3, v1, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    invoke-static {v12}, Lmaps/ae/d;->c(Lmaps/ae/k;)Lmaps/ao/b;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lmaps/ae/h;->a(I)Lmaps/ac/bs;

    move-result-object v3

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v2, :cond_4

    iget-boolean v2, v12, Lmaps/ae/k;->c:Z

    if-nez v2, :cond_4

    invoke-interface {v3}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v2

    if-eq v2, v14, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v2, v13, v3}, Lmaps/ag/aj;->a(Lmaps/ac/bt;Lmaps/ac/bs;)V

    :cond_4
    if-eqz v10, :cond_6

    invoke-interface {v3}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v2

    if-eq v2, v14, :cond_6

    :cond_5
    invoke-interface {v10, v13, v3, v1}, Lmaps/ag/f;->a(Lmaps/ac/bt;Lmaps/ac/bs;[B)V

    :cond_6
    invoke-interface {v3}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-interface {v3}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v1

    if-ne v1, v14, :cond_18

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ae/d;->e:Lmaps/ao/b;

    invoke-virtual {v13, v1}, Lmaps/ac/bt;->a(Lmaps/ao/b;)Lmaps/ac/bt;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v2, v1}, Lmaps/ag/aj;->c(Lmaps/ac/bt;)Lmaps/ac/bs;

    move-result-object v1

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v2, v1}, Lmaps/ag/aj;->a(Lmaps/ac/bs;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_7
    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v1, v3}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    move v2, v5

    move v3, v7

    move v5, v8

    move v7, v9

    :goto_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v8, v5

    move v9, v7

    move v7, v3

    move v5, v2

    move v1, v6

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget v1, v0, Lmaps/ae/d;->p:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lmaps/ae/d;->p:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lmaps/ae/d;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Could not parse tile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v1, v2}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    move v2, v5

    move v3, v7

    move v5, v8

    move v7, v9

    goto :goto_4

    :cond_9
    :try_start_1
    check-cast v1, Lmaps/ac/cs;

    move-object v0, v3

    check-cast v0, Lmaps/ac/cs;

    move-object v2, v0

    invoke-static {v1, v2}, Lmaps/ac/as;->b(Lmaps/ac/cs;Lmaps/ac/cs;)Lmaps/ac/cs;

    move-result-object v1

    :goto_5
    iget-boolean v2, v12, Lmaps/ae/k;->c:Z

    if-eqz v2, :cond_a

    add-int/lit8 v8, v8, 0x1

    :goto_6
    if-eqz v14, :cond_b

    invoke-interface {v3}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ae/d;->e:Lmaps/ao/b;

    if-ne v2, v3, :cond_b

    move v2, v5

    move v3, v7

    move v5, v8

    move v7, v9

    goto :goto_4

    :cond_a
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v2, v1}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    move v2, v5

    move v3, v7

    move v5, v8

    move v7, v9

    goto :goto_4

    :cond_c
    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->f()Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v2, v13}, Lmaps/ag/aj;->b(Lmaps/ac/bt;)Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v1, v1, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v1, v13}, Lmaps/ag/aj;->c(Lmaps/ac/bt;)Lmaps/ac/bs;

    move-result-object v1

    :cond_d
    :goto_7
    if-eqz v1, :cond_11

    invoke-interface {v1}, Lmaps/ac/bs;->e()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->d:Lmaps/bs/b;

    invoke-interface {v1}, Lmaps/ac/bs;->g()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v2, :cond_e

    iget-boolean v2, v12, Lmaps/ae/k;->c:Z

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v2, v2, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v2, v13, v1}, Lmaps/ag/aj;->a(Lmaps/ac/bt;Lmaps/ac/bs;)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v2}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-interface {v2, v13}, Lmaps/ag/f;->d(Lmaps/ac/bt;)[B

    move-result-object v3

    if-eqz v3, :cond_f

    invoke-interface {v2, v13, v1, v3}, Lmaps/ag/f;->a(Lmaps/ac/bt;Lmaps/ac/bs;[B)V

    :cond_f
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v2, v1}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    const/4 v1, 0x1

    :goto_8
    if-eqz v1, :cond_12

    add-int/lit8 v7, v7, 0x1

    move v2, v5

    move v3, v7

    move v5, v8

    move v7, v9

    goto/16 :goto_4

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v2}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-interface {v2, v13}, Lmaps/ag/f;->b(Lmaps/ac/bt;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v2, v13}, Lmaps/ag/f;->c(Lmaps/ac/bt;)Lmaps/ac/bs;

    move-result-object v1

    goto :goto_7

    :cond_11
    const/4 v1, 0x0

    goto :goto_8

    :cond_12
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lmaps/ae/d;->a(Lmaps/ae/k;Lmaps/ac/bt;)V

    add-int/lit8 v5, v5, 0x1

    move v2, v5

    move v3, v7

    move v5, v8

    move v7, v9

    goto/16 :goto_4

    :cond_13
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lmaps/ae/d;->a(Lmaps/ae/k;Lmaps/ac/bt;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v5, v5, 0x1

    move v2, v5

    move v3, v7

    move v5, v8

    move v7, v9

    goto/16 :goto_4

    :cond_14
    invoke-direct/range {p0 .. p0}, Lmaps/ae/d;->g()V

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->f()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-virtual/range {p0 .. p0}, Lmaps/ae/d;->k()Lmaps/ao/b;

    move-result-object v2

    sget-object v3, Lmaps/ao/b;->a:Lmaps/ao/b;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    invoke-virtual/range {p0 .. p0}, Lmaps/ae/d;->k()Lmaps/ao/b;

    move-result-object v2

    sget-object v3, Lmaps/ao/b;->b:Lmaps/ao/b;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    invoke-virtual/range {p0 .. p0}, Lmaps/ae/d;->k()Lmaps/ao/b;

    move-result-object v2

    sget-object v3, Lmaps/ao/b;->o:Lmaps/ao/b;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    :cond_15
    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ae/d;->g:Lmaps/bn/k;

    invoke-interface {v2}, Lmaps/bn/k;->r()J

    move-result-wide v2

    const-wide/16 v12, 0x64

    rem-long/2addr v2, v12

    const-wide/16 v12, 0x8

    cmp-long v2, v2, v12

    if-nez v2, :cond_16

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "t="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "f="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "p="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "r="

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "n="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "v="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "d="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lmaps/ac/cs;->r()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x6d

    const-string v9, "b"

    const/4 v10, 0x7

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v2, v10, v11

    const/4 v2, 0x1

    aput-object v3, v10, v2

    const/4 v2, 0x2

    aput-object v4, v10, v2

    const/4 v2, 0x3

    aput-object v6, v10, v2

    const/4 v2, 0x4

    aput-object v5, v10, v2

    const/4 v2, 0x5

    aput-object v1, v10, v2

    const/4 v1, 0x6

    aput-object v7, v10, v1

    invoke-static {v10}, Lmaps/br/g;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v9, v1}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_16
    return-void

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_18
    move-object v1, v3

    goto/16 :goto_5

    :cond_19
    move v6, v1

    goto/16 :goto_2
.end method

.method static synthetic a(Lmaps/ae/d;Lmaps/ae/k;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ae/d;->b(Lmaps/ae/k;)V

    return-void
.end method

.method private a(Lmaps/ae/k;)V
    .locals 2

    iget-object v0, p0, Lmaps/ae/d;->a:Lmaps/ae/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ae/d;->a:Lmaps/ae/i;

    invoke-virtual {v0}, Lmaps/ae/i;->a()V

    :cond_0
    iget-object v0, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private a(Lmaps/ae/k;Lmaps/ac/bt;)V
    .locals 2

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v0, v0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v0, v0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v0, p2}, Lmaps/ag/aj;->a(Lmaps/ac/bt;)V

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    return-void
.end method

.method static synthetic b(Lmaps/ae/d;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ae/d;->r:Z

    return-void
.end method

.method private b(Lmaps/ae/k;)V
    .locals 12

    const/4 v11, 0x0

    const/4 v8, 0x3

    const/4 v9, 0x1

    const/4 v4, 0x0

    iget-object v0, p1, Lmaps/ae/k;->a:Lmaps/ac/bt;

    iget-object v1, p0, Lmaps/ae/d;->e:Lmaps/ao/b;

    invoke-virtual {v0, v1}, Lmaps/ac/bt;->a(Lmaps/ao/b;)Lmaps/ac/bt;

    move-result-object v6

    sget-object v0, Lmaps/ae/y;->f:Lmaps/ac/bt;

    invoke-virtual {v0, v6}, Lmaps/ac/bt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, v8, v11}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ae/d;->m:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ae/k;

    invoke-static {p1}, Lmaps/ae/d;->c(Lmaps/ae/k;)Lmaps/ao/b;

    move-result-object v1

    if-eqz v1, :cond_5

    move v3, v9

    :goto_1
    iget-boolean v2, p1, Lmaps/ae/k;->f:Z

    if-eqz v2, :cond_6

    iget-boolean v2, p1, Lmaps/ae/k;->e:Z

    invoke-direct {p0, p1, v9, v4}, Lmaps/ae/d;->a(Lmaps/ae/k;ZZ)Landroid/util/Pair;

    move-result-object v7

    iget-object v2, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v2, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lmaps/ae/k;

    move-object v10, v2

    :goto_2
    if-nez v5, :cond_2

    invoke-virtual {p0, p1, v8, v11}, Lmaps/ae/d;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    :cond_2
    if-eqz v10, :cond_0

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    move-result-object v2

    if-eqz v2, :cond_3

    move v4, v9

    :cond_3
    if-eqz v4, :cond_4

    iget-boolean v2, v10, Lmaps/ae/k;->c:Z

    :cond_4
    if-eqz v0, :cond_a

    iget-boolean v1, v10, Lmaps/ae/k;->g:Z

    if-nez v1, :cond_0

    invoke-virtual {v0, v10}, Lmaps/ae/k;->a(Lmaps/ae/k;)V

    goto :goto_0

    :cond_5
    move v3, v4

    goto :goto_1

    :cond_6
    iget-boolean v2, p1, Lmaps/ae/k;->e:Z

    if-eqz v2, :cond_7

    move v2, v4

    :goto_3
    move v5, v2

    move-object v10, p1

    goto :goto_2

    :cond_7
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lmaps/ae/k;->a()Z

    move-result v2

    if-nez v2, :cond_8

    iget-boolean v2, v0, Lmaps/ae/k;->g:Z

    if-nez v2, :cond_9

    :cond_8
    move v2, v4

    goto :goto_3

    :cond_9
    invoke-direct {p0, p1, v9, v3}, Lmaps/ae/d;->a(Lmaps/ae/k;ZZ)Landroid/util/Pair;

    move-result-object v7

    iget-object v2, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_12

    iget-object v2, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lmaps/ae/k;

    move-object v10, v2

    goto :goto_2

    :cond_a
    iget-boolean v0, p0, Lmaps/ae/d;->r:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmaps/ae/d;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    iget-boolean v0, v10, Lmaps/ae/k;->c:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lmaps/ae/d;->l:Lmaps/ax/f;

    invoke-virtual {v0, v6}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ae/k;

    if-eqz v0, :cond_b

    invoke-virtual {v0, v10}, Lmaps/ae/k;->a(Lmaps/ae/k;)V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lmaps/ae/d;->l:Lmaps/ax/f;

    invoke-virtual {v0, v6, v10}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lmaps/ae/d;->m:Ljava/util/Map;

    invoke-interface {v0, v6, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-virtual {v0, v10}, Lmaps/ae/h;->a(Lmaps/ae/k;)Z

    move-result v0

    if-eqz v0, :cond_d

    if-eqz v3, :cond_e

    iget-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-static {v0}, Lmaps/ae/h;->b(Lmaps/ae/h;)Z

    move-result v0

    if-nez v0, :cond_e

    :cond_d
    invoke-direct {p0}, Lmaps/ae/d;->d()V

    :cond_e
    iget-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-virtual {p0}, Lmaps/ae/d;->k()Lmaps/ao/b;

    move-result-object v2

    invoke-static {v2, v6}, Lmaps/aw/a;->a(Lmaps/ao/b;Lmaps/ac/bt;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2, v10}, Lmaps/ae/h;->a(Landroid/util/Pair;Lmaps/ae/k;)V

    if-eqz v3, :cond_f

    iget-object v0, p1, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-virtual {v0, v1}, Lmaps/ac/bt;->a(Lmaps/ao/b;)Lmaps/ac/bt;

    move-result-object v2

    new-instance v0, Lmaps/ae/k;

    iget-object v3, v10, Lmaps/ae/k;->b:Lmaps/af/d;

    iget-object v4, v10, Lmaps/ae/k;->d:Lmaps/ae/c;

    iget-boolean v5, v10, Lmaps/ae/k;->e:Z

    iget-boolean v6, v10, Lmaps/ae/k;->f:Z

    iget v7, v10, Lmaps/ae/k;->i:I

    iget-boolean v8, v10, Lmaps/ae/k;->g:Z

    invoke-direct/range {v0 .. v8}, Lmaps/ae/k;-><init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;Lmaps/ae/c;ZZIZ)V

    iget-object v1, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    iget-object v3, v0, Lmaps/ae/k;->h:Lmaps/ao/b;

    invoke-static {v3, v2}, Lmaps/aw/a;->a(Lmaps/ao/b;Lmaps/ac/bt;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lmaps/ae/h;->a(Landroid/util/Pair;Lmaps/ae/k;)V

    :cond_f
    iget v0, p0, Lmaps/ae/d;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ae/d;->o:I

    iget-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-virtual {v0}, Lmaps/ae/h;->ae_()Z

    move-result v0

    if-nez v0, :cond_10

    iget-boolean v0, v10, Lmaps/ae/k;->e:Z

    if-eqz v0, :cond_11

    :cond_10
    invoke-direct {p0}, Lmaps/ae/d;->d()V

    goto/16 :goto_0

    :cond_11
    iget-boolean v0, p0, Lmaps/ae/d;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iput-boolean v9, p0, Lmaps/ae/d;->i:Z

    goto/16 :goto_0

    :cond_12
    move v2, v5

    goto/16 :goto_3
.end method

.method private static c(Lmaps/ae/k;)Lmaps/ao/b;
    .locals 3

    iget-object v0, p0, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-virtual {v0}, Lmaps/ac/bt;->j()Lmaps/ac/cf;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/cf;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bx;

    invoke-virtual {v0}, Lmaps/ac/bx;->a()Lmaps/ao/b;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lmaps/ac/bx;->a()Lmaps/ao/b;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 4

    iget-object v2, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    monitor-enter v2

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ae/z;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmaps/ae/z;->a()V

    move v0, v1

    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic c(Lmaps/ae/d;)V
    .locals 5

    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lmaps/ae/d;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lmaps/ae/d;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ae/h;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Lmaps/ae/h;->c()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {v0, v1}, Lmaps/ae/h;->c(I)Lmaps/ae/k;

    move-result-object v3

    iget-object v4, p0, Lmaps/ae/d;->m:Ljava/util/Map;

    iget-object v3, v3, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v3, p0, Lmaps/ae/d;->o:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lmaps/ae/d;->o:I

    invoke-virtual {v0, v1}, Lmaps/ae/h;->c(I)Lmaps/ae/k;

    move-result-object v3

    invoke-direct {p0, v3}, Lmaps/ae/d;->b(Lmaps/ae/k;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic d(Lmaps/ae/d;)I
    .locals 1

    iget v0, p0, Lmaps/ae/d;->n:I

    return v0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-virtual {v0}, Lmaps/ae/h;->c()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Lmaps/z/b;

    const-string v1, "addRequest"

    iget-object v2, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-direct {v0, v1, v2}, Lmaps/z/b;-><init>(Ljava/lang/String;Lmaps/bn/c;)V

    invoke-static {}, Lmaps/z/a;->a()V

    iget-object v0, p0, Lmaps/ae/d;->g:Lmaps/bn/k;

    iget-object v1, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-interface {v0, v1}, Lmaps/bn/k;->c(Lmaps/bn/c;)V

    iget-object v0, p0, Lmaps/ae/d;->k:Ljava/util/List;

    iget-object v1, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lmaps/ae/d;->f()Lmaps/ae/h;

    move-result-object v0

    iput-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    iget-object v0, p0, Lmaps/ae/d;->j:Lmaps/ae/h;

    invoke-static {v0, p0}, Lmaps/ae/h;->a(Lmaps/ae/h;Lmaps/ae/d;)Lmaps/ae/d;

    :cond_0
    return-void
.end method

.method static synthetic e(Lmaps/ae/d;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ae/d;->u:Z

    return v0
.end method

.method static synthetic f(Lmaps/ae/d;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ae/d;->u:Z

    return v0
.end method

.method private g()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lmaps/ae/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v0, v0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v0, v0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    invoke-interface {v0}, Lmaps/ag/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ae/d;->a:Lmaps/ae/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ae/d;->a:Lmaps/ae/i;

    invoke-virtual {v0}, Lmaps/ae/i;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Lmaps/ae/i;

    invoke-direct {v0, p0}, Lmaps/ae/i;-><init>(Lmaps/ae/d;)V

    iput-object v0, p0, Lmaps/ae/d;->a:Lmaps/ae/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lmaps/ae/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ae/d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method static synthetic g(Lmaps/ae/d;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ae/d;->g()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/ac/bt;Z)Lmaps/ac/bs;
    .locals 5

    new-instance v1, Lmaps/ae/j;

    invoke-direct {v1}, Lmaps/ae/j;-><init>()V

    new-instance v0, Lmaps/ae/k;

    iget-object v2, p0, Lmaps/ae/d;->e:Lmaps/ao/b;

    invoke-direct {v0, v2, p1, v1}, Lmaps/ae/k;-><init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;)V

    const/4 v2, 0x0

    invoke-direct {p0, v0, p2, v2}, Lmaps/ae/d;->a(Lmaps/ae/k;ZZ)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lmaps/ae/k;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    iget-object v3, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    invoke-static {v1}, Lmaps/ae/j;->a(Lmaps/ae/j;)Lmaps/ac/bs;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(IZLjava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/ae/d;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Network Error! "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, " : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public a(Lmaps/ac/bt;Lmaps/af/d;)V
    .locals 2

    new-instance v0, Lmaps/ae/k;

    iget-object v1, p0, Lmaps/ae/d;->e:Lmaps/ao/b;

    invoke-direct {v0, v1, p1, p2}, Lmaps/ae/k;-><init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;)V

    invoke-direct {p0, v0}, Lmaps/ae/d;->a(Lmaps/ae/k;)V

    return-void
.end method

.method final a(Lmaps/ae/k;ILmaps/ac/bs;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    move-object v2, p1

    :goto_0
    if-eqz v2, :cond_2

    if-nez p2, :cond_1

    iget-object v3, v2, Lmaps/ae/k;->d:Lmaps/ae/c;

    invoke-static {v3}, Lmaps/ae/u;->a(Lmaps/ae/c;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p3}, Lmaps/ac/bs;->h()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v0}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v0

    iget-object v3, p1, Lmaps/ae/k;->a:Lmaps/ac/bt;

    iget-object v4, v2, Lmaps/ae/k;->b:Lmaps/af/d;

    iget-object v5, v2, Lmaps/ae/k;->d:Lmaps/ae/c;

    invoke-static {v5, v1}, Lmaps/ae/u;->a(Lmaps/ae/c;Z)I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lmaps/ag/f;->a(Lmaps/ac/bt;Lmaps/af/d;I)V

    move v0, v1

    :goto_1
    invoke-static {v2}, Lmaps/ae/k;->b(Lmaps/ae/k;)Lmaps/ae/k;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v3, 0x4

    invoke-static {v2, v3, p3}, Lmaps/ae/k;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    goto :goto_1

    :cond_1
    invoke-static {v2, p2, p3}, Lmaps/ae/k;->a(Lmaps/ae/k;ILmaps/ac/bs;)V

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lmaps/ae/d;->u:Z

    invoke-direct {p0}, Lmaps/ae/d;->g()V

    :cond_3
    return-void
.end method

.method public final a(Lmaps/ae/z;)V
    .locals 3

    iget-object v1, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ae/d;->t:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ba/h;)V
    .locals 0

    iput-object p1, p0, Lmaps/ae/d;->s:Lmaps/ba/h;

    return-void
.end method

.method public final a(Lmaps/bn/c;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/bn/c;)V
    .locals 3

    instance-of v0, p1, Lmaps/ae/h;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmaps/ae/h;

    invoke-static {v0}, Lmaps/ae/h;->a(Lmaps/ae/h;)Lmaps/ae/d;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public final c(Lmaps/ac/bt;Lmaps/af/d;)V
    .locals 3

    new-instance v0, Lmaps/ae/k;

    iget-object v1, p0, Lmaps/ae/d;->e:Lmaps/ao/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lmaps/ae/k;-><init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;B)V

    invoke-direct {p0, v0}, Lmaps/ae/d;->a(Lmaps/ae/k;)V

    return-void
.end method

.method public e()V
    .locals 4

    :try_start_0
    invoke-static {}, Lmaps/ap/p;->d()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    new-instance v0, Lmaps/ae/g;

    invoke-direct {v0, p0}, Lmaps/ae/g;-><init>(Lmaps/ae/d;)V

    iput-object v0, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v0}, Lmaps/ae/aa;->a()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/ae/d;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not set thread priority: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract f()Lmaps/ae/h;
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v0}, Lmaps/ae/aa;->c()V

    invoke-direct {p0}, Lmaps/ae/d;->c()V

    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lmaps/ae/d;->g:Lmaps/bn/k;

    invoke-interface {v0, p0}, Lmaps/bn/k;->a(Lmaps/bn/l;)V

    invoke-virtual {p0}, Lmaps/ae/d;->start()V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lmaps/ae/d;->h:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    iget-object v1, v0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v0}, Lmaps/ag/aj;->a()Z

    :cond_0
    return-void
.end method

.method public k()Lmaps/ao/b;
    .locals 1

    iget-object v0, p0, Lmaps/ae/d;->e:Lmaps/ao/b;

    return-object v0
.end method
