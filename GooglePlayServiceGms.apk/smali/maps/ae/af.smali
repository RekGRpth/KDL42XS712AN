.class final Lmaps/ae/af;
.super Lmaps/ae/b;


# instance fields
.field private synthetic f:Lmaps/ae/ae;


# direct methods
.method constructor <init>(Lmaps/ae/ae;)V
    .locals 0

    iput-object p1, p0, Lmaps/ae/af;->f:Lmaps/ae/ae;

    invoke-direct {p0, p1}, Lmaps/ae/b;-><init>(Lmaps/ae/a;)V

    return-void
.end method


# virtual methods
.method protected final a(I)Lmaps/ac/bs;
    .locals 8

    const-wide/16 v6, -0x1

    iget-object v0, p0, Lmaps/ae/af;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ae/af;->f:Lmaps/ae/ae;

    iget-wide v0, v0, Lmaps/ae/ae;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->i()Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/ae/af;->f:Lmaps/ae/ae;

    iget-wide v2, v2, Lmaps/ae/ae;->b:J

    add-long v4, v0, v2

    :goto_1
    iget-object v0, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v0, v0, p1

    iget-object v0, v0, Lmaps/ae/k;->a:Lmaps/ac/bt;

    iget-object v1, p0, Lmaps/ae/af;->b:[[B

    aget-object v1, v1, p1

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v3, v3, p1

    iget-object v3, v3, Lmaps/ae/k;->h:Lmaps/ao/b;

    invoke-static/range {v0 .. v7}, Lmaps/ac/cs;->a(Lmaps/ac/bt;[BILmaps/ao/b;JJ)Lmaps/ac/cs;

    move-result-object v0

    iget-object v1, p0, Lmaps/ae/af;->f:Lmaps/ae/ae;

    iget-object v1, v1, Lmaps/ae/ae;->d:Lmaps/bs/b;

    invoke-interface {v0}, Lmaps/ac/bs;->g()V

    goto :goto_0

    :cond_1
    move-wide v4, v6

    goto :goto_1
.end method

.method protected final a(II)[B
    .locals 3

    add-int/lit8 v0, p1, 0x8

    new-array v0, v0, [B

    iget v1, p0, Lmaps/ae/af;->a:I

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lmaps/ac/ar;->a(I[BI)V

    const/4 v1, 0x4

    invoke-static {p2, v0, v1}, Lmaps/ac/ar;->a(I[BI)V

    return-object v0
.end method

.method protected final b(I)[B
    .locals 1

    iget-object v0, p0, Lmaps/ae/af;->b:[[B

    aget-object v0, v0, p1

    return-object v0
.end method
