.class public final Lmaps/ae/k;
.super Ljava/lang/Object;


# instance fields
.field final a:Lmaps/ac/bt;

.field final b:Lmaps/af/d;

.field final c:Z

.field final d:Lmaps/ae/c;

.field final e:Z

.field final f:Z

.field final g:Z

.field final h:Lmaps/ao/b;

.field i:I

.field private volatile j:Z

.field private k:Lmaps/ae/k;


# direct methods
.method protected constructor <init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;)V
    .locals 9

    const/4 v5, 0x0

    sget-object v4, Lmaps/ae/c;->b:Lmaps/ae/c;

    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, v5

    move v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/ae/k;-><init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;Lmaps/ae/c;ZZIZ)V

    return-void
.end method

.method protected constructor <init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;B)V
    .locals 9

    const/4 v5, 0x0

    sget-object v4, Lmaps/ae/c;->b:Lmaps/ae/c;

    const/4 v6, 0x1

    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/ae/k;-><init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;Lmaps/ae/c;ZZIZ)V

    return-void
.end method

.method protected constructor <init>(Lmaps/ao/b;Lmaps/ac/bt;Lmaps/af/d;Lmaps/ae/c;ZZIZ)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/ae/k;->j:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/ae/k;->k:Lmaps/ae/k;

    iput-object p1, p0, Lmaps/ae/k;->h:Lmaps/ao/b;

    iput-object p2, p0, Lmaps/ae/k;->a:Lmaps/ac/bt;

    iput-object p3, p0, Lmaps/ae/k;->b:Lmaps/af/d;

    iput-object p4, p0, Lmaps/ae/k;->d:Lmaps/ae/c;

    sget-object v1, Lmaps/ae/c;->e:Lmaps/ae/c;

    invoke-virtual {p4, v1}, Lmaps/ae/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lmaps/ae/c;->d:Lmaps/ae/c;

    invoke-virtual {p4, v1}, Lmaps/ae/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lmaps/ae/k;->c:Z

    iput-boolean p5, p0, Lmaps/ae/k;->e:Z

    iput p7, p0, Lmaps/ae/k;->i:I

    iput-boolean p6, p0, Lmaps/ae/k;->f:Z

    iput-boolean p8, p0, Lmaps/ae/k;->g:Z

    return-void
.end method

.method static synthetic a(Lmaps/ae/k;ILmaps/ac/bs;)V
    .locals 2

    iget-object v0, p0, Lmaps/ae/k;->b:Lmaps/af/d;

    iget-object v1, p0, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-interface {v0, v1, p1, p2}, Lmaps/af/d;->a(Lmaps/ac/bt;ILmaps/ac/bs;)V

    return-void
.end method

.method static synthetic b(Lmaps/ae/k;)Lmaps/ae/k;
    .locals 1

    iget-object v0, p0, Lmaps/ae/k;->k:Lmaps/ae/k;

    return-object v0
.end method


# virtual methods
.method final a(Lmaps/ae/k;)V
    .locals 1

    iget-object v0, p0, Lmaps/ae/k;->k:Lmaps/ae/k;

    iput-object v0, p1, Lmaps/ae/k;->k:Lmaps/ae/k;

    iput-object p1, p0, Lmaps/ae/k;->k:Lmaps/ae/k;

    return-void
.end method

.method protected final a()Z
    .locals 1

    iget-object v0, p0, Lmaps/ae/k;->k:Lmaps/ae/k;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmaps/ae/k;->h:Lmaps/ao/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
