.class public Lmaps/by/d;
.super Lmaps/by/b;


# instance fields
.field private b:J

.field private c:J

.field private d:J

.field private e:J

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Lmaps/by/c;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/by/d;-><init>(Lmaps/by/c;B)V

    return-void
.end method

.method private constructor <init>(Lmaps/by/c;B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/by/d;-><init>(Lmaps/by/c;C)V

    return-void
.end method

.method private constructor <init>(Lmaps/by/c;C)V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v4}, Lmaps/by/b;-><init>(Lmaps/by/c;Ljava/lang/Runnable;B)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/by/d;->b:J

    iput-wide v2, p0, Lmaps/by/d;->c:J

    iput-wide v2, p0, Lmaps/by/d;->d:J

    iput-wide v2, p0, Lmaps/by/d;->e:J

    const/4 v0, -0x1

    iput v0, p0, Lmaps/by/d;->f:I

    iput-boolean v4, p0, Lmaps/by/d;->g:Z

    return-void
.end method


# virtual methods
.method final declared-synchronized c()I
    .locals 4

    const-wide/16 v2, -0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/by/d;->a:Lmaps/by/c;

    invoke-virtual {v0, p0}, Lmaps/by/c;->c(Lmaps/by/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/by/d;->e:J

    iget v0, p0, Lmaps/by/d;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-wide v0, p0, Lmaps/by/d;->e:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/by/d;->e:J

    iget v0, p0, Lmaps/by/d;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()V
    .locals 4

    const-wide/16 v2, -0x1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/by/d;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lmaps/by/d;->f:I

    if-lez v0, :cond_1

    :cond_0
    iget-wide v0, p0, Lmaps/by/d;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-wide v0, p0, Lmaps/by/d;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    invoke-static {}, Lmaps/bs/a;->p()Lmaps/bs/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bs/a;->i()Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/by/d;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/by/d;->e:J

    :cond_1
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/by/d;->a:Lmaps/by/c;

    invoke-virtual {v0, p0}, Lmaps/by/c;->a(Lmaps/by/a;)V

    return-void

    :cond_2
    :try_start_1
    iget-wide v0, p0, Lmaps/by/d;->c:J

    iget-wide v2, p0, Lmaps/by/d;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/by/d;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized f()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lmaps/by/d;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/by/d;->a:Lmaps/by/c;

    invoke-virtual {v0, p0}, Lmaps/by/c;->a(Lmaps/by/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final g()V
    .locals 4

    const-wide/16 v2, -0x1

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lmaps/by/d;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/by/d;->e:J

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Lmaps/by/b;->g()V

    iget-object v0, p0, Lmaps/by/d;->a:Lmaps/by/c;

    invoke-virtual {v0, p0}, Lmaps/by/c;->a(Lmaps/by/a;)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lmaps/by/d;->f:I

    if-lez v0, :cond_1

    iget v0, p0, Lmaps/by/d;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/by/d;->f:I

    :cond_1
    iget v0, p0, Lmaps/by/d;->f:I

    if-nez v0, :cond_2

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/by/d;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-static {}, Lmaps/bs/a;->p()Lmaps/bs/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bs/a;->i()Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/by/d;->d:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/by/d;->e:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized i()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lmaps/by/d;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()V
    .locals 2

    monitor-enter p0

    const-wide/32 v0, 0xa4cb80

    :try_start_0
    iput-wide v0, p0, Lmaps/by/d;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
