.class public abstract Lmaps/bt/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bt/e;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Lmaps/bt/k;

.field private final e:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/bt/a;->a:Z

    iput-boolean v0, p0, Lmaps/bt/a;->b:Z

    iput-boolean v0, p0, Lmaps/bt/a;->c:Z

    iput-object p1, p0, Lmaps/bt/a;->e:Ljava/lang/String;

    invoke-static {}, Lmaps/bs/a;->p()Lmaps/bs/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bs/a;->q()Lmaps/bt/k;

    move-result-object v0

    iput-object v0, p0, Lmaps/bt/a;->d:Lmaps/bt/k;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lmaps/bt/a;->a:Z

    invoke-virtual {p0}, Lmaps/bt/a;->b()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmaps/bt/a;->b:Z

    iget-object v1, p0, Lmaps/bt/a;->d:Lmaps/bt/k;

    iget-object v2, p0, Lmaps/bt/a;->e:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [B

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-byte v5, v3, v4

    invoke-interface {v1, v2, v3}, Lmaps/bt/k;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lmaps/bt/a;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bt/a;->d:Lmaps/bt/k;

    iget-object v2, p0, Lmaps/bt/a;->e:Ljava/lang/String;

    invoke-interface {v0, v2}, Lmaps/bt/k;->a(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v1, p0, Lmaps/bt/a;->c:Z

    iput-boolean v0, p0, Lmaps/bt/a;->b:Z

    :cond_0
    iget-boolean v0, p0, Lmaps/bt/a;->b:Z

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/bt/a;->a:Z

    return v0
.end method
