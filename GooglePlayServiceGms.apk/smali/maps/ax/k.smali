.class public final Lmaps/ax/k;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:I

.field private d:Z

.field private e:Z


# direct methods
.method constructor <init>(Lmaps/bv/a;)V
    .locals 7

    const/16 v6, 0x1d

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lmaps/bv/a;->d(I)I

    move-result v2

    neg-int v2, v2

    int-to-double v2, v2

    const-wide v4, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lmaps/ax/k;->a(Lmaps/bv/a;I)D

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x8

    invoke-static {p1, v0}, Lmaps/ax/k;->a(Lmaps/bv/a;I)D

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x1b

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x13

    invoke-static {p1, v0}, Lmaps/ax/k;->a(Lmaps/bv/a;I)D

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x15

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x18

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/ax/k;->a:I

    const/16 v0, 0x19

    invoke-virtual {p1, v0}, Lmaps/bv/a;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ax/k;->b:Z

    const/16 v0, 0x1a

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/ax/k;->c:I

    const/16 v0, 0x1c

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x1e

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    new-instance v1, Lmaps/ax/l;

    invoke-virtual {p1, v6}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v6}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Lmaps/ax/l;-><init>(Lmaps/bv/a;)V

    const/16 v0, 0x1f

    invoke-virtual {p1, v0}, Lmaps/bv/a;->b(I)Z

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    const/16 v0, 0x21

    invoke-virtual {p1, v0}, Lmaps/bv/a;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ax/k;->d:Z

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lmaps/bv/a;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ax/k;->e:Z

    return-void

    :cond_0
    new-instance v0, Lmaps/bv/a;

    sget-object v2, Lmaps/cm/f;->j:Lmaps/bv/c;

    invoke-direct {v0, v2}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    goto :goto_0
.end method

.method private static final a(Lmaps/bv/a;I)D
    .locals 4

    invoke-virtual {p0, p1}, Lmaps/bv/a;->d(I)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    mul-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lmaps/ax/k;->a:I

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ax/k;->b:Z

    return v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lmaps/ax/k;->c:I

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ax/k;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ax/k;->e:Z

    return v0
.end method
