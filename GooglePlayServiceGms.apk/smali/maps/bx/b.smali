.class public final Lmaps/bx/b;
.super Ljava/lang/Thread;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Runnable;

.field private synthetic c:Lmaps/bx/a;


# direct methods
.method public constructor <init>(Lmaps/bx/a;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lmaps/bx/b;->c:Lmaps/bx/a;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    invoke-static {p1}, Lmaps/bx/a;->a(Lmaps/bx/a;)V

    iput-object p2, p0, Lmaps/bx/b;->a:Ljava/lang/String;

    iput-object p3, p0, Lmaps/bx/b;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lmaps/bx/b;->c:Lmaps/bx/a;

    invoke-static {v0}, Lmaps/bx/a;->b(Lmaps/bx/a;)V

    iget-object v0, p0, Lmaps/bx/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/bx/b;->c:Lmaps/bx/a;

    invoke-static {v0}, Lmaps/bx/a;->c(Lmaps/bx/a;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/bx/b;->c:Lmaps/bx/a;

    invoke-static {v0}, Lmaps/bx/a;->c(Lmaps/bx/a;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/bx/b;->c:Lmaps/bx/a;

    invoke-static {v1}, Lmaps/bx/a;->c(Lmaps/bx/a;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Thread["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/bx/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/bx/b;->getPriority()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
