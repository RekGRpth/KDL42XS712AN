.class public final Lmaps/ay/z;
.super Lmaps/ay/u;


# static fields
.field private static final a:F

.field private static final b:F


# instance fields
.field private c:Lmaps/ac/av;

.field private d:I

.field private e:Lmaps/ap/b;

.field private f:Z

.field private final g:Lmaps/at/n;

.field private final h:Lmaps/at/a;

.field private final i:Lmaps/at/a;

.field private final j:Lmaps/ac/av;

.field private final k:Lmaps/ac/av;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide v0, 0x3fb657184ae74487L    # 0.08726646259971647

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lmaps/ay/z;->a:F

    const-wide v0, 0x3faacee9f37bebd6L    # 0.05235987755982989

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lmaps/ay/z;->b:F

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x6

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lmaps/ay/z;->d:I

    sget-object v0, Lmaps/ap/b;->f:Lmaps/ap/b;

    iput-object v0, p0, Lmaps/ay/z;->e:Lmaps/ap/b;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/ay/z;->j:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/ay/z;->k:Lmaps/ac/av;

    new-instance v0, Lmaps/at/n;

    invoke-direct {v0, v2}, Lmaps/at/n;-><init>(I)V

    iput-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    new-instance v0, Lmaps/at/a;

    invoke-direct {v0, v2}, Lmaps/at/a;-><init>(I)V

    iput-object v0, p0, Lmaps/ay/z;->h:Lmaps/at/a;

    sget-object v0, Lmaps/ap/b;->a:Lmaps/ap/b;

    invoke-static {v0}, Lmaps/ap/f;->a(Lmaps/ap/b;)[I

    move-result-object v0

    invoke-static {v0}, Lmaps/ay/z;->a([I)I

    move-result v0

    iget-object v1, p0, Lmaps/ay/z;->h:Lmaps/at/a;

    invoke-virtual {v1, v0, v3}, Lmaps/at/a;->a(II)V

    iget-object v1, p0, Lmaps/ay/z;->h:Lmaps/at/a;

    or-int/lit16 v0, v0, 0xff

    invoke-virtual {v1, v0, v4}, Lmaps/at/a;->a(II)V

    new-instance v0, Lmaps/at/a;

    invoke-direct {v0, v2}, Lmaps/at/a;-><init>(I)V

    iput-object v0, p0, Lmaps/ay/z;->i:Lmaps/at/a;

    sget-object v0, Lmaps/ap/b;->c:Lmaps/ap/b;

    invoke-static {v0}, Lmaps/ap/f;->a(Lmaps/ap/b;)[I

    move-result-object v0

    invoke-static {v0}, Lmaps/ay/z;->a([I)I

    move-result v0

    iget-object v1, p0, Lmaps/ay/z;->i:Lmaps/at/a;

    invoke-virtual {v1, v0, v3}, Lmaps/at/a;->a(II)V

    iget-object v1, p0, Lmaps/ay/z;->i:Lmaps/at/a;

    or-int/lit16 v0, v0, 0xff

    invoke-virtual {v1, v0, v4}, Lmaps/at/a;->a(II)V

    return-void
.end method

.method private static a([I)I
    .locals 3

    const v2, 0xff00

    const/4 v0, 0x0

    aget v0, p0, v0

    and-int/2addr v0, v2

    shl-int/lit8 v0, v0, 0x10

    const/4 v1, 0x1

    aget v1, p0, v1

    and-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    const/4 v1, 0x2

    aget v1, p0, v1

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v0

    iget-boolean v2, p0, Lmaps/ay/z;->f:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/ay/z;->e:Lmaps/ap/b;

    if-eq v0, v2, :cond_2

    :cond_0
    iput-object v0, p0, Lmaps/ay/z;->e:Lmaps/ap/b;

    iput-boolean v1, p0, Lmaps/ay/z;->f:Z

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v2

    iget-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->a(Lmaps/as/a;)V

    sget-object v0, Lmaps/ap/b;->f:Lmaps/ap/b;

    if-eq v2, v0, :cond_1

    sget-object v0, Lmaps/ap/b;->e:Lmaps/ap/b;

    if-eq v2, v0, :cond_1

    invoke-virtual {p2}, Lmaps/ar/a;->o()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    iget-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->a()I

    move-result v0

    if-nez v0, :cond_6

    :goto_2
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lmaps/ar/a;->r()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/av;->h()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Lmaps/ar/a;->b(F)F

    move-result v3

    sget-object v0, Lmaps/ap/b;->b:Lmaps/ap/b;

    if-ne v2, v0, :cond_4

    const/high16 v0, 0x40400000    # 3.0f

    :goto_3
    sub-float v0, v3, v0

    invoke-virtual {p2}, Lmaps/ar/a;->o()F

    move-result v3

    invoke-virtual {p2}, Lmaps/ar/a;->l()F

    move-result v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    cmpg-float v3, v3, v0

    if-lez v3, :cond_1

    invoke-virtual {p2}, Lmaps/ar/a;->m()F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lmaps/ay/z;->d:I

    invoke-virtual {p2, v0}, Lmaps/ar/a;->c(F)Lmaps/ac/cw;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/cw;->c()Lmaps/ac/be;

    move-result-object v0

    check-cast v0, Lmaps/ac/t;

    invoke-virtual {v0}, Lmaps/ac/t;->d()Lmaps/ac/av;

    move-result-object v4

    iput-object v4, p0, Lmaps/ay/z;->c:Lmaps/ac/av;

    iget-object v4, p0, Lmaps/ay/z;->j:Lmaps/ac/av;

    invoke-virtual {v4, v1, v1}, Lmaps/ac/av;->d(II)V

    invoke-virtual {v0}, Lmaps/ac/t;->c()Lmaps/ac/av;

    move-result-object v0

    iget-object v4, p0, Lmaps/ay/z;->c:Lmaps/ac/av;

    iget-object v5, p0, Lmaps/ay/z;->k:Lmaps/ac/av;

    invoke-static {v0, v4, v5}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    iget-object v4, p0, Lmaps/ay/z;->j:Lmaps/ac/av;

    iget v5, p0, Lmaps/ay/z;->d:I

    invoke-virtual {v0, v4, v5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    iget-object v4, p0, Lmaps/ay/z;->k:Lmaps/ac/av;

    iget v5, p0, Lmaps/ay/z;->d:I

    invoke-virtual {v0, v4, v5}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v0, p0, Lmaps/ay/z;->c:Lmaps/ac/av;

    invoke-virtual {p2}, Lmaps/ar/a;->r()Lmaps/ac/av;

    move-result-object v4

    invoke-virtual {v0, v4}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v4

    sget-object v0, Lmaps/ap/b;->b:Lmaps/ap/b;

    if-ne v2, v0, :cond_5

    sget v0, Lmaps/ay/z;->b:F

    :goto_4
    mul-float/2addr v0, v4

    float-to-int v0, v0

    iget-object v2, p0, Lmaps/ay/z;->j:Lmaps/ac/av;

    invoke-virtual {v2, v0}, Lmaps/ac/av;->b(I)V

    iget-object v2, p0, Lmaps/ay/z;->k:Lmaps/ac/av;

    invoke-virtual {v2, v0}, Lmaps/ac/av;->b(I)V

    iget-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    iget-object v2, p0, Lmaps/ay/z;->j:Lmaps/ac/av;

    iget v4, p0, Lmaps/ay/z;->d:I

    invoke-virtual {v0, v2, v4}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    iget-object v2, p0, Lmaps/ay/z;->k:Lmaps/ac/av;

    iget v4, p0, Lmaps/ay/z;->d:I

    invoke-virtual {v0, v2, v4}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v0, p0, Lmaps/ay/z;->j:Lmaps/ac/av;

    invoke-virtual {v0, v3}, Lmaps/ac/av;->b(I)V

    iget-object v0, p0, Lmaps/ay/z;->k:Lmaps/ac/av;

    invoke-virtual {v0, v3}, Lmaps/ac/av;->b(I)V

    iget-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    iget-object v2, p0, Lmaps/ay/z;->j:Lmaps/ac/av;

    iget v3, p0, Lmaps/ay/z;->d:I

    invoke-virtual {v0, v2, v3}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v0, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    iget-object v2, p0, Lmaps/ay/z;->k:Lmaps/ac/av;

    iget v3, p0, Lmaps/ay/z;->d:I

    invoke-virtual {v0, v2, v3}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    goto/16 :goto_1

    :cond_4
    const/high16 v0, 0x40a00000    # 5.0f

    goto/16 :goto_3

    :cond_5
    sget v0, Lmaps/ay/z;->a:F

    goto :goto_4

    :cond_6
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v2, p0, Lmaps/ay/z;->c:Lmaps/ac/av;

    iget v3, p0, Lmaps/ay/z;->d:I

    int-to-float v3, v3

    invoke-static {p1, p2, v2, v3}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    invoke-virtual {p1}, Lmaps/as/a;->p()V

    invoke-virtual {p1}, Lmaps/as/a;->u()V

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-interface {v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v2, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    invoke-virtual {v2, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v2

    sget-object v3, Lmaps/ap/b;->c:Lmaps/ap/b;

    if-ne v2, v3, :cond_7

    iget-object v2, p0, Lmaps/ay/z;->i:Lmaps/at/a;

    invoke-virtual {v2, p1}, Lmaps/at/a;->c(Lmaps/as/a;)V

    :goto_5
    const/4 v2, 0x5

    iget-object v3, p0, Lmaps/ay/z;->g:Lmaps/at/n;

    invoke-virtual {v3}, Lmaps/at/n;->a()I

    move-result v3

    invoke-interface {v0, v2, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_2

    :cond_7
    iget-object v2, p0, Lmaps/ay/z;->h:Lmaps/at/a;

    invoke-virtual {v2, p1}, Lmaps/at/a;->c(Lmaps/as/a;)V

    goto :goto_5
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/z;->f:Z

    return v0
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    sget-object v0, Lmaps/ay/v;->l:Lmaps/ay/v;

    return-object v0
.end method
