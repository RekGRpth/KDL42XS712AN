.class public final enum Lmaps/ay/k;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/ay/k;

.field public static final enum b:Lmaps/ay/k;

.field private static final synthetic c:[Lmaps/ay/k;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/ay/k;

    const-string v1, "UPPER_LEFT"

    invoke-direct {v0, v1, v2}, Lmaps/ay/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/k;->a:Lmaps/ay/k;

    new-instance v0, Lmaps/ay/k;

    const-string v1, "UPPER_RIGHT"

    invoke-direct {v0, v1, v3}, Lmaps/ay/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/k;->b:Lmaps/ay/k;

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/ay/k;

    sget-object v1, Lmaps/ay/k;->a:Lmaps/ay/k;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/ay/k;->b:Lmaps/ay/k;

    aput-object v1, v0, v3

    sput-object v0, Lmaps/ay/k;->c:[Lmaps/ay/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/ay/k;
    .locals 1

    const-class v0, Lmaps/ay/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/ay/k;

    return-object v0
.end method

.method public static values()[Lmaps/ay/k;
    .locals 1

    sget-object v0, Lmaps/ay/k;->c:[Lmaps/ay/k;

    invoke-virtual {v0}, [Lmaps/ay/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/ay/k;

    return-object v0
.end method
