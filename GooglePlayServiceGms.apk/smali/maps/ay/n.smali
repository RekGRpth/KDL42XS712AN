.class public final Lmaps/ay/n;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ay/b;

.field private final b:Lmaps/ay/c;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Lmaps/ay/b;Lmaps/ay/c;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ay/n;->a:Lmaps/ay/b;

    iput-object p2, p0, Lmaps/ay/n;->b:Lmaps/ay/c;

    iput p3, p0, Lmaps/ay/n;->c:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lmaps/ay/n;->c:I

    return v0
.end method

.method public final a(FFLmaps/ar/a;)V
    .locals 3

    iget-object v0, p0, Lmaps/ay/n;->a:Lmaps/ay/b;

    invoke-interface {v0, p1, p2, p3}, Lmaps/ay/b;->a(FFLmaps/ar/a;)I

    move-result v0

    iput v0, p0, Lmaps/ay/n;->c:I

    iget v0, p0, Lmaps/ay/n;->c:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lmaps/ay/n;->b:Lmaps/ay/c;

    invoke-virtual {p3}, Lmaps/ar/a;->k()F

    move-result v0

    const/high16 v1, 0x40a00000    # 5.0f

    mul-float/2addr v0, v1

    mul-float/2addr v0, v0

    iget v1, p0, Lmaps/ay/n;->c:I

    iget-object v2, p0, Lmaps/ay/n;->b:Lmaps/ay/c;

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ay/n;->c:I

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/ay/n;->d:Z

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ay/n;->d:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    iget-object v0, p0, Lmaps/ay/n;->b:Lmaps/ay/c;

    iget-object v1, p0, Lmaps/ay/n;->a:Lmaps/ay/b;

    invoke-virtual {v0, v1}, Lmaps/ay/c;->a(Lmaps/ay/b;)Z

    move-result v0

    return v0
.end method
