.class final enum Lmaps/ay/bd;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/ay/bd;

.field public static final enum b:Lmaps/ay/bd;

.field public static final enum c:Lmaps/ay/bd;

.field public static final enum d:Lmaps/ay/bd;

.field private static final synthetic e:[Lmaps/ay/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/ay/bd;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lmaps/ay/bd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/bd;->a:Lmaps/ay/bd;

    new-instance v0, Lmaps/ay/bd;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lmaps/ay/bd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/bd;->b:Lmaps/ay/bd;

    new-instance v0, Lmaps/ay/bd;

    const-string v1, "ZOOM"

    invoke-direct {v0, v1, v4}, Lmaps/ay/bd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/bd;->c:Lmaps/ay/bd;

    new-instance v0, Lmaps/ay/bd;

    const-string v1, "ROTATE"

    invoke-direct {v0, v1, v5}, Lmaps/ay/bd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/bd;->d:Lmaps/ay/bd;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/ay/bd;

    sget-object v1, Lmaps/ay/bd;->a:Lmaps/ay/bd;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/ay/bd;->b:Lmaps/ay/bd;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/ay/bd;->c:Lmaps/ay/bd;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/ay/bd;->d:Lmaps/ay/bd;

    aput-object v1, v0, v5

    sput-object v0, Lmaps/ay/bd;->e:[Lmaps/ay/bd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/ay/bd;
    .locals 1

    const-class v0, Lmaps/ay/bd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/ay/bd;

    return-object v0
.end method

.method public static values()[Lmaps/ay/bd;
    .locals 1

    sget-object v0, Lmaps/ay/bd;->e:[Lmaps/ay/bd;

    invoke-virtual {v0}, [Lmaps/ay/bd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/ay/bd;

    return-object v0
.end method
