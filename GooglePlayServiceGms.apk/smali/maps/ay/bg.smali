.class public Lmaps/ay/bg;
.super Lmaps/aj/d;

# interfaces
.implements Lmaps/aj/c;
.implements Lmaps/ay/be;


# instance fields
.field public h:Lmaps/ap/f;

.field public i:Lmaps/ay/bh;

.field public j:Lmaps/ay/bc;

.field public k:Lmaps/ap/e;

.field private final l:Lmaps/ay/m;

.field private final m:Landroid/content/res/Resources;

.field private n:Lmaps/bo/g;

.field private o:Lmaps/ay/au;

.field private p:Lmaps/ay/u;

.field private q:Z

.field private r:Lmaps/ar/b;

.field private s:J

.field private t:Z

.field private u:Lmaps/aj/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 9

    invoke-direct {p0, p1}, Lmaps/aj/d;-><init>(Landroid/content/Context;)V

    new-instance v0, Lmaps/ay/m;

    invoke-direct {v0}, Lmaps/ay/m;-><init>()V

    iput-object v0, p0, Lmaps/ay/bg;->l:Lmaps/ay/m;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lmaps/ay/bg;->s:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/bg;->t:Z

    iput-object p2, p0, Lmaps/ay/bg;->m:Landroid/content/res/Resources;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/aj/d;->g:Z

    new-instance v0, Lmaps/ay/bc;

    invoke-direct {v0, p0}, Lmaps/ay/bc;-><init>(Lmaps/ay/be;)V

    iput-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    new-instance v0, Lmaps/bo/g;

    invoke-direct {v0}, Lmaps/bo/g;-><init>()V

    iput-object v0, p0, Lmaps/ay/bg;->n:Lmaps/bo/g;

    iget-object v0, p0, Lmaps/ay/bg;->n:Lmaps/bo/g;

    invoke-virtual {p0}, Lmaps/ay/bg;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0, v1, v2}, Lmaps/bo/g;->a(Landroid/content/Context;Lmaps/bo/m;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/ay/bg;->setFocusable(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/ay/bg;->setClickable(Z)V

    iget-object v0, p0, Lmaps/ay/bg;->m:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v7, v0, Landroid/util/DisplayMetrics;->density:F

    new-instance v0, Lmaps/aj/a;

    invoke-direct {v0, p0}, Lmaps/aj/a;-><init>(Lmaps/aj/c;)V

    iput-object v0, p0, Lmaps/ay/bg;->u:Lmaps/aj/a;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lmaps/bb/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/ay/ac;

    const/16 v1, 0x8

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/4 v4, 0x0

    const/16 v5, 0x10

    const/16 v6, 0x8

    invoke-direct/range {v0 .. v6}, Lmaps/ay/ac;-><init>(IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v0, Lmaps/ay/ac;

    const/4 v1, 0x5

    const/4 v2, 0x6

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/16 v5, 0x10

    const/16 v6, 0x8

    invoke-direct/range {v0 .. v6}, Lmaps/ay/ac;-><init>(IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/ay/ac;

    const/4 v1, 0x5

    const/4 v2, 0x6

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/16 v5, 0x10

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lmaps/ay/ac;-><init>(IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/ac;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lmaps/ay/ac;->a(Z)V

    goto :goto_0

    :cond_1
    new-instance v1, Lmaps/ay/ab;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lmaps/ay/ac;

    invoke-interface {v8, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/ay/ac;

    invoke-direct {v1, v0}, Lmaps/ay/ab;-><init>([Lmaps/ay/ac;)V

    invoke-super {p0}, Lmaps/aj/d;->f()V

    iput-object v1, p0, Lmaps/aj/d;->d:Lmaps/aj/h;

    new-instance v0, Lmaps/ar/a;

    sget-object v1, Lmaps/ar/a;->b:Lmaps/ar/b;

    const/16 v2, 0x100

    const/16 v3, 0x100

    const/4 v5, 0x0

    move v4, v7

    invoke-direct/range {v0 .. v5}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIFB)V

    sget-object v1, Lmaps/ao/b;->a:Lmaps/ao/b;

    iget-object v2, p0, Lmaps/ay/bg;->m:Landroid/content/res/Resources;

    invoke-static {v1, v2}, Lmaps/ay/ap;->a(Lmaps/ao/b;Landroid/content/res/Resources;)Lmaps/ay/ap;

    move-result-object v1

    new-instance v2, Lmaps/ap/f;

    iget-object v3, p0, Lmaps/ay/bg;->u:Lmaps/aj/a;

    iget-object v4, p0, Lmaps/ay/bg;->m:Landroid/content/res/Resources;

    invoke-direct {v2, v3, v4, v0, v1}, Lmaps/ap/f;-><init>(Lmaps/aj/a;Landroid/content/res/Resources;Lmaps/ar/a;Lmaps/ay/ap;)V

    iput-object v2, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-super {p0}, Lmaps/aj/d;->f()V

    iget-object v1, p0, Lmaps/aj/d;->d:Lmaps/aj/h;

    if-nez v1, :cond_2

    new-instance v1, Lmaps/aj/q;

    invoke-direct {v1, p0}, Lmaps/aj/q;-><init>(Lmaps/aj/d;)V

    iput-object v1, p0, Lmaps/aj/d;->d:Lmaps/aj/h;

    :cond_2
    iget-object v1, p0, Lmaps/aj/d;->e:Lmaps/aj/i;

    if-nez v1, :cond_3

    new-instance v1, Lmaps/aj/g;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lmaps/aj/g;-><init>(Lmaps/aj/d;B)V

    iput-object v1, p0, Lmaps/aj/d;->e:Lmaps/aj/i;

    :cond_3
    iget-object v1, p0, Lmaps/aj/d;->f:Lmaps/aj/j;

    if-nez v1, :cond_4

    new-instance v1, Lmaps/aj/j;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lmaps/aj/j;-><init>(B)V

    iput-object v1, p0, Lmaps/aj/d;->f:Lmaps/aj/j;

    :cond_4
    iput-object v0, p0, Lmaps/aj/d;->c:Lmaps/aj/p;

    new-instance v0, Lmaps/aj/l;

    iget-object v1, p0, Lmaps/aj/d;->a:Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Lmaps/aj/l;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    invoke-virtual {v0}, Lmaps/aj/l;->start()V

    iget-object v0, p0, Lmaps/aj/d;->b:Lmaps/aj/l;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/aj/l;->a(I)V

    return-void
.end method

.method private w()F
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->m:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 2

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lmaps/ay/bg;->a()V

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ay/v;)Lmaps/ay/r;
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->a(Lmaps/ay/v;)Lmaps/ay/r;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->k:Lmaps/ap/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bg;->k:Lmaps/ap/e;

    invoke-virtual {v0}, Lmaps/ap/e;->c()V

    :cond_0
    invoke-super {p0}, Lmaps/aj/d;->a()V

    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1, p2}, Lmaps/ap/f;->a(II)V

    return-void
.end method

.method public final a(Lmaps/ap/b;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->b(Lmaps/ap/b;)V

    return-void
.end method

.method public final a(Lmaps/av/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->a(Lmaps/av/a;)V

    return-void
.end method

.method public final a(Lmaps/ay/ap;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->a(Lmaps/ay/ap;)V

    return-void
.end method

.method public final a(Lmaps/ay/au;)V
    .locals 2

    iput-object p1, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    iget-object v1, p0, Lmaps/ay/bg;->u:Lmaps/aj/a;

    invoke-virtual {v0, v1}, Lmaps/ay/au;->a(Lmaps/ap/n;)V

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, v1}, Lmaps/ay/au;->a(Lmaps/ak/c;)V

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    iget-object v1, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v0, v1}, Lmaps/ap/f;->a(Lmaps/ap/a;)V

    return-void
.end method

.method public final a(Lmaps/ay/b;Lmaps/ay/e;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/bg;->q:Z

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1, p2}, Lmaps/ap/f;->a(Lmaps/ay/b;Lmaps/ay/e;)V

    return-void
.end method

.method public final a(Lmaps/ay/u;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->a(Lmaps/ay/u;)V

    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->u:Lmaps/aj/a;

    invoke-virtual {v0, p1, p2}, Lmaps/aj/a;->a(ZZ)V

    return-void
.end method

.method public final a(FF)Z
    .locals 5

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/ar/a;

    iget-object v1, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v1}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/ay/bg;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lmaps/ay/bg;->getHeight()I

    move-result v3

    invoke-direct {p0}, Lmaps/ay/bg;->w()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIF)V

    invoke-virtual {v0, p1, p2}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v1}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v1

    invoke-virtual {v1, p1, p2, v0}, Lmaps/ay/o;->b(FFLmaps/ar/a;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 10

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->g()Lmaps/az/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/az/b;->g()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v0, v4, v5, v1}, Lmaps/az/b;->d(FFLmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2, v3}, Lmaps/ay/bg;->a(ZZ)V

    move v0, v3

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->d()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_1
    if-ltz v4, :cond_4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    invoke-virtual {v0}, Lmaps/ay/u;->g()Z

    move-result v6

    if-eqz v6, :cond_3

    if-nez v1, :cond_2

    new-instance v1, Lmaps/ar/a;

    iget-object v6, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v6}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v6

    invoke-virtual {p0}, Lmaps/ay/bg;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lmaps/ay/bg;->getHeight()I

    move-result v8

    invoke-direct {p0}, Lmaps/ay/bg;->w()F

    move-result v9

    invoke-direct {v1, v6, v7, v8, v9}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIF)V

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-virtual {v0, v6, v7, v1}, Lmaps/ay/u;->d(FFLmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v2, v3}, Lmaps/ay/bg;->a(ZZ)V

    move v0, v3

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->k:Lmaps/ap/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bg;->k:Lmaps/ap/e;

    invoke-virtual {v0}, Lmaps/ap/e;->d()V

    :cond_0
    return-void
.end method

.method public final b(FF)V
    .locals 11

    const/4 v2, 0x1

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Lmaps/ar/a;

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ay/bg;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lmaps/ay/bg;->getHeight()I

    move-result v3

    invoke-direct {p0}, Lmaps/ay/bg;->w()F

    move-result v5

    invoke-direct {v4, v0, v1, v3, v5}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIF)V

    invoke-virtual {v4, p1, p2}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v5

    const/4 v0, 0x0

    iget-boolean v6, p0, Lmaps/ay/bg;->q:Z

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v1}, Lmaps/ap/f;->g()Lmaps/az/b;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->g()Lmaps/az/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v5, v4}, Lmaps/az/b;->a(FFLmaps/ac/av;Lmaps/ar/a;)Z

    move-result v0

    :cond_1
    if-nez v0, :cond_2

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v1}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v5, v4}, Lmaps/ay/o;->a(FFLmaps/ac/av;Lmaps/ar/a;)Z

    move-result v0

    :cond_2
    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v1}, Lmaps/ap/f;->d()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v1, v8, -0x1

    move v3, v1

    move v1, v0

    :goto_1
    if-nez v1, :cond_4

    if-ltz v3, :cond_4

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    invoke-virtual {v0}, Lmaps/ay/u;->an_()Z

    move-result v10

    if-eqz v10, :cond_3

    check-cast v0, Lmaps/ay/c;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_b

    iget-object v10, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v10}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v10

    if-eq v0, v10, :cond_b

    invoke-virtual {v0, p1, p2, v5, v4}, Lmaps/ay/u;->a(FFLmaps/ac/av;Lmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v2

    goto :goto_2

    :cond_4
    if-nez v1, :cond_7

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    if-eqz v6, :cond_5

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v0

    iget-object v3, p0, Lmaps/ay/bg;->r:Lmaps/ar/b;

    invoke-virtual {v0, v3}, Lmaps/ar/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    iget-object v0, p0, Lmaps/ay/bg;->l:Lmaps/ay/m;

    invoke-virtual {v0}, Lmaps/ay/m;->a()V

    :cond_6
    iget-object v0, p0, Lmaps/ay/bg;->l:Lmaps/ay/m;

    invoke-virtual {v0, p1, p2, v4, v9}, Lmaps/ay/m;->a(FFLmaps/ar/a;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v1, v2

    :cond_7
    add-int/lit8 v0, v8, -0x1

    move v3, v0

    :goto_3
    if-nez v1, :cond_8

    if-ltz v3, :cond_8

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    invoke-virtual {v0}, Lmaps/ay/u;->an_()Z

    move-result v6

    if-nez v6, :cond_a

    iget-object v6, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v6}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v6

    if-eq v0, v6, :cond_a

    invoke-virtual {v0, p1, p2, v5, v4}, Lmaps/ay/u;->a(FFLmaps/ac/av;Lmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v2

    :goto_4
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v1, v0

    goto :goto_3

    :cond_8
    if-nez v1, :cond_9

    iget-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    invoke-interface {v0, v5}, Lmaps/ay/bh;->a(Lmaps/ac/av;)V

    :cond_9
    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/bg;->r:Lmaps/ar/b;

    invoke-virtual {p0}, Lmaps/ay/bg;->a()V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto :goto_4

    :cond_b
    move v0, v1

    goto/16 :goto_2
.end method

.method public final b(Lmaps/ay/u;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->b(Lmaps/ay/u;)V

    iget-object v0, p0, Lmaps/ay/bg;->p:Lmaps/ay/u;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lmaps/ay/bg;->p()V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0, p1}, Lmaps/ay/bc;->a(Z)V

    return-void
.end method

.method public final c(FF)V
    .locals 7

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Lmaps/ar/a;

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ay/bg;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lmaps/ay/bg;->getHeight()I

    move-result v2

    invoke-direct {p0}, Lmaps/ay/bg;->w()F

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIF)V

    invoke-virtual {v3, p1, p2}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v4

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v1}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v3}, Lmaps/ay/o;->c(FFLmaps/ar/a;)Z

    move-result v0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->d()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    iget-object v6, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v6}, Lmaps/ap/f;->f()Lmaps/ay/o;

    move-result-object v6

    if-eq v0, v6, :cond_3

    invoke-virtual {v0, p1, p2, v3}, Lmaps/ay/u;->c(FFLmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_1
    if-nez v1, :cond_2

    iget-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    invoke-interface {v0, v4}, Lmaps/ay/bh;->b(Lmaps/ac/av;)V

    :cond_2
    invoke-virtual {p0}, Lmaps/ay/bg;->a()V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0, p1}, Lmaps/ay/bc;->b(Z)V

    return-void
.end method

.method public final d()V
    .locals 2

    invoke-virtual {p0}, Lmaps/ay/bg;->p()V

    iget-object v0, p0, Lmaps/ay/bg;->u:Lmaps/aj/a;

    invoke-virtual {v0}, Lmaps/aj/a;->g()V

    invoke-super {p0}, Lmaps/aj/d;->d()V

    invoke-static {}, Lmaps/aq/a;->a()Lmaps/aq/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v1}, Lmaps/ap/f;->i()Lmaps/as/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/aq/a;->b(Lmaps/as/a;)V

    :cond_0
    return-void
.end method

.method public final d(FF)V
    .locals 5

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Lmaps/ar/a;

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v0}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ay/bg;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lmaps/ay/bg;->getHeight()I

    move-result v3

    invoke-direct {p0}, Lmaps/ay/bg;->w()F

    move-result v4

    invoke-direct {v2, v0, v1, v3, v4}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIF)V

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->d()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    invoke-virtual {v0, p1, p2, v2}, Lmaps/ay/u;->a_(FFLmaps/ar/a;)Z

    move-result v4

    if-eqz v4, :cond_2

    iput-object v0, p0, Lmaps/ay/bg;->p:Lmaps/ay/u;

    invoke-virtual {p0}, Lmaps/ay/bg;->a()V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1
.end method

.method public final d(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0, p1}, Lmaps/ay/bc;->c(Z)V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Lmaps/aj/d;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-super {p0}, Lmaps/aj/d;->e()V

    iget-object v0, p0, Lmaps/ay/bg;->u:Lmaps/aj/a;

    invoke-virtual {v0}, Lmaps/aj/a;->f()V

    return-void
.end method

.method public final e(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0, p1}, Lmaps/ay/bc;->d(Z)V

    return-void
.end method

.method public final e(FF)Z
    .locals 10

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    if-nez v0, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->d()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v2, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v2}, Lmaps/ap/f;->g()Lmaps/az/b;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/az/b;->g()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2, p1, p2, v1, v1}, Lmaps/az/b;->b(FFLmaps/ac/av;Lmaps/ar/a;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v3, v4}, Lmaps/ay/bg;->a(ZZ)V

    move v0, v4

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    move-object v2, v1

    move v5, v0

    :goto_1
    if-ltz v5, :cond_4

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    invoke-virtual {v0}, Lmaps/ay/u;->g()Z

    move-result v7

    if-eqz v7, :cond_3

    if-nez v2, :cond_2

    new-instance v2, Lmaps/ar/a;

    iget-object v1, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v1}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/ay/bg;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lmaps/ay/bg;->getHeight()I

    move-result v8

    invoke-direct {p0}, Lmaps/ay/bg;->w()F

    move-result v9

    invoke-direct {v2, v1, v7, v8, v9}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIF)V

    invoke-virtual {v2, p1, p2}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v1

    :cond_2
    invoke-virtual {v0, p1, p2, v1, v2}, Lmaps/ay/u;->b(FFLmaps/ac/av;Lmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v4

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method public final f(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0, p1}, Lmaps/ap/f;->c(Z)V

    return-void
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->m:Landroid/content/res/Resources;

    return-object v0
.end method

.method public final i()Lmaps/ay/aj;
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->c()Lmaps/ay/aj;

    move-result-object v0

    return-object v0
.end method

.method public isOpaque()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->b()V

    return-void
.end method

.method public final k()V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/ay/bg;->s:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x4e20

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-object v2, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lmaps/ap/f;->b(Z)V

    :goto_0
    iput-wide v0, p0, Lmaps/ay/bg;->s:J

    return-void

    :cond_0
    iget-object v2, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lmaps/ap/f;->b(Z)V

    goto :goto_0
.end method

.method public final l()Lmaps/ay/au;
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    return-object v0
.end method

.method public final m()Lmaps/ar/a;
    .locals 5

    new-instance v0, Lmaps/ar/a;

    iget-object v1, p0, Lmaps/ay/bg;->o:Lmaps/ay/au;

    invoke-virtual {v1}, Lmaps/ay/au;->c()Lmaps/ar/b;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/ay/bg;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lmaps/ay/bg;->getHeight()I

    move-result v3

    invoke-direct {p0}, Lmaps/ay/bg;->w()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIF)V

    return-object v0
.end method

.method public final n()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    :cond_0
    return-void
.end method

.method public final o()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bg;->i:Lmaps/ay/bh;

    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 0

    invoke-super {p0}, Lmaps/aj/d;->onFinishInflate()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lmaps/ay/bg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ay/bg;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bg;->n:Lmaps/bo/g;

    invoke-virtual {v0, p1}, Lmaps/bo/g;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    invoke-super {p0, p1}, Lmaps/aj/d;->onWindowFocusChanged(Z)V

    iget-object v1, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lmaps/ap/f;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->p:Lmaps/ay/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bg;->p:Lmaps/ay/u;

    invoke-virtual {v0}, Lmaps/ay/u;->at_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/bg;->p:Lmaps/ay/u;

    invoke-virtual {p0}, Lmaps/ay/bg;->a()V

    :cond_0
    return-void
.end method

.method public final q()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/bg;->q:Z

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    invoke-virtual {v0}, Lmaps/ap/f;->e()V

    return-void
.end method

.method public final r()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0}, Lmaps/ay/bc;->a()Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0}, Lmaps/ay/bc;->b()Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0}, Lmaps/ay/bc;->c()Z

    move-result v0

    return v0
.end method

.method public final u()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->j:Lmaps/ay/bc;

    invoke-virtual {v0}, Lmaps/ay/bc;->d()Z

    move-result v0

    return v0
.end method

.method public final v()Lmaps/ap/f;
    .locals 1

    iget-object v0, p0, Lmaps/ay/bg;->h:Lmaps/ap/f;

    return-object v0
.end method
