.class final Lmaps/ay/j;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/an/b;


# instance fields
.field private final a:Lmaps/an/an;

.field private final b:Lmaps/an/k;

.field private final c:Lmaps/an/j;

.field private d:Z

.field private e:Z

.field private final f:I

.field private g:Lmaps/an/c;

.field private final h:Lmaps/v/h;

.field private i:F

.field private volatile j:Z


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/ay/j;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/j;->b:Lmaps/an/k;

    iget-object v1, p0, Lmaps/ay/j;->a:Lmaps/an/an;

    invoke-virtual {v0, v1}, Lmaps/an/k;->a(Lmaps/an/an;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/j;->d:Z

    :cond_0
    iget-object v0, p0, Lmaps/ay/j;->h:Lmaps/v/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/j;->h:Lmaps/v/h;

    invoke-virtual {v0}, Lmaps/v/h;->hasEnded()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ay/j;->j:Z

    iget-boolean v0, p0, Lmaps/ay/j;->j:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ay/j;->h:Lmaps/v/h;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/v/h;->b(J)F

    move-result v0

    iput v0, p0, Lmaps/ay/j;->i:F

    :cond_1
    iget-boolean v0, p0, Lmaps/ay/j;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ay/j;->c:Lmaps/an/j;

    iget v1, p0, Lmaps/ay/j;->f:I

    invoke-interface {v0, v1}, Lmaps/an/j;->a(I)V

    :goto_0
    iget-object v0, p0, Lmaps/ay/j;->h:Lmaps/v/h;

    invoke-virtual {v0}, Lmaps/v/h;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/ay/j;->g:Lmaps/an/c;

    sget-object v1, Lmaps/an/au;->b:Lmaps/an/au;

    invoke-interface {v0, p0, v1}, Lmaps/an/c;->a(Lmaps/an/b;Lmaps/an/ar;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    iget-object v0, p0, Lmaps/ay/j;->c:Lmaps/an/j;

    iget v1, p0, Lmaps/ay/j;->i:F

    iget v2, p0, Lmaps/ay/j;->i:F

    iget v3, p0, Lmaps/ay/j;->i:F

    iget v4, p0, Lmaps/ay/j;->i:F

    invoke-interface {v0, v1, v2, v3, v4}, Lmaps/an/j;->a(FFFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/an/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/ay/j;->g:Lmaps/an/c;

    sget-object v0, Lmaps/an/au;->b:Lmaps/an/au;

    invoke-interface {p1, p0, v0}, Lmaps/an/c;->a(Lmaps/an/b;Lmaps/an/ar;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
