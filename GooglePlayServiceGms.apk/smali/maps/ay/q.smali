.class public final Lmaps/ay/q;
.super Lmaps/ay/u;


# static fields
.field private static final a:Lmaps/ac/av;

.field private static final b:Lmaps/ac/av;


# instance fields
.field private final c:Lmaps/ac/az;

.field private d:Lmaps/ac/az;

.field private e:Lmaps/ac/az;

.field private final f:Ljava/util/List;

.field private final g:Lmaps/ac/r;

.field private h:Lmaps/ac/bd;

.field private i:F

.field private j:F

.field private final k:Lmaps/at/n;

.field private final l:Lmaps/at/d;

.field private final m:Lmaps/at/i;

.field private n:F

.field private o:I

.field private p:Z

.field private final q:I

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lmaps/ac/av;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v2}, Lmaps/ac/av;-><init>(II)V

    sput-object v0, Lmaps/ay/q;->a:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/av;

    const/high16 v1, -0x40000000    # -2.0f

    invoke-direct {v0, v1, v2}, Lmaps/ac/av;-><init>(II)V

    sput-object v0, Lmaps/ay/q;->b:Lmaps/ac/av;

    return-void
.end method

.method public constructor <init>(Lmaps/ac/az;FI)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmaps/ay/q;-><init>(Lmaps/ac/az;FIZ)V

    return-void
.end method

.method public constructor <init>(Lmaps/ac/az;FIZ)V
    .locals 3

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/q;->r:Z

    iput-object p1, p0, Lmaps/ay/q;->c:Lmaps/ac/az;

    iput p2, p0, Lmaps/ay/q;->n:F

    iput p3, p0, Lmaps/ay/q;->o:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ay/q;->f:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/q;->g:Lmaps/ac/r;

    iput-boolean p4, p0, Lmaps/ay/q;->r:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lmaps/al/h;->a(Ljava/util/List;)I

    move-result v1

    iput v1, p0, Lmaps/ay/q;->q:I

    new-instance v1, Lmaps/at/n;

    iget v2, p0, Lmaps/ay/q;->q:I

    invoke-direct {v1, v2}, Lmaps/at/n;-><init>(I)V

    iput-object v1, p0, Lmaps/ay/q;->k:Lmaps/at/n;

    new-instance v1, Lmaps/at/i;

    iget v2, p0, Lmaps/ay/q;->q:I

    invoke-direct {v1, v2}, Lmaps/at/i;-><init>(I)V

    iput-object v1, p0, Lmaps/ay/q;->m:Lmaps/at/i;

    new-instance v1, Lmaps/at/d;

    invoke-static {v0}, Lmaps/al/h;->b(Ljava/util/List;)I

    move-result v0

    invoke-direct {v1, v0}, Lmaps/at/d;-><init>(I)V

    iput-object v1, p0, Lmaps/ay/q;->l:Lmaps/at/d;

    return-void
.end method

.method private a(Lmaps/ar/a;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v4, 0x3fa00000    # 1.25f

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/ay/q;->p:Z

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ay/q;->p:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    return v0

    :cond_1
    monitor-exit p0

    invoke-virtual {p1}, Lmaps/ar/a;->m()F

    move-result v2

    iget v3, p0, Lmaps/ay/q;->j:F

    mul-float/2addr v3, v4

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_0

    iget v3, p0, Lmaps/ay/q;->j:F

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lmaps/ar/a;)V
    .locals 19

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/q;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->p()F

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/q;->c:Lmaps/ac/az;

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cw;->b()Lmaps/ac/bd;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/ac/bd;->f()I

    move-result v3

    invoke-virtual {v5}, Lmaps/ac/bd;->g()I

    move-result v4

    const v6, 0x71c71c7

    if-gt v3, v6, :cond_0

    const v6, 0x71c71c7

    if-le v4, v6, :cond_4

    :cond_0
    new-instance v3, Lmaps/ac/av;

    invoke-virtual {v5}, Lmaps/ac/bd;->e()Lmaps/ac/av;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/av;->f()I

    move-result v4

    const/high16 v6, 0x20000000

    sub-int/2addr v4, v6

    const/high16 v6, -0x40000000    # -2.0f

    invoke-direct {v3, v4, v6}, Lmaps/ac/av;-><init>(II)V

    new-instance v4, Lmaps/ac/av;

    invoke-virtual {v5}, Lmaps/ac/bd;->e()Lmaps/ac/av;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/ac/av;->f()I

    move-result v5

    const/high16 v6, 0x20000000

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    const v6, 0x3fffffff    # 1.9999999f

    invoke-direct {v4, v5, v6}, Lmaps/ac/av;-><init>(II)V

    move-object v5, v4

    move-object v4, v3

    :goto_1
    new-instance v3, Lmaps/ac/bd;

    invoke-direct {v3, v4, v5}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->p()F

    move-result v3

    float-to-int v3, v3

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lmaps/ay/q;->r:Z

    invoke-static {v3, v7}, Lmaps/ay/x;->a(IZ)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lmaps/ac/az;->b(F)Lmaps/ac/az;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/ac/az;->b()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_5

    const/4 v2, 0x1

    new-array v2, v2, [Lmaps/ac/az;

    const/4 v3, 0x0

    aput-object v7, v2, v3

    invoke-static {v2}, Lmaps/m/ck;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ac/az;

    new-instance v3, Lmaps/ac/h;

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    invoke-direct {v3, v8}, Lmaps/ac/h;-><init>(Lmaps/ac/be;)V

    invoke-virtual {v3, v2, v6}, Lmaps/ac/h;->a(Lmaps/ac/az;Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/q;->f:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v6}, Ljava/util/List;->clear()V

    new-instance v3, Lmaps/ac/bd;

    sget-object v8, Lmaps/ay/q;->a:Lmaps/ac/av;

    invoke-virtual {v4, v8}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v8

    sget-object v9, Lmaps/ay/q;->a:Lmaps/ac/av;

    invoke-virtual {v5, v9}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v9

    invoke-direct {v3, v8, v9}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    new-instance v8, Lmaps/ac/h;

    invoke-direct {v8, v3}, Lmaps/ac/h;-><init>(Lmaps/ac/be;)V

    invoke-virtual {v8, v2, v6}, Lmaps/ac/h;->a(Lmaps/ac/az;Ljava/util/List;)V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/ac/az;

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/ay/q;->f:Ljava/util/List;

    sget-object v10, Lmaps/ay/q;->b:Lmaps/ac/av;

    invoke-virtual {v3, v10}, Lmaps/ac/az;->b(Lmaps/ac/av;)Lmaps/ac/az;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/q;->d:Lmaps/ac/az;

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/q;->c:Lmaps/ac/az;

    const/16 v4, 0xa

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lmaps/ay/q;->r:Z

    invoke-static {v4, v5}, Lmaps/ay/x;->a(IZ)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lmaps/ac/az;->b(F)Lmaps/ac/az;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/q;->d:Lmaps/ac/az;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/q;->d:Lmaps/ac/az;

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lmaps/ay/q;->r:Z

    invoke-static {v4, v5}, Lmaps/ay/x;->a(IZ)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lmaps/ac/az;->b(F)Lmaps/ac/az;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/q;->e:Lmaps/ac/az;

    :cond_2
    const/high16 v3, 0x40c00000    # 6.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/q;->d:Lmaps/ac/az;

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/q;->e:Lmaps/ac/az;

    goto/16 :goto_0

    :cond_4
    new-instance v6, Lmaps/ac/av;

    mul-int/lit8 v3, v3, 0x4

    mul-int/lit8 v4, v4, 0x4

    invoke-direct {v6, v3, v4}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v5}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmaps/ac/av;->f(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {v5}, Lmaps/ac/bd;->d()Lmaps/ac/av;

    move-result-object v4

    invoke-virtual {v4, v6}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v4

    move-object v5, v4

    move-object v4, v3

    goto/16 :goto_1

    :cond_5
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Lmaps/ac/bb;

    invoke-virtual {v7}, Lmaps/ac/az;->b()I

    move-result v2

    invoke-direct {v8, v2}, Lmaps/ac/bb;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v9

    invoke-virtual {v8, v9}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    new-instance v10, Lmaps/ac/av;

    invoke-direct {v10}, Lmaps/ac/av;-><init>()V

    const/4 v2, 0x1

    :goto_5
    invoke-virtual {v7}, Lmaps/ac/az;->b()I

    move-result v11

    if-ge v2, v11, :cond_8

    invoke-virtual {v7, v2, v10}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-virtual {v10}, Lmaps/ac/av;->f()I

    move-result v11

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v12

    if-ge v11, v12, :cond_7

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v11

    invoke-virtual {v10}, Lmaps/ac/av;->f()I

    move-result v12

    sub-int/2addr v11, v12

    invoke-virtual {v10}, Lmaps/ac/av;->f()I

    move-result v12

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v13

    sub-int/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    add-int/2addr v12, v13

    if-ge v12, v11, :cond_6

    const/high16 v11, 0x20000000

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v13

    sub-int/2addr v11, v13

    invoke-virtual {v10}, Lmaps/ac/av;->g()I

    move-result v13

    invoke-virtual {v9}, Lmaps/ac/av;->g()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-virtual {v9}, Lmaps/ac/av;->g()I

    move-result v14

    int-to-double v15, v13

    int-to-double v0, v11

    move-wide/from16 v17, v0

    mul-double v15, v15, v17

    int-to-double v11, v12

    div-double v11, v15, v11

    invoke-static {v11, v12}, Ljava/lang/Math;->round(D)J

    move-result-wide v11

    long-to-int v11, v11

    add-int/2addr v11, v14

    new-instance v12, Lmaps/ac/av;

    const v13, 0x1fffffff

    invoke-direct {v12, v13, v11}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v8, v12}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v8}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v12

    invoke-interface {v3, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8}, Lmaps/ac/bb;->c()V

    new-instance v12, Lmaps/ac/av;

    const/high16 v13, -0x20000000

    invoke-direct {v12, v13, v11}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v8, v12}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    :cond_6
    :goto_6
    invoke-virtual {v8, v10}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v9, v10}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {v10}, Lmaps/ac/av;->f()I

    move-result v11

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v12

    if-le v11, v12, :cond_6

    invoke-virtual {v10}, Lmaps/ac/av;->f()I

    move-result v11

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v12

    sub-int/2addr v11, v12

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v12

    invoke-virtual {v10}, Lmaps/ac/av;->f()I

    move-result v13

    sub-int/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    add-int/2addr v12, v13

    if-ge v12, v11, :cond_6

    invoke-virtual {v9}, Lmaps/ac/av;->f()I

    move-result v11

    const/high16 v13, 0x20000000

    add-int/2addr v11, v13

    invoke-virtual {v10}, Lmaps/ac/av;->g()I

    move-result v13

    invoke-virtual {v9}, Lmaps/ac/av;->g()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-virtual {v9}, Lmaps/ac/av;->g()I

    move-result v14

    int-to-double v15, v13

    int-to-double v0, v11

    move-wide/from16 v17, v0

    mul-double v15, v15, v17

    int-to-double v11, v12

    div-double v11, v15, v11

    invoke-static {v11, v12}, Ljava/lang/Math;->round(D)J

    move-result-wide v11

    long-to-int v11, v11

    add-int/2addr v11, v14

    new-instance v12, Lmaps/ac/av;

    const/high16 v13, -0x20000000

    invoke-direct {v12, v13, v11}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v8, v12}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v8}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v12

    invoke-interface {v3, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8}, Lmaps/ac/bb;->c()V

    new-instance v12, Lmaps/ac/av;

    const v13, 0x1fffffff

    invoke-direct {v12, v13, v11}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v8, v12}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    goto :goto_6

    :cond_8
    invoke-virtual {v8}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/az;->b()I

    move-result v7

    const/4 v8, 0x2

    if-lt v7, v8, :cond_9

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    move-object v2, v3

    goto/16 :goto_2

    :cond_a
    invoke-interface {v6}, Ljava/util/List;->clear()V

    new-instance v3, Lmaps/ac/bd;

    sget-object v8, Lmaps/ay/q;->b:Lmaps/ac/av;

    invoke-virtual {v4, v8}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v8

    sget-object v9, Lmaps/ay/q;->b:Lmaps/ac/av;

    invoke-virtual {v5, v9}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v9

    invoke-direct {v3, v8, v9}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    new-instance v8, Lmaps/ac/h;

    invoke-direct {v8, v3}, Lmaps/ac/h;-><init>(Lmaps/ac/be;)V

    invoke-virtual {v8, v2, v6}, Lmaps/ac/h;->a(Lmaps/ac/az;Ljava/util/List;)V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ac/az;

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/ay/q;->f:Ljava/util/List;

    sget-object v9, Lmaps/ay/q;->a:Lmaps/ac/av;

    invoke-virtual {v2, v9}, Lmaps/ac/az;->b(Lmaps/ac/av;)Lmaps/ac/az;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_b
    invoke-interface {v6}, Ljava/util/List;->clear()V

    goto/16 :goto_3

    :cond_c
    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->m()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lmaps/ay/q;->i:F

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(F)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/ay/q;->n:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/q;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/q;->k:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/q;->l:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/q;->m:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 12

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lmaps/ay/q;->b(Lmaps/ar/a;)V

    :cond_0
    invoke-direct {p0, p2}, Lmaps/ay/q;->a(Lmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ay/q;->k:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/q;->l:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/q;->m:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/az;

    iget-object v0, p0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    invoke-virtual {v0}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v4

    iget-object v0, p0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    invoke-virtual {v0}, Lmaps/ac/bd;->f()I

    move-result v5

    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Lmaps/ar/a;->v()F

    move-result v0

    iget v2, p0, Lmaps/ay/q;->n:F

    mul-float/2addr v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    const/high16 v2, 0x40e00000    # 7.0f

    div-float/2addr v0, v2

    const/high16 v2, 0x41000000    # 8.0f

    mul-float/2addr v2, v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lmaps/al/h;->a()Lmaps/al/h;

    move-result-object v0

    const/4 v6, 0x0

    const/high16 v7, 0x10000

    iget-object v8, p0, Lmaps/ay/q;->k:Lmaps/at/n;

    iget-object v9, p0, Lmaps/ay/q;->l:Lmaps/at/d;

    iget-object v10, p0, Lmaps/ay/q;->m:Lmaps/at/i;

    move v3, v2

    invoke-virtual/range {v0 .. v10}, Lmaps/al/h;->a(Lmaps/ac/az;FFLmaps/ac/av;IIILmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    invoke-virtual {p2}, Lmaps/ar/a;->m()F

    move-result v0

    iput v0, p0, Lmaps/ay/q;->j:F

    :cond_2
    iget-object v0, p0, Lmaps/ay/q;->k:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->a()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    iget-object v0, p0, Lmaps/ay/q;->g:Lmaps/ac/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ay/q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ay/q;->f:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v0

    if-nez v0, :cond_7

    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_4

    iget-object v0, p0, Lmaps/ay/q;->f:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/az;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v0

    invoke-interface {v1, p1, p2, p3, v0}, Lmaps/aj/af;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;Lmaps/ac/av;)V

    :cond_4
    iget-object v0, p0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    invoke-virtual {v0}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v0

    iget-object v4, p0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    invoke-virtual {v4}, Lmaps/ac/bd;->f()I

    move-result v4

    int-to-float v4, v4

    invoke-static {p1, p2, v0, v4}, Lmaps/ap/o;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    invoke-virtual {p1}, Lmaps/as/a;->r()V

    const/4 v0, 0x1

    const/16 v4, 0x303

    invoke-interface {v3, v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v0, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v3, v0, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    monitor-enter p0

    :try_start_1
    iget v0, p0, Lmaps/ay/q;->o:I

    shr-int/lit8 v0, v0, 0x10

    const v4, 0xff00

    and-int/2addr v0, v4

    iget v4, p0, Lmaps/ay/q;->o:I

    shr-int/lit8 v4, v4, 0x8

    const v5, 0xff00

    and-int/2addr v4, v5

    iget v5, p0, Lmaps/ay/q;->o:I

    const v6, 0xff00

    and-int/2addr v5, v6

    iget v6, p0, Lmaps/ay/q;->o:I

    shl-int/lit8 v6, v6, 0x8

    const v7, 0xff00

    and-int/2addr v6, v7

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-interface {v3, v4, v5, v6, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    invoke-virtual {p1}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v0

    const/16 v3, 0x18

    invoke-virtual {v0, v3}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/ay/q;->k:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/q;->m:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/q;->l:Lmaps/at/d;

    const/4 v3, 0x4

    invoke-virtual {v0, p1, v3}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    invoke-interface {v0, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    if-eqz v1, :cond_5

    invoke-interface {v1, p1, p3}, Lmaps/aj/af;->a(Lmaps/as/a;Lmaps/ap/c;)V

    :cond_5
    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_6
    return-void

    :cond_7
    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    move-result-object v0

    iget-object v1, p0, Lmaps/ay/q;->g:Lmaps/ac/r;

    invoke-virtual {v0, v1}, Lmaps/ab/q;->e(Lmaps/ac/r;)Lmaps/ab/k;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/ay/q;->o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 4

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v1, 0x1

    iget-object v0, p0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lmaps/ay/q;->b(Lmaps/ar/a;)V

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/ay/q;->p:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    return v1

    :cond_1
    invoke-virtual {p1}, Lmaps/ar/a;->m()F

    move-result v0

    iget v2, p0, Lmaps/ay/q;->i:F

    mul-float/2addr v2, v3

    cmpl-float v2, v0, v2

    if-gtz v2, :cond_2

    iget v2, p0, Lmaps/ay/q;->i:F

    div-float/2addr v2, v3

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/ay/q;->h:Lmaps/ac/bd;

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/cw;->c()Lmaps/ac/be;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/ac/bd;->b(Lmaps/ac/be;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Lmaps/as/a;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/ay/q;->a(Lmaps/as/a;)V

    return-void
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    sget-object v0, Lmaps/ay/v;->a:Lmaps/ay/v;

    return-object v0
.end method
