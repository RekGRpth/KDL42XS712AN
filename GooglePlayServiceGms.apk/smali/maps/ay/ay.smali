.class final Lmaps/ay/ay;
.super Lmaps/ak/a;


# instance fields
.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private volatile h:Z

.field private final i:Lmaps/ak/b;

.field private final j:[F


# direct methods
.method public constructor <init>(Lmaps/ar/b;Lmaps/ak/b;)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/ak/a;-><init>(Lmaps/ar/b;)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/ay/ay;->j:[F

    iput-object p2, p0, Lmaps/ay/ay;->i:Lmaps/ak/b;

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ar/a;)Lmaps/ar/c;
    .locals 11

    monitor-enter p0

    :try_start_0
    iget v4, p0, Lmaps/ay/ay;->f:F

    iget v5, p0, Lmaps/ay/ay;->g:F

    iget v7, p0, Lmaps/ay/ay;->d:F

    iget v8, p0, Lmaps/ay/ay;->e:F

    iget v0, p0, Lmaps/ay/ay;->b:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget v0, p0, Lmaps/ay/ay;->b:F

    iget v1, p0, Lmaps/ay/ay;->b:F

    iget v2, p0, Lmaps/ay/ay;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lmaps/ay/ay;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    :goto_0
    iget v0, p0, Lmaps/ay/ay;->c:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p0, Lmaps/ay/ay;->c:F

    iget v1, p0, Lmaps/ay/ay;->c:F

    iget v2, p0, Lmaps/ay/ay;->c:F

    mul-float/2addr v1, v2

    const v2, -0x42333333    # -0.1f

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v6, v0

    :goto_1
    iget v0, p0, Lmaps/ay/ay;->b:F

    sub-float/2addr v0, v3

    iput v0, p0, Lmaps/ay/ay;->b:F

    iget v0, p0, Lmaps/ay/ay;->c:F

    sub-float/2addr v0, v6

    iput v0, p0, Lmaps/ay/ay;->c:F

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ay/ay;->d:F

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ay/ay;->e:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v9

    if-gez v0, :cond_3

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v9

    if-gez v0, :cond_3

    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/ay;->h:Z

    iget-object v0, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    monitor-exit p0

    move-object p0, v0

    :cond_0
    :goto_2
    return-object p0

    :cond_1
    iget v0, p0, Lmaps/ay/ay;->b:F

    iget v1, p0, Lmaps/ay/ay;->b:F

    iget v2, p0, Lmaps/ay/ay;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lmaps/ay/ay;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto :goto_0

    :cond_2
    iget v0, p0, Lmaps/ay/ay;->c:F

    iget v1, p0, Lmaps/ay/ay;->c:F

    iget v2, p0, Lmaps/ay/ay;->c:F

    mul-float/2addr v1, v2

    const v2, 0x3dcccccd    # 0.1f

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    move v6, v0

    goto :goto_1

    :cond_3
    monitor-exit p0

    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_8

    :cond_4
    const/4 v0, 0x1

    move v2, v0

    :goto_3
    const/4 v0, 0x0

    cmpl-float v0, v6, v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    move v1, v0

    :goto_4
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_5
    if-eqz v2, :cond_6

    iget-object v2, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    invoke-static {v2, p1, v7, v8}, Lmaps/ay/au;->a(Lmaps/ar/b;Lmaps/ar/a;FF)Lmaps/ar/b;

    move-result-object v2

    iput-object v2, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    if-nez v1, :cond_5

    if-eqz v0, :cond_6

    :cond_5
    iget-object v2, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    invoke-virtual {p1, v2}, Lmaps/ar/a;->a(Lmaps/ar/b;)V

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {p1, v4, v5}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v1

    iget-object v2, p0, Lmaps/ay/ay;->i:Lmaps/ak/b;

    invoke-static {p1, v2, v1, v6}, Lmaps/ay/au;->a(Lmaps/ar/a;Lmaps/ak/b;Lmaps/ac/av;F)Lmaps/ar/b;

    move-result-object v1

    iput-object v1, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    if-eqz v0, :cond_7

    iget-object v1, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    invoke-virtual {p1, v1}, Lmaps/ar/a;->a(Lmaps/ar/b;)V

    :cond_7
    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    iget-object v2, p0, Lmaps/ay/ay;->i:Lmaps/ak/b;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lmaps/ay/au;->a(Lmaps/ar/b;Lmaps/ar/a;Lmaps/ak/b;FFF)Lmaps/ar/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_8
    const/4 v0, 0x0

    move v2, v0

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto :goto_4

    :cond_a
    const/4 v0, 0x0

    goto :goto_5
.end method

.method final declared-synchronized a(FFFFFF)[F
    .locals 4

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/ay/ay;->b:F

    add-float/2addr v0, p1

    iput v0, p0, Lmaps/ay/ay;->b:F

    iget v0, p0, Lmaps/ay/ay;->c:F

    add-float/2addr v0, p2

    iput v0, p0, Lmaps/ay/ay;->c:F

    iget v0, p0, Lmaps/ay/ay;->d:F

    add-float/2addr v0, p5

    iput v0, p0, Lmaps/ay/ay;->d:F

    iget v0, p0, Lmaps/ay/ay;->e:F

    add-float/2addr v0, p6

    iput v0, p0, Lmaps/ay/ay;->e:F

    cmpl-float v0, p1, v1

    if-nez v0, :cond_0

    cmpl-float v0, p2, v1

    if-eqz v0, :cond_1

    :cond_0
    iput p3, p0, Lmaps/ay/ay;->f:F

    iput p4, p0, Lmaps/ay/ay;->g:F

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ay;->h:Z

    iget-object v0, p0, Lmaps/ay/ay;->j:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    invoke-virtual {v2}, Lmaps/ar/b;->b()F

    move-result v2

    iget v3, p0, Lmaps/ay/ay;->b:F

    add-float/2addr v2, v3

    invoke-static {}, Lmaps/ay/au;->f()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/ay/ay;->j:[F

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/ay/ay;->a:Lmaps/ar/b;

    invoke-virtual {v2}, Lmaps/ar/b;->e()F

    move-result v2

    iget v3, p0, Lmaps/ay/ay;->c:F

    add-float/2addr v2, v3

    invoke-static {v2}, Lmaps/ay/au;->b(F)F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/ay/ay;->j:[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()I
    .locals 1

    iget-boolean v0, p0, Lmaps/ay/ay;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
