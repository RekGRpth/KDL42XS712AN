.class public final Lmaps/ay/a;
.super Lmaps/ay/u;


# static fields
.field private static i:Lmaps/at/n;

.field private static j:Lmaps/at/d;

.field private static k:Lmaps/at/n;

.field private static l:Lmaps/at/d;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lmaps/ac/av;

.field private c:Lmaps/ac/r;

.field private d:Lmaps/ac/bd;

.field private e:I

.field private f:F

.field private g:I

.field private h:I

.field private m:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x64

    new-instance v0, Lmaps/at/n;

    invoke-direct {v0, v1}, Lmaps/at/n;-><init>(I)V

    sput-object v0, Lmaps/ay/a;->i:Lmaps/at/n;

    new-instance v0, Lmaps/at/d;

    invoke-direct {v0, v1}, Lmaps/at/d;-><init>(I)V

    sput-object v0, Lmaps/ay/a;->j:Lmaps/at/d;

    new-instance v0, Lmaps/at/n;

    const/16 v1, 0x65

    invoke-direct {v0, v1}, Lmaps/at/n;-><init>(I)V

    sput-object v0, Lmaps/ay/a;->k:Lmaps/at/n;

    new-instance v0, Lmaps/at/d;

    const/16 v1, 0x66

    invoke-direct {v0, v1}, Lmaps/at/d;-><init>(I)V

    sput-object v0, Lmaps/ay/a;->l:Lmaps/at/d;

    sget-object v0, Lmaps/ay/a;->i:Lmaps/at/n;

    sget-object v1, Lmaps/ay/a;->j:Lmaps/at/d;

    invoke-static {v0, v1}, Lmaps/al/r;->a(Lmaps/at/o;Lmaps/at/e;)V

    sget-object v0, Lmaps/ay/a;->k:Lmaps/at/n;

    sget-object v1, Lmaps/ay/a;->l:Lmaps/at/d;

    invoke-static {v0, v1}, Lmaps/al/r;->b(Lmaps/at/o;Lmaps/at/e;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    iput-object v1, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    iput v0, p0, Lmaps/ay/a;->e:I

    invoke-direct {p0}, Lmaps/ay/a;->b()V

    iput v0, p0, Lmaps/ay/a;->g:I

    iput v0, p0, Lmaps/ay/a;->h:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmaps/ay/a;->m:F

    iput-object v1, p0, Lmaps/ay/a;->c:Lmaps/ac/r;

    iput-object p1, p0, Lmaps/ay/a;->a:Ljava/lang/String;

    return-void
.end method

.method private b()V
    .locals 3

    iget v0, p0, Lmaps/ay/a;->e:I

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lmaps/ay/a;->f:F

    iget-object v0, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    iget v1, p0, Lmaps/ay/a;->f:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lmaps/ac/bd;->a(Lmaps/ac/av;I)Lmaps/ac/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/a;->d:Lmaps/ac/bd;

    :cond_1
    return-void

    :cond_2
    int-to-float v0, v0

    invoke-virtual {v1}, Lmaps/ac/av;->e()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 0

    iput p1, p0, Lmaps/ay/a;->m:F

    return-void
.end method

.method public final a(Lmaps/ac/av;I)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lmaps/ay/a;->e:I

    if-eq v0, p2, :cond_1

    :cond_0
    iput-object p1, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    iput p2, p0, Lmaps/ay/a;->e:I

    invoke-direct {p0}, Lmaps/ay/a;->b()V

    :cond_1
    return-void
.end method

.method public final a(Lmaps/as/a;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 4

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/ay/a;->f:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/cw;->a()Lmaps/ac/cx;

    move-result-object v0

    iget-object v1, p0, Lmaps/ay/a;->d:Lmaps/ac/bd;

    invoke-virtual {v0, v1}, Lmaps/ac/cx;->b(Lmaps/ac/be;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lmaps/ay/a;->c:Lmaps/ac/r;

    if-eqz v1, :cond_2

    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    move-result-object v1

    iget-object v2, p0, Lmaps/ay/a;->c:Lmaps/ac/r;

    invoke-virtual {v1, v2}, Lmaps/ab/q;->e(Lmaps/ac/r;)Lmaps/ab/k;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    invoke-virtual {v1, p2, v3}, Lmaps/ab/k;->a(Lmaps/ar/a;Lmaps/ac/av;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v1}, Lmaps/ac/av;->b(I)V

    :cond_2
    invoke-virtual {p2}, Lmaps/ar/a;->v()F

    move-result v1

    iget-object v2, p0, Lmaps/ay/a;->b:Lmaps/ac/av;

    invoke-static {p1, p2, v2, v1}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    iget v2, p0, Lmaps/ay/a;->f:F

    div-float v1, v2, v1

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v1, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/16 v1, 0x302

    const/16 v3, 0x303

    invoke-interface {v2, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    sget-object v1, Lmaps/ay/a;->k:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget v1, p0, Lmaps/ay/a;->h:I

    invoke-static {v2, v1}, Lmaps/al/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    sget-object v1, Lmaps/ay/a;->l:Lmaps/at/d;

    const/4 v3, 0x6

    invoke-virtual {v1, p1, v3}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    sget-object v1, Lmaps/ay/a;->i:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget v1, p0, Lmaps/ay/a;->g:I

    invoke-static {v2, v1}, Lmaps/al/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget v1, p0, Lmaps/ay/a;->m:F

    invoke-interface {v2, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    sget-object v1, Lmaps/ay/a;->j:Lmaps/at/d;

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v2}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 0

    iput p1, p0, Lmaps/ay/a;->g:I

    return-void
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final c(I)V
    .locals 0

    iput p1, p0, Lmaps/ay/a;->h:I

    return-void
.end method

.method public final c(Lmaps/as/a;)V
    .locals 0

    return-void
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    sget-object v0, Lmaps/ay/v;->a:Lmaps/ay/v;

    return-object v0
.end method
