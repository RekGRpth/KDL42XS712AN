.class public final Lmaps/ay/al;
.super Ljava/lang/Object;


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field private final h:Ljava/lang/Boolean;

.field private final i:Ljava/lang/Boolean;

.field private final j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIII)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ay/al;->h:Ljava/lang/Boolean;

    iput-object p2, p0, Lmaps/ay/al;->i:Ljava/lang/Boolean;

    iput-object p3, p0, Lmaps/ay/al;->j:Ljava/lang/Boolean;

    iput-boolean p4, p0, Lmaps/ay/al;->a:Z

    iput p5, p0, Lmaps/ay/al;->b:I

    iput p6, p0, Lmaps/ay/al;->c:I

    iput v0, p0, Lmaps/ay/al;->d:I

    iput v0, p0, Lmaps/ay/al;->e:I

    iput p7, p0, Lmaps/ay/al;->f:I

    iput p8, p0, Lmaps/ay/al;->g:I

    return-void
.end method


# virtual methods
.method public final a(ZZZ)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ay/al;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/ay/al;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/ay/al;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/ay/al;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p2, :cond_0

    :cond_2
    iget-object v1, p0, Lmaps/ay/al;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/ay/al;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p3, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
