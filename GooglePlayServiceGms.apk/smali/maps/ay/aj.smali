.class public final Lmaps/ay/aj;
.super Lmaps/ay/c;

# interfaces
.implements Lmaps/ap/a;
.implements Lmaps/ay/b;


# static fields
.field private static D:Lmaps/at/a;

.field private static final b:F


# instance fields
.field private A:F

.field private final B:Lmaps/ac/av;

.field private C:Lmaps/ay/ak;

.field private c:F

.field private d:F

.field private e:F

.field private final f:Landroid/content/res/Resources;

.field private final g:Z

.field private h:I

.field private volatile i:Lmaps/ap/n;

.field private final j:Ljava/util/Map;

.field private k:Ljava/util/List;

.field private l:Lmaps/v/k;

.field private m:Lmaps/ay/a;

.field private final n:Lmaps/ac/au;

.field private final o:Lmaps/ac/au;

.field private final p:Lmaps/ac/au;

.field private q:Z

.field private r:F

.field private s:F

.field private t:I

.field private volatile u:Lmaps/ac/ad;

.field private volatile v:Z

.field private w:F

.field private x:F

.field private y:Lmaps/ay/v;

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x4

    const/high16 v0, 0x40000000    # 2.0f

    sput v0, Lmaps/ay/aj;->b:F

    new-instance v0, Lmaps/at/a;

    invoke-direct {v0, v2}, Lmaps/at/a;-><init>(I)V

    sput-object v0, Lmaps/ay/aj;->D:Lmaps/at/a;

    const v1, 0x73217bce

    invoke-virtual {v0, v1, v2}, Lmaps/at/a;->b(II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lmaps/ay/o;Z)V
    .locals 6

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, p2}, Lmaps/ay/c;-><init>(Lmaps/ay/o;)V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/aj;->j:Ljava/util/Map;

    iput-boolean v4, p0, Lmaps/ay/aj;->q:Z

    iput-boolean v3, p0, Lmaps/ay/aj;->v:Z

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/ay/aj;->B:Lmaps/ac/av;

    new-instance v0, Lmaps/ac/au;

    invoke-direct {v0}, Lmaps/ac/au;-><init>()V

    iput-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    new-instance v0, Lmaps/ac/au;

    invoke-direct {v0}, Lmaps/ac/au;-><init>()V

    iput-object v0, p0, Lmaps/ay/aj;->o:Lmaps/ac/au;

    new-instance v0, Lmaps/ac/au;

    invoke-direct {v0}, Lmaps/ac/au;-><init>()V

    iput-object v0, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    iput-object p1, p0, Lmaps/ay/aj;->f:Landroid/content/res/Resources;

    iput-boolean v3, p0, Lmaps/ay/aj;->g:Z

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/ay/al;

    new-instance v1, Lmaps/ay/am;

    invoke-direct {v1}, Lmaps/ay/am;-><init>()V

    invoke-virtual {v1, v3}, Lmaps/ay/am;->a(Z)Lmaps/ay/am;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ay/am;->b()Lmaps/ay/am;

    move-result-object v1

    sget v2, Lmaps/b/e;->f:I

    invoke-virtual {v1, v2}, Lmaps/ay/am;->a(I)Lmaps/ay/am;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ay/am;->d()Lmaps/ay/al;

    move-result-object v1

    aput-object v1, v0, v4

    new-instance v1, Lmaps/ay/am;

    invoke-direct {v1}, Lmaps/ay/am;-><init>()V

    invoke-virtual {v1, v4}, Lmaps/ay/am;->a(Z)Lmaps/ay/am;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ay/am;->c()Lmaps/ay/am;

    move-result-object v1

    sget v2, Lmaps/b/e;->e:I

    invoke-virtual {v1, v2}, Lmaps/ay/am;->a(I)Lmaps/ay/am;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ay/am;->d()Lmaps/ay/al;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lmaps/ay/aj;->a([Lmaps/ay/al;)V

    const/high16 v0, 0x42800000    # 64.0f

    iput v0, p0, Lmaps/ay/aj;->s:F

    iput v5, p0, Lmaps/ay/aj;->w:F

    iput v5, p0, Lmaps/ay/aj;->x:F

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lmaps/ay/aj;->a(F)V

    const/16 v0, 0x4000

    invoke-virtual {p0, v0}, Lmaps/ay/aj;->b(I)V

    sget-object v0, Lmaps/ay/v;->m:Lmaps/ay/v;

    iput-object v0, p0, Lmaps/ay/aj;->y:Lmaps/ay/v;

    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Lmaps/ay/aj;->c:F

    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lmaps/ay/aj;->d:F

    const/high16 v0, 0x3f400000    # 0.75f

    iput v0, p0, Lmaps/ay/aj;->e:F

    iget-object v0, p0, Lmaps/ay/aj;->f:Landroid/content/res/Resources;

    sget v1, Lmaps/b/d;->j:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lmaps/ay/aj;->z:I

    new-instance v0, Lmaps/v/l;

    invoke-direct {v0}, Lmaps/v/l;-><init>()V

    iput-object v0, p0, Lmaps/ay/aj;->l:Lmaps/v/k;

    return-void
.end method

.method private declared-synchronized a(Lmaps/as/a;I)Lmaps/as/b;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/aj;->j:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/as/b;

    invoke-direct {v0, p1}, Lmaps/as/b;-><init>(Lmaps/as/a;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/as/b;->c(Z)V

    iget-object v1, p0, Lmaps/ay/aj;->f:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, Lmaps/as/b;->a(Landroid/content/res/Resources;I)V

    iget-object v1, p0, Lmaps/ay/aj;->j:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ZZZ)Lmaps/ay/al;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/aj;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/al;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/al;->a(ZZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(F)V
    .locals 1

    iget v0, p0, Lmaps/ay/aj;->s:F

    mul-float/2addr v0, p1

    iput v0, p0, Lmaps/ay/aj;->r:F

    invoke-direct {p0}, Lmaps/ay/aj;->k()V

    return-void
.end method

.method private declared-synchronized b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 11

    const/4 v0, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v1}, Lmaps/ac/au;->c()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lmaps/ay/aj;->m:Lmaps/ay/a;

    if-nez v1, :cond_0

    new-instance v1, Lmaps/ay/a;

    const-string v2, "MyLocation"

    invoke-direct {v1, v2}, Lmaps/ay/a;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lmaps/ay/aj;->m:Lmaps/ay/a;

    iget-object v1, p0, Lmaps/ay/aj;->m:Lmaps/ay/a;

    sget v2, Lmaps/ay/aj;->b:F

    invoke-virtual {v1, v2}, Lmaps/ay/a;->a(F)V

    :cond_0
    iget-object v1, p0, Lmaps/ay/aj;->m:Lmaps/ay/a;

    iget-object v2, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v2}, Lmaps/ac/au;->d()Lmaps/ac/av;

    move-result-object v2

    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->c()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    invoke-virtual {v1, v2, v3}, Lmaps/ay/a;->a(Lmaps/ac/av;I)V

    invoke-direct {p0}, Lmaps/ay/aj;->r()Lmaps/ay/al;

    move-result-object v1

    iget-object v2, p0, Lmaps/ay/aj;->m:Lmaps/ay/a;

    iget v3, v1, Lmaps/ay/al;->f:I

    invoke-virtual {v2, v3}, Lmaps/ay/a;->b(I)V

    iget-object v2, p0, Lmaps/ay/aj;->m:Lmaps/ay/a;

    iget v1, v1, Lmaps/ay/al;->g:I

    invoke-virtual {v2, v1}, Lmaps/ay/a;->c(I)V

    iget-object v1, p0, Lmaps/ay/aj;->m:Lmaps/ay/a;

    invoke-virtual {v1, p1, p2, p3}, Lmaps/ay/a;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    :cond_1
    iget-object v1, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v1}, Lmaps/ac/au;->a()Lmaps/ac/av;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Lmaps/ar/a;->a(Lmaps/ac/av;Z)F

    move-result v2

    invoke-direct {p0}, Lmaps/ay/aj;->r()Lmaps/ay/al;

    move-result-object v1

    iget-boolean v1, v1, Lmaps/ay/al;->a:Z

    if-eqz v1, :cond_7

    iget v1, p0, Lmaps/ay/aj;->r:F

    :goto_0
    invoke-virtual {p2, v1, v2}, Lmaps/ar/a;->a(FF)F

    move-result v1

    iget v2, p0, Lmaps/ay/aj;->A:F

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->a()Lmaps/ac/av;

    move-result-object v3

    invoke-static {p1, p2, v3, v1}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    invoke-virtual {p1}, Lmaps/as/a;->r()V

    iget-object v1, p1, Lmaps/as/a;->f:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v1, p1, Lmaps/as/a;->b:Lmaps/at/i;

    invoke-virtual {v1, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    invoke-direct {p0}, Lmaps/ay/aj;->r()Lmaps/ay/al;

    move-result-object v1

    iget v3, v1, Lmaps/ay/al;->c:I

    iget v4, v1, Lmaps/ay/al;->b:I

    if-eq v3, v4, :cond_2

    const/4 v0, 0x1

    :cond_2
    iget-boolean v3, v1, Lmaps/ay/al;->a:Z

    if-nez v3, :cond_3

    iget v3, p0, Lmaps/ay/aj;->t:I

    iget v4, p0, Lmaps/ay/aj;->t:I

    iget v5, p0, Lmaps/ay/aj;->t:I

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    :cond_3
    const/4 v3, 0x1

    const/16 v4, 0x303

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget v3, v1, Lmaps/ay/al;->d:I

    if-eqz v3, :cond_4

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v3, v1, Lmaps/ay/al;->d:I

    invoke-direct {p0, p1, v3}, Lmaps/ay/aj;->a(Lmaps/as/a;I)Lmaps/as/b;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->h()F

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    mul-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v4}, Lmaps/ac/au;->h()F

    move-result v4

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    sub-float v4, v10, v4

    iget-object v5, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v5}, Lmaps/ac/au;->h()F

    move-result v5

    const/high16 v6, 0x40800000    # 4.0f

    mul-float/2addr v5, v6

    iget-object v6, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v6}, Lmaps/ac/au;->h()F

    move-result v6

    const/high16 v7, -0x3f800000    # -4.0f

    mul-float/2addr v6, v7

    invoke-interface {v2, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    const/4 v4, 0x0

    invoke-interface {v2, v5, v6, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-interface {v2, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/4 v4, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-interface {v2, v4, v7, v8}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    div-float v4, v10, v3

    div-float v7, v10, v3

    div-float v3, v10, v3

    invoke-interface {v2, v4, v7, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    neg-float v3, v5

    neg-float v4, v6

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    :cond_4
    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->h()F

    move-result v3

    cmpl-float v3, v3, v9

    if-eqz v3, :cond_5

    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->h()F

    move-result v3

    iget-object v4, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v4}, Lmaps/ac/au;->a()Lmaps/ac/av;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/av;->e()D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_5
    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->e()Z

    move-result v3

    if-eqz v3, :cond_6

    iget v3, v1, Lmaps/ay/al;->e:I

    if-eqz v3, :cond_6

    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x40000000    # 2.0f

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget v3, v1, Lmaps/ay/al;->e:I

    invoke-direct {p0, p1, v3}, Lmaps/ay/aj;->a(Lmaps/as/a;I)Lmaps/as/b;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f000000    # 0.5f

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->b()F

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_6
    invoke-direct {p0}, Lmaps/ay/aj;->r()Lmaps/ay/al;

    move-result-object v3

    iget-boolean v3, v3, Lmaps/ay/al;->a:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v3}, Lmaps/ac/au;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :goto_1
    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->j()F

    move-result v0

    cmpl-float v0, v0, v10

    if-nez v0, :cond_9

    iget v0, v1, Lmaps/ay/al;->c:I

    invoke-direct {p0, p1, v0}, Lmaps/ay/aj;->a(Lmaps/as/a;I)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    :goto_2
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v3, 0x4

    invoke-interface {v2, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_7
    :try_start_1
    iget v1, p0, Lmaps/ay/aj;->s:F

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p2}, Lmaps/ar/a;->n()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    invoke-virtual {p2}, Lmaps/ar/a;->o()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_9
    :try_start_2
    iget v0, v1, Lmaps/ay/al;->b:I

    invoke-direct {p0, p1, v0}, Lmaps/ay/aj;->a(Lmaps/as/a;I)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private declared-synchronized j()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/aj;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/ay/aj;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private k()V
    .locals 5

    iget-object v0, p0, Lmaps/ay/aj;->C:Lmaps/ay/ak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/aj;->C:Lmaps/ay/ak;

    iget v1, p0, Lmaps/ay/aj;->s:F

    iget v2, p0, Lmaps/ay/aj;->r:F

    iget v3, p0, Lmaps/ay/aj;->t:I

    int-to-float v3, v3

    const/high16 v4, 0x47800000    # 65536.0f

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ay/ak;->a(FFF)V

    :cond_0
    return-void
.end method

.method private declared-synchronized n()Lmaps/ac/au;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private r()Lmaps/ay/al;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/ay/aj;->n()Lmaps/ac/au;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/au;->e()Z

    move-result v1

    invoke-virtual {v0}, Lmaps/ac/au;->g()Z

    move-result v2

    invoke-virtual {v0}, Lmaps/ac/au;->i()Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, v1, v0, v2}, Lmaps/ay/aj;->a(ZZZ)Lmaps/ay/al;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(FFLmaps/ar/a;)I
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->a()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {p3, v0}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    aget v1, v0, v2

    int-to-float v1, v1

    sub-float v1, p1, v1

    aget v2, v0, v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    mul-float/2addr v1, v2

    aget v2, v0, v3

    int-to-float v2, v2

    sub-float v2, p2, v2

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/ar/a;)I
    .locals 1

    iget v0, p0, Lmaps/ay/aj;->h:I

    return v0
.end method

.method public final a(FII)V
    .locals 3

    const/high16 v2, 0x42c80000    # 100.0f

    iget v0, p0, Lmaps/ay/aj;->r:F

    iget v1, p0, Lmaps/ay/aj;->s:F

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v1, p1

    iput v1, p0, Lmaps/ay/aj;->s:F

    int-to-float v1, p2

    div-float/2addr v1, v2

    iput v1, p0, Lmaps/ay/aj;->w:F

    int-to-float v1, p3

    div-float/2addr v1, v2

    iput v1, p0, Lmaps/ay/aj;->x:F

    invoke-direct {p0, v0}, Lmaps/ay/aj;->a(F)V

    invoke-direct {p0}, Lmaps/ay/aj;->k()V

    return-void
.end method

.method public final a(Ljava/util/List;FFLmaps/ar/a;I)V
    .locals 2

    iget-boolean v0, p0, Lmaps/ay/aj;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2, p3, p4}, Lmaps/ay/aj;->a(FFLmaps/ar/a;)I

    move-result v0

    if-ge v0, p5, :cond_0

    new-instance v1, Lmaps/ay/n;

    invoke-direct {v1, p0, p0, v0}, Lmaps/ay/n;-><init>(Lmaps/ay/b;Lmaps/ay/c;I)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final declared-synchronized a(Lmaps/ac/au;)V
    .locals 3

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->k()Z

    move-result v0

    invoke-virtual {p1}, Lmaps/ac/au;->k()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->f()Lmaps/ac/ad;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/ac/au;->f()Lmaps/ac/ad;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->g()Z

    move-result v0

    invoke-virtual {p1}, Lmaps/ac/au;->g()Z

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/aj;->v:Z

    :cond_1
    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0, p1}, Lmaps/ac/au;->a(Lmaps/ac/au;)V

    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ay/aj;->l:Lmaps/v/k;

    iget-object v1, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-interface {v0, v1}, Lmaps/v/k;->b(Lmaps/ac/au;)V

    :goto_0
    iget-object v0, p0, Lmaps/ay/aj;->i:Lmaps/ap/n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ay/aj;->i:Lmaps/ap/n;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/ap/n;->a(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    new-instance v0, Lmaps/v/l;

    invoke-direct {v0}, Lmaps/v/l;-><init>()V

    iput-object v0, p0, Lmaps/ay/aj;->l:Lmaps/v/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 0

    iput-object p2, p0, Lmaps/ay/aj;->i:Lmaps/ap/n;

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 4

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->k()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lmaps/ay/aj;->h:I

    iget v0, p0, Lmaps/ay/aj;->h:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ay/aj;->l:Lmaps/v/k;

    iget-object v1, p0, Lmaps/ay/aj;->o:Lmaps/ac/au;

    invoke-interface {v0, v1}, Lmaps/v/k;->a(Lmaps/ac/au;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ay/aj;->o:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    iget-object v1, p0, Lmaps/ay/aj;->o:Lmaps/ac/au;

    invoke-virtual {v0, v1}, Lmaps/ac/au;->a(Lmaps/ac/au;)V

    invoke-virtual {p1}, Lmaps/as/a;->e()J

    move-result-wide v0

    const-wide/16 v2, 0xc8

    add-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lmaps/as/a;->a(J)V

    :goto_1
    iget-object v0, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->k()Z

    move-result v0

    if-nez v0, :cond_3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    iget-object v1, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0, v1}, Lmaps/ac/au;->a(Lmaps/ac/au;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_3
    monitor-exit p0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/4 v0, 0x0

    iget-object v2, p0, Lmaps/ay/aj;->u:Lmaps/ac/ad;

    if-eqz v2, :cond_4

    invoke-virtual {p3}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v0

    iget-object v2, p0, Lmaps/ay/aj;->u:Lmaps/ac/ad;

    invoke-virtual {v2}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/aj/ad;->a(Lmaps/ac/o;)Lmaps/aj/af;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lmaps/ay/aj;->p:Lmaps/ac/au;

    invoke-virtual {v2}, Lmaps/ac/au;->a()Lmaps/ac/av;

    move-result-object v2

    invoke-interface {v0, p1, p2, p3, v2}, Lmaps/aj/af;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;Lmaps/ac/av;)V

    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lmaps/ay/aj;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    if-eqz v0, :cond_5

    invoke-interface {v0, p1, p3}, Lmaps/aj/af;->a(Lmaps/as/a;Lmaps/ap/c;)V

    :cond_5
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public final a(Lmaps/ay/v;)V
    .locals 0

    iput-object p1, p0, Lmaps/ay/aj;->y:Lmaps/ay/v;

    return-void
.end method

.method public final varargs declared-synchronized a([Lmaps/ay/al;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/m/ay;->a([Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/aj;->k:Ljava/util/List;

    invoke-direct {p0}, Lmaps/ay/aj;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/List;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lmaps/ay/aj;->v:Z

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lmaps/ay/aj;->v:Z

    invoke-virtual {p0}, Lmaps/ay/aj;->p()Lmaps/ac/ad;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/aj;->u:Lmaps/ac/ad;

    const/4 v0, 0x0

    iget-object v3, p0, Lmaps/ay/aj;->u:Lmaps/ac/ad;

    if-eqz v3, :cond_0

    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    move-result-object v0

    iget-object v3, p0, Lmaps/ay/aj;->u:Lmaps/ac/ad;

    invoke-virtual {v3}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/ab/q;->e(Lmaps/ac/r;)Lmaps/ab/k;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object v3, p0, Lmaps/ay/aj;->u:Lmaps/ac/ad;

    if-eqz v3, :cond_1

    if-nez v0, :cond_2

    :cond_1
    invoke-super {p0, p1}, Lmaps/ay/c;->a(Ljava/util/List;)Z

    move-result v0

    :goto_0
    return v0

    :cond_2
    sget-object v3, Lmaps/aj/ae;->i:Lmaps/aj/ae;

    new-array v4, v1, [Lmaps/aj/af;

    aput-object v0, v4, v2

    invoke-virtual {p0, v3, v4}, Lmaps/ay/aj;->a(Lmaps/aj/ae;[Lmaps/aj/af;)Lmaps/aj/ad;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public final am_()Lmaps/ap/a;
    .locals 0

    return-object p0
.end method

.method public final ap_()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ay/aj;->g:Z

    return v0
.end method

.method public final b()V
    .locals 1

    const/high16 v0, 0x41600000    # 14.0f

    iput v0, p0, Lmaps/ay/aj;->c:F

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lmaps/ay/aj;->d:F

    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lmaps/ay/aj;->e:F

    return-void
.end method

.method public final b(I)V
    .locals 0

    iput p1, p0, Lmaps/ay/aj;->t:I

    invoke-direct {p0}, Lmaps/ay/aj;->k()V

    return-void
.end method

.method public final b(Lmaps/ar/a;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/ay/aj;->o()Lmaps/ac/av;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, v0}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v3

    invoke-direct {p0}, Lmaps/ay/aj;->r()Lmaps/ay/al;

    move-result-object v0

    iget-boolean v0, v0, Lmaps/ay/al;->a:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/ay/aj;->r:F

    iget v4, p0, Lmaps/ay/aj;->s:F

    div-float/2addr v0, v4

    :goto_1
    iget v4, p0, Lmaps/ay/aj;->A:F

    mul-float/2addr v4, v0

    invoke-direct {p0}, Lmaps/ay/aj;->r()Lmaps/ay/al;

    move-result-object v0

    iget-boolean v0, v0, Lmaps/ay/al;->a:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/ay/aj;->r:F

    :goto_2
    mul-float/2addr v4, v0

    invoke-direct {p0}, Lmaps/ay/aj;->r()Lmaps/ay/al;

    move-result-object v0

    iget-boolean v0, v0, Lmaps/ay/al;->a:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lmaps/ay/aj;->x:F

    :goto_3
    mul-float/2addr v0, v4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    aget v4, v3, v1

    sub-int/2addr v4, v0

    aget v5, v3, v1

    add-int/2addr v5, v0

    aget v6, v3, v2

    sub-int/2addr v6, v0

    aget v3, v3, v2

    add-int/2addr v0, v3

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v3

    if-ge v4, v3, :cond_4

    if-ltz v5, :cond_4

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v3

    if-ge v6, v3, :cond_4

    if-ltz v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_1
    const/high16 v0, 0x3e800000    # 0.25f

    goto :goto_1

    :cond_2
    iget v0, p0, Lmaps/ay/aj;->s:F

    goto :goto_2

    :cond_3
    iget v0, p0, Lmaps/ay/aj;->w:F

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 5

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lmaps/ay/aj;->o()Lmaps/ac/av;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    iget v3, p0, Lmaps/ay/aj;->z:I

    int-to-float v3, v3

    iget v4, p0, Lmaps/ay/aj;->A:F

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lmaps/ay/aj;->B:Lmaps/ac/av;

    invoke-static {p1, v1, v2, v3, v4}, Lmaps/ay/o;->a(Lmaps/ar/a;Lmaps/ac/av;IILmaps/ac/av;)V

    :cond_0
    invoke-virtual {p1}, Lmaps/ar/a;->p()F

    move-result v1

    iget v2, p0, Lmaps/ay/aj;->c:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_1

    iget v2, p0, Lmaps/ay/aj;->d:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_2

    iget v0, p0, Lmaps/ay/aj;->e:F

    :cond_1
    :goto_0
    iput v0, p0, Lmaps/ay/aj;->A:F

    iget-object v0, p0, Lmaps/ay/aj;->l:Lmaps/v/k;

    const/4 v0, 0x1

    return v0

    :cond_2
    iget v2, p0, Lmaps/ay/aj;->e:F

    sub-float/2addr v0, v2

    iget v2, p0, Lmaps/ay/aj;->c:F

    iget v3, p0, Lmaps/ay/aj;->d:F

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    iget v2, p0, Lmaps/ay/aj;->e:F

    iget v3, p0, Lmaps/ay/aj;->d:F

    sub-float/2addr v1, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_0
.end method

.method public final c(Lmaps/as/a;)V
    .locals 1

    invoke-direct {p0}, Lmaps/ay/aj;->j()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/aj;->v:Z

    return-void
.end method

.method public final d()Lmaps/ar/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    iget-object v0, p0, Lmaps/ay/aj;->y:Lmaps/ay/v;

    return-object v0
.end method

.method public final l()V
    .locals 0

    return-void
.end method

.method public final m()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized o()Lmaps/ac/av;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/aj;->n:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->a()Lmaps/ac/av;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized p()Lmaps/ac/ad;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/ay/aj;->n()Lmaps/ac/au;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/au;->f()Lmaps/ac/ad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final q()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ay/aj;->B:Lmaps/ac/av;

    return-object v0
.end method
