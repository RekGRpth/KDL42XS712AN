.class public abstract Lmaps/ay/u;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/au/h;


# instance fields
.field private a:Lmaps/ay/w;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a(Ljavax/microedition/khronos/opengles/GL10;I)V
    .locals 5

    const v4, 0xff00

    shr-int/lit8 v0, p1, 0x10

    and-int/2addr v0, v4

    shr-int/lit8 v1, p1, 0x8

    and-int/2addr v1, v4

    and-int v2, p1, v4

    shl-int/lit8 v3, p1, 0x8

    and-int/2addr v3, v4

    invoke-interface {p0, v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method


# virtual methods
.method protected final a(Lmaps/aj/ae;)Lmaps/aj/ad;
    .locals 2

    new-instance v0, Lmaps/aj/ad;

    const/4 v1, 0x0

    new-array v1, v1, [Lmaps/aj/af;

    invoke-direct {v0, p0, p1, v1}, Lmaps/aj/ad;-><init>(Lmaps/ay/u;Lmaps/aj/ae;[Lmaps/aj/af;)V

    return-object v0
.end method

.method protected final a(Lmaps/aj/ae;Ljava/util/Collection;Ljava/util/Collection;)Lmaps/aj/ad;
    .locals 1

    new-instance v0, Lmaps/aj/ad;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/aj/ad;-><init>(Lmaps/ay/u;Lmaps/aj/ae;Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v0
.end method

.method protected final varargs a(Lmaps/aj/ae;[Lmaps/aj/af;)Lmaps/aj/ad;
    .locals 1

    new-instance v0, Lmaps/aj/ad;

    invoke-direct {v0, p0, p1, p2}, Lmaps/aj/ad;-><init>(Lmaps/ay/u;Lmaps/aj/ae;[Lmaps/aj/af;)V

    return-object v0
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/as/a;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 0

    return-void
.end method

.method public abstract a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
.end method

.method public final a(Lmaps/ay/w;)V
    .locals 0

    iput-object p1, p0, Lmaps/ay/u;->a:Lmaps/ay/w;

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(FFLmaps/ac/av;Lmaps/ar/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljava/util/List;)Z
    .locals 1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ay/u;->aq_()Lmaps/aj/ae;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ay/u;->a(Lmaps/aj/ae;)Lmaps/aj/ad;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(FFLmaps/ar/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public am_()Lmaps/ap/a;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public an_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected aq_()Lmaps/aj/ae;
    .locals 1

    sget-object v0, Lmaps/aj/ae;->i:Lmaps/aj/ae;

    return-object v0
.end method

.method public ar_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public at_()V
    .locals 0

    return-void
.end method

.method protected au_()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/u;->a:Lmaps/ay/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/u;->a:Lmaps/ay/w;

    invoke-interface {v0}, Lmaps/ay/w;->a()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/as/a;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/ay/u;->a(Lmaps/as/a;)V

    return-void
.end method

.method public b(FFLmaps/ac/av;Lmaps/ar/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(FFLmaps/ar/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(Lmaps/as/a;)V
    .locals 0

    return-void
.end method

.method public c(FFLmaps/ar/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d(FFLmaps/ar/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract f()Lmaps/ay/v;
.end method

.method public g()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i()V
    .locals 0

    return-void
.end method
