.class public final Lmaps/ay/o;
.super Lmaps/ay/u;


# instance fields
.field private final a:Lmaps/ay/v;

.field private final b:[F

.field private c:Lmaps/ay/b;

.field private final d:Lmaps/ac/av;

.field private e:Landroid/view/View;

.field private f:Lmaps/ay/e;

.field private g:Landroid/graphics/Bitmap;

.field private h:Lmaps/as/b;

.field private i:Lmaps/ay/p;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:F

.field private final o:I

.field private final p:Lmaps/v/e;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    sget-object v0, Lmaps/ay/v;->r:Lmaps/ay/v;

    invoke-direct {p0, v0, p1}, Lmaps/ay/o;-><init>(Lmaps/ay/v;Landroid/content/res/Resources;)V

    return-void
.end method

.method private constructor <init>(Lmaps/ay/v;Landroid/content/res/Resources;)V
    .locals 8

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/ay/o;->b:[F

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/ay/o;->d:Lmaps/ac/av;

    new-instance v0, Lmaps/v/e;

    const-wide/16 v1, 0xbb8

    const-wide/16 v3, 0x2710

    sget-object v5, Lmaps/v/g;->c:Lmaps/v/g;

    const/high16 v6, 0x10000

    const v7, 0x8000

    invoke-direct/range {v0 .. v7}, Lmaps/v/e;-><init>(JJLmaps/v/g;II)V

    iput-object v0, p0, Lmaps/ay/o;->p:Lmaps/v/e;

    iput-object p1, p0, Lmaps/ay/o;->a:Lmaps/ay/v;

    if-nez p2, :cond_0

    const v0, 0xffff00

    :goto_0
    iput v0, p0, Lmaps/ay/o;->o:I

    return-void

    :cond_0
    sget v0, Lmaps/b/c;->b:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/as/a;Lmaps/at/n;Lmaps/at/i;)V
    .locals 3

    invoke-virtual {p3, p2}, Lmaps/at/n;->d(Lmaps/as/a;)V

    invoke-virtual {p4, p2}, Lmaps/at/i;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/o;->h:Lmaps/as/b;

    invoke-virtual {v0, p1}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    return-void
.end method

.method public static a(Lmaps/ar/a;Lmaps/ac/av;IILmaps/ac/av;)V
    .locals 16

    move/from16 v0, p3

    int-to-float v1, v0

    invoke-virtual/range {p0 .. p0}, Lmaps/ar/a;->m()F

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lmaps/ar/a;->a(FF)F

    move-result v1

    move/from16 v0, p2

    int-to-float v2, v0

    invoke-virtual/range {p0 .. p0}, Lmaps/ar/a;->m()F

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lmaps/ar/a;->a(FF)F

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lmaps/ar/a;->o()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    invoke-virtual/range {p0 .. p0}, Lmaps/ar/a;->o()F

    move-result v5

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    invoke-virtual/range {p0 .. p0}, Lmaps/ar/a;->n()F

    move-result v7

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    invoke-virtual/range {p0 .. p0}, Lmaps/ar/a;->n()F

    move-result v9

    float-to-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/av;->f()I

    move-result v11

    float-to-double v12, v1

    mul-double/2addr v12, v3

    mul-double/2addr v12, v9

    float-to-double v14, v2

    mul-double/2addr v14, v7

    add-double/2addr v12, v14

    double-to-int v12, v12

    add-int/2addr v11, v12

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/av;->g()I

    move-result v12

    float-to-double v13, v1

    mul-double/2addr v3, v13

    mul-double/2addr v3, v7

    float-to-double v7, v2

    mul-double/2addr v7, v9

    sub-double v2, v3, v7

    double-to-int v2, v2

    add-int/2addr v2, v12

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/av;->h()I

    move-result v3

    float-to-double v7, v1

    mul-double v4, v7, v5

    double-to-int v1, v4

    add-int/2addr v1, v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v11, v2, v1}, Lmaps/ac/av;->a(III)V

    return-void
.end method

.method private e(FFLmaps/ar/a;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v2}, Lmaps/ay/b;->o()Lmaps/ac/av;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v2}, Lmaps/ay/b;->q()Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {p3, v2}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v2

    iget-object v3, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    aget v3, v2, v1

    iget v4, p0, Lmaps/ay/o;->l:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lmaps/ay/o;->l:I

    add-int/2addr v4, v3

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-ltz v3, :cond_3

    int-to-float v3, v4

    cmpl-float v3, p1, v3

    if-lez v3, :cond_4

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    aget v2, v2, v0

    iget v3, p0, Lmaps/ay/o;->m:I

    sub-int v3, v2, v3

    int-to-float v3, v3

    cmpg-float v3, p2, v3

    if-ltz v3, :cond_5

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-lez v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private j()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/ay/o;->h:Lmaps/as/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->h:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    iput-object v1, p0, Lmaps/ay/o;->h:Lmaps/as/b;

    :cond_0
    iget-object v0, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    invoke-virtual {v0}, Lmaps/ay/e;->b()V

    :cond_2
    return-void
.end method

.method private k()Landroid/graphics/Bitmap;
    .locals 6

    const/4 v5, -0x2

    const/high16 v2, -0x80000000

    const/4 v4, 0x0

    iget v0, p0, Lmaps/ay/o;->j:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v1, p0, Lmaps/ay/o;->k:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lmaps/ay/o;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/ay/o;->e:Landroid/view/View;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget-object v2, p0, Lmaps/ay/o;->e:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lmaps/ay/o;->l:I

    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lmaps/ay/o;->m:I

    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    iget v1, p0, Lmaps/ay/o;->l:I

    iget v2, p0, Lmaps/ay/o;->m:I

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/view/View;->layout(IIII)V

    iget v0, p0, Lmaps/ay/o;->l:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lmaps/as/b;->c(II)I

    move-result v0

    iget v1, p0, Lmaps/ay/o;->m:I

    const/16 v2, 0x20

    invoke-static {v1, v2}, Lmaps/as/b;->c(II)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v4, p0, Lmaps/ay/o;->l:I

    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    iget v4, p0, Lmaps/ay/o;->m:I

    sub-int/2addr v1, v4

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    return-object v2
.end method


# virtual methods
.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/ay/o;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 9

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-nez v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    :cond_0
    monitor-exit p0

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v0}, Lmaps/ay/b;->q()Lmaps/ac/av;

    move-result-object v0

    iget-object v3, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v3}, Lmaps/ay/b;->p()Lmaps/ac/ad;

    move-result-object v3

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    move-result-object v4

    invoke-virtual {v3}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v3

    invoke-virtual {v4, v3}, Lmaps/ab/q;->e(Lmaps/ac/r;)Lmaps/ab/k;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v4, p0, Lmaps/ay/o;->d:Lmaps/ac/av;

    invoke-virtual {v4, v0}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    iget-object v4, p0, Lmaps/ay/o;->d:Lmaps/ac/av;

    invoke-virtual {v3, p2, v0}, Lmaps/ab/k;->a(Lmaps/ar/a;Lmaps/ac/av;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v4, v0}, Lmaps/ac/av;->b(I)V

    iget-object v0, p0, Lmaps/ay/o;->d:Lmaps/ac/av;

    :cond_3
    invoke-virtual {p2}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cy;->a()Lmaps/ac/cx;

    move-result-object v4

    if-nez v0, :cond_4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_1
    invoke-virtual {v4, v0}, Lmaps/ac/cx;->a(Lmaps/ac/av;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3, v0}, Lmaps/ac/cy;->a(Lmaps/ac/av;)Z

    move-result v3

    if-nez v3, :cond_7

    :cond_5
    iget-object v3, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v3, p2}, Lmaps/ay/b;->b(Lmaps/ar/a;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v3}, Lmaps/ay/b;->q()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {p2, v3}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v3

    iget-object v4, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iget-object v5, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/4 v6, 0x0

    aget v6, v3, v6

    sub-int/2addr v6, v4

    const/4 v7, 0x0

    aget v7, v3, v7

    add-int/2addr v4, v7

    const/4 v7, 0x1

    aget v7, v3, v7

    sub-int/2addr v7, v5

    const/4 v8, 0x1

    aget v3, v3, v8

    add-int/2addr v3, v5

    invoke-virtual {p2}, Lmaps/ar/a;->i()I

    move-result v5

    invoke-virtual {p2}, Lmaps/ar/a;->j()I

    move-result v8

    if-ltz v4, :cond_6

    if-ge v6, v5, :cond_6

    if-ltz v3, :cond_6

    if-ge v7, v8, :cond_6

    :goto_1
    if-nez v1, :cond_7

    monitor-exit p0

    goto/16 :goto_0

    :cond_6
    move v1, v2

    goto :goto_1

    :cond_7
    invoke-virtual {p2}, Lmaps/ar/a;->h()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lmaps/ay/o;->b:[F

    invoke-virtual {p2, v0, v1}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    iget-object v0, p0, Lmaps/ay/o;->b:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/ay/o;->b:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v0

    :cond_8
    if-nez v0, :cond_9

    const-string v0, "UI"

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Null point for ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/ay/o;->b:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/ay/o;->b:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "); camera="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    monitor-exit p0

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/as/a;->r()V

    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x1e01

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v2, p0, Lmaps/ay/o;->h:Lmaps/as/b;

    if-nez v2, :cond_a

    new-instance v2, Lmaps/as/b;

    invoke-direct {v2, p1}, Lmaps/as/b;-><init>(Lmaps/as/a;)V

    iput-object v2, p0, Lmaps/ay/o;->h:Lmaps/as/b;

    iget-object v2, p0, Lmaps/ay/o;->h:Lmaps/as/b;

    iget-object v3, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lmaps/as/b;->b(Landroid/graphics/Bitmap;)V

    :cond_a
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v2, p0, Lmaps/ay/o;->n:F

    invoke-static {p1, p2, v0, v2}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    invoke-static {v1, p2}, Lmaps/ap/o;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/ar/a;)V

    invoke-virtual {p2}, Lmaps/ar/a;->r()Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {p2}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lmaps/ac/av;->c(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)F

    move-result v0

    iget-object v2, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iget-object v3, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    neg-float v3, v2

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    invoke-virtual {v0}, Lmaps/ay/e;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v0, p0, Lmaps/ay/o;->o:I

    invoke-static {v1, v0}, Lmaps/ay/o;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget-object v0, p1, Lmaps/as/a;->e:Lmaps/at/n;

    iget-object v2, p1, Lmaps/as/a;->b:Lmaps/at/i;

    invoke-direct {p0, v1, p1, v0, v2}, Lmaps/ay/o;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/as/a;Lmaps/at/n;Lmaps/at/i;)V

    :goto_2
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    goto/16 :goto_0

    :cond_b
    :try_start_2
    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->i()Z

    iget-object v0, p1, Lmaps/as/a;->e:Lmaps/at/n;

    iget-object v2, p1, Lmaps/as/a;->b:Lmaps/at/i;

    invoke-direct {p0, v1, p1, v0, v2}, Lmaps/ay/o;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/as/a;Lmaps/at/n;Lmaps/at/i;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final declared-synchronized a(Lmaps/ay/b;Lmaps/ay/e;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput-object p2, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    iget-object v0, p0, Lmaps/ay/o;->p:Lmaps/v/e;

    invoke-virtual {v0}, Lmaps/v/e;->a()V

    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    invoke-virtual {p2}, Lmaps/ay/e;->d()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v0}, Lmaps/ay/b;->m()V

    :cond_0
    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/ay/o;->j()V

    invoke-direct {p0}, Lmaps/ay/o;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v0}, Lmaps/ay/b;->l()V

    :cond_3
    iput-object p1, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v0}, Lmaps/ay/b;->m()V

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lmaps/ay/e;->d()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    :cond_4
    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/ay/o;->j()V

    invoke-direct {p0}, Lmaps/ay/o;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmaps/ay/o;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/ay/p;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/ay/o;->i:Lmaps/ay/p;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(FFLmaps/ac/av;Lmaps/ar/a;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p4}, Lmaps/ay/o;->e(FFLmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    invoke-virtual {p0}, Lmaps/ay/o;->au_()Z

    const/4 v0, 0x1

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/ay/o;->d()V

    invoke-virtual {p0}, Lmaps/ay/o;->au_()Z

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a_(FFLmaps/ar/a;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lmaps/ay/o;->e(FFLmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->p:Lmaps/v/e;

    invoke-virtual {v0}, Lmaps/v/e;->a()V

    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    invoke-virtual {v0}, Lmaps/ay/e;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized at_()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->f:Lmaps/ay/e;

    invoke-virtual {v0}, Lmaps/ay/e;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final au_()Z
    .locals 2

    invoke-super {p0}, Lmaps/ay/u;->au_()Z

    move-result v0

    iget-object v1, p0, Lmaps/ay/o;->i:Lmaps/ay/p;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->i:Lmaps/ay/p;

    iget-object v1, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v0, v1}, Lmaps/ay/p;->b(Lmaps/ay/b;)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final declared-synchronized b()Lmaps/ay/b;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(FFLmaps/ar/a;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lmaps/ay/o;->e(FFLmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/o;->i:Lmaps/ay/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->i:Lmaps/ay/p;

    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    :cond_0
    const/4 v0, 0x1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_1
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 2

    monitor-enter p0

    const/high16 v0, 0x3f800000    # 1.0f

    :try_start_0
    invoke-virtual {p1}, Lmaps/ar/a;->m()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmaps/ar/a;->a(FF)F

    move-result v0

    iput v0, p0, Lmaps/ay/o;->n:F

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v0

    iget v1, p0, Lmaps/ay/o;->j:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v0

    iget v1, p0, Lmaps/ay/o;->k:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v0

    iput v0, p0, Lmaps/ay/o;->j:I

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v0

    iput v0, p0, Lmaps/ay/o;->k:I

    iget-object v0, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/ay/o;->j()V

    invoke-direct {p0}, Lmaps/ay/o;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/o;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lmaps/as/a;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/ay/o;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(FFLmaps/ar/a;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lmaps/ay/o;->e(FFLmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ay/o;->at_()V

    const/4 v0, 0x1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_0
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;

    invoke-interface {v0}, Lmaps/ay/b;->l()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/o;->c:Lmaps/ay/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    iget-object v0, p0, Lmaps/ay/o;->a:Lmaps/ay/v;

    return-object v0
.end method
