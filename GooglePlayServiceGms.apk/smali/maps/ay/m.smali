.class public final Lmaps/ay/m;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/List;

.field private b:I

.field private c:[F

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ay/m;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/m;->d:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/m;->d:Z

    return-void
.end method

.method public final a(FFLmaps/ar/a;Ljava/util/List;)Z
    .locals 10

    const/high16 v4, 0x41f00000    # 30.0f

    const/4 v9, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    invoke-virtual {p3}, Lmaps/ar/a;->k()F

    move-result v0

    mul-float/2addr v0, v4

    float-to-int v0, v0

    mul-int/2addr v0, v0

    iget-boolean v1, p0, Lmaps/ay/m;->d:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/ay/m;->c:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ay/m;->c:[F

    aget v1, v1, v6

    iget-object v2, p0, Lmaps/ay/m;->c:[F

    aget v2, v2, v9

    sub-float v3, v1, p1

    sub-float/2addr v1, p1

    mul-float/2addr v1, v3

    sub-float v3, v2, p2

    sub-float/2addr v2, p2

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    if-le v1, v0, :cond_2

    :cond_0
    iput-boolean v6, p0, Lmaps/ay/m;->d:Z

    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/ay/m;->c:[F

    if-nez v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/ay/m;->c:[F

    :cond_1
    iget-object v0, p0, Lmaps/ay/m;->c:[F

    aput p1, v0, v6

    iget-object v0, p0, Lmaps/ay/m;->c:[F

    aput p2, v0, v9

    invoke-virtual {p3}, Lmaps/ar/a;->k()F

    move-result v0

    mul-float/2addr v0, v4

    float-to-int v0, v0

    mul-int v5, v0, v0

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/c;

    iget-object v1, p0, Lmaps/ay/m;->a:Ljava/util/List;

    move v2, p1

    move v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lmaps/ay/c;->a(Ljava/util/List;FFLmaps/ar/a;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/n;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/n;->a(FFLmaps/ar/a;)V

    goto :goto_1

    :cond_3
    iget v0, p0, Lmaps/ay/m;->b:I

    if-eq v0, v7, :cond_4

    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    iget v1, p0, Lmaps/ay/m;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/n;

    invoke-virtual {v0, v9}, Lmaps/ay/n;->a(Z)V

    :cond_4
    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_7

    move v3, v7

    :cond_5
    :goto_2
    iput v3, p0, Lmaps/ay/m;->b:I

    iget v0, p0, Lmaps/ay/m;->b:I

    if-eq v0, v7, :cond_6

    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    iget v1, p0, Lmaps/ay/m;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/n;

    invoke-virtual {v0, v9}, Lmaps/ay/n;->a(Z)V

    invoke-virtual {v0}, Lmaps/ay/n;->c()Z

    move-result v6

    :cond_6
    return v6

    :cond_7
    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v9, :cond_8

    move v3, v6

    goto :goto_2

    :cond_8
    const v0, 0x7fffffff

    move v1, v6

    move v2, v0

    move v3, v7

    :goto_3
    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/n;

    invoke-virtual {v0}, Lmaps/ay/n;->b()Z

    move-result v4

    if-nez v4, :cond_9

    invoke-virtual {v0}, Lmaps/ay/n;->a()I

    move-result v4

    if-ge v4, v2, :cond_9

    invoke-virtual {v0}, Lmaps/ay/n;->a()I

    move-result v2

    move v3, v1

    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_a
    if-ne v3, v7, :cond_5

    iget-object v0, p0, Lmaps/ay/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/n;

    invoke-virtual {v0, v6}, Lmaps/ay/n;->a(Z)V

    goto :goto_4
.end method
