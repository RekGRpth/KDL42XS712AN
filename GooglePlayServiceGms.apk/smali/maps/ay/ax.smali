.class final Lmaps/ay/ax;
.super Lmaps/ak/a;


# instance fields
.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:I

.field private final f:J

.field private final g:Lmaps/ak/b;

.field private h:Lmaps/ak/d;


# direct methods
.method protected constructor <init>(Lmaps/ar/b;Lmaps/ak/b;FFFI)V
    .locals 2

    invoke-direct {p0, p1}, Lmaps/ak/a;-><init>(Lmaps/ar/b;)V

    iput-object p2, p0, Lmaps/ay/ax;->g:Lmaps/ak/b;

    iput p3, p0, Lmaps/ay/ax;->b:F

    iput p4, p0, Lmaps/ay/ax;->c:F

    iput p5, p0, Lmaps/ay/ax;->d:F

    iput p6, p0, Lmaps/ay/ax;->e:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/ay/ax;->f:J

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ar/a;)Lmaps/ar/c;
    .locals 6

    iget-object v0, p0, Lmaps/ay/ax;->a:Lmaps/ar/b;

    iget-object v2, p0, Lmaps/ay/ax;->g:Lmaps/ak/b;

    iget v3, p0, Lmaps/ay/ax;->b:F

    iget v4, p0, Lmaps/ay/ax;->c:F

    iget v5, p0, Lmaps/ay/ax;->d:F

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lmaps/ay/au;->a(Lmaps/ar/b;Lmaps/ar/a;Lmaps/ak/b;FFF)Lmaps/ar/b;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v3, p0, Lmaps/ay/ax;->f:J

    sub-long/2addr v0, v3

    long-to-int v3, v0

    new-instance v0, Lmaps/ay/av;

    iget-object v1, p0, Lmaps/ay/ax;->a:Lmaps/ar/b;

    iget v4, p0, Lmaps/ay/ax;->e:I

    sub-int v3, v4, v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/ay/av;-><init>(Lmaps/ar/b;Lmaps/ar/c;IZF)V

    iput-object v0, p0, Lmaps/ay/ax;->h:Lmaps/ak/d;

    iget-object v0, p0, Lmaps/ay/ax;->h:Lmaps/ak/d;

    invoke-interface {v0, p1}, Lmaps/ak/d;->a(Lmaps/ar/a;)Lmaps/ar/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lmaps/ay/ax;->h:Lmaps/ak/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/ax;->h:Lmaps/ak/d;

    invoke-interface {v0}, Lmaps/ak/d;->b()I

    move-result v0

    goto :goto_0
.end method
