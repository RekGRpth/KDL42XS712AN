.class public final Lmaps/ay/au;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ap/a;


# static fields
.field private static final a:F

.field private static final b:Lmaps/ar/b;

.field private static c:F


# instance fields
.field private final d:Z

.field private final e:Lmaps/ak/b;

.field private volatile f:Lmaps/ar/b;

.field private volatile g:Lmaps/ar/b;

.field private volatile h:F

.field private volatile i:Lmaps/ar/c;

.field private volatile j:Z

.field private k:Z

.field private l:Lmaps/u/a;

.field private m:Lmaps/ap/n;

.field private n:Lmaps/ap/t;

.field private o:Lmaps/ap/d;

.field private p:Lmaps/ay/az;

.field private q:Z

.field private r:I

.field private s:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lmaps/ay/au;->a:F

    const/4 v0, 0x0

    sput-object v0, Lmaps/ay/au;->b:Lmaps/ar/b;

    const/high16 v0, 0x41a80000    # 21.0f

    sput v0, Lmaps/ay/au;->c:F

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    iput v0, p0, Lmaps/ay/au;->r:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmaps/ay/au;->s:F

    sget-object v0, Lmaps/ar/a;->b:Lmaps/ar/b;

    iput-object v0, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    sget-object v0, Lmaps/ar/a;->b:Lmaps/ar/b;

    iput-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    :cond_0
    new-instance v0, Lmaps/ak/b;

    invoke-direct {v0}, Lmaps/ak/b;-><init>()V

    iput-object v0, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->b()Z

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/au;->d:Z

    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    return-void
.end method

.method public static a(Lmaps/ar/a;Lmaps/ak/b;Lmaps/ac/av;F)Lmaps/ar/b;
    .locals 6

    invoke-virtual {p0}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v1

    invoke-virtual {p2}, Lmaps/ac/av;->f()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Lmaps/ac/av;->g()I

    move-result v0

    invoke-virtual {p2}, Lmaps/ac/av;->g()I

    move-result v2

    sub-int/2addr v0, v2

    float-to-double v2, p3

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L    # 180.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    neg-float v3, v2

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    neg-float v2, v2

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    int-to-float v4, v1

    mul-float/2addr v4, v2

    int-to-float v5, v0

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    int-to-float v1, v1

    mul-float/2addr v1, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    new-instance v1, Lmaps/ac/av;

    invoke-virtual {p2}, Lmaps/ac/av;->f()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v4

    float-to-int v2, v2

    invoke-virtual {p2}, Lmaps/ac/av;->g()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v0, v0

    invoke-direct {v1, v2, v0}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {p0}, Lmaps/ar/a;->n()F

    move-result v0

    add-float/2addr v0, p3

    invoke-static {v0}, Lmaps/ay/au;->c(F)F

    move-result v4

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {p0}, Lmaps/ar/a;->p()F

    move-result v2

    invoke-virtual {p0}, Lmaps/ar/a;->o()F

    move-result v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    invoke-virtual {p1, v0}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/ar/b;Lmaps/ar/a;FF)Lmaps/ar/b;
    .locals 6

    invoke-virtual {p1}, Lmaps/ar/a;->v()F

    move-result v0

    mul-float/2addr v0, p2

    neg-float v1, p3

    invoke-virtual {p1}, Lmaps/ar/a;->v()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Lmaps/ar/a;->o()F

    move-result v2

    const v3, 0x3c8efa35

    mul-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, Lmaps/ar/a;->s()Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {p1}, Lmaps/ar/a;->t()Lmaps/ac/av;

    move-result-object v3

    new-instance v4, Lmaps/ac/av;

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v5

    invoke-virtual {v2}, Lmaps/ac/av;->g()I

    move-result v2

    invoke-direct {v4, v5, v2}, Lmaps/ac/av;-><init>(II)V

    new-instance v5, Lmaps/ac/av;

    invoke-virtual {v3}, Lmaps/ac/av;->f()I

    move-result v2

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v3

    invoke-direct {v5, v2, v3}, Lmaps/ac/av;-><init>(II)V

    invoke-static {v4, v0, v4}, Lmaps/ac/av;->b(Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-static {v5, v1, v5}, Lmaps/ac/av;->b(Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-virtual {p1}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ar/b;->b()F

    move-result v2

    invoke-virtual {v0}, Lmaps/ac/av;->h()I

    move-result v3

    invoke-virtual {v0, v4}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v1

    invoke-static {v1, v5, v1}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-virtual {v1, v3}, Lmaps/ac/av;->b(I)V

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {p0}, Lmaps/ar/b;->d()F

    move-result v3

    invoke-virtual {p0}, Lmaps/ar/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    return-object v0
.end method

.method public static a(Lmaps/ar/b;Lmaps/ar/a;Lmaps/ak/b;FFF)Lmaps/ar/b;
    .locals 8

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v6, p4, v0

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v7, p5, v0

    invoke-static {p0, p1, v6, v7}, Lmaps/ay/au;->a(Lmaps/ar/b;Lmaps/ar/a;FF)Lmaps/ar/b;

    move-result-object v0

    invoke-virtual {p2, v0}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v4

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {v4}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    sget v2, Lmaps/ay/au;->c:F

    invoke-virtual {v4}, Lmaps/ar/b;->b()F

    move-result v3

    add-float/2addr v3, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v4}, Lmaps/ar/b;->d()F

    move-result v3

    invoke-virtual {v4}, Lmaps/ar/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    invoke-virtual {p1, v0}, Lmaps/ar/a;->a(Lmaps/ar/b;)V

    neg-float v1, v6

    neg-float v2, v7

    invoke-static {v0, p1, v1, v2}, Lmaps/ay/au;->a(Lmaps/ar/b;Lmaps/ar/a;FF)Lmaps/ar/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/ar/c;I)V
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lmaps/ay/au;->a(Lmaps/ar/c;II)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(Lmaps/ar/c;Lmaps/ar/b;)V
    .locals 3

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/au;->p:Lmaps/ay/az;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/au;->p:Lmaps/ay/az;

    invoke-interface {v0}, Lmaps/ay/az;->a()V

    :cond_0
    instance-of v0, p1, Lmaps/ak/d;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/au;->j:Z

    :cond_1
    iput-object p1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/au;->k:Z

    iget-object v0, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    iget-object v1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    invoke-interface {v1}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    if-eqz p2, :cond_2

    iget-object v0, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-virtual {v0, p2}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object p2

    :cond_2
    iput-object p2, p0, Lmaps/ay/au;->g:Lmaps/ar/b;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ay/au;->m:Lmaps/ap/n;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ay/au;->m:Lmaps/ap/n;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/ap/n;->a(ZZ)V

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(F)F
    .locals 1

    invoke-static {p0}, Lmaps/ay/au;->c(F)F

    move-result v0

    return v0
.end method

.method private static c(F)F
    .locals 7

    const-wide v5, 0x4076800000000000L    # 360.0

    move v0, p0

    :goto_0
    float-to-double v1, v0

    cmpl-double v1, v1, v5

    if-ltz v1, :cond_0

    float-to-double v0, v0

    sub-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_0

    :cond_0
    :goto_1
    float-to-double v1, v0

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    float-to-double v0, v0

    add-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_1

    :cond_1
    return v0
.end method

.method static synthetic f()F
    .locals 1

    sget v0, Lmaps/ay/au;->c:F

    return v0
.end method


# virtual methods
.method public final a()F
    .locals 1

    iget-object v0, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-virtual {v0}, Lmaps/ak/b;->a()Lmaps/ak/c;

    move-result-object v0

    if-nez v0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lmaps/ak/c;->a()F

    move-result v0

    goto :goto_0
.end method

.method public final declared-synchronized a(FFF)F
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    iget-object v7, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {v6}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v6}, Lmaps/ar/b;->b()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {v6}, Lmaps/ar/b;->d()F

    move-result v3

    invoke-virtual {v6}, Lmaps/ar/b;->e()F

    move-result v4

    invoke-virtual {v6}, Lmaps/ar/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    invoke-virtual {v7, v0}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v0

    invoke-virtual {v6}, Lmaps/ar/b;->b()F

    move-result v1

    invoke-virtual {v0}, Lmaps/ar/b;->b()F

    move-result v0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lmaps/ar/b;->b()F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    instance-of v0, v0, Lmaps/ay/ay;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    check-cast v0, Lmaps/ay/ay;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lmaps/ay/ay;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    :goto_1
    iput v0, p0, Lmaps/ay/au;->h:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    new-instance v0, Lmaps/ay/ay;

    iget-object v1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    invoke-interface {v1}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v1

    iget-object v2, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-direct {v0, v1, v2}, Lmaps/ay/ay;-><init>(Lmaps/ar/b;Lmaps/ak/b;)V

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lmaps/ay/ay;->a(FFFFFF)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_1
.end method

.method public final a(FFFI)F
    .locals 7

    iget-object v1, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    invoke-virtual {v1}, Lmaps/ar/b;->b()F

    move-result v0

    add-float/2addr v0, p1

    iput v0, p0, Lmaps/ay/au;->h:F

    new-instance v0, Lmaps/ay/ax;

    iget-object v2, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lmaps/ay/ax;-><init>(Lmaps/ar/b;Lmaps/ak/b;FFFI)V

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    invoke-virtual {v1}, Lmaps/ar/b;->b()F

    move-result v0

    add-float/2addr v0, p1

    sget v1, Lmaps/ay/au;->c:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public final a(FI)F
    .locals 7

    iget-object v5, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    iget-object v6, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {v5}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v5}, Lmaps/ar/b;->b()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {v5}, Lmaps/ar/b;->d()F

    move-result v3

    invoke-virtual {v5}, Lmaps/ar/b;->e()F

    move-result v4

    invoke-virtual {v5}, Lmaps/ar/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    invoke-virtual {v6, v0}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lmaps/ay/au;->a(Lmaps/ar/c;I)V

    invoke-virtual {v0}, Lmaps/ar/b;->b()F

    move-result v0

    return v0
.end method

.method public final a(Lmaps/ac/av;)F
    .locals 1

    iget-object v0, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-virtual {v0}, Lmaps/ak/b;->a()Lmaps/ak/c;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lmaps/ay/au;->c:F

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0, p1}, Lmaps/ak/c;->a(Lmaps/ac/av;)F

    move-result v0

    goto :goto_0
.end method

.method public final a(Lmaps/ar/a;)I
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    instance-of v1, v1, Lmaps/ak/d;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    check-cast v0, Lmaps/ak/d;

    invoke-interface {v0}, Lmaps/ak/d;->b()I

    move-result v1

    invoke-interface {v0, p1}, Lmaps/ak/d;->a(Lmaps/ar/a;)Lmaps/ar/c;

    move-result-object v2

    iput-object v2, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    iget-object v2, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    iget-object v3, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    invoke-interface {v3}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v2

    iput-object v2, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    invoke-interface {v0}, Lmaps/ak/d;->b()I

    move-result v0

    or-int/2addr v0, v1

    :goto_0
    iget-object v1, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    invoke-virtual {p1, v1}, Lmaps/ar/a;->a(Lmaps/ar/b;)V

    iget-boolean v1, p0, Lmaps/ay/au;->k:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    iget-object v1, p0, Lmaps/ay/au;->o:Lmaps/ap/d;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/ay/au;->l:Lmaps/u/a;

    if-nez v1, :cond_0

    new-instance v1, Lmaps/ay/bb;

    new-instance v2, Lmaps/ao/a;

    invoke-direct {v2}, Lmaps/ao/a;-><init>()V

    invoke-direct {v1, v2}, Lmaps/ay/bb;-><init>(Lmaps/ao/a;)V

    iput-object v1, p0, Lmaps/ay/au;->l:Lmaps/u/a;

    :cond_0
    iget-object v1, p0, Lmaps/ay/au;->l:Lmaps/u/a;

    invoke-interface {v1, p1}, Lmaps/u/a;->b(Lmaps/ar/a;)Ljava/util/List;

    iget-object v1, p0, Lmaps/ay/au;->o:Lmaps/ap/d;

    iget-object v2, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    invoke-interface {v1, v2}, Lmaps/ap/d;->a(Lmaps/ar/b;)V

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ay/au;->k:Z

    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_3

    iget-object v1, p0, Lmaps/ay/au;->n:Lmaps/ap/t;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/ay/au;->n:Lmaps/ap/t;

    invoke-virtual {v1, p1}, Lmaps/ap/t;->a(Lmaps/ar/a;)V

    :cond_3
    return v0

    :cond_4
    :try_start_1
    iget-object v1, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    iget-object v2, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    invoke-interface {v2}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v1

    iput-object v1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    iget-object v1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    invoke-interface {v1}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v1

    iput-object v1, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ay/au;->j:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(F)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/ay/au;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    instance-of v0, v0, Lmaps/ay/ba;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    check-cast v0, Lmaps/ay/ba;

    invoke-virtual {v0, p1}, Lmaps/ay/ba;->a(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lmaps/ay/ba;

    iget-object v1, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    iget-object v2, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-direct {v0, v1, v2}, Lmaps/ay/ba;-><init>(Lmaps/ar/b;Lmaps/ak/b;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    invoke-virtual {v0, p1}, Lmaps/ay/ba;->a(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v5, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    invoke-virtual {v5}, Lmaps/ar/b;->d()F

    move-result v0

    add-float v3, v0, p1

    iget-object v6, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {v5}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v5}, Lmaps/ar/b;->b()F

    move-result v2

    invoke-virtual {v5}, Lmaps/ar/b;->e()F

    move-result v4

    invoke-virtual {v5}, Lmaps/ar/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    invoke-virtual {v6, v0}, Lmaps/ak/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/ay/au;->a(Lmaps/ar/c;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(FF)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    instance-of v0, v0, Lmaps/ay/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    check-cast v0, Lmaps/ay/ay;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/ay/ay;->a(FFFFFF)[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lmaps/ay/ay;

    iget-object v1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    invoke-interface {v1}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v1

    iget-object v2, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-direct {v0, v1, v2}, Lmaps/ay/ay;-><init>(Lmaps/ar/b;Lmaps/ak/b;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/ay/ay;->a(FFFFFF)[F

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/ak/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-virtual {v0, p1}, Lmaps/ak/b;->a(Lmaps/ak/c;)V

    return-void
.end method

.method public final declared-synchronized a(Lmaps/ap/d;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/ay/au;->o:Lmaps/ap/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/ap/n;)V
    .locals 1

    iput-object p1, p0, Lmaps/ay/au;->m:Lmaps/ap/n;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/au;->q:Z

    return-void
.end method

.method public final a(Lmaps/ap/t;)V
    .locals 0

    iput-object p1, p0, Lmaps/ay/au;->n:Lmaps/ap/t;

    return-void
.end method

.method public final a(Lmaps/ar/c;II)V
    .locals 14

    iget-boolean v1, p0, Lmaps/ay/au;->q:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lmaps/ay/au;->q:Z

    if-eqz v1, :cond_1

    const/16 p2, 0x0

    const/16 p3, 0x0

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ay/au;->q:Z

    iget-object v2, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    invoke-interface {p1}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v1

    invoke-virtual {v1, v2}, Lmaps/ar/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/ar/b;->b()F

    move-result v1

    iput v1, p0, Lmaps/ay/au;->h:F

    invoke-static {}, Lmaps/ax/m;->a()Lmaps/ax/k;

    move-result-object v1

    invoke-virtual {v7}, Lmaps/ar/b;->b()F

    move-result v3

    invoke-virtual {v2}, Lmaps/ar/b;->b()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v1}, Lmaps/ax/k;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lmaps/ax/k;->c()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v1, v3, v1

    if-lez v1, :cond_3

    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v7}, Lmaps/ar/b;->b()F

    move-result v1

    invoke-virtual {v2}, Lmaps/ar/b;->b()F

    move-result v4

    add-float/2addr v1, v4

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    const/16 v4, 0x1e

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/high16 v4, 0x40000000    # 2.0f

    shr-int/2addr v4, v1

    invoke-virtual {v7}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v2}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v5

    invoke-virtual {v1, v5}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v5

    int-to-float v1, v4

    div-float v6, v5, v1

    iget v1, p0, Lmaps/ay/au;->r:I

    int-to-float v1, v1

    cmpg-float v1, v6, v1

    if-gtz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_6

    if-nez p2, :cond_5

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const/4 v1, -0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_9

    const/high16 v1, 0x41000000    # 8.0f

    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/high16 v3, 0x41000000    # 8.0f

    div-float/2addr v1, v3

    const/high16 v3, 0x3f400000    # 0.75f

    mul-float/2addr v1, v3

    const/high16 v3, 0x3fc00000    # 1.5f

    add-float/2addr v1, v3

    const/high16 v3, 0x442f0000    # 700.0f

    mul-float/2addr v1, v3

    float-to-int v4, v1

    :goto_2
    new-instance v1, Lmaps/ay/av;

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lmaps/ay/av;-><init>(Lmaps/ar/b;Lmaps/ar/c;IZF)V

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    goto/16 :goto_0

    :cond_6
    if-nez p3, :cond_7

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    goto/16 :goto_0

    :cond_7
    const/4 v1, -0x1

    move/from16 v0, p3

    if-ne v0, v1, :cond_8

    iget v1, p0, Lmaps/ay/au;->r:I

    int-to-float v1, v1

    sub-float v1, v6, v1

    const/high16 v3, 0x4e800000

    int-to-float v4, v4

    div-float/2addr v3, v4

    iget v4, p0, Lmaps/ay/au;->r:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    div-float/2addr v1, v3

    const v3, 0x40833333    # 4.1f

    mul-float/2addr v1, v3

    const v3, 0x3fb33333    # 1.4f

    add-float/2addr v1, v3

    const/high16 v3, 0x442f0000    # 700.0f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    const/16 v3, 0x9c4

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    iget v3, p0, Lmaps/ay/au;->s:F

    mul-float/2addr v1, v3

    float-to-int v4, v1

    :goto_3
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const-wide v10, 0x40031eb851eb851fL    # 2.39

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->log(D)D

    move-result-wide v5

    sget v1, Lmaps/ay/au;->a:F

    float-to-double v12, v1

    mul-double/2addr v5, v12

    mul-double/2addr v5, v10

    const-wide v10, 0x404d5ae147ae147bL    # 58.71

    sub-double/2addr v5, v10

    invoke-static {v8, v9, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    double-to-float v6, v5

    new-instance v1, Lmaps/ay/av;

    const/4 v5, 0x0

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lmaps/ay/av;-><init>(Lmaps/ar/b;Lmaps/ar/c;IZF)V

    invoke-direct {p0, v1, v7}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    goto/16 :goto_0

    :cond_8
    move/from16 v4, p3

    goto :goto_3

    :cond_9
    move/from16 v4, p2

    goto :goto_2
.end method

.method public final declared-synchronized a(Lmaps/ay/az;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/ay/au;->p:Lmaps/ay/az;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(FFF)F
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    instance-of v0, v0, Lmaps/ay/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    check-cast v0, Lmaps/ay/ay;

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/ay/ay;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    new-instance v0, Lmaps/ay/ay;

    iget-object v1, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    iget-object v2, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    invoke-direct {v0, v1, v2}, Lmaps/ay/ay;-><init>(Lmaps/ar/b;Lmaps/ak/b;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/ay/ay;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lmaps/ay/au;->e:Lmaps/ak/b;

    const/high16 v1, 0x42870000    # 67.5f

    invoke-virtual {v0, v1}, Lmaps/ak/b;->a(F)V

    return-void
.end method

.method public final declared-synchronized b(FF)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    instance-of v0, v0, Lmaps/ay/aw;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ay/aw;

    iget-object v1, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    invoke-interface {v1}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/ay/aw;-><init>(Lmaps/ar/b;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/ay/au;->a(Lmaps/ar/c;Lmaps/ar/b;)V

    :cond_0
    iget-object v0, p0, Lmaps/ay/au;->i:Lmaps/ar/c;

    check-cast v0, Lmaps/ay/aw;

    invoke-virtual {v0, p1, p2}, Lmaps/ay/aw;->a(FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lmaps/ar/b;
    .locals 1

    iget-object v0, p0, Lmaps/ay/au;->f:Lmaps/ar/b;

    return-object v0
.end method

.method public final d()Lmaps/ar/b;
    .locals 1

    iget-object v0, p0, Lmaps/ay/au;->g:Lmaps/ar/b;

    return-object v0
.end method

.method public final e()F
    .locals 1

    iget v0, p0, Lmaps/ay/au;->h:F

    return v0
.end method
