.class public final Lmaps/ay/ac;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private g:[I

.field private h:Z


# direct methods
.method constructor <init>(IIIIII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/ac;->g:[I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/ac;->h:Z

    iput p1, p0, Lmaps/ay/ac;->a:I

    iput p2, p0, Lmaps/ay/ac;->b:I

    iput p3, p0, Lmaps/ay/ac;->c:I

    iput p4, p0, Lmaps/ay/ac;->d:I

    iput p5, p0, Lmaps/ay/ac;->e:I

    iput p6, p0, Lmaps/ay/ac;->f:I

    return-void
.end method

.method constructor <init>(Lmaps/ay/ac;)V
    .locals 7

    iget v1, p1, Lmaps/ay/ac;->a:I

    iget v2, p1, Lmaps/ay/ac;->b:I

    iget v3, p1, Lmaps/ay/ac;->c:I

    iget v4, p1, Lmaps/ay/ac;->d:I

    iget v5, p1, Lmaps/ay/ac;->e:I

    iget v6, p1, Lmaps/ay/ac;->f:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/ay/ac;-><init>(IIIIII)V

    return-void
.end method

.method static synthetic a(Lmaps/ay/ac;)I
    .locals 1

    iget v0, p0, Lmaps/ay/ac;->e:I

    return v0
.end method

.method static synthetic b(Lmaps/ay/ac;)I
    .locals 1

    iget v0, p0, Lmaps/ay/ac;->f:I

    return v0
.end method

.method static synthetic c(Lmaps/ay/ac;)I
    .locals 1

    iget v0, p0, Lmaps/ay/ac;->a:I

    return v0
.end method

.method static synthetic d(Lmaps/ay/ac;)I
    .locals 1

    iget v0, p0, Lmaps/ay/ac;->b:I

    return v0
.end method

.method static synthetic e(Lmaps/ay/ac;)I
    .locals 1

    iget v0, p0, Lmaps/ay/ac;->c:I

    return v0
.end method

.method static synthetic f(Lmaps/ay/ac;)I
    .locals 1

    iget v0, p0, Lmaps/ay/ac;->d:I

    return v0
.end method


# virtual methods
.method final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/ay/ac;->h:Z

    return-void
.end method

.method final a()[I
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x4

    iget-object v0, p0, Lmaps/ay/ac;->g:[I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/ay/ac;->h:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xf

    new-array v0, v0, [I

    const/16 v1, 0x3024

    aput v1, v0, v2

    iget v1, p0, Lmaps/ay/ac;->a:I

    aput v1, v0, v4

    const/16 v1, 0x3023

    aput v1, v0, v5

    iget v1, p0, Lmaps/ay/ac;->b:I

    aput v1, v0, v6

    const/16 v1, 0x3022

    aput v1, v0, v3

    const/4 v1, 0x5

    iget v2, p0, Lmaps/ay/ac;->c:I

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lmaps/ay/ac;->d:I

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lmaps/ay/ac;->e:I

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lmaps/ay/ac;->f:I

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3040

    aput v2, v0, v1

    const/16 v1, 0xd

    aput v3, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x3038

    aput v2, v0, v1

    iput-object v0, p0, Lmaps/ay/ac;->g:[I

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/ay/ac;->g:[I

    return-object v0

    :cond_1
    const/16 v0, 0xd

    new-array v0, v0, [I

    const/16 v1, 0x3024

    aput v1, v0, v2

    iget v1, p0, Lmaps/ay/ac;->a:I

    aput v1, v0, v4

    const/16 v1, 0x3023

    aput v1, v0, v5

    iget v1, p0, Lmaps/ay/ac;->b:I

    aput v1, v0, v6

    const/16 v1, 0x3022

    aput v1, v0, v3

    const/4 v1, 0x5

    iget v2, p0, Lmaps/ay/ac;->c:I

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lmaps/ay/ac;->d:I

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lmaps/ay/ac;->e:I

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lmaps/ay/ac;->f:I

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3038

    aput v2, v0, v1

    iput-object v0, p0, Lmaps/ay/ac;->g:[I

    goto :goto_0
.end method
