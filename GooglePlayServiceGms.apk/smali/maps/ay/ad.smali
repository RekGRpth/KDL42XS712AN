.class public final Lmaps/ay/ad;
.super Lmaps/ay/ap;

# interfaces
.implements Lmaps/ab/f;
.implements Lmaps/ab/p;
.implements Lmaps/ab/s;
.implements Lmaps/ap/a;


# instance fields
.field private volatile e:Z

.field private volatile f:Z

.field private final g:Lmaps/ab/q;

.field private final h:Lmaps/ab/n;

.field private i:Lmaps/y/a;

.field private final j:Lmaps/ay/af;

.field private final k:Ljava/util/Set;

.field private volatile l:Ljava/util/Set;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Lmaps/aj/af;


# direct methods
.method protected constructor <init>(Lmaps/ah/d;Lmaps/u/c;IILmaps/ay/v;Lmaps/ab/q;)V
    .locals 18

    sget-object v4, Lmaps/ao/b;->n:Lmaps/ao/b;

    new-instance v6, Lmaps/ay/ah;

    move-object/from16 v0, p2

    move/from16 v1, p3

    move-object/from16 v2, p6

    invoke-direct {v6, v0, v1, v2}, Lmaps/ay/ah;-><init>(Lmaps/u/c;ILmaps/ab/q;)V

    const/4 v9, 0x4

    const/16 v11, 0x100

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    move/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v10, p5

    invoke-direct/range {v3 .. v17}, Lmaps/ay/ap;-><init>(Lmaps/ao/b;Lmaps/ah/d;Lmaps/u/c;IIILmaps/ay/v;IZZZZZZ)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lmaps/ay/ad;->e:Z

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lmaps/ay/ad;->f:Z

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/ad;->l:Ljava/util/Set;

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/ad;->m:Ljava/util/Map;

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/ad;->n:Ljava/util/Map;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lmaps/ay/ae;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lmaps/ay/ae;-><init>(Lmaps/ay/ad;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/ad;->o:Lmaps/aj/af;

    invoke-static {}, Lmaps/ae/n;->c()Lmaps/ae/n;

    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lmaps/ay/ad;->g:Lmaps/ab/q;

    new-instance v3, Lmaps/ab/n;

    invoke-direct {v3}, Lmaps/ab/n;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/ad;->h:Lmaps/ab/n;

    new-instance v3, Lmaps/ay/af;

    sget-object v4, Lmaps/ay/v;->d:Lmaps/ay/v;

    invoke-direct {v3, v4}, Lmaps/ay/af;-><init>(Lmaps/ay/v;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/ay/ad;->j:Lmaps/ay/af;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ad;->j:Lmaps/ay/af;

    const v4, -0x7fafafb0

    invoke-virtual {v3, v4}, Lmaps/ay/af;->b(I)V

    return-void
.end method

.method static synthetic a(Lmaps/ay/ad;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ad;->l:Ljava/util/Set;

    return-object v0
.end method

.method private a(Lmaps/ac/r;)Lmaps/am/b;
    .locals 2

    iget-object v0, p0, Lmaps/ay/ad;->d:Lmaps/am/b;

    new-instance v1, Lmaps/am/a;

    invoke-direct {v1, v0, p1}, Lmaps/am/a;-><init>(Lmaps/am/b;Ljava/lang/Object;)V

    return-object v1
.end method

.method private b(Ljava/util/List;)Z
    .locals 10

    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lmaps/ay/ad;->f:Z

    if-eqz v0, :cond_8

    iput-boolean v3, p0, Lmaps/ay/ad;->f:Z

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/ay/ad;->j:Lmaps/ay/af;

    invoke-virtual {v0}, Lmaps/ay/af;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/aj/ae;->d:Lmaps/aj/ae;

    invoke-virtual {p0, v0}, Lmaps/ay/ad;->a(Lmaps/aj/ae;)Lmaps/aj/ad;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v1}, Lmaps/ab/q;->i()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v6, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/k;

    invoke-virtual {v0}, Lmaps/ab/k;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lmaps/aj/ae;->h:Lmaps/aj/ae;

    new-array v7, v2, [Lmaps/aj/af;

    aput-object v0, v7, v3

    invoke-virtual {p0, v1, v7}, Lmaps/ay/ad;->a(Lmaps/aj/ae;[Lmaps/aj/af;)Lmaps/aj/ad;

    move-result-object v1

    :goto_1
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lmaps/ab/k;->b()F

    move-result v1

    cmpl-float v1, v1, v9

    if-lez v1, :cond_4

    invoke-virtual {v0}, Lmaps/ab/k;->c()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lmaps/ab/k;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    invoke-virtual {v0}, Lmaps/ab/k;->b()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_3

    sget-object v1, Lmaps/aj/ae;->f:Lmaps/aj/ae;

    invoke-static {v0}, Lmaps/m/ay;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v7

    iget-object v8, p0, Lmaps/ay/ad;->o:Lmaps/aj/af;

    invoke-static {v8}, Lmaps/m/ay;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v8

    invoke-virtual {p0, v1, v7, v8}, Lmaps/ay/ad;->a(Lmaps/aj/ae;Ljava/util/Collection;Ljava/util/Collection;)Lmaps/aj/ad;

    move-result-object v1

    goto :goto_1

    :cond_3
    sget-object v1, Lmaps/aj/ae;->c:Lmaps/aj/ae;

    invoke-static {v0}, Lmaps/m/ay;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v7

    iget-object v8, p0, Lmaps/ay/ad;->o:Lmaps/aj/af;

    invoke-static {v8}, Lmaps/m/ay;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v8

    invoke-virtual {p0, v1, v7, v8}, Lmaps/ay/ad;->a(Lmaps/aj/ae;Ljava/util/Collection;Ljava/util/Collection;)Lmaps/aj/ad;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lmaps/ab/k;->b()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_1

    invoke-virtual {v0}, Lmaps/ab/k;->c()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lmaps/ab/k;->f()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    sget-object v1, Lmaps/aj/ae;->g:Lmaps/aj/ae;

    new-array v7, v2, [Lmaps/aj/af;

    aput-object v0, v7, v3

    invoke-virtual {p0, v1, v7}, Lmaps/ay/ad;->a(Lmaps/aj/ae;[Lmaps/aj/af;)Lmaps/aj/ad;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lmaps/aj/ae;->b:Lmaps/aj/ae;

    new-array v1, v2, [Lmaps/aj/af;

    new-instance v6, Lmaps/ay/ag;

    invoke-direct {v6, v4}, Lmaps/ay/ag;-><init>(Ljava/util/Set;)V

    aput-object v6, v1, v3

    invoke-virtual {p0, v0, v1}, Lmaps/ay/ad;->a(Lmaps/aj/ae;[Lmaps/aj/af;)Lmaps/aj/ad;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lmaps/aj/ae;->e:Lmaps/aj/ae;

    new-array v1, v2, [Lmaps/aj/af;

    new-instance v4, Lmaps/ay/ag;

    invoke-direct {v4, v5}, Lmaps/ay/ag;-><init>(Ljava/util/Set;)V

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Lmaps/ay/ad;->a(Lmaps/aj/ae;[Lmaps/aj/af;)Lmaps/aj/ad;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    move v0, v2

    :goto_2
    return v0

    :cond_8
    move v0, v3

    goto :goto_2
.end method

.method private q()V
    .locals 3

    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-virtual {v0}, Lmaps/y/a;->b()V

    iget-object v0, p0, Lmaps/ay/ad;->b:Lmaps/ap/n;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/ap/n;->a(ZZ)V

    goto :goto_0
.end method

.method private r()V
    .locals 2

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0}, Lmaps/ab/q;->c()Lmaps/ac/z;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/ay/ad;->j:Lmaps/ay/af;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmaps/ac/aa;->f()I

    move-result v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lmaps/ay/af;->b_(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v1, v0}, Lmaps/ab/q;->b(Lmaps/ac/z;)Lmaps/ac/aa;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lmaps/ac/cw;Lmaps/aj/ac;Ljava/util/Set;)I
    .locals 5

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0}, Lmaps/ab/q;->g()Ljava/util/Set;

    move-result-object v2

    iget-object v0, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v1

    sget-object v4, Lmaps/ac/bx;->d:Lmaps/ac/bx;

    invoke-virtual {v1, v4}, Lmaps/ac/bt;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v1

    check-cast v1, Lmaps/ac/ae;

    invoke-virtual {v1}, Lmaps/ac/ae;->b()Lmaps/ac/r;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p1, :cond_1

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v4

    invoke-virtual {p1, v4}, Lmaps/ac/cw;->b(Lmaps/ac/be;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_1
    invoke-direct {p0, v1}, Lmaps/ay/ad;->a(Lmaps/ac/r;)Lmaps/am/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/au/ap;->a(Lmaps/am/b;)V

    invoke-interface {v0, p2}, Lmaps/au/ap;->a(Lmaps/aj/ac;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lmaps/ar/a;)I
    .locals 5

    const/4 v1, 0x2

    const/4 v0, 0x0

    iget-object v2, p0, Lmaps/ay/ad;->j:Lmaps/ay/af;

    invoke-virtual {v2}, Lmaps/ay/af;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    iget-object v2, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    monitor-exit v2

    :goto_0
    return v0

    :cond_1
    iget-object v3, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/k;

    invoke-virtual {v0}, Lmaps/ab/k;->e()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ad;->f:Z

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-virtual {v1}, Lmaps/y/a;->c()V

    iget-object v1, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v1}, Lmaps/ab/q;->b()V

    invoke-direct {p0}, Lmaps/ay/ad;->q()V

    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a()V
    .locals 0

    invoke-direct {p0}, Lmaps/ay/ad;->q()V

    return-void
.end method

.method public final a(Ljava/util/Set;Ljava/util/Map;)V
    .locals 2

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0}, Lmaps/ab/q;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/aa;

    invoke-virtual {v0}, Lmaps/ac/aa;->b()Lmaps/ac/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ay/ad;->a(Lmaps/ac/r;)Lmaps/am/b;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/ay/ad;->m:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public final a(Lmaps/ab/q;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ad;->e:Z

    iput-boolean v0, p0, Lmaps/ay/ad;->f:Z

    invoke-direct {p0}, Lmaps/ay/ad;->r()V

    invoke-virtual {p1}, Lmaps/ab/q;->c()Lmaps/ac/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/ac/z;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lmaps/ac/ab;->a:Lmaps/k/e;

    invoke-static {v0, v1}, Lmaps/m/bz;->a(Ljava/lang/Iterable;Lmaps/k/e;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/bo;->a(Ljava/lang/Iterable;)Lmaps/m/bo;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/ad;->l:Ljava/util/Set;

    :goto_0
    invoke-direct {p0}, Lmaps/ay/ad;->q()V

    return-void

    :cond_0
    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/ad;->l:Ljava/util/Set;

    goto :goto_0
.end method

.method public final a(Lmaps/ab/q;Lmaps/ac/z;)V
    .locals 6

    const/4 v5, 0x1

    iput-boolean v5, p0, Lmaps/ay/ad;->e:Z

    iput-boolean v5, p0, Lmaps/ay/ad;->f:Z

    invoke-direct {p0}, Lmaps/ay/ad;->r()V

    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {p2}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ab/q;->a(Lmaps/ac/r;)Lmaps/ac/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {p2}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ab/q;->c(Lmaps/ac/r;)Lmaps/ac/ad;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    invoke-direct {p0}, Lmaps/ay/ad;->q()V

    return-void

    :cond_1
    iget-object v2, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v2, v1, v0}, Lmaps/ab/q;->b(Lmaps/ac/ad;Lmaps/ac/ad;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-virtual {v2}, Lmaps/y/a;->c()V

    iget-object v2, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v2, v1, v0}, Lmaps/ab/q;->a(Lmaps/ac/ad;Lmaps/ac/ad;)V

    iget-object v2, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {v2, v0, v5, v5}, Lmaps/ab/q;->a(Lmaps/ac/r;ZZ)Lmaps/ab/k;

    move-result-object v2

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v1}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v5, v4}, Lmaps/ab/q;->a(Lmaps/ac/r;ZZ)Lmaps/ab/k;

    move-result-object v3

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    iget-object v4, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/k;

    invoke-virtual {v0}, Lmaps/ab/k;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lmaps/ab/k;->b()F

    move-result v0

    invoke-virtual {v3}, Lmaps/ab/k;->b()F

    move-result v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_3

    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Lmaps/ab/k;->a(I)V

    const/16 v0, 0x18

    invoke-virtual {v3, v0}, Lmaps/ab/k;->a(I)V

    :goto_2
    iget-object v0, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/ay/ad;->k:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-virtual {p2}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lmaps/y/a;->a(Lmaps/ac/r;Lmaps/ac/ad;)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x14

    :try_start_2
    invoke-virtual {v2, v0}, Lmaps/ab/k;->a(I)V

    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Lmaps/ab/k;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 2

    invoke-virtual {p3}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aj/ad;->b()Lmaps/aj/ae;

    move-result-object v0

    sget-object v1, Lmaps/aj/ae;->d:Lmaps/aj/ae;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lmaps/ay/ad;->j:Lmaps/ay/af;

    invoke-virtual {v0}, Lmaps/ay/af;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/ad;->j:Lmaps/ay/af;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/ay/af;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lmaps/ar/a;->q()F

    move-result v0

    const v1, 0x416e6666    # 14.9f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    invoke-super {p0, p1, p2, p3}, Lmaps/ay/ap;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ad;->c:Z

    goto :goto_0
.end method

.method protected final a(Lmaps/u/a;)V
    .locals 1

    move-object v0, p1

    check-cast v0, Lmaps/y/a;

    iput-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-super {p0, p1}, Lmaps/ay/ap;->a(Lmaps/u/a;)V

    return-void
.end method

.method public final a(Ljava/util/List;)Z
    .locals 1

    invoke-direct {p0, p1}, Lmaps/ay/ad;->b(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final am_()Lmaps/ap/a;
    .locals 0

    return-object p0
.end method

.method public final as_()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0, p0}, Lmaps/ab/q;->b(Lmaps/ab/s;)V

    iget-object v0, p0, Lmaps/ay/ad;->h:Lmaps/ab/n;

    invoke-virtual {v0, p0}, Lmaps/ab/n;->b(Lmaps/ab/p;)V

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0}, Lmaps/ab/q;->j()Lmaps/ab/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lmaps/ab/e;->b(Lmaps/ab/f;)V

    return-void
.end method

.method protected final b(Lmaps/ar/a;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-virtual {v0, p1}, Lmaps/y/a;->a(Lmaps/ar/a;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    invoke-direct {p0}, Lmaps/ay/ad;->q()V

    return-void
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/high16 v5, 0x41880000    # 17.0f

    const v4, 0x416e6666    # 14.9f

    const/4 v3, 0x1

    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/ar/a;->q()F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_8

    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-virtual {v0, p1}, Lmaps/y/a;->d(Lmaps/ar/a;)Lmaps/ac/r;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v2, v0}, Lmaps/ab/q;->d(Lmaps/ac/r;)V

    :cond_0
    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/ar/a;->q()F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-virtual {v0, p1}, Lmaps/y/a;->c(Lmaps/ar/a;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v1, v0}, Lmaps/ab/q;->a(Ljava/util/Set;)V

    iget-object v0, p0, Lmaps/ay/ad;->i:Lmaps/y/a;

    invoke-virtual {v0, p1}, Lmaps/y/a;->b(Lmaps/ar/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    iget-object v2, p0, Lmaps/ay/ad;->h:Lmaps/ab/n;

    invoke-virtual {v2, v0}, Lmaps/ab/n;->a(Lmaps/ac/bt;)Lmaps/ab/l;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0, v1}, Lmaps/ab/q;->a(Ljava/util/Set;)V

    :cond_2
    iget-object v0, p0, Lmaps/ay/ad;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p1}, Lmaps/ar/a;->q()F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_7

    iput-boolean v7, p0, Lmaps/ay/ad;->e:Z

    invoke-super {p0, p1, p2}, Lmaps/ay/ap;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    move-result v2

    iget-object v0, p0, Lmaps/ay/ad;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/ay/ad;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p1}, Lmaps/ar/a;->q()F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_6

    iget-object v0, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v1

    sget-object v5, Lmaps/ac/bx;->d:Lmaps/ac/bx;

    invoke-virtual {v1, v5}, Lmaps/ac/bt;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v1

    check-cast v1, Lmaps/ac/ae;

    invoke-virtual {v1}, Lmaps/ac/ae;->b()Lmaps/ac/r;

    move-result-object v5

    iget-object v1, p0, Lmaps/ay/ad;->n:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/am/b;

    if-nez v1, :cond_4

    invoke-direct {p0, v5}, Lmaps/ay/ad;->a(Lmaps/ac/r;)Lmaps/am/b;

    move-result-object v1

    iget-object v6, p0, Lmaps/ay/ad;->n:Ljava/util/Map;

    invoke-interface {v6, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    iget-object v5, p0, Lmaps/ay/ad;->h:Lmaps/ab/n;

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v0

    invoke-virtual {v5, v0}, Lmaps/ab/n;->a(Lmaps/ac/bt;)Lmaps/ab/l;

    move-result-object v5

    if-eqz v5, :cond_3

    sget-object v0, Lmaps/ab/n;->a:Lmaps/ab/l;

    if-eq v5, v0, :cond_3

    iget-object v0, p0, Lmaps/ay/ad;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/k;

    if-nez v0, :cond_5

    new-instance v0, Lmaps/ac/k;

    new-array v6, v3, [Lmaps/ac/j;

    aput-object v5, v6, v7

    invoke-direct {v0, v6}, Lmaps/ac/k;-><init>([Lmaps/ac/j;)V

    iget-object v5, p0, Lmaps/ay/ad;->m:Ljava/util/Map;

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    invoke-virtual {v0, v5}, Lmaps/ac/k;->a(Lmaps/ac/j;)V

    goto :goto_2

    :cond_6
    move v0, v2

    :goto_3
    return v0

    :cond_7
    iput-boolean v3, p0, Lmaps/ay/ad;->e:Z

    move v0, v3

    goto :goto_3

    :cond_8
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ad;->f:Z

    return-void
.end method

.method public final c(Lmaps/as/a;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lmaps/ay/ap;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0, v1}, Lmaps/ab/q;->d(Lmaps/ac/r;)V

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0, v1}, Lmaps/ab/q;->a(Ljava/util/Set;)V

    return-void
.end method

.method public final d()Lmaps/ar/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0, p0}, Lmaps/ab/q;->a(Lmaps/ab/s;)V

    iget-object v0, p0, Lmaps/ay/ad;->h:Lmaps/ab/n;

    invoke-virtual {v0, p0}, Lmaps/ab/n;->a(Lmaps/ab/p;)V

    iget-object v0, p0, Lmaps/ay/ad;->g:Lmaps/ab/q;

    invoke-virtual {v0}, Lmaps/ab/q;->j()Lmaps/ab/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lmaps/ab/e;->a(Lmaps/ab/f;)V

    return-void
.end method
