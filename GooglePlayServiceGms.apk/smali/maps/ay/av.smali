.class public final Lmaps/ay/av;
.super Lmaps/ak/a;


# instance fields
.field private final b:Lmaps/ar/b;

.field private c:Lmaps/ar/c;

.field private final d:I

.field private final e:J

.field private f:I

.field private final g:Lmaps/bs/b;

.field private h:F


# direct methods
.method protected constructor <init>(Lmaps/ar/b;Lmaps/ar/c;IZF)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lmaps/ak/a;-><init>(Lmaps/ar/b;)V

    iput v2, p0, Lmaps/ay/av;->f:I

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->i()Lmaps/bs/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/av;->g:Lmaps/bs/b;

    iput-object p1, p0, Lmaps/ay/av;->b:Lmaps/ar/b;

    iput-object p2, p0, Lmaps/ay/av;->c:Lmaps/ar/c;

    iget-object v0, p0, Lmaps/ay/av;->g:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/ay/av;->e:J

    iget-object v0, p0, Lmaps/ay/av;->b:Lmaps/ar/b;

    iget-object v1, p0, Lmaps/ay/av;->c:Lmaps/ar/c;

    invoke-interface {v1}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ar/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v2, p0, Lmaps/ay/av;->d:I

    :goto_0
    if-eqz p4, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ay/av;->h:F

    :goto_1
    return-void

    :cond_0
    invoke-static {v2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lmaps/ay/av;->d:I

    goto :goto_0

    :cond_1
    iput p5, p0, Lmaps/ay/av;->h:F

    goto :goto_1
.end method


# virtual methods
.method public final a(Lmaps/ar/a;)Lmaps/ar/c;
    .locals 7

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/ay/av;->g:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iget v4, p0, Lmaps/ay/av;->d:I

    if-nez v4, :cond_0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lmaps/ay/av;->c:Lmaps/ar/c;

    instance-of v0, v0, Lmaps/ak/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ay/av;->c:Lmaps/ar/c;

    check-cast v0, Lmaps/ak/d;

    invoke-interface {v0, p1}, Lmaps/ak/d;->a(Lmaps/ar/a;)Lmaps/ar/c;

    move-result-object v3

    iput-object v3, p0, Lmaps/ay/av;->c:Lmaps/ar/c;

    invoke-interface {v0}, Lmaps/ak/d;->b()I

    move-result v0

    :goto_1
    cmpl-float v3, v1, v2

    if-ltz v3, :cond_1

    iput v0, p0, Lmaps/ay/av;->f:I

    iget-object p0, p0, Lmaps/ay/av;->c:Lmaps/ar/c;

    :goto_2
    return-object p0

    :cond_0
    iget-wide v4, p0, Lmaps/ay/av;->e:J

    sub-long/2addr v0, v4

    long-to-float v0, v0

    iget v1, p0, Lmaps/ay/av;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_1
    float-to-double v3, v1

    const-wide v5, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    double-to-float v1, v3

    add-float/2addr v1, v2

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v1, v3

    sub-float v1, v2, v1

    iget-object v2, p0, Lmaps/ay/av;->b:Lmaps/ar/b;

    iget-object v3, p0, Lmaps/ay/av;->c:Lmaps/ar/c;

    invoke-interface {v3}, Lmaps/ar/c;->a()Lmaps/ar/b;

    move-result-object v3

    iget-object v4, p0, Lmaps/ay/av;->b:Lmaps/ar/b;

    invoke-virtual {v3, v4}, Lmaps/ar/b;->a(Lmaps/ar/b;)Lmaps/ar/b;

    move-result-object v3

    iget v4, p0, Lmaps/ay/av;->h:F

    invoke-static {v2, v3, v1, v4}, Lmaps/ar/b;->a(Lmaps/ar/b;Lmaps/ar/b;FF)Lmaps/ar/b;

    move-result-object v1

    iput-object v1, p0, Lmaps/ay/av;->a:Lmaps/ar/b;

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lmaps/ay/av;->f:I

    goto :goto_2

    :cond_2
    move v0, v3

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lmaps/ay/av;->f:I

    return v0
.end method
