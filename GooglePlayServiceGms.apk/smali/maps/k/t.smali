.class abstract enum Lmaps/k/t;
.super Ljava/lang/Enum;

# interfaces
.implements Lmaps/k/p;


# static fields
.field public static final enum a:Lmaps/k/t;

.field private static enum b:Lmaps/k/t;

.field private static enum c:Lmaps/k/t;

.field private static enum d:Lmaps/k/t;

.field private static final synthetic e:[Lmaps/k/t;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/k/u;

    const-string v1, "ALWAYS_TRUE"

    invoke-direct {v0, v1}, Lmaps/k/u;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/k/t;->b:Lmaps/k/t;

    new-instance v0, Lmaps/k/v;

    const-string v1, "ALWAYS_FALSE"

    invoke-direct {v0, v1}, Lmaps/k/v;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/k/t;->c:Lmaps/k/t;

    new-instance v0, Lmaps/k/w;

    const-string v1, "IS_NULL"

    invoke-direct {v0, v1}, Lmaps/k/w;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/k/t;->a:Lmaps/k/t;

    new-instance v0, Lmaps/k/x;

    const-string v1, "NOT_NULL"

    invoke-direct {v0, v1}, Lmaps/k/x;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/k/t;->d:Lmaps/k/t;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/k/t;

    const/4 v1, 0x0

    sget-object v2, Lmaps/k/t;->b:Lmaps/k/t;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lmaps/k/t;->c:Lmaps/k/t;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lmaps/k/t;->a:Lmaps/k/t;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lmaps/k/t;->d:Lmaps/k/t;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/k/t;->e:[Lmaps/k/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/k/t;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/k/t;
    .locals 1

    const-class v0, Lmaps/k/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/k/t;

    return-object v0
.end method

.method public static values()[Lmaps/k/t;
    .locals 1

    sget-object v0, Lmaps/k/t;->e:[Lmaps/k/t;

    invoke-virtual {v0}, [Lmaps/k/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/k/t;

    return-object v0
.end method


# virtual methods
.method final a()Lmaps/k/p;
    .locals 0

    return-object p0
.end method
