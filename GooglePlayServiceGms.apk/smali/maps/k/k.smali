.class public final Lmaps/k/k;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lmaps/k/l;

.field private c:Lmaps/k/l;

.field private d:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/k/l;

    invoke-direct {v0, v1}, Lmaps/k/l;-><init>(B)V

    iput-object v0, p0, Lmaps/k/k;->b:Lmaps/k/l;

    iget-object v0, p0, Lmaps/k/k;->b:Lmaps/k/l;

    iput-object v0, p0, Lmaps/k/k;->c:Lmaps/k/l;

    iput-boolean v1, p0, Lmaps/k/k;->d:Z

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmaps/k/k;->a:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/k/k;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;
    .locals 2

    new-instance v1, Lmaps/k/l;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lmaps/k/l;-><init>(B)V

    iget-object v0, p0, Lmaps/k/k;->c:Lmaps/k/l;

    iput-object v1, v0, Lmaps/k/l;->c:Lmaps/k/l;

    iput-object v1, p0, Lmaps/k/k;->c:Lmaps/k/l;

    iput-object p2, v1, Lmaps/k/l;->b:Ljava/lang/Object;

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lmaps/k/l;->a:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;D)Lmaps/k/k;
    .locals 1

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmaps/k/k;->b(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;F)Lmaps/k/k;
    .locals 1

    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmaps/k/k;->b(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;I)Lmaps/k/k;
    .locals 1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmaps/k/k;->b(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;J)Lmaps/k/k;
    .locals 1

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmaps/k/k;->b(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/k/k;->b(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)Lmaps/k/k;
    .locals 1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmaps/k/k;->b(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v1, ""

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v2, p0, Lmaps/k/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x7b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lmaps/k/k;->b:Lmaps/k/l;

    iget-object v0, v0, Lmaps/k/l;->c:Lmaps/k/l;

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    iget-object v3, v0, Lmaps/k/l;->a:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lmaps/k/l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v3, v0, Lmaps/k/l;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lmaps/k/l;->c:Lmaps/k/l;

    goto :goto_0

    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
