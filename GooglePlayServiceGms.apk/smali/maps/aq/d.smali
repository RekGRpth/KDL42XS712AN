.class final Lmaps/aq/d;
.super Lmaps/ax/f;


# instance fields
.field private final b:Ljava/lang/Long;

.field private final c:Ljava/util/List;

.field private synthetic d:Lmaps/aq/a;


# direct methods
.method public constructor <init>(Lmaps/aq/a;J)V
    .locals 1

    iput-object p1, p0, Lmaps/aq/d;->d:Lmaps/aq/a;

    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lmaps/ax/f;-><init>(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/aq/d;->c:Ljava/util/List;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmaps/aq/d;->b:Ljava/lang/Long;

    return-void
.end method

.method static synthetic a(Lmaps/aq/d;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/aq/d;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 7

    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lmaps/aq/d;->h()Lmaps/ax/g;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, Lmaps/ax/g;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lmaps/ax/g;->a()Lmaps/ax/h;

    move-result-object v3

    iget-object v0, v3, Lmaps/ax/h;->a:Ljava/lang/Object;

    sget-object v4, Lmaps/aq/a;->a:Lmaps/ac/bt;

    if-eq v0, v4, :cond_2

    iget-object v0, v3, Lmaps/ax/h;->b:Ljava/lang/Object;

    check-cast v0, Lmaps/aq/b;

    iget-object v4, v0, Lmaps/aq/b;->a:Lmaps/au/ap;

    if-eqz v4, :cond_0

    invoke-interface {v4}, Lmaps/au/ap;->i()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    iget-object v0, v3, Lmaps/ax/h;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lmaps/aq/d;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lmaps/as/a;->b(J)Lmaps/as/a;

    move-result-object v3

    invoke-interface {v4, v3}, Lmaps/au/ap;->a(Lmaps/as/a;)V

    iget-object v3, p0, Lmaps/aq/d;->d:Lmaps/aq/a;

    iget v4, v0, Lmaps/aq/b;->b:I

    invoke-static {v3, v4}, Lmaps/aq/a;->a(Lmaps/aq/a;I)I

    const/4 v3, 0x0

    iput v3, v0, Lmaps/aq/b;->b:I

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    invoke-virtual {p0, v0}, Lmaps/aq/d;->d(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p1, Lmaps/ac/bt;

    check-cast p2, Lmaps/aq/b;

    invoke-super {p0, p1, p2}, Lmaps/ax/f;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/aq/d;->d:Lmaps/aq/a;

    iget v1, p2, Lmaps/aq/b;->b:I

    invoke-static {v0, v1}, Lmaps/aq/a;->a(Lmaps/aq/a;I)I

    iget-object v0, p0, Lmaps/aq/d;->d:Lmaps/aq/a;

    iget v1, p2, Lmaps/aq/b;->c:I

    invoke-static {v0, v1}, Lmaps/aq/a;->b(Lmaps/aq/a;I)I

    iget-object v0, p2, Lmaps/aq/b;->a:Lmaps/au/ap;

    if-eqz v0, :cond_0

    iput v2, p2, Lmaps/aq/b;->b:I

    iput v2, p2, Lmaps/aq/b;->c:I

    invoke-virtual {p0, p2}, Lmaps/aq/d;->a(Lmaps/aq/b;)V

    :cond_0
    return-void
.end method

.method public final a(Lmaps/aq/b;)V
    .locals 1

    iget-object v0, p0, Lmaps/aq/d;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lmaps/as/a;)V
    .locals 4

    iget-object v0, p0, Lmaps/aq/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aq/b;

    iget-object v2, v0, Lmaps/aq/b;->a:Lmaps/au/ap;

    invoke-interface {v2, p1}, Lmaps/au/ap;->b(Lmaps/as/a;)V

    iget-object v2, p0, Lmaps/aq/d;->d:Lmaps/aq/a;

    iget v3, v0, Lmaps/aq/b;->b:I

    invoke-static {v2, v3}, Lmaps/aq/a;->a(Lmaps/aq/a;I)I

    iget-object v2, p0, Lmaps/aq/d;->d:Lmaps/aq/a;

    iget v0, v0, Lmaps/aq/b;->c:I

    invoke-static {v2, v0}, Lmaps/aq/a;->b(Lmaps/aq/a;I)I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/aq/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final b()Lmaps/ax/h;
    .locals 2

    invoke-virtual {p0}, Lmaps/aq/d;->h()Lmaps/ax/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ax/g;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lmaps/ax/g;->a()Lmaps/ax/h;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    iget-object v0, p0, Lmaps/aq/d;->d:Lmaps/aq/a;

    invoke-static {v0}, Lmaps/aq/a;->a(Lmaps/aq/a;)Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v1

    sget-object v0, Lmaps/aq/a;->a:Lmaps/ac/bt;

    invoke-virtual {p0, v0}, Lmaps/aq/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aq/b;

    if-nez v0, :cond_0

    sget-object v0, Lmaps/aq/a;->a:Lmaps/ac/bt;

    new-instance v3, Lmaps/aq/b;

    invoke-direct {v3, v1, v2}, Lmaps/aq/b;-><init>(J)V

    invoke-virtual {p0, v0, v3}, Lmaps/aq/d;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iput-wide v1, v0, Lmaps/aq/b;->d:J

    goto :goto_0
.end method
