.class public final Lmaps/f/b;
.super Letr;

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Leqg;
.implements Ljava/lang/Runnable;
.implements Lmaps/f/a;


# static fields
.field private static final f:Lcom/google/android/gms/location/LocationRequest;


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Z

.field private c:Leuf;

.field private d:Leqf;

.field private e:Landroid/location/Location;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v1, 0x10

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    sput-object v0, Lmaps/f/b;->f:Lcom/google/android/gms/location/LocationRequest;

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Letr;-><init>()V

    iput-object p1, p0, Lmaps/f/b;->a:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lmaps/f/b;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    new-instance v0, Lmaps/f/b;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, Lmaps/f/b;-><init>(Landroid/os/Handler;)V

    new-instance v1, Leqf;

    invoke-direct {v1, p0, v0, v0}, Leqf;-><init>(Landroid/content/Context;Lbbr;Lbbs;)V

    iput-object v1, v0, Lmaps/f/b;->d:Leqf;

    return-object v0
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lmaps/f/b;->d:Leqf;

    invoke-virtual {v0}, Leqf;->b()V

    iget-object v0, p0, Lmaps/f/b;->a:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/f/b;->e:Landroid/location/Location;

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 0

    return-void
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lmaps/f/b;->c:Leuf;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "already activated"

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/f/b;->c:Leuf;

    iget-boolean v0, p0, Lmaps/f/b;->b:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/f/b;->f()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    iput-object p1, p0, Lmaps/f/b;->e:Landroid/location/Location;

    iget-object v0, p0, Lmaps/f/b;->a:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    return-void
.end method

.method public final a(Leuf;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/f/b;->c:Leuf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "already activated"

    invoke-static {v0, v3}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    if-eqz p1, :cond_2

    :goto_1
    const-string v0, "listener cannot be null"

    invoke-static {v1, v0}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lmaps/f/b;->c:Leuf;

    iget-boolean v0, p0, Lmaps/f/b;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/b;->d:Leqf;

    invoke-virtual {v0}, Leqf;->a()V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/f/b;->b:Z

    iget-object v0, p0, Lmaps/f/b;->c:Leuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/b;->d:Leqf;

    invoke-virtual {v0}, Leqf;->a()V

    :cond_0
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lmaps/f/b;->d:Leqf;

    sget-object v1, Lmaps/f/b;->f:Lcom/google/android/gms/location/LocationRequest;

    iget-object v0, v0, Leqf;->a:Lera;

    invoke-virtual {v0, v1, p0}, Lera;->a(Lcom/google/android/gms/location/LocationRequest;Leqg;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lmaps/f/b;->c:Leuf;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/f/b;->f()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/f/b;->b:Z

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/b;->e:Landroid/location/Location;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final run()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lmaps/f/b;->c:Leuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/b;->c:Leuf;

    iget-object v1, p0, Lmaps/f/b;->e:Landroid/location/Location;

    invoke-static {v1}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v1

    invoke-interface {v0, v1}, Leuf;->a(Lcrv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
