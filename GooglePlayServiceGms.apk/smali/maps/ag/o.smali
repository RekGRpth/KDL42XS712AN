.class final Lmaps/ag/o;
.super Ljava/lang/Object;


# instance fields
.field private final a:[B

.field private final b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    const/16 v0, 0x2000

    new-array v0, v0, [B

    invoke-direct {p0, p1, v0}, Lmaps/ag/o;-><init>(I[B)V

    return-void
.end method

.method private constructor <init>(I[B)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/ag/o;->d:I

    iput v0, p0, Lmaps/ag/o;->e:I

    iput-object p2, p0, Lmaps/ag/o;->a:[B

    iput p1, p0, Lmaps/ag/o;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ag/o;->c:I

    return-void
.end method

.method constructor <init>([B)V
    .locals 2

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/ag/o;->d:I

    iput v0, p0, Lmaps/ag/o;->e:I

    iput-object p1, p0, Lmaps/ag/o;->a:[B

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/ag/h;->a([BI)I

    move-result v0

    iput v0, p0, Lmaps/ag/o;->b:I

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmaps/ag/h;->a([BI)I

    move-result v0

    iput v0, p0, Lmaps/ag/o;->c:I

    return-void
.end method

.method static synthetic a(Lmaps/ag/o;)I
    .locals 1

    iget v0, p0, Lmaps/ag/o;->b:I

    return v0
.end method

.method static synthetic b(Lmaps/ag/o;)I
    .locals 1

    iget v0, p0, Lmaps/ag/o;->c:I

    return v0
.end method


# virtual methods
.method final a()I
    .locals 1

    iget v0, p0, Lmaps/ag/o;->b:I

    return v0
.end method

.method final a(I)V
    .locals 4

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    const-wide/16 v2, -0x1

    invoke-static {v0, v1, v2, v3}, Lmaps/ag/h;->a([BIJ)V

    return-void
.end method

.method final a(Lmaps/ag/n;)V
    .locals 2

    iget v0, p0, Lmaps/ag/o;->c:I

    invoke-virtual {p0, p1, v0}, Lmaps/ag/o;->a(Lmaps/ag/n;I)V

    iget v0, p0, Lmaps/ag/o;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ag/o;->c:I

    iget v0, p1, Lmaps/ag/n;->b:I

    iget v1, p1, Lmaps/ag/n;->d:I

    add-int/2addr v0, v1

    iget v1, p1, Lmaps/ag/n;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ag/o;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ag/o;->e:I

    return-void
.end method

.method final a(Lmaps/ag/n;I)V
    .locals 2

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    mul-int/lit8 v1, p2, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-virtual {p1, v0, v1}, Lmaps/ag/n;->a([BI)V

    return-void
.end method

.method final a(Lmaps/x/a;)V
    .locals 5

    const/16 v4, 0x1ffc

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    iget v1, p0, Lmaps/ag/o;->b:I

    invoke-static {v0, v3, v1}, Lmaps/ag/h;->a([BII)V

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    const/4 v1, 0x4

    iget v2, p0, Lmaps/ag/o;->c:I

    invoke-static {v0, v1, v2}, Lmaps/ag/h;->a([BII)V

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    invoke-static {v0, v3, v4}, Lmaps/ag/h;->c([BII)I

    move-result v0

    iget-object v1, p0, Lmaps/ag/o;->a:[B

    invoke-static {v1, v4, v0}, Lmaps/ag/h;->a([BII)V

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    invoke-virtual {p1, v0}, Lmaps/x/a;->a([B)V

    return-void
.end method

.method final b()I
    .locals 1

    iget v0, p0, Lmaps/ag/o;->c:I

    return v0
.end method

.method final b(I)J
    .locals 2

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lmaps/ag/h;->c([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method final c()I
    .locals 3

    iget v0, p0, Lmaps/ag/o;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/ag/o;->d:I

    if-gez v0, :cond_1

    iget v0, p0, Lmaps/ag/o;->c:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lmaps/ag/o;->e(I)Lmaps/ag/n;

    move-result-object v0

    iget v1, v0, Lmaps/ag/n;->b:I

    iget v2, v0, Lmaps/ag/n;->d:I

    add-int/2addr v1, v2

    iget v0, v0, Lmaps/ag/n;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ag/o;->d:I

    :cond_1
    iget v0, p0, Lmaps/ag/o;->d:I

    goto :goto_0
.end method

.method final c(I)I
    .locals 2

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lmaps/ag/n;->b([BI)I

    move-result v0

    return v0
.end method

.method final d()I
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, Lmaps/ag/o;->e:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    iput v0, p0, Lmaps/ag/o;->e:I

    :goto_0
    iget v1, p0, Lmaps/ag/o;->c:I

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lmaps/ag/o;->d(I)I

    move-result v1

    if-lez v1, :cond_0

    iget v1, p0, Lmaps/ag/o;->e:I

    invoke-virtual {p0, v0}, Lmaps/ag/o;->c(I)I

    move-result v2

    iget-object v3, p0, Lmaps/ag/o;->a:[B

    mul-int/lit8 v4, v0, 0x14

    add-int/lit8 v4, v4, 0x8

    invoke-static {v3, v4}, Lmaps/ag/n;->c([BI)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lmaps/ag/o;->e:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/ag/o;->e:I

    return v0
.end method

.method final d(I)I
    .locals 2

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lmaps/ag/n;->d([BI)I

    move-result v0

    return v0
.end method

.method final e(I)Lmaps/ag/n;
    .locals 3

    iget-object v0, p0, Lmaps/ag/o;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    iget v2, p0, Lmaps/ag/o;->b:I

    invoke-static {v0, v1, v2, p1}, Lmaps/ag/n;->a([BIII)Lmaps/ag/n;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ID:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lmaps/ag/o;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/o;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
