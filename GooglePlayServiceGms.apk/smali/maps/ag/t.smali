.class public final Lmaps/ag/t;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ax/f;

.field private final b:Lmaps/ax/f;

.field private c:Lmaps/ag/a;

.field private final d:Lmaps/bs/b;

.field private e:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;Lmaps/bs/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/ax/f;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    new-instance v0, Lmaps/ax/f;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    iput-object p2, p0, Lmaps/ag/t;->d:Lmaps/bs/b;

    iput-object p1, p0, Lmaps/ag/t;->e:Ljava/util/Locale;

    return-void
.end method

.method private a(Lmaps/ac/r;Z)Lmaps/ac/z;
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/r;

    move-object p1, v0

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/z;

    if-eqz v0, :cond_2

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    monitor-exit v2

    if-eqz p2, :cond_3

    iget-object v0, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    if-nez v0, :cond_4

    :cond_3
    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    iget-object v0, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    invoke-virtual {p1}, Lmaps/ac/r;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/ag/a;->a(Ljava/lang/String;)Lmaps/ag/d;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, v0, Lmaps/ag/d;->a:Lmaps/bv/a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/ac/r;->b(Ljava/lang/String;)Lmaps/ac/r;

    move-result-object v2

    if-nez v2, :cond_6

    move-object v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p1, v2}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v0, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    invoke-virtual {v2}, Lmaps/ac/r;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/ag/a;->a(Ljava/lang/String;)Lmaps/ag/d;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v1, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    monitor-enter v1

    :try_start_2
    iget-object v3, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    invoke-virtual {v3, p1, v2}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_7
    iget-object v1, v0, Lmaps/ag/d;->a:Lmaps/bv/a;

    iget-wide v2, v0, Lmaps/ag/d;->b:J

    invoke-static {v1, v2, v3}, Lmaps/ac/z;->a(Lmaps/bv/a;J)Lmaps/ac/z;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lmaps/ag/t;->b(Lmaps/ac/z;)V

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_8
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lmaps/ac/z;)Z
    .locals 1

    instance-of v0, p0, Lmaps/ag/u;

    return v0
.end method

.method private b(Lmaps/ac/z;)V
    .locals 5

    invoke-virtual {p1}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v1

    iget-object v2, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    invoke-virtual {v0, v1, p1}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v2, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    monitor-enter v2

    :try_start_1
    invoke-virtual {p1}, Lmaps/ac/z;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/aa;

    iget-object v4, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ac/aa;->b()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {v4, v0, v1}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/r;)Lmaps/ac/z;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmaps/ag/t;->a(Lmaps/ac/r;Z)Lmaps/ac/z;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lmaps/ac/r;Lmaps/bv/a;)Lmaps/ac/z;
    .locals 10

    const/4 v0, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    invoke-virtual {p2, v8}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmaps/ac/r;->b(Ljava/lang/String;)Lmaps/ac/r;

    move-result-object v1

    if-eqz p1, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lmaps/ag/t;->d:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    iget-object v4, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    invoke-virtual {v1}, Lmaps/ac/r;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p2}, Lmaps/ag/a;->a(Ljava/lang/String;Lmaps/bv/a;)V

    invoke-virtual {p2, v9}, Lmaps/bv/a;->j(I)I

    move-result v4

    if-eqz v4, :cond_3

    new-instance v5, Lmaps/bv/a;

    sget-object v6, Lmaps/ck/a;->a:Lmaps/bv/c;

    invoke-direct {v5, v6}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v1}, Lmaps/ac/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v8, v1}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {p2, v9, v1}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v6

    invoke-virtual {v6, v8}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lmaps/ac/r;->b(Ljava/lang/String;)Lmaps/ac/r;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v7, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    invoke-virtual {v6}, Lmaps/ac/r;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6, v5}, Lmaps/ag/a;->a(Ljava/lang/String;Lmaps/bv/a;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-static {p2, v2, v3}, Lmaps/ac/z;->a(Lmaps/bv/a;J)Lmaps/ac/z;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lmaps/ag/t;->b(Lmaps/ac/z;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    iget-object v1, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/File;)V
    .locals 4

    new-instance v0, Lmaps/ag/a;

    iget-object v1, p0, Lmaps/ag/t;->d:Lmaps/bs/b;

    const-string v2, "bd"

    sget-object v3, Lmaps/ck/a;->a:Lmaps/bv/c;

    invoke-direct {v0, v1, v2, v3}, Lmaps/ag/a;-><init>(Lmaps/bs/b;Ljava/lang/String;Lmaps/bv/c;)V

    invoke-virtual {v0, p1}, Lmaps/ag/a;->a(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ag/t;->e:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lmaps/ag/a;->a(Ljava/util/Locale;)Z

    iput-object v0, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    :cond_0
    return-void
.end method

.method public final b(Lmaps/ac/r;)Lmaps/ac/z;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/ag/t;->a(Lmaps/ac/r;Z)Lmaps/ac/z;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2

    iget-object v1, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ag/t;->b:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/t;->c:Lmaps/ag/a;

    invoke-virtual {v0}, Lmaps/ag/a;->a()Z

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Lmaps/ac/r;)V
    .locals 3

    new-instance v0, Lmaps/ag/u;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lmaps/ag/u;-><init>(Lmaps/ac/r;B)V

    iget-object v1, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/ag/t;->a:Lmaps/ax/f;

    invoke-virtual {v2, p1, v0}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
