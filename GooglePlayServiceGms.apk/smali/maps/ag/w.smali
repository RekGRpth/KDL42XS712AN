.class public final Lmaps/ag/w;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ag/f;


# static fields
.field private static final a:Lmaps/ac/bs;

.field private static final b:[B


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lmaps/ag/ag;

.field private final f:Ljava/util/HashMap;

.field private g:Lmaps/ag/h;

.field private final h:I

.field private final i:Lmaps/ao/b;

.field private j:I

.field private k:Lmaps/bs/b;

.field private l:Lmaps/ag/x;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/ac/aq;

    invoke-direct {v0}, Lmaps/ac/aq;-><init>()V

    sput-object v0, Lmaps/ag/w;->a:Lmaps/ac/bs;

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lmaps/ag/w;->b:[B

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILmaps/ag/ag;Lmaps/ao/b;Lmaps/ag/g;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ag/w;->j:I

    new-instance v0, Lmaps/bs/b;

    invoke-direct {v0}, Lmaps/bs/b;-><init>()V

    iput-object v0, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    iput-object p1, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    iput p2, p0, Lmaps/ag/w;->d:I

    iput-object p3, p0, Lmaps/ag/w;->e:Lmaps/ag/ag;

    invoke-static {}, Lmaps/ap/p;->f()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    const/16 v1, 0x100

    mul-int/lit8 v0, v0, 0x20

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lmaps/ag/w;->h:I

    iget v0, p0, Lmaps/ag/w;->h:I

    invoke-static {v0}, Lmaps/m/co;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    iput-object p4, p0, Lmaps/ag/w;->i:Lmaps/ao/b;

    if-eqz p5, :cond_0

    new-instance v0, Lmaps/ag/x;

    iget-object v1, p0, Lmaps/ag/w;->i:Lmaps/ao/b;

    invoke-direct {v0, v1, p5}, Lmaps/ag/x;-><init>(Lmaps/ao/b;Lmaps/ag/g;)V

    iput-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    :cond_0
    return-void
.end method

.method private a(ILjava/util/Locale;)Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v0, p1, p2}, Lmaps/ag/h;->a(ILjava/util/Locale;)V

    invoke-direct {p0}, Lmaps/ag/w;->g()V

    iget-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    invoke-virtual {v0}, Lmaps/ag/x;->b()V

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/io/File;)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-eqz v0, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    new-instance v4, Lmaps/x/b;

    invoke-direct {v4, p1}, Lmaps/x/b;-><init>(Ljava/io/File;)V

    iget-object v0, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->c()J

    :try_start_0
    iget-object v0, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    iget-object v1, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    invoke-static {v0, v4, v1}, Lmaps/ag/h;->a(Ljava/lang/String;Lmaps/x/b;Lmaps/ag/r;)Lmaps/ag/h;

    move-result-object v0

    iput-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v7

    :goto_1
    if-nez v0, :cond_1

    invoke-direct {p0}, Lmaps/ag/w;->h()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v2}, Lmaps/ag/h;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    :cond_1
    move v0, v6

    :goto_2
    if-eqz v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    iget v1, p0, Lmaps/ag/w;->d:I

    const/4 v2, -0x1

    new-instance v3, Ljava/util/Locale;

    const-string v5, ""

    invoke-direct {v3, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    invoke-static/range {v0 .. v5}, Lmaps/ag/h;->a(Ljava/lang/String;IILjava/util/Locale;Lmaps/x/b;Lmaps/ag/r;)Lmaps/ag/h;

    move-result-object v0

    iput-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-direct {p0}, Lmaps/ag/w;->g()V

    iget-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    invoke-virtual {v0}, Lmaps/ag/x;->a()V

    iget-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    invoke-virtual {v0}, Lmaps/ag/x;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    iget-object v0, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->c()J

    move v0, v6

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v6

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Creating cache: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    goto :goto_0
.end method

.method private g()V
    .locals 4

    new-instance v0, Lmaps/bj/a;

    invoke-direct {v0}, Lmaps/bj/a;-><init>()V

    iget-object v1, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v1}, Lmaps/ag/h;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/bj/a;->writeLong(J)V

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/bj/a;->a()[B

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "disk_creation_time_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lmaps/bt/k;->a([BLjava/lang/String;)I

    return-void
.end method

.method private h()J
    .locals 5

    const-wide/16 v0, 0x0

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "disk_creation_time_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lmaps/bt/k;->c(Ljava/lang/String;)[B

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    new-instance v3, Lmaps/bw/a;

    invoke-direct {v3, v2}, Lmaps/bw/a;-><init>([B)V

    :try_start_0
    invoke-virtual {v3}, Lmaps/bw/a;->readLong()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "disk_creation_time_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lmaps/bt/k;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Lmaps/ac/bt;)V
    .locals 2

    sget-object v0, Lmaps/ag/w;->a:Lmaps/ac/bs;

    sget-object v1, Lmaps/ag/w;->b:[B

    invoke-virtual {p0, p1, v0, v1}, Lmaps/ag/w;->a(Lmaps/ac/bt;Lmaps/ac/bs;[B)V

    return-void
.end method

.method public final a(Lmaps/ac/bt;Lmaps/ac/bs;)V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Don\'t store unencrypted tiles into SD cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lmaps/ac/bt;Lmaps/ac/bs;[B)V
    .locals 13

    const/4 v10, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v8, -0x1

    iget-object v1, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Uninitialized"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    instance-of v1, p2, Lmaps/ac/as;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can\'t insert a MutableVectorTile into SD cache"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v1

    const/16 v2, 0x15

    if-gt v1, v2, :cond_3

    iget-object v1, p0, Lmaps/ag/w;->i:Lmaps/ao/b;

    invoke-static {v1, p1}, Lmaps/aw/a;->a(Lmaps/ao/b;Lmaps/ac/bt;)Landroid/util/Pair;

    move-result-object v11

    move-object/from16 v0, p3

    array-length v1, v0

    if-lez v1, :cond_9

    instance-of v1, p2, Lmaps/ac/m;

    if-eqz v1, :cond_8

    move-object v1, p2

    check-cast v1, Lmaps/ac/m;

    invoke-interface {v1}, Lmaps/ac/m;->n()J

    move-result-wide v1

    cmp-long v3, v1, v8

    if-eqz v3, :cond_7

    iget-object v3, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v6

    sub-long/2addr v1, v6

    iget-object v3, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v6

    add-long/2addr v1, v6

    cmp-long v3, v1, v4

    if-gez v3, :cond_7

    move-wide v2, v4

    :goto_0
    move-object v1, p2

    check-cast v1, Lmaps/ac/m;

    invoke-interface {v1}, Lmaps/ac/m;->q()J

    move-result-wide v6

    cmp-long v1, v6, v8

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iget-object v1, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v1, v6, v4

    if-gez v1, :cond_6

    :goto_1
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    move-object/from16 v0, p3

    array-length v1, v0

    add-int/lit8 v1, v1, 0x18

    invoke-direct {v7, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v8, Ljava/io/DataOutputStream;

    invoke-direct {v8, v7}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v1, 0x18

    :try_start_0
    invoke-virtual {v8, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v8, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v8, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    :try_start_1
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    iget-object v12, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    monitor-enter v12

    :try_start_2
    iget-object v1, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lmaps/ag/af;

    move-object v9, v0

    if-eqz v9, :cond_5

    iget v1, v9, Lmaps/ag/af;->d:I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lmaps/ae/u;->a(II)I

    move-result v5

    :goto_3
    new-instance v1, Lmaps/ag/af;

    iget-object v2, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    if-eqz v9, :cond_4

    iget-object v8, v9, Lmaps/ag/af;->h:Lmaps/af/d;

    :goto_4
    move-object v7, p2

    invoke-direct/range {v1 .. v8}, Lmaps/ag/af;-><init>(JLjava/lang/String;I[BLmaps/ac/bs;Lmaps/af/d;)V

    iget v2, p0, Lmaps/ag/w;->j:I

    iget v3, p0, Lmaps/ag/w;->h:I

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v9, :cond_2

    iget v1, p0, Lmaps/ag/w;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/ag/w;->j:I

    :cond_2
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    :try_start_3
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    :catchall_0
    move-exception v1

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    :try_start_4
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    throw v1

    :cond_4
    const/4 v8, 0x0

    goto :goto_4

    :catchall_1
    move-exception v1

    monitor-exit v12

    throw v1

    :catch_2
    move-exception v2

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_2

    :cond_5
    move v5, v10

    goto :goto_3

    :cond_6
    move-wide v4, v6

    goto/16 :goto_1

    :cond_7
    move-wide v2, v1

    goto/16 :goto_0

    :cond_8
    move-wide v4, v8

    move-wide v2, v8

    goto/16 :goto_1

    :cond_9
    move-object/from16 v6, p3

    goto :goto_2
.end method

.method public final a(Lmaps/ac/bt;Lmaps/af/d;I)V
    .locals 10

    iget-object v1, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Uninitialized"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v9, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    monitor-enter v9

    :try_start_0
    iget-object v1, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lmaps/ag/af;

    move-object v7, v0

    if-eqz v7, :cond_3

    iget-object v1, v7, Lmaps/ag/af;->h:Lmaps/af/d;

    if-eqz v1, :cond_4

    if-eqz p2, :cond_1

    new-instance v8, Lmaps/af/a;

    iget-object v1, v7, Lmaps/ag/af;->h:Lmaps/af/d;

    invoke-direct {v8, v1, p2}, Lmaps/af/a;-><init>(Lmaps/af/d;Lmaps/af/d;)V

    :goto_0
    iget-object v1, v7, Lmaps/ag/af;->f:Lmaps/ac/bs;

    if-eqz v1, :cond_2

    new-instance v1, Lmaps/ag/af;

    iget-wide v2, v7, Lmaps/ag/af;->b:J

    iget-object v4, v7, Lmaps/ag/af;->c:Ljava/lang/String;

    iget v5, v7, Lmaps/ag/af;->d:I

    invoke-static {v5, p3}, Lmaps/ae/u;->a(II)I

    move-result v5

    iget-object v6, v7, Lmaps/ag/af;->e:[B

    iget-object v7, v7, Lmaps/ag/af;->f:Lmaps/ac/bs;

    invoke-direct/range {v1 .. v8}, Lmaps/ag/af;-><init>(JLjava/lang/String;I[BLmaps/ac/bs;Lmaps/af/d;)V

    :goto_1
    iget-object v2, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    monitor-exit v9

    return-void

    :cond_1
    iget-object v8, v7, Lmaps/ag/af;->h:Lmaps/af/d;

    goto :goto_0

    :cond_2
    new-instance v2, Lmaps/ag/af;

    iget-wide v3, v7, Lmaps/ag/af;->b:J

    iget-object v5, v7, Lmaps/ag/af;->c:Ljava/lang/String;

    iget v1, v7, Lmaps/ag/af;->d:I

    invoke-static {v1, p3}, Lmaps/ae/u;->a(II)I

    move-result v6

    move-object v7, p1

    invoke-direct/range {v2 .. v8}, Lmaps/ag/af;-><init>(JLjava/lang/String;ILmaps/ac/bt;Lmaps/af/d;)V

    move-object v1, v2

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lmaps/ag/w;->i:Lmaps/ao/b;

    invoke-static {v1, p1}, Lmaps/aw/a;->a(Lmaps/ao/b;Lmaps/ac/bt;)Landroid/util/Pair;

    move-result-object v4

    new-instance v1, Lmaps/ag/af;

    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move v5, p3

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lmaps/ag/af;-><init>(JLjava/lang/String;ILmaps/ac/bt;Lmaps/af/d;)V

    iget-object v2, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v9

    throw v1

    :cond_4
    move-object v8, p2

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v0}, Lmaps/ag/h;->a()I

    move-result v0

    iget-object v1, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v1}, Lmaps/ag/h;->c()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmaps/ag/w;->a(ILjava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public final a(I)Z
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v0, p1}, Lmaps/ag/h;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/io/File;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lmaps/bl/a;->a()V

    invoke-direct {p0, p1}, Lmaps/ag/w;->b(Ljava/io/File;)Z

    move-result v0

    invoke-static {}, Lmaps/bl/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/Locale;)Z
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v0}, Lmaps/ag/h;->a()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lmaps/ag/w;->a(ILjava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public final a(Lmaps/ac/bs;)Z
    .locals 1

    sget-object v0, Lmaps/ag/w;->a:Lmaps/ac/bs;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 14

    const/4 v13, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->c()J

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    monitor-enter v5

    :try_start_0
    iget-object v0, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/af;

    iget-object v8, v0, Lmaps/ag/af;->e:[B

    if-eqz v8, :cond_1

    iget-object v8, v0, Lmaps/ag/af;->e:[B

    :cond_1
    iget v8, v0, Lmaps/ag/af;->a:I

    packed-switch v8, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-wide v8, v0, Lmaps/ag/af;->b:J

    iget-object v10, v0, Lmaps/ag/af;->c:Ljava/lang/String;

    iget v11, v0, Lmaps/ag/af;->d:I

    iget-object v12, v0, Lmaps/ag/af;->e:[B

    invoke-static {v8, v9, v10, v11, v12}, Lmaps/ag/h;->a(JLjava/lang/String;I[B)Lmaps/ag/k;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v8, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    iget-object v9, v0, Lmaps/ag/af;->f:Lmaps/ac/bs;

    invoke-virtual {v8, v9}, Lmaps/ag/x;->a(Lmaps/ac/bs;)V

    :cond_2
    iget-object v8, v0, Lmaps/ag/af;->h:Lmaps/af/d;

    if-eqz v8, :cond_0

    iget-object v8, v0, Lmaps/ag/af;->h:Lmaps/af/d;

    iget-object v0, v0, Lmaps/ag/af;->f:Lmaps/ac/bs;

    invoke-static {v8, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :pswitch_1
    :try_start_1
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmaps/ag/w;->j:I

    iget-object v0, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    :try_start_2
    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v0, v1}, Lmaps/ag/h;->a(Ljava/util/Collection;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    if-ne v0, v13, :cond_9

    move v0, v3

    :goto_1
    move v5, v0

    :goto_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/af/d;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lmaps/ac/bs;

    invoke-interface {v2}, Lmaps/ac/bs;->a()Lmaps/ac/bt;

    move-result-object v2

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lmaps/ac/bs;

    invoke-interface {v1, v2, v5, v0}, Lmaps/af/d;->a(Lmaps/ac/bt;ILmaps/ac/bs;)V

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v5, v3

    goto :goto_2

    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/af;

    iget v1, v0, Lmaps/ag/af;->d:I

    if-lez v1, :cond_8

    :try_start_3
    iget-object v1, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    iget-wide v5, v0, Lmaps/ag/af;->b:J

    iget-object v7, v0, Lmaps/ag/af;->c:Ljava/lang/String;

    iget v8, v0, Lmaps/ag/af;->d:I

    invoke-virtual {v1, v5, v6, v7, v8}, Lmaps/ag/h;->a(JLjava/lang/String;I)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v1

    if-ne v1, v13, :cond_8

    const/4 v1, 0x2

    :goto_5
    iget-object v5, v0, Lmaps/ag/af;->h:Lmaps/af/d;

    if-eqz v5, :cond_5

    iget-object v5, v0, Lmaps/ag/af;->h:Lmaps/af/d;

    iget-object v0, v0, Lmaps/ag/af;->g:Lmaps/ac/bt;

    const/4 v6, 0x0

    invoke-interface {v5, v0, v1, v6}, Lmaps/af/d;->a(Lmaps/ac/bt;ILmaps/ac/bs;)V

    goto :goto_4

    :catch_1
    move-exception v1

    move v1, v3

    goto :goto_5

    :cond_6
    iget-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    invoke-virtual {v0}, Lmaps/ag/x;->c()V

    iget-object v0, p0, Lmaps/ag/w;->l:Lmaps/ag/x;

    invoke-virtual {v0}, Lmaps/ag/x;->b()V

    :cond_7
    return-void

    :cond_8
    move v1, v4

    goto :goto_5

    :cond_9
    move v0, v4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lmaps/ac/bt;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v0

    const/16 v3, 0x15

    if-le v0, v3, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/ag/w;->i:Lmaps/ao/b;

    invoke-static {v0, p1}, Lmaps/aw/a;->a(Lmaps/ao/b;Lmaps/ac/bt;)Landroid/util/Pair;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v0}, Lmaps/ag/h;->b(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/af;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lmaps/ag/af;->e:[B

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final c(Lmaps/ac/bt;)Lmaps/ac/bs;
    .locals 13

    const-wide/16 v9, -0x1

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v0

    const/16 v1, 0x15

    if-le v0, v1, :cond_1

    move-object v0, v8

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/af;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lmaps/ag/af;->f:Lmaps/ac/bs;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    monitor-exit v1

    iget-object v0, p0, Lmaps/ag/w;->i:Lmaps/ao/b;

    invoke-static {v0, p1}, Lmaps/aw/a;->a(Lmaps/ao/b;Lmaps/ac/bt;)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, Lmaps/ag/h;->a(JLjava/lang/String;)[B

    move-result-object v2

    if-nez v2, :cond_3

    move-object v0, v8

    goto :goto_0

    :cond_3
    :try_start_1
    array-length v0, v2

    if-nez v0, :cond_4

    sget-object v0, Lmaps/ag/w;->a:Lmaps/ac/bs;

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    cmp-long v0, v4, v9

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v11

    sub-long/2addr v4, v11

    iget-object v0, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v11

    add-long/2addr v4, v11

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    move-wide v4, v6

    :cond_5
    const/16 v0, 0x10

    if-le v3, v0, :cond_7

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    cmp-long v9, v0, v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v9

    sub-long/2addr v0, v9

    iget-object v9, p0, Lmaps/ag/w;->k:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v9

    add-long/2addr v0, v9

    cmp-long v9, v0, v6

    if-gez v9, :cond_6

    :goto_1
    iget-object v0, p0, Lmaps/ag/w;->e:Lmaps/ag/ag;

    move-object v1, p1

    invoke-interface/range {v0 .. v7}, Lmaps/ag/ag;->a(Lmaps/ac/bt;[BIJJ)Lmaps/ac/bs;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not unpack tile in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    goto/16 :goto_0

    :cond_6
    move-wide v6, v0

    goto :goto_1

    :cond_7
    move-wide v6, v9

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v0}, Lmaps/ag/h;->a()I

    move-result v0

    return v0
.end method

.method public final d(Lmaps/ac/bt;)[B
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v0

    const/16 v1, 0x15

    if-le v0, v1, :cond_1

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lmaps/ag/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/af;

    if-eqz v0, :cond_7

    iget-object v1, v0, Lmaps/ag/af;->e:[B

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    iget-object v0, p0, Lmaps/ag/w;->i:Lmaps/ao/b;

    invoke-static {v0, p1}, Lmaps/aw/a;->a(Lmaps/ao/b;Lmaps/ac/bt;)Landroid/util/Pair;

    move-result-object v1

    iget-object v3, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0}, Lmaps/ag/h;->a(JLjava/lang/String;)[B

    move-result-object v1

    :cond_2
    if-eqz v1, :cond_3

    array-length v0, v1

    if-nez v0, :cond_4

    :cond_3
    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    array-length v0, v1

    sub-int v4, v0, v3

    if-ltz v4, :cond_5

    const/16 v0, 0x18

    if-le v3, v0, :cond_6

    :cond_5
    const-string v0, "SDCardTileCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid tile data length["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] in "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    :cond_6
    new-array v0, v4, [B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v2, 0x0

    :try_start_2
    invoke-static {v1, v3, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_2
    const-string v2, "SDCardTileCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid tile data in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lmaps/ag/w;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    goto :goto_2

    :cond_7
    move-object v1, v2

    goto/16 :goto_1
.end method

.method public final e()Ljava/util/Locale;
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v0}, Lmaps/ag/h;->c()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized f()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/ag/w;->g:Lmaps/ag/h;

    invoke-virtual {v0}, Lmaps/ag/h;->d()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "shutDown(): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
