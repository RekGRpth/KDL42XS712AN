.class public final Lmaps/ag/ah;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ag/aj;


# static fields
.field private static b:Lmaps/ac/bs;


# instance fields
.field private a:Lmaps/ax/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/ac/aq;

    invoke-direct {v0}, Lmaps/ac/aq;-><init>()V

    sput-object v0, Lmaps/ag/ah;->b:Lmaps/ac/bs;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/ax/f;

    invoke-direct {v0, p1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ag/ah;->a:Lmaps/ax/f;

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/bt;)V
    .locals 1

    sget-object v0, Lmaps/ag/ah;->b:Lmaps/ac/bs;

    invoke-virtual {p0, p1, v0}, Lmaps/ag/ah;->a(Lmaps/ac/bt;Lmaps/ac/bs;)V

    return-void
.end method

.method public final a(Lmaps/ac/bt;Lmaps/ac/bs;)V
    .locals 3

    iget-object v1, p0, Lmaps/ag/ah;->a:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/ah;->a:Lmaps/ax/f;

    new-instance v2, Lmaps/ag/ai;

    invoke-direct {v2, p2}, Lmaps/ag/ai;-><init>(Lmaps/ac/bs;)V

    invoke-virtual {v0, p1, v2}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a()Z
    .locals 2

    iget-object v1, p0, Lmaps/ag/ah;->a:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/ah;->a:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ac/bs;)Z
    .locals 1

    sget-object v0, Lmaps/ag/ah;->b:Lmaps/ac/bs;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lmaps/ac/bt;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/ag/ah;->c(Lmaps/ac/bt;)Lmaps/ac/bs;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lmaps/ac/bt;)Lmaps/ac/bs;
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ag/ah;->a:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ag/ah;->a:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/ai;

    if-eqz v0, :cond_3

    iget-object v3, v0, Lmaps/ag/ai;->b:Lmaps/ac/bs;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lmaps/ag/ai;->b:Lmaps/ac/bs;

    :goto_0
    if-nez v0, :cond_0

    iget-object v1, p0, Lmaps/ag/ah;->a:Lmaps/ax/f;

    invoke-virtual {v1, p1}, Lmaps/ax/f;->d(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v2

    :goto_1
    return-object v0

    :cond_1
    iget-object v3, v0, Lmaps/ag/ai;->a:Ljava/lang/ref/SoftReference;

    if-nez v3, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lmaps/ag/ai;->a:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_3
    monitor-exit v2

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
