.class final Lmaps/ag/n;
.super Ljava/lang/Object;


# instance fields
.field final a:J

.field final b:I

.field final c:I

.field final d:I

.field final e:I

.field final f:I

.field final g:I

.field final h:I


# direct methods
.method constructor <init>(JIIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lmaps/ag/n;->a:J

    iput p3, p0, Lmaps/ag/n;->b:I

    iput p6, p0, Lmaps/ag/n;->c:I

    iput p4, p0, Lmaps/ag/n;->d:I

    iput p5, p0, Lmaps/ag/n;->e:I

    iput p7, p0, Lmaps/ag/n;->f:I

    iput p8, p0, Lmaps/ag/n;->g:I

    iput p9, p0, Lmaps/ag/n;->h:I

    return-void
.end method

.method static a([BIII)Lmaps/ag/n;
    .locals 10

    invoke-static {p0, p1}, Lmaps/ag/h;->c([BI)J

    move-result-wide v1

    add-int/lit8 v0, p1, 0x8

    invoke-static {p0, v0}, Lmaps/ag/h;->a([BI)I

    move-result v4

    add-int/lit8 v0, v0, 0x4

    invoke-static {p0, v0}, Lmaps/ag/h;->a([BI)I

    move-result v5

    add-int/lit8 v0, v0, 0x4

    invoke-static {p0, v0}, Lmaps/ag/h;->a([BI)I

    move-result v7

    ushr-int/lit8 v3, v4, 0x5

    and-int/lit8 v6, v4, 0x1f

    ushr-int/lit8 v4, v5, 0x18

    const v0, 0xffffff

    and-int/2addr v5, v0

    new-instance v0, Lmaps/ag/n;

    move v8, p2

    move v9, p3

    invoke-direct/range {v0 .. v9}, Lmaps/ag/n;-><init>(JIIIIIII)V

    return-object v0
.end method

.method static b([BI)I
    .locals 1

    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-static {p0, v0}, Lmaps/ag/h;->a([BI)I

    move-result v0

    ushr-int/lit8 v0, v0, 0x18

    return v0
.end method

.method static c([BI)I
    .locals 2

    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-static {p0, v0}, Lmaps/ag/h;->a([BI)I

    move-result v0

    const v1, 0xffffff

    and-int/2addr v0, v1

    return v0
.end method

.method static d([BI)I
    .locals 1

    add-int/lit8 v0, p1, 0x8

    invoke-static {p0, v0}, Lmaps/ag/h;->a([BI)I

    move-result v0

    and-int/lit8 v0, v0, 0x1f

    return v0
.end method


# virtual methods
.method final a([BI)V
    .locals 4

    iget-wide v0, p0, Lmaps/ag/n;->a:J

    invoke-static {p1, p2, v0, v1}, Lmaps/ag/h;->a([BIJ)V

    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lmaps/ag/n;->b:I

    shl-int/lit8 v1, v1, 0x5

    iget v2, p0, Lmaps/ag/n;->c:I

    or-int/2addr v1, v2

    ushr-int/lit8 v2, v1, 0x5

    iget v3, p0, Lmaps/ag/n;->b:I

    if-eq v2, v3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not pack data offset of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmaps/ag/n;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lmaps/ag/n;->c:I

    if-eq v2, v3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not pack refCount of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmaps/ag/n;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1, v0, v1}, Lmaps/ag/h;->a([BII)V

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lmaps/ag/n;->d:I

    shl-int/lit8 v1, v1, 0x18

    iget v2, p0, Lmaps/ag/n;->e:I

    or-int/2addr v1, v2

    invoke-static {p1, v0, v1}, Lmaps/ag/h;->a([BII)V

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lmaps/ag/n;->f:I

    invoke-static {p1, v0, v1}, Lmaps/ag/h;->a([BII)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/ag/n;

    iget v2, p0, Lmaps/ag/n;->g:I

    iget v3, p1, Lmaps/ag/n;->g:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lmaps/ag/n;->h:I

    iget v3, p1, Lmaps/ag/n;->h:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lmaps/ag/n;->g:I

    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lmaps/ag/n;->h:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ID:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lmaps/ag/n;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Off:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/n;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " KeyLen:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/n;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DataLen:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/n;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Checksum:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/n;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Shard:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/n;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ShardIndex:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/n;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
