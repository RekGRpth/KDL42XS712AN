.class public final Lmaps/q/o;
.super Lmaps/m/al;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private transient a:Lmaps/m/bo;

.field private synthetic b:Lmaps/q/f;


# direct methods
.method constructor <init>(Lmaps/q/f;)V
    .locals 0

    iput-object p1, p0, Lmaps/q/o;->b:Lmaps/q/f;

    invoke-direct {p0}, Lmaps/m/al;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lmaps/q/o;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/q/o;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Ljava/util/Set;
    .locals 2

    iget-object v0, p0, Lmaps/q/o;->a:Lmaps/m/bo;

    if-nez v0, :cond_0

    sget-object v0, Lmaps/q/h;->a:Lmaps/q/h;

    iget-object v1, p0, Lmaps/q/o;->b:Lmaps/q/f;

    invoke-virtual {v0, v1}, Lmaps/q/h;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/ah;->a(Ljava/lang/Iterable;)Lmaps/m/ah;

    move-result-object v0

    sget-object v1, Lmaps/q/l;->a:Lmaps/q/l;

    invoke-virtual {v0, v1}, Lmaps/m/ah;->a(Lmaps/k/p;)Lmaps/m/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/ah;->a()Lmaps/m/bo;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/o;->a:Lmaps/m/bo;

    :cond_0
    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 2

    sget-object v0, Lmaps/q/h;->b:Lmaps/q/h;

    iget-object v1, p0, Lmaps/q/o;->b:Lmaps/q/f;

    invoke-static {v1}, Lmaps/q/f;->a(Lmaps/q/f;)Lmaps/m/bo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/h;->a(Ljava/lang/Iterable;)Lmaps/m/ay;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/bo;->a(Ljava/util/Collection;)Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method
