.class abstract enum Lmaps/q/l;
.super Ljava/lang/Enum;

# interfaces
.implements Lmaps/k/p;


# static fields
.field public static final enum a:Lmaps/q/l;

.field private static enum b:Lmaps/q/l;

.field private static final synthetic c:[Lmaps/q/l;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/q/m;

    const-string v1, "IGNORE_TYPE_VARIABLE_OR_WILDCARD"

    invoke-direct {v0, v1}, Lmaps/q/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/q/l;->a:Lmaps/q/l;

    new-instance v0, Lmaps/q/n;

    const-string v1, "INTERFACE_ONLY"

    invoke-direct {v0, v1}, Lmaps/q/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/q/l;->b:Lmaps/q/l;

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/q/l;

    const/4 v1, 0x0

    sget-object v2, Lmaps/q/l;->a:Lmaps/q/l;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lmaps/q/l;->b:Lmaps/q/l;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/q/l;->c:[Lmaps/q/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/q/l;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/q/l;
    .locals 1

    const-class v0, Lmaps/q/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/q/l;

    return-object v0
.end method

.method public static values()[Lmaps/q/l;
    .locals 1

    sget-object v0, Lmaps/q/l;->c:[Lmaps/q/l;

    invoke-virtual {v0}, [Lmaps/q/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/q/l;

    return-object v0
.end method
