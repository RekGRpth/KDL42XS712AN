.class abstract enum Lmaps/q/r;
.super Ljava/lang/Enum;


# static fields
.field static final a:Lmaps/q/r;

.field private static enum b:Lmaps/q/r;

.field private static enum c:Lmaps/q/r;

.field private static final synthetic d:[Lmaps/q/r;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v1, 0x0

    new-instance v0, Lmaps/q/s;

    const-string v2, "OWNED_BY_ENCLOSING_CLASS"

    invoke-direct {v0, v2}, Lmaps/q/s;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/q/r;->b:Lmaps/q/r;

    new-instance v0, Lmaps/q/u;

    const-string v2, "LOCAL_CLASS_HAS_NO_OWNER"

    invoke-direct {v0, v2}, Lmaps/q/u;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/q/r;->c:Lmaps/q/r;

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/q/r;

    sget-object v2, Lmaps/q/r;->b:Lmaps/q/r;

    aput-object v2, v0, v1

    const/4 v2, 0x1

    sget-object v3, Lmaps/q/r;->c:Lmaps/q/r;

    aput-object v3, v0, v2

    sput-object v0, Lmaps/q/r;->d:[Lmaps/q/r;

    new-instance v0, Lmaps/q/v;

    invoke-direct {v0}, Lmaps/q/v;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-static {}, Lmaps/q/r;->values()[Lmaps/q/r;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    const-class v5, Lmaps/q/t;

    invoke-virtual {v4, v5}, Lmaps/q/r;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v5

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v6

    if-ne v5, v6, :cond_0

    sput-object v4, Lmaps/q/r;->a:Lmaps/q/r;

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/q/r;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/q/r;
    .locals 1

    const-class v0, Lmaps/q/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/q/r;

    return-object v0
.end method

.method public static values()[Lmaps/q/r;
    .locals 1

    sget-object v0, Lmaps/q/r;->d:[Lmaps/q/r;

    invoke-virtual {v0}, [Lmaps/q/r;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/q/r;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Class;)Ljava/lang/Class;
.end method
