.class Lmaps/ao/p;
.super Lmaps/ao/b;


# instance fields
.field private final I:Z


# direct methods
.method private constructor <init>(Lmaps/ao/q;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/ao/b;-><init>(Lmaps/ao/e;B)V

    invoke-static {p1}, Lmaps/ao/q;->a(Lmaps/ao/q;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ao/p;->I:Z

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ao/q;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ao/p;-><init>(Lmaps/ao/q;)V

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    const/16 v0, 0x1000

    return v0
.end method

.method public final a(Lmaps/bn/k;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lmaps/ae/y;
    .locals 9

    const/4 v8, 0x0

    sget-object v0, Lmaps/ao/p;->n:Lmaps/ao/b;

    if-ne p0, v0, :cond_1

    invoke-static {}, Lmaps/ba/f;->g()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v8

    :cond_1
    iget-boolean v0, p0, Lmaps/ao/b;->D:Z

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    :goto_1
    const/16 v0, 0x100

    invoke-static {p2, v0}, Lmaps/ay/ap;->a(Landroid/content/res/Resources;I)I

    move-result v3

    sget-object v0, Lmaps/ao/b;->u:Lmaps/ao/b;

    if-eq p0, v0, :cond_0

    if-eqz p5, :cond_3

    iget-boolean v0, p0, Lmaps/ao/b;->E:Z

    if-eqz v0, :cond_3

    const/4 v6, 0x1

    :goto_2
    iget-boolean v0, p0, Lmaps/ao/b;->A:Z

    if-eqz v0, :cond_4

    new-instance v0, Lmaps/ae/ac;

    sget-object v8, Lmaps/ae/ac;->a:Lmaps/ae/ad;

    move-object v1, p1

    move-object v2, p0

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lmaps/ae/ac;-><init>(Lmaps/bn/k;Lmaps/ao/b;IFLjava/util/Locale;ZLjava/io/File;Lmaps/ae/ad;)V

    move-object v8, v0

    goto :goto_0

    :cond_2
    const/high16 v4, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    :cond_4
    new-instance v0, Lmaps/ae/ae;

    move-object v1, p1

    move-object v2, p0

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lmaps/ae/ae;-><init>(Lmaps/bn/k;Lmaps/ao/b;IFLjava/util/Locale;ZLjava/io/File;Lmaps/ag/g;)V

    sget-object v1, Lmaps/ao/b;->f:Lmaps/ao/b;

    if-eq p0, v1, :cond_5

    sget-object v1, Lmaps/ao/b;->g:Lmaps/ao/b;

    if-ne p0, v1, :cond_6

    :cond_5
    invoke-virtual {v0}, Lmaps/ae/ae;->g()V

    :cond_6
    iget-boolean v1, p0, Lmaps/ao/p;->I:Z

    if-eqz v1, :cond_7

    if-eqz p6, :cond_7

    invoke-static {}, Lmaps/ba/h;->a()Lmaps/ba/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ae/ae;->a(Lmaps/ba/h;)V

    :cond_7
    move-object v8, v0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/ao/b;

    invoke-super {p0, p1}, Lmaps/ao/b;->a(Lmaps/ao/b;)I

    move-result v0

    return v0
.end method

.method public final i()Lmaps/ag/ag;
    .locals 1

    new-instance v0, Lmaps/ao/r;

    invoke-direct {v0, p0}, Lmaps/ao/r;-><init>(Lmaps/ao/b;)V

    return-object v0
.end method
