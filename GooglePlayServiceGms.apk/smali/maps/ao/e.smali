.class abstract Lmaps/ao/e;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method private constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lmaps/ao/e;->b:I

    const-string v0, ""

    iput-object v0, p0, Lmaps/ao/e;->c:Ljava/lang/String;

    iput-boolean v1, p0, Lmaps/ao/e;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ao/e;->e:Z

    iput-boolean v1, p0, Lmaps/ao/e;->f:Z

    iput-boolean v1, p0, Lmaps/ao/e;->g:Z

    iput p1, p0, Lmaps/ao/e;->a:I

    return-void
.end method

.method synthetic constructor <init>(IB)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ao/e;-><init>(I)V

    return-void
.end method

.method static synthetic a(Lmaps/ao/e;)I
    .locals 1

    iget v0, p0, Lmaps/ao/e;->a:I

    return v0
.end method

.method static synthetic b(Lmaps/ao/e;)I
    .locals 1

    iget v0, p0, Lmaps/ao/e;->b:I

    return v0
.end method

.method static synthetic c(Lmaps/ao/e;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ao/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lmaps/ao/e;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ao/e;->d:Z

    return v0
.end method

.method static synthetic e(Lmaps/ao/e;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ao/e;->e:Z

    return v0
.end method

.method static synthetic f(Lmaps/ao/e;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ao/e;->f:Z

    return v0
.end method

.method static synthetic g(Lmaps/ao/e;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ao/e;->g:Z

    return v0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lmaps/ao/e;
    .locals 0

    iput-object p1, p0, Lmaps/ao/e;->c:Ljava/lang/String;

    return-object p0
.end method

.method final a(Z)Lmaps/ao/e;
    .locals 0

    iput-boolean p1, p0, Lmaps/ao/e;->g:Z

    return-object p0
.end method

.method abstract b()Lmaps/ao/b;
.end method

.method final c()Lmaps/ao/e;
    .locals 1

    const/16 v0, 0x80

    iput v0, p0, Lmaps/ao/e;->b:I

    return-object p0
.end method

.method final d()Lmaps/ao/e;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ao/e;->d:Z

    return-object p0
.end method

.method final e()Lmaps/ao/e;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ao/e;->e:Z

    return-object p0
.end method

.method final f()Lmaps/ao/e;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ao/e;->f:Z

    return-object p0
.end method
