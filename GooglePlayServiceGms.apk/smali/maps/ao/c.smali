.class final Lmaps/ao/c;
.super Lmaps/ao/p;


# instance fields
.field private final I:Z


# direct methods
.method private constructor <init>(Lmaps/ao/d;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/ao/p;-><init>(Lmaps/ao/q;B)V

    invoke-static {p1}, Lmaps/ao/d;->a(Lmaps/ao/d;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ao/c;->I:Z

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ao/d;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ao/c;-><init>(Lmaps/ao/d;)V

    return-void
.end method


# virtual methods
.method public final a(ILmaps/ap/b;)I
    .locals 1

    iget-boolean v0, p0, Lmaps/ao/c;->I:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ap/b;->b:Lmaps/ap/b;

    if-ne p2, v0, :cond_2

    :cond_0
    sget-object v0, Lmaps/ap/b;->e:Lmaps/ap/b;

    if-eq p2, v0, :cond_1

    sget-object v0, Lmaps/ap/b;->d:Lmaps/ap/b;

    if-ne p2, v0, :cond_3

    :cond_1
    const/4 p1, 0x0

    :cond_2
    :goto_0
    return p1

    :cond_3
    sget-object v0, Lmaps/ap/b;->a:Lmaps/ap/b;

    if-eq p2, v0, :cond_2

    sget-object v0, Lmaps/ap/b;->c:Lmaps/ap/b;

    if-eq p2, v0, :cond_2

    and-int/lit16 p1, p1, -0x1a07

    goto :goto_0
.end method

.method public final a(Lmaps/ac/bd;)Lmaps/ac/av;
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmaps/ac/bd;->a(I)Lmaps/ac/av;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x3e99999a    # 0.3f

    invoke-interface {p1, v1, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    return-void
.end method

.method public final e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final g()Lmaps/au/ak;
    .locals 3

    new-instance v0, Lmaps/au/ak;

    sget-object v1, Lmaps/au/aj;->f:Lmaps/au/aj;

    sget-object v2, Lmaps/au/s;->b:Lmaps/au/s;

    invoke-direct {v0, v1, v2}, Lmaps/au/ak;-><init>(Lmaps/au/aj;Lmaps/au/s;)V

    return-object v0
.end method

.method public final h()Lmaps/ac/bl;
    .locals 1

    sget-object v0, Lmaps/ao/b;->F:Lmaps/ac/bl;

    return-object v0
.end method
