.class public abstract Lmaps/ao/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field static final F:Lmaps/ac/bl;

.field static final G:Lmaps/ac/bl;

.field static final H:Lmaps/ac/bl;

.field private static final I:Ljava/util/Map;

.field private static final M:Lmaps/ac/bk;

.field private static final N:Lmaps/ac/bq;

.field private static final O:Lmaps/ac/bq;

.field private static final P:Lmaps/ac/bq;

.field private static final Q:Lmaps/ac/br;

.field public static final a:Lmaps/ao/b;

.field public static final b:Lmaps/ao/b;

.field public static final c:Lmaps/ao/b;

.field public static final d:Lmaps/ao/b;

.field public static final e:Lmaps/ao/b;

.field public static final f:Lmaps/ao/b;

.field public static final g:Lmaps/ao/b;

.field public static final h:Lmaps/ao/b;

.field public static final i:Lmaps/ao/b;

.field public static final j:Lmaps/ao/b;

.field public static final k:Lmaps/ao/b;

.field public static final l:Lmaps/ao/b;

.field public static final m:Lmaps/ao/b;

.field public static final n:Lmaps/ao/b;

.field public static final o:Lmaps/ao/b;

.field public static final p:Lmaps/ao/b;

.field public static final q:Lmaps/ao/b;

.field public static final r:Lmaps/ao/b;

.field public static final s:Lmaps/ao/b;

.field public static final t:Lmaps/ao/b;

.field public static final u:Lmaps/ao/b;

.field public static final v:Lmaps/ao/b;

.field public static final w:Lmaps/ao/b;

.field public static final x:Lmaps/ao/b;


# instance fields
.field public final A:Z

.field public final B:I

.field public final C:Ljava/lang/String;

.field final D:Z

.field final E:Z

.field private final J:I

.field private final K:Z

.field private final L:Lmaps/ag/ag;

.field public final y:I

.field public final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/16 v10, 0xc

    const/4 v1, -0x1

    const/4 v9, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmaps/ao/b;->I:Ljava/util/Map;

    new-instance v0, Lmaps/ao/d;

    const/16 v3, 0xa

    invoke-direct {v0, v3, v2}, Lmaps/ao/d;-><init>(IB)V

    invoke-virtual {v0}, Lmaps/ao/d;->g()Lmaps/ao/q;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/q;->f()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->a:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/d;

    const/16 v3, 0x16

    invoke-direct {v0, v3, v2}, Lmaps/ao/d;-><init>(IB)V

    invoke-virtual {v0}, Lmaps/ao/d;->g()Lmaps/ao/q;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/q;->f()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->b:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/d;

    const/16 v3, 0x14

    invoke-direct {v0, v3, v2}, Lmaps/ao/d;-><init>(IB)V

    invoke-virtual {v0}, Lmaps/ao/d;->g()Lmaps/ao/q;

    move-result-object v0

    const-string v3, "_tran_base"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->f()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->c:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/g;

    const/4 v3, 0x3

    invoke-direct {v0, v3, v2}, Lmaps/ao/g;-><init>(IB)V

    invoke-virtual {v0}, Lmaps/ao/g;->e()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->d:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/g;

    invoke-direct {v0, v10, v2}, Lmaps/ao/g;-><init>(IB)V

    const-string v3, "_ter"

    invoke-virtual {v0, v3}, Lmaps/ao/g;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->e()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->e:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/4 v3, 0x4

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->f:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0x17

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->g:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/l;

    invoke-direct {v0, v2}, Lmaps/ao/l;-><init>(B)V

    invoke-virtual {v0}, Lmaps/ao/l;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->h:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/n;

    invoke-direct {v0, v2}, Lmaps/ao/n;-><init>(B)V

    invoke-virtual {v0, v4}, Lmaps/ao/n;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->i:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/d;

    const/16 v3, 0x12

    invoke-direct {v0, v3, v2}, Lmaps/ao/d;-><init>(IB)V

    invoke-virtual {v0}, Lmaps/ao/d;->a()Lmaps/ao/d;

    move-result-object v0

    const-string v3, "_vec_bic"

    invoke-virtual {v0, v3}, Lmaps/ao/d;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->j:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/g;

    const/4 v3, 0x7

    invoke-direct {v0, v3, v2}, Lmaps/ao/g;-><init>(IB)V

    invoke-virtual {v0}, Lmaps/ao/g;->c()Lmaps/ao/e;

    move-result-object v0

    const-string v3, "_ter_bic"

    invoke-virtual {v0, v3}, Lmaps/ao/e;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->k:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/g;

    const/4 v3, 0x6

    invoke-direct {v0, v3, v2}, Lmaps/ao/g;-><init>(IB)V

    invoke-virtual {v0}, Lmaps/ao/g;->c()Lmaps/ao/e;

    move-result-object v0

    const-string v3, "_hy_bic"

    invoke-virtual {v0, v3}, Lmaps/ao/e;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->l:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0xd

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_tran"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->m:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0xe

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    invoke-virtual {v0, v4}, Lmaps/ao/q;->a(Z)Lmaps/ao/e;

    move-result-object v0

    const-string v3, "_inaka"

    invoke-virtual {v0, v3}, Lmaps/ao/e;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->n:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/j;

    const/16 v3, 0xf

    invoke-direct {v0, v3, v2}, Lmaps/ao/j;-><init>(IB)V

    const-string v3, "_labl"

    invoke-virtual {v0, v3}, Lmaps/ao/j;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->f()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->o:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/j;

    const/16 v3, 0x15

    invoke-direct {v0, v3, v2}, Lmaps/ao/j;-><init>(IB)V

    const-string v3, "_tran_labl"

    invoke-virtual {v0, v3}, Lmaps/ao/j;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->f()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->p:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0x10

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_psm"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->d()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->q:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0x11

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_related"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->d()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->r:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0x18

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_high"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->d()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->s:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0x19

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_api"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->t:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    invoke-direct {v0, v2, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_star"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->d()Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->u:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0x1a

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_spotlight"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->v:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/q;

    const/16 v3, 0x1b

    invoke-direct {v0, v3, v2}, Lmaps/ao/q;-><init>(IB)V

    const-string v3, "_maps_engine_vector"

    invoke-virtual {v0, v3}, Lmaps/ao/q;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->w:Lmaps/ao/b;

    new-instance v0, Lmaps/ao/g;

    const/16 v3, 0x1c

    invoke-direct {v0, v3, v2}, Lmaps/ao/g;-><init>(IB)V

    const-string v3, "_maps_engine_image"

    invoke-virtual {v0, v3}, Lmaps/ao/g;->a(Ljava/lang/String;)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/ao/e;->a(Z)Lmaps/ao/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/e;->b()Lmaps/ao/b;

    move-result-object v0

    sput-object v0, Lmaps/ao/b;->x:Lmaps/ao/b;

    new-instance v0, Lmaps/ac/bk;

    const/4 v3, 0x0

    new-array v4, v2, [I

    invoke-direct {v0, v2, v3, v4, v2}, Lmaps/ac/bk;-><init>(IF[II)V

    sput-object v0, Lmaps/ao/b;->M:Lmaps/ac/bk;

    new-instance v0, Lmaps/ac/bq;

    const/high16 v3, -0x1000000

    sget-object v4, Lmaps/ao/b;->M:Lmaps/ac/bk;

    invoke-direct {v0, v3, v4}, Lmaps/ac/bq;-><init>(ILmaps/ac/bk;)V

    sput-object v0, Lmaps/ao/b;->N:Lmaps/ac/bq;

    new-instance v0, Lmaps/ac/bq;

    const v3, -0xffff01

    sget-object v4, Lmaps/ao/b;->M:Lmaps/ac/bk;

    invoke-direct {v0, v3, v4}, Lmaps/ac/bq;-><init>(ILmaps/ac/bk;)V

    sput-object v0, Lmaps/ao/b;->O:Lmaps/ac/bq;

    new-instance v0, Lmaps/ac/bq;

    const/high16 v3, -0x10000

    sget-object v4, Lmaps/ao/b;->M:Lmaps/ac/bk;

    invoke-direct {v0, v3, v4}, Lmaps/ac/bq;-><init>(ILmaps/ac/bk;)V

    sput-object v0, Lmaps/ao/b;->P:Lmaps/ac/bq;

    new-instance v0, Lmaps/ac/br;

    const/16 v3, 0xa

    const v4, 0x3f99999a    # 1.2f

    const/high16 v5, 0x3f800000    # 1.0f

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lmaps/ac/br;-><init>(IIIFFI)V

    sput-object v0, Lmaps/ao/b;->Q:Lmaps/ac/br;

    new-instance v0, Lmaps/ac/bl;

    sget-object v5, Lmaps/ao/b;->Q:Lmaps/ac/br;

    sget-object v6, Lmaps/ao/b;->N:Lmaps/ac/bq;

    move v2, v10

    move-object v3, v9

    move-object v4, v9

    move-object v7, v9

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lmaps/ac/bl;-><init>(II[I[Lmaps/ac/bk;Lmaps/ac/br;Lmaps/ac/bq;Lmaps/ac/bk;Lmaps/ac/x;)V

    sput-object v0, Lmaps/ao/b;->F:Lmaps/ac/bl;

    new-instance v0, Lmaps/ac/bl;

    sget-object v5, Lmaps/ao/b;->Q:Lmaps/ac/br;

    sget-object v6, Lmaps/ao/b;->P:Lmaps/ac/bq;

    move v2, v10

    move-object v3, v9

    move-object v4, v9

    move-object v7, v9

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lmaps/ac/bl;-><init>(II[I[Lmaps/ac/bk;Lmaps/ac/br;Lmaps/ac/bq;Lmaps/ac/bk;Lmaps/ac/x;)V

    sput-object v0, Lmaps/ao/b;->G:Lmaps/ac/bl;

    new-instance v0, Lmaps/ac/bl;

    sget-object v5, Lmaps/ao/b;->Q:Lmaps/ac/br;

    sget-object v6, Lmaps/ao/b;->O:Lmaps/ac/bq;

    move v2, v10

    move-object v3, v9

    move-object v4, v9

    move-object v7, v9

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lmaps/ac/bl;-><init>(II[I[Lmaps/ac/bk;Lmaps/ac/br;Lmaps/ac/bq;Lmaps/ac/bk;Lmaps/ac/x;)V

    sput-object v0, Lmaps/ao/b;->H:Lmaps/ac/bl;

    return-void
.end method

.method private constructor <init>(Lmaps/ao/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/ao/e;->a(Lmaps/ao/e;)I

    move-result v0

    iput v0, p0, Lmaps/ao/b;->y:I

    invoke-static {p1}, Lmaps/ao/e;->b(Lmaps/ao/e;)I

    move-result v0

    iput v0, p0, Lmaps/ao/b;->z:I

    invoke-static {p1}, Lmaps/ao/e;->c(Lmaps/ao/e;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/ao/b;->C:Ljava/lang/String;

    invoke-static {p1}, Lmaps/ao/e;->d(Lmaps/ao/e;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ao/b;->A:Z

    invoke-static {p1}, Lmaps/ao/e;->e(Lmaps/ao/e;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ao/b;->D:Z

    invoke-static {p1}, Lmaps/ao/e;->f(Lmaps/ao/e;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ao/b;->E:Z

    invoke-static {p1}, Lmaps/ao/e;->g(Lmaps/ao/e;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ao/b;->K:Z

    iget-boolean v0, p0, Lmaps/ao/b;->K:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ao/b;->i()Lmaps/ag/ag;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lmaps/ao/b;->L:Lmaps/ag/ag;

    iget v0, p0, Lmaps/ao/b;->y:I

    iget v1, p0, Lmaps/ao/b;->z:I

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ao/b;->B:I

    sget-object v0, Lmaps/ao/b;->I:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lmaps/ao/b;->J:I

    iget v0, p0, Lmaps/ao/b;->y:I

    iget v1, p0, Lmaps/ao/b;->z:I

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lmaps/ao/b;->I:Ljava/util/Map;

    invoke-interface {v1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lmaps/ao/e;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ao/b;-><init>(Lmaps/ao/e;)V

    return-void
.end method

.method public static a(I)Lmaps/ao/b;
    .locals 2

    sget-object v0, Lmaps/ao/b;->I:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ao/b;

    return-object v0
.end method

.method public static c()Ljava/lang/Iterable;
    .locals 1

    sget-object v0, Lmaps/ao/b;->I:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract a()I
.end method

.method public a(ILmaps/ap/b;)I
    .locals 0

    return p1
.end method

.method public final a(Lmaps/ao/b;)I
    .locals 2

    iget v0, p0, Lmaps/ao/b;->J:I

    iget v1, p1, Lmaps/ao/b;->J:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Lmaps/ac/bd;)Lmaps/ac/av;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Lmaps/bn/k;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lmaps/ae/y;
.end method

.method public final a(Ljava/lang/String;ZLmaps/ag/g;)Lmaps/ag/f;
    .locals 6

    iget-boolean v0, p0, Lmaps/ao/b;->K:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/ag/w;

    if-eqz p2, :cond_1

    const/4 v2, -0x1

    :goto_1
    iget-object v3, p0, Lmaps/ao/b;->L:Lmaps/ag/ag;

    move-object v1, p1

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lmaps/ag/w;-><init>(Ljava/lang/String;ILmaps/ag/ag;Lmaps/ao/b;Lmaps/ag/g;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmaps/ao/b;->a()I

    move-result v2

    goto :goto_1
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 0

    return-void
.end method

.method public b()Lmaps/ag/aj;
    .locals 3

    new-instance v0, Lmaps/ag/ah;

    invoke-static {}, Lmaps/ap/p;->f()I

    move-result v1

    shr-int/lit8 v1, v1, 0x3

    const/16 v2, 0x100

    mul-int/lit8 v1, v1, 0x20

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/16 v2, 0x40

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v0, v1}, Lmaps/ag/ah;-><init>(I)V

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/ao/b;

    invoke-virtual {p0, p1}, Lmaps/ao/b;->a(Lmaps/ao/b;)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lmaps/ao/b;->J:I

    return v0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public g()Lmaps/au/ak;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Lmaps/ac/bl;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method abstract i()Lmaps/ag/ag;
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    :try_start_0
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-ne p0, v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "unknown"

    goto :goto_1
.end method
