.class public final Lmaps/ac/au;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/ac/av;

.field private b:F

.field private c:I

.field private d:Lmaps/ac/av;

.field private e:F

.field private f:Z

.field private g:Lmaps/ac/ad;

.field private h:Z

.field private i:F

.field private j:Z

.field private k:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lmaps/ac/au;->l()V

    return-void
.end method

.method public constructor <init>(Lmaps/ac/av;FI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2, p3}, Lmaps/ac/au;->a(Lmaps/ac/av;FI)V

    return-void
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput-object v3, p0, Lmaps/ac/au;->a:Lmaps/ac/av;

    iput v1, p0, Lmaps/ac/au;->b:F

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ac/au;->c:I

    iput-object v3, p0, Lmaps/ac/au;->d:Lmaps/ac/av;

    iput v1, p0, Lmaps/ac/au;->e:F

    iput-boolean v2, p0, Lmaps/ac/au;->f:Z

    iput-object v3, p0, Lmaps/ac/au;->g:Lmaps/ac/ad;

    iput-boolean v2, p0, Lmaps/ac/au;->h:Z

    iput v1, p0, Lmaps/ac/au;->i:F

    iput-boolean v2, p0, Lmaps/ac/au;->j:Z

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmaps/ac/au;->k:F

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ac/au;->a:Lmaps/ac/av;

    return-object v0
.end method

.method public final a(F)V
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lmaps/ac/au;->k:F

    return-void
.end method

.method public final a(Lmaps/ac/au;)V
    .locals 3

    if-nez p1, :cond_0

    invoke-direct {p0}, Lmaps/ac/au;->l()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lmaps/ac/au;->a:Lmaps/ac/av;

    iget v1, p1, Lmaps/ac/au;->b:F

    iget v2, p1, Lmaps/ac/au;->c:I

    invoke-virtual {p0, v0, v1, v2}, Lmaps/ac/au;->a(Lmaps/ac/av;FI)V

    iget-object v0, p1, Lmaps/ac/au;->d:Lmaps/ac/av;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lmaps/ac/au;->d:Lmaps/ac/av;

    iget v0, p1, Lmaps/ac/au;->e:F

    iput v0, p0, Lmaps/ac/au;->e:F

    iget-boolean v0, p1, Lmaps/ac/au;->f:Z

    iput-boolean v0, p0, Lmaps/ac/au;->f:Z

    iget-object v0, p1, Lmaps/ac/au;->g:Lmaps/ac/ad;

    iput-object v0, p0, Lmaps/ac/au;->g:Lmaps/ac/ad;

    iget-boolean v0, p1, Lmaps/ac/au;->h:Z

    iput-boolean v0, p0, Lmaps/ac/au;->h:Z

    iget v0, p1, Lmaps/ac/au;->i:F

    iput v0, p0, Lmaps/ac/au;->i:F

    iget-boolean v0, p1, Lmaps/ac/au;->j:Z

    iput-boolean v0, p0, Lmaps/ac/au;->j:Z

    iget v0, p1, Lmaps/ac/au;->k:F

    iput v0, p0, Lmaps/ac/au;->k:F

    goto :goto_0

    :cond_1
    new-instance v0, Lmaps/ac/av;

    iget-object v1, p1, Lmaps/ac/au;->d:Lmaps/ac/av;

    invoke-direct {v0, v1}, Lmaps/ac/av;-><init>(Lmaps/ac/av;)V

    goto :goto_1
.end method

.method public final a(Lmaps/ac/av;)V
    .locals 0

    iput-object p1, p0, Lmaps/ac/au;->d:Lmaps/ac/av;

    return-void
.end method

.method public final a(Lmaps/ac/av;FI)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lmaps/ac/au;->a:Lmaps/ac/av;

    iput p2, p0, Lmaps/ac/au;->b:F

    iput p3, p0, Lmaps/ac/au;->c:I

    return-void

    :cond_0
    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0, p1}, Lmaps/ac/av;-><init>(Lmaps/ac/av;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/ac/au;->f:Z

    return-void
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/ac/au;->b:F

    return v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lmaps/ac/au;->c:I

    return v0
.end method

.method public final d()Lmaps/ac/av;
    .locals 2

    iget-object v0, p0, Lmaps/ac/au;->d:Lmaps/ac/av;

    iget-object v1, p0, Lmaps/ac/au;->a:Lmaps/ac/av;

    invoke-static {v0, v1}, Lmaps/k/j;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/av;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ac/au;->f:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/ac/au;

    iget-object v2, p0, Lmaps/ac/au;->a:Lmaps/ac/av;

    iget-object v3, p1, Lmaps/ac/au;->a:Lmaps/ac/av;

    invoke-static {v2, v3}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lmaps/ac/au;->b:F

    iget v3, p1, Lmaps/ac/au;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lmaps/ac/au;->c:I

    iget v3, p1, Lmaps/ac/au;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lmaps/ac/au;->d:Lmaps/ac/av;

    iget-object v3, p1, Lmaps/ac/au;->d:Lmaps/ac/av;

    invoke-static {v2, v3}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lmaps/ac/au;->e:F

    iget v3, p1, Lmaps/ac/au;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lmaps/ac/au;->f:Z

    iget-boolean v3, p1, Lmaps/ac/au;->f:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lmaps/ac/au;->g:Lmaps/ac/ad;

    iget-object v3, p1, Lmaps/ac/au;->g:Lmaps/ac/ad;

    invoke-static {v2, v3}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lmaps/ac/au;->h:Z

    iget-boolean v3, p1, Lmaps/ac/au;->h:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lmaps/ac/au;->i:F

    iget v3, p1, Lmaps/ac/au;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lmaps/ac/au;->j:Z

    iget-boolean v3, p1, Lmaps/ac/au;->j:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lmaps/ac/au;->k:F

    iget v3, p1, Lmaps/ac/au;->k:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final f()Lmaps/ac/ad;
    .locals 1

    iget-object v0, p0, Lmaps/ac/au;->g:Lmaps/ac/ad;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ac/au;->h:Z

    return v0
.end method

.method public final h()F
    .locals 1

    iget v0, p0, Lmaps/ac/au;->i:F

    return v0
.end method

.method public final hashCode()I
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ac/au;->a:Lmaps/ac/av;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lmaps/ac/au;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lmaps/ac/au;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lmaps/ac/au;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lmaps/ac/au;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lmaps/ac/au;->g:Lmaps/ac/ad;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lmaps/ac/au;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lmaps/ac/au;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lmaps/ac/au;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lmaps/ac/au;->k:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ac/au;->j:Z

    return v0
.end method

.method public final j()F
    .locals 1

    iget v0, p0, Lmaps/ac/au;->k:F

    return v0
.end method

.method public final k()Z
    .locals 1

    iget-object v0, p0, Lmaps/ac/au;->a:Lmaps/ac/av;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "@"

    iget-object v2, p0, Lmaps/ac/au;->a:Lmaps/ac/av;

    invoke-virtual {v2}, Lmaps/ac/av;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    const-string v1, "Accuracy"

    iget v2, p0, Lmaps/ac/au;->c:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    iget-object v1, p0, Lmaps/ac/au;->d:Lmaps/ac/av;

    if-eqz v1, :cond_0

    const-string v1, "Accuracy point"

    iget-object v2, p0, Lmaps/ac/au;->d:Lmaps/ac/av;

    invoke-virtual {v2}, Lmaps/ac/av;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    :cond_0
    const-string v1, "Accuracy emphasis"

    iget v2, p0, Lmaps/ac/au;->e:F

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;F)Lmaps/k/k;

    const-string v1, "Use bearing"

    iget-boolean v2, p0, Lmaps/ac/au;->f:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    iget-boolean v1, p0, Lmaps/ac/au;->f:Z

    if-eqz v1, :cond_1

    const-string v1, "Bearing"

    iget v2, p0, Lmaps/ac/au;->b:F

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;F)Lmaps/k/k;

    :cond_1
    const-string v1, "Brightness"

    iget v2, p0, Lmaps/ac/au;->k:F

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;F)Lmaps/k/k;

    const-string v1, "Height"

    iget v2, p0, Lmaps/ac/au;->i:F

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;F)Lmaps/k/k;

    const-string v1, "Level"

    iget-object v2, p0, Lmaps/ac/au;->g:Lmaps/ac/ad;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    const-string v1, "Stale"

    iget-boolean v2, p0, Lmaps/ac/au;->j:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
