.class public final Lmaps/ac/ay;
.super Lmaps/ac/be;


# instance fields
.field private a:[Lmaps/ac/av;

.field private volatile b:Lmaps/ac/bd;


# direct methods
.method public constructor <init>([Lmaps/ac/av;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ac/be;-><init>()V

    iput-object p1, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    return-void
.end method


# virtual methods
.method public final a(I)Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()Lmaps/ac/bd;
    .locals 1

    iget-object v0, p0, Lmaps/ac/ay;->b:Lmaps/ac/bd;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    invoke-static {v0}, Lmaps/ac/bd;->a([Lmaps/ac/av;)Lmaps/ac/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/ay;->b:Lmaps/ac/bd;

    :cond_0
    iget-object v0, p0, Lmaps/ac/ay;->b:Lmaps/ac/bd;

    return-object v0
.end method

.method public final a(Lmaps/ac/av;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/ac/ay;->a()Lmaps/ac/bd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/ac/bd;->a(Lmaps/ac/av;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    array-length v6, v0

    iget-object v0, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    add-int/lit8 v3, v6, -0x1

    aget-object v0, v0, v3

    move v3, v1

    move-object v4, v0

    move v0, v1

    :goto_1
    if-ge v3, v6, :cond_2

    iget-object v5, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    aget-object v5, v5, v3

    invoke-static {v4, v5, p1}, Lmaps/ac/ax;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    move-object v4, v5

    goto :goto_1

    :cond_2
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    array-length v0, v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/ac/ay;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/ac/ay;

    iget-object v0, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    iget-object v1, p1, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/ay;->a:[Lmaps/ac/av;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
