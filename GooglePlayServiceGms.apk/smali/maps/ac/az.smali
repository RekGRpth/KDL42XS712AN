.class public final Lmaps/ac/az;
.super Ljava/lang/Object;


# instance fields
.field private final a:[I

.field private volatile b:Lmaps/ac/bd;

.field private volatile c:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/ac/ba;

    invoke-direct {v0}, Lmaps/ac/ba;-><init>()V

    return-void
.end method

.method private constructor <init>([I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/az;->a:[I

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lmaps/ac/az;->c:F

    return-void
.end method

.method synthetic constructor <init>([IB)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ac/az;-><init>([I)V

    return-void
.end method

.method private a(FIIILmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;[Z)I
    .locals 15

    move/from16 v0, p3

    move-object/from16 v1, p5

    invoke-virtual {p0, v0, v1}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move/from16 v0, p4

    move-object/from16 v1, p6

    invoke-virtual {p0, v0, v1}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    const/4 v8, -0x1

    add-int v6, p3, p2

    move/from16 v5, p1

    :goto_0
    add-int/lit8 v4, p4, -0x1

    if-gt v6, v4, :cond_0

    move-object/from16 v0, p8

    invoke-virtual {p0, v6, v0}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move-object/from16 v2, p8

    move-object/from16 v3, p7

    invoke-static {v0, v1, v2, v3}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)F

    move-result v4

    cmpl-float v7, v4, v5

    if-lez v7, :cond_3

    move v8, v6

    :goto_1
    add-int v6, v6, p2

    move v5, v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    if-ltz v8, :cond_2

    const/4 v4, 0x1

    const/4 v5, 0x1

    aput-boolean v5, p9, v8

    add-int/lit8 v5, p3, 0x1

    if-le v8, v5, :cond_1

    move-object v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    invoke-direct/range {v4 .. v13}, Lmaps/ac/az;->a(FIIILmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;[Z)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    :cond_1
    add-int/lit8 v5, p4, -0x1

    if-ge v8, v5, :cond_2

    move-object v5, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    invoke-direct/range {v5 .. v14}, Lmaps/ac/az;->a(FIIILmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;[Z)I

    move-result v5

    add-int/2addr v4, v5

    :cond_2
    return v4

    :cond_3
    move v4, v5

    goto :goto_1
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/az;
    .locals 3

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v1

    mul-int/lit8 v0, v1, 0x3

    new-array v2, v0, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0, p1, v2, v0}, Lmaps/ac/av;->a(Ljava/io/DataInput;Lmaps/ac/bt;[II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/ac/az;

    invoke-direct {v0, v2}, Lmaps/ac/az;-><init>([I)V

    return-object v0
.end method

.method public static a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/az;
    .locals 2

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lmaps/ac/av;->a([II)V

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lmaps/ac/av;->a([II)V

    new-instance v1, Lmaps/ac/az;

    invoke-direct {v1, v0}, Lmaps/ac/az;-><init>([I)V

    return-object v1
.end method

.method public static a([I)Lmaps/ac/az;
    .locals 7

    const/4 v1, 0x0

    array-length v0, p0

    div-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x3

    new-array v3, v0, [I

    move v0, v1

    move v2, v1

    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_0

    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v0, 0x1

    aget v0, p0, v0

    aput v0, v3, v2

    add-int/lit8 v6, v4, 0x1

    add-int/lit8 v0, v5, 0x1

    aget v2, p0, v5

    aput v2, v3, v4

    add-int/lit8 v2, v6, 0x1

    aput v1, v3, v6

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/ac/az;

    invoke-direct {v0, v3}, Lmaps/ac/az;-><init>([I)V

    return-object v0
.end method

.method public static b([I)Lmaps/ac/az;
    .locals 1

    new-instance v0, Lmaps/ac/az;

    invoke-direct {v0, p0}, Lmaps/ac/az;-><init>([I)V

    return-object v0
.end method


# virtual methods
.method public final a(F)Lmaps/ac/av;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    invoke-virtual {p0, v0}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_1

    invoke-virtual {p0}, Lmaps/ac/az;->c()Lmaps/ac/av;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmaps/ac/az;->d()F

    move-result v1

    mul-float/2addr v1, p1

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    add-int/lit8 v3, v2, -0x1

    move v5, v0

    move v0, v1

    move v1, v5

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p0, v1}, Lmaps/ac/az;->b(I)F

    move-result v2

    cmpl-float v4, v2, v0

    if-ltz v4, :cond_2

    div-float v2, v0, v2

    new-instance v3, Lmaps/ac/av;

    invoke-direct {v3}, Lmaps/ac/av;-><init>()V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    invoke-virtual {p0, v1, v3}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v0}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-static {v3, v0, v2, v0}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;FLmaps/ac/av;)V

    goto :goto_0

    :cond_2
    sub-float v2, v0, v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lmaps/ac/az;->c()Lmaps/ac/av;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)Lmaps/ac/av;
    .locals 5

    mul-int/lit8 v0, p1, 0x3

    new-instance v1, Lmaps/ac/av;

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    aget v2, v2, v0

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lmaps/ac/av;-><init>(III)V

    return-object v1
.end method

.method public final a()Lmaps/ac/bd;
    .locals 7

    iget-object v0, p0, Lmaps/ac/az;->b:Lmaps/ac/bd;

    if-nez v0, :cond_5

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    if-lez v0, :cond_6

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v5

    iget v2, v5, Lmaps/ac/av;->a:I

    iget v1, v5, Lmaps/ac/av;->b:I

    const/4 v0, 0x1

    move v3, v2

    move v4, v2

    move v2, v1

    :goto_0
    iget-object v6, p0, Lmaps/ac/az;->a:[I

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    if-ge v0, v6, :cond_4

    invoke-virtual {p0, v0, v5}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    iget v6, v5, Lmaps/ac/av;->a:I

    if-ge v6, v4, :cond_0

    iget v4, v5, Lmaps/ac/av;->a:I

    :cond_0
    iget v6, v5, Lmaps/ac/av;->a:I

    if-le v6, v3, :cond_1

    iget v3, v5, Lmaps/ac/av;->a:I

    :cond_1
    iget v6, v5, Lmaps/ac/av;->b:I

    if-ge v6, v2, :cond_2

    iget v2, v5, Lmaps/ac/av;->b:I

    :cond_2
    iget v6, v5, Lmaps/ac/av;->b:I

    if-le v6, v1, :cond_3

    iget v1, v5, Lmaps/ac/av;->b:I

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v5, v4, v2}, Lmaps/ac/av;->d(II)V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0, v3, v1}, Lmaps/ac/av;-><init>(II)V

    new-instance v1, Lmaps/ac/bd;

    invoke-direct {v1, v5, v0}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    iput-object v1, p0, Lmaps/ac/az;->b:Lmaps/ac/bd;

    :cond_5
    :goto_1
    iget-object v0, p0, Lmaps/ac/az;->b:Lmaps/ac/bd;

    return-object v0

    :cond_6
    new-instance v0, Lmaps/ac/bd;

    new-instance v1, Lmaps/ac/av;

    invoke-direct {v1}, Lmaps/ac/av;-><init>()V

    new-instance v2, Lmaps/ac/av;

    invoke-direct {v2}, Lmaps/ac/av;-><init>()V

    invoke-direct {v0, v1, v2}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    iput-object v0, p0, Lmaps/ac/az;->b:Lmaps/ac/bd;

    goto :goto_1
.end method

.method public final a(ILmaps/ac/av;)V
    .locals 3

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    aget v1, v1, v0

    iput v1, p2, Lmaps/ac/av;->a:I

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iput v1, p2, Lmaps/ac/av;->b:I

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iput v0, p2, Lmaps/ac/av;->c:I

    return-void
.end method

.method public final a(ILmaps/ac/av;Lmaps/ac/av;)V
    .locals 3

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    aget v1, v1, v0

    iget v2, p2, Lmaps/ac/av;->a:I

    sub-int/2addr v1, v2

    iput v1, p3, Lmaps/ac/av;->a:I

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iget v2, p2, Lmaps/ac/av;->b:I

    sub-int/2addr v1, v2

    iput v1, p3, Lmaps/ac/av;->b:I

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iget v1, p2, Lmaps/ac/av;->c:I

    sub-int/2addr v0, v1

    iput v0, p3, Lmaps/ac/av;->c:I

    return-void
.end method

.method public final a(Lmaps/ac/av;)V
    .locals 3

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x3

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    aget v1, v1, v0

    iput v1, p1, Lmaps/ac/av;->a:I

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iput v1, p1, Lmaps/ac/av;->b:I

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iput v0, p1, Lmaps/ac/av;->c:I

    return-void
.end method

.method public final b(I)F
    .locals 6

    mul-int/lit8 v0, p1, 0x3

    add-int/lit8 v1, v0, 0x3

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v0, v2, v0

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v4, v1, 0x1

    aget v1, v2, v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v5, v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    aget v2, v3, v2

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    aget v3, v3, v5

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public final b(F)Lmaps/ac/az;
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    div-int/lit8 v10, v0, 0x3

    new-array v9, v10, [Z

    aput-boolean v2, v9, v3

    add-int/lit8 v0, v10, -0x1

    aput-boolean v2, v9, v0

    mul-float v1, p1, p1

    add-int/lit8 v4, v10, -0x1

    new-instance v5, Lmaps/ac/av;

    invoke-direct {v5}, Lmaps/ac/av;-><init>()V

    new-instance v6, Lmaps/ac/av;

    invoke-direct {v6}, Lmaps/ac/av;-><init>()V

    new-instance v7, Lmaps/ac/av;

    invoke-direct {v7}, Lmaps/ac/av;-><init>()V

    new-instance v8, Lmaps/ac/av;

    invoke-direct {v8}, Lmaps/ac/av;-><init>()V

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lmaps/ac/az;->a(FIIILmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;[Z)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    if-eq v0, v10, :cond_0

    mul-int/lit8 v0, v0, 0x3

    new-array v1, v0, [I

    move v0, v3

    :goto_1
    if-ge v3, v10, :cond_3

    aget-boolean v2, v9, v3

    if-eqz v2, :cond_2

    mul-int/lit8 v2, v3, 0x3

    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v6, v2, 0x1

    aget v2, v5, v2

    aput v2, v1, v0

    add-int/lit8 v2, v4, 0x1

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v5, v6, 0x1

    aget v0, v0, v6

    aput v0, v1, v4

    add-int/lit8 v0, v2, 0x1

    iget-object v4, p0, Lmaps/ac/az;->a:[I

    aget v4, v4, v5

    aput v4, v1, v2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    new-instance p0, Lmaps/ac/az;

    invoke-direct {p0, v1}, Lmaps/ac/az;-><init>([I)V

    goto :goto_0
.end method

.method public final b(Lmaps/ac/av;)Lmaps/ac/az;
    .locals 5

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    new-array v1, v0, [I

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lmaps/ac/az;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    aget v2, v2, v0

    iget v3, p1, Lmaps/ac/av;->a:I

    add-int/2addr v2, v3

    aput v2, v1, v0

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget v4, p1, Lmaps/ac/av;->b:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v0, 0x2

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v4, v0, 0x2

    aget v3, v3, v4

    iget v4, p1, Lmaps/ac/av;->c:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/ac/az;

    invoke-direct {v0, v1}, Lmaps/ac/az;-><init>([I)V

    return-object v0
.end method

.method public final c()Lmaps/ac/av;
    .locals 5

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x3

    new-instance v1, Lmaps/ac/av;

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    aget v2, v2, v0

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lmaps/ac/av;-><init>(III)V

    return-object v1
.end method

.method public final c(F)Lmaps/ac/az;
    .locals 25

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/az;->e()Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ac/az;->a:[I

    array-length v2, v2

    div-int/lit8 v6, v2, 0x3

    add-int/lit8 v7, v6, -0x1

    const/4 v2, 0x2

    if-le v6, v2, :cond_0

    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-lez v2, :cond_0

    const/4 v2, 0x3

    if-gt v6, v2, :cond_1

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    new-instance v8, Lmaps/ac/bb;

    invoke-direct {v8, v6}, Lmaps/ac/bb;-><init>(I)V

    if-eqz v5, :cond_3

    add-int/lit8 v2, v7, -0x1

    :goto_1
    add-int/lit8 v3, v2, -0x1

    mul-int/lit8 v3, v3, 0x3

    mul-int/lit8 v4, v2, 0x3

    add-int/lit8 v9, v2, 0x1

    rem-int/2addr v9, v6

    mul-int/lit8 v9, v9, 0x3

    add-int/lit8 v2, v2, 0x2

    rem-int/2addr v2, v6

    mul-int/lit8 v2, v2, 0x3

    new-instance v10, Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/ac/az;->a:[I

    aget v11, v11, v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/ac/az;->a:[I

    add-int/lit8 v3, v3, 0x1

    aget v3, v12, v3

    const/4 v12, 0x0

    invoke-direct {v10, v11, v3, v12}, Lmaps/ac/av;-><init>(III)V

    new-instance v11, Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ac/az;->a:[I

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/ac/az;->a:[I

    add-int/lit8 v4, v4, 0x1

    aget v4, v12, v4

    const/4 v12, 0x0

    invoke-direct {v11, v3, v4, v12}, Lmaps/ac/av;-><init>(III)V

    new-instance v12, Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ac/az;->a:[I

    aget v3, v3, v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ac/az;->a:[I

    add-int/lit8 v9, v9, 0x1

    aget v4, v4, v9

    const/4 v9, 0x0

    invoke-direct {v12, v3, v4, v9}, Lmaps/ac/av;-><init>(III)V

    new-instance v9, Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ac/az;->a:[I

    aget v3, v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ac/az;->a:[I

    add-int/lit8 v13, v2, 0x1

    aget v4, v4, v13

    const/4 v13, 0x0

    invoke-direct {v9, v3, v4, v13}, Lmaps/ac/av;-><init>(III)V

    new-instance v13, Lmaps/ac/av;

    invoke-direct {v13}, Lmaps/ac/av;-><init>()V

    if-eqz v5, :cond_4

    invoke-virtual {v11, v12}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v3

    cmpg-float v3, v3, p1

    if-gtz v3, :cond_4

    const/4 v3, 0x1

    move v4, v3

    :goto_2
    if-nez v5, :cond_8

    invoke-virtual {v8, v10}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    if-nez v2, :cond_6

    invoke-virtual {v10, v11}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_2

    invoke-virtual {v11, v12}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_2

    invoke-virtual {v8, v11}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    :cond_2
    invoke-virtual {v8, v12}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v8}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object p0

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_4
    const/4 v3, 0x0

    move v4, v3

    goto :goto_2

    :cond_5
    invoke-virtual {v11, v12}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    invoke-virtual {v12, v9}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ac/az;->a:[I

    aget v3, v3, v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/ac/az;->a:[I

    add-int/lit8 v15, v2, 0x1

    aget v14, v14, v15

    const/4 v15, 0x0

    invoke-virtual {v9, v3, v14, v15}, Lmaps/ac/av;->a(III)V

    :cond_6
    invoke-virtual {v10, v11}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v3

    cmpg-float v3, v3, p1

    if-gtz v3, :cond_8

    add-int/lit8 v2, v2, 0x3

    mul-int/lit8 v3, v6, 0x3

    if-ne v2, v3, :cond_5

    invoke-virtual {v10, v12}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_7

    invoke-virtual {v12, v9}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_7

    invoke-virtual {v8, v12}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    :cond_7
    invoke-virtual {v8, v9}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    invoke-virtual {v8}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object p0

    goto/16 :goto_0

    :cond_8
    move v3, v2

    :goto_3
    mul-int/lit8 v14, v6, 0x3

    if-ge v3, v14, :cond_e

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/ac/az;->a:[I

    aget v14, v14, v3

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/ac/az;->a:[I

    add-int/lit8 v16, v3, 0x1

    aget v15, v15, v16

    invoke-virtual {v9, v14, v15}, Lmaps/ac/av;->d(II)V

    if-eqz v4, :cond_9

    add-int/lit8 v14, v7, -0x1

    mul-int/lit8 v14, v14, 0x3

    if-ne v3, v14, :cond_b

    invoke-virtual {v9, v13}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    :cond_9
    :goto_4
    invoke-virtual {v11, v12}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v14

    cmpl-float v15, v14, p1

    if-lez v15, :cond_d

    if-eqz v5, :cond_c

    if-ne v3, v2, :cond_c

    invoke-virtual {v13, v11}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    :goto_5
    invoke-virtual {v10, v11}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    invoke-virtual {v11, v12}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    invoke-virtual {v12, v9}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    :cond_a
    :goto_6
    add-int/lit8 v3, v3, 0x3

    goto :goto_3

    :cond_b
    mul-int/lit8 v14, v7, 0x3

    if-eq v3, v14, :cond_a

    goto :goto_4

    :cond_c
    invoke-virtual {v8, v11}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    goto :goto_5

    :cond_d
    invoke-virtual {v10, v11}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v15

    invoke-virtual {v12, v9}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v16

    add-float/2addr v15, v14

    float-to-double v0, v15

    move-wide/from16 v17, v0

    add-float v14, v14, v16

    float-to-double v14, v14

    iget v0, v11, Lmaps/ac/av;->a:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v19, v0

    mul-double v19, v19, v17

    iget v0, v12, Lmaps/ac/av;->a:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v21, v0

    mul-double v21, v21, v14

    add-double v19, v19, v21

    add-double v21, v17, v14

    div-double v19, v19, v21

    iget v0, v11, Lmaps/ac/av;->b:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v21, v0

    mul-double v21, v21, v17

    iget v0, v12, Lmaps/ac/av;->b:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v23, v0

    mul-double v23, v23, v14

    add-double v21, v21, v23

    add-double v14, v14, v17

    div-double v14, v21, v14

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->round(D)J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    invoke-static {v14, v15}, Ljava/lang/Math;->round(D)J

    move-result-wide v14

    long-to-int v14, v14

    move/from16 v0, v16

    invoke-virtual {v11, v0, v14}, Lmaps/ac/av;->d(II)V

    invoke-virtual {v12, v9}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    goto :goto_6

    :cond_e
    if-nez v5, :cond_f

    invoke-virtual {v11, v12}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_10

    :cond_f
    invoke-virtual {v8, v11}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    :cond_10
    if-eqz v5, :cond_11

    invoke-virtual {v8}, Lmaps/ac/bb;->b()Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {v8, v2}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    :goto_7
    invoke-virtual {v8}, Lmaps/ac/bb;->a()I

    move-result v2

    if-eq v2, v6, :cond_0

    invoke-virtual {v8}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object p0

    goto/16 :goto_0

    :cond_11
    invoke-virtual {v8, v12}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    goto :goto_7
.end method

.method public final c(I)Lmaps/ac/az;
    .locals 9

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v0, -0x20000000

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    if-gez p1, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p0, Lmaps/ac/az;->a:[I

    array-length v3, v3

    div-int/lit8 v4, v3, 0x3

    new-instance v5, Lmaps/ac/bb;

    invoke-direct {v5, v4}, Lmaps/ac/bb;-><init>(I)V

    new-instance v6, Lmaps/ac/av;

    invoke-direct {v6}, Lmaps/ac/av;-><init>()V

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_5

    invoke-virtual {p0, v3, v6}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    if-eqz v0, :cond_4

    iget v7, v6, Lmaps/ac/av;->a:I

    if-ge v7, p1, :cond_2

    iget v2, v6, Lmaps/ac/av;->a:I

    add-int/2addr v2, v8

    iput v2, v6, Lmaps/ac/av;->a:I

    move v2, v1

    :cond_2
    :goto_3
    invoke-virtual {v5, v6}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iget v7, v6, Lmaps/ac/av;->a:I

    if-le v7, p1, :cond_2

    iget v2, v6, Lmaps/ac/av;->a:I

    sub-int/2addr v2, v8

    iput v2, v6, Lmaps/ac/av;->a:I

    move v2, v1

    goto :goto_3

    :cond_5
    if-eqz v2, :cond_0

    invoke-virtual {v5}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object p0

    goto :goto_0
.end method

.method public final d()F
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lmaps/ac/az;->c:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v2, v0, -0x1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lmaps/ac/az;->b(I)F

    move-result v3

    add-float/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lmaps/ac/az;->c:F

    :cond_1
    iget v0, p0, Lmaps/ac/az;->c:F

    return v0
.end method

.method public final d(I)F
    .locals 4

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v2, v0, 0x3

    aget v1, v1, v2

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    aget v2, v2, v0

    sub-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v0, v0, 0x1

    aget v0, v3, v0

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Lmaps/ac/ax;->a(II)F

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lmaps/ac/az;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x3

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    aget v3, v3, v1

    iget-object v4, p0, Lmaps/ac/az;->a:[I

    aget v4, v4, v2

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    aget v3, v3, v0

    iget-object v4, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v5, v2, 0x1

    aget v4, v4, v5

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, Lmaps/ac/az;->a:[I

    add-int/lit8 v2, v2, 0x2

    aget v2, v4, v2

    if-ne v3, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/ac/az;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/ac/az;

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    iget-object v1, p1, Lmaps/ac/az;->a:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 13

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ac/az;->a:[I

    array-length v1, v1

    div-int/lit8 v8, v1, 0x3

    move v3, v0

    move-wide v1, v4

    :goto_0
    add-int/lit8 v6, v8, -0x1

    if-ge v3, v6, :cond_0

    invoke-virtual {p0, v3}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v6

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {p0, v7}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v7

    iget v9, v6, Lmaps/ac/av;->a:I

    int-to-long v9, v9

    iget v11, v7, Lmaps/ac/av;->b:I

    int-to-long v11, v11

    mul-long/2addr v9, v11

    iget v7, v7, Lmaps/ac/av;->a:I

    int-to-long v11, v7

    iget v6, v6, Lmaps/ac/av;->b:I

    int-to-long v6, v6

    mul-long/2addr v6, v11

    sub-long v6, v9, v6

    add-long/2addr v6, v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-wide v1, v6

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmaps/ac/az;->e()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lmaps/ac/az;->c()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {p0, v0}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v6

    iget v7, v3, Lmaps/ac/av;->a:I

    int-to-long v7, v7

    iget v9, v6, Lmaps/ac/av;->b:I

    int-to-long v9, v9

    mul-long/2addr v7, v9

    iget v6, v6, Lmaps/ac/av;->a:I

    int-to-long v9, v6

    iget v3, v3, Lmaps/ac/av;->b:I

    int-to-long v11, v3

    mul-long/2addr v9, v11

    sub-long v6, v7, v9

    add-long/2addr v1, v6

    :cond_1
    cmp-long v1, v1, v4

    if-lez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public final g()I
    .locals 12

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    const/high16 v4, -0x20000000

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    div-int/lit8 v6, v0, 0x3

    if-nez v6, :cond_0

    move v0, v4

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lmaps/aa/t;->a()Lmaps/aa/t;

    move-result-object v1

    new-instance v7, Lmaps/ac/av;

    invoke-direct {v7}, Lmaps/ac/av;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v0, v7}, Lmaps/ac/av;->i(Lmaps/ac/av;)V

    const/4 v0, 0x1

    move-object v5, v1

    :goto_1
    if-ge v0, v6, :cond_1

    new-instance v1, Lmaps/ac/av;

    invoke-direct {v1, v7}, Lmaps/ac/av;-><init>(Lmaps/ac/av;)V

    invoke-virtual {p0, v0}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v8

    invoke-virtual {v8, v7}, Lmaps/ac/av;->i(Lmaps/ac/av;)V

    invoke-virtual {v1}, Lmaps/ac/av;->f()I

    move-result v1

    invoke-static {v1}, Lmaps/ac/av;->c(I)I

    move-result v1

    invoke-static {v1}, Lmaps/aa/t;->a(I)D

    move-result-wide v8

    invoke-virtual {v7}, Lmaps/ac/av;->f()I

    move-result v1

    invoke-static {v1}, Lmaps/ac/av;->c(I)I

    move-result v1

    invoke-static {v1}, Lmaps/aa/t;->a(I)D

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Lmaps/aa/t;->a(DD)Lmaps/aa/t;

    move-result-object v1

    invoke-virtual {v5, v1}, Lmaps/aa/t;->a(Lmaps/aa/t;)Lmaps/aa/t;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    move-object v5, v1

    goto :goto_1

    :cond_1
    invoke-virtual {v5}, Lmaps/aa/t;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v4

    goto :goto_0

    :cond_2
    invoke-static {v4}, Lmaps/aa/t;->a(I)D

    move-result-wide v0

    const-wide v6, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpl-double v6, v0, v6

    if-nez v6, :cond_3

    move-wide v0, v2

    :cond_3
    invoke-virtual {v5, v0, v1}, Lmaps/aa/t;->a(D)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v4

    goto :goto_0

    :cond_4
    invoke-virtual {v5}, Lmaps/aa/t;->d()Lmaps/aa/t;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aa/t;->c()D

    move-result-wide v0

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x41c0000000000000L    # 5.36870912E8

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-static {v0}, Lmaps/ac/av;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public final h()Lmaps/ac/az;
    .locals 6

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v1, v0

    new-array v2, v1, [I

    iget-object v3, p0, Lmaps/ac/az;->a:[I

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lmaps/ac/az;->a:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    sub-int v4, v1, v0

    add-int/lit8 v4, v4, -0x3

    aget v4, v3, v4

    aput v4, v2, v0

    add-int/lit8 v4, v0, 0x1

    sub-int v5, v1, v0

    add-int/lit8 v5, v5, -0x2

    aget v5, v3, v5

    aput v5, v2, v4

    add-int/lit8 v4, v0, 0x2

    sub-int v5, v1, v0

    add-int/lit8 v5, v5, -0x1

    aget v5, v3, v5

    aput v5, v2, v4

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/ac/az;

    invoke-direct {v0, v2}, Lmaps/ac/az;-><init>([I)V

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/az;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit16 v0, v0, 0xa0

    return v0
.end method
