.class public final Lmaps/ac/aw;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/n;


# instance fields
.field private final a:Lmaps/ac/bt;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lmaps/ac/o;

.field private final e:Lmaps/ac/bl;

.field private final f:I

.field private final g:[I

.field private final h:I

.field private final i:I

.field private final j:I

.field private k:F

.field private l:F

.field private final m:[Lmaps/ac/a;

.field private final n:Lmaps/ac/ag;

.field private final o:Lmaps/ac/ag;

.field private final p:[Lmaps/ac/c;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lmaps/ac/bt;Lmaps/ac/o;[Lmaps/ac/a;Lmaps/ac/ag;Lmaps/ac/ag;[Lmaps/ac/c;Ljava/lang/String;Lmaps/ac/bl;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;[I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lmaps/ac/aw;->k:F

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lmaps/ac/aw;->l:F

    iput-object p2, p0, Lmaps/ac/aw;->d:Lmaps/ac/o;

    iput-object p3, p0, Lmaps/ac/aw;->m:[Lmaps/ac/a;

    iput-object p4, p0, Lmaps/ac/aw;->n:Lmaps/ac/ag;

    iput-object p5, p0, Lmaps/ac/aw;->o:Lmaps/ac/ag;

    iput-object p6, p0, Lmaps/ac/aw;->p:[Lmaps/ac/c;

    iput-object p7, p0, Lmaps/ac/aw;->c:Ljava/lang/String;

    iput-object p8, p0, Lmaps/ac/aw;->e:Lmaps/ac/bl;

    iput-object p9, p0, Lmaps/ac/aw;->q:Ljava/lang/String;

    iput p10, p0, Lmaps/ac/aw;->f:I

    iput p11, p0, Lmaps/ac/aw;->h:I

    const/4 v1, -0x1

    if-ne p12, v1, :cond_0

    const/16 p12, 0x1e

    :cond_0
    iput p12, p0, Lmaps/ac/aw;->i:I

    iput p13, p0, Lmaps/ac/aw;->j:I

    move-object/from16 v0, p14

    iput-object v0, p0, Lmaps/ac/aw;->r:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lmaps/ac/aw;->b:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lmaps/ac/aw;->g:[I

    iput-object p1, p0, Lmaps/ac/aw;->a:Lmaps/ac/bt;

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/aw;
    .locals 20

    invoke-static/range {p0 .. p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v5, v4, [Lmaps/ac/a;

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/bv;->b()Lmaps/ac/bt;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/bv;->a()I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lmaps/ac/a;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/a;

    move-result-object v6

    aput-object v6, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    aget-object v2, v5, v2

    invoke-virtual {v2}, Lmaps/ac/a;->b()Lmaps/ac/av;

    invoke-static/range {p0 .. p1}, Lmaps/ac/bo;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;

    move-result-object v11

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v11}, Lmaps/ac/ag;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bo;)Lmaps/ac/ag;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v11}, Lmaps/ac/ag;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bo;)Lmaps/ac/ag;

    move-result-object v7

    invoke-static/range {p0 .. p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v8, v4, [Lmaps/ac/c;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-static/range {p0 .. p0}, Lmaps/ac/c;->b(Ljava/io/DataInput;)Lmaps/ac/c;

    move-result-object v9

    aput-object v9, v8, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v12

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v13

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v14

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v15

    const/4 v4, 0x0

    const/4 v2, 0x1

    invoke-static {v2, v15}, Lmaps/ac/ar;->a(II)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static/range {p0 .. p0}, Lmaps/ac/o;->a(Ljava/io/DataInput;)Lmaps/ac/p;

    move-result-object v4

    :cond_2
    :goto_2
    const/4 v9, 0x0

    const/16 v2, 0x20

    invoke-static {v2, v15}, Lmaps/ac/ar;->a(II)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/w/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    :cond_3
    const/16 v16, 0x0

    const/16 v2, 0x40

    invoke-static {v2, v15}, Lmaps/ac/ar;->a(II)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v16

    :cond_4
    const/16 v2, 0x80

    invoke-static {v2, v15}, Lmaps/ac/ar;->a(II)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v17

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lmaps/ac/bv;->a()I

    move-result v2

    const/16 v10, 0xb

    if-ne v2, v10, :cond_9

    const/16 v2, 0x200

    invoke-static {v2, v15}, Lmaps/ac/ar;->a(II)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static/range {p0 .. p0}, Lmaps/ac/c;->b(Ljava/io/DataInput;)Lmaps/ac/c;

    :goto_4
    invoke-static/range {p0 .. p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v10

    new-array v0, v10, [I

    move-object/from16 v18, v0

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v10, :cond_a

    invoke-static/range {p0 .. p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v19

    aput v19, v18, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_5
    const/4 v2, 0x2

    invoke-static {v2, v15}, Lmaps/ac/ar;->a(II)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {p0 .. p0}, Lmaps/ac/o;->b(Ljava/io/DataInput;)Lmaps/ac/q;

    move-result-object v4

    goto :goto_2

    :cond_6
    invoke-virtual {v6}, Lmaps/ac/ag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lmaps/ac/ag;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v17

    if-lez v17, :cond_7

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v17

    if-lez v17, :cond_7

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v17, 0xa

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_7
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto :goto_3

    :cond_8
    sget-object v2, Lmaps/ac/c;->c:Lmaps/ac/c;

    goto :goto_4

    :cond_9
    sget-object v2, Lmaps/ac/c;->c:Lmaps/ac/c;

    goto :goto_4

    :cond_a
    new-instance v2, Lmaps/ac/aw;

    invoke-virtual/range {p2 .. p2}, Lmaps/ac/bf;->a()I

    invoke-virtual {v11}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v10

    invoke-virtual {v11}, Lmaps/ac/bo;->c()I

    invoke-virtual {v11}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v2 .. v18}, Lmaps/ac/aw;-><init>(Lmaps/ac/bt;Lmaps/ac/o;[Lmaps/ac/a;Lmaps/ac/ag;Lmaps/ac/ag;[Lmaps/ac/c;Ljava/lang/String;Lmaps/ac/bl;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;[I)V

    return-object v2
.end method


# virtual methods
.method public final a()Lmaps/ac/o;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->d:Lmaps/ac/o;

    return-object v0
.end method

.method public final a(F)V
    .locals 0

    iput p1, p0, Lmaps/ac/aw;->k:F

    return-void
.end method

.method public final b()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->a:Lmaps/ac/bt;

    return-object v0
.end method

.method public final b(F)V
    .locals 0

    iput p1, p0, Lmaps/ac/aw;->l:F

    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->e:Lmaps/ac/bl;

    return-object v0
.end method

.method public final e()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/ac/aw;->f:I

    return v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lmaps/ac/aw;->h:I

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lmaps/ac/aw;->i:I

    return v0
.end method

.method public final i()[I
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->g:[I

    return-object v0
.end method

.method public final j()I
    .locals 7

    const/4 v2, 0x0

    const/16 v0, 0x78

    iget-object v1, p0, Lmaps/ac/aw;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/ac/aw;->b:Ljava/lang/String;

    invoke-static {v0}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x78

    :cond_0
    iget-object v1, p0, Lmaps/ac/aw;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/ac/aw;->c:Ljava/lang/String;

    invoke-static {v1}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lmaps/ac/aw;->m:[Lmaps/ac/a;

    if-eqz v1, :cond_2

    iget-object v5, p0, Lmaps/ac/aw;->m:[Lmaps/ac/a;

    array-length v6, v5

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v4, v5, v3

    invoke-virtual {v4}, Lmaps/ac/a;->d()I

    move-result v4

    add-int/2addr v4, v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_0

    :cond_2
    move v1, v2

    :cond_3
    iget-object v3, p0, Lmaps/ac/aw;->p:[Lmaps/ac/c;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lmaps/ac/aw;->p:[Lmaps/ac/c;

    array-length v5, v3

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_4

    invoke-static {}, Lmaps/ac/c;->c()I

    move-result v4

    add-int/2addr v4, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1

    :cond_4
    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/aw;->n:Lmaps/ac/ag;

    invoke-static {v2}, Lmaps/ac/ar;->a(Lmaps/ac/ag;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/aw;->o:Lmaps/ac/ag;

    invoke-static {v2}, Lmaps/ac/ar;->a(Lmaps/ac/ag;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/aw;->d:Lmaps/ac/o;

    invoke-static {v2}, Lmaps/ac/ar;->a(Lmaps/ac/o;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/aw;->e:Lmaps/ac/bl;

    invoke-static {v2}, Lmaps/ac/ar;->a(Lmaps/ac/bl;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/aw;->q:Ljava/lang/String;

    invoke-static {v2}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/aw;->r:Ljava/lang/String;

    invoke-static {v2}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public final k()F
    .locals 1

    iget v0, p0, Lmaps/ac/aw;->k:F

    return v0
.end method

.method public final l()F
    .locals 1

    iget v0, p0, Lmaps/ac/aw;->l:F

    return v0
.end method

.method public final m()[Lmaps/ac/a;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->m:[Lmaps/ac/a;

    return-object v0
.end method

.method public final n()Lmaps/ac/ag;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->n:Lmaps/ac/ag;

    return-object v0
.end method

.method public final o()Lmaps/ac/ag;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->o:Lmaps/ac/ag;

    return-object v0
.end method

.method public final p()[Lmaps/ac/c;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aw;->p:[Lmaps/ac/c;

    return-object v0
.end method

.method public final q()Z
    .locals 2

    const/16 v0, 0x10

    iget v1, p0, Lmaps/ac/aw;->j:I

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method
