.class public final Lmaps/ac/a;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ac/av;

.field private final b:I

.field private final c:F

.field private final d:Lmaps/ac/av;

.field private final e:F

.field private final f:F

.field private final g:F


# direct methods
.method public constructor <init>(Lmaps/ac/av;IFLmaps/ac/av;FFF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/a;->a:Lmaps/ac/av;

    iput p2, p0, Lmaps/ac/a;->b:I

    iput p3, p0, Lmaps/ac/a;->c:F

    iput-object p4, p0, Lmaps/ac/a;->d:Lmaps/ac/av;

    iput p5, p0, Lmaps/ac/a;->e:F

    iput p6, p0, Lmaps/ac/a;->f:F

    iput p7, p0, Lmaps/ac/a;->g:F

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/a;
    .locals 8

    const/high16 v6, 0x41200000    # 10.0f

    const/high16 v7, 0x41000000    # 8.0f

    const/high16 v0, 0x7fc00000    # NaNf

    invoke-static {p0, p1}, Lmaps/ac/av;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/av;

    move-result-object v1

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lmaps/ac/ar;->a(II)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    :goto_0
    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v2, v5}, Lmaps/ac/ar;->a(II)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p0, p1}, Lmaps/ac/av;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/av;

    move-result-object v4

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v0

    int-to-float v0, v0

    div-float v5, v0, v6

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v0

    int-to-float v0, v0

    div-float v6, v0, v7

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v0

    int-to-float v0, v0

    div-float v7, v0, v7

    :goto_1
    new-instance v0, Lmaps/ac/a;

    invoke-direct/range {v0 .. v7}, Lmaps/ac/a;-><init>(Lmaps/ac/av;IFLmaps/ac/av;FFF)V

    return-object v0

    :cond_0
    move v7, v0

    move v6, v0

    move v5, v0

    goto :goto_1

    :cond_1
    move v3, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    iget v0, p0, Lmaps/ac/a;->b:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final b()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ac/a;->a:Lmaps/ac/av;

    return-object v0
.end method

.method public final c()F
    .locals 1

    iget v0, p0, Lmaps/ac/a;->c:F

    return v0
.end method

.method public final d()I
    .locals 2

    iget-object v0, p0, Lmaps/ac/a;->a:Lmaps/ac/av;

    invoke-static {v0}, Lmaps/ac/ar;->a(Lmaps/ac/av;)I

    move-result v0

    add-int/lit8 v0, v0, 0x28

    iget-object v1, p0, Lmaps/ac/a;->d:Lmaps/ac/av;

    invoke-static {v1}, Lmaps/ac/ar;->a(Lmaps/ac/av;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/ac/a;

    iget-object v2, p0, Lmaps/ac/a;->d:Lmaps/ac/av;

    if-nez v2, :cond_4

    iget-object v2, p1, Lmaps/ac/a;->d:Lmaps/ac/av;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lmaps/ac/a;->d:Lmaps/ac/av;

    iget-object v3, p1, Lmaps/ac/a;->d:Lmaps/ac/av;

    invoke-virtual {v2, v3}, Lmaps/ac/av;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lmaps/ac/a;->f:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lmaps/ac/a;->f:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lmaps/ac/a;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lmaps/ac/a;->e:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lmaps/ac/a;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lmaps/ac/a;->g:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lmaps/ac/a;->b:I

    iget v3, p1, Lmaps/ac/a;->b:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lmaps/ac/a;->a:Lmaps/ac/av;

    if-nez v2, :cond_a

    iget-object v2, p1, Lmaps/ac/a;->a:Lmaps/ac/av;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lmaps/ac/a;->a:Lmaps/ac/av;

    iget-object v3, p1, Lmaps/ac/a;->a:Lmaps/ac/av;

    invoke-virtual {v2, v3}, Lmaps/ac/av;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget v2, p0, Lmaps/ac/a;->c:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lmaps/ac/a;->c:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/ac/a;->d:Lmaps/ac/av;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lmaps/ac/a;->f:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lmaps/ac/a;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lmaps/ac/a;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lmaps/ac/a;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lmaps/ac/a;->a:Lmaps/ac/av;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/ac/a;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/a;->d:Lmaps/ac/av;

    invoke-virtual {v0}, Lmaps/ac/av;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lmaps/ac/a;->a:Lmaps/ac/av;

    invoke-virtual {v1}, Lmaps/ac/av;->hashCode()I

    move-result v1

    goto :goto_1
.end method
