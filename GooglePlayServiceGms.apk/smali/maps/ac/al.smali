.class public final Lmaps/ac/al;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/n;


# instance fields
.field private a:Lmaps/ac/az;

.field private b:Lmaps/ac/bl;

.field private final c:Ljava/lang/String;

.field private d:I

.field private final e:[I


# direct methods
.method public constructor <init>(Lmaps/ac/az;Lmaps/ac/bl;Ljava/lang/String;I[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/al;->a:Lmaps/ac/az;

    iput-object p2, p0, Lmaps/ac/al;->b:Lmaps/ac/bl;

    iput-object p3, p0, Lmaps/ac/al;->c:Ljava/lang/String;

    iput p4, p0, Lmaps/ac/al;->d:I

    iput-object p5, p0, Lmaps/ac/al;->e:[I

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/o;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Lmaps/ac/az;
    .locals 1

    iget-object v0, p0, Lmaps/ac/al;->a:Lmaps/ac/az;

    return-object v0
.end method

.method public final d()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/al;->b:Lmaps/ac/bl;

    return-object v0
.end method

.method public final e()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/ac/al;->d:I

    return v0
.end method

.method public final i()[I
    .locals 1

    iget-object v0, p0, Lmaps/ac/al;->e:[I

    return-object v0
.end method

.method public final j()I
    .locals 2

    iget-object v0, p0, Lmaps/ac/al;->a:Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->i()I

    move-result v0

    add-int/lit8 v0, v0, 0x2c

    iget-object v1, p0, Lmaps/ac/al;->c:Ljava/lang/String;

    invoke-static {v1}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ac/al;->b:Lmaps/ac/bl;

    invoke-static {v1}, Lmaps/ac/ar;->a(Lmaps/ac/bl;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
