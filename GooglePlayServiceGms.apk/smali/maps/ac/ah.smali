.class public final Lmaps/ac/ah;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:F

.field private final e:Ljava/lang/String;

.field private final f:Lmaps/ac/bl;

.field private final g:Ljava/lang/String;

.field private final h:F


# direct methods
.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;Lmaps/ac/bl;Ljava/lang/String;F)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/ac/ah;->a:I

    iput-object p2, p0, Lmaps/ac/ah;->b:Ljava/lang/String;

    iput p3, p0, Lmaps/ac/ah;->c:I

    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p0, Lmaps/ac/ah;->d:F

    iput-object p4, p0, Lmaps/ac/ah;->e:Ljava/lang/String;

    iput-object p5, p0, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    iput-object p6, p0, Lmaps/ac/ah;->g:Ljava/lang/String;

    iput p7, p0, Lmaps/ac/ah;->h:F

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bo;Ljava/util/List;)V
    .locals 11

    const/16 v5, 0xa

    const/16 v10, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v1

    invoke-static {v1, v8}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lmaps/ac/bv;->a()I

    move-result v0

    if-ne v0, v5, :cond_7

    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    :goto_0
    const/4 v3, 0x2

    invoke-static {v1, v3}, Lmaps/ac/ar;->a(II)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmaps/w/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    :goto_1
    const/4 v3, 0x4

    invoke-static {v1, v3}, Lmaps/ac/ar;->a(II)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p0, p1}, Lmaps/ac/bo;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;

    move-result-object p2

    :cond_0
    :goto_2
    invoke-virtual {p1}, Lmaps/ac/bv;->a()I

    move-result v3

    if-eq v3, v5, :cond_5

    invoke-static {v1, v8}, Lmaps/ac/ar;->a(II)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p2}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bl;->h()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v3}, Lmaps/ac/bl;->l()Lmaps/ac/x;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/x;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lmaps/ac/x;->b()I

    move-result v0

    move v3, v0

    :goto_3
    const/4 v7, 0x0

    const/16 v0, 0x10

    invoke-static {v1, v0}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v5, 0x41000000    # 8.0f

    div-float v7, v0, v5

    :cond_1
    const/16 v0, 0x20

    invoke-static {v1, v0}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    :cond_2
    invoke-static {v1, v10}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eq v1, v10, :cond_4

    new-instance v0, Lmaps/ac/ah;

    xor-int/lit8 v1, v1, 0x8

    invoke-virtual {p2}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v5

    invoke-virtual {p2}, Lmaps/ac/bo;->c()I

    invoke-virtual {p2}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lmaps/ac/ah;-><init>(ILjava/lang/String;ILjava/lang/String;Lmaps/ac/bl;Ljava/lang/String;F)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/ac/ah;

    const/high16 v7, -0x40800000    # -1.0f

    move v1, v10

    move-object v2, v9

    move v3, v8

    move-object v4, v9

    move-object v5, v9

    move-object v6, v9

    invoke-direct/range {v0 .. v7}, Lmaps/ac/ah;-><init>(ILjava/lang/String;ILjava/lang/String;Lmaps/ac/bl;Ljava/lang/String;F)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    return-void

    :cond_3
    invoke-static {v1}, Lmaps/ac/ah;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lmaps/ac/ag;->e()Lmaps/ac/bo;

    move-result-object p2

    goto :goto_2

    :cond_4
    new-instance v0, Lmaps/ac/ah;

    invoke-virtual {p2}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v5

    invoke-virtual {p2}, Lmaps/ac/bo;->c()I

    invoke-virtual {p2}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lmaps/ac/ah;-><init>(ILjava/lang/String;ILjava/lang/String;Lmaps/ac/bl;Ljava/lang/String;F)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_5
    move v3, v0

    goto :goto_3

    :cond_6
    move-object v4, v9

    goto/16 :goto_1

    :cond_7
    move v0, v8

    move-object v2, v9

    goto/16 :goto_0
.end method

.method private static a(I)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-static {p0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, v0}, Lmaps/ac/ar;->a(II)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget v0, p0, Lmaps/ac/ah;->a:I

    invoke-static {v0}, Lmaps/ac/ah;->a(I)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 2

    iget v0, p0, Lmaps/ac/ah;->a:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 2

    iget v0, p0, Lmaps/ac/ah;->a:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    iget v0, p0, Lmaps/ac/ah;->a:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 2

    iget v0, p0, Lmaps/ac/ah;->a:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/ac/ah;

    iget v2, p0, Lmaps/ac/ah;->a:I

    iget v3, p1, Lmaps/ac/ah;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lmaps/ac/ah;->h:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lmaps/ac/ah;->h:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lmaps/ac/ah;->b:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lmaps/ac/ah;->b:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lmaps/ac/ah;->b:Ljava/lang/String;

    iget-object v3, p1, Lmaps/ac/ah;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    if-nez v2, :cond_8

    iget-object v2, p1, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    iget-object v3, p1, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    invoke-virtual {v2, v3}, Lmaps/ac/bl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lmaps/ac/ah;->g:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Lmaps/ac/ah;->g:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lmaps/ac/ah;->g:Ljava/lang/String;

    iget-object v3, p1, Lmaps/ac/ah;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget v2, p0, Lmaps/ac/ah;->c:I

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lmaps/ac/ah;->c:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Lmaps/ac/ah;->e:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Lmaps/ac/ah;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lmaps/ac/ah;->e:Ljava/lang/String;

    iget-object v1, p1, Lmaps/ac/ah;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public final f()Z
    .locals 2

    iget v0, p0, Lmaps/ac/ah;->a:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/ah;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final h()F
    .locals 1

    iget v0, p0, Lmaps/ac/ah;->d:F

    return v0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lmaps/ac/ah;->a:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lmaps/ac/ah;->h:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lmaps/ac/ah;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lmaps/ac/ah;->g:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lmaps/ac/ah;->c:I

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lmaps/ac/ah;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/ah;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    invoke-virtual {v0}, Lmaps/ac/bl;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/ac/ah;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lmaps/ac/ah;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/ah;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    return-object v0
.end method

.method public final k()F
    .locals 1

    iget v0, p0, Lmaps/ac/ah;->h:F

    return v0
.end method

.method public final l()I
    .locals 2

    iget-object v0, p0, Lmaps/ac/ah;->b:Ljava/lang/String;

    invoke-static {v0}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x30

    iget-object v1, p0, Lmaps/ac/ah;->e:Ljava/lang/String;

    invoke-static {v1}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ac/ah;->g:Ljava/lang/String;

    invoke-static {v1}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ac/ah;->f:Lmaps/ac/bl;

    invoke-static {v1}, Lmaps/ac/ar;->a(Lmaps/ac/bl;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
