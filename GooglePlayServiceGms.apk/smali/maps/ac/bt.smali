.class public Lmaps/ac/bt;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Lmaps/ac/cf;

.field final e:I

.field final f:I

.field final g:I

.field private h:Lmaps/ac/bt;

.field private i:Lmaps/ac/bd;


# direct methods
.method public constructor <init>(III)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    return-void
.end method

.method public constructor <init>(IIILmaps/ac/cf;)V
    .locals 3

    const/high16 v2, 0x20000000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ac/bt;->h:Lmaps/ac/bt;

    iput p1, p0, Lmaps/ac/bt;->a:I

    iput p2, p0, Lmaps/ac/bt;->b:I

    iput p3, p0, Lmaps/ac/bt;->c:I

    if-nez p4, :cond_0

    new-instance p4, Lmaps/ac/cf;

    invoke-direct {p4}, Lmaps/ac/cf;-><init>()V

    :cond_0
    iput-object p4, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    rsub-int/lit8 v0, p1, 0x12

    iput v0, p0, Lmaps/ac/bt;->g:I

    const/high16 v0, 0x40000000    # 2.0f

    shr-int/2addr v0, p1

    iget v1, p0, Lmaps/ac/bt;->b:I

    mul-int/2addr v1, v0

    sub-int/2addr v1, v2

    iput v1, p0, Lmaps/ac/bt;->e:I

    iget v1, p0, Lmaps/ac/bt;->c:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    sub-int/2addr v0, v2

    neg-int v0, v0

    iput v0, p0, Lmaps/ac/bt;->f:I

    return-void
.end method

.method public static a(Lmaps/ac/cx;)Ljava/util/ArrayList;
    .locals 2

    const/16 v0, 0xf

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lmaps/ac/bt;->a(Lmaps/ac/cx;ILmaps/ac/cf;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/ac/cx;I)Ljava/util/ArrayList;
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p0}, Lmaps/ac/cx;->f()Lmaps/ac/av;

    move-result-object v0

    iget v0, v0, Lmaps/ac/av;->a:I

    invoke-virtual {p0}, Lmaps/ac/cx;->g()Lmaps/ac/av;

    move-result-object v1

    iget v1, v1, Lmaps/ac/av;->b:I

    invoke-static {p1, v0, v1}, Lmaps/ac/bt;->b(III)Lmaps/ac/bt;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ac/cx;->g()Lmaps/ac/av;

    move-result-object v1

    iget v1, v1, Lmaps/ac/av;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0}, Lmaps/ac/cx;->f()Lmaps/ac/av;

    move-result-object v2

    iget v2, v2, Lmaps/ac/av;->b:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {p1, v1, v2}, Lmaps/ac/bt;->b(III)Lmaps/ac/bt;

    move-result-object v2

    iget v1, v0, Lmaps/ac/bt;->b:I

    iget v3, v0, Lmaps/ac/bt;->c:I

    iget v5, v2, Lmaps/ac/bt;->b:I

    iget v6, v2, Lmaps/ac/bt;->c:I

    iget v4, v0, Lmaps/ac/bt;->e:I

    iget v0, v0, Lmaps/ac/bt;->f:I

    invoke-static {v4, v0}, Lmaps/ac/av;->e(II)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, v2, Lmaps/ac/bt;->e:I

    iget v2, v2, Lmaps/ac/bt;->f:I

    invoke-static {v0, v2}, Lmaps/ac/av;->e(II)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    shl-int v2, v0, p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-le v1, v5, :cond_9

    move v4, v1

    :goto_0
    if-ge v4, v2, :cond_4

    move v1, v3

    :goto_1
    if-gez v1, :cond_2

    new-instance v7, Lmaps/ac/bt;

    invoke-direct {v7, p1, v4, v1, v8}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_2
    if-gt v1, v6, :cond_3

    new-instance v7, Lmaps/ac/bt;

    invoke-direct {v7, p1, v4, v1, v8}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    move v4, v1

    :goto_3
    if-gt v4, v5, :cond_0

    move v1, v3

    :goto_4
    if-gez v1, :cond_5

    new-instance v7, Lmaps/ac/bt;

    invoke-direct {v7, p1, v4, v1, v8}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    move v1, v2

    :goto_5
    if-gt v1, v6, :cond_6

    new-instance v7, Lmaps/ac/bt;

    invoke-direct {v7, p1, v4, v1, v8}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_7
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    :goto_6
    if-gt v4, v5, :cond_0

    move v1, v3

    :goto_7
    if-gez v1, :cond_8

    new-instance v7, Lmaps/ac/bt;

    invoke-direct {v7, p1, v4, v1, v8}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_8
    move v1, v2

    :goto_8
    if-gt v1, v6, :cond_7

    new-instance v7, Lmaps/ac/bt;

    invoke-direct {v7, p1, v4, v1, v8}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_9
    move v4, v1

    goto :goto_6
.end method

.method public static a(Lmaps/ac/cx;ILmaps/ac/cf;)Ljava/util/ArrayList;
    .locals 11

    const/4 v10, 0x2

    const/4 v2, 0x0

    if-gez p1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lmaps/ac/cx;->f()Lmaps/ac/av;

    move-result-object v0

    iget v0, v0, Lmaps/ac/av;->a:I

    invoke-virtual {p0}, Lmaps/ac/cx;->g()Lmaps/ac/av;

    move-result-object v1

    iget v1, v1, Lmaps/ac/av;->b:I

    invoke-static {p1, v0, v1, p2}, Lmaps/ac/bt;->a(IIILmaps/ac/cf;)Lmaps/ac/bt;

    move-result-object v5

    invoke-virtual {p0}, Lmaps/ac/cx;->g()Lmaps/ac/av;

    move-result-object v0

    iget v0, v0, Lmaps/ac/av;->a:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, Lmaps/ac/cx;->f()Lmaps/ac/av;

    move-result-object v1

    iget v1, v1, Lmaps/ac/av;->b:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1, v0, v1, p2}, Lmaps/ac/bt;->a(IIILmaps/ac/cf;)Lmaps/ac/bt;

    move-result-object v6

    iget v1, v5, Lmaps/ac/bt;->b:I

    iget v3, v5, Lmaps/ac/bt;->c:I

    iget v7, v6, Lmaps/ac/bt;->b:I

    iget v8, v6, Lmaps/ac/bt;->c:I

    const/4 v0, 0x1

    shl-int v9, v0, p1

    if-le v1, v7, :cond_2

    sub-int v0, v9, v1

    add-int/2addr v0, v7

    add-int/lit8 v0, v0, 0x1

    sub-int v4, v8, v3

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    move v4, v0

    :goto_1
    if-gez v4, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_2
    sub-int v0, v7, v1

    add-int/lit8 v0, v0, 0x1

    sub-int v4, v8, v3

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    move v4, v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    if-gt v4, v10, :cond_4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v4, v10, :cond_0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    if-le v1, v7, :cond_9

    :goto_2
    if-ge v1, v9, :cond_7

    move v4, v3

    :goto_3
    if-gt v4, v8, :cond_5

    new-instance v5, Lmaps/ac/bt;

    invoke-direct {v5, p1, v1, v4, p2}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    :cond_7
    if-gt v2, v7, :cond_0

    move v1, v3

    :goto_4
    if-gt v1, v8, :cond_6

    new-instance v4, Lmaps/ac/bt;

    invoke-direct {v4, p1, v2, v1, p2}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    add-int/lit8 v1, v1, 0x1

    :cond_9
    if-gt v1, v7, :cond_0

    move v2, v3

    :goto_5
    if-gt v2, v8, :cond_8

    new-instance v4, Lmaps/ac/bt;

    invoke-direct {v4, p1, v1, v2, p2}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_5
.end method

.method private static a(IIILmaps/ac/cf;)Lmaps/ac/bt;
    .locals 5

    const/high16 v4, 0x20000000

    const/16 v1, 0x1e

    const/4 v0, 0x0

    if-gtz p0, :cond_0

    new-instance v1, Lmaps/ac/bt;

    invoke-direct {v1, v0, v0, v0}, Lmaps/ac/bt;-><init>(III)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    if-le p0, v1, :cond_1

    move p0, v1

    :cond_1
    rsub-int/lit8 v1, p0, 0x1e

    add-int v2, p1, v4

    shr-int/2addr v2, v1

    neg-int v3, p2

    add-int/2addr v3, v4

    shr-int v1, v3, v1

    const/4 v3, 0x1

    shl-int/2addr v3, p0

    if-gez v2, :cond_3

    add-int/2addr v2, v3

    :cond_2
    :goto_1
    if-gez v1, :cond_4

    :goto_2
    new-instance v1, Lmaps/ac/bt;

    invoke-direct {v1, p0, v2, v0, p3}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    if-lt v2, v3, :cond_2

    sub-int/2addr v2, v3

    goto :goto_1

    :cond_4
    if-lt v1, v3, :cond_5

    add-int/lit8 v0, v3, -0x1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public static a(Ljava/io/DataInput;)Lmaps/ac/bt;
    .locals 4

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v1

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    new-instance v3, Lmaps/ac/bt;

    invoke-direct {v3, v0, v1, v2}, Lmaps/ac/bt;-><init>(III)V

    return-object v3
.end method

.method public static a(Lmaps/ac/av;)Lmaps/ac/bt;
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x20000000

    iget v0, p0, Lmaps/ac/av;->a:I

    iget v1, p0, Lmaps/ac/av;->b:I

    const/high16 v2, -0x20000000

    if-le v1, v2, :cond_0

    if-le v1, v3, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    add-int/2addr v0, v3

    shr-int/lit8 v0, v0, 0x0

    neg-int v1, v1

    add-int/2addr v1, v3

    shr-int/lit8 v2, v1, 0x0

    if-gez v0, :cond_3

    add-int/2addr v0, v4

    :cond_2
    :goto_1
    new-instance v1, Lmaps/ac/bt;

    const/16 v3, 0x1e

    invoke-direct {v1, v3, v0, v2}, Lmaps/ac/bt;-><init>(III)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    if-lt v0, v4, :cond_2

    sub-int/2addr v0, v4

    goto :goto_1
.end method

.method private static b(III)Lmaps/ac/bt;
    .locals 4

    const/high16 v3, 0x20000000

    const/16 v0, 0x1e

    const/4 v1, 0x0

    if-gtz p0, :cond_0

    new-instance v0, Lmaps/ac/bt;

    invoke-direct {v0, v1, v1, v1}, Lmaps/ac/bt;-><init>(III)V

    :goto_0
    return-object v0

    :cond_0
    if-le p0, v0, :cond_1

    move p0, v0

    :cond_1
    rsub-int/lit8 v0, p0, 0x1e

    add-int v1, p1, v3

    shr-int/2addr v1, v0

    neg-int v2, p2

    add-int/2addr v2, v3

    shr-int/2addr v2, v0

    new-instance v0, Lmaps/ac/bt;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    goto :goto_0
.end method

.method public static b(Lmaps/ac/av;)Lmaps/ac/bt;
    .locals 4

    const/16 v0, 0xf

    iget v1, p0, Lmaps/ac/av;->a:I

    iget v2, p0, Lmaps/ac/av;->b:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lmaps/ac/bt;->a(IIILmaps/ac/cf;)Lmaps/ac/bt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lmaps/ac/bt;
    .locals 5

    iget-object v0, p0, Lmaps/ac/bt;->h:Lmaps/ac/bt;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ac/bt;

    iget v1, p0, Lmaps/ac/bt;->a:I

    iget v2, p0, Lmaps/ac/bt;->b:I

    iget v3, p0, Lmaps/ac/bt;->c:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    iput-object v0, p0, Lmaps/ac/bt;->h:Lmaps/ac/bt;

    :cond_0
    iget-object v0, p0, Lmaps/ac/bt;->h:Lmaps/ac/bt;

    return-object v0
.end method

.method public final a(I)Lmaps/ac/bt;
    .locals 3

    iget v0, p0, Lmaps/ac/bt;->a:I

    sub-int/2addr v0, p1

    if-gtz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget v1, p0, Lmaps/ac/bt;->b:I

    shr-int/2addr v1, v0

    iget v2, p0, Lmaps/ac/bt;->c:I

    shr-int v0, v2, v0

    invoke-virtual {p0, p1, v1, v0}, Lmaps/ac/bt;->a(III)Lmaps/ac/bt;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(III)Lmaps/ac/bt;
    .locals 2

    new-instance v0, Lmaps/ac/bt;

    iget-object v1, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-direct {v0, p1, p2, p3, v1}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    return-object v0
.end method

.method public a(Lmaps/ac/cf;)Lmaps/ac/bt;
    .locals 4

    new-instance v0, Lmaps/ac/bt;

    iget v1, p0, Lmaps/ac/bt;->a:I

    iget v2, p0, Lmaps/ac/bt;->b:I

    iget v3, p0, Lmaps/ac/bt;->c:I

    invoke-direct {v0, v1, v2, v3, p1}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    return-object v0
.end method

.method public final a(Lmaps/ao/b;)Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-virtual {v0, p1}, Lmaps/ac/cf;->a(Lmaps/ao/b;)Lmaps/ac/cf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ac/bt;->a(Lmaps/ac/cf;)Lmaps/ac/bt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lmaps/ac/bx;)Lmaps/ac/bw;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-virtual {v0, p1}, Lmaps/ac/cf;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/DataOutput;)V
    .locals 1

    iget v0, p0, Lmaps/ac/bt;->a:I

    invoke-static {p1, v0}, Lmaps/ac/cl;->a(Ljava/io/DataOutput;I)V

    iget v0, p0, Lmaps/ac/bt;->b:I

    invoke-static {p1, v0}, Lmaps/ac/cl;->a(Ljava/io/DataOutput;I)V

    iget v0, p0, Lmaps/ac/bt;->c:I

    invoke-static {p1, v0}, Lmaps/ac/cl;->a(Ljava/io/DataOutput;I)V

    return-void
.end method

.method public final a(Lmaps/ao/b;Lmaps/bv/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-virtual {v0, p1, p2}, Lmaps/ac/cf;->a(Lmaps/ao/b;Lmaps/bv/a;)V

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lmaps/ac/bt;->a:I

    return v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lmaps/ac/bt;->b:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lmaps/ac/bt;

    iget v0, p0, Lmaps/ac/bt;->a:I

    iget v1, p1, Lmaps/ac/bt;->a:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lmaps/ac/bt;->b:I

    iget v1, p1, Lmaps/ac/bt;->b:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lmaps/ac/bt;->c:I

    iget v1, p1, Lmaps/ac/bt;->c:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    iget-object v1, p1, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-virtual {v0, v1}, Lmaps/ac/cf;->a(Lmaps/ac/cf;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/ac/bt;->c:I

    iget v1, p1, Lmaps/ac/bt;->c:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/ac/bt;->b:I

    iget v1, p1, Lmaps/ac/bt;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lmaps/ac/bt;->a:I

    iget v1, p1, Lmaps/ac/bt;->a:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lmaps/ac/bt;->c:I

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lmaps/ac/bt;->e:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lmaps/ac/bt;

    if-eqz v1, :cond_0

    check-cast p1, Lmaps/ac/bt;

    iget v1, p0, Lmaps/ac/bt;->b:I

    iget v2, p1, Lmaps/ac/bt;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lmaps/ac/bt;->c:I

    iget v2, p1, Lmaps/ac/bt;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lmaps/ac/bt;->a:I

    iget v2, p1, Lmaps/ac/bt;->a:I

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    iget-object v1, p1, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-virtual {v0, v1}, Lmaps/ac/cf;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/ac/bt;->f:I

    return v0
.end method

.method public final g()Lmaps/ac/av;
    .locals 3

    new-instance v0, Lmaps/ac/av;

    iget v1, p0, Lmaps/ac/bt;->e:I

    iget v2, p0, Lmaps/ac/bt;->f:I

    invoke-direct {v0, v1, v2}, Lmaps/ac/av;-><init>(II)V

    return-object v0
.end method

.method public final h()Lmaps/ac/av;
    .locals 4

    const/high16 v0, 0x40000000    # 2.0f

    iget v1, p0, Lmaps/ac/bt;->a:I

    shr-int/2addr v0, v1

    new-instance v1, Lmaps/ac/av;

    iget v2, p0, Lmaps/ac/bt;->e:I

    add-int/2addr v2, v0

    iget v3, p0, Lmaps/ac/bt;->f:I

    add-int/2addr v0, v3

    invoke-direct {v1, v2, v0}, Lmaps/ac/av;-><init>(II)V

    return-object v1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lmaps/ac/bt;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/ac/bt;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/ac/bt;->c:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-virtual {v1}, Lmaps/ac/cf;->b()Z

    move-result v1

    if-nez v1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-virtual {v1}, Lmaps/ac/cf;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public final i()Lmaps/ac/bd;
    .locals 6

    iget-object v0, p0, Lmaps/ac/bt;->i:Lmaps/ac/bd;

    if-nez v0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    iget v1, p0, Lmaps/ac/bt;->a:I

    shr-int/2addr v0, v1

    new-instance v1, Lmaps/ac/bd;

    new-instance v2, Lmaps/ac/av;

    iget v3, p0, Lmaps/ac/bt;->e:I

    iget v4, p0, Lmaps/ac/bt;->f:I

    invoke-direct {v2, v3, v4}, Lmaps/ac/av;-><init>(II)V

    new-instance v3, Lmaps/ac/av;

    iget v4, p0, Lmaps/ac/bt;->e:I

    add-int/2addr v4, v0

    iget v5, p0, Lmaps/ac/bt;->f:I

    add-int/2addr v0, v5

    invoke-direct {v3, v4, v0}, Lmaps/ac/av;-><init>(II)V

    invoke-direct {v1, v2, v3}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    iput-object v1, p0, Lmaps/ac/bt;->i:Lmaps/ac/bd;

    :cond_0
    iget-object v0, p0, Lmaps/ac/bt;->i:Lmaps/ac/bd;

    return-object v0
.end method

.method public final j()Lmaps/ac/cf;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lmaps/ac/bt;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/ac/bt;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/ac/bt;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
