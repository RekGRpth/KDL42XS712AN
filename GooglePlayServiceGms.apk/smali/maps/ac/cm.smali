.class public final Lmaps/ac/cm;
.super Ljava/lang/Object;


# instance fields
.field private a:F

.field private b:F


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/ac/cm;->a:F

    iput v0, p0, Lmaps/ac/cm;->b:F

    return-void
.end method

.method public static a(Lmaps/ac/av;Lmaps/ac/cm;Lmaps/ac/av;)Lmaps/ac/av;
    .locals 3

    iget v0, p0, Lmaps/ac/av;->a:I

    iget v1, p1, Lmaps/ac/cm;->a:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lmaps/ac/av;->b:I

    iget v2, p1, Lmaps/ac/cm;->b:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Lmaps/ac/av;->d(II)V

    return-object p2
.end method


# virtual methods
.method public final a()Lmaps/ac/cm;
    .locals 1

    iget v0, p0, Lmaps/ac/cm;->a:F

    neg-float v0, v0

    iput v0, p0, Lmaps/ac/cm;->a:F

    iget v0, p0, Lmaps/ac/cm;->b:F

    neg-float v0, v0

    iput v0, p0, Lmaps/ac/cm;->b:F

    return-object p0
.end method

.method public final a(F)Lmaps/ac/cm;
    .locals 1

    iget v0, p0, Lmaps/ac/cm;->a:F

    mul-float/2addr v0, p1

    iput v0, p0, Lmaps/ac/cm;->a:F

    iget v0, p0, Lmaps/ac/cm;->b:F

    mul-float/2addr v0, p1

    iput v0, p0, Lmaps/ac/cm;->b:F

    return-object p0
.end method

.method public final a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/cm;
    .locals 2

    iget v0, p2, Lmaps/ac/av;->a:I

    iget v1, p1, Lmaps/ac/av;->a:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lmaps/ac/cm;->a:F

    iget v0, p2, Lmaps/ac/av;->b:I

    iget v1, p1, Lmaps/ac/av;->b:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lmaps/ac/cm;->b:F

    return-object p0
.end method

.method public final a(Lmaps/ac/cm;)Lmaps/ac/cm;
    .locals 1

    iget v0, p1, Lmaps/ac/cm;->a:F

    iput v0, p0, Lmaps/ac/cm;->a:F

    iget v0, p1, Lmaps/ac/cm;->b:F

    iput v0, p0, Lmaps/ac/cm;->b:F

    return-object p0
.end method

.method public final b()F
    .locals 1

    invoke-virtual {p0, p0}, Lmaps/ac/cm;->c(Lmaps/ac/cm;)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method public final b(Lmaps/ac/cm;)Lmaps/ac/cm;
    .locals 2

    iget v0, p0, Lmaps/ac/cm;->a:F

    iget v1, p1, Lmaps/ac/cm;->a:F

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/ac/cm;->a:F

    iget v0, p0, Lmaps/ac/cm;->b:F

    iget v1, p1, Lmaps/ac/cm;->b:F

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/ac/cm;->b:F

    return-object p0
.end method

.method public final c(Lmaps/ac/cm;)F
    .locals 3

    iget v0, p0, Lmaps/ac/cm;->a:F

    iget v1, p1, Lmaps/ac/cm;->a:F

    mul-float/2addr v0, v1

    iget v1, p0, Lmaps/ac/cm;->b:F

    iget v2, p1, Lmaps/ac/cm;->b:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final c()Lmaps/ac/cm;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lmaps/ac/cm;->b()F

    move-result v0

    cmpl-float v1, v0, v2

    if-nez v1, :cond_0

    iput v2, p0, Lmaps/ac/cm;->a:F

    iput v2, p0, Lmaps/ac/cm;->b:F

    :goto_0
    return-object p0

    :cond_0
    iget v1, p0, Lmaps/ac/cm;->a:F

    div-float/2addr v1, v0

    iput v1, p0, Lmaps/ac/cm;->a:F

    iget v1, p0, Lmaps/ac/cm;->b:F

    div-float v0, v1, v0

    iput v0, p0, Lmaps/ac/cm;->b:F

    goto :goto_0
.end method

.method public final d()Lmaps/ac/cm;
    .locals 2

    iget v0, p0, Lmaps/ac/cm;->a:F

    iget v1, p0, Lmaps/ac/cm;->b:F

    neg-float v1, v1

    iput v1, p0, Lmaps/ac/cm;->a:F

    iput v0, p0, Lmaps/ac/cm;->b:F

    return-object p0
.end method

.method public final d(Lmaps/ac/cm;)Z
    .locals 3

    iget v0, p0, Lmaps/ac/cm;->a:F

    iget v1, p1, Lmaps/ac/cm;->b:F

    mul-float/2addr v0, v1

    iget v1, p1, Lmaps/ac/cm;->a:F

    iget v2, p0, Lmaps/ac/cm;->b:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lmaps/ac/cm;->a:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/ac/cm;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/ac/cm;

    iget v2, p0, Lmaps/ac/cm;->a:F

    iget v3, p1, Lmaps/ac/cm;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lmaps/ac/cm;->b:F

    iget v3, p1, Lmaps/ac/cm;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lmaps/ac/cm;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lmaps/ac/cm;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lmaps/ac/cm;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ac/cm;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
