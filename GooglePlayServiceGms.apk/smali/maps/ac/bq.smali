.class public final Lmaps/ac/bq;
.super Ljava/lang/Object;


# static fields
.field private static c:Lmaps/ac/bq;


# instance fields
.field private final a:I

.field private final b:Lmaps/ac/bk;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/ac/bq;

    const/4 v1, 0x0

    invoke-static {}, Lmaps/ac/bk;->a()Lmaps/ac/bk;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/ac/bq;-><init>(ILmaps/ac/bk;)V

    sput-object v0, Lmaps/ac/bq;->c:Lmaps/ac/bq;

    return-void
.end method

.method public constructor <init>(ILmaps/ac/bk;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/ac/bq;->a:I

    iput-object p2, p0, Lmaps/ac/bq;->b:Lmaps/ac/bk;

    return-void
.end method

.method public static a()Lmaps/ac/bq;
    .locals 1

    sget-object v0, Lmaps/ac/bq;->c:Lmaps/ac/bq;

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lmaps/ac/bq;
    .locals 3

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    invoke-static {p0}, Lmaps/ac/bk;->a(Ljava/io/DataInput;)Lmaps/ac/bk;

    move-result-object v1

    new-instance v2, Lmaps/ac/bq;

    invoke-direct {v2, v0, v1}, Lmaps/ac/bq;-><init>(ILmaps/ac/bk;)V

    return-object v2
.end method


# virtual methods
.method public final b()I
    .locals 1

    iget v0, p0, Lmaps/ac/bq;->a:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/ac/bq;

    iget v2, p0, Lmaps/ac/bq;->a:I

    iget v3, p1, Lmaps/ac/bq;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lmaps/ac/bq;->b:Lmaps/ac/bk;

    if-nez v2, :cond_5

    iget-object v2, p1, Lmaps/ac/bq;->b:Lmaps/ac/bk;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lmaps/ac/bq;->b:Lmaps/ac/bk;

    iget-object v3, p1, Lmaps/ac/bq;->b:Lmaps/ac/bk;

    invoke-virtual {v2, v3}, Lmaps/ac/bk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lmaps/ac/bq;->a:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lmaps/ac/bq;->b:Lmaps/ac/bk;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/bq;->b:Lmaps/ac/bk;

    invoke-virtual {v0}, Lmaps/ac/bk;->hashCode()I

    move-result v0

    goto :goto_0
.end method
