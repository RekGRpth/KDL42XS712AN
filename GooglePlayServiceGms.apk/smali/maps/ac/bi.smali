.class public final Lmaps/ac/bi;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/bw;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/bx;
    .locals 1

    sget-object v0, Lmaps/ac/bx;->a:Lmaps/ac/bx;

    return-object v0
.end method

.method public final a(Lmaps/bv/a;)V
    .locals 2

    const/16 v0, 0xa

    iget-object v1, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    return-void
.end method

.method public final a(Lmaps/ac/bw;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/ac/bi;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Lmaps/ao/b;)Z
    .locals 1

    sget-object v0, Lmaps/ao/b;->r:Lmaps/ao/b;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lmaps/ac/bw;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lmaps/ac/bi;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    iget-object v2, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lmaps/ac/bi;

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/ac/bi;

    iget-object v0, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    iget-object v1, p1, Lmaps/ac/bi;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/bi;->a:Ljava/lang/String;

    goto :goto_0
.end method
