.class final Lmaps/ac/cv;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/cu;


# instance fields
.field private a:I

.field private b:I

.field private synthetic c:Lmaps/ac/cs;


# direct methods
.method private constructor <init>(Lmaps/ac/cs;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lmaps/ac/cv;->c:Lmaps/ac/cs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/ac/cv;->a:I

    iput v0, p0, Lmaps/ac/cv;->b:I

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ac/cs;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ac/cv;-><init>(Lmaps/ac/cs;)V

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/n;
    .locals 2

    iget-object v0, p0, Lmaps/ac/cv;->c:Lmaps/ac/cs;

    invoke-static {v0}, Lmaps/ac/cs;->a(Lmaps/ac/cs;)[Lmaps/ac/n;

    move-result-object v0

    iget v1, p0, Lmaps/ac/cv;->a:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final b()V
    .locals 1

    iget v0, p0, Lmaps/ac/cv;->a:I

    iput v0, p0, Lmaps/ac/cv;->b:I

    return-void
.end method

.method public final c()V
    .locals 1

    iget v0, p0, Lmaps/ac/cv;->b:I

    iput v0, p0, Lmaps/ac/cv;->a:I

    return-void
.end method

.method public final hasNext()Z
    .locals 2

    iget v0, p0, Lmaps/ac/cv;->a:I

    iget-object v1, p0, Lmaps/ac/cv;->c:Lmaps/ac/cs;

    invoke-static {v1}, Lmaps/ac/cs;->a(Lmaps/ac/cs;)[Lmaps/ac/n;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lmaps/ac/cv;->c:Lmaps/ac/cs;

    invoke-static {v0}, Lmaps/ac/cs;->a(Lmaps/ac/cs;)[Lmaps/ac/n;

    move-result-object v0

    iget v1, p0, Lmaps/ac/cv;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/ac/cv;->a:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
