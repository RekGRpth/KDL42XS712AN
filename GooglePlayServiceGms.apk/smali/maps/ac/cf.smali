.class public final Lmaps/ac/cf;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lmaps/ac/cf;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-static {v0}, Lmaps/m/co;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(Lmaps/ac/cf;Lmaps/ac/bx;)Lmaps/ac/bw;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lmaps/ac/cf;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lmaps/ac/cf;)I
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Lmaps/ac/bx;->values()[Lmaps/ac/bx;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    invoke-virtual {p0, v0}, Lmaps/ac/cf;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v5

    invoke-virtual {p1, v0}, Lmaps/ac/cf;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v0

    if-nez v5, :cond_1

    if-eqz v0, :cond_2

    const/4 v0, -0x1

    :cond_0
    :goto_1
    return v0

    :cond_1
    invoke-interface {v5, v0}, Lmaps/ac/bw;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lmaps/ac/bx;)Lmaps/ac/bw;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bw;

    return-object v0
.end method

.method public final a(Lmaps/ao/b;)Lmaps/ac/cf;
    .locals 4

    new-instance v1, Lmaps/ac/cf;

    invoke-direct {v1}, Lmaps/ac/cf;-><init>()V

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bw;

    invoke-interface {v0, p1}, Lmaps/ac/bw;->a(Lmaps/ao/b;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Lmaps/ac/cf;->a(Lmaps/ac/bw;)V

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public final a(Lmaps/ac/bw;)V
    .locals 2

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/ac/bw;->a()Lmaps/ac/bx;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Lmaps/ao/b;Lmaps/bv/a;)V
    .locals 3

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bw;

    invoke-interface {v0, p1}, Lmaps/ac/bw;->a(Lmaps/ao/b;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p2}, Lmaps/ac/bw;->a(Lmaps/bv/a;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/ac/cf;

    invoke-virtual {p0, p1}, Lmaps/ac/cf;->a(Lmaps/ac/cf;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    check-cast p1, Lmaps/ac/cf;

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    iget-object v1, p1, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    return v0

    :cond_1
    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/cf;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
