.class public final Lmaps/ac/k;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/j;


# static fields
.field private static c:Lmaps/ac/bd;


# instance fields
.field private a:Ljava/util/List;

.field private b:Lmaps/ac/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/high16 v1, -0x80000000

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0, v1, v1}, Lmaps/ac/av;-><init>(II)V

    new-instance v1, Lmaps/ac/bd;

    invoke-direct {v1, v0, v0}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    sput-object v1, Lmaps/ac/k;->c:Lmaps/ac/bd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ac/k;->a:Ljava/util/List;

    sget-object v0, Lmaps/ac/k;->c:Lmaps/ac/bd;

    iput-object v0, p0, Lmaps/ac/k;->b:Lmaps/ac/bd;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/m/ck;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/k;->a:Ljava/util/List;

    sget-object v0, Lmaps/ac/k;->c:Lmaps/ac/bd;

    iput-object v0, p0, Lmaps/ac/k;->b:Lmaps/ac/bd;

    return-void
.end method

.method private constructor <init>(Ljava/util/Collection;)V
    .locals 2

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/ac/k;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/j;

    invoke-virtual {p0, v0}, Lmaps/ac/k;->a(Lmaps/ac/j;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public varargs constructor <init>([Lmaps/ac/j;)V
    .locals 1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ac/k;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/bd;
    .locals 1

    iget-object v0, p0, Lmaps/ac/k;->b:Lmaps/ac/bd;

    return-object v0
.end method

.method public final a(Lmaps/ac/j;)V
    .locals 3

    invoke-interface {p1}, Lmaps/ac/j;->a()Lmaps/ac/bd;

    move-result-object v0

    sget-object v1, Lmaps/ac/k;->c:Lmaps/ac/bd;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lmaps/ac/k;->b:Lmaps/ac/bd;

    sget-object v2, Lmaps/ac/k;->c:Lmaps/ac/bd;

    if-ne v1, v2, :cond_1

    new-instance v1, Lmaps/ac/bd;

    iget-object v2, v0, Lmaps/ac/bd;->a:Lmaps/ac/av;

    invoke-static {v2}, Lmaps/ac/av;->a(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v2

    iget-object v0, v0, Lmaps/ac/bd;->b:Lmaps/ac/av;

    invoke-static {v0}, Lmaps/ac/av;->a(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    iput-object v1, p0, Lmaps/ac/k;->b:Lmaps/ac/bd;

    :goto_1
    iget-object v0, p0, Lmaps/ac/k;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lmaps/ac/k;->b:Lmaps/ac/bd;

    invoke-virtual {v1, v0}, Lmaps/ac/bd;->a(Lmaps/ac/bd;)V

    goto :goto_1
.end method

.method public final a(Lmaps/ac/av;)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/ac/k;->b:Lmaps/ac/bd;

    invoke-virtual {v0, p1}, Lmaps/ac/bd;->a(Lmaps/ac/av;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lmaps/ac/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lmaps/ac/k;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/j;

    invoke-interface {v0, p1}, Lmaps/ac/j;->a(Lmaps/ac/av;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final a(Lmaps/ac/be;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/ac/be;->a()Lmaps/ac/bd;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/k;->b:Lmaps/ac/bd;

    invoke-virtual {v1, v0}, Lmaps/ac/bd;->a(Lmaps/ac/be;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lmaps/ac/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lmaps/ac/k;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/j;

    invoke-interface {v0, p1}, Lmaps/ac/j;->a(Lmaps/ac/be;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
