.class public Lmaps/ac/z;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lmaps/ac/r;

.field private final b:Ljava/util/List;

.field private final c:I

.field private final d:Lmaps/ac/av;

.field private final e:J

.field private f:Z


# direct methods
.method public constructor <init>(Lmaps/ac/r;Ljava/util/List;IZLmaps/ac/av;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/z;->a:Lmaps/ac/r;

    iput-object p2, p0, Lmaps/ac/z;->b:Ljava/util/List;

    iput p3, p0, Lmaps/ac/z;->c:I

    iput-boolean p4, p0, Lmaps/ac/z;->f:Z

    iput-object p5, p0, Lmaps/ac/z;->d:Lmaps/ac/av;

    iput-wide p6, p0, Lmaps/ac/z;->e:J

    return-void
.end method

.method public static a(Lmaps/bv/a;J)Lmaps/ac/z;
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmaps/ac/r;->b(Ljava/lang/String;)Lmaps/ac/r;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v5

    :cond_0
    invoke-virtual {p0, v7}, Lmaps/bv/a;->j(I)I

    move-result v6

    invoke-static {v6}, Lmaps/m/ck;->a(I)Ljava/util/ArrayList;

    move-result-object v2

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_2

    invoke-virtual {p0, v7, v3}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v4

    invoke-static {v4}, Lmaps/ac/aa;->a(Lmaps/bv/a;)Lmaps/ac/aa;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Lmaps/bv/a;->b(I)Z

    move-result v4

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lmaps/bv/a;->d(I)I

    move-result v3

    if-ltz v3, :cond_3

    if-lt v3, v6, :cond_4

    :cond_3
    move v3, v0

    :cond_4
    if-nez v4, :cond_5

    if-nez v6, :cond_6

    :cond_5
    const/4 v3, -0x1

    :cond_6
    invoke-virtual {p0, v8}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, v8}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    invoke-static {v0}, Lmaps/ac/av;->a(Lmaps/bv/a;)Lmaps/ac/av;

    move-result-object v5

    :cond_7
    new-instance v0, Lmaps/ac/z;

    move-wide v6, p1

    invoke-direct/range {v0 .. v7}, Lmaps/ac/z;-><init>(Lmaps/ac/r;Ljava/util/List;IZLmaps/ac/av;J)V

    move-object v5, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lmaps/ac/ad;)Lmaps/ac/aa;
    .locals 1

    invoke-virtual {p1}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ac/z;->a(Lmaps/ac/r;)Lmaps/ac/aa;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lmaps/ac/r;)Lmaps/ac/aa;
    .locals 3

    iget-object v0, p0, Lmaps/ac/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/aa;

    invoke-virtual {v0}, Lmaps/ac/aa;->b()Lmaps/ac/r;

    move-result-object v2

    invoke-virtual {p1, v2}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lmaps/ac/r;
    .locals 1

    iget-object v0, p0, Lmaps/ac/z;->a:Lmaps/ac/r;

    return-object v0
.end method

.method public final b(Lmaps/ac/ad;)I
    .locals 2

    invoke-virtual {p1}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ac/z;->a(Lmaps/ac/r;)Lmaps/ac/aa;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lmaps/ac/aa;->b()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ac/z;->a(Lmaps/ac/r;)Lmaps/ac/aa;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/z;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ac/z;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c()Lmaps/ac/aa;
    .locals 2

    iget v0, p0, Lmaps/ac/z;->c:I

    if-ltz v0, :cond_0

    iget-object v1, p0, Lmaps/ac/z;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lmaps/ac/z;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/aa;

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ac/z;->f:Z

    return v0
.end method

.method public final e()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ac/z;->d:Lmaps/ac/av;

    return-object v0
.end method

.method public final f()Z
    .locals 4

    iget-wide v0, p0, Lmaps/ac/z;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lmaps/bs/b;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/ac/z;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Building: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/ac/z;->a:Lmaps/ac/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
