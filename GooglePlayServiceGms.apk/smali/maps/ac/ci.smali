.class public final Lmaps/ac/ci;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/Integer;


# instance fields
.field private b:Lmaps/ac/o;

.field private c:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lmaps/ac/ci;->a:[Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/ac/ci;->c:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/ch;
    .locals 4

    iget-object v0, p0, Lmaps/ac/ci;->c:Ljava/util/Set;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ac/ci;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    :cond_0
    new-instance v1, Lmaps/ac/ch;

    iget-object v2, p0, Lmaps/ac/ci;->b:Lmaps/ac/o;

    iget-object v0, p0, Lmaps/ac/ci;->c:Ljava/util/Set;

    sget-object v3, Lmaps/ac/ci;->a:[Ljava/lang/Integer;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    invoke-direct {v1, v2, v0}, Lmaps/ac/ch;-><init>(Lmaps/ac/o;[Ljava/lang/Integer;)V

    return-object v1
.end method

.method public final a(I)Lmaps/ac/ci;
    .locals 2

    iget-object v0, p0, Lmaps/ac/ci;->c:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lmaps/ac/o;)Lmaps/ac/ci;
    .locals 0

    iput-object p1, p0, Lmaps/ac/ci;->b:Lmaps/ac/o;

    return-object p0
.end method
