.class public final Lmaps/ac/bc;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/n;


# instance fields
.field private final a:[B

.field private final b:I

.field private final c:Lmaps/ac/bl;

.field private final d:[I


# direct methods
.method public constructor <init>([BILmaps/ac/bl;[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/bc;->a:[B

    iput p2, p0, Lmaps/ac/bc;->b:I

    iput-object p3, p0, Lmaps/ac/bc;->c:Lmaps/ac/bl;

    iput-object p4, p0, Lmaps/ac/bc;->d:[I

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/o;
    .locals 1

    sget-object v0, Lmaps/ac/o;->a:Lmaps/ac/o;

    return-object v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lmaps/ac/bc;->a:[B

    return-object v0
.end method

.method public final d()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bc;->c:Lmaps/ac/bl;

    return-object v0
.end method

.method public final e()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/ac/bc;->b:I

    return v0
.end method

.method public final i()[I
    .locals 1

    iget-object v0, p0, Lmaps/ac/bc;->d:[I

    return-object v0
.end method

.method public final j()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/bc;->a:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x24

    return v0
.end method
