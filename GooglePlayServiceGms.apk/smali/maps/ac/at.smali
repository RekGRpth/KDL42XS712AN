.class final Lmaps/ac/at;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/cu;


# instance fields
.field private a:I

.field private b:I

.field private synthetic c:Lmaps/ac/as;


# direct methods
.method private constructor <init>(Lmaps/ac/as;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lmaps/ac/at;->c:Lmaps/ac/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/ac/at;->a:I

    iput v0, p0, Lmaps/ac/at;->b:I

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ac/as;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ac/at;-><init>(Lmaps/ac/as;)V

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/n;
    .locals 2

    iget-object v0, p0, Lmaps/ac/at;->c:Lmaps/ac/as;

    invoke-static {v0}, Lmaps/ac/as;->a(Lmaps/ac/as;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lmaps/ac/at;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    return-object v0
.end method

.method public final b()V
    .locals 1

    iget v0, p0, Lmaps/ac/at;->a:I

    iput v0, p0, Lmaps/ac/at;->b:I

    return-void
.end method

.method public final c()V
    .locals 1

    iget v0, p0, Lmaps/ac/at;->b:I

    iput v0, p0, Lmaps/ac/at;->a:I

    return-void
.end method

.method public final hasNext()Z
    .locals 2

    iget v0, p0, Lmaps/ac/at;->a:I

    iget-object v1, p0, Lmaps/ac/at;->c:Lmaps/ac/as;

    invoke-static {v1}, Lmaps/ac/as;->a(Lmaps/ac/as;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lmaps/ac/at;->c:Lmaps/ac/as;

    invoke-static {v0}, Lmaps/ac/as;->a(Lmaps/ac/as;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lmaps/ac/at;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/ac/at;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    return-object v0
.end method

.method public final remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
