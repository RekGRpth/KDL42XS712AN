.class public final Lmaps/ac/cx;
.super Lmaps/ac/cy;


# instance fields
.field private b:Lmaps/ac/bd;

.field private c:Lmaps/ac/av;

.field private d:Lmaps/ac/av;

.field private e:I

.field private f:I

.field private g:I

.field private volatile h:Lmaps/ac/av;

.field private volatile i:Lmaps/ac/av;

.field private volatile j:Lmaps/ac/av;

.field private volatile k:Lmaps/ac/av;

.field private volatile l:Lmaps/ac/av;

.field private volatile m:Lmaps/ac/av;


# direct methods
.method private constructor <init>(Lmaps/ac/bd;)V
    .locals 4

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {p0}, Lmaps/ac/cy;-><init>()V

    iput-object p1, p0, Lmaps/ac/cx;->b:Lmaps/ac/bd;

    invoke-virtual {p1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/ac/bd;->d()Lmaps/ac/av;

    move-result-object v2

    iget v0, v1, Lmaps/ac/av;->a:I

    if-gez v0, :cond_1

    iget v0, v1, Lmaps/ac/av;->a:I

    neg-int v0, v0

    iput v0, p0, Lmaps/ac/cx;->e:I

    :cond_0
    :goto_0
    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget-object v0, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    invoke-virtual {v1, v0}, Lmaps/ac/av;->i(Lmaps/ac/av;)V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget-object v0, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    invoke-virtual {v2, v0}, Lmaps/ac/av;->i(Lmaps/ac/av;)V

    iget-object v0, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v0, v0, Lmaps/ac/av;->a:I

    iget-object v3, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v3, v3, Lmaps/ac/av;->a:I

    if-le v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lmaps/ac/cx;->a:Z

    iget v0, v1, Lmaps/ac/av;->a:I

    iget v1, p0, Lmaps/ac/cx;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ac/cx;->f:I

    iget v0, v2, Lmaps/ac/av;->a:I

    iget v1, p0, Lmaps/ac/cx;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ac/cx;->g:I

    return-void

    :cond_1
    iget v0, v2, Lmaps/ac/av;->a:I

    if-le v0, v3, :cond_0

    iget v0, v2, Lmaps/ac/av;->a:I

    sub-int v0, v3, v0

    iput v0, p0, Lmaps/ac/cx;->e:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lmaps/ac/bd;)Lmaps/ac/cx;
    .locals 1

    new-instance v0, Lmaps/ac/cx;

    invoke-direct {v0, p0}, Lmaps/ac/cx;-><init>(Lmaps/ac/bd;)V

    return-object v0
.end method


# virtual methods
.method public final a(I)Lmaps/ac/av;
    .locals 3

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lmaps/ac/cx;->h:Lmaps/ac/av;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ac/av;

    iget-object v1, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->a:I

    iget-object v2, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/ac/av;-><init>(II)V

    iput-object v0, p0, Lmaps/ac/cx;->h:Lmaps/ac/av;

    :cond_0
    iget-object v0, p0, Lmaps/ac/cx;->h:Lmaps/ac/av;

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/ac/cx;->i:Lmaps/ac/av;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/ac/av;

    iget-object v1, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->a:I

    iget-object v2, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/ac/av;-><init>(II)V

    iput-object v0, p0, Lmaps/ac/cx;->i:Lmaps/ac/av;

    :cond_1
    iget-object v0, p0, Lmaps/ac/cx;->i:Lmaps/ac/av;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a()Lmaps/ac/cx;
    .locals 0

    return-object p0
.end method

.method public final a(I[Lmaps/ac/av;)V
    .locals 6

    const/4 v1, 0x3

    const/4 v5, 0x2

    const v2, -0x20000001

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lmaps/ac/cx;->a:Z

    if-eqz v0, :cond_4

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v3}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v3

    invoke-virtual {p0, v4}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v4}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v3

    iget-object v0, p0, Lmaps/ac/cx;->j:Lmaps/ac/av;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ac/av;

    iget-object v1, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->b:I

    invoke-direct {v0, v2, v1}, Lmaps/ac/av;-><init>(II)V

    iput-object v0, p0, Lmaps/ac/cx;->j:Lmaps/ac/av;

    :cond_0
    iget-object v0, p0, Lmaps/ac/cx;->j:Lmaps/ac/av;

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/ac/cx;->k:Lmaps/ac/av;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/ac/av;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/ac/av;-><init>(II)V

    iput-object v0, p0, Lmaps/ac/cx;->k:Lmaps/ac/av;

    :cond_1
    iget-object v0, p0, Lmaps/ac/cx;->k:Lmaps/ac/av;

    aput-object v0, p2, v3

    invoke-virtual {p0, v5}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v5}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v3

    invoke-virtual {p0, v1}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v1}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v3

    iget-object v0, p0, Lmaps/ac/cx;->l:Lmaps/ac/av;

    if-nez v0, :cond_2

    new-instance v0, Lmaps/ac/av;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->b:I

    invoke-direct {v0, v1, v2}, Lmaps/ac/av;-><init>(II)V

    iput-object v0, p0, Lmaps/ac/cx;->l:Lmaps/ac/av;

    :cond_2
    iget-object v0, p0, Lmaps/ac/cx;->l:Lmaps/ac/av;

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lmaps/ac/cx;->m:Lmaps/ac/av;

    if-nez v0, :cond_3

    new-instance v0, Lmaps/ac/av;

    iget-object v1, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->b:I

    invoke-direct {v0, v2, v1}, Lmaps/ac/av;-><init>(II)V

    iput-object v0, p0, Lmaps/ac/cx;->m:Lmaps/ac/av;

    :cond_3
    iget-object v0, p0, Lmaps/ac/cx;->m:Lmaps/ac/av;

    aput-object v0, p2, v3

    invoke-virtual {p0, v3}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v4

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0, p1}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v3

    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lmaps/ac/cx;->a(I)Lmaps/ac/av;

    move-result-object v0

    aput-object v0, p2, v4

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lmaps/ac/av;)Z
    .locals 2

    iget v0, p1, Lmaps/ac/av;->a:I

    iget v1, p0, Lmaps/ac/cx;->e:I

    add-int/2addr v0, v1

    const v1, 0x3fffffff    # 1.9999999f

    and-int/2addr v0, v1

    iget v1, p0, Lmaps/ac/cx;->f:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lmaps/ac/cx;->g:I

    if-gt v0, v1, :cond_0

    iget v0, p1, Lmaps/ac/av;->b:I

    iget-object v1, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->b:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lmaps/ac/av;->b:I

    iget-object v1, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->b:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lmaps/ac/be;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v7, 0x20000000

    const/high16 v6, -0x20000000

    const/high16 v5, 0x40000000    # 2.0f

    iget-boolean v0, p0, Lmaps/ac/cx;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ac/cx;->b:Lmaps/ac/bd;

    invoke-virtual {v0, p1}, Lmaps/ac/bd;->b(Lmaps/ac/be;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lmaps/ac/be;->a()Lmaps/ac/bd;

    move-result-object v0

    iget-object v3, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v3, v3, Lmaps/ac/av;->b:I

    iget-object v4, v0, Lmaps/ac/bd;->a:Lmaps/ac/av;

    iget v4, v4, Lmaps/ac/av;->b:I

    if-gt v3, v4, :cond_1

    iget-object v3, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v3, v3, Lmaps/ac/av;->b:I

    iget-object v4, v0, Lmaps/ac/bd;->b:Lmaps/ac/av;

    iget v4, v4, Lmaps/ac/av;->b:I

    if-ge v3, v4, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v3, v0, Lmaps/ac/bd;->a:Lmaps/ac/av;

    iget v3, v3, Lmaps/ac/av;->a:I

    iget-object v0, v0, Lmaps/ac/bd;->b:Lmaps/ac/av;

    iget v0, v0, Lmaps/ac/av;->a:I

    iget-object v4, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v4, v4, Lmaps/ac/av;->a:I

    if-gt v4, v3, :cond_3

    if-gt v7, v0, :cond_4

    :cond_3
    if-gt v6, v3, :cond_5

    iget-object v4, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v4, v4, Lmaps/ac/av;->a:I

    if-lt v4, v0, :cond_5

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    if-ge v3, v6, :cond_8

    add-int/2addr v3, v5

    :cond_6
    :goto_1
    if-ge v0, v6, :cond_9

    add-int/2addr v0, v5

    :cond_7
    :goto_2
    iget-object v4, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v4, v4, Lmaps/ac/av;->a:I

    if-gt v4, v3, :cond_a

    iget-object v3, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v3, v3, Lmaps/ac/av;->a:I

    if-lt v3, v0, :cond_a

    move v0, v1

    goto :goto_0

    :cond_8
    if-lt v3, v7, :cond_6

    sub-int/2addr v3, v5

    goto :goto_1

    :cond_9
    if-lt v0, v7, :cond_7

    sub-int/2addr v0, v5

    goto :goto_2

    :cond_a
    move v0, v2

    goto :goto_0
.end method

.method public final b()Lmaps/ac/bd;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cx;->b:Lmaps/ac/bd;

    return-object v0
.end method

.method public final b(Lmaps/ac/be;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lmaps/ac/cx;->a:Z

    if-nez v1, :cond_1

    iget-object v0, p0, Lmaps/ac/cx;->b:Lmaps/ac/bd;

    invoke-virtual {v0, p1}, Lmaps/ac/bd;->a(Lmaps/ac/be;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lmaps/ac/bd;

    if-eqz v1, :cond_4

    check-cast p1, Lmaps/ac/bd;

    iget-object v1, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->b:I

    iget-object v2, p1, Lmaps/ac/bd;->b:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->b:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->b:I

    iget-object v2, p1, Lmaps/ac/bd;->a:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->b:I

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->a:I

    iget-object v2, p1, Lmaps/ac/bd;->b:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->a:I

    if-gt v1, v2, :cond_2

    const/high16 v1, 0x20000000

    iget-object v2, p1, Lmaps/ac/bd;->a:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->a:I

    if-gt v1, v2, :cond_3

    :cond_2
    const/high16 v1, -0x20000000

    iget-object v2, p1, Lmaps/ac/bd;->b:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->a:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->a:I

    iget-object v2, p1, Lmaps/ac/bd;->a:Lmaps/ac/av;

    iget v2, v2, Lmaps/ac/av;->a:I

    if-lt v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Lmaps/ac/cy;->b(Lmaps/ac/be;)Z

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic c()Lmaps/ac/be;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cx;->b:Lmaps/ac/bd;

    return-object v0
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/cx;->b:Lmaps/ac/bd;

    invoke-virtual {v0}, Lmaps/ac/bd;->f()I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 2

    iget-object v0, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    iget v0, v0, Lmaps/ac/av;->b:I

    iget-object v1, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    iget v1, v1, Lmaps/ac/av;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final f()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cx;->c:Lmaps/ac/av;

    return-object v0
.end method

.method public final g()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cx;->d:Lmaps/ac/av;

    return-object v0
.end method

.method public final h()I
    .locals 1

    iget-boolean v0, p0, Lmaps/ac/cx;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
