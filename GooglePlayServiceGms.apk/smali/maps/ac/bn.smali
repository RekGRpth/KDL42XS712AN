.class public final Lmaps/ac/bn;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ac/bn;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bp;)Lmaps/ac/bn;
    .locals 7

    new-instance v1, Lmaps/ac/bn;

    invoke-direct {v1}, Lmaps/ac/bn;-><init>()V

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, v1, Lmaps/ac/bn;->a:Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v5

    new-instance v6, Lmaps/ac/bm;

    invoke-virtual {p1, v5}, Lmaps/ac/bp;->a(I)Lmaps/ac/bl;

    move-result-object v5

    invoke-direct {v6, v4, v5}, Lmaps/ac/bm;-><init>(Ljava/lang/String;Lmaps/ac/bl;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(I)Lmaps/ac/bm;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/bn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bm;

    goto :goto_0
.end method
