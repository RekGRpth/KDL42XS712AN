.class public final Lmaps/ac/bo;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ac/bl;

.field private final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method public constructor <init>(Lmaps/ac/bl;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/bo;->a:Lmaps/ac/bl;

    iput-object p2, p0, Lmaps/ac/bo;->b:Ljava/lang/String;

    iput p3, p0, Lmaps/ac/bo;->c:I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;
    .locals 4

    invoke-virtual {p1}, Lmaps/ac/bv;->a()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    invoke-virtual {p1, v2}, Lmaps/ac/bv;->b(I)Lmaps/ac/bm;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmaps/ac/bm;->b()Lmaps/ac/bl;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/ac/bm;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v3, Lmaps/ac/bo;

    invoke-direct {v3, v0, v1, v2}, Lmaps/ac/bo;-><init>(Lmaps/ac/bl;Ljava/lang/String;I)V

    return-object v3

    :cond_0
    invoke-static {}, Lmaps/ac/bl;->a()Lmaps/ac/bl;

    move-result-object v0

    const-string v1, ""

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-virtual {p1, v0}, Lmaps/ac/bv;->a(I)Lmaps/ac/bl;

    move-result-object v0

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    invoke-virtual {p1, v2}, Lmaps/ac/bv;->c(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bo;->a:Lmaps/ac/bl;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lmaps/ac/bo;->c:I

    return v0
.end method
