.class public final Lmaps/ac/g;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/n;


# static fields
.field private static final l:[B

.field private static final m:[I


# instance fields
.field private final a:Lmaps/ac/o;

.field private final b:Lmaps/ac/cj;

.field private final c:Lmaps/ac/cj;

.field private final d:[B

.field private final e:Lmaps/ac/bl;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/ac/g;->l:[B

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lmaps/ac/g;->m:[I

    return-void

    :array_0
    .array-data 1
        0x1t
        0x2t
        0x4t
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1
        0x1
        0x2
        0x1
        0x2
        0x2
        0x3
    .end array-data
.end method

.method private constructor <init>(Lmaps/ac/o;Lmaps/ac/cj;Lmaps/ac/cj;[BIILmaps/ac/bl;Ljava/lang/String;II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/g;->a:Lmaps/ac/o;

    iput-object p2, p0, Lmaps/ac/g;->b:Lmaps/ac/cj;

    iput-object p3, p0, Lmaps/ac/g;->c:Lmaps/ac/cj;

    iput-object p4, p0, Lmaps/ac/g;->d:[B

    iput p5, p0, Lmaps/ac/g;->g:I

    iput p6, p0, Lmaps/ac/g;->h:I

    iput-object p7, p0, Lmaps/ac/g;->e:Lmaps/ac/bl;

    iput-object p8, p0, Lmaps/ac/g;->f:Ljava/lang/String;

    iput p9, p0, Lmaps/ac/g;->i:I

    iput p10, p0, Lmaps/ac/g;->j:I

    iput-object p11, p0, Lmaps/ac/g;->k:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/g;
    .locals 13

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lmaps/ac/bv;->b()Lmaps/ac/bt;

    move-result-object v0

    invoke-static {p0, v0}, Lmaps/ac/cj;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/cj;

    move-result-object v2

    invoke-static {p0, p1}, Lmaps/ac/cj;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/cj;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/cj;->a()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v8

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lmaps/ac/cj;->a()I

    move-result v0

    :goto_1
    new-array v4, v0, [B

    invoke-interface {p0, v4}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v5

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v6

    invoke-static {p0, p1}, Lmaps/ac/bo;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;

    move-result-object v12

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v9

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v10

    const/4 v1, 0x0

    invoke-static {v8, v10}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lmaps/ac/o;->a(Ljava/io/DataInput;)Lmaps/ac/p;

    move-result-object v1

    :cond_0
    :goto_2
    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v8

    new-array v11, v8, [I

    move v0, v7

    :goto_3
    if-ge v0, v8, :cond_4

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v7

    aput v7, v11, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_1
    move v0, v7

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lmaps/ac/cj;->a()I

    move-result v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    invoke-static {v0, v10}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lmaps/ac/o;->b(Ljava/io/DataInput;)Lmaps/ac/q;

    move-result-object v1

    goto :goto_2

    :cond_4
    new-instance v0, Lmaps/ac/g;

    invoke-virtual {p2}, Lmaps/ac/bf;->a()I

    invoke-virtual {v12}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v7

    invoke-virtual {v12}, Lmaps/ac/bo;->c()I

    invoke-virtual {v12}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v11}, Lmaps/ac/g;-><init>(Lmaps/ac/o;Lmaps/ac/cj;Lmaps/ac/cj;[BIILmaps/ac/bl;Ljava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public final a()Lmaps/ac/o;
    .locals 1

    iget-object v0, p0, Lmaps/ac/g;->a:Lmaps/ac/o;

    return-object v0
.end method

.method public final a(II)Z
    .locals 2

    iget-object v0, p0, Lmaps/ac/g;->d:[B

    aget-byte v0, v0, p1

    sget-object v1, Lmaps/ac/g;->l:[B

    aget-byte v1, v1, p2

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lmaps/ac/cj;
    .locals 1

    iget-object v0, p0, Lmaps/ac/g;->b:Lmaps/ac/cj;

    return-object v0
.end method

.method public final c()I
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lmaps/ac/g;->d:[B

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lmaps/ac/g;->m:[I

    iget-object v3, p0, Lmaps/ac/g;->d:[B

    aget-byte v3, v3, v0

    and-int/lit8 v3, v3, 0x7

    aget v2, v2, v3

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public final d()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/g;->e:Lmaps/ac/bl;

    return-object v0
.end method

.method public final e()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/ac/g;->i:I

    return v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lmaps/ac/g;->j:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lmaps/ac/g;->g:I

    return v0
.end method

.method public final i()[I
    .locals 1

    iget-object v0, p0, Lmaps/ac/g;->k:[I

    return-object v0
.end method

.method public final j()I
    .locals 3

    iget-object v0, p0, Lmaps/ac/g;->b:Lmaps/ac/cj;

    invoke-virtual {v0}, Lmaps/ac/cj;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x44

    iget-object v1, p0, Lmaps/ac/g;->d:[B

    array-length v1, v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lmaps/ac/g;->c:Lmaps/ac/cj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lmaps/ac/g;->a:Lmaps/ac/o;

    invoke-static {v2}, Lmaps/ac/ar;->a(Lmaps/ac/o;)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/ac/g;->f:Ljava/lang/String;

    invoke-static {v2}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/ac/g;->e:Lmaps/ac/bl;

    invoke-static {v2}, Lmaps/ac/ar;->a(Lmaps/ac/bl;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/g;->c:Lmaps/ac/cj;

    invoke-virtual {v0}, Lmaps/ac/cj;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    iget v0, p0, Lmaps/ac/g;->h:I

    return v0
.end method
