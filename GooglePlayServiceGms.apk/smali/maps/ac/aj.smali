.class public Lmaps/ac/aj;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/n;


# instance fields
.field private final a:Lmaps/ac/o;

.field private final b:Lmaps/ac/az;

.field private final c:[Lmaps/ac/ag;

.field private final d:Lmaps/ac/bl;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:F

.field private final h:Z

.field private final i:[I


# direct methods
.method public constructor <init>(Lmaps/ac/o;Lmaps/ac/az;[Lmaps/ac/ag;Lmaps/ac/bl;Ljava/lang/String;IF[I)V
    .locals 10

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lmaps/ac/aj;-><init>(Lmaps/ac/o;Lmaps/ac/az;[Lmaps/ac/ag;Lmaps/ac/bl;Ljava/lang/String;IF[IZ)V

    return-void
.end method

.method public constructor <init>(Lmaps/ac/o;Lmaps/ac/az;[Lmaps/ac/ag;Lmaps/ac/bl;Ljava/lang/String;IF[IZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/aj;->a:Lmaps/ac/o;

    iput-object p2, p0, Lmaps/ac/aj;->b:Lmaps/ac/az;

    iput-object p3, p0, Lmaps/ac/aj;->c:[Lmaps/ac/ag;

    iput-object p4, p0, Lmaps/ac/aj;->d:Lmaps/ac/bl;

    iput-object p5, p0, Lmaps/ac/aj;->e:Ljava/lang/String;

    iput p6, p0, Lmaps/ac/aj;->f:I

    iput p7, p0, Lmaps/ac/aj;->g:F

    iput-object p8, p0, Lmaps/ac/aj;->i:[I

    iput-boolean p9, p0, Lmaps/ac/aj;->h:Z

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/aj;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lmaps/ac/aj;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;Z)Lmaps/ac/aj;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;Z)Lmaps/ac/aj;
    .locals 23

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/bv;->b()Lmaps/ac/bt;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lmaps/ac/az;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/az;

    move-result-object v5

    invoke-static/range {p0 .. p1}, Lmaps/ac/bo;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;

    move-result-object v9

    invoke-static/range {p0 .. p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v6, v3, [Lmaps/ac/ag;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v9}, Lmaps/ac/ag;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bo;)Lmaps/ac/ag;

    move-result-object v4

    aput-object v4, v6, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v10

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float v11, v2, v3

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v12

    const/4 v4, 0x0

    const/4 v2, 0x1

    invoke-static {v2, v12}, Lmaps/ac/ar;->a(II)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {p0 .. p0}, Lmaps/ac/o;->a(Ljava/io/DataInput;)Lmaps/ac/p;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-static/range {p0 .. p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v13, v3, [I

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_3

    invoke-static/range {p0 .. p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v7

    aput v7, v13, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x2

    invoke-static {v2, v12}, Lmaps/ac/ar;->a(II)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static/range {p0 .. p0}, Lmaps/ac/o;->b(Ljava/io/DataInput;)Lmaps/ac/q;

    move-result-object v4

    goto :goto_1

    :cond_3
    if-eqz p3, :cond_4

    new-instance v2, Lmaps/ac/ak;

    invoke-virtual/range {p2 .. p2}, Lmaps/ac/bf;->a()I

    move-result v3

    invoke-virtual {v9}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v7

    invoke-virtual {v9}, Lmaps/ac/bo;->c()I

    move-result v8

    invoke-virtual {v9}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v2 .. v13}, Lmaps/ac/ak;-><init>(ILmaps/ac/o;Lmaps/ac/az;[Lmaps/ac/ag;Lmaps/ac/bl;ILjava/lang/String;IFI[I)V

    :goto_3
    return-object v2

    :cond_4
    new-instance v14, Lmaps/ac/aj;

    invoke-virtual/range {p2 .. p2}, Lmaps/ac/bf;->a()I

    invoke-virtual {v9}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v18

    invoke-virtual {v9}, Lmaps/ac/bo;->c()I

    invoke-virtual {v9}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v19

    move-object v15, v4

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move/from16 v20, v10

    move/from16 v21, v11

    move-object/from16 v22, v13

    invoke-direct/range {v14 .. v22}, Lmaps/ac/aj;-><init>(Lmaps/ac/o;Lmaps/ac/az;[Lmaps/ac/ag;Lmaps/ac/bl;Ljava/lang/String;IF[I)V

    move-object v2, v14

    goto :goto_3
.end method


# virtual methods
.method public final a(I)Lmaps/ac/ag;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aj;->c:[Lmaps/ac/ag;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()Lmaps/ac/o;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aj;->a:Lmaps/ac/o;

    return-object v0
.end method

.method public final b()Lmaps/ac/az;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aj;->b:Lmaps/ac/az;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/aj;->c:[Lmaps/ac/ag;

    array-length v0, v0

    return v0
.end method

.method public final d()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aj;->d:Lmaps/ac/bl;

    return-object v0
.end method

.method public e()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/ac/aj;->f:I

    return v0
.end method

.method public final g()F
    .locals 1

    iget v0, p0, Lmaps/ac/aj;->g:F

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ac/aj;->h:Z

    return v0
.end method

.method public final i()[I
    .locals 1

    iget-object v0, p0, Lmaps/ac/aj;->i:[I

    return-object v0
.end method

.method public final j()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ac/aj;->b:Lmaps/ac/az;

    invoke-virtual {v1}, Lmaps/ac/az;->i()I

    move-result v3

    iget-object v1, p0, Lmaps/ac/aj;->c:[Lmaps/ac/ag;

    if-eqz v1, :cond_0

    iget-object v4, p0, Lmaps/ac/aj;->c:[Lmaps/ac/ag;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v2, v4, v1

    invoke-virtual {v2}, Lmaps/ac/ag;->d()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/ac/aj;->a:Lmaps/ac/o;

    invoke-static {v1}, Lmaps/ac/ar;->a(Lmaps/ac/o;)I

    move-result v1

    add-int/lit8 v1, v1, 0x3c

    iget-object v2, p0, Lmaps/ac/aj;->e:Ljava/lang/String;

    invoke-static {v2}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/aj;->d:Lmaps/ac/bl;

    invoke-static {v2}, Lmaps/ac/ar;->a(Lmaps/ac/bl;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    return v0
.end method
