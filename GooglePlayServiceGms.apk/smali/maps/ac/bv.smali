.class public final Lmaps/ac/bv;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:Lmaps/ac/bt;

.field private final c:Lmaps/ac/bp;

.field private final d:[Ljava/lang/String;

.field private final e:Lmaps/ac/bn;


# direct methods
.method public constructor <init>(ILmaps/ac/bt;Lmaps/ac/bp;[Ljava/lang/String;Lmaps/ac/bn;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/ac/bv;->a:I

    iput-object p2, p0, Lmaps/ac/bv;->b:Lmaps/ac/bt;

    iput-object p3, p0, Lmaps/ac/bv;->c:Lmaps/ac/bp;

    iput-object p4, p0, Lmaps/ac/bv;->d:[Ljava/lang/String;

    iput-object p5, p0, Lmaps/ac/bv;->e:Lmaps/ac/bn;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lmaps/ac/bv;->a:I

    return v0
.end method

.method public final a(I)Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bv;->c:Lmaps/ac/bp;

    invoke-virtual {v0, p1}, Lmaps/ac/bp;->a(I)Lmaps/ac/bl;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lmaps/ac/bm;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bv;->e:Lmaps/ac/bn;

    invoke-virtual {v0, p1}, Lmaps/ac/bn;->a(I)Lmaps/ac/bm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bv;->b:Lmaps/ac/bt;

    return-object v0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bv;->d:[Ljava/lang/String;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ac/bv;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
