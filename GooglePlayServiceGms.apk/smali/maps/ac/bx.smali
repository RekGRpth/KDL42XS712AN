.class public abstract enum Lmaps/ac/bx;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/ac/bx;

.field public static final enum b:Lmaps/ac/bx;

.field public static final enum c:Lmaps/ac/bx;

.field public static final enum d:Lmaps/ac/bx;

.field public static final enum e:Lmaps/ac/bx;

.field public static final enum f:Lmaps/ac/bx;

.field public static final enum g:Lmaps/ac/bx;

.field private static final synthetic h:[Lmaps/ac/bx;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/ac/by;

    const-string v1, "SPOTLIGHT"

    invoke-direct {v0, v1}, Lmaps/ac/by;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/ac/bx;->a:Lmaps/ac/bx;

    new-instance v0, Lmaps/ac/bz;

    const-string v1, "SPOTLIGHT_DIFFTILE"

    invoke-direct {v0, v1}, Lmaps/ac/bz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/ac/bx;->b:Lmaps/ac/bx;

    new-instance v0, Lmaps/ac/ca;

    const-string v1, "HIGHLIGHT"

    invoke-direct {v0, v1}, Lmaps/ac/ca;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/ac/bx;->c:Lmaps/ac/bx;

    new-instance v0, Lmaps/ac/cb;

    const-string v1, "INDOOR"

    invoke-direct {v0, v1}, Lmaps/ac/cb;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/ac/bx;->d:Lmaps/ac/bx;

    new-instance v0, Lmaps/ac/cc;

    const-string v1, "TRANSIT"

    invoke-direct {v0, v1}, Lmaps/ac/cc;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/ac/bx;->e:Lmaps/ac/bx;

    new-instance v0, Lmaps/ac/cd;

    const-string v1, "MAPS_ENGINE"

    invoke-direct {v0, v1}, Lmaps/ac/cd;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/ac/bx;->f:Lmaps/ac/bx;

    new-instance v0, Lmaps/ac/ce;

    const-string v1, "ALTERNATE_PAINTFE"

    invoke-direct {v0, v1}, Lmaps/ac/ce;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/ac/bx;->g:Lmaps/ac/bx;

    const/4 v0, 0x7

    new-array v0, v0, [Lmaps/ac/bx;

    const/4 v1, 0x0

    sget-object v2, Lmaps/ac/bx;->a:Lmaps/ac/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lmaps/ac/bx;->b:Lmaps/ac/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lmaps/ac/bx;->c:Lmaps/ac/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lmaps/ac/bx;->d:Lmaps/ac/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lmaps/ac/bx;->e:Lmaps/ac/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lmaps/ac/bx;->f:Lmaps/ac/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/ac/bx;->g:Lmaps/ac/bx;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/ac/bx;->h:[Lmaps/ac/bx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/ac/bx;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/ac/bx;
    .locals 1

    const-class v0, Lmaps/ac/bx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/ac/bx;

    return-object v0
.end method

.method public static values()[Lmaps/ac/bx;
    .locals 1

    sget-object v0, Lmaps/ac/bx;->h:[Lmaps/ac/bx;

    invoke-virtual {v0}, [Lmaps/ac/bx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/ac/bx;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lmaps/bv/a;)Lmaps/ac/bw;
.end method

.method public a()Lmaps/ao/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
