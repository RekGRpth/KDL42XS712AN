.class public final Lmaps/ac/cg;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/n;


# instance fields
.field private final a:Lmaps/ac/az;

.field private final b:Lmaps/ac/bl;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:[I


# direct methods
.method private constructor <init>(Lmaps/ac/az;Lmaps/ac/bl;Ljava/lang/String;II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/cg;->a:Lmaps/ac/az;

    iput-object p2, p0, Lmaps/ac/cg;->b:Lmaps/ac/bl;

    iput-object p3, p0, Lmaps/ac/cg;->c:Ljava/lang/String;

    iput p4, p0, Lmaps/ac/cg;->e:I

    iput p5, p0, Lmaps/ac/cg;->d:I

    iput-object p6, p0, Lmaps/ac/cg;->f:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/n;
    .locals 8

    invoke-virtual {p1}, Lmaps/ac/bv;->b()Lmaps/ac/bt;

    move-result-object v0

    invoke-static {p0, v0}, Lmaps/ac/az;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/az;

    move-result-object v1

    invoke-static {p0, p1}, Lmaps/ac/bo;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;

    move-result-object v3

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v4

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v5

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v6, v2, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v7

    aput v7, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/ac/cg;

    invoke-virtual {p2}, Lmaps/ac/bf;->a()I

    invoke-virtual {v3}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v2

    invoke-virtual {v3}, Lmaps/ac/bo;->c()I

    invoke-virtual {v3}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v6}, Lmaps/ac/cg;-><init>(Lmaps/ac/az;Lmaps/ac/bl;Ljava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public final a()Lmaps/ac/o;
    .locals 1

    sget-object v0, Lmaps/ac/o;->a:Lmaps/ac/o;

    return-object v0
.end method

.method public final b()Lmaps/ac/az;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cg;->a:Lmaps/ac/az;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    iget v0, p0, Lmaps/ac/cg;->d:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final d()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cg;->b:Lmaps/ac/bl;

    return-object v0
.end method

.method public final e()I
    .locals 1

    const/16 v0, 0x9

    return v0
.end method

.method public final f()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lmaps/ac/cg;->e:I

    return v0
.end method

.method public final i()[I
    .locals 1

    iget-object v0, p0, Lmaps/ac/cg;->f:[I

    return-object v0
.end method

.method public final j()I
    .locals 2

    iget-object v0, p0, Lmaps/ac/cg;->a:Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->i()I

    move-result v0

    add-int/lit8 v0, v0, 0x2c

    iget-object v1, p0, Lmaps/ac/cg;->c:Ljava/lang/String;

    invoke-static {v1}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ac/cg;->b:Lmaps/ac/bl;

    invoke-static {v1}, Lmaps/ac/ar;->a(Lmaps/ac/bl;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
