.class public final Lmaps/ac/ao;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/bw;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/SortedMap;

.field private final c:Ljava/lang/String;

.field private final d:Lmaps/bv/a;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmaps/ac/ao;->a:Ljava/lang/String;

    new-instance v1, Ljava/util/TreeMap;

    invoke-static {p2}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, Lmaps/ac/ao;->b:Ljava/util/SortedMap;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lmaps/ac/ao;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "|"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-static {v0}, Lmaps/k/f;->a(Ljava/lang/String;)Lmaps/k/f;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Lmaps/k/f;->c(Ljava/lang/String;)Lmaps/k/i;

    move-result-object v0

    iget-object v2, p0, Lmaps/ac/ao;->b:Ljava/util/SortedMap;

    invoke-virtual {v0, v1, v2}, Lmaps/k/i;->a(Ljava/lang/StringBuilder;Ljava/util/Map;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/ao;->c:Ljava/lang/String;

    invoke-direct {p0}, Lmaps/ac/ao;->b()Lmaps/bv/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/ao;->d:Lmaps/bv/a;

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/ac/ao;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private b()Lmaps/bv/a;
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    new-instance v2, Lmaps/bv/a;

    sget-object v0, Lmaps/cm/y;->g:Lmaps/bv/c;

    invoke-direct {v2, v0}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    iget-object v0, p0, Lmaps/ac/ao;->a:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    iget-object v0, p0, Lmaps/ac/ao;->b:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v4, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/y;->h:Lmaps/bv/c;

    invoke-direct {v4, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v6, v0}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    invoke-virtual {v2, v6, v4}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public final a()Lmaps/ac/bx;
    .locals 1

    sget-object v0, Lmaps/ac/bx;->f:Lmaps/ac/bx;

    return-object v0
.end method

.method public final a(Lmaps/bv/a;)V
    .locals 2

    const/16 v0, 0x1d

    iget-object v1, p0, Lmaps/ac/ao;->d:Lmaps/bv/a;

    invoke-virtual {p1, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    return-void
.end method

.method public final a(Lmaps/ac/bw;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/ac/ao;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Lmaps/ao/b;)Z
    .locals 1

    sget-object v0, Lmaps/ao/b;->x:Lmaps/ao/b;

    if-eq p1, v0, :cond_0

    sget-object v0, Lmaps/ao/b;->w:Lmaps/ao/b;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lmaps/ac/bw;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lmaps/ac/ao;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/ac/ao;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lmaps/ac/ao;

    iget-object v2, p0, Lmaps/ac/ao;->a:Ljava/lang/String;

    iget-object v3, p1, Lmaps/ac/ao;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmaps/ac/ao;->b:Ljava/util/SortedMap;

    iget-object v3, p1, Lmaps/ac/ao;->b:Ljava/util/SortedMap;

    invoke-static {v2, v3}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ac/ao;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/ac/ao;->b:Ljava/util/SortedMap;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/ao;->c:Ljava/lang/String;

    return-object v0
.end method
