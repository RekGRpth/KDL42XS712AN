.class public final Lmaps/ar/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ar/c;


# instance fields
.field private final a:Lmaps/ac/av;

.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:F


# direct methods
.method public constructor <init>(Lmaps/ac/av;FFFF)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/ac/av;

    invoke-virtual {p1}, Lmaps/ac/av;->f()I

    move-result v1

    invoke-virtual {p1}, Lmaps/ac/av;->g()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lmaps/ac/av;-><init>(II)V

    iput-object v0, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    const/high16 v0, 0x41a80000    # 21.0f

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/ar/b;->b:F

    iput p3, p0, Lmaps/ar/b;->c:F

    iput p4, p0, Lmaps/ar/b;->d:F

    iput p5, p0, Lmaps/ar/b;->e:F

    return-void
.end method

.method public constructor <init>(Lmaps/ac/u;FFFF)V
    .locals 6

    invoke-virtual {p1}, Lmaps/ac/u;->a()I

    move-result v0

    invoke-virtual {p1}, Lmaps/ac/u;->b()I

    move-result v1

    invoke-static {v0, v1}, Lmaps/ac/av;->b(II)Lmaps/ac/av;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    return-void
.end method

.method public static a(F)F
    .locals 4

    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    float-to-double v0, p0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    neg-double v0, v0

    const-wide v2, 0x3ff7154760000000L    # 1.4426950216293335

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x42000000    # 32.0f

    goto :goto_0
.end method

.method public static a(Lmaps/ar/b;Lmaps/ar/b;FF)Lmaps/ar/b;
    .locals 11

    const/high16 v10, 0x43340000    # 180.0f

    const v3, 0x40490fdb    # (float)Math.PI

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v9, 0x43b40000    # 360.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    iget-object v1, p1, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v0, v1, p2}, Lmaps/ac/av;->a(Lmaps/ac/av;F)Lmaps/ac/av;

    move-result-object v1

    iget v0, p0, Lmaps/ar/b;->b:F

    iget v2, p1, Lmaps/ar/b;->b:F

    sub-float/2addr v2, v0

    mul-float/2addr v2, p2

    add-float/2addr v2, v0

    :goto_0
    iget v0, p0, Lmaps/ar/b;->c:F

    iget v3, p1, Lmaps/ar/b;->c:F

    sub-float/2addr v3, v0

    mul-float/2addr v3, p2

    add-float/2addr v3, v0

    iget v4, p0, Lmaps/ar/b;->d:F

    iget v0, p1, Lmaps/ar/b;->d:F

    cmpl-float v5, v4, v0

    if-lez v5, :cond_3

    sub-float v5, v4, v0

    cmpl-float v5, v5, v10

    if-lez v5, :cond_0

    sub-float/2addr v4, v9

    :cond_0
    :goto_1
    sub-float/2addr v0, v4

    mul-float/2addr v0, p2

    add-float/2addr v4, v0

    float-to-double v5, v4

    const-wide/16 v7, 0x0

    cmpg-double v0, v5, v7

    if-gez v0, :cond_1

    add-float/2addr v4, v9

    :cond_1
    iget v0, p0, Lmaps/ar/b;->e:F

    iget v5, p1, Lmaps/ar/b;->e:F

    sub-float/2addr v5, v0

    mul-float/2addr v5, p2

    add-float/2addr v5, v0

    new-instance v0, Lmaps/ar/b;

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    return-object v0

    :cond_2
    iget-object v0, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    iget-object v1, p1, Lmaps/ar/b;->a:Lmaps/ac/av;

    sub-float v2, p2, v4

    mul-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    add-float/2addr v2, v4

    div-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Lmaps/ac/av;->a(Lmaps/ac/av;F)Lmaps/ac/av;

    move-result-object v1

    iget v0, p0, Lmaps/ar/b;->b:F

    iget v2, p1, Lmaps/ar/b;->b:F

    mul-float/2addr v3, p2

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    invoke-static {v0}, Lmaps/ar/b;->b(F)F

    move-result v0

    invoke-static {v2}, Lmaps/ar/b;->b(F)F

    move-result v2

    sub-float/2addr v4, p2

    mul-float/2addr v0, v4

    mul-float/2addr v2, p2

    add-float/2addr v0, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    float-to-double v2, v3

    const-wide v6, 0x3ff3333333333333L    # 1.2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v2, v4

    float-to-double v4, p3

    const-wide v6, 0x3fd999999999999aL    # 0.4

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    add-float/2addr v0, v2

    const/high16 v2, 0x43200000    # 160.0f

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v0}, Lmaps/ar/b;->a(F)F

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto :goto_0

    :cond_3
    sub-float v5, v0, v4

    cmpl-float v5, v5, v10

    if-lez v5, :cond_0

    sub-float/2addr v0, v9

    goto :goto_1
.end method

.method public static b(F)F
    .locals 6

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    float-to-double v4, p0

    sub-double/2addr v2, v4

    const-wide v4, 0x3ff7154760000000L    # 1.4426950216293335

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method public final a()Lmaps/ar/b;
    .locals 0

    return-object p0
.end method

.method public final a(Lmaps/ar/b;)Lmaps/ar/b;
    .locals 6

    const/high16 v3, 0x40000000    # 2.0f

    iget-object v0, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v0

    iget-object v1, p1, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v1}, Lmaps/ac/av;->f()I

    move-result v1

    sub-int/2addr v0, v1

    const/high16 v1, 0x20000000

    if-le v0, v1, :cond_0

    new-instance v0, Lmaps/ar/b;

    new-instance v1, Lmaps/ac/av;

    iget-object v2, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v2

    sub-int/2addr v2, v3

    iget-object v3, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lmaps/ac/av;-><init>(II)V

    iget v2, p0, Lmaps/ar/b;->b:F

    iget v3, p0, Lmaps/ar/b;->c:F

    iget v4, p0, Lmaps/ar/b;->d:F

    iget v5, p0, Lmaps/ar/b;->e:F

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    :goto_0
    return-object v0

    :cond_0
    const/high16 v1, -0x20000000

    if-ge v0, v1, :cond_1

    new-instance v0, Lmaps/ar/b;

    new-instance v1, Lmaps/ac/av;

    iget-object v2, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v3, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lmaps/ac/av;-><init>(II)V

    iget v2, p0, Lmaps/ar/b;->b:F

    iget v3, p0, Lmaps/ar/b;->c:F

    iget v4, p0, Lmaps/ar/b;->d:F

    iget v5, p0, Lmaps/ar/b;->e:F

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/ar/b;->b:F

    return v0
.end method

.method public final c()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-static {v0}, Lmaps/ac/av;->a(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    return-object v0
.end method

.method public final d()F
    .locals 1

    iget v0, p0, Lmaps/ar/b;->c:F

    return v0
.end method

.method public final e()F
    .locals 1

    iget v0, p0, Lmaps/ar/b;->d:F

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/ar/b;

    if-eqz v2, :cond_3

    check-cast p1, Lmaps/ar/b;

    iget-object v2, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    iget-object v3, p1, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v2, v3}, Lmaps/ac/av;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lmaps/ar/b;->b:F

    iget v3, p1, Lmaps/ar/b;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lmaps/ar/b;->c:F

    iget v3, p1, Lmaps/ar/b;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lmaps/ar/b;->d:F

    iget v3, p1, Lmaps/ar/b;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lmaps/ar/b;->e:F

    iget v3, p1, Lmaps/ar/b;->e:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()F
    .locals 1

    iget v0, p0, Lmaps/ar/b;->e:F

    return v0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lmaps/ar/b;->b:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x25

    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, Lmaps/ar/b;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, Lmaps/ar/b;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, Lmaps/ar/b;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x25

    iget-object v0, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v0}, Lmaps/ac/av;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[target:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/ar/b;->a:Lmaps/ac/av;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " zoom:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ar/b;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " viewingAngle:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ar/b;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bearing:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ar/b;->d:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lookAhead:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ar/b;->e:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
