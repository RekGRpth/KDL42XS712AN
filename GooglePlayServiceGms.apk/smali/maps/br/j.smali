.class final Lmaps/br/j;
.super Lmaps/bn/m;


# instance fields
.field private synthetic a:Lmaps/br/g;


# direct methods
.method constructor <init>(Lmaps/br/g;[BZZ)V
    .locals 6

    const/4 v3, 0x0

    iput-object p1, p0, Lmaps/br/j;->a:Lmaps/br/g;

    const/16 v1, 0x7d

    move-object v0, p0

    move-object v2, p2

    move v4, v3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lmaps/bn/m;-><init>(I[BZZZ)V

    return-void
.end method


# virtual methods
.method public final d()V
    .locals 4

    const-class v1, Lmaps/br/g;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lmaps/br/g;->c()Z

    invoke-static {}, Lmaps/br/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/br/g;->e()Lmaps/br/k;

    move-result-object v0

    invoke-interface {v0}, Lmaps/br/k;->a()Lmaps/bv/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lmaps/br/g;->e()Lmaps/br/k;

    move-result-object v0

    invoke-interface {v0}, Lmaps/br/k;->a()Lmaps/bv/a;

    move-result-object v0

    iget-object v2, p0, Lmaps/br/j;->a:Lmaps/br/g;

    invoke-static {v2}, Lmaps/br/g;->b(Lmaps/br/g;)I

    move-result v2

    invoke-static {v0, v2}, Lmaps/br/g;->a(Lmaps/bv/a;I)Lmaps/bv/a;

    move-result-object v0

    invoke-static {}, Lmaps/br/g;->e()Lmaps/br/k;

    move-result-object v2

    invoke-interface {v2, v0}, Lmaps/br/k;->a(Lmaps/bv/a;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/br/j;->a:Lmaps/br/g;

    invoke-virtual {v0}, Lmaps/br/g;->b()V

    monitor-exit v1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lmaps/br/g;

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".uploadEventLog"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    const-string v2, "USER_EVENTSUserEventReporter"

    invoke-static {v2, v0}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lmaps/br/j;->a:Lmaps/br/g;

    invoke-virtual {v0}, Lmaps/br/g;->b()V

    return-void
.end method
