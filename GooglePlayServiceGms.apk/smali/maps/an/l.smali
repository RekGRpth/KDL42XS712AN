.class public final enum Lmaps/an/l;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/an/l;

.field public static final enum b:Lmaps/an/l;

.field public static final enum c:Lmaps/an/l;

.field public static final enum d:Lmaps/an/l;

.field private static final synthetic g:[Lmaps/an/l;


# instance fields
.field public final e:Z

.field public final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/an/l;

    const-string v1, "NOT_LIVE"

    invoke-direct {v0, v1, v2, v2, v2}, Lmaps/an/l;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lmaps/an/l;->a:Lmaps/an/l;

    new-instance v0, Lmaps/an/l;

    const-string v1, "NOT_LIVE_WITH_NEW_CONTEXT"

    invoke-direct {v0, v1, v3, v2, v3}, Lmaps/an/l;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lmaps/an/l;->b:Lmaps/an/l;

    new-instance v0, Lmaps/an/l;

    const-string v1, "LIVE"

    invoke-direct {v0, v1, v4, v3, v2}, Lmaps/an/l;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lmaps/an/l;->c:Lmaps/an/l;

    new-instance v0, Lmaps/an/l;

    const-string v1, "LIVE_WITH_NEW_CONTEXT"

    invoke-direct {v0, v1, v5, v3, v3}, Lmaps/an/l;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lmaps/an/l;->d:Lmaps/an/l;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/an/l;

    sget-object v1, Lmaps/an/l;->a:Lmaps/an/l;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/an/l;->b:Lmaps/an/l;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/an/l;->c:Lmaps/an/l;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/an/l;->d:Lmaps/an/l;

    aput-object v1, v0, v5

    sput-object v0, Lmaps/an/l;->g:[Lmaps/an/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lmaps/an/l;->e:Z

    iput-boolean p4, p0, Lmaps/an/l;->f:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/an/l;
    .locals 1

    const-class v0, Lmaps/an/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/an/l;

    return-object v0
.end method

.method public static values()[Lmaps/an/l;
    .locals 1

    sget-object v0, Lmaps/an/l;->g:[Lmaps/an/l;

    invoke-virtual {v0}, [Lmaps/an/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/an/l;

    return-object v0
.end method
