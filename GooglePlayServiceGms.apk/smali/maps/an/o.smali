.class public final Lmaps/an/o;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/an/n;

.field private b:Lmaps/an/n;


# direct methods
.method private declared-synchronized a()Lmaps/an/n;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/an/o;->a:Lmaps/an/n;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lmaps/an/n;->a(Lmaps/an/n;)Lmaps/an/n;

    move-result-object v1

    iput-object v1, p0, Lmaps/an/o;->a:Lmaps/an/n;

    iget-object v1, p0, Lmaps/an/o;->a:Lmaps/an/n;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/an/o;->b:Lmaps/an/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lmaps/an/o;)Lmaps/an/n;
    .locals 1

    invoke-direct {p0}, Lmaps/an/o;->a()Lmaps/an/n;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lmaps/an/n;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/an/o;->b:Lmaps/an/n;

    iput-object p1, p0, Lmaps/an/o;->b:Lmaps/an/n;

    if-nez v0, :cond_0

    iput-object p1, p0, Lmaps/an/o;->a:Lmaps/an/n;

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lmaps/an/n;->a(Lmaps/an/n;Lmaps/an/n;)Lmaps/an/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {v0, p1}, Lmaps/an/n;->a(Lmaps/an/n;Lmaps/an/n;)Lmaps/an/n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lmaps/an/o;Lmaps/an/n;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/an/o;->a(Lmaps/an/n;)V

    return-void
.end method
