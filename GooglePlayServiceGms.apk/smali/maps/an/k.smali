.class public Lmaps/an/k;
.super Ljava/lang/Object;


# instance fields
.field volatile a:Lmaps/an/an;

.field protected b:Z

.field protected c:[Lmaps/an/an;

.field protected d:[I

.field protected e:Z

.field private final f:[[Lmaps/an/q;

.field private final g:[Lmaps/an/ap;

.field private h:[[F

.field private i:Ljava/lang/String;

.field private j:B

.field private k:B

.field private l:Lmaps/an/m;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/k;->b:Z

    iput-boolean v1, p0, Lmaps/an/k;->e:Z

    iput-byte v1, p0, Lmaps/an/k;->j:B

    const/4 v0, -0x1

    iput-byte v0, p0, Lmaps/an/k;->k:B

    new-array v0, v2, [Lmaps/an/ap;

    iput-object v0, p0, Lmaps/an/k;->g:[Lmaps/an/ap;

    sget v0, Lmaps/an/q;->b:I

    filled-new-array {v2, v0}, [I

    move-result-object v0

    const-class v1, Lmaps/an/q;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lmaps/an/q;

    iput-object v0, p0, Lmaps/an/k;->f:[[Lmaps/an/q;

    new-instance v0, Lmaps/an/an;

    invoke-direct {v0}, Lmaps/an/an;-><init>()V

    iput-object v0, p0, Lmaps/an/k;->a:Lmaps/an/an;

    new-array v0, v2, [Lmaps/an/an;

    iput-object v0, p0, Lmaps/an/k;->c:[Lmaps/an/an;

    new-array v0, v2, [[F

    iput-object v0, p0, Lmaps/an/k;->h:[[F

    new-array v0, v2, [I

    iput-object v0, p0, Lmaps/an/k;->d:[I

    return-void
.end method


# virtual methods
.method final a()Lmaps/an/aa;
    .locals 2

    iget-object v0, p0, Lmaps/an/k;->f:[[Lmaps/an/q;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sget-object v1, Lmaps/an/r;->a:Lmaps/an/r;

    invoke-virtual {v1}, Lmaps/an/r;->a()I

    move-result v1

    aget-object v0, v0, v1

    check-cast v0, Lmaps/an/aa;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    iget-boolean v0, p0, Lmaps/an/k;->e:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    int-to-byte v0, p1

    iput-byte v0, p0, Lmaps/an/k;->k:B

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lmaps/an/k;->i:Ljava/lang/String;

    return-void
.end method

.method public a(Lmaps/an/an;)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lmaps/an/k;->e:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/an/k;->a:Lmaps/an/an;

    iget-object v1, p1, Lmaps/an/an;->a:[F

    iget-object v0, v0, Lmaps/an/an;->a:[F

    const/16 v2, 0x10

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/k;->b:Z

    return-void
.end method

.method public a(Lmaps/an/ap;I)V
    .locals 4

    iget-boolean v0, p0, Lmaps/an/k;->e:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iget-byte v0, p0, Lmaps/an/k;->j:B

    or-int/2addr v0, p2

    int-to-byte v0, v0

    iput-byte v0, p0, Lmaps/an/k;->j:B

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_5

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/2addr v1, p2

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/an/k;->g:[Lmaps/an/ap;

    aget-object v1, v1, v0

    iget-object v2, p0, Lmaps/an/k;->g:[Lmaps/an/ap;

    aput-object p1, v2, v0

    iget-object v2, p0, Lmaps/an/k;->c:[Lmaps/an/an;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    iget-object v2, p0, Lmaps/an/k;->c:[Lmaps/an/an;

    new-instance v3, Lmaps/an/an;

    invoke-direct {v3}, Lmaps/an/an;-><init>()V

    aput-object v3, v2, v0

    :cond_1
    iget-object v2, p0, Lmaps/an/k;->h:[[F

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    iget-object v2, p0, Lmaps/an/k;->h:[[F

    const/16 v3, 0x10

    new-array v3, v3, [F

    aput-object v3, v2, v0

    :cond_2
    iget-boolean v2, p0, Lmaps/an/k;->e:Z

    if-eqz v2, :cond_4

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lmaps/an/ap;->a()V

    iget-object v2, p0, Lmaps/an/k;->l:Lmaps/an/m;

    sget-object v3, Lmaps/an/l;->a:Lmaps/an/l;

    invoke-virtual {v1, v2, v3}, Lmaps/an/ap;->a(Lmaps/an/m;Lmaps/an/l;)Z

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lmaps/an/ap;->b()V

    iget-object v1, p0, Lmaps/an/k;->l:Lmaps/an/m;

    sget-object v2, Lmaps/an/l;->c:Lmaps/an/l;

    invoke-virtual {p1, v1, v2}, Lmaps/an/ap;->a(Lmaps/an/m;Lmaps/an/l;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method public a(Lmaps/an/q;I)V
    .locals 5

    const/4 v0, 0x1

    iget-boolean v1, p0, Lmaps/an/k;->e:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iget-object v1, p1, Lmaps/an/q;->e:Lmaps/an/r;

    sget-object v2, Lmaps/an/r;->a:Lmaps/an/r;

    if-ne v1, v2, :cond_1

    move p2, v0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_4

    shl-int v2, v0, v1

    and-int/2addr v2, p2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmaps/an/k;->f:[[Lmaps/an/q;

    aget-object v2, v2, v1

    iget-object v3, p1, Lmaps/an/q;->e:Lmaps/an/r;

    invoke-virtual {v3}, Lmaps/an/r;->a()I

    move-result v3

    aget-object v2, v2, v3

    iget-object v3, p0, Lmaps/an/k;->f:[[Lmaps/an/q;

    aget-object v3, v3, v1

    iget-object v4, p1, Lmaps/an/q;->e:Lmaps/an/r;

    invoke-virtual {v4}, Lmaps/an/r;->a()I

    move-result v4

    aput-object p1, v3, v4

    iget-boolean v3, p0, Lmaps/an/k;->e:Z

    if-eqz v3, :cond_3

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lmaps/an/q;->c()V

    iget-object v3, p0, Lmaps/an/k;->l:Lmaps/an/m;

    sget-object v4, Lmaps/an/l;->a:Lmaps/an/l;

    invoke-virtual {v2, v3, v4}, Lmaps/an/q;->a(Lmaps/an/m;Lmaps/an/l;)Z

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lmaps/an/q;->b()V

    iget-object v2, p0, Lmaps/an/k;->l:Lmaps/an/m;

    sget-object v3, Lmaps/an/l;->c:Lmaps/an/l;

    invoke-virtual {p1, v2, v3}, Lmaps/an/q;->a(Lmaps/an/m;Lmaps/an/l;)Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method final a(Lmaps/an/m;Lmaps/an/l;)Z
    .locals 9

    const/4 v0, 0x0

    iget-boolean v1, p2, Lmaps/an/l;->e:Z

    iget-boolean v2, p0, Lmaps/an/k;->e:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p2, Lmaps/an/l;->f:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lmaps/an/k;->l:Lmaps/an/m;

    iget-object v2, p0, Lmaps/an/k;->g:[Lmaps/an/ap;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    if-eqz v4, :cond_2

    iget-boolean v5, p2, Lmaps/an/l;->f:Z

    if-nez v5, :cond_1

    iget-boolean v5, p2, Lmaps/an/l;->e:Z

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lmaps/an/ap;->b()V

    :cond_1
    :goto_2
    invoke-virtual {v4, p1, p2}, Lmaps/an/ap;->a(Lmaps/an/m;Lmaps/an/l;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v4}, Lmaps/an/ap;->a()V

    goto :goto_2

    :cond_4
    invoke-static {}, Lmaps/an/m;->c()V

    iget-object v3, p0, Lmaps/an/k;->f:[[Lmaps/an/q;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_8

    aget-object v7, v5, v1

    if-eqz v7, :cond_6

    iget-boolean v8, p2, Lmaps/an/l;->f:Z

    if-nez v8, :cond_5

    iget-boolean v8, p2, Lmaps/an/l;->e:Z

    if-eqz v8, :cond_7

    invoke-virtual {v7}, Lmaps/an/q;->b()V

    :cond_5
    :goto_5
    invoke-virtual {v7, p1, p2}, Lmaps/an/q;->a(Lmaps/an/m;Lmaps/an/l;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {v7}, Lmaps/an/q;->c()V

    goto :goto_5

    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_9
    invoke-static {}, Lmaps/an/m;->c()V

    iget-boolean v0, p2, Lmaps/an/l;->e:Z

    iput-boolean v0, p0, Lmaps/an/k;->e:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()B
    .locals 1

    iget-byte v0, p0, Lmaps/an/k;->j:B

    return v0
.end method
