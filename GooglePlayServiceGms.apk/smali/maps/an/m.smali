.class public final Lmaps/an/m;
.super Ljava/lang/Object;


# static fields
.field private static final h:Ljava/util/List;

.field private static final i:Ljava/util/List;

.field private static l:Ljava/lang/String;


# instance fields
.field private a:[Lmaps/an/q;

.field private b:Lmaps/an/o;

.field private c:Lmaps/an/d;

.field private d:[Lmaps/an/ac;

.field private e:Ljava/util/Set;

.field private f:Ljava/util/Set;

.field private final g:Lmaps/an/p;

.field private j:I

.field private k:I

.field private m:Lmaps/an/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lmaps/an/m;->h:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lmaps/an/m;->i:Ljava/util/List;

    const/4 v0, 0x0

    sput-object v0, Lmaps/an/m;->l:Ljava/lang/String;

    return-void
.end method

.method public static c()V
    .locals 1

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->dumpStack()V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    new-instance v0, Lmaps/an/s;

    invoke-direct {v0}, Lmaps/an/s;-><init>()V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method final a()V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/an/m;->m:Lmaps/an/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/m;->m:Lmaps/an/ab;

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/an/o;

    invoke-static {v0}, Lmaps/an/o;->a(Lmaps/an/o;)Lmaps/an/n;

    move-result-object v2

    if-eqz v2, :cond_6

    :try_start_0
    iget v0, v2, Lmaps/an/n;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    move-result-object v0

    sget-object v3, Lmaps/an/l;->c:Lmaps/an/l;

    invoke-virtual {v0, p0, v3}, Lmaps/an/k;->a(Lmaps/an/m;Lmaps/an/l;)Z

    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/k;->b()B

    move-result v3

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_2

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_1

    iget-object v4, p0, Lmaps/an/m;->d:[Lmaps/an/ac;

    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/an/m;->m:Lmaps/an/ab;

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/k;->a()Lmaps/an/aa;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/m;->m:Lmaps/an/ab;

    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    move-result-object v0

    sget-object v3, Lmaps/an/l;->c:Lmaps/an/l;

    invoke-virtual {v0, p0, v3}, Lmaps/an/k;->a(Lmaps/an/m;Lmaps/an/l;)Z

    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/k;->b()B

    move-result v3

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_4

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_3

    iget-object v4, p0, Lmaps/an/m;->d:[Lmaps/an/ac;

    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lmaps/an/m;->m:Lmaps/an/ab;

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/k;->a()Lmaps/an/aa;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/m;->m:Lmaps/an/ab;

    invoke-virtual {v2}, Lmaps/an/n;->a()Lmaps/an/k;

    goto :goto_0

    :pswitch_2
    invoke-virtual {v2}, Lmaps/an/n;->b()Lmaps/an/g;

    move-result-object v0

    sget-object v3, Lmaps/an/l;->c:Lmaps/an/l;

    invoke-virtual {v0, v3}, Lmaps/an/g;->a(Lmaps/an/l;)Z

    iget-object v0, p0, Lmaps/an/m;->e:Ljava/util/Set;

    invoke-virtual {v2}, Lmaps/an/n;->b()Lmaps/an/g;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/an/g;->b()Lmaps/an/ad;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/an/m;->f:Ljava/util/Set;

    invoke-virtual {v2}, Lmaps/an/n;->b()Lmaps/an/g;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lmaps/an/n;->b()Lmaps/an/g;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/g;->d()B

    move-result v3

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_0

    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_5

    iget-object v4, p0, Lmaps/an/m;->d:[Lmaps/an/ac;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lmaps/an/n;->b()Lmaps/an/g;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/an/ac;->a(Lmaps/an/g;)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Remove camera not implemented"

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_4
    iget-object v0, p0, Lmaps/an/m;->c:Lmaps/an/d;

    invoke-virtual {v2}, Lmaps/an/n;->c()Lmaps/an/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/an/d;->a(Lmaps/an/b;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lmaps/an/m;->c:Lmaps/an/d;

    invoke-virtual {v2}, Lmaps/an/n;->c()Lmaps/an/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/an/d;->b(Lmaps/an/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lmaps/an/m;->c:Lmaps/an/d;

    invoke-virtual {v0}, Lmaps/an/d;->a()V

    iget-object v0, p0, Lmaps/an/m;->m:Lmaps/an/ab;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/an/m;->m:Lmaps/an/ab;

    :cond_7
    iget-object v0, p0, Lmaps/an/m;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/ad;

    invoke-virtual {v0}, Lmaps/an/ad;->c()V

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lmaps/an/m;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/g;

    invoke-virtual {v0}, Lmaps/an/g;->c()V

    goto :goto_5

    :cond_9
    iget-object v0, p0, Lmaps/an/m;->d:[Lmaps/an/ac;

    iget-object v0, p0, Lmaps/an/m;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lmaps/an/m;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_7

    :cond_b
    iget v0, p0, Lmaps/an/m;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/an/m;->k:I

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lmaps/an/k;)V
    .locals 3

    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/an/o;

    new-instance v1, Lmaps/an/n;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lmaps/an/n;-><init>(Lmaps/an/k;Z)V

    invoke-static {v0, v1}, Lmaps/an/o;->a(Lmaps/an/o;Lmaps/an/n;)V

    return-void
.end method

.method final b()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/an/m;->g:Lmaps/an/p;

    invoke-virtual {v0}, Lmaps/an/p;->a()V

    iget-object v0, p0, Lmaps/an/m;->d:[Lmaps/an/ac;

    move v0, v1

    :goto_0
    sget v2, Lmaps/an/q;->b:I

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lmaps/an/m;->a:[Lmaps/an/q;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v2, 0xd57

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    aget v0, v0, v1

    iput v0, p0, Lmaps/an/m;->j:I

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p0, Lmaps/an/m;->j:I

    add-int/lit8 v2, v2, -0x1

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    sget-object v0, Lmaps/an/m;->l:Ljava/lang/String;

    if-nez v0, :cond_1

    const/16 v0, 0x1f03

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/an/m;->l:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public final b(Lmaps/an/k;)V
    .locals 3

    iget-object v0, p0, Lmaps/an/m;->b:Lmaps/an/o;

    new-instance v1, Lmaps/an/n;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lmaps/an/n;-><init>(Lmaps/an/k;Z)V

    invoke-static {v0, v1}, Lmaps/an/o;->a(Lmaps/an/o;Lmaps/an/n;)V

    return-void
.end method

.method public final d()Lmaps/an/ae;
    .locals 1

    iget-object v0, p0, Lmaps/an/m;->g:Lmaps/an/p;

    return-object v0
.end method

.method final e()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    sget v1, Lmaps/an/q;->b:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lmaps/an/m;->a:[Lmaps/an/q;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/an/m;->a:[Lmaps/an/q;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lmaps/an/q;->a()V

    iget-object v1, p0, Lmaps/an/m;->a:[Lmaps/an/q;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
