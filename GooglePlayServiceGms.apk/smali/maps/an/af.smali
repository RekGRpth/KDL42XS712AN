.class public Lmaps/an/af;
.super Ljava/lang/Object;


# instance fields
.field a:I

.field protected b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/an/af;->c:Ljava/lang/String;

    iput-object p2, p0, Lmaps/an/af;->d:Ljava/lang/String;

    return-void
.end method

.method protected static a(ILjava/lang/String;)I
    .locals 3

    invoke-static {p0, p1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to get "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " handle"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lmaps/an/m;->c()V

    return v0
.end method

.method private static b(ILjava/lang/String;)I
    .locals 4

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x8b81

    invoke-static {v1, v3, v2, v0}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    aget v2, v2, v0

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected a(I)V
    .locals 1

    const-string v0, "uMVPMatrix"

    invoke-static {p1, v0}, Lmaps/an/af;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lmaps/an/af;->b:I

    return-void
.end method

.method public final a(Lmaps/an/l;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p1, Lmaps/an/l;->e:Z

    iget-boolean v3, p0, Lmaps/an/af;->e:Z

    if-ne v0, v3, :cond_1

    iget-boolean v0, p1, Lmaps/an/l;->f:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-boolean v0, p1, Lmaps/an/l;->e:Z

    if-nez v0, :cond_2

    iget-boolean v0, p1, Lmaps/an/l;->f:Z

    if-eqz v0, :cond_0

    :cond_2
    iget-boolean v0, p1, Lmaps/an/l;->e:Z

    iput-boolean v0, p0, Lmaps/an/af;->e:Z

    iget-boolean v0, p0, Lmaps/an/af;->e:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p1, Lmaps/an/l;->f:Z

    if-eqz v0, :cond_3

    iput v2, p0, Lmaps/an/af;->a:I

    :cond_3
    iget v0, p0, Lmaps/an/af;->a:I

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    const-string v3, "Attempt to overwrite existing shader program: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lmaps/an/af;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lmaps/k/o;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/an/af;->c:Ljava/lang/String;

    iget-object v3, p0, Lmaps/an/af;->d:Ljava/lang/String;

    const v4, 0x8b31

    invoke-static {v4, v0}, Lmaps/an/af;->b(ILjava/lang/String;)I

    move-result v4

    if-nez v4, :cond_6

    move v0, v2

    :cond_4
    :goto_2
    iput v0, p0, Lmaps/an/af;->a:I

    iget v0, p0, Lmaps/an/af;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    iget v0, p0, Lmaps/an/af;->a:I

    invoke-virtual {p0, v0}, Lmaps/an/af;->a(I)V

    invoke-static {v2}, Landroid/opengl/GLES20;->glUseProgram(I)V

    :goto_3
    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    const v0, 0x8b30

    invoke-static {v0, v3}, Lmaps/an/af;->b(ILjava/lang/String;)I

    move-result v3

    if-nez v3, :cond_7

    move v0, v2

    goto :goto_2

    :cond_7
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v0, v4}, Landroid/opengl/GLES20;->glAttachShader(II)V

    invoke-static {}, Lmaps/an/m;->c()V

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    invoke-static {}, Lmaps/an/m;->c()V

    const-string v3, "aPosition"

    invoke-static {v0, v2, v3}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    invoke-static {}, Lmaps/an/m;->c()V

    const-string v3, "aNormal"

    invoke-static {v0, v1, v3}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    invoke-static {}, Lmaps/an/m;->c()V

    const/4 v3, 0x2

    const-string v4, "aColor"

    invoke-static {v0, v3, v4}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    invoke-static {}, Lmaps/an/m;->c()V

    const/4 v3, 0x4

    const-string v4, "aTextureCoord"

    invoke-static {v0, v3, v4}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    invoke-static {}, Lmaps/an/m;->c()V

    invoke-static {v0}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v3, v1, [I

    const v4, 0x8b82

    invoke-static {v0, v4, v3, v2}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v3, v3, v2

    if-eq v3, v1, :cond_4

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    move v0, v2

    goto :goto_2

    :cond_8
    iget-boolean v0, p1, Lmaps/an/l;->f:Z

    if-nez v0, :cond_9

    iget v0, p0, Lmaps/an/af;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    :cond_9
    iput v2, p0, Lmaps/an/af;->a:I

    goto :goto_3
.end method
