.class public Lmaps/an/y;
.super Lmaps/an/ap;


# instance fields
.field protected final a:Ljava/nio/ByteBuffer;

.field protected b:I

.field protected final c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private final h:Ljava/nio/ShortBuffer;

.field private final i:I

.field private j:I

.field private k:[I

.field private l:[I

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Z

.field private final q:Z


# direct methods
.method public constructor <init>([F)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/an/y;-><init>([FB)V

    return-void
.end method

.method private constructor <init>([FB)V
    .locals 4

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/an/ap;-><init>()V

    iput v2, p0, Lmaps/an/y;->b:I

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/an/y;->k:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lmaps/an/y;->l:[I

    iget-object v0, p0, Lmaps/an/y;->k:[I

    aput v3, v0, v2

    iget-object v0, p0, Lmaps/an/y;->l:[I

    aput v3, v0, v2

    iput-boolean v1, p0, Lmaps/an/y;->m:Z

    iput-boolean v2, p0, Lmaps/an/y;->n:Z

    iput-boolean v2, p0, Lmaps/an/y;->p:Z

    iput-boolean v2, p0, Lmaps/an/y;->q:Z

    iput-boolean v1, p0, Lmaps/an/y;->o:Z

    const/16 v0, 0x9

    iput v0, p0, Lmaps/an/y;->c:I

    const/4 v0, 0x5

    iput v0, p0, Lmaps/an/y;->j:I

    iget-boolean v0, p0, Lmaps/an/y;->q:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "COLORS_BYTE_MASK can not be used with this constructor "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmaps/an/y;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lmaps/an/y;->m:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/an/y;->b:I

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lmaps/an/y;->b:I

    :cond_1
    iget-boolean v0, p0, Lmaps/an/y;->n:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/an/y;->b:I

    iput v0, p0, Lmaps/an/y;->f:I

    iget v0, p0, Lmaps/an/y;->b:I

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lmaps/an/y;->b:I

    :cond_2
    iget-boolean v0, p0, Lmaps/an/y;->p:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lmaps/an/y;->b:I

    iput v0, p0, Lmaps/an/y;->g:I

    iget v0, p0, Lmaps/an/y;->b:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lmaps/an/y;->b:I

    :cond_3
    iget-boolean v0, p0, Lmaps/an/y;->q:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lmaps/an/y;->b:I

    iput v0, p0, Lmaps/an/y;->g:I

    iget v0, p0, Lmaps/an/y;->b:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/an/y;->b:I

    :cond_4
    iget-boolean v0, p0, Lmaps/an/y;->o:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lmaps/an/y;->b:I

    iput v0, p0, Lmaps/an/y;->e:I

    iget v0, p0, Lmaps/an/y;->b:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lmaps/an/y;->b:I

    :cond_5
    array-length v0, p1

    iget v1, p0, Lmaps/an/y;->b:I

    div-int/lit8 v1, v1, 0x4

    div-int/2addr v0, v1

    iput v0, p0, Lmaps/an/y;->d:I

    iget v0, p0, Lmaps/an/y;->d:I

    iget v1, p0, Lmaps/an/y;->b:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/an/y;->a:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lmaps/an/y;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/an/y;->h:Ljava/nio/ShortBuffer;

    iput v2, p0, Lmaps/an/y;->i:I

    return-void
.end method


# virtual methods
.method public final a(Lmaps/an/m;Lmaps/an/l;)Z
    .locals 8

    const v7, 0x88e4

    const v6, 0x8893

    const v5, 0x8892

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Lmaps/an/ap;->a(Lmaps/an/m;Lmaps/an/l;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p2, Lmaps/an/l;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/an/y;->k:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    iget-object v1, p0, Lmaps/an/y;->k:[I

    aget v1, v1, v3

    invoke-static {v5, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    iget-object v1, p0, Lmaps/an/y;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget v1, p0, Lmaps/an/y;->d:I

    iget v2, p0, Lmaps/an/y;->b:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lmaps/an/y;->a:Ljava/nio/ByteBuffer;

    invoke-static {v5, v1, v2, v7}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    invoke-static {v5, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    invoke-static {}, Lmaps/an/m;->c()V

    iget-object v1, p0, Lmaps/an/y;->h:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/an/y;->l:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    iget-object v1, p0, Lmaps/an/y;->l:[I

    aget v1, v1, v3

    invoke-static {v6, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    iget-object v1, p0, Lmaps/an/y;->h:Ljava/nio/ShortBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    iget v1, p0, Lmaps/an/y;->i:I

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lmaps/an/y;->h:Ljava/nio/ShortBuffer;

    invoke-static {v6, v1, v2, v7}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    invoke-static {v6, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    invoke-static {}, Lmaps/an/m;->c()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/an/y;->k:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    iget-object v1, p0, Lmaps/an/y;->k:[I

    aput v3, v1, v3

    iget-object v1, p0, Lmaps/an/y;->h:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/an/y;->l:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    iget-object v1, p0, Lmaps/an/y;->l:[I

    aput v3, v1, v3

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/an/y;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lmaps/an/y;

    iget-object v1, p0, Lmaps/an/y;->k:[I

    aget v1, v1, v0

    iget-object v2, p1, Lmaps/an/y;->k:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmaps/an/y;->l:[I

    aget v1, v1, v0

    iget-object v2, p1, Lmaps/an/y;->l:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
