.class public abstract Lmaps/an/ap;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/an/ap;->a:Z

    iput v0, p0, Lmaps/an/ap;->b:I

    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    iget v0, p0, Lmaps/an/ap;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/an/ap;->b:I

    return-void
.end method

.method protected a(Lmaps/an/m;Lmaps/an/l;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p2, Lmaps/an/l;->e:Z

    iget-boolean v2, p0, Lmaps/an/ap;->a:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p2, Lmaps/an/l;->f:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p2, Lmaps/an/l;->e:Z

    if-nez v1, :cond_2

    iget-boolean v1, p2, Lmaps/an/l;->f:Z

    if-nez v1, :cond_2

    iget v1, p0, Lmaps/an/ap;->b:I

    if-nez v1, :cond_0

    :cond_2
    iget-boolean v0, p2, Lmaps/an/l;->e:Z

    iput-boolean v0, p0, Lmaps/an/ap;->a:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final b()V
    .locals 1

    iget v0, p0, Lmaps/an/ap;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/an/ap;->b:I

    return-void
.end method
