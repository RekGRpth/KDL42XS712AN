.class public final Lmaps/an/t;
.super Lmaps/an/u;


# static fields
.field private static final i:Lmaps/an/ao;

.field private static final j:Lmaps/an/ao;


# instance fields
.field final f:Lmaps/ac/av;

.field private h:[F

.field private k:F

.field private l:[F

.field private m:[F

.field private n:Z

.field private o:F

.field private final p:Z

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    new-instance v0, Lmaps/an/ao;

    invoke-direct {v0, v2, v1}, Lmaps/an/ao;-><init>(FF)V

    sput-object v0, Lmaps/an/t;->i:Lmaps/an/ao;

    new-instance v0, Lmaps/an/ao;

    invoke-direct {v0, v1, v2}, Lmaps/an/ao;-><init>(FF)V

    sput-object v0, Lmaps/an/t;->j:Lmaps/an/ao;

    return-void
.end method

.method public constructor <init>(Lmaps/ay/v;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/an/t;-><init>(Lmaps/ay/v;B)V

    return-void
.end method

.method private constructor <init>(Lmaps/ay/v;B)V
    .locals 2

    const/4 v1, 0x3

    invoke-direct {p0, p1}, Lmaps/an/u;-><init>(Lmaps/ay/v;)V

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/an/t;->h:[F

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/an/t;->f:Lmaps/ac/av;

    new-array v0, v1, [F

    iput-object v0, p0, Lmaps/an/t;->l:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lmaps/an/t;->m:[F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/an/t;->q:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/t;->p:Z

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    iget-boolean v0, p0, Lmaps/an/t;->e:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iput p1, p0, Lmaps/an/t;->o:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/t;->b:Z

    return-void
.end method

.method public final a(Lmaps/ac/av;F)V
    .locals 0

    invoke-virtual {p0, p1, p2, p2, p2}, Lmaps/an/t;->a(Lmaps/ac/av;FFF)V

    return-void
.end method

.method public final a(Lmaps/ac/av;FFF)V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lmaps/an/t;->e:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/an/t;->f:Lmaps/ac/av;

    invoke-virtual {v0, p1}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    iget-object v0, p0, Lmaps/an/t;->l:[F

    const/4 v1, 0x0

    aput p2, v0, v1

    iget-object v0, p0, Lmaps/an/t;->l:[F

    aput p3, v0, v2

    iget-object v0, p0, Lmaps/an/t;->l:[F

    const/4 v1, 0x2

    aput p4, v0, v1

    iput-boolean v2, p0, Lmaps/an/t;->n:Z

    iput-boolean v2, p0, Lmaps/an/t;->b:Z

    return-void
.end method

.method public final a(Lmaps/an/an;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Lmaps/ac/av;F)V
    .locals 1

    iget-boolean v0, p0, Lmaps/an/t;->e:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/an/t;->f:Lmaps/ac/av;

    invoke-virtual {v0, p1}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    iput p2, p0, Lmaps/an/t;->k:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/an/t;->n:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/t;->b:Z

    return-void
.end method
