.class public Lmaps/an/aq;
.super Lmaps/an/ad;

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# instance fields
.field protected final f:Lmaps/an/m;


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 1

    iget-object v0, p0, Lmaps/an/aq;->f:Lmaps/an/m;

    invoke-virtual {v0}, Lmaps/an/m;->a()V

    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 5

    iget-object v1, p0, Lmaps/an/ad;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput p2, p0, Lmaps/an/ad;->b:I

    iput p3, p0, Lmaps/an/ad;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/ad;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v1, p0, Lmaps/an/ad;->e:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/an/ad;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/an/ad;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/g;

    iget v3, p0, Lmaps/an/ad;->b:I

    iget v4, p0, Lmaps/an/ad;->c:I

    invoke-virtual {v0, v3, v4}, Lmaps/an/g;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1

    iget-object v0, p0, Lmaps/an/aq;->f:Lmaps/an/m;

    invoke-virtual {v0}, Lmaps/an/m;->b()V

    return-void
.end method
