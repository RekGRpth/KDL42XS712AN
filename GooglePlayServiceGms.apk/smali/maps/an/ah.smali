.class public final Lmaps/an/ah;
.super Lmaps/an/ag;

# interfaces
.implements Lmaps/an/j;


# instance fields
.field private f:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    const-class v0, Lmaps/an/al;

    invoke-direct {p0, v0}, Lmaps/an/ag;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/an/ah;->f:[F

    return-void
.end method

.method public constructor <init>([F)V
    .locals 3

    const/4 v2, 0x4

    const/4 v1, 0x0

    const-class v0, Lmaps/an/aj;

    invoke-direct {p0, v0}, Lmaps/an/ag;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/an/ah;->f:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/an/ah;->f:[F

    iget-object v0, p0, Lmaps/an/ah;->f:[F

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public final a(FFFF)V
    .locals 2

    iget-boolean v0, p0, Lmaps/an/ah;->d:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/an/ah;->f:[F

    if-nez v0, :cond_1

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/an/ah;->f:[F

    :cond_1
    iget-object v0, p0, Lmaps/an/ah;->f:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lmaps/an/ah;->f:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lmaps/an/ah;->f:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    iget-object v0, p0, Lmaps/an/ah;->f:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    return-void
.end method

.method public final a(I)V
    .locals 4

    const/high16 v3, 0x437f0000    # 255.0f

    iget-boolean v0, p0, Lmaps/an/ah;->d:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/an/ah;->f:[F

    if-nez v0, :cond_1

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/an/ah;->f:[F

    :cond_1
    iget-object v0, p0, Lmaps/an/ah;->f:[F

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/an/ah;->f:[F

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/an/ah;->f:[F

    const/4 v1, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/an/ah;->f:[F

    const/4 v1, 0x3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    return-void
.end method
