.class public abstract Lmaps/an/q;
.super Ljava/lang/Object;


# static fields
.field public static final b:I

.field public static final c:I


# instance fields
.field private a:I

.field protected d:Z

.field final e:Lmaps/an/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lmaps/an/r;->values()[Lmaps/an/r;

    move-result-object v0

    array-length v0, v0

    sput v0, Lmaps/an/q;->b:I

    sget-object v0, Lmaps/an/r;->b:Lmaps/an/r;

    invoke-virtual {v0}, Lmaps/an/r;->a()I

    move-result v0

    sput v0, Lmaps/an/q;->c:I

    return-void
.end method

.method protected constructor <init>(Lmaps/an/r;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/an/q;->d:Z

    iput v0, p0, Lmaps/an/q;->a:I

    iput-object p1, p0, Lmaps/an/q;->e:Lmaps/an/r;

    return-void
.end method


# virtual methods
.method abstract a()V
.end method

.method a(Lmaps/an/m;Lmaps/an/l;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p2, Lmaps/an/l;->e:Z

    iget-boolean v2, p0, Lmaps/an/q;->d:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p2, Lmaps/an/l;->f:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p2, Lmaps/an/l;->e:Z

    if-nez v1, :cond_2

    iget-boolean v1, p2, Lmaps/an/l;->f:Z

    if-nez v1, :cond_2

    iget v1, p0, Lmaps/an/q;->a:I

    if-nez v1, :cond_0

    :cond_2
    iget-boolean v0, p2, Lmaps/an/l;->e:Z

    iput-boolean v0, p0, Lmaps/an/q;->d:Z

    iget-boolean v0, p0, Lmaps/an/q;->d:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final b()V
    .locals 1

    iget v0, p0, Lmaps/an/q;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/an/q;->a:I

    return-void
.end method

.method final c()V
    .locals 1

    iget v0, p0, Lmaps/an/q;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/an/q;->a:I

    return-void
.end method
