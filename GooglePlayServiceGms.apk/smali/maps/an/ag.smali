.class public abstract Lmaps/an/ag;
.super Lmaps/an/q;


# instance fields
.field protected a:Lmaps/an/af;

.field private f:Ljava/lang/Class;


# direct methods
.method protected constructor <init>(Ljava/lang/Class;)V
    .locals 1

    sget-object v0, Lmaps/an/r;->d:Lmaps/an/r;

    invoke-direct {p0, v0}, Lmaps/an/q;-><init>(Lmaps/an/r;)V

    iput-object p1, p0, Lmaps/an/ag;->f:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    invoke-static {}, Lmaps/an/m;->c()V

    return-void
.end method

.method public final a(Lmaps/an/m;Lmaps/an/l;)Z
    .locals 3

    invoke-super {p0, p1, p2}, Lmaps/an/q;->a(Lmaps/an/m;Lmaps/an/l;)Z

    move-result v0

    iget-object v1, p0, Lmaps/an/ag;->a:Lmaps/an/af;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lmaps/an/m;->d()Lmaps/an/ae;

    move-result-object v1

    iget-object v2, p0, Lmaps/an/ag;->f:Ljava/lang/Class;

    invoke-interface {v1, v2}, Lmaps/an/ae;->a(Ljava/lang/Class;)Lmaps/an/af;

    move-result-object v1

    iput-object v1, p0, Lmaps/an/ag;->a:Lmaps/an/af;

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/an/ag;->a:Lmaps/an/af;

    invoke-virtual {v1, p2}, Lmaps/an/af;->a(Lmaps/an/l;)Z

    :cond_1
    return v0
.end method
