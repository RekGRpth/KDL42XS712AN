.class public Lmaps/an/am;
.super Lmaps/an/q;


# instance fields
.field a:Lmaps/an/z;

.field private f:Lmaps/an/x;

.field private g:[I

.field private volatile h:I

.field private volatile i:I

.field private volatile j:Z

.field private volatile k:I

.field private volatile l:I

.field private volatile m:Z

.field private final n:I

.field private o:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmaps/an/am;-><init>(Lmaps/an/x;)V

    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lmaps/an/am;-><init>()V

    return-void
.end method

.method private constructor <init>(Lmaps/an/x;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/an/am;-><init>(Lmaps/an/x;I)V

    return-void
.end method

.method private constructor <init>(Lmaps/an/x;I)V
    .locals 3

    const/16 v2, 0x2901

    const/4 v1, 0x0

    sget-object v0, Lmaps/an/r;->b:Lmaps/an/r;

    invoke-direct {p0, v0}, Lmaps/an/q;-><init>(Lmaps/an/r;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/an/am;->f:Lmaps/an/x;

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/an/am;->g:[I

    const/16 v0, 0x2600

    iput v0, p0, Lmaps/an/am;->h:I

    const/16 v0, 0x2601

    iput v0, p0, Lmaps/an/am;->i:I

    iput-boolean v1, p0, Lmaps/an/am;->j:Z

    iput v2, p0, Lmaps/an/am;->k:I

    iput v2, p0, Lmaps/an/am;->l:I

    iput-boolean v1, p0, Lmaps/an/am;->m:Z

    iput-boolean v1, p0, Lmaps/an/am;->o:Z

    new-instance v0, Lmaps/an/z;

    invoke-direct {v0}, Lmaps/an/z;-><init>()V

    iput-object v0, p0, Lmaps/an/am;->a:Lmaps/an/z;

    iput-object p1, p0, Lmaps/an/am;->f:Lmaps/an/x;

    iput-boolean v1, p0, Lmaps/an/am;->o:Z

    const v0, 0x84c0

    iput v0, p0, Lmaps/an/am;->n:I

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Lmaps/an/am;->f:Lmaps/an/x;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lmaps/an/am;->n:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    const/16 v0, 0xde1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 4

    const v3, 0x8370

    const v2, 0x812f

    const/16 v1, 0x2901

    iget-boolean v0, p0, Lmaps/an/am;->d:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    if-eq p1, v1, :cond_1

    if-eq p1, v2, :cond_1

    if-ne p1, v3, :cond_2

    :cond_1
    if-eq p2, v1, :cond_3

    if-eq p2, v2, :cond_3

    if-eq p2, v3, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal Wrap Mode: wrapS = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " wrapT = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput p1, p0, Lmaps/an/am;->k:I

    iput p2, p0, Lmaps/an/am;->l:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/am;->m:Z

    return-void
.end method

.method protected final a(Lmaps/an/x;Z)V
    .locals 2

    iget-boolean v0, p0, Lmaps/an/am;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must be called BEFORE set live"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean p2, p0, Lmaps/an/am;->o:Z

    iput-object p1, p0, Lmaps/an/am;->f:Lmaps/an/x;

    return-void
.end method

.method final a(Lmaps/an/m;Lmaps/an/l;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/16 v3, 0xde1

    invoke-super {p0, p1, p2}, Lmaps/an/q;->a(Lmaps/an/m;Lmaps/an/l;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p2, Lmaps/an/l;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/an/am;->g:[I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    iget-object v1, p0, Lmaps/an/am;->g:[I

    aget v1, v1, v2

    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    iget-object v1, p0, Lmaps/an/am;->f:Lmaps/an/x;

    invoke-virtual {v1, p2}, Lmaps/an/x;->a(Lmaps/an/l;)Z

    const/16 v1, 0x2801

    iget v2, p0, Lmaps/an/am;->h:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2800

    iget v2, p0, Lmaps/an/am;->i:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2802

    iget v2, p0, Lmaps/an/am;->k:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2803

    iget v2, p0, Lmaps/an/am;->l:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    iget-boolean v1, p0, Lmaps/an/am;->o:Z

    if-eqz v1, :cond_0

    invoke-static {v3}, Landroid/opengl/GLES20;->glGenerateMipmap(I)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/an/am;->f:Lmaps/an/x;

    invoke-virtual {v1, p2}, Lmaps/an/x;->a(Lmaps/an/l;)Z

    iget-object v1, p0, Lmaps/an/am;->g:[I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    goto :goto_0
.end method

.method public final b(II)V
    .locals 3

    const/16 v2, 0x2601

    const/16 v1, 0x2600

    iget-boolean v0, p0, Lmaps/an/am;->d:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_1

    const/16 v0, 0x2703

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2701

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2702

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2700

    if-ne p1, v0, :cond_2

    :cond_1
    if-eq p2, v2, :cond_3

    if-eq p2, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal Filter Mode: min = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput p1, p0, Lmaps/an/am;->h:I

    iput p2, p0, Lmaps/an/am;->i:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/am;->j:Z

    return-void
.end method
