.class public final Lmaps/ab/a;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ac/r;

.field private final b:Lmaps/ac/j;

.field private final c:Lmaps/ac/av;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lmaps/ac/r;Lmaps/ac/j;Lmaps/ac/av;[Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ab/a;->a:Lmaps/ac/r;

    iput-object p2, p0, Lmaps/ab/a;->b:Lmaps/ac/j;

    if-nez p3, :cond_0

    invoke-interface {p2}, Lmaps/ac/j;->a()Lmaps/ac/bd;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bd;->e()Lmaps/ac/av;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lmaps/ab/a;->c:Lmaps/ac/av;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/ab/a;->d:Ljava/util/Set;

    array-length v1, p4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p4, v0

    iget-object v3, p0, Lmaps/ab/a;->d:Ljava/util/Set;

    invoke-static {v2}, Lmaps/ac/o;->a(Ljava/lang/String;)Lmaps/ac/o;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Ljava/util/Collection;Lmaps/ac/be;)Ljava/util/Collection;
    .locals 4

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/a;

    invoke-virtual {v0, p1}, Lmaps/ab/a;->a(Lmaps/ac/be;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method static a(Ljava/lang/String;)Lmaps/ac/av;
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x6

    invoke-static {p0}, Lmaps/bp/a;->a(Ljava/lang/String;)Lmaps/bp/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/bp/a;->a()I

    move-result v1

    invoke-virtual {v0}, Lmaps/bp/a;->b()I

    move-result v0

    invoke-static {v1, v0}, Lmaps/ac/av;->b(II)Lmaps/ac/av;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0x1:0x"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0xe

    if-gt v0, v2, :cond_1

    const-string v0, "0"

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    :try_start_0
    invoke-static {v0}, Lmaps/bf/c;->b(Ljava/lang/String;)I

    move-result v3

    invoke-static {v2}, Lmaps/bf/c;->b(Ljava/lang/String;)I

    move-result v2

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0, v3, v2}, Lmaps/ac/av;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x8

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lmaps/ac/r;
    .locals 1

    iget-object v0, p0, Lmaps/ab/a;->a:Lmaps/ac/r;

    return-object v0
.end method

.method final a(Ljava/util/Set;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/a;->d:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/ab/a;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public final a(Lmaps/ac/be;)Z
    .locals 1

    iget-object v0, p0, Lmaps/ab/a;->b:Lmaps/ac/j;

    invoke-interface {v0, p1}, Lmaps/ac/j;->a(Lmaps/ac/be;)Z

    move-result v0

    return v0
.end method

.method public final b()Lmaps/ac/bd;
    .locals 1

    iget-object v0, p0, Lmaps/ab/a;->b:Lmaps/ac/j;

    invoke-interface {v0}, Lmaps/ac/j;->a()Lmaps/ac/bd;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ab/a;->c:Lmaps/ac/av;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/ab/a;

    if-eqz v2, :cond_3

    check-cast p1, Lmaps/ab/a;

    iget-object v2, p1, Lmaps/ab/a;->a:Lmaps/ac/r;

    iget-object v3, p0, Lmaps/ab/a;->a:Lmaps/ac/r;

    invoke-virtual {v2, v3}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lmaps/ab/a;->b:Lmaps/ac/j;

    iget-object v3, p0, Lmaps/ab/a;->b:Lmaps/ac/j;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lmaps/ab/a;->c:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/ab/a;->c:Lmaps/ac/av;

    invoke-virtual {v2, v3}, Lmaps/ac/av;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lmaps/ab/a;->d:Ljava/util/Set;

    iget-object v3, p0, Lmaps/ab/a;->d:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lmaps/ab/a;->b:Lmaps/ac/j;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/ab/a;->c:Lmaps/ac/av;

    invoke-virtual {v1}, Lmaps/ac/av;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/ab/a;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/ab/a;->a:Lmaps/ac/r;

    invoke-virtual {v1}, Lmaps/ac/r;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/ab/a;->a:Lmaps/ac/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ab/a;->b:Lmaps/ac/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ab/a;->c:Lmaps/ac/av;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ab/a;->d:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
