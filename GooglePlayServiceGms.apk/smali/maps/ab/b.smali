.class public final Lmaps/ab/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/af/b;
.implements Lmaps/af/d;


# instance fields
.field private final a:Lmaps/ae/y;

.field private final b:Lmaps/ae/n;

.field private final c:Lmaps/ac/bt;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Collection;

.field private volatile f:Z

.field private volatile g:Lmaps/ab/d;


# direct methods
.method public constructor <init>(Lmaps/ae/y;Lmaps/ae/n;Lmaps/ac/bt;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/b;->d:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/b;->e:Ljava/util/Collection;

    iput-object p1, p0, Lmaps/ab/b;->a:Lmaps/ae/y;

    iput-object p2, p0, Lmaps/ab/b;->b:Lmaps/ae/n;

    iput-object p3, p0, Lmaps/ab/b;->c:Lmaps/ac/bt;

    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lmaps/ab/b;->g:Lmaps/ab/d;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lmaps/ab/b;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ab/b;->g:Lmaps/ab/d;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lmaps/ab/d;->a(Lmaps/ab/b;Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ab/b;->g:Lmaps/ab/d;

    iget-object v1, p0, Lmaps/ab/b;->e:Ljava/util/Collection;

    invoke-interface {v0, p0, v1}, Lmaps/ab/d;->a(Lmaps/ab/b;Ljava/util/Collection;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/ab/b;->c:Lmaps/ac/bt;

    return-object v0
.end method

.method public final a(Lmaps/ab/d;)V
    .locals 2

    iput-object p1, p0, Lmaps/ab/b;->g:Lmaps/ab/d;

    iget-object v0, p0, Lmaps/ab/b;->a:Lmaps/ae/y;

    iget-object v1, p0, Lmaps/ab/b;->c:Lmaps/ac/bt;

    invoke-interface {v0, v1, p0}, Lmaps/ae/y;->a(Lmaps/ac/bt;Lmaps/af/d;)V

    return-void
.end method

.method public final a(Lmaps/ac/bt;ILmaps/ac/bs;)V
    .locals 6

    const/4 v5, 0x3

    const/4 v1, 0x1

    if-ne p2, v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    if-ne p2, v1, :cond_2

    iput-boolean v1, p0, Lmaps/ab/b;->f:Z

    :cond_2
    const/4 v0, 0x0

    if-eqz p3, :cond_5

    check-cast p3, Lmaps/ac/cs;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p3}, Lmaps/ac/cs;->l()Lmaps/ac/cu;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Lmaps/ac/cu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    invoke-interface {v0}, Lmaps/ac/n;->e()I

    move-result v3

    if-ne v3, v5, :cond_3

    check-cast v0, Lmaps/ac/f;

    invoke-virtual {v0}, Lmaps/ac/f;->m()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lmaps/ac/f;->a()Lmaps/ac/o;

    move-result-object v3

    if-eqz v3, :cond_3

    sget-object v4, Lmaps/ac/o;->a:Lmaps/ac/o;

    if-eq v3, v4, :cond_3

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v0, v1

    :cond_5
    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    invoke-direct {p0}, Lmaps/ab/b;->b()V

    goto :goto_0

    :cond_7
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/f;

    new-instance v2, Lmaps/ab/c;

    invoke-direct {v2, v0}, Lmaps/ab/c;-><init>(Lmaps/ac/f;)V

    iget-object v0, p0, Lmaps/ab/b;->d:Ljava/util/Map;

    invoke-static {v2}, Lmaps/ab/c;->a(Lmaps/ab/c;)Lmaps/ac/r;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lmaps/ab/b;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/c;

    iget-object v2, p0, Lmaps/ab/b;->b:Lmaps/ae/n;

    invoke-static {v0}, Lmaps/ab/c;->a(Lmaps/ab/c;)Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {v2, v0, p0}, Lmaps/ae/n;->a(Lmaps/ac/r;Lmaps/af/b;)V

    goto :goto_3
.end method

.method public final a(Lmaps/ac/r;ILmaps/ac/z;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lmaps/ab/b;->d:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/b;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/c;

    iget-object v2, p0, Lmaps/ab/b;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lmaps/ac/z;->e()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ab/c;->a(Lmaps/ac/av;)V

    iget-object v1, p0, Lmaps/ab/b;->e:Ljava/util/Collection;

    invoke-virtual {v0}, Lmaps/ab/c;->a()Lmaps/ab/a;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_2
    if-ne p2, v3, :cond_3

    iput-boolean v3, p0, Lmaps/ab/b;->f:Z

    :cond_3
    if-eqz v2, :cond_0

    invoke-direct {p0}, Lmaps/ab/b;->b()V

    goto :goto_0
.end method
