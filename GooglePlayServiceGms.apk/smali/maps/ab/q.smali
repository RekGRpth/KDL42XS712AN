.class public final Lmaps/ab/q;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/af/b;


# static fields
.field private static b:Lmaps/ab/q;

.field private static final p:Lmaps/ac/ad;


# instance fields
.field private final a:Ljava/util/Map;

.field private final c:Lmaps/ax/f;

.field private final d:Lmaps/ax/f;

.field private e:Lmaps/ac/r;

.field private f:Lmaps/ac/ad;

.field private g:Lmaps/ac/z;

.field private final h:Ljava/util/Set;

.field private final i:Ljava/util/List;

.field private final j:Ljava/util/Set;

.field private final k:Ljava/lang/Object;

.field private final l:Lmaps/ae/n;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Lmaps/ab/e;

.field private volatile q:Lmaps/ac/ad;

.field private volatile r:Lmaps/ac/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/16 v2, 0x0

    new-instance v0, Lmaps/ac/ad;

    new-instance v1, Lmaps/ac/r;

    invoke-direct {v1, v2, v3, v2, v3}, Lmaps/ac/r;-><init>(JJ)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmaps/ac/ad;-><init>(Lmaps/ac/r;I)V

    sput-object v0, Lmaps/ab/q;->p:Lmaps/ac/ad;

    return-void
.end method

.method private constructor <init>(Lmaps/ae/n;)V
    .locals 2

    const/16 v1, 0x64

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/ab/q;->h:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ab/q;->i:Ljava/util/List;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/ab/q;->j:Ljava/util/Set;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/q;->m:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    new-instance v0, Lmaps/ax/f;

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    new-instance v0, Lmaps/ax/f;

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ab/q;->d:Lmaps/ax/f;

    iput-object p1, p0, Lmaps/ab/q;->l:Lmaps/ae/n;

    new-instance v0, Lmaps/ab/g;

    invoke-direct {v0}, Lmaps/ab/g;-><init>()V

    iput-object v0, p0, Lmaps/ab/q;->o:Lmaps/ab/e;

    return-void
.end method

.method public static a()Lmaps/ab/q;
    .locals 1

    sget-object v0, Lmaps/ab/q;->b:Lmaps/ab/q;

    return-object v0
.end method

.method public static a(Lmaps/ae/n;)Lmaps/ab/q;
    .locals 1

    sget-object v0, Lmaps/ab/q;->b:Lmaps/ab/q;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ab/q;

    invoke-direct {v0, p0}, Lmaps/ab/q;-><init>(Lmaps/ae/n;)V

    sput-object v0, Lmaps/ab/q;->b:Lmaps/ab/q;

    :cond_0
    sget-object v0, Lmaps/ab/q;->b:Lmaps/ab/q;

    return-object v0
.end method

.method static synthetic a(Lmaps/ab/q;Lmaps/ac/z;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ab/q;->d(Lmaps/ac/z;)V

    return-void
.end method

.method private a(Lmaps/ac/r;Lmaps/af/b;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/q;->l:Lmaps/ae/n;

    invoke-virtual {v0, p1}, Lmaps/ae/n;->b(Lmaps/ac/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/ab/q;->l:Lmaps/ae/n;

    invoke-virtual {v0, p1, p2}, Lmaps/ae/n;->a(Lmaps/ac/r;Lmaps/af/b;)V

    goto :goto_0
.end method

.method private a(Lmaps/ac/r;Lmaps/ac/ad;)Z
    .locals 7

    iget-object v2, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/ad;

    invoke-virtual {p2, v0}, Lmaps/ac/ad;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    monitor-exit v2

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    invoke-virtual {v1, p1, p2}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz v0, :cond_1

    iget-object v3, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, p0, Lmaps/ab/q;->d:Lmaps/ax/f;

    invoke-virtual {v1, p1, v0}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Lmaps/ab/q;->l:Lmaps/ae/n;

    invoke-virtual {v0}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/ae/n;->c(Lmaps/ac/r;)Lmaps/ac/aa;

    move-result-object v4

    if-nez v4, :cond_2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    :try_start_3
    sget-object v0, Lmaps/ab/q;->p:Lmaps/ac/ad;

    if-ne p2, v0, :cond_4

    invoke-static {}, Lmaps/m/ay;->e()Lmaps/m/ay;

    move-result-object v0

    move-object v1, v0

    :goto_2
    invoke-virtual {v4}, Lmaps/ac/aa;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/r;

    invoke-virtual {v0, p1}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lmaps/ab/q;->d:Lmaps/ax/f;

    iget-object v6, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    invoke-virtual {v6, v0}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v5, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    sget-object v6, Lmaps/ab/q;->p:Lmaps/ac/ad;

    invoke-virtual {v5, v0, v6}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    :try_start_5
    iget-object v0, p0, Lmaps/ab/q;->l:Lmaps/ae/n;

    invoke-virtual {p2}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ae/n;->c(Lmaps/ac/r;)Lmaps/ac/aa;

    move-result-object v0

    if-nez v0, :cond_5

    monitor-exit v3

    goto :goto_1

    :cond_5
    invoke-virtual {v0}, Lmaps/ac/aa;->c()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_6
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method private c(Lmaps/ac/z;)Lmaps/ac/ad;
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    invoke-virtual {p1}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/ad;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lmaps/ac/z;->c()Lmaps/ac/aa;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lmaps/ab/q;->p:Lmaps/ac/ad;

    :goto_0
    iget-object v3, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    invoke-virtual {p1}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v3, Lmaps/ab/q;->p:Lmaps/ac/ad;

    if-eq v0, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lmaps/ab/q;->d(Lmaps/ac/z;)V

    :cond_1
    return-object v0

    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lmaps/ac/aa;->a()Lmaps/ac/ad;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private d(Lmaps/ac/z;)V
    .locals 3

    invoke-direct {p0}, Lmaps/ab/q;->m()V

    iget-object v1, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/s;

    invoke-interface {v0, p0, p1}, Lmaps/ab/s;->a(Lmaps/ab/q;Lmaps/ac/z;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private k()V
    .locals 3

    iget-object v1, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/s;

    invoke-interface {v0, p0}, Lmaps/ab/s;->a(Lmaps/ab/q;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private l()V
    .locals 3

    invoke-direct {p0}, Lmaps/ab/q;->m()V

    iget-object v1, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/s;

    invoke-interface {v0}, Lmaps/ab/s;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private m()V
    .locals 6

    iget-object v2, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p0}, Lmaps/ab/q;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/aa;

    invoke-virtual {v0}, Lmaps/ac/aa;->a()Lmaps/ac/ad;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v4, p0, Lmaps/ab/q;->q:Lmaps/ac/ad;

    invoke-virtual {v1, v4}, Lmaps/ac/ad;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lmaps/ab/q;->r:Lmaps/ac/ad;

    invoke-virtual {v1, v4}, Lmaps/ac/ad;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/ab/q;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/ac/aa;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ab/k;

    if-nez v1, :cond_3

    new-instance v1, Lmaps/ab/k;

    invoke-direct {v1, v0}, Lmaps/ab/k;-><init>(Lmaps/ac/aa;)V

    iget-object v4, p0, Lmaps/ab/q;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/ac/aa;->e()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    iget-object v4, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/ac/aa;->b()Lmaps/ac/r;

    move-result-object v0

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    :try_start_1
    invoke-virtual {v1, v0}, Lmaps/ab/k;->a(Lmaps/ac/aa;)Z

    goto :goto_2

    :cond_4
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/r;ZZ)Lmaps/ab/k;
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/k;

    if-eqz p2, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lmaps/ab/k;->f()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_4

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_0

    monitor-exit v3

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ab/q;->l:Lmaps/ae/n;

    invoke-virtual {v0, p1}, Lmaps/ae/n;->a(Lmaps/ac/r;)Lmaps/ac/z;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/ab/q;->a(Lmaps/ac/r;Lmaps/af/b;)V

    monitor-exit v3

    move-object v0, v1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, p1}, Lmaps/ac/z;->a(Lmaps/ac/r;)Lmaps/ac/aa;

    move-result-object v0

    if-nez v0, :cond_2

    monitor-exit v3

    move-object v0, v1

    goto :goto_1

    :cond_2
    new-instance v1, Lmaps/ab/k;

    invoke-direct {v1, v0}, Lmaps/ab/k;-><init>(Lmaps/ac/aa;)V

    if-eqz p3, :cond_3

    iget-object v0, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v2, :cond_3

    invoke-virtual {v2, p1}, Lmaps/ab/k;->a(Lmaps/ac/r;)Lmaps/ab/k;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ab/k;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/r;

    iget-object v5, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    invoke-interface {v5, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v2, v1

    goto :goto_0
.end method

.method public final a(Lmaps/ac/r;)Lmaps/ac/ad;
    .locals 2

    iget-object v1, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/ad;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lmaps/ab/q;->l:Lmaps/ae/n;

    invoke-virtual {v1, p1}, Lmaps/ae/n;->a(Lmaps/ac/r;)Lmaps/ac/z;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v1}, Lmaps/ab/q;->c(Lmaps/ac/z;)Lmaps/ac/ad;

    move-result-object v0

    :cond_0
    :goto_0
    sget-object v1, Lmaps/ab/q;->p:Lmaps/ac/ad;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    invoke-direct {p0, p1, p0}, Lmaps/ab/q;->a(Lmaps/ac/r;Lmaps/af/b;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 5

    iget-object v1, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v1

    if-nez p1, :cond_0

    :try_start_0
    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lmaps/ab/q;->j:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ab/q;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/ab/q;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/ab/q;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/ab/q;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/ab/q;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/ab/q;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/r;

    iget-object v3, p0, Lmaps/ab/q;->l:Lmaps/ae/n;

    invoke-virtual {v3, v0}, Lmaps/ae/n;->a(Lmaps/ac/r;)Lmaps/ac/z;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v4, p0, Lmaps/ab/q;->i:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lmaps/ab/q;->h:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    invoke-direct {p0, v0, p0}, Lmaps/ab/q;->a(Lmaps/ac/r;Lmaps/af/b;)V

    goto :goto_1

    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lmaps/ab/q;->l()V

    goto :goto_0
.end method

.method public final a(Lmaps/ab/s;)V
    .locals 2

    iget-object v0, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Lmaps/ac/ad;)V
    .locals 2

    iget-object v1, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->f:Lmaps/ac/ad;

    invoke-virtual {p1, v0}, Lmaps/ac/ad;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lmaps/ab/q;->f:Lmaps/ac/ad;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-direct {p0, v0, p0}, Lmaps/ab/q;->a(Lmaps/ac/r;Lmaps/af/b;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ac/ad;Lmaps/ac/ad;)V
    .locals 0

    iput-object p1, p0, Lmaps/ab/q;->q:Lmaps/ac/ad;

    iput-object p2, p0, Lmaps/ab/q;->r:Lmaps/ac/ad;

    invoke-direct {p0}, Lmaps/ab/q;->m()V

    return-void
.end method

.method public final a(Lmaps/ac/r;ILmaps/ac/z;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x2

    if-eq p2, v3, :cond_5

    if-nez p2, :cond_5

    iget-object v3, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lmaps/ab/q;->f:Lmaps/ac/ad;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lmaps/ab/q;->f:Lmaps/ac/ad;

    invoke-virtual {v4}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v4

    invoke-virtual {v4, p1}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lmaps/ab/q;->f:Lmaps/ac/ad;

    const/4 v4, 0x0

    iput-object v4, p0, Lmaps/ab/q;->f:Lmaps/ac/ad;

    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    invoke-virtual {p3, v0}, Lmaps/ac/z;->a(Lmaps/ac/ad;)Lmaps/ac/aa;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lmaps/ac/aa;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/r;

    invoke-virtual {v3}, Lmaps/ac/aa;->a()Lmaps/ac/ad;

    move-result-object v5

    invoke-direct {p0, v0, v5}, Lmaps/ab/q;->a(Lmaps/ac/r;Lmaps/ac/ad;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Lmaps/ab/r;

    invoke-direct {v5, p0}, Lmaps/ab/r;-><init>(Lmaps/ab/q;)V

    invoke-direct {p0, v0, v5}, Lmaps/ab/q;->a(Lmaps/ac/r;Lmaps/af/b;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    invoke-direct {p0, p3}, Lmaps/ab/q;->c(Lmaps/ac/z;)Lmaps/ac/ad;

    iget-object v3, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, Lmaps/ab/q;->e:Lmaps/ac/r;

    invoke-virtual {p1, v0}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v0

    iget-object v4, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    invoke-virtual {v4}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v4

    invoke-virtual {v0, v4}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_3
    invoke-virtual {p3}, Lmaps/ac/z;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    move v0, v1

    :goto_1
    const/4 v4, 0x0

    iput-object v4, p0, Lmaps/ab/q;->e:Lmaps/ac/r;

    :goto_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lmaps/ab/q;->k()V

    :cond_4
    iget-object v3, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iget-object v0, p0, Lmaps/ab/q;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/ab/q;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/ab/q;->i:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    :goto_3
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lmaps/ab/q;->l()V

    :cond_5
    return-void

    :cond_6
    :try_start_3
    iput-object p3, p0, Lmaps/ab/q;->g:Lmaps/ac/z;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_2
.end method

.method public final a(Lmaps/ac/z;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v0

    sget-object v1, Lmaps/ab/q;->p:Lmaps/ac/ad;

    invoke-direct {p0, v0, v1}, Lmaps/ab/q;->a(Lmaps/ac/r;Lmaps/ac/ad;)Z

    invoke-direct {p0, p1}, Lmaps/ab/q;->d(Lmaps/ac/z;)V

    :cond_0
    return-void
.end method

.method public final b(Lmaps/ac/z;)Lmaps/ac/aa;
    .locals 1

    invoke-virtual {p1}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ab/q;->a(Lmaps/ac/r;)Lmaps/ac/ad;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lmaps/ac/z;->a(Lmaps/ac/ad;)Lmaps/ac/aa;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lmaps/ac/r;)Lmaps/ac/ae;
    .locals 2

    invoke-virtual {p0, p1}, Lmaps/ab/q;->a(Lmaps/ac/r;)Lmaps/ac/ad;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lmaps/ac/af;

    invoke-direct {v1}, Lmaps/ac/af;-><init>()V

    invoke-virtual {v1, v0}, Lmaps/ac/af;->a(Lmaps/ac/ad;)Lmaps/ac/af;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/af;->a()Lmaps/ac/ae;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ab/q;->q:Lmaps/ac/ad;

    iput-object v0, p0, Lmaps/ab/q;->r:Lmaps/ac/ad;

    invoke-direct {p0}, Lmaps/ab/q;->m()V

    return-void
.end method

.method public final b(Lmaps/ab/s;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/q;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b(Lmaps/ac/ad;Lmaps/ac/ad;)Z
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lmaps/ab/q;->q:Lmaps/ac/ad;

    invoke-virtual {p1, v0}, Lmaps/ac/ad;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ab/q;->r:Lmaps/ac/ad;

    invoke-virtual {p2, v0}, Lmaps/ac/ad;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lmaps/ac/r;)Lmaps/ac/ad;
    .locals 3

    iget-object v1, p0, Lmaps/ab/q;->c:Lmaps/ax/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->d:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/ad;

    sget-object v2, Lmaps/ab/q;->p:Lmaps/ac/ad;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()Lmaps/ac/z;
    .locals 2

    iget-object v1, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d(Lmaps/ac/r;)V
    .locals 3

    if-nez p1, :cond_2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ab/q;->e:Lmaps/ac/r;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    const/4 v0, 0x1

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/ab/q;->k()V

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    iget-object v1, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ab/q;->e:Lmaps/ac/r;

    invoke-virtual {p1, v0}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/ab/q;->g:Lmaps/ac/z;

    invoke-virtual {v0}, Lmaps/ac/z;->a()Lmaps/ac/r;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/ac/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_4
    :try_start_2
    iput-object p1, p0, Lmaps/ab/q;->e:Lmaps/ac/r;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-direct {p0, p1, p0}, Lmaps/ab/q;->a(Lmaps/ac/r;Lmaps/af/b;)V

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    iget-object v1, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Ljava/util/List;
    .locals 2

    iget-object v1, p0, Lmaps/ab/q;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->i:Ljava/util/List;

    invoke-static {v0}, Lmaps/m/ay;->a(Ljava/util/Collection;)Lmaps/m/ay;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e(Lmaps/ac/r;)Lmaps/ab/k;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lmaps/ab/q;->a(Lmaps/ac/r;ZZ)Lmaps/ab/k;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/util/Set;
    .locals 3

    invoke-virtual {p0}, Lmaps/ab/q;->e()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/z;

    invoke-virtual {p0, v0}, Lmaps/ab/q;->b(Lmaps/ac/z;)Lmaps/ac/aa;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public final g()Ljava/util/Set;
    .locals 3

    invoke-virtual {p0}, Lmaps/ab/q;->e()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/z;

    invoke-virtual {p0, v0}, Lmaps/ab/q;->b(Lmaps/ac/z;)Lmaps/ac/aa;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/ac/aa;->a()Lmaps/ac/ad;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public final h()Z
    .locals 2

    invoke-virtual {p0}, Lmaps/ab/q;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/aa;

    invoke-virtual {v0}, Lmaps/ac/aa;->e()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/util/Set;
    .locals 2

    iget-object v1, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/q;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/bo;->a(Ljava/util/Collection;)Lmaps/m/bo;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final j()Lmaps/ab/e;
    .locals 1

    iget-object v0, p0, Lmaps/ab/q;->o:Lmaps/ab/e;

    return-object v0
.end method
