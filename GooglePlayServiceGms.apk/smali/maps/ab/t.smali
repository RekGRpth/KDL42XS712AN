.class public final Lmaps/ab/t;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ab/e;
.implements Lmaps/ai/b;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lmaps/ac/av;

.field private final d:Lmaps/ac/av;

.field private volatile e:Z

.field private volatile f:Lmaps/ac/av;

.field private final g:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private volatile h:Lmaps/ab/v;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    const v1, 0x4c4b40

    const v2, 0x3d0900

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0, v1, v1}, Lmaps/ac/av;-><init>(II)V

    new-instance v1, Lmaps/ac/av;

    invoke-direct {v1, v2, v2}, Lmaps/ac/av;-><init>(II)V

    invoke-direct {p0, p1, v0, v1}, Lmaps/ab/t;-><init>(Ljava/lang/String;Lmaps/ac/av;Lmaps/ac/av;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lmaps/ac/av;Lmaps/ac/av;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ab/t;->b:Ljava/lang/String;

    iput-object p2, p0, Lmaps/ab/t;->c:Lmaps/ac/av;

    invoke-virtual {p2}, Lmaps/ac/av;->f()I

    move-result v0

    invoke-virtual {p3}, Lmaps/ac/av;->f()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p2}, Lmaps/ac/av;->g()I

    move-result v0

    invoke-virtual {p3}, Lmaps/ac/av;->g()I

    move-result v1

    if-lt v0, v1, :cond_0

    iput-object p3, p0, Lmaps/ab/t;->d:Lmaps/ac/av;

    :goto_0
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ab/t;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/ab/t;->f:Lmaps/ac/av;

    new-instance v0, Lmaps/ab/v;

    invoke-direct {v0}, Lmaps/ab/v;-><init>()V

    iput-object v0, p0, Lmaps/ab/t;->h:Lmaps/ab/v;

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/ab/t;->c:Lmaps/ac/av;

    iput-object v0, p0, Lmaps/ab/t;->d:Lmaps/ac/av;

    goto :goto_0
.end method

.method private static a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/cx;
    .locals 5

    invoke-virtual {p0, p1}, Lmaps/ac/av;->f(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    invoke-static {v0}, Lmaps/ac/bt;->b(Lmaps/ac/av;)Lmaps/ac/bt;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bt;->g()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {p0, p1}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    invoke-static {v0}, Lmaps/ac/bt;->b(Lmaps/ac/av;)Lmaps/ac/bt;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bt;->h()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/ac/av;->f()I

    move-result v2

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v3

    if-le v2, v3, :cond_0

    new-instance v2, Lmaps/ac/av;

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v0, v2}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    :cond_0
    new-instance v2, Lmaps/ac/bd;

    invoke-direct {v2, v1, v0}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v2}, Lmaps/ac/cx;->a(Lmaps/ac/bd;)Lmaps/ac/cx;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lmaps/ab/t;)V
    .locals 2

    invoke-static {}, Lmaps/ai/c;->c()Lmaps/ai/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ab/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lmaps/ai/c;->a(Ljava/lang/String;Lmaps/ai/b;)Lmaps/ai/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ab/t;->a(Lmaps/ai/a;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/bt;)Ljava/util/Collection;
    .locals 4

    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v0

    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v0

    iget-object v1, p0, Lmaps/ab/t;->h:Lmaps/ab/v;

    iget-boolean v2, p0, Lmaps/ab/t;->e:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lmaps/ab/v;->c:Lmaps/ac/cx;

    invoke-virtual {v2, v0}, Lmaps/ac/cx;->a(Lmaps/ac/be;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lmaps/ac/bd;->e()Lmaps/ac/av;

    move-result-object v2

    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lmaps/ab/t;->e:Z

    if-eqz v3, :cond_2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    iget-object v2, v1, Lmaps/ab/v;->b:Lmaps/ac/cx;

    invoke-virtual {v2, v0}, Lmaps/ac/cx;->a(Lmaps/ac/be;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v1, Lmaps/ab/v;->a:Lmaps/ab/j;

    invoke-virtual {v0, p1}, Lmaps/ab/j;->a(Lmaps/ac/bt;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, p0, Lmaps/ab/t;->e:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v2, p0, Lmaps/ab/t;->f:Lmaps/ac/av;

    new-instance v2, Lmaps/ab/u;

    invoke-static {}, Lmaps/ba/i;->a()Lmaps/by/c;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lmaps/ab/u;-><init>(Lmaps/ab/t;Lmaps/by/c;)V

    invoke-virtual {v2}, Lmaps/ab/u;->e()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    sget-object v0, Lmaps/ab/t;->a:Ljava/util/Collection;

    goto :goto_0
.end method

.method public final a(Lmaps/ab/f;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/ab/t;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final a(Lmaps/ai/a;)V
    .locals 5

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    monitor-enter p1

    :try_start_0
    iget-boolean v0, p0, Lmaps/ab/t;->e:Z

    if-nez v0, :cond_1

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lmaps/ai/a;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lmaps/ai/a;->e()[B

    move-result-object v0

    if-eqz v0, :cond_2

    array-length v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-lez v1, :cond_2

    :try_start_2
    iget-object v1, p0, Lmaps/ab/t;->f:Lmaps/ac/av;

    iget-object v2, p0, Lmaps/ab/t;->c:Lmaps/ac/av;

    invoke-static {v1, v2}, Lmaps/ab/t;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/cx;

    move-result-object v1

    iget-object v2, p0, Lmaps/ab/t;->f:Lmaps/ac/av;

    iget-object v3, p0, Lmaps/ab/t;->d:Lmaps/ac/av;

    invoke-static {v2, v3}, Lmaps/ab/t;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/cx;

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Ljava/io/InputStreamReader;

    const-string v4, "UTF-8"

    invoke-direct {v0, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lmaps/ab/j;->a(Ljava/io/Reader;Lmaps/ac/cx;)Lmaps/ab/j;

    move-result-object v0

    new-instance v3, Lmaps/ab/v;

    invoke-direct {v3, v0, v1, v2}, Lmaps/ab/v;-><init>(Lmaps/ab/j;Lmaps/ac/cx;Lmaps/ac/cx;)V

    iput-object v3, p0, Lmaps/ab/t;->h:Lmaps/ab/v;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    :goto_1
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, Lmaps/ab/t;->e:Z

    iget-object v0, p0, Lmaps/ab/t;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/f;

    invoke-interface {v0}, Lmaps/ab/f;->a()V

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "LazyBuildingBoundProvider"

    invoke-static {v1, v0}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final a(Lmaps/ac/o;)Z
    .locals 1

    iget-object v0, p0, Lmaps/ab/t;->h:Lmaps/ab/v;

    iget-object v0, v0, Lmaps/ab/v;->a:Lmaps/ab/j;

    invoke-virtual {v0, p1}, Lmaps/ab/j;->a(Lmaps/ac/o;)Z

    move-result v0

    return v0
.end method

.method public final b(Lmaps/ab/f;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/ab/t;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
