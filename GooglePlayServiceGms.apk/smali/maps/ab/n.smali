.class public final Lmaps/ab/n;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/af/d;


# static fields
.field public static final a:Lmaps/ab/l;


# instance fields
.field private final b:Lmaps/ae/y;

.field private final c:Lmaps/ae/z;

.field private final d:Lmaps/ax/f;

.field private e:I

.field private final f:Ljava/util/Set;

.field private final g:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmaps/ab/l;

    invoke-static {}, Lmaps/m/ay;->e()Lmaps/m/ay;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/ab/l;-><init>(Ljava/util/List;)V

    sput-object v0, Lmaps/ab/n;->a:Lmaps/ab/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/ab/n;->f:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lmaps/ab/n;->g:Ljava/util/Set;

    sget-object v0, Lmaps/ao/b;->n:Lmaps/ao/b;

    invoke-static {v0}, Lmaps/ae/ab;->a(Lmaps/ao/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ao/b;->n:Lmaps/ao/b;

    invoke-static {v0}, Lmaps/ae/ab;->b(Lmaps/ao/b;)Lmaps/ae/y;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/n;->b:Lmaps/ae/y;

    new-instance v0, Lmaps/ax/f;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ab/n;->d:Lmaps/ax/f;

    new-instance v0, Lmaps/ab/o;

    invoke-direct {v0, p0}, Lmaps/ab/o;-><init>(Lmaps/ab/n;)V

    iput-object v0, p0, Lmaps/ab/n;->c:Lmaps/ae/z;

    iget-object v0, p0, Lmaps/ab/n;->b:Lmaps/ae/y;

    iget-object v1, p0, Lmaps/ab/n;->c:Lmaps/ae/z;

    invoke-interface {v0, v1}, Lmaps/ae/y;->a(Lmaps/ae/z;)V

    :goto_0
    return-void

    :cond_0
    iput-object v1, p0, Lmaps/ab/n;->b:Lmaps/ae/y;

    iput-object v1, p0, Lmaps/ab/n;->d:Lmaps/ax/f;

    iput-object v1, p0, Lmaps/ab/n;->c:Lmaps/ae/z;

    goto :goto_0
.end method

.method private declared-synchronized b(Lmaps/ac/bt;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ab/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/ab/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/ab/n;->b:Lmaps/ae/y;

    invoke-interface {v0, p1, p0}, Lmaps/ae/y;->a(Lmaps/ac/bt;Lmaps/af/d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lmaps/ac/bt;)Lmaps/ab/l;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ab/n;->d:Lmaps/ax/f;

    invoke-virtual {v0, p1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lmaps/ab/n;->b(Lmaps/ac/bt;)V

    iget v0, p0, Lmaps/ab/n;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ab/n;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ab/n;->d:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    iget-object v0, p0, Lmaps/ab/n;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/ab/p;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/n;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lmaps/ac/bt;ILmaps/ac/bs;)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move v1, v0

    :goto_0
    :pswitch_1
    if-eqz v0, :cond_0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ab/n;->d:Lmaps/ax/f;

    invoke-virtual {v0, p1, v2}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v1, :cond_5

    iget-object v0, p0, Lmaps/ab/n;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/p;

    invoke-interface {v0}, Lmaps/ab/p;->b()V

    goto :goto_1

    :pswitch_2
    instance-of v0, p3, Lmaps/ac/cs;

    if-eqz v0, :cond_3

    check-cast p3, Lmaps/ac/cs;

    new-instance v2, Lmaps/ab/m;

    invoke-direct {v2}, Lmaps/ab/m;-><init>()V

    invoke-virtual {p3}, Lmaps/ac/cs;->l()Lmaps/ac/cu;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Lmaps/ac/cu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    instance-of v4, v0, Lmaps/ac/f;

    if-eqz v4, :cond_1

    check-cast v0, Lmaps/ac/f;

    invoke-virtual {v0}, Lmaps/ac/f;->l()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2, v0}, Lmaps/ab/m;->a(Lmaps/ac/f;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Lmaps/ab/m;->a()Lmaps/ab/l;

    move-result-object v0

    :goto_3
    move-object v2, v0

    move v0, v1

    goto :goto_0

    :cond_3
    sget-object v0, Lmaps/ab/n;->a:Lmaps/ab/l;

    goto :goto_3

    :pswitch_3
    sget-object v2, Lmaps/ab/n;->a:Lmaps/ab/l;

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lmaps/ab/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_5
    return-void

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lmaps/ab/p;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/n;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
