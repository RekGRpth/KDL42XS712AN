.class public final Lmaps/bn/j;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bn/k;


# instance fields
.field private a:Ljava/util/Vector;

.field private b:Z

.field private final c:Lmaps/bv/a;

.field private synthetic d:Lmaps/bn/d;


# direct methods
.method private constructor <init>(Lmaps/bn/d;)V
    .locals 2

    iput-object p1, p0, Lmaps/bn/j;->d:Lmaps/bn/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lmaps/bn/j;->a:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bn/j;->b:Z

    new-instance v0, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/g;->a:Lmaps/bv/c;

    invoke-direct {v0, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    iput-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/bn/d;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/bn/j;-><init>(Lmaps/bn/d;)V

    return-void
.end method

.method private constructor <init>(Lmaps/bn/d;Lmaps/bv/a;)V
    .locals 1

    iput-object p1, p0, Lmaps/bn/j;->d:Lmaps/bn/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lmaps/bn/j;->a:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bn/j;->b:Z

    invoke-virtual {p2}, Lmaps/bv/a;->a()Lmaps/bv/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/bn/d;Lmaps/bv/a;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/bn/j;-><init>(Lmaps/bn/d;Lmaps/bv/a;)V

    return-void
.end method

.method private a()Lmaps/bn/h;
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lmaps/bn/j;->a:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_0

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v2, p0, Lmaps/bn/j;->b:Z

    if-nez v2, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    new-instance v2, Lmaps/bn/h;

    iget-object v0, p0, Lmaps/bn/j;->d:Lmaps/bn/d;

    iget-object v3, p0, Lmaps/bn/j;->a:Ljava/util/Vector;

    iget-object v4, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    invoke-direct {v2, v0, v3, v4}, Lmaps/bn/h;-><init>(Lmaps/bn/d;Ljava/util/Vector;Lmaps/bv/a;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lmaps/bn/j;->a:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bn/j;->b:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/br/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lmaps/bn/j;)V
    .locals 3

    const/16 v2, 0x19

    iget-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    invoke-virtual {v0, v2}, Lmaps/bv/a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lmaps/bv/a;->e(II)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lmaps/bn/j;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    return-void
.end method

.method static synthetic a(Lmaps/bn/j;Z)V
    .locals 2

    iget-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    const/16 v1, 0x24

    invoke-virtual {v0, v1, p1}, Lmaps/bv/a;->a(IZ)Lmaps/bv/a;

    return-void
.end method

.method static synthetic b(Lmaps/bn/j;)Lmaps/bv/a;
    .locals 1

    iget-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    return-object v0
.end method

.method private declared-synchronized b()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lmaps/bn/d;->v()Lmaps/bn/d;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bn/d;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lmaps/bn/j;)Lmaps/bn/h;
    .locals 1

    invoke-direct {p0}, Lmaps/bn/j;->a()Lmaps/bn/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lmaps/bn/j;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/bn/j;->b:Z

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    const/16 v1, 0x19

    invoke-virtual {v0, v1, p1}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    return-void
.end method

.method public final a(I[BZZZ)V
    .locals 6

    new-instance v0, Lmaps/bn/m;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lmaps/bn/m;-><init>(I[BZZZ)V

    invoke-virtual {p0, v0}, Lmaps/bn/j;->c(Lmaps/bn/c;)V

    return-void
.end method

.method public final a(Lmaps/bn/l;)V
    .locals 1

    invoke-static {}, Lmaps/bn/d;->v()Lmaps/bn/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/bn/d;->a(Lmaps/bn/l;)V

    return-void
.end method

.method public final a([BZ)V
    .locals 6

    const/4 v4, 0x0

    const/16 v1, 0x8

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lmaps/bn/j;->a(I[BZZZ)V

    return-void
.end method

.method public final c(Lmaps/bn/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/bn/j;->d:Lmaps/bn/d;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lmaps/bn/c;->af_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/bn/j;->b:Z

    :cond_0
    iget-object v0, p0, Lmaps/bn/j;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/bn/c;->af_()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/bn/j;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/bn/j;->d:Lmaps/bn/d;

    iget-object v0, v0, Lmaps/bn/d;->d:Lmaps/bn/g;

    invoke-static {v0}, Lmaps/bn/g;->a(Lmaps/bn/g;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final p()V
    .locals 1

    invoke-static {}, Lmaps/bn/d;->v()Lmaps/bn/d;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bn/d;->p()V

    return-void
.end method

.method public final r()J
    .locals 2

    invoke-static {}, Lmaps/bn/d;->v()Lmaps/bn/d;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bn/d;->r()J

    move-result-wide v0

    return-wide v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lmaps/bn/j;->c:Lmaps/bv/a;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
