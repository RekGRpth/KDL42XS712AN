.class public final Lmaps/bn/a;
.super Lmaps/bn/c;


# instance fields
.field private final a:Lmaps/bv/a;


# direct methods
.method public constructor <init>(Lmaps/bv/a;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Lmaps/bn/c;-><init>()V

    iput-object p1, p0, Lmaps/bn/a;->a:Lmaps/bv/a;

    invoke-virtual {p1, v2}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->e()Lmaps/bs/d;

    move-result-object v0

    const-string v1, "Cohort"

    invoke-virtual {v0, v1}, Lmaps/bs/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2, v0}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, Lmaps/bn/a;->a:Lmaps/bv/a;

    invoke-static {p1, v0}, Lmaps/bv/e;->a(Ljava/io/DataOutput;Lmaps/bv/a;)V

    return-void
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    sget-object v0, Lmaps/cm/g;->b:Lmaps/bv/c;

    invoke-static {v0, p1}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/DataInput;)Lmaps/bv/a;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/bv/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v4}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/bn/a;->a:Lmaps/bv/a;

    invoke-virtual {v2, v4, v1}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/bf/a;->e()Lmaps/bs/d;

    move-result-object v2

    const-string v3, "Cohort"

    invoke-virtual {v2, v3, v1}, Lmaps/bs/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v5}, Lmaps/bv/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lmaps/bn/d;->a()Lmaps/bn/d;

    move-result-object v1

    invoke-virtual {v0, v5}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/bn/d;->a(Lmaps/bv/a;)V

    :cond_1
    return v4
.end method

.method public final af_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final ag_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final ai_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x3e

    return v0
.end method
