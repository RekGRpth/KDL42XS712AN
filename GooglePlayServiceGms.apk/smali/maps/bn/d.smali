.class public Lmaps/bn/d;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bn/k;


# static fields
.field private static G:I

.field private static volatile H:Lmaps/bn/d;


# instance fields
.field private volatile A:I

.field private volatile B:I

.field private C:Lmaps/ba/g;

.field private final D:Lmaps/bs/b;

.field private volatile E:I

.field private F:I

.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Z

.field protected final d:Lmaps/bn/g;

.field protected e:Lmaps/bn/j;

.field protected f:I

.field protected g:I

.field protected h:Lmaps/bt/g;

.field protected i:Lmaps/bn/b;

.field private volatile j:Ljava/lang/String;

.field private volatile k:Z

.field private final l:Ljava/util/List;

.field private m:Lmaps/bn/k;

.field private final n:Ljava/lang/String;

.field private o:Ljava/lang/Long;

.field private final p:Ljava/util/List;

.field private final q:Ljava/util/Random;

.field private r:J

.field private volatile s:Z

.field private volatile t:I

.field private volatile u:J

.field private volatile v:J

.field private volatile w:Z

.field private x:J

.field private y:J

.field private volatile z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lmaps/bn/d;->G:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    const/4 v5, -0x1

    const-wide/high16 v3, -0x8000000000000000L

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/bn/d;->p:Ljava/util/List;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lmaps/bn/d;->q:Ljava/util/Random;

    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lmaps/bn/d;->r:J

    iput-boolean v2, p0, Lmaps/bn/d;->s:Z

    iput-wide v3, p0, Lmaps/bn/d;->u:J

    iput-wide v3, p0, Lmaps/bn/d;->v:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/bn/d;->x:J

    iput-wide v3, p0, Lmaps/bn/d;->y:J

    iput v2, p0, Lmaps/bn/d;->z:I

    iput v2, p0, Lmaps/bn/d;->A:I

    iput v2, p0, Lmaps/bn/d;->B:I

    iput v5, p0, Lmaps/bn/d;->E:I

    iput v5, p0, Lmaps/bn/d;->F:I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    invoke-static {p1}, Lmaps/bn/d;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/d;->j:Ljava/lang/String;

    iput-object p3, p0, Lmaps/bn/d;->b:Ljava/lang/String;

    iput-object p2, p0, Lmaps/bn/d;->a:Ljava/lang/String;

    iput-object p4, p0, Lmaps/bn/d;->n:Ljava/lang/String;

    iput-boolean p5, p0, Lmaps/bn/d;->c:Z

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->r()Lmaps/bt/g;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/d;->h:Lmaps/bt/g;

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->i()Lmaps/bs/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/d;->D:Lmaps/bs/b;

    new-instance v0, Lmaps/bn/b;

    iget-object v1, p0, Lmaps/bn/d;->D:Lmaps/bs/b;

    invoke-direct {v0, p0, v1}, Lmaps/bn/b;-><init>(Lmaps/bn/d;Lmaps/bs/b;)V

    iput-object v0, p0, Lmaps/bn/d;->i:Lmaps/bn/b;

    iput v2, p0, Lmaps/bn/d;->f:I

    iput v2, p0, Lmaps/bn/d;->g:I

    new-instance v0, Lmaps/bn/g;

    iget-object v1, p0, Lmaps/bn/d;->j:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lmaps/bn/g;-><init>(Lmaps/bn/d;Ljava/lang/String;B)V

    iput-object v0, p0, Lmaps/bn/d;->d:Lmaps/bn/g;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/bn/d;->l:Ljava/util/List;

    new-instance v0, Lmaps/bn/j;

    invoke-direct {v0, p0, v2}, Lmaps/bn/j;-><init>(Lmaps/bn/d;B)V

    iput-object v0, p0, Lmaps/bn/d;->e:Lmaps/bn/j;

    iget-object v0, p0, Lmaps/bn/d;->l:Ljava/util/List;

    iget-object v1, p0, Lmaps/bn/d;->e:Lmaps/bn/j;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private A()Lmaps/ba/g;
    .locals 1

    iget-object v0, p0, Lmaps/bn/d;->C:Lmaps/ba/g;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ba/g;

    invoke-direct {v0}, Lmaps/ba/g;-><init>()V

    iput-object v0, p0, Lmaps/bn/d;->C:Lmaps/ba/g;

    :cond_0
    iget-object v0, p0, Lmaps/bn/d;->C:Lmaps/ba/g;

    return-object v0
.end method

.method static synthetic a(Lmaps/bn/d;J)J
    .locals 0

    iput-wide p1, p0, Lmaps/bn/d;->u:J

    return-wide p1
.end method

.method static synthetic a(Lmaps/bn/d;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    iput-object p1, p0, Lmaps/bn/d;->o:Ljava/lang/Long;

    return-object p1
.end method

.method public static a()Lmaps/bn/d;
    .locals 1

    sget-object v0, Lmaps/bn/d;->H:Lmaps/bn/d;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lmaps/bn/d;
    .locals 1

    invoke-static {p0, p1, p2, p3, p4}, Lmaps/bn/d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lmaps/bn/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lmaps/bn/d;)Lmaps/bn/j;
    .locals 3

    new-instance v0, Lmaps/bn/j;

    iget-object v1, p0, Lmaps/bn/d;->e:Lmaps/bn/j;

    invoke-static {v1}, Lmaps/bn/j;->b(Lmaps/bn/j;)Lmaps/bv/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lmaps/bn/j;-><init>(Lmaps/bn/d;Lmaps/bv/a;B)V

    iget-object v1, p0, Lmaps/bn/d;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lmaps/bn/d;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/j;

    invoke-static {v0}, Lmaps/bn/j;->b(Lmaps/bn/j;)Lmaps/bv/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(IZ)V
    .locals 2

    iget-object v0, p0, Lmaps/bn/d;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/j;

    invoke-static {v0}, Lmaps/bn/j;->b(Lmaps/bn/j;)Lmaps/bv/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/bv/a;->a(IZ)Lmaps/bv/a;

    goto :goto_0

    :cond_0
    return-void
.end method

.method static a(J)V
    .locals 3

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {v1, p0, p1}, Ljava/io/DataOutput;->writeLong(J)V

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v1

    const-string v2, "SessionID"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lmaps/bt/k;->a(Ljava/lang/String;[B)Z

    invoke-interface {v1}, Lmaps/bt/k;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic a(Lmaps/bn/d;I)V
    .locals 4

    const/16 v0, 0xc8

    if-le p1, v0, :cond_0

    const/4 v0, 0x3

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/bn/d;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/j;

    invoke-static {v0}, Lmaps/bn/j;->b(Lmaps/bn/j;)Lmaps/bv/a;

    move-result-object v0

    const/16 v3, 0x16

    invoke-virtual {v0, v3, v1}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    goto :goto_1

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic a(Lmaps/bn/d;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x1b

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lmaps/bn/d;Z)V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(IZ)V

    return-void
.end method

.method static synthetic a(Lmaps/bn/d;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/bn/d;->a(ZZ)V

    return-void
.end method

.method private declared-synchronized a(ZZ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/bn/d;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bn/d;->z:I

    if-eqz p1, :cond_0

    iget v0, p0, Lmaps/bn/d;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bn/d;->A:I

    :cond_0
    if-eqz p2, :cond_1

    iget v0, p0, Lmaps/bn/d;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bn/d;->B:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static a(Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/c;

    invoke-virtual {v0}, Lmaps/bn/c;->ai_()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/bn/d;J)J
    .locals 0

    iput-wide p1, p0, Lmaps/bn/d;->v:J

    return-wide p1
.end method

.method private static declared-synchronized b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lmaps/bn/d;
    .locals 7

    const-class v6, Lmaps/bn/d;

    monitor-enter v6

    :try_start_0
    sget-object v0, Lmaps/bn/d;->H:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to create multiple DataRequestDispatchers"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    :try_start_1
    invoke-static {p0}, Lmaps/bn/d;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lmaps/bn/d;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lmaps/bn/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Lmaps/bn/d;->H:Lmaps/bn/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v6

    return-object v0
.end method

.method public static b()Lmaps/bn/k;
    .locals 1

    sget-object v0, Lmaps/bn/d;->H:Lmaps/bn/d;

    iget-object v0, v0, Lmaps/bn/d;->m:Lmaps/bn/k;

    return-object v0
.end method

.method static synthetic b(Lmaps/bn/d;)Lmaps/bs/b;
    .locals 1

    iget-object v0, p0, Lmaps/bn/d;->D:Lmaps/bs/b;

    return-object v0
.end method

.method static synthetic b(Lmaps/bn/d;I)V
    .locals 5

    const-wide/16 v3, 0x7d0

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/bn/d;->h:Lmaps/bt/g;

    iput p1, p0, Lmaps/bn/d;->F:I

    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    iget-wide v1, p0, Lmaps/bn/d;->x:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lmaps/bn/d;->w:Z

    if-eqz v1, :cond_3

    :cond_0
    invoke-direct {p0}, Lmaps/bn/d;->z()V

    iput p1, p0, Lmaps/bn/d;->F:I

    const-wide/16 v1, 0xc8

    iput-wide v1, p0, Lmaps/bn/d;->x:J

    :cond_1
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lmaps/bn/d;->a(I)V

    :cond_2
    return-void

    :cond_3
    :try_start_1
    iget-wide v1, p0, Lmaps/bn/d;->x:J

    iget-wide v3, p0, Lmaps/bn/d;->r:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    iget-wide v1, p0, Lmaps/bn/d;->x:J

    const-wide/16 v3, 0x2

    mul-long/2addr v1, v3

    iput-wide v1, p0, Lmaps/bn/d;->x:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_2
    iget-boolean v1, p0, Lmaps/bn/d;->w:Z

    if-nez v1, :cond_6

    const-wide/16 v1, 0xc8

    iput-wide v1, p0, Lmaps/bn/d;->x:J

    iget-wide v1, p0, Lmaps/bn/d;->y:J

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    iget-object v1, p0, Lmaps/bn/d;->D:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v1

    iput-wide v1, p0, Lmaps/bn/d;->y:J

    goto :goto_0

    :cond_5
    iget-wide v1, p0, Lmaps/bn/d;->y:J

    const-wide/16 v3, 0x3a98

    add-long/2addr v1, v3

    iget-object v3, p0, Lmaps/bn/d;->D:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    iget-wide v1, p0, Lmaps/bn/d;->x:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_7

    const-wide/16 v1, 0x7d0

    iput-wide v1, p0, Lmaps/bn/d;->x:J

    :goto_1
    iget-wide v1, p0, Lmaps/bn/d;->x:J

    iget-wide v3, p0, Lmaps/bn/d;->r:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iget-wide v1, p0, Lmaps/bn/d;->r:J

    iput-wide v1, p0, Lmaps/bn/d;->x:J

    goto :goto_0

    :cond_7
    iget-wide v1, p0, Lmaps/bn/d;->x:J

    const-wide/16 v3, 0x5

    mul-long/2addr v1, v3

    const-wide/16 v3, 0x4

    div-long/2addr v1, v3

    iput-wide v1, p0, Lmaps/bn/d;->x:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method static synthetic b(Lmaps/bn/d;Z)V
    .locals 1

    const/16 v0, 0x1d

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(IZ)V

    return-void
.end method

.method static synthetic b(Lmaps/bn/d;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/bn/d;->b(ZZ)V

    return-void
.end method

.method private declared-synchronized b(ZZ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/bn/d;->z:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/bn/d;->z:I

    if-eqz p1, :cond_0

    iget v0, p0, Lmaps/bn/d;->A:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/bn/d;->A:I

    :cond_0
    if-eqz p2, :cond_1

    iget v0, p0, Lmaps/bn/d;->B:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/bn/d;->B:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static b(Ljava/util/Vector;)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method static synthetic c(Lmaps/bn/d;I)I
    .locals 0

    iput p1, p0, Lmaps/bn/d;->E:I

    return p1
.end method

.method static synthetic c(Lmaps/bn/d;Z)V
    .locals 1

    const/16 v0, 0x20

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(IZ)V

    return-void
.end method

.method protected static c(Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/c;

    invoke-virtual {v0}, Lmaps/bn/c;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic c(Lmaps/bn/d;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/bn/d;->s:Z

    return v0
.end method

.method static synthetic d(Lmaps/bn/d;)V
    .locals 0

    invoke-direct {p0}, Lmaps/bn/d;->z()V

    return-void
.end method

.method static synthetic d(Lmaps/bn/d;Z)V
    .locals 1

    const/16 v0, 0x21

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(IZ)V

    return-void
.end method

.method static synthetic e(Lmaps/bn/d;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/bn/d;->k:Z

    return v0
.end method

.method private static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method static synthetic f(Lmaps/bn/d;)Lmaps/ba/g;
    .locals 1

    invoke-direct {p0}, Lmaps/bn/d;->A()Lmaps/ba/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lmaps/bn/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/bn/d;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lmaps/bn/d;)Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lmaps/bn/d;->o:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic i(Lmaps/bn/d;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/bn/d;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Lmaps/bn/d;)I
    .locals 1

    iget v0, p0, Lmaps/bn/d;->F:I

    return v0
.end method

.method static synthetic k(Lmaps/bn/d;)J
    .locals 2

    iget-wide v0, p0, Lmaps/bn/d;->x:J

    return-wide v0
.end method

.method static synthetic l(Lmaps/bn/d;)Ljava/util/Random;
    .locals 1

    iget-object v0, p0, Lmaps/bn/d;->q:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic v()Lmaps/bn/d;
    .locals 1

    sget-object v0, Lmaps/bn/d;->H:Lmaps/bn/d;

    return-object v0
.end method

.method static synthetic w()I
    .locals 2

    sget v0, Lmaps/bn/d;->G:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lmaps/bn/d;->G:I

    return v0
.end method

.method private static x()J
    .locals 3

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->e()Lmaps/bs/d;

    move-result-object v0

    const-string v1, "SessionID"

    invoke-virtual {v0, v1}, Lmaps/bs/d;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Ljava/io/DataInput;->readLong()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v0

    const-string v1, "SessionID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/bt/k;->a(Ljava/lang/String;[B)Z

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized y()[Lmaps/bn/l;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bn/d;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lmaps/bn/l;

    iget-object v1, p0, Lmaps/bn/d;->p:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized z()V
    .locals 2

    monitor-enter p0

    const-wide/high16 v0, -0x8000000000000000L

    :try_start_0
    iput-wide v0, p0, Lmaps/bn/d;->y:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bn/d;->w:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/bn/d;->x:J

    const/4 v0, -0x1

    iput v0, p0, Lmaps/bn/d;->F:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/bn/d;->w:Z

    if-nez v2, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmaps/bn/d;->w:Z

    const-wide/high16 v1, -0x8000000000000000L

    iput-wide v1, p0, Lmaps/bn/d;->y:J

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/bn/d;->h:Lmaps/bt/g;

    invoke-interface {v1}, Lmaps/bt/g;->b()Z

    move-result v1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lmaps/bn/d;->a(IZLjava/lang/String;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected final a(IZLjava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Lmaps/bn/d;->y()[Lmaps/bn/l;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2, p3}, Lmaps/bn/l;->a(IZLjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(I[BZZZ)V
    .locals 6

    iget-object v0, p0, Lmaps/bn/d;->e:Lmaps/bn/j;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lmaps/bn/j;->a(I[BZZZ)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x12

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(ILjava/lang/String;)V

    return-void
.end method

.method protected final a(Lmaps/bn/c;)V
    .locals 4

    invoke-direct {p0}, Lmaps/bn/d;->y()[Lmaps/bn/l;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lmaps/bn/l;->b(Lmaps/bn/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lmaps/bn/k;)V
    .locals 0

    iput-object p1, p0, Lmaps/bn/d;->m:Lmaps/bn/k;

    return-void
.end method

.method public final declared-synchronized a(Lmaps/bn/l;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bn/d;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/d;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/bv/a;)V
    .locals 1

    invoke-direct {p0}, Lmaps/bn/d;->A()Lmaps/ba/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/ba/g;->a(Lmaps/bv/a;)V

    return-void
.end method

.method public final a([BZ)V
    .locals 6

    const/4 v4, 0x0

    const/16 v1, 0x8

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lmaps/bn/d;->a(I[BZZZ)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x28

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(ILjava/lang/String;)V

    return-void
.end method

.method protected final b(Lmaps/bn/c;)V
    .locals 4

    invoke-direct {p0}, Lmaps/bn/d;->y()[Lmaps/bn/l;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lmaps/bn/l;->a(Lmaps/bn/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final declared-synchronized b(Lmaps/bn/l;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bn/d;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/bn/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x27

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final c(Lmaps/bn/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/bn/d;->e:Lmaps/bn/j;

    invoke-virtual {v0, p1}, Lmaps/bn/j;->c(Lmaps/bn/c;)V

    return-void
.end method

.method public final declared-synchronized d()V
    .locals 4

    const-wide/16 v2, 0x7530

    const-wide/16 v0, 0x7d0

    monitor-enter p0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const-wide/16 v0, 0x7d0

    :try_start_0
    iput-wide v0, p0, Lmaps/bn/d;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const-wide/16 v0, 0x7530

    :try_start_1
    iput-wide v0, p0, Lmaps/bn/d;->r:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x13

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x26

    invoke-direct {p0, v0, p1}, Lmaps/bn/d;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final declared-synchronized e()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/bn/d;->t:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/bn/d;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bn/d;->t:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/bn/d;->t:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/bn/d;->t:I

    invoke-virtual {p0}, Lmaps/bn/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0

    iget-object v0, p0, Lmaps/bn/d;->d:Lmaps/bn/g;

    invoke-static {v0}, Lmaps/bn/g;->c(Lmaps/bn/g;)V

    iget-object v0, p0, Lmaps/bn/d;->i:Lmaps/bn/b;

    invoke-virtual {v0}, Lmaps/bn/b;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final h()V
    .locals 4

    invoke-direct {p0}, Lmaps/bn/d;->y()[Lmaps/bn/l;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3}, Lmaps/bn/l;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected final i()V
    .locals 4

    invoke-direct {p0}, Lmaps/bn/d;->y()[Lmaps/bn/l;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3}, Lmaps/bn/l;->b()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final declared-synchronized j()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/bn/d;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/bn/d;->s:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/bn/d;->z:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lmaps/bn/d;->h:Lmaps/bt/g;

    invoke-interface {v0}, Lmaps/bt/g;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/bn/d;->z:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final l()J
    .locals 2

    iget-wide v0, p0, Lmaps/bn/d;->u:J

    return-wide v0
.end method

.method public final m()I
    .locals 1

    iget v0, p0, Lmaps/bn/d;->f:I

    return v0
.end method

.method public final n()I
    .locals 1

    iget v0, p0, Lmaps/bn/d;->g:I

    return v0
.end method

.method public final o()I
    .locals 1

    iget v0, p0, Lmaps/bn/d;->E:I

    return v0
.end method

.method public final p()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bn/d;->s:Z

    return-void
.end method

.method public final q()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/bn/d;->s:Z

    iget-object v0, p0, Lmaps/bn/d;->d:Lmaps/bn/g;

    invoke-static {v0}, Lmaps/bn/g;->e(Lmaps/bn/g;)V

    return-void
.end method

.method public final declared-synchronized r()J
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bn/d;->o:Ljava/lang/Long;

    if-nez v0, :cond_1

    invoke-static {}, Lmaps/bn/d;->x()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/bn/d;->e:Lmaps/bn/j;

    new-instance v3, Lmaps/bn/f;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lmaps/bn/f;-><init>(Lmaps/bn/d;B)V

    invoke-virtual {v2, v3}, Lmaps/bn/j;->c(Lmaps/bn/c;)V

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/d;->o:Ljava/lang/Long;

    :cond_1
    iget-object v0, p0, Lmaps/bn/d;->o:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final s()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/bn/d;->k:Z

    return-void
.end method

.method public final t()V
    .locals 2

    const/16 v0, 0x2d

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmaps/bn/d;->a(IZ)V

    return-void
.end method

.method public final u()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lmaps/bn/d;->e:Lmaps/bn/j;

    invoke-static {v0}, Lmaps/bn/j;->b(Lmaps/bn/j;)Lmaps/bv/a;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
