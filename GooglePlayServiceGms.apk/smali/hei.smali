.class public final Lhei;
.super Lhfb;
.source "SourceFile"


# static fields
.field private static final a:Lbqa;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lhfb;

.field private final d:Lhcg;

.field private final e:Lgsi;

.field private final f:Lgsg;

.field private final g:Lgse;

.field private final h:Lhep;

.field private final i:Lhef;

.field private final j:Lhga;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhej;

    invoke-direct {v0}, Lhej;-><init>()V

    sput-object v0, Lhei;->a:Lbqa;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lhfb;Lhcg;Lgsi;Lgsg;Lgse;Lhef;Lhga;Lhep;)V
    .locals 0

    invoke-direct {p0}, Lhfb;-><init>()V

    iput-object p1, p0, Lhei;->b:Landroid/content/Context;

    iput-object p2, p0, Lhei;->c:Lhfb;

    iput-object p3, p0, Lhei;->d:Lhcg;

    iput-object p4, p0, Lhei;->e:Lgsi;

    iput-object p5, p0, Lhei;->f:Lgsg;

    iput-object p6, p0, Lhei;->g:Lgse;

    iput-object p9, p0, Lhei;->h:Lhep;

    iput-object p7, p0, Lhei;->i:Lhef;

    iput-object p8, p0, Lhei;->j:Lhga;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 9

    iget-object v0, p0, Lhei;->c:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    instance-of v1, v0, Ljav;

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Ljau;

    move-result-object v8

    check-cast v0, Ljav;

    iget-object v1, v0, Ljav;->j:Ljbb;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v2

    iget-object v0, v0, Ljav;->j:Ljbb;

    invoke-static {v0}, Lhfx;->a(Ljbb;)Lizz;

    move-result-object v3

    iget-object v0, p0, Lhei;->e:Lgsi;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    iget-object v4, v8, Ljau;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    iget-boolean v5, v8, Ljau;->i:Z

    iget-boolean v6, v8, Ljau;->h:Z

    invoke-static/range {v0 .. v6}, Lhfx;->a(Lgsi;Landroid/accounts/Account;ILizz;ZZZ)V

    iget-object v0, p0, Lhei;->g:Lgse;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2, v3}, Lhfx;->a(Lgse;Ljava/lang/String;ILizz;)V

    iget-boolean v0, v8, Ljau;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhei;->f:Lgsg;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4, v2, v3}, Lhfx;->a(Lgsg;Landroid/accounts/Account;Ljava/lang/String;ILizz;)V

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8}, Lhfx;->a(Ljau;)Ljbg;

    move-result-object v1

    iget-object v2, p0, Lhei;->d:Lhcg;

    invoke-virtual {v2, v0, v1, v3}, Lhcg;->b(Ljava/lang/String;Lizs;Lizs;)V

    :cond_1
    return-object v7

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c()Ljao;

    move-result-object v0

    iget-object v1, p0, Lhei;->c:Lhfb;

    invoke-virtual {v1, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v2

    iget-object v3, p0, Lhei;->d:Lhcg;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v2}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    return-object v1
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    iget-object v0, p0, Lhei;->c:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    iget-object v0, p0, Lhei;->c:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 20

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->b()Ljaw;

    move-result-object v15

    iget-object v0, v15, Ljaw;->a:Ljbg;

    move-object/from16 v16, v0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    iget-boolean v3, v15, Ljaw;->i:Z

    iget-object v2, v15, Ljaw;->h:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_9

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lhei;->d:Lhcg;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v4, v0, v1}, Lhcg;->a(Ljava/lang/String;Lizs;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v4

    if-nez v3, :cond_0

    if-nez v2, :cond_0

    iget-object v2, v15, Ljaw;->g:Lipv;

    if-nez v2, :cond_0

    iget-object v2, v15, Ljaw;->e:Linv;

    if-nez v2, :cond_0

    if-nez v4, :cond_a

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->c:Lhfb;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lhei;->d:Lhcg;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0, v15, v2}, Lhcg;->a(Ljava/lang/String;Lizs;Lizs;)V

    move-object v14, v3

    :goto_1
    instance-of v3, v2, Ljax;

    if-eqz v3, :cond_b

    move-object v13, v2

    check-cast v13, Ljax;

    iget-object v0, v13, Ljax;->b:Ljbh;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v19

    if-eqz v18, :cond_6

    move-object/from16 v0, v18

    iget-object v2, v0, Ljbh;->e:Ljbi;

    if-eqz v2, :cond_6

    iget-object v2, v13, Ljax;->a:[I

    array-length v2, v2

    if-nez v2, :cond_6

    move-object/from16 v0, v18

    iget-object v12, v0, Ljbh;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->j:Lhga;

    invoke-virtual {v2, v12}, Lhga;->a(Ljava/lang/String;)Lhgb;

    move-result-object v2

    if-eqz v2, :cond_26

    iget-boolean v2, v2, Lhgb;->i:Z

    move v10, v2

    :goto_2
    new-instance v2, Lhgb;

    move-object/from16 v0, v18

    iget-object v3, v0, Ljbh;->e:Ljbi;

    iget-object v3, v3, Ljbi;->j:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v4, v0, Ljbh;->d:Lipv;

    if-eqz v4, :cond_27

    move-object/from16 v0, v18

    iget-object v4, v0, Ljbh;->d:Lipv;

    iget-object v4, v4, Lipv;->b:Ljava/lang/String;

    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->b()Ljaw;

    move-result-object v6

    iget-object v6, v6, Ljaw;->a:Ljbg;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d()Z

    move-result v7

    iget-boolean v8, v15, Ljaw;->i:Z

    iget-object v9, v15, Ljaw;->h:[Ljava/lang/String;

    if-nez v10, :cond_1

    iget-object v10, v15, Ljaw;->e:Linv;

    if-eqz v10, :cond_28

    :cond_1
    const/4 v10, 0x1

    :goto_4
    const/4 v11, 0x0

    invoke-direct/range {v2 .. v11}, Lhgb;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljbg;ZZ[Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhei;->j:Lhga;

    invoke-virtual {v3, v12, v2}, Lhga;->a(Ljava/lang/String;Lhgb;)V

    iget-object v3, v15, Ljaw;->e:Linv;

    if-eqz v3, :cond_3

    move-object/from16 v0, v18

    iget-object v3, v0, Ljbh;->e:Ljbi;

    invoke-static {v3}, Lgtk;->a(Ljbi;)Lioj;

    move-result-object v3

    const/4 v4, 0x1

    iput-boolean v4, v3, Lioj;->f:Z

    new-instance v4, Lizx;

    invoke-direct {v4}, Lizx;-><init>()V

    const/4 v5, 0x1

    new-array v5, v5, [Lioj;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    iput-object v5, v4, Lizx;->a:[Lioj;

    iget-object v3, v15, Ljaw;->g:Lipv;

    if-eqz v3, :cond_2

    iget-object v3, v4, Lizx;->b:[Lipv;

    move-object/from16 v0, v18

    iget-object v5, v0, Ljbh;->d:Lipv;

    invoke-static {v3, v5}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lipv;

    iput-object v3, v4, Lizx;->b:[Lipv;

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lhei;->e:Lgsi;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v6

    invoke-virtual {v3, v5, v6, v4}, Lgsi;->a(Landroid/accounts/Account;ILizx;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lhei;->i:Lhef;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lhef;->a(Ljava/lang/String;)Ljan;

    move-result-object v3

    if-eqz v3, :cond_4

    move-object/from16 v0, v16

    iput-object v3, v0, Ljbg;->a:Ljan;

    move-object/from16 v0, v18

    iget-object v3, v0, Ljbh;->a:Ljava/lang/String;

    move-object/from16 v0, v16

    iput-object v3, v0, Ljbg;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhei;->b:Landroid/content/Context;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->c()Lcom/google/android/gms/wallet/Cart;

    move-result-object v7

    iget-object v8, v2, Lhgb;->b:Ljava/lang/String;

    iget-object v9, v2, Lhgb;->c:Ljava/lang/String;

    iget-boolean v10, v2, Lhgb;->i:Z

    iget-boolean v11, v2, Lhgb;->j:Z

    move-object/from16 v4, p0

    move-object/from16 v6, v16

    move-object/from16 v12, p1

    invoke-static/range {v3 .. v12}, Lheo;->a(Landroid/content/Context;Lhfb;Landroid/accounts/Account;Ljbg;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lheo;

    move-result-object v3

    new-instance v4, Lheq;

    iget-object v5, v2, Lhgb;->b:Ljava/lang/String;

    iget-object v2, v2, Lhgb;->c:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v4, v0, v5, v2}, Lheq;-><init>(Ljbg;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->h:Lhep;

    invoke-virtual {v2, v4, v3}, Lhep;->a(Lheq;Lheo;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v3, v2}, Lbox;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    :cond_4
    iget-boolean v2, v15, Ljaw;->i:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->f:Lgsg;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lgsh;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lhei;->b:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v5, 0x40

    :try_start_0
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v4

    if-lez v4, :cond_5

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const-string v4, "SHA-256"

    invoke-static {v3, v4}, Lhbq;->a(Landroid/content/pm/Signature;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lgsh;->a:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lhei;->f:Lgsg;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v19

    invoke-virtual {v3, v0, v4, v5, v2}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;Lgsh;)V

    :cond_6
    iget-object v2, v13, Ljax;->d:Ljbb;

    if-eqz v2, :cond_8

    iget-object v2, v13, Ljax;->d:Ljbb;

    invoke-static {v2}, Lhfx;->a(Ljbb;)Lizz;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->e:Lgsi;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object/from16 v0, v16

    iget-boolean v6, v0, Ljbg;->g:Z

    move-object/from16 v0, v16

    iget-boolean v7, v0, Ljbg;->h:Z

    move-object/from16 v0, v16

    iget-boolean v8, v0, Ljbg;->i:Z

    move/from16 v4, v19

    invoke-static/range {v2 .. v8}, Lhfx;->a(Lgsi;Landroid/accounts/Account;ILizz;ZZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->g:Lgse;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v3

    move/from16 v0, v19

    invoke-static {v2, v3, v0, v5}, Lhfx;->a(Lgse;Ljava/lang/String;ILizz;)V

    move-object/from16 v0, v16

    iget-boolean v2, v0, Ljbg;->k:Z

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->f:Lgsg;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v19

    invoke-static {v2, v3, v4, v0, v5}, Lhfx;->a(Lgsg;Landroid/accounts/Account;Ljava/lang/String;ILizz;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->d:Lhcg;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v2, v0, v1, v5}, Lhcg;->b(Ljava/lang/String;Lizs;Lizs;)V

    :cond_8
    invoke-static {}, Ljaj;->c()[Ljaj;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Ljbh;->g:[Ljaj;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2b

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->d()Ljava/lang/String;

    move-result-object v3

    const-string v4, "dont_send_loyalty_wob_id"

    invoke-static {v3, v4}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_29

    move-object/from16 v0, v18

    iget-object v3, v0, Ljbh;->f:[Ljai;

    array-length v5, v3

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v5, :cond_29

    move-object/from16 v0, v18

    iget-object v2, v0, Ljbh;->f:[Ljai;

    aget-object v2, v2, v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v6, v2, Ljai;->a:Ljava/lang/String;

    invoke-static {v4, v6}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_29

    const/4 v4, 0x0

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v4

    goto :goto_6

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v4}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v2

    check-cast v2, Lizz;

    iget-object v3, v2, Lizz;->b:[Lioj;

    iget-object v4, v15, Ljaw;->c:Ljava/lang/String;

    invoke-static {v3, v4}, Lgth;->a([Lioj;Ljava/lang/String;)Lioj;

    move-result-object v6

    if-nez v6, :cond_c

    const-string v2, "CachedOwInternalService"

    const-string v3, "Could not find selected instrument in cached wallet items."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v14, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    :cond_b
    :goto_7
    return-object v14

    :cond_c
    const/4 v3, 0x0

    iget-object v4, v15, Ljaw;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2c

    iget-object v3, v2, Lizz;->c:[Lipv;

    iget-object v4, v15, Ljaw;->d:Ljava/lang/String;

    invoke-static {v3, v4}, Lgth;->a([Lipv;Ljava/lang/String;)Lipv;

    move-result-object v3

    if-nez v3, :cond_d

    const-string v2, "CachedOwInternalService"

    const-string v3, "Could not find selected address in cached wallet items."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v14, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_7

    :cond_d
    move-object v5, v3

    :goto_8
    if-nez v6, :cond_11

    const/4 v3, 0x0

    :goto_9
    new-instance v4, Ljbh;

    invoke-direct {v4}, Ljbh;-><init>()V

    iget-object v7, v15, Ljaw;->a:Ljbg;

    iget-object v7, v7, Ljbg;->b:Ljava/lang/String;

    iput-object v7, v4, Ljbh;->a:Ljava/lang/String;

    iput-object v3, v4, Ljbh;->e:Ljbi;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v6}, Lgth;->a(Lioj;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    iput-object v3, v4, Ljbh;->b:[Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljah;

    const/4 v7, 0x0

    invoke-static {v6}, Lgth;->c(Lioj;)Ljah;

    move-result-object v6

    aput-object v6, v3, v7

    iput-object v3, v4, Ljbh;->c:[Ljah;

    iget-object v2, v2, Lizz;->a:Ljbb;

    iget-object v3, v2, Ljbb;->l:[Ljai;

    array-length v3, v3

    if-lez v3, :cond_e

    iget-object v3, v2, Ljbb;->l:[Ljai;

    iput-object v3, v4, Ljbh;->f:[Ljai;

    :cond_e
    iget-object v3, v2, Ljbb;->m:[Ljaj;

    array-length v3, v3

    if-lez v3, :cond_f

    iget-object v2, v2, Ljbb;->m:[Ljaj;

    iput-object v2, v4, Ljbh;->g:[Ljaj;

    :cond_f
    if-eqz v5, :cond_10

    iput-object v5, v4, Ljbh;->d:Lipv;

    :cond_10
    new-instance v2, Ljax;

    invoke-direct {v2}, Ljax;-><init>()V

    iput-object v4, v2, Ljax;->b:Ljbh;

    new-instance v3, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v4, 0xe

    invoke-direct {v3, v4, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    move-object v14, v3

    goto/16 :goto_1

    :cond_11
    new-instance v4, Ljbi;

    invoke-direct {v4}, Ljbi;-><init>()V

    iget-object v3, v6, Lioj;->e:Lipv;

    if-eqz v3, :cond_13

    iget-object v7, v6, Lioj;->e:Lipv;

    if-nez v7, :cond_18

    const/4 v3, 0x0

    :cond_12
    :goto_a
    iput-object v3, v4, Ljbi;->h:Ljab;

    :cond_13
    iget-object v3, v6, Lioj;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, v6, Lioj;->b:Ljava/lang/String;

    iput-object v3, v4, Ljbi;->a:Ljava/lang/String;

    :cond_14
    iget-object v3, v6, Lioj;->m:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_15

    iget-object v3, v6, Lioj;->m:Ljava/lang/String;

    iput-object v3, v4, Ljbi;->d:Ljava/lang/String;

    :cond_15
    iget-object v3, v6, Lioj;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_16

    iget-object v3, v6, Lioj;->a:Ljava/lang/String;

    iput-object v3, v4, Ljbi;->j:Ljava/lang/String;

    :cond_16
    iget-object v3, v6, Lioj;->i:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_17

    iget-object v3, v6, Lioj;->i:Ljava/lang/String;

    iput-object v3, v4, Ljbi;->k:Ljava/lang/String;

    :cond_17
    iget v3, v6, Lioj;->h:I

    packed-switch v3, :pswitch_data_0

    :goto_b
    iget-object v7, v6, Lioj;->g:[I

    array-length v8, v7

    const/4 v3, 0x0

    :goto_c
    if-ge v3, v8, :cond_25

    aget v9, v7, v3

    packed-switch v9, :pswitch_data_1

    :pswitch_0
    const/4 v9, 0x7

    iput v9, v4, Ljbi;->i:I

    :goto_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_18
    new-instance v3, Ljab;

    invoke-direct {v3}, Ljab;-><init>()V

    iget-object v8, v7, Lipv;->c:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_19

    iget-object v8, v7, Lipv;->c:Ljava/lang/String;

    iput-object v8, v3, Ljab;->a:Ljava/lang/String;

    :cond_19
    iget-boolean v8, v7, Lipv;->e:Z

    iput-boolean v8, v3, Ljab;->k:Z

    iget-object v8, v7, Lipv;->a:Lixo;

    if-eqz v8, :cond_23

    iget-object v8, v7, Lipv;->a:Lixo;

    iget-object v9, v8, Lixo;->q:[Ljava/lang/String;

    array-length v9, v9

    if-lez v9, :cond_1a

    iget-object v9, v8, Lixo;->q:[Ljava/lang/String;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    iput-object v9, v3, Ljab;->c:Ljava/lang/String;

    :cond_1a
    iget-object v9, v8, Lixo;->q:[Ljava/lang/String;

    array-length v9, v9

    const/4 v10, 0x2

    if-lt v9, v10, :cond_1b

    iget-object v9, v8, Lixo;->q:[Ljava/lang/String;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    iput-object v9, v3, Ljab;->d:Ljava/lang/String;

    :cond_1b
    iget-object v9, v8, Lixo;->q:[Ljava/lang/String;

    array-length v9, v9

    const/4 v10, 0x3

    if-lt v9, v10, :cond_1c

    iget-object v9, v8, Lixo;->q:[Ljava/lang/String;

    const/4 v10, 0x2

    aget-object v9, v9, v10

    iput-object v9, v3, Ljab;->e:Ljava/lang/String;

    :cond_1c
    iget-object v9, v8, Lixo;->f:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1d

    iget-object v9, v8, Lixo;->f:Ljava/lang/String;

    iput-object v9, v3, Ljab;->g:Ljava/lang/String;

    :cond_1d
    iget-object v9, v8, Lixo;->a:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1e

    iget-object v9, v8, Lixo;->a:Ljava/lang/String;

    iput-object v9, v3, Ljab;->f:Ljava/lang/String;

    :cond_1e
    iget-object v9, v8, Lixo;->s:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1f

    iget-object v9, v8, Lixo;->s:Ljava/lang/String;

    iput-object v9, v3, Ljab;->b:Ljava/lang/String;

    :cond_1f
    iget-object v9, v8, Lixo;->k:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_20

    iget-object v9, v8, Lixo;->k:Ljava/lang/String;

    iput-object v9, v3, Ljab;->i:Ljava/lang/String;

    :cond_20
    iget-object v9, v8, Lixo;->d:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_21

    iget-object v9, v8, Lixo;->d:Ljava/lang/String;

    iput-object v9, v3, Ljab;->h:Ljava/lang/String;

    :cond_21
    iget-object v9, v8, Lixo;->r:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_22

    iget-object v9, v8, Lixo;->r:Ljava/lang/String;

    iput-object v9, v3, Ljab;->l:Ljava/lang/String;

    :cond_22
    iget-object v8, v8, Lixo;->q:[Ljava/lang/String;

    array-length v8, v8

    if-lez v8, :cond_24

    const/4 v8, 0x1

    iput v8, v3, Ljab;->m:I

    :cond_23
    :goto_e
    iget-object v8, v7, Lipv;->d:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_12

    iget-object v7, v7, Lipv;->d:Ljava/lang/String;

    iput-object v7, v3, Ljab;->j:Ljava/lang/String;

    goto/16 :goto_a

    :cond_24
    const/4 v8, 0x2

    iput v8, v3, Ljab;->m:I

    goto :goto_e

    :pswitch_1
    const/4 v3, 0x2

    iput v3, v4, Ljbi;->i:I

    goto/16 :goto_b

    :pswitch_2
    const/4 v3, 0x3

    iput v3, v4, Ljbi;->i:I

    goto/16 :goto_b

    :pswitch_3
    const/4 v3, 0x7

    iput v3, v4, Ljbi;->i:I

    goto/16 :goto_b

    :pswitch_4
    const/4 v9, 0x7

    iput v9, v4, Ljbi;->i:I

    goto/16 :goto_d

    :pswitch_5
    const/4 v9, 0x5

    iput v9, v4, Ljbi;->i:I

    goto/16 :goto_d

    :pswitch_6
    const/4 v9, 0x7

    iput v9, v4, Ljbi;->i:I

    goto/16 :goto_d

    :pswitch_7
    const/4 v9, 0x4

    iput v9, v4, Ljbi;->i:I

    goto/16 :goto_d

    :pswitch_8
    const/4 v9, 0x4

    iput v9, v4, Ljbi;->i:I

    goto/16 :goto_d

    :cond_25
    iget v3, v6, Lioj;->c:I

    packed-switch v3, :pswitch_data_2

    const/4 v3, 0x0

    iput v3, v4, Ljbi;->b:I

    :goto_f
    iget v3, v6, Lioj;->l:I

    packed-switch v3, :pswitch_data_3

    const/4 v3, 0x0

    iput v3, v4, Ljbi;->l:I

    :goto_10
    move-object v3, v4

    goto/16 :goto_9

    :pswitch_9
    const/4 v3, 0x3

    iput v3, v4, Ljbi;->b:I

    goto :goto_f

    :pswitch_a
    const/4 v3, 0x4

    iput v3, v4, Ljbi;->b:I

    goto :goto_f

    :pswitch_b
    const/4 v3, 0x6

    iput v3, v4, Ljbi;->b:I

    goto :goto_f

    :pswitch_c
    const/4 v3, 0x2

    iput v3, v4, Ljbi;->b:I

    goto :goto_f

    :pswitch_d
    const/4 v3, 0x5

    iput v3, v4, Ljbi;->b:I

    goto :goto_f

    :pswitch_e
    const/4 v3, 0x7

    iput v3, v4, Ljbi;->b:I

    goto :goto_f

    :pswitch_f
    const/4 v3, 0x0

    iput v3, v4, Ljbi;->b:I

    goto :goto_f

    :pswitch_10
    const/4 v3, 0x1

    iput v3, v4, Ljbi;->b:I

    goto :goto_f

    :pswitch_11
    const/4 v3, 0x1

    iput v3, v4, Ljbi;->l:I

    goto :goto_10

    :pswitch_12
    const/4 v3, 0x2

    iput v3, v4, Ljbi;->l:I

    goto :goto_10

    :pswitch_13
    const/4 v3, 0x3

    iput v3, v4, Ljbi;->l:I

    goto :goto_10

    :cond_26
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_2

    :cond_27
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_28
    const/4 v10, 0x0

    goto/16 :goto_4

    :cond_29
    invoke-static {}, Ljai;->c()[Ljai;

    move-result-object v3

    move-object/from16 v0, v18

    iput-object v3, v0, Ljbh;->f:[Ljai;

    if-eqz v2, :cond_2a

    move-object/from16 v0, v18

    iget-object v3, v0, Ljbh;->f:[Ljai;

    invoke-static {v3, v2}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljai;

    move-object/from16 v0, v18

    iput-object v2, v0, Ljbh;->f:[Ljai;

    :cond_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->f:Lgsg;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lgsh;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lgsh;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhei;->f:Lgsg;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v19

    invoke-virtual {v3, v0, v4, v5, v2}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;Lgsh;)V

    goto/16 :goto_7

    :cond_2b
    invoke-static {}, Ljai;->c()[Ljai;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Ljbh;->f:[Ljai;

    goto/16 :goto_7

    :catch_0
    move-exception v3

    goto/16 :goto_5

    :cond_2c
    move-object v5, v3

    goto/16 :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_c
        :pswitch_9
        :pswitch_a
        :pswitch_d
        :pswitch_b
        :pswitch_e
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 18

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b()Ljba;

    move-result-object v2

    iget-object v9, v2, Ljba;->a:Ljbg;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lhcg;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->d:Lhcg;

    invoke-virtual {v2, v3, v9}, Lhcg;->a(Ljava/lang/String;Lizs;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->f:Lgsg;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lgsh;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->b:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v2, v5}, Lgsh;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->g:Lgse;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lgse;->a(ILjava/lang/String;)Lgsf;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c()Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->e:Lgsi;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, Lgsi;->a(Landroid/accounts/Account;I)Lizx;

    move-result-object v5

    if-eqz v5, :cond_b

    new-instance v7, Lizz;

    invoke-direct {v7}, Lizz;-><init>()V

    const/4 v2, 0x2

    iput v2, v7, Lizz;->d:I

    iget-object v2, v5, Lizx;->a:[Lioj;

    iget-object v3, v5, Lizx;->a:[Lioj;

    sget-object v4, Lhei;->a:Lbqa;

    invoke-static {v3, v4}, Lboz;->a([Ljava/lang/Object;Lbqa;)I

    move-result v3

    invoke-static {v2, v3}, Lboz;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lioj;

    iput-object v2, v7, Lizz;->b:[Lioj;

    iget-object v2, v5, Lizx;->b:[Lipv;

    array-length v8, v2

    new-array v2, v8, [Lipv;

    iput-object v2, v7, Lizz;->c:[Lipv;

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v8, :cond_4

    iget-object v2, v5, Lizx;->b:[Lipv;

    aget-object v12, v2, v4

    iget-object v13, v9, Ljbg;->n:[Ljbj;

    iget-boolean v2, v12, Lipv;->g:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    iget-object v3, v12, Lipv;->a:Lixo;

    if-eqz v3, :cond_1

    iget-object v3, v12, Lipv;->a:Lixo;

    iget-object v14, v3, Lixo;->a:Ljava/lang/String;

    const/4 v3, 0x0

    array-length v15, v13

    :goto_2
    if-ge v3, v15, :cond_1

    aget-object v16, v13, v3

    move-object/from16 v0, v16

    iget-object v0, v0, Ljbj;->a:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_3

    const/4 v2, 0x1

    :cond_1
    if-nez v2, :cond_2

    const/4 v2, 0x0

    iput-boolean v2, v12, Lipv;->g:Z

    iget-object v2, v12, Lipv;->i:[I

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lboz;->b([II)[I

    move-result-object v2

    iput-object v2, v12, Lipv;->i:[I

    :cond_2
    iget-object v2, v7, Lizz;->c:[Lipv;

    aput-object v12, v2, v4

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    new-instance v5, Ljbb;

    invoke-direct {v5}, Ljbb;-><init>()V

    iget-object v2, v9, Ljbg;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, v9, Ljbg;->d:Ljava/lang/String;

    iput-object v2, v5, Ljbb;->h:Ljava/lang/String;

    :cond_5
    :goto_3
    iget-boolean v2, v10, Lgsf;->b:Z

    iput-boolean v2, v5, Ljbb;->i:Z

    iget-object v2, v11, Lgsh;->c:[Ljai;

    if-eqz v2, :cond_9

    iget-object v2, v11, Lgsh;->c:[Ljai;

    array-length v2, v2

    if-lez v2, :cond_9

    iget-object v2, v11, Lgsh;->b:Ljava/lang/String;

    const-string v3, "dont_send_loyalty_wob_id"

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    iget-object v2, v11, Lgsh;->c:[Ljai;

    array-length v12, v2

    const/4 v2, 0x0

    add-int/lit8 v3, v12, 0x1

    new-array v3, v3, [Ljai;

    iput-object v3, v5, Ljbb;->l:[Ljai;

    if-eqz v8, :cond_6

    iget-object v3, v5, Ljbb;->l:[Ljai;

    const/4 v4, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lhei;->b:Landroid/content/Context;

    invoke-static {v13}, Lgtt;->a(Landroid/content/Context;)Ljai;

    move-result-object v13

    aput-object v13, v3, v4

    :cond_6
    const/4 v3, 0x0

    move/from16 v17, v3

    move v3, v2

    move/from16 v2, v17

    :goto_4
    if-ge v2, v12, :cond_8

    iget-object v13, v5, Ljbb;->l:[Ljai;

    add-int/lit8 v4, v3, 0x1

    iget-object v14, v11, Lgsh;->c:[Ljai;

    aget-object v14, v14, v2

    aput-object v14, v13, v3

    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_4

    :cond_7
    iget-object v2, v10, Lgsf;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, v10, Lgsf;->c:Ljava/lang/String;

    iput-object v2, v5, Ljbb;->h:Ljava/lang/String;

    goto :goto_3

    :cond_8
    if-nez v8, :cond_9

    iget-object v2, v5, Ljbb;->l:[Ljai;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhei;->b:Landroid/content/Context;

    invoke-static {v4}, Lgtt;->a(Landroid/content/Context;)Ljai;

    move-result-object v4

    aput-object v4, v2, v3

    :cond_9
    iget-boolean v2, v10, Lgsf;->d:Z

    if-eqz v2, :cond_a

    iget-boolean v2, v9, Ljbg;->j:Z

    if-nez v2, :cond_a

    const/4 v2, 0x1

    :goto_5
    iput-boolean v2, v5, Ljbb;->j:Z

    iput-boolean v6, v5, Ljbb;->k:Z

    iput-object v5, v7, Lizz;->a:Ljbb;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->b:Landroid/content/Context;

    const-string v3, "wallet_items_cache"

    const-string v4, "cache_hit"

    const-string v5, "persistent_cache"

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v2, v3, v4, v5, v6}, Lgsl;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->b:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v3

    const-string v4, "wallet_items_persistent_cache_hit"

    invoke-static {v2, v3, v4}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v3, 0x13

    invoke-direct {v2, v3, v7}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    goto/16 :goto_0

    :cond_a
    const/4 v2, 0x0

    goto :goto_5

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->b:Landroid/content/Context;

    const-string v5, "wallet_items_cache"

    const-string v7, "cache_miss"

    const-string v8, "persistent_cache"

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-static {v2, v5, v7, v8, v12}, Lgsl;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->c:Lhfb;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->d:Lhcg;

    invoke-virtual {v2, v3, v9, v5}, Lhcg;->b(Ljava/lang/String;Lizs;Lizs;)V

    instance-of v2, v5, Lizz;

    if-eqz v2, :cond_10

    check-cast v5, Lizz;

    iget-object v13, v5, Lizz;->a:Ljbb;

    iget-boolean v2, v13, Ljbb;->k:Z

    if-eqz v2, :cond_e

    if-eqz v6, :cond_e

    const/4 v2, 0x1

    :goto_6
    iput-boolean v2, v13, Ljbb;->k:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->e:Lgsi;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    iget-boolean v6, v9, Ljbg;->g:Z

    iget-boolean v7, v9, Ljbg;->h:Z

    iget-boolean v8, v9, Ljbg;->i:Z

    invoke-static/range {v2 .. v8}, Lhfx;->a(Lgsi;Landroid/accounts/Account;ILizz;ZZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhei;->g:Lgse;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4, v5, v10}, Lhfx;->a(Lgse;Ljava/lang/String;ILizz;Lgsf;)V

    iget-boolean v2, v9, Ljbg;->k:Z

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v6, v0, Lhei;->f:Lgsg;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v8

    move v9, v4

    move-object v10, v5

    invoke-static/range {v6 .. v11}, Lhfx;->a(Lgsg;Landroid/accounts/Account;Ljava/lang/String;ILizz;Lgsh;)V

    :cond_d
    iget-object v2, v13, Ljbb;->l:[Ljai;

    array-length v2, v2

    if-lez v2, :cond_10

    iget-object v2, v11, Lgsh;->b:Ljava/lang/String;

    const-string v3, "dont_send_loyalty_wob_id"

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v5, v13, Ljbb;->l:[Ljai;

    array-length v2, v5

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Ljai;

    iput-object v2, v13, Ljbb;->l:[Ljai;

    iget-object v2, v13, Ljbb;->l:[Ljai;

    const/4 v4, 0x0

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lhei;->b:Landroid/content/Context;

    invoke-static {v6}, Lgtt;->a(Landroid/content/Context;)Ljai;

    move-result-object v6

    aput-object v6, v2, v4

    array-length v6, v5

    const/4 v2, 0x0

    :goto_7
    if-ge v2, v6, :cond_10

    aget-object v7, v5, v2

    iget-object v8, v13, Ljbb;->l:[Ljai;

    add-int/lit8 v4, v3, 0x1

    aput-object v7, v8, v3

    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_7

    :cond_e
    const/4 v2, 0x0

    goto :goto_6

    :cond_f
    iget-object v2, v13, Ljbb;->l:[Ljai;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhei;->b:Landroid/content/Context;

    invoke-static {v3}, Lgtt;->a(Landroid/content/Context;)Ljai;

    move-result-object v3

    invoke-static {v2, v3}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljai;

    iput-object v2, v13, Ljbb;->l:[Ljai;

    :cond_10
    move-object v2, v12

    goto/16 :goto_0
.end method
