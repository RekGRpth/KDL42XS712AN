.class public final Lfj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lfs;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    new-instance v0, Lfr;

    invoke-direct {v0}, Lfr;-><init>()V

    sput-object v0, Lfj;->a:Lfs;

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    new-instance v0, Lfq;

    invoke-direct {v0}, Lfq;-><init>()V

    sput-object v0, Lfj;->a:Lfs;

    goto :goto_0

    :cond_1
    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    new-instance v0, Lfp;

    invoke-direct {v0}, Lfp;-><init>()V

    sput-object v0, Lfj;->a:Lfs;

    goto :goto_0

    :cond_2
    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    new-instance v0, Lfo;

    invoke-direct {v0}, Lfo;-><init>()V

    sput-object v0, Lfj;->a:Lfs;

    goto :goto_0

    :cond_3
    const/16 v1, 0xb

    if-lt v0, v1, :cond_4

    new-instance v0, Lfn;

    invoke-direct {v0}, Lfn;-><init>()V

    sput-object v0, Lfj;->a:Lfs;

    goto :goto_0

    :cond_4
    const/16 v1, 0x9

    if-lt v0, v1, :cond_5

    new-instance v0, Lfm;

    invoke-direct {v0}, Lfm;-><init>()V

    sput-object v0, Lfj;->a:Lfs;

    goto :goto_0

    :cond_5
    new-instance v0, Lfk;

    invoke-direct {v0}, Lfk;-><init>()V

    sput-object v0, Lfj;->a:Lfs;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;IIII)V
    .locals 6

    sget-object v0, Lfj;->a:Lfs;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lfs;->a(Landroid/view/View;IIII)V

    return-void
.end method

.method public static a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0, p1, p2}, Lfs;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0, p1}, Lfs;->a(Landroid/view/View;Landroid/graphics/Paint;)V

    return-void
.end method

.method public static a(Landroid/view/View;Ldv;)V
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0, p1}, Lfs;->a(Landroid/view/View;Ldv;)V

    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0, p1}, Lfs;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Landroid/view/View;)Z
    .locals 2

    sget-object v0, Lfj;->a:Lfs;

    const/4 v1, -0x1

    invoke-interface {v0, p0, v1}, Lfs;->b(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;I)Z
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0, p1}, Lfs;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)I
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0}, Lfs;->a(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/View;)V
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0}, Lfs;->b(Landroid/view/View;)V

    return-void
.end method

.method public static d(Landroid/view/View;)I
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0}, Lfs;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/view/View;)V
    .locals 2

    sget-object v0, Lfj;->a:Lfs;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lfs;->c(Landroid/view/View;I)V

    return-void
.end method

.method public static f(Landroid/view/View;)I
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0}, Lfs;->d(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static g(Landroid/view/View;)I
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0}, Lfs;->e(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static h(Landroid/view/View;)Landroid/view/ViewParent;
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0}, Lfs;->f(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method public static i(Landroid/view/View;)Z
    .locals 1

    sget-object v0, Lfj;->a:Lfs;

    invoke-interface {v0, p0}, Lfs;->g(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
