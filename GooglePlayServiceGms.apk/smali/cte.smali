.class public final Lcte;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lbdo;

.field public static final b:Lbem;

.field public static final c:Lbdm;

.field public static final d:Lbem;

.field public static final e:Lbdm;

.field public static final f:Lctl;

.field public static final g:Lcub;

.field public static final h:Ldfo;

.field public static final i:Ldfx;

.field public static final j:Ldgs;

.field public static final k:Ldgh;

.field public static final l:Ldfy;

.field public static final m:Lctw;

.field public static final n:Lctp;

.field public static final o:Ldlf;

.field public static final p:Ldej;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lctf;

    invoke-direct {v0}, Lctf;-><init>()V

    sput-object v0, Lcte;->a:Lbdo;

    new-instance v0, Lbem;

    const-string v1, "https://www.googleapis.com/auth/games"

    invoke-direct {v0, v1}, Lbem;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcte;->b:Lbem;

    new-instance v0, Lbdm;

    sget-object v1, Lcte;->a:Lbdo;

    new-array v2, v5, [Lbem;

    sget-object v3, Lcte;->b:Lbem;

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lbdm;-><init>(Lbdo;[Lbem;)V

    sput-object v0, Lcte;->c:Lbdm;

    new-instance v0, Lbem;

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    invoke-direct {v0, v1}, Lbem;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcte;->d:Lbem;

    new-instance v0, Lbdm;

    sget-object v1, Lcte;->a:Lbdo;

    new-array v2, v5, [Lbem;

    sget-object v3, Lcte;->d:Lbem;

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lbdm;-><init>(Lbdo;[Lbem;)V

    sput-object v0, Lcte;->e:Lbdm;

    new-instance v0, Ldbj;

    invoke-direct {v0}, Ldbj;-><init>()V

    sput-object v0, Lcte;->f:Lctl;

    new-instance v0, Ldaz;

    invoke-direct {v0}, Ldaz;-><init>()V

    sput-object v0, Lcte;->g:Lcub;

    new-instance v0, Ldbr;

    invoke-direct {v0}, Ldbr;-><init>()V

    sput-object v0, Lcte;->h:Ldfo;

    new-instance v0, Ldbq;

    invoke-direct {v0}, Ldbq;-><init>()V

    sput-object v0, Lcte;->i:Ldfx;

    new-instance v0, Lddn;

    invoke-direct {v0}, Lddn;-><init>()V

    sput-object v0, Lcte;->j:Ldgs;

    new-instance v0, Ldcx;

    invoke-direct {v0}, Ldcx;-><init>()V

    sput-object v0, Lcte;->k:Ldgh;

    new-instance v0, Ldcc;

    invoke-direct {v0}, Ldcc;-><init>()V

    sput-object v0, Lcte;->l:Ldfy;

    new-instance v0, Ldcn;

    invoke-direct {v0}, Ldcn;-><init>()V

    sput-object v0, Lcte;->m:Lctw;

    new-instance v0, Ldcd;

    invoke-direct {v0}, Ldcd;-><init>()V

    sput-object v0, Lcte;->n:Lctp;

    new-instance v0, Ldcy;

    invoke-direct {v0}, Ldcy;-><init>()V

    sput-object v0, Lcte;->o:Ldlf;

    new-instance v0, Ldbd;

    invoke-direct {v0}, Ldbd;-><init>()V

    sput-object v0, Lcte;->p:Ldej;

    return-void
.end method

.method public static a(Lbdu;)Lcwm;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "GoogleApiClient parameter is required."

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-interface {p0}, Lbdu;->d()Z

    move-result v0

    const-string v3, "GoogleApiClient must be connected."

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    sget-object v0, Lcte;->a:Lbdo;

    invoke-interface {p0, v0}, Lbdu;->a(Lbdo;)Lbdn;

    move-result-object v0

    check-cast v0, Lcwm;

    if-eqz v0, :cond_1

    :goto_1
    const-string v2, "GoogleApiClient is not configured to use the Games Api. Pass Games.API into GoogleApiClient.Builder#addApi() to use this feature."

    invoke-static {v1, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public static a(Lbdu;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcwm;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbdu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcwm;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static b(Lbdu;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lbdu;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lbdu;)Landroid/content/Intent;
    .locals 1

    invoke-static {p0}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->q()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lbdu;)Lbeh;
    .locals 1

    new-instance v0, Lctg;

    invoke-direct {v0}, Lctg;-><init>()V

    invoke-interface {p0, v0}, Lbdu;->b(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method
