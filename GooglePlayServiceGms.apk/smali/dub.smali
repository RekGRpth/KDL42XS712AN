.class public final Ldub;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:Landroid/os/Bundle;

.field private final g:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;II[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldub;->b:Ldad;

    iput-object p3, p0, Ldub;->c:Ljava/lang/String;

    iput p4, p0, Ldub;->d:I

    iput p5, p0, Ldub;->e:I

    iput-object p6, p0, Ldub;->g:[Ljava/lang/String;

    iput-object p7, p0, Ldub;->f:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldub;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->n(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    iget-object v2, p0, Ldub;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldub;->c:Ljava/lang/String;

    iget v4, p0, Ldub;->d:I

    iget v0, p0, Ldub;->e:I

    iget-object v5, p0, Ldub;->g:[Ljava/lang/String;

    iget-object v6, p0, Ldub;->f:Landroid/os/Bundle;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I[Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
