.class Lju;
.super Ljq;
.source "SourceFile"


# instance fields
.field d:Landroid/view/Menu;


# direct methods
.method constructor <init>(Ljp;)V
    .locals 0

    invoke-direct {p0, p1}, Ljq;-><init>(Ljp;)V

    return-void
.end method


# virtual methods
.method public a()Ljj;
    .locals 3

    new-instance v0, Lkc;

    iget-object v1, p0, Lju;->a:Ljp;

    iget-object v2, p0, Lju;->a:Ljp;

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Landroid/app/Activity;Ljk;)V

    return-object v0
.end method

.method a(Landroid/content/Context;Landroid/view/ActionMode;)Lks;
    .locals 1

    new-instance v0, Lks;

    invoke-direct {v0, p1, p2}, Lks;-><init>(Landroid/content/Context;Landroid/view/ActionMode;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0, p1}, Ljp;->b(I)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    const-string v0, "splitActionBarWhenNarrow"

    invoke-virtual {p0}, Lju;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0}, Ljp;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setUiOptions(II)V

    :cond_0
    invoke-super {p0, p1}, Ljq;->a(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lju;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lju;->a:Ljp;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljp;->requestWindowFeature(I)Z

    :cond_1
    iget-boolean v0, p0, Lju;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lju;->a:Ljp;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljp;->requestWindowFeature(I)Z

    :cond_2
    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0}, Ljp;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v1

    new-instance v2, Ljv;

    invoke-direct {v2, p0, v1}, Ljv;-><init>(Lju;Landroid/view/Window$Callback;)V

    invoke-virtual {v0, v2}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    return-void
.end method

.method public final a(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {p0}, Lju;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lju;->a(Landroid/content/Context;Landroid/view/ActionMode;)Lks;

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0, p1}, Ljp;->a(Landroid/view/View;)V

    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0, p1, p2}, Ljp;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    return-void
.end method

.method public final a(ILandroid/view/Menu;)Z
    .locals 2

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_3

    :cond_0
    iget-object v0, p0, Lju;->d:Landroid/view/Menu;

    if-nez v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    new-instance v0, Lmg;

    invoke-direct {v0, p2}, Lmg;-><init>(Landroid/view/Menu;)V

    move-object p2, v0

    :cond_1
    iput-object p2, p0, Lju;->d:Landroid/view/Menu;

    :cond_2
    iget-object v0, p0, Lju;->a:Ljp;

    iget-object v1, p0, Lju;->d:Landroid/view/Menu;

    invoke-virtual {v0, p1, v1}, Ljp;->a(ILandroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_3
    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0, p1, p2}, Ljp;->a(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(ILandroid/view/MenuItem;)Z
    .locals 1

    if-nez p1, :cond_0

    invoke-static {p2}, Lmf;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0, p1, p2}, Ljp;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 2

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lju;->a:Ljp;

    iget-object v1, p0, Lju;->d:Landroid/view/Menu;

    invoke-virtual {v0, p1, p2, v1}, Ljp;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0, p1, p2, p3}, Ljp;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(I)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {p0}, Lju;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lju;->a(Landroid/content/Context;Landroid/view/ActionMode;)Lks;

    return-void
.end method

.method public final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Lju;->a:Ljp;

    invoke-virtual {v0, p1, p2}, Ljp;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 0

    return-void
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lju;->d:Landroid/view/Menu;

    return-void
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lju;->a:Ljp;

    return-void
.end method
