.class public final Leeu;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final i:Ljava/util/HashSet;


# instance fields
.field a:I

.field b:I

.field c:Z

.field d:I

.field e:I

.field public f:Lega;

.field g:Limv;

.field public h:Z

.field private j:Lefs;

.field private k:Landroid/content/Context;

.field private l:Landroid/os/PowerManager$WakeLock;

.field private m:Ljava/lang/Object;

.field private n:Landroid/os/Handler;

.field private o:Lorg/apache/http/client/HttpClient;

.field private p:Legb;

.field private q:Lefw;

.field private r:I

.field private s:Lefa;

.field private final t:Ljava/lang/Runnable;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "android.intent.category.MASTER_CLEAR"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "android.server.checkin.CHECKIN"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "com.google.android.gsf.subscribedfeeds"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "INSTALL_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "REMOVE_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "SERVER_NOTIFICATION"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "DECLINE_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "com.google.android.gsf"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    const-string v1, "com.google.android.apps.googlevoice.INBOX_NOTIFICATION"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Legb;Lefw;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const v0, 0x15180

    iput v0, p0, Leeu;->a:I

    const/16 v0, 0x1000

    iput v0, p0, Leeu;->b:I

    iput-boolean v2, p0, Leeu;->c:Z

    iput v2, p0, Leeu;->d:I

    const/16 v0, 0xe10

    iput v0, p0, Leeu;->e:I

    iput v1, p0, Leeu;->r:I

    new-instance v0, Leev;

    invoke-direct {v0, p0}, Leev;-><init>(Leeu;)V

    iput-object v0, p0, Leeu;->t:Ljava/lang/Runnable;

    const/4 v0, 0x0

    iput-object v0, p0, Leeu;->u:Ljava/lang/String;

    iput-boolean v1, p0, Leeu;->h:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Leeu;->m:Ljava/lang/Object;

    iput-object p1, p0, Leeu;->k:Landroid/content/Context;

    iput-object p2, p0, Leeu;->n:Landroid/os/Handler;

    iget-object v0, p0, Leeu;->k:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "GOOGLE_C2DM"

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Leeu;->l:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Leeu;->l:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iput-object p3, p0, Leeu;->p:Legb;

    iput-object p4, p0, Leeu;->q:Lefw;

    new-instance v0, Lega;

    iget-object v1, p0, Leeu;->k:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lega;-><init>(Landroid/content/Context;Leeu;)V

    iput-object v0, p0, Leeu;->f:Lega;

    new-instance v0, Lefa;

    iget-object v1, p0, Leeu;->n:Landroid/os/Handler;

    iget-object v2, p0, Leeu;->t:Ljava/lang/Runnable;

    invoke-direct {v0, v1, v2, p4}, Lefa;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;Lefw;)V

    iput-object v0, p0, Leeu;->s:Lefa;

    invoke-virtual {p0, p1}, Leeu;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;ILeez;Ljava/util/Map;)V
    .locals 8

    const/4 v6, 0x0

    sget-object v0, Leeu;->i:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v2, v6

    :goto_0
    iput p3, p4, Leez;->b:I

    if-nez p5, :cond_4

    const-string v0, ""

    :goto_1
    iput-object v0, p4, Leez;->c:Ljava/lang/String;

    const-string v0, "GCM/DMM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Send broadcast "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v2, :cond_5

    const-string v0, ""

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v1, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, " extras: "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Leeu;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Leeu;->n:Landroid/os/Handler;

    new-instance v1, Leey;

    invoke-direct {v1, p0, p4}, Leey;-><init>(Leeu;Leez;)V

    const-wide/16 v3, 0x1388

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v1, p0, Leeu;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leeu;->l:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gtz p3, :cond_7

    iget-object v0, p0, Leeu;->k:Landroid/content/Context;

    iget-object v4, p0, Leeu;->n:Landroid/os/Handler;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p4

    move-object v7, v6

    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    :goto_4
    return-void

    :cond_1
    const-string v0, "INSTALL_ASSET"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "REMOVE_ASSET"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "DECLINE_ASSET"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "UPDATES_AVAILABLE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "SERVER_NOTIFICATION"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move-object v2, v6

    goto/16 :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".permission.C2D_MESSAGE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_4
    const-string v0, "UAID"

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_1

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, " with permission="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_6
    const-string v0, ""

    goto/16 :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_7
    invoke-static {p3}, Leet;->b(I)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "GCM/DMM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to send message to stopped user "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v0, p0, Leeu;->k:Landroid/content/Context;

    iget-object v1, p0, Leeu;->n:Landroid/os/Handler;

    invoke-static {v0, p3, p1, p4, v1}, Leet;->a(Landroid/content/Context;ILandroid/content/Intent;Landroid/content/BroadcastReceiver;Landroid/os/Handler;)V

    goto/16 :goto_4
.end method

.method private static a(Landroid/os/Messenger;)V
    .locals 2

    if-eqz p0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0}, Leeu;->a(Landroid/os/Messenger;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/os/Messenger;Landroid/content/Intent;)V
    .locals 3

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "GCM/DMM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCM/DMM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sent API result "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "Failed to return send result using messenger"

    invoke-static {v0}, Leeu;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Leeu;)V
    .locals 0

    invoke-direct {p0}, Leeu;->f()V

    return-void
.end method

.method private static a(Line;Landroid/content/Intent;Ljava/util/Map;)V
    .locals 5

    iget-object v0, p0, Line;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linc;

    iget-object v2, v0, Linc;->a:Ljava/lang/String;

    iget-object v0, v0, Linc;->b:Ljava/lang/String;

    const-string v3, "from"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "google."

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_2
    return-void
.end method

.method static a(Line;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Linc;

    invoke-direct {v0}, Linc;-><init>()V

    invoke-virtual {v0, p1}, Linc;->a(Ljava/lang/String;)Linc;

    invoke-virtual {v0, p2}, Linc;->b(Ljava/lang/String;)Linc;

    invoke-virtual {p0, v0}, Line;->a(Linc;)Line;

    goto :goto_0
.end method

.method private a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    .locals 9

    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, p1, Line;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "error"

    invoke-virtual {v2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "message_type"

    const-string v1, "send_error"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    const-string v0, "google.message_id"

    invoke-virtual {v2, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    if-eqz p4, :cond_1

    invoke-static {p4, v2}, Leeu;->a(Landroid/os/Messenger;Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    iget-object v7, p1, Line;->d:Ljava/lang/String;

    iget-wide v0, p1, Line;->i:J

    long-to-int v8, v0

    new-instance v0, Leez;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Leez;-><init>(Leeu;Landroid/content/Intent;Line;J)V

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, v7

    move v4, v8

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Leeu;->a(Landroid/content/Intent;Ljava/lang/String;ILeez;Ljava/util/Map;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 2

    const-string v0, "GCM/DMM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCM/DMM"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    new-instance v1, Line;

    invoke-direct {v1}, Line;-><init>()V

    const-string v0, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v1, v0}, Line;->c(Ljava/lang/String;)Line;

    const-string v0, "app"

    invoke-static {v1, v0, p1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "USN"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "UAID"

    invoke-static {v1, v0, p3}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "info"

    invoke-static {v1, v0, p4}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "gcmr"

    if-eqz p5, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-static {v1, v2, v0}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Leeu;->k:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/gcm/GcmProvisioning;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "ver"

    invoke-static {v1, v2, v0}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "GCM/DMM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GCM/DMM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendAppStatus: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Leeu;->g:Limv;

    invoke-virtual {v0, v1}, Limv;->d(Lizk;)V

    return-void

    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    if-lez p2, :cond_2

    invoke-static {p1, p2}, Leet;->a(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "GCM/DMM"

    const/4 v4, 0x3

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "findReceiverForIntent: queryBroadcastReceivers took "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms, found="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Leeu;->a(Ljava/lang/String;)V

    :cond_1
    return v0

    :cond_2
    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private a(Line;Landroid/os/Messenger;)Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Leeu;->h:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Leeu;->k:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Leex;

    invoke-direct {v2, p0, p1, p2}, Leex;-><init>(Leeu;Line;Landroid/os/Messenger;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Leeu;->g:Limv;

    invoke-virtual {v1}, Limv;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Leeu;->g:Limv;

    invoke-virtual {v1, p1}, Limv;->d(Lizk;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)I
    .locals 3

    const/4 v1, 0x0

    const-string v0, "google.delay"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "google.delay"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    iget v2, p0, Leeu;->d:I

    if-ge v0, v2, :cond_1

    :goto_1
    iget v0, p0, Leeu;->e:I

    if-le v1, v0, :cond_0

    iget v1, p0, Leeu;->e:I

    :cond_0
    return v1

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_1
    move v1, v0

    goto :goto_1
.end method

.method private d(Line;)Z
    .locals 6

    :try_start_0
    iget-object v0, p0, Leeu;->s:Lefa;

    invoke-virtual {v0}, Lefa;->a()I

    move-result v1

    const/16 v2, 0xa

    if-lt v1, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Delayed message queue is full"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p1, Line;->j:I

    if-nez v1, :cond_3

    iget-object v1, v0, Lefa;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    iget v1, p1, Line;->o:I

    iget-object v2, v0, Lefa;->b:Lefg;

    invoke-interface {v2}, Lefg;->a()J

    move-result-wide v2

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v4, v1

    add-long v1, v2, v4

    invoke-virtual {v0}, Lefa;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lefa;->f:J

    cmp-long v3, v1, v3

    if-gez v3, :cond_2

    :cond_1
    invoke-virtual {v0}, Lefa;->c()V

    iput-wide v1, v0, Lefa;->f:J

    iget-object v0, v0, Lefa;->a:Leff;

    invoke-interface {v0, v1, v2}, Leff;->a(J)V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget v1, v0, Lefa;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lefa;->d:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method private f()V
    .locals 8

    const-wide/16 v6, 0x3e8

    iget-object v1, p0, Leeu;->s:Lefa;

    invoke-virtual {v1}, Lefa;->c()V

    invoke-virtual {v1}, Lefa;->a()I

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Line;

    const-string v2, "GCM/DMM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "GCM/DMM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending delayed message "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    div-long/2addr v2, v6

    iget-wide v4, v0, Line;->k:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v0, v2}, Line;->e(I)Line;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Leeu;->a(Line;Landroid/os/Messenger;)Z

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v1, Lefa;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, v1, Lefa;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, v1, Lefa;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    :cond_3
    iget v2, v1, Lefa;->d:I

    if-lez v2, :cond_0

    iget-object v2, v1, Lefa;->c:Lefh;

    new-instance v3, Lefe;

    invoke-direct {v3, v1, v0}, Lefe;-><init>(Lefa;Ljava/util/List;)V

    invoke-interface {v2, v3}, Lefh;->a(Lefz;)V

    iget-object v2, v1, Lefa;->b:Lefg;

    invoke-interface {v2}, Lefg;->a()J

    move-result-wide v2

    div-long/2addr v2, v6

    iput-wide v2, v1, Lefa;->g:J

    const/4 v2, 0x0

    iput v2, v1, Lefa;->d:I

    goto :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Leeu;->f:Lega;

    iget-object v1, v0, Lega;->d:Leeq;

    invoke-virtual {v1}, Leeq;->c()V

    iget-object v1, v0, Lega;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gcm_ttl"

    const v2, 0x15180

    invoke-static {v0, v1, v2}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leeu;->a:I

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gcm_len"

    const/16 v2, 0x1000

    invoke-static {v0, v1, v2}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leeu;->b:I

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gcm_upreg"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leeu;->r:I

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gcm_delay_enable"

    invoke-static {v0, v1, v3}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Leeu;->c:Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gcm_delay_min"

    invoke-static {v0, v1, v3}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leeu;->d:I

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gcm_delay_max"

    const/16 v2, 0xe10

    invoke-static {v0, v1, v2}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leeu;->e:I

    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 15

    const/4 v5, 0x0

    const-string v1, "google.ttl"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "google.message_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v1, "collapse_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "google.messenger"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    if-eqz v1, :cond_0

    const-string v2, "google.messenger"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    const-string v2, "app"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_2

    instance-of v6, v2, Landroid/app/PendingIntent;

    if-eqz v6, :cond_2

    check-cast v2, Landroid/app/PendingIntent;

    invoke-virtual {v2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    move-object v8, v2

    :goto_0
    if-nez v8, :cond_3

    const-string v1, "GCM/DMM"

    const-string v2, "Failed to send message - missing package name"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v2, 0x0

    move-object v8, v2

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v2, "app"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    new-instance v10, Line;

    invoke-direct {v10}, Line;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_1c

    :try_start_2
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    :goto_2
    if-ltz v2, :cond_4

    :try_start_3
    iget v3, p0, Leeu;->a:I

    if-le v2, v3, :cond_1b

    :cond_4
    iget v2, p0, Leeu;->a:I

    move v7, v2

    :goto_3
    invoke-virtual {v10, v7}, Line;->b(I)Line;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v11, 0x3e8

    div-long/2addr v2, v11

    invoke-virtual {v10, v2, v3}, Line;->b(J)Line;

    invoke-direct/range {p0 .. p1}, Leeu;->c(Landroid/content/Intent;)I

    move-result v11

    if-lez v11, :cond_5

    invoke-virtual {v10, v11}, Line;->d(I)Line;

    :cond_5
    if-eqz v9, :cond_6

    invoke-virtual {v10, v9}, Line;->a(Ljava/lang/String;)Line;

    :cond_6
    if-eqz v4, :cond_7

    invoke-virtual {v10, v4}, Line;->d(Ljava/lang/String;)Line;

    const-string v2, "collapse_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_7
    const-string v2, "GOOG.USER_SERIAL"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v10, v2, v3}, Line;->a(J)Line;

    const-string v2, "GOOG.USER_SERIAL"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v2, "GOOG.USER_AID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    :cond_8
    iget-object v2, p0, Leeu;->u:Ljava/lang/String;

    if-nez v2, :cond_9

    const-string v2, "AUTHENTICATION_FAILED"

    invoke-direct {p0, v10, v9, v2, v1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    goto :goto_1

    :catch_0
    move-exception v2

    move v2, v5

    goto :goto_2

    :cond_9
    :try_start_4
    const-string v2, "google.to"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a

    const-string v2, "missing_to"

    invoke-direct {p0, v10, v9, v2, v1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    goto/16 :goto_1

    :cond_a
    :try_start_5
    const-string v3, "google.to"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v3, "registration_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "registration_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    if-eqz v3, :cond_b

    invoke-virtual {v10, v3}, Line;->f(Ljava/lang/String;)Line;

    :cond_b
    invoke-virtual {v10, v2}, Line;->b(Ljava/lang/String;)Line;

    invoke-virtual {v10, v8}, Line;->c(Ljava/lang/String;)Line;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    if-eqz v12, :cond_1a

    invoke-virtual {v12}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v6, v5

    :cond_c
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_c

    instance-of v3, v4, Ljava/lang/String;

    if-eqz v3, :cond_d

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v3, v0

    invoke-static {v10, v2, v3}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v6

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    add-int v6, v2, v3

    goto :goto_4

    :cond_d
    const-string v3, "GCM"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v14, "Ignoring "

    invoke-direct {v4, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v1

    move v2, v5

    :goto_5
    if-nez v2, :cond_e

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    :cond_e
    throw v1

    :cond_f
    move v2, v6

    :goto_6
    :try_start_6
    iget v3, p0, Leeu;->b:I

    if-le v2, v3, :cond_10

    const-string v2, "MessageTooBig"

    invoke-direct {p0, v10, v9, v2, v1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    goto/16 :goto_1

    :cond_10
    if-eqz v7, :cond_11

    if-eqz v9, :cond_11

    const/4 v2, 0x1

    move v3, v2

    :goto_7
    if-eqz v3, :cond_14

    :try_start_7
    iget-object v2, p0, Leeu;->k:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Leeu;->j:Lefs;

    invoke-virtual {v2}, Lefs;->c()Z

    move-result v2

    if-nez v2, :cond_12

    const-string v2, "TtlUnsupported"

    invoke-direct {p0, v10, v9, v2, v1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    goto/16 :goto_1

    :cond_11
    move v3, v5

    goto :goto_7

    :cond_12
    :try_start_8
    iget-object v2, p0, Leeu;->q:Lefw;

    invoke-virtual {v2, v8}, Lefw;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_13

    const-string v2, "TooManyMessages"

    invoke-direct {p0, v10, v9, v2, v1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    goto/16 :goto_1

    :cond_13
    :try_start_9
    iget-object v2, p0, Leeu;->q:Lefw;

    invoke-virtual {v2, v10}, Lefw;->a(Lizk;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    invoke-static {v1}, Leeu;->a(Landroid/os/Messenger;)V

    :cond_14
    iget-boolean v2, p0, Leeu;->c:Z

    if-eqz v2, :cond_17

    if-lez v11, :cond_16

    invoke-direct {p0, v10}, Leeu;->d(Line;)Z

    move-result v2

    if-eqz v2, :cond_16

    const-string v1, "GCM/DMM"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_15

    const-string v1, "GCM/DMM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Delaying message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_15
    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    goto/16 :goto_1

    :catch_1
    move-exception v2

    :try_start_b
    const-string v2, "save_error"

    invoke-direct {p0, v10, v9, v2, v1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    goto/16 :goto_1

    :cond_16
    :try_start_c
    invoke-direct {p0}, Leeu;->f()V

    :cond_17
    invoke-direct {p0, v10, v1}, Leeu;->a(Line;Landroid/os/Messenger;)Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result v2

    if-nez v2, :cond_18

    if-eqz v3, :cond_19

    :try_start_d
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Queued GCM "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v10, Line;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :cond_18
    :goto_8
    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    goto/16 :goto_1

    :cond_19
    :try_start_e
    const-string v3, "SERVICE_NOT_AVAILABLE"

    invoke-direct {p0, v10, v9, v3, v1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto :goto_8

    :catchall_1
    move-exception v1

    goto/16 :goto_5

    :cond_1a
    move v2, v5

    goto/16 :goto_6

    :cond_1b
    move v7, v2

    goto/16 :goto_3

    :cond_1c
    move v7, v5

    goto/16 :goto_3
.end method

.method protected final a(Landroid/content/Intent;Line;IILjava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    if-nez p3, :cond_0

    const-string v0, "GCM/DMM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "broadcast intent callback: result=CANCELLED for"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    int-to-long v0, p4

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Leeu;->k:Landroid/content/Context;

    long-to-int v0, v0

    invoke-static {v2, p1, v0}, Leeu;->a(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p2, Line;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Should not happen. Received intent with no package name. "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Leeu;->a(Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v5

    goto :goto_0

    :cond_2
    iget-object v2, p0, Leeu;->k:Landroid/content/Context;

    invoke-static {v2}, Lefr;->a(Landroid/content/Context;)Lefr;

    move-result-object v2

    iget-object v2, v2, Lefr;->b:Lefu;

    invoke-virtual {v2, v1, p4}, Lefu;->a(Ljava/lang/String;I)V

    const-string v2, "GCM/DMM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Receiver package not found, unregister application "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sender "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Leeu;->r:I

    if-lez v0, :cond_3

    const/4 v4, 0x0

    move-object v0, p0

    move v2, p4

    move-object v3, p5

    invoke-direct/range {v0 .. v5}, Leeu;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "app"

    iget-object v3, p0, Leeu;->k:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {v3, v5, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "gcm_unreg_caller"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "app_gsf"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "GOOG.USER_SERIAL"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz p5, :cond_4

    const-string v1, "GOOG.USER_AID"

    invoke-virtual {p1, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_4
    iget-object v1, p0, Leeu;->k:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1
.end method

.method public final a(Lefs;)V
    .locals 0

    iput-object p1, p0, Leeu;->j:Lefs;

    return-void
.end method

.method public final a(Limv;)V
    .locals 1

    iput-object p1, p0, Leeu;->g:Limv;

    iget-object v0, p0, Leeu;->p:Legb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leeu;->p:Legb;

    iput-object p1, v0, Legb;->b:Limv;

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Leeu;->v:Ljava/lang/String;

    iput-object p2, p0, Leeu;->u:Ljava/lang/String;

    return-void
.end method

.method public final a(Lorg/apache/http/client/HttpClient;)V
    .locals 0

    iput-object p1, p0, Leeu;->o:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Leeu;->f:Lega;

    iput-boolean p1, v0, Lega;->h:Z

    return-void
.end method

.method public final a(Line;)Z
    .locals 7

    const/4 v0, 0x0

    iget v1, p1, Line;->j:I

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    iget-wide v3, p1, Line;->k:J

    iget v5, p1, Line;->j:I

    int-to-long v5, v5

    add-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iget-object v1, p1, Line;->a:Ljava/lang/String;

    const-string v2, "SERVICE_NOT_AVAILABLE"

    const/4 v3, 0x0

    invoke-direct {p0, p1, v1, v2, v3}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Line;Landroid/os/Messenger;Z)Z
    .locals 9

    const/16 v4, 0xc8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Leeu;->u:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p1, Line;->a:Ljava/lang/String;

    const-string v1, "AUTHENTICATION_FAILED"

    invoke-direct {p0, p1, v0, v1, p2}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V

    iget v0, p1, Line;->j:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Leeu;->q:Lefw;

    iget-object v1, p1, Line;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lefw;->a(Ljava/lang/String;)I

    :cond_0
    move v0, v6

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Leeu;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "d2c_url"

    const-string v2, "https://android.clients.google.com/gcm/upstream"

    invoke-static {v0, v1, v2}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v8, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v8, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const-string v0, "Authorization"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AidLogin "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Leeu;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Leeu;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Line;->c:Ljava/lang/String;

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, "project_id"

    iget-object v1, p1, Line;->c:Ljava/lang/String;

    invoke-virtual {v8, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-wide v2, p1, Line;->k:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Line;->c(I)Line;

    iget-object v0, p0, Leeu;->g:Limv;

    instance-of v0, v0, Leer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Leeu;->g:Limv;

    sget-object v0, Leer;->b:Landroid/os/Messenger;

    if-eqz v0, :cond_3

    iget-object v0, p0, Leeu;->g:Limv;

    const-string v0, "out"

    invoke-static {v0, p1}, Leer;->a(Ljava/lang/String;Lizk;)V

    :cond_3
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {p1}, Line;->d()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v8, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    :try_start_0
    iget-object v0, p0, Leeu;->o:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v8}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    const/16 v0, 0x191

    if-ne v5, v0, :cond_7

    const/16 v0, 0x193

    if-ne v5, v0, :cond_7

    const/4 v0, 0x0

    iput-object v0, p0, Leeu;->u:Ljava/lang/String;

    iget-object v0, p1, Line;->a:Ljava/lang/String;

    const-string v1, "AUTHENTICATION_FAILED"

    invoke-direct {p0, p1, v0, v1, p2}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V

    iget v0, p1, Line;->j:I

    if-eqz v0, :cond_4

    iget-object v0, p0, Leeu;->q:Lefw;

    iget-object v1, p1, Line;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lefw;->a(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :goto_1
    if-eqz p3, :cond_5

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    :cond_5
    :try_start_1
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_6
    :goto_2
    iget v0, p1, Line;->j:I

    if-nez v0, :cond_f

    iget-object v0, p1, Line;->a:Ljava/lang/String;

    const-string v1, "SERVICE_NOT_AVAILABLE"

    invoke-direct {p0, p1, v0, v1, p2}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V

    :goto_3
    move v0, v7

    goto/16 :goto_0

    :cond_7
    if-eq v5, v4, :cond_9

    :try_start_2
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sent http error "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Line;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    iget-object v0, p0, Leeu;->g:Limv;

    const/4 v1, 0x1

    const/16 v2, 0x8

    invoke-static {p1}, Lina;->b(Lizk;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Limv;->a(ZILjava/lang/String;II)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sent http io error "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Line;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz p3, :cond_8

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    :cond_8
    :try_start_4
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2

    :cond_9
    :try_start_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sent http "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Line;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Line;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    iget-object v0, p0, Leeu;->g:Limv;

    const/4 v1, 0x1

    const/16 v2, 0x8

    invoke-static {p1}, Lina;->b(Lizk;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0xc8

    invoke-virtual/range {v0 .. v5}, Limv;->a(ZILjava/lang/String;II)V

    invoke-virtual {p0, p1}, Leeu;->b(Line;)V

    iget v0, p1, Line;->j:I

    if-eqz v0, :cond_c

    iget-object v0, p0, Leeu;->q:Lefw;

    iget-object v1, p1, Line;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lefw;->a(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_4
    if-eqz p3, :cond_a

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    :cond_a
    :try_start_6
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_b
    :goto_5
    move v0, v6

    goto/16 :goto_0

    :cond_c
    :try_start_7
    iget-object v0, p1, Line;->a:Ljava/lang/String;

    invoke-static {p2}, Leeu;->a(Landroid/os/Messenger;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    if-eqz p3, :cond_d

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    :cond_d
    :try_start_8
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    :cond_e
    :goto_6
    throw v0

    :cond_f
    invoke-virtual {p0}, Leeu;->c()V

    goto/16 :goto_3

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v0

    goto/16 :goto_2

    :catch_4
    move-exception v0

    goto :goto_5
.end method

.method public final b()V
    .locals 2

    iget-object v1, p0, Leeu;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leeu;->l:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 8

    const-wide/16 v2, -0x1

    const-string v0, "cmt"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "cmt_ht"

    const-string v1, "cmt"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leeu;->k:Landroid/content/Context;

    invoke-static {v0}, Legd;->a(Landroid/content/Context;)Legd;

    move-result-object v4

    iget-object v5, p0, Leeu;->k:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v0, "gcm_hc_duration"

    invoke-static {v5, v0, v2, v3}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v7, v0, v2

    if-eqz v7, :cond_1

    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_3

    const-wide/32 v2, 0x5265c00

    cmp-long v2, v0, v2

    if-gtz v2, :cond_3

    const-string v2, "GCM.HTTP"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Enabling HTTP moratorium for duration: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, v4, Legd;->a:J

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.gcm.http.intent.moratorium"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "untilElapsedRealtime"

    iget-wide v2, v4, Legd;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {v5, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "mds"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_0

    :cond_3
    const-string v2, "GCM.HTTP"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring HTTP moratorium control message with missing or invalid moratorium duration: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    const-string v0, "app"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "app"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    const-string v0, "USN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "UAID"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v2, "info"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "gcm_unreg"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v0, :cond_6

    const/4 v2, 0x0

    :goto_2
    new-instance v0, Landroid/content/Intent;

    const-string v6, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "1"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Leeu;->k:Landroid/content/Context;

    invoke-static {v5}, Lefr;->a(Landroid/content/Context;)Lefr;

    move-result-object v5

    iget-object v5, v5, Lefr;->b:Lefu;

    invoke-virtual {v5, v1, v2}, Lefu;->a(Ljava/lang/String;I)V

    :cond_5
    iget-object v5, p0, Leeu;->k:Landroid/content/Context;

    invoke-static {v5, v0, v2}, Leeu;->a(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Leeu;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "GCM/DMM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_6
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    goto :goto_2
.end method

.method final b(Line;)V
    .locals 9

    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, p1, Line;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "event"

    const-string v1, "sent"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "message_type"

    const-string v1, "send_event"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "google.message_id"

    iget-object v1, p1, Line;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v7, p1, Line;->d:Ljava/lang/String;

    iget-wide v0, p1, Line;->i:J

    long-to-int v8, v0

    new-instance v0, Leez;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Leez;-><init>(Leeu;Landroid/content/Intent;Line;J)V

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, v7

    move v4, v8

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Leeu;->a(Landroid/content/Intent;Ljava/lang/String;ILeez;Ljava/util/Map;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-boolean v0, p0, Leeu;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Leeu;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Leeu;->q:Lefw;

    invoke-virtual {v0}, Lefw;->d()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leeu;->j:Lefs;

    invoke-virtual {v0}, Lefs;->d()V

    goto :goto_0
.end method

.method public final c(Line;)V
    .locals 9

    iget-object v7, p1, Line;->d:Ljava/lang/String;

    const-string v0, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Line;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linc;

    iget-object v2, v0, Linc;->a:Ljava/lang/String;

    iget-object v0, v0, Linc;->b:Ljava/lang/String;

    const-string v3, "UFS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Leeu;->p:Legb;

    const-string v2, "1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v1, Legb;->c:Z

    iget-object v2, p0, Leeu;->p:Legb;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const/4 v0, -0x1

    invoke-virtual {v2, v3, v0}, Legb;->a(Ljava/util/Map;I)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v4, Line;

    invoke-direct {v4}, Line;-><init>()V

    const-string v0, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v4, v0}, Line;->c(Ljava/lang/String;)Line;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, v0, v1}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, v2, Legb;->b:Limv;

    invoke-virtual {v0, v4}, Limv;->d(Lizk;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Leeu;->f:Lega;

    invoke-virtual {v0, p1}, Lega;->a(Line;)V

    goto :goto_1

    :cond_4
    iget-object v1, p1, Line;->b:Ljava/lang/String;

    iget-object v0, p1, Line;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v0, "GCM/DMM"

    const-string v1, "found msg w/o category, dropping"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :cond_5
    :goto_2
    if-nez v2, :cond_a

    const-string v0, "GCM/DMM"

    const-string v1, "processPacket: cannot parse data message "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    const-string v2, "GSYNC_TICKLE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, "com.google.android.gsf.subscribedfeeds"

    :cond_7
    iget-wide v2, p1, Line;->i:J

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v3, Leeu;->i:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :goto_3
    const-string v0, "from"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean v0, p1, Line;->l:Z

    if-eqz v0, :cond_8

    iget-object v0, p1, Line;->m:Lizf;

    if-eqz v0, :cond_8

    iget-object v0, p1, Line;->m:Lizf;

    invoke-virtual {v0}, Lizf;->a()I

    move-result v0

    if-lez v0, :cond_8

    const-string v0, "rawData"

    iget-object v1, p1, Line;->m:Lizf;

    invoke-virtual {v1}, Lizf;->b()[B

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_8
    iget-object v0, p1, Line;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "collapse_key"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    :cond_9
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    :cond_a
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1, v2, v6}, Leeu;->a(Line;Landroid/content/Intent;Ljava/util/Map;)V

    iget-object v0, p1, Line;->b:Ljava/lang/String;

    invoke-static {v7}, Lefr;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "google.com"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "registration_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v0, "com.google.android.c2dm.intent.REGISTRATION"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2, v7}, Landroid/content/Intent;->removeCategory(Ljava/lang/String;)V

    const-string v0, "app"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :cond_b
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "gcms"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "gcm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0, v2}, Leeu;->b(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_c
    iget-wide v0, p1, Line;->i:J

    long-to-int v8, v0

    new-instance v0, Leez;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Leez;-><init>(Leeu;Landroid/content/Intent;Line;J)V

    move-object v1, p0

    move-object v3, v7

    move v4, v8

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Leeu;->a(Landroid/content/Intent;Ljava/lang/String;ILeez;Ljava/util/Map;)V

    goto/16 :goto_1
.end method

.method final d()I
    .locals 5

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Leeu;->q:Lefw;

    new-instance v4, Leew;

    invoke-direct {v4, p0, v2, v0, v1}, Leew;-><init>(Leeu;Ljava/util/ArrayList;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-virtual {v3, v4}, Lefw;->a(Lefz;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Leeu;->q:Lefw;

    invoke-virtual {v3, v2}, Lefw;->a(Ljava/util/List;)I

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Resent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " err: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " unsent="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Leeu;->q:Lefw;

    invoke-virtual {v2}, Lefw;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Leeu;->j:Lefs;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lefs;->b(Z)V

    :cond_1
    invoke-virtual {p0}, Leeu;->c()V

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Leeu;->n:Landroid/os/Handler;

    iget-object v1, p0, Leeu;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GCM/DMM"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCM/DMM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DataMessageManager receive "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Leeu;->f:Lega;

    invoke-virtual {v0}, Lega;->b()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "android.intent.action.DREAMING_STARTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v0, p0, Leeu;->f:Lega;

    invoke-virtual {v0}, Lega;->d()V

    goto :goto_0

    :cond_4
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "android.intent.action.DREAMING_STOPPED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    iget-object v1, p0, Leeu;->f:Lega;

    :try_start_0
    iget-object v0, v1, Lega;->a:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "isKeyguardLocked"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v2, v1, Lega;->a:Landroid/app/KeyguardManager;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lega;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_6
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v0, p0, Leeu;->f:Lega;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lega;->h:Z

    iget-object v0, p0, Leeu;->f:Lega;

    iget-boolean v0, v0, Lega;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Leeu;->f:Lega;

    invoke-virtual {v0}, Lega;->c()V

    goto :goto_0

    :cond_7
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v0, p0, Leeu;->f:Lega;

    iput-boolean v4, v0, Lega;->h:Z

    iget-object v0, p0, Leeu;->f:Lega;

    iget-boolean v0, v0, Lega;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Leeu;->f:Lega;

    invoke-virtual {v0}, Lega;->c()V

    goto :goto_0

    :cond_8
    const-string v1, "android.net.conn.DATA_ACTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v0, "isActive"

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "deviceType"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Leeu;->f:Lega;

    iput-boolean v0, v2, Lega;->i:Z

    if-eqz v0, :cond_1

    invoke-static {v1}, Leet;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Leeu;->f:Lega;

    iget-boolean v0, v0, Lega;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Leeu;->f:Lega;

    invoke-virtual {v0}, Lega;->c()V

    goto/16 :goto_0

    :cond_9
    const-string v1, "android.intent.action.USER_STOPPED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "android.intent.action.USER_ADDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "android.intent.action.USER_STOPPING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_a
    iget-object v0, p0, Leeu;->g:Limv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Leeu;->p:Legb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Leeu;->p:Legb;

    invoke-virtual {v0, p0, p2}, Legb;->a(Landroid/content/BroadcastReceiver;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
