.class public final Lenw;
.super Lenu;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;Z)V
    .locals 0

    iput-object p1, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-direct {p0, p1}, Lenu;-><init>(Landroid/content/Context;)V

    iput-boolean p2, p0, Lenw;->c:Z

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->e(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->e(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lenv;

    invoke-super {p0, p1}, Lenu;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->d(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->e(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->f(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    iget-wide v2, p1, Lenv;->c:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->g(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    iget-wide v2, p1, Lenv;->b:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0b01e8    # com.google.android.gms.R.string.icing_storage_managment_reclaim_button_label

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->c(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b01eb    # com.google.android.gms.R.string.icing_storage_managment_empty_list

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->h(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lenx;

    iget-object v2, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    iget-object v3, p1, Lenv;->a:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lenx;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 4

    const/4 v1, 0x4

    const/4 v2, 0x0

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->d(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v3

    iget-boolean v0, p0, Lenw;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->e(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    iget-boolean v3, p0, Lenw;->c:Z

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lenw;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method
