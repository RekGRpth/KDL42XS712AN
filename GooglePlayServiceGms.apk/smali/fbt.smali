.class public final Lfbt;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/debug/PeopleExportActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/debug/PeopleExportActivity;)V
    .locals 0

    iput-object p1, p0, Lfbt;->a:Lcom/google/android/gms/people/debug/PeopleExportActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/people/debug/PeopleExportActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lfbt;-><init>(Lcom/google/android/gms/people/debug/PeopleExportActivity;)V

    return-void
.end method

.method private varargs a()Landroid/net/Uri;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lfbt;->a:Lcom/google/android/gms/people/debug/PeopleExportActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lfbs;->a(Landroid/content/Context;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to export"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lfbt;->a()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Landroid/net/Uri;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lfbt;->a:Lcom/google/android/gms/people/debug/PeopleExportActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->a(Lcom/google/android/gms/people/debug/PeopleExportActivity;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method protected final onPreExecute()V
    .locals 2

    iget-object v0, p0, Lfbt;->a:Lcom/google/android/gms/people/debug/PeopleExportActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->setProgressBarIndeterminateVisibility(Z)V

    return-void
.end method
