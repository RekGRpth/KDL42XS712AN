.class public final Lbao;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/service/CastService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/cast/service/CastService;)V
    .locals 0

    iput-object p1, p0, Lbao;->a:Lcom/google/android/gms/cast/service/CastService;

    invoke-direct {p0}, Lbip;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/cast/service/CastService;B)V
    .locals 0

    invoke-direct {p0, p1}, Lbao;-><init>(Lcom/google/android/gms/cast/service/CastService;)V

    return-void
.end method


# virtual methods
.method public final a(Lbjv;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 12

    new-instance v11, Lbbj;

    const-string v1, "CastService"

    invoke-direct {v11, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    const-string v1, "instance-%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/gms/cast/service/CastService;->a()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Laye;->a(Ljava/lang/String;)V

    :try_start_0
    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/cast/CastDevice;->b(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v4

    const-string v1, "last_application_id"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v1, "last_session_id"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    const-string v1, "com.google.android.gms.cast.EXTRA_CAST_FLAGS"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v1, "connecting to device with lastApplicationId=%s, lastSessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    const/4 v3, 0x1

    aput-object v6, v2, v3

    invoke-virtual {v11, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez p4, :cond_0

    const/4 v7, 0x0

    :goto_0
    new-instance v1, Lbal;

    iget-object v2, p0, Lbao;->a:Lcom/google/android/gms/cast/service/CastService;

    move-object v3, p1

    move-object v8, p3

    invoke-direct/range {v1 .. v11}, Lbal;-><init>(Lcom/google/android/gms/cast/service/CastService;Lbjv;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;Layb;Ljava/lang/String;JLaye;)V

    :goto_1
    return-void

    :catch_0
    move-exception v1

    const-string v2, "Cast device was not valid."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v11, v1, v2, v3}, Laye;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/16 v1, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_1
    invoke-interface {p1, v1, v2, v3}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v1, "client died while brokering service"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v11, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    const-string v1, "com.google.android.gms.cast.internal.ICastDeviceControllerListener"

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    instance-of v2, v1, Layb;

    if-eqz v2, :cond_1

    check-cast v1, Layb;

    move-object v7, v1

    goto :goto_0

    :cond_1
    new-instance v7, Layd;

    move-object/from16 v0, p4

    invoke-direct {v7, v0}, Layd;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method
