.class public final Lbql;
.super Landroid/os/Handler;
.source "SourceFile"


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:Z

.field private d:Landroid/os/Message;

.field private final e:Lbqk;

.field private f:Z

.field private g:[Lbqo;

.field private h:I

.field private i:[Lbqo;

.field private j:I

.field private final k:Lbqm;

.field private final l:Lbqn;

.field private m:Lbqi;

.field private final n:Ljava/util/HashMap;

.field private o:Lbqh;

.field private p:Lbqh;

.field private final q:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbql;->c:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/os/Looper;Lbqi;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-boolean v1, p0, Lbql;->a:Z

    iput-boolean v1, p0, Lbql;->b:Z

    new-instance v0, Lbqk;

    invoke-direct {v0, v1}, Lbqk;-><init>(B)V

    iput-object v0, p0, Lbql;->e:Lbqk;

    const/4 v0, -0x1

    iput v0, p0, Lbql;->h:I

    new-instance v0, Lbqm;

    invoke-direct {v0, p0, v1}, Lbqm;-><init>(Lbql;B)V

    iput-object v0, p0, Lbql;->k:Lbqm;

    new-instance v0, Lbqn;

    invoke-direct {v0, p0, v1}, Lbqn;-><init>(Lbql;B)V

    iput-object v0, p0, Lbql;->l:Lbqn;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbql;->n:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbql;->q:Ljava/util/ArrayList;

    iput-object p2, p0, Lbql;->m:Lbqi;

    iget-object v0, p0, Lbql;->k:Lbqm;

    invoke-direct {p0, v0, v2}, Lbql;->a(Lbqh;Lbqh;)Lbqo;

    iget-object v0, p0, Lbql;->l:Lbqn;

    invoke-direct {p0, v0, v2}, Lbql;->a(Lbqh;Lbqh;)Lbqo;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Looper;Lbqi;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbql;-><init>(Landroid/os/Looper;Lbqi;)V

    return-void
.end method

.method static synthetic a(Lbql;)Lbqi;
    .locals 1

    iget-object v0, p0, Lbql;->m:Lbqi;

    return-object v0
.end method

.method private final a(Lbqh;Lbqh;)Lbqo;
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v2, "StateMachine"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "addStateInternal: E state="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbqh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",parent="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p2, :cond_3

    const-string v0, ""

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lbql;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqo;

    if-nez v0, :cond_6

    invoke-direct {p0, p2, v1}, Lbql;->a(Lbqh;Lbqh;)Lbqo;

    move-result-object v0

    move-object v1, v0

    :cond_1
    :goto_1
    iget-object v0, p0, Lbql;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqo;

    if-nez v0, :cond_2

    new-instance v0, Lbqo;

    invoke-direct {v0, p0, v4}, Lbqo;-><init>(Lbql;B)V

    iget-object v2, p0, Lbql;->n:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v2, v0, Lbqo;->b:Lbqo;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lbqo;->b:Lbqo;

    if-eq v2, v1, :cond_4

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "state already added"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p2}, Lbqh;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iput-object p1, v0, Lbqo;->a:Lbqh;

    iput-object v1, v0, Lbqo;->b:Lbqo;

    iput-boolean v4, v0, Lbqo;->c:Z

    iget-boolean v1, p0, Lbql;->b:Z

    if-eqz v1, :cond_5

    const-string v1, "StateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addStateInternal: X stateInfo: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-object v0

    :cond_6
    move-object v1, v0

    goto :goto_1
.end method

.method static synthetic a(Lbql;Lbqh;Lbqh;)Lbqo;
    .locals 1

    invoke-direct {p0, p1, p2}, Lbql;->a(Lbqh;Lbqh;)Lbqo;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 6

    const/4 v1, 0x0

    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lbql;->p:Lbqh;

    if-eqz v2, :cond_4

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    const-string v2, "handleMessage: new destination call exit"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lbql;->p:Lbqh;

    iput-object v1, p0, Lbql;->p:Lbqh;

    const/4 v0, 0x0

    iput v0, p0, Lbql;->j:I

    iget-object v0, p0, Lbql;->n:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqo;

    :cond_1
    iget-object v3, p0, Lbql;->i:[Lbqo;

    iget v4, p0, Lbql;->j:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lbql;->j:I

    aput-object v0, v3, v4

    iget-object v0, v0, Lbqo;->b:Lbqo;

    if-eqz v0, :cond_2

    iget-boolean v3, v0, Lbqo;->c:Z

    if-eqz v3, :cond_1

    :cond_2
    iget-boolean v3, p0, Lbql;->b:Z

    if-eqz v3, :cond_3

    const-string v3, "StateMachine"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "setupTempStateStackWithStatesToEnter: X mTempStateStackCount="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lbql;->j:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",curStateInfo: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-direct {p0, v0}, Lbql;->a(Lbqo;)V

    invoke-direct {p0}, Lbql;->d()I

    move-result v0

    invoke-direct {p0, v0}, Lbql;->a(I)V

    invoke-direct {p0}, Lbql;->c()V

    move-object v0, v2

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_5

    iget-object v1, p0, Lbql;->l:Lbqn;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lbql;->m:Lbqi;

    invoke-virtual {v0}, Lbqi;->a()V

    invoke-direct {p0}, Lbql;->b()V

    :cond_5
    :goto_1
    return-void

    :cond_6
    iget-object v1, p0, Lbql;->k:Lbqm;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lbql;->m:Lbqi;

    goto :goto_1
.end method

.method private final a(I)V
    .locals 3

    :goto_0
    iget v0, p0, Lbql;->h:I

    if-gt p1, v0, :cond_1

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invokeEnterMethods: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbql;->g:[Lbqo;

    aget-object v2, v2, p1

    iget-object v2, v2, Lbqo;->a:Lbqh;

    invoke-virtual {v2}, Lbqh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lbql;->g:[Lbqo;

    aget-object v0, v0, p1

    iget-object v0, v0, Lbqo;->a:Lbqh;

    invoke-virtual {v0}, Lbqh;->b()V

    iget-object v0, p0, Lbql;->g:[Lbqo;

    aget-object v0, v0, p1

    const/4 v1, 0x1

    iput-boolean v1, v0, Lbqo;->c:Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final a(Landroid/os/Message;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbql;->g:[Lbqo;

    iget v1, p0, Lbql;->h:I

    aget-object v0, v0, v1

    iget-boolean v1, p0, Lbql;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "StateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processMsg: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lbqo;->a:Lbqh;

    invoke-virtual {v3}, Lbqh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget-object v2, Lbql;->c:Ljava/lang/Object;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_3

    iget-object v0, p0, Lbql;->l:Lbqn;

    invoke-direct {p0, v0}, Lbql;->a(Lbqg;)V

    :goto_1
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lbql;->b:Z

    if-eqz v1, :cond_3

    const-string v1, "StateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processMsg: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lbqo;->a:Lbqh;

    invoke-virtual {v3}, Lbqh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v1, v0, Lbqo;->a:Lbqh;

    invoke-virtual {v1, p1}, Lbqh;->a(Landroid/os/Message;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v0, v0, Lbqo;->b:Lbqo;

    if-nez v0, :cond_2

    iget-object v1, p0, Lbql;->m:Lbqi;

    invoke-virtual {v1, p1}, Lbqi;->b(Landroid/os/Message;)V

    :cond_4
    iget-object v1, p0, Lbql;->m:Lbqi;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lbql;->g:[Lbqo;

    iget v2, p0, Lbql;->h:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lbqo;->a:Lbqh;

    iget-object v2, p0, Lbql;->e:Lbqk;

    iget-object v3, p0, Lbql;->m:Lbqi;

    const-string v3, ""

    iget-object v0, v0, Lbqo;->a:Lbqh;

    invoke-virtual {v2, p1, v3, v0, v1}, Lbqk;->a(Landroid/os/Message;Ljava/lang/String;Lbqh;Lbqh;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lbql;->e:Lbqk;

    iget-object v1, p0, Lbql;->m:Lbqi;

    const-string v1, ""

    invoke-virtual {v0, p1, v1, v4, v4}, Lbqk;->a(Landroid/os/Message;Ljava/lang/String;Lbqh;Lbqh;)V

    goto :goto_1
.end method

.method private final a(Lbqg;)V
    .locals 3

    check-cast p1, Lbqh;

    iput-object p1, p0, Lbql;->p:Lbqh;

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "transitionTo: destState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbql;->p:Lbqh;

    invoke-virtual {v2}, Lbqh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method static synthetic a(Lbql;Landroid/os/Message;)V
    .locals 4

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deferMessage: msg="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbql;->m:Lbqi;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lbqi;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lbql;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    iget-object v1, p0, Lbql;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Lbql;Lbqg;)V
    .locals 0

    invoke-direct {p0, p1}, Lbql;->a(Lbqg;)V

    return-void
.end method

.method static synthetic a(Lbql;Lbqh;)V
    .locals 3

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setInitialState: initialState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbqh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lbql;->o:Lbqh;

    return-void
.end method

.method static synthetic a(Lbql;Z)V
    .locals 0

    iput-boolean p1, p0, Lbql;->b:Z

    return-void
.end method

.method private final a(Lbqo;)V
    .locals 4

    :goto_0
    iget v0, p0, Lbql;->h:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lbql;->g:[Lbqo;

    iget v1, p0, Lbql;->h:I

    aget-object v0, v0, v1

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lbql;->g:[Lbqo;

    iget v1, p0, Lbql;->h:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lbqo;->a:Lbqh;

    iget-boolean v1, p0, Lbql;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "StateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "invokeExitMethods: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbqh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Lbqh;->c()V

    iget-object v0, p0, Lbql;->g:[Lbqo;

    iget v1, p0, Lbql;->h:I

    aget-object v0, v0, v1

    const/4 v1, 0x0

    iput-boolean v1, v0, Lbqo;->c:Z

    iget v0, p0, Lbql;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbql;->h:I

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static synthetic b(Lbql;)Lbqg;
    .locals 2

    iget-object v0, p0, Lbql;->g:[Lbqo;

    iget v1, p0, Lbql;->h:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lbqo;->a:Lbqh;

    return-object v0
.end method

.method private final b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbql;->m:Lbqi;

    iget-object v0, v0, Lbqi;->c:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbql;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    iget-object v0, p0, Lbql;->m:Lbqi;

    iput-object v1, v0, Lbqi;->c:Landroid/os/HandlerThread;

    :cond_0
    iget-object v0, p0, Lbql;->m:Lbqi;

    iget-object v0, v0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    :goto_0
    iget-object v0, p0, Lbql;->m:Lbqi;

    iget-object v0, v0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbql;->m:Lbqi;

    iget-object v0, v0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbql;->m:Lbqi;

    iput-object v1, v0, Lbqi;->b:Lbql;

    iput-object v1, p0, Lbql;->m:Lbqi;

    iput-object v1, p0, Lbql;->d:Landroid/os/Message;

    iget-object v0, p0, Lbql;->e:Lbqk;

    invoke-virtual {v0}, Lbqk;->c()V

    iput-object v1, p0, Lbql;->g:[Lbqo;

    iput-object v1, p0, Lbql;->i:[Lbqo;

    iget-object v0, p0, Lbql;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iput-object v1, p0, Lbql;->o:Lbqh;

    iput-object v1, p0, Lbql;->p:Lbqh;

    iget-object v0, p0, Lbql;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbql;->a:Z

    return-void
.end method

.method private final c()V
    .locals 5

    iget-object v0, p0, Lbql;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lbql;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    iget-boolean v2, p0, Lbql;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "StateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "moveDeferredMessageAtFrontOfQueue; what="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lbql;->m:Lbqi;

    invoke-virtual {v2, v0}, Lbqi;->c(Landroid/os/Message;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbql;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method static synthetic c(Lbql;)Z
    .locals 1

    iget-boolean v0, p0, Lbql;->b:Z

    return v0
.end method

.method private final d()I
    .locals 6

    iget v0, p0, Lbql;->h:I

    add-int/lit8 v1, v0, 0x1

    iget v0, p0, Lbql;->j:I

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v0, v1

    :goto_0
    if-ltz v2, :cond_1

    iget-boolean v3, p0, Lbql;->b:Z

    if-eqz v3, :cond_0

    const-string v3, "StateMachine"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "moveTempStackToStateStack: i="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",j="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, p0, Lbql;->g:[Lbqo;

    iget-object v4, p0, Lbql;->i:[Lbqo;

    aget-object v4, v4, v2

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbql;->h:I

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "StateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "moveTempStackToStateStack: X mStateStackTop="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lbql;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",startingIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",Top="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lbql;->g:[Lbqo;

    iget v4, p0, Lbql;->h:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lbqo;->a:Lbqh;

    invoke-virtual {v3}, Lbqh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return v1
.end method

.method static synthetic d(Lbql;)Lbqk;
    .locals 1

    iget-object v0, p0, Lbql;->e:Lbqk;

    return-object v0
.end method

.method private final e()V
    .locals 3

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setupInitialStateStack: E mInitialState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbql;->o:Lbqh;

    invoke-virtual {v2}, Lbqh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lbql;->n:Ljava/util/HashMap;

    iget-object v1, p0, Lbql;->o:Lbqh;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqo;

    const/4 v1, 0x0

    iput v1, p0, Lbql;->j:I

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lbql;->i:[Lbqo;

    iget v2, p0, Lbql;->j:I

    aput-object v0, v1, v2

    iget-object v0, v0, Lbqo;->b:Lbqo;

    iget v1, p0, Lbql;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbql;->j:I

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lbql;->h:I

    invoke-direct {p0}, Lbql;->d()I

    return-void
.end method

.method public static synthetic e(Lbql;)V
    .locals 3

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    const-string v1, "abort:"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lbql;->m:Lbqi;

    const/4 v1, -0x1

    sget-object v2, Lbql;->c:Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lbql;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbqi;->c(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic f(Lbql;)V
    .locals 5

    const/4 v2, 0x0

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    const-string v1, "completeConstruction: E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lbql;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqo;

    move-object v3, v0

    move v0, v2

    :goto_1
    if-eqz v3, :cond_1

    iget-object v3, v3, Lbqo;->b:Lbqo;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    if-ge v1, v0, :cond_5

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "StateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "completeConstruction: maxDepth="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-array v0, v1, [Lbqo;

    iput-object v0, p0, Lbql;->g:[Lbqo;

    new-array v0, v1, [Lbqo;

    iput-object v0, p0, Lbql;->i:[Lbqo;

    invoke-direct {p0}, Lbql;->e()V

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "StateMachine"

    const-string v1, "completeConstruction: X"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method static synthetic g(Lbql;)V
    .locals 3

    iget-object v0, p0, Lbql;->m:Lbqi;

    const/4 v1, -0x2

    sget-object v2, Lbql;->c:Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lbql;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbqi;->c(Landroid/os/Message;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    iget-boolean v0, p0, Lbql;->a:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "StateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage: E msg.what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbql;->m:Lbqi;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lbqi;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lbql;->d:Landroid/os/Message;

    iget-boolean v0, p0, Lbql;->f:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lbql;->a(Landroid/os/Message;)V

    :goto_0
    invoke-direct {p0}, Lbql;->a()V

    iget-boolean v0, p0, Lbql;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "StateMachine"

    const-string v1, "handleMessage: X"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lbql;->m:Lbqi;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbql;->m:Lbqi;

    iget-object v0, v0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbql;->m:Lbqi;

    iget-object v0, v0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbql;->m:Lbqi;

    iget-object v0, v0, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_2
    return-void

    :cond_3
    iget-boolean v0, p0, Lbql;->f:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lbql;->d:Landroid/os/Message;

    iget v0, v0, Landroid/os/Message;->what:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lbql;->d:Landroid/os/Message;

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget-object v1, Lbql;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbql;->f:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbql;->a(I)V

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StateMachine.handleMessage: The start method not called, received msg: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
