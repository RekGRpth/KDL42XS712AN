.class public final Lcqw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/lang/Object;

.field private c:Z

.field private d:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcqy;)Lcqw;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcqw;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqw;->b:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcqy;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object p0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcqw;->d:Ljava/lang/Throwable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcqw;->d:Ljava/lang/Throwable;

    invoke-interface {p1, v0}, Lcqy;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcqw;->a:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcqw;->a:Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcqw;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)Lcqw;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcqw;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcqw;->d:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcqw;->c:Z

    iput-object p1, p0, Lcqw;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcqw;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcqw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqy;

    invoke-interface {v0, p1}, Lcqy;->a(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcqw;->a:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, p0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcqw;
    .locals 1

    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcqw;->a(Ljava/lang/Throwable;)Lcqw;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/Throwable;)Lcqw;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcqw;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcqw;->d:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iput-object p1, p0, Lcqw;->d:Ljava/lang/Throwable;

    iget-object v0, p0, Lcqw;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcqw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqy;

    invoke-interface {v0, p1}, Lcqy;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcqw;->a:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, p0

    goto :goto_0
.end method
