.class public final Lhcv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field private final b:Lgut;

.field private final c:Lgtm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgut;Lgtm;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhcv;->a:Landroid/content/Context;

    iput-object p2, p0, Lhcv;->b:Lgut;

    iput-object p3, p0, Lhcv;->c:Lgtm;

    return-void
.end method

.method static synthetic a(Lhcv;Ljava/lang/String;Ljava/lang/String;Lhgm;Lizs;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 9

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    const/4 v2, 0x1

    :goto_0
    const-string v3, "Data to be encrypted is missing."

    invoke-static {v2, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    move-object/from16 v0, p8

    move-object/from16 v1, p9

    invoke-direct {p0, p2, v0, v1}, Lhcv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "cvn"

    invoke-direct {v2, v3, p6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "card_number"

    invoke-direct {v2, v3, p5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "request_content_type"

    const-string v4, "application/x-protobuf"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "request"

    invoke-static {p4}, Lizs;->a(Lizs;)[B

    move-result-object v4

    invoke-static {v4}, Lbpd;->b([B)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Lhcv;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lhcv;->b:Lgut;

    const/4 v8, 0x4

    move-object v3, p1

    move-object v4, p3

    move/from16 v7, p7

    invoke-virtual/range {v2 .. v8}, Lgut;->a(Ljava/lang/String;Lhgm;Ljava/util/List;Ljava/util/ArrayList;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v2

    invoke-static {p2, v2}, Lhcv;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    return-object v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lhcv;Ljava/lang/String;Ljava/lang/String;Lhgm;[BILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    invoke-direct {p0, p2, p6, p7}, Lhcv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {p2}, Lhcv;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lhcv;->b:Lgut;

    const/4 v6, 0x4

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lgut;->a(Ljava/lang/String;Lhgm;[BLjava/util/ArrayList;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-static {p2, v0}, Lhcv;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "X-API-USER-CHALLENGE-CAPABILITIES"

    const-string v2, "FULL_ADDRESS"

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "session_token"

    invoke-static {v1, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "X-Buy-Flow"

    const-string v2, "INAPP"

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "RISK-CLIENT-DATA"

    iget-object v2, p0, Lhcv;->c:Lgtm;

    invoke-virtual {v2, p2, p3}, Lgtm;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static a(Linv;)V
    .locals 1

    if-eqz p0, :cond_0

    iget-object v0, p0, Linv;->b:Lint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Linv;->b:Lint;

    iget-object v0, v0, Lint;->a:Linu;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 5

    const-string v0, "InAppServerConnection"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Sending request for txnId=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    if-nez p1, :cond_1

    const/4 v0, -0x1

    :goto_0
    const-string v1, "InAppServerConnection"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Got responseType=%d for txnId=%s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object p0, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lipe;

    const-string v1, "InAppServerConnection"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Error type=%d and msg=%s"

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, v0, Lipe;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v0, v0, Lipe;->a:Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    goto :goto_0
.end method
