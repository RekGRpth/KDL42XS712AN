.class public abstract Lbt;
.super Lcb;
.source "SourceFile"


# instance fields
.field volatile a:Lbu;

.field volatile b:Lbu;

.field c:J

.field d:J

.field e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Lcb;-><init>(Landroid/content/Context;)V

    const-wide/16 v0, -0x2710

    iput-wide v0, p0, Lbt;->d:J

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-super {p0}, Lcb;->a()V

    invoke-virtual {p0}, Lbt;->b()Z

    new-instance v0, Lbu;

    invoke-direct {v0, p0}, Lbu;-><init>(Lbt;)V

    iput-object v0, p0, Lbt;->a:Lbu;

    invoke-virtual {p0}, Lbt;->c()V

    return-void
.end method

.method final a(Lbu;Ljava/lang/Object;)V
    .locals 2

    invoke-virtual {p0, p2}, Lbt;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lbt;->b:Lbu;

    if-ne v0, p1, :cond_1

    iget-boolean v0, p0, Lcb;->t:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcb;->s:Z

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbt;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lbt;->b:Lbu;

    invoke-virtual {p0}, Lbt;->c()V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    invoke-super {p0, p1, p2, p3, p4}, Lcb;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    iget-object v0, p0, Lbt;->a:Lbu;

    if-eqz v0, :cond_0

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lbt;->a:Lbu;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lbt;->a:Lbu;

    iget-boolean v0, v0, Lbu;->b:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    :cond_0
    iget-object v0, p0, Lbt;->b:Lbu;

    if-eqz v0, :cond_1

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCancellingTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lbt;->b:Lbu;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lbt;->b:Lbu;

    iget-boolean v0, v0, Lbu;->b:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    :cond_1
    iget-wide v0, p0, Lbt;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUpdateThrottle="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide v0, p0, Lbt;->c:J

    invoke-static {v0, v1, p3}, Ldu;->a(JLjava/io/PrintWriter;)V

    const-string v0, " mLastLoadCompleteTime="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide v0, p0, Lbt;->d:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p3}, Ldu;->a(JJLjava/io/PrintWriter;)V

    invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V

    :cond_2
    return-void
.end method

.method public final b()Z
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lbt;->a:Lbu;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbt;->b:Lbu;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbt;->a:Lbu;

    iget-boolean v1, v1, Lbu;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbt;->a:Lbu;

    iput-boolean v0, v1, Lbu;->b:Z

    iget-object v1, p0, Lbt;->e:Landroid/os/Handler;

    iget-object v2, p0, Lbt;->a:Lbu;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    iput-object v3, p0, Lbt;->a:Lbu;

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lbt;->a:Lbu;

    iget-boolean v1, v1, Lbu;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lbt;->a:Lbu;

    iput-boolean v0, v1, Lbu;->b:Z

    iget-object v1, p0, Lbt;->e:Landroid/os/Handler;

    iget-object v2, p0, Lbt;->a:Lbu;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lbt;->a:Lbu;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lbt;->a:Lbu;

    invoke-virtual {v0}, Lbu;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lbt;->a:Lbu;

    iput-object v1, p0, Lbt;->b:Lbu;

    :cond_4
    iput-object v3, p0, Lbt;->a:Lbu;

    goto :goto_0
.end method

.method final c()V
    .locals 6

    iget-object v0, p0, Lbt;->b:Lbu;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbt;->a:Lbu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbt;->a:Lbu;

    iget-boolean v0, v0, Lbu;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbt;->a:Lbu;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lbu;->b:Z

    iget-object v0, p0, Lbt;->e:Landroid/os/Handler;

    iget-object v1, p0, Lbt;->a:Lbu;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    iget-wide v0, p0, Lbt;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lbt;->d:J

    iget-wide v4, p0, Lbt;->c:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    iget-object v0, p0, Lbt;->a:Lbu;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lbu;->b:Z

    iget-object v0, p0, Lbt;->e:Landroid/os/Handler;

    iget-object v1, p0, Lbt;->a:Lbu;

    iget-wide v2, p0, Lbt;->d:J

    iget-wide v4, p0, Lbt;->c:J

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lbt;->a:Lbu;

    sget-object v1, Lci;->d:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1}, Lbu;->a(Ljava/util/concurrent/Executor;)Lci;

    goto :goto_0
.end method

.method public abstract d()Ljava/lang/Object;
.end method
