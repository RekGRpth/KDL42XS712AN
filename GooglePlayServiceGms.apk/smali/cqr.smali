.class final Lcqr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcqy;


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lcqw;

.field final synthetic c:Lcqq;


# direct methods
.method constructor <init>(Lcqq;Ljava/lang/Object;Lcqw;)V
    .locals 0

    iput-object p1, p0, Lcqr;->c:Lcqq;

    iput-object p2, p0, Lcqr;->a:Ljava/lang/Object;

    iput-object p3, p0, Lcqr;->b:Lcqw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcqr;->c:Lcqq;

    iget-object v0, v0, Lcqq;->b:Lcqo;

    iget-object v1, p0, Lcqr;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcqo;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcqp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    :goto_0
    iget-object v0, p0, Lcqr;->b:Lcqw;

    invoke-virtual {v0, p1}, Lcqw;->a(Ljava/lang/Object;)Lcqw;

    iget-object v0, p0, Lcqr;->c:Lcqq;

    iget-object v1, v0, Lcqq;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcqr;->c:Lcqq;

    iget-object v0, v0, Lcqq;->a:Ljava/util/Map;

    iget-object v2, p0, Lcqr;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "DG"

    const-string v2, "Write to cache failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    iget-object v0, p0, Lcqr;->b:Lcqw;

    invoke-virtual {v0, p1}, Lcqw;->a(Ljava/lang/Throwable;)Lcqw;

    iget-object v0, p0, Lcqr;->c:Lcqq;

    iget-object v1, v0, Lcqq;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcqr;->c:Lcqq;

    iget-object v0, v0, Lcqq;->a:Ljava/util/Map;

    iget-object v2, p0, Lcqr;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
