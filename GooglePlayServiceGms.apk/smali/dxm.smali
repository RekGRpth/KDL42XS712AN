.class public abstract Ldxm;
.super Ldvn;
.source "SourceFile"

# interfaces
.implements Lbic;


# instance fields
.field protected p:Ljava/lang/String;

.field protected q:I

.field public r:Lcom/google/android/gms/games/GameEntity;

.field protected s:Z

.field protected t:Z

.field public u:Ljava/util/ArrayList;

.field private final v:Z

.field private w:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldxm;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ldvn;-><init>()V

    iput-boolean v0, p0, Ldxm;->s:Z

    iput-boolean v0, p0, Ldxm;->t:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldxm;->u:Ljava/util/ArrayList;

    iput-boolean p1, p0, Ldxm;->v:Z

    return-void
.end method

.method private a(Lcom/google/android/gms/games/Game;)V
    .locals 4

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/GameEntity;

    iput-object v0, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    iget-boolean v0, p0, Ldxm;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/GameEntity;->n_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldxm;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    iget-boolean v0, p0, Ldxm;->v:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ldxm;->t:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/GameEntity;->i()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/common/images/ImageManager;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/ImageManager;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/google/android/gms/common/images/ImageManager;->b(Lbic;Landroid/net/Uri;)V

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Ldxm;->u:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Ldxm;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxo;

    iget-object v3, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    invoke-interface {v0, v3}, Ldxo;->a(Lcom/google/android/gms/games/Game;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Z)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljj;->a(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ClientUiFragAct"

    const-string v1, "onImageLoaded(): null drawable!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    iget-object v0, p0, Ldxm;->w:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldxm;->w:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lbbo;)V
    .locals 4

    const/16 v3, 0x2711

    const-string v0, "ClientUiFragAct"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection failed: result = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbbo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const-string v0, "ClientUiFragAct"

    const-string v1, "Developer error."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ldxm;->setResult(I)V

    invoke-virtual {p0}, Ldxm;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    const-string v0, "ClientUiFragAct"

    const-string v1, "License check failed."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x2713

    invoke-virtual {p0, v0}, Ldxm;->setResult(I)V

    invoke-virtual {p0}, Ldxm;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const-string v0, "ClientUiFragAct"

    const-string v1, "Not signed in. To launch Client UI activities, you must be connected to the games service AND signed in."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ldxm;->setResult(I)V

    invoke-virtual {p0}, Ldxm;->finish()V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Ldvn;->a(Lbbo;)V

    goto :goto_0
.end method

.method public final a(Lctn;)V
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Lctn;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-interface {p1}, Lctn;->a()Lctb;

    move-result-object v2

    :try_start_0
    invoke-virtual {p0, v1}, Ldxm;->d(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v2}, Lctb;->b()V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v2}, Lctb;->a()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lctb;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    :cond_1
    if-nez v0, :cond_2

    const-string v0, "ClientUiFragAct"

    const-string v1, "onGamesLoaded: couldn\'t load gameId "

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Lctb;->b()V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-direct {p0, v0}, Ldxm;->a(Lcom/google/android/gms/games/Game;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v2}, Lctb;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lctb;->b()V

    throw v0
.end method

.method public final d(I)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    const/16 v1, 0x2711

    invoke-virtual {p0, v1}, Ldxm;->setResult(I)V

    invoke-virtual {p0}, Ldxm;->finish()V

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x7

    if-ne p1, v1, :cond_1

    const/16 v1, 0x2713

    invoke-virtual {p0, v1}, Ldxm;->setResult(I)V

    invoke-virtual {p0}, Ldxm;->finish()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final f()Lbdu;
    .locals 9

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Ldxm;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Ldab;->a(Landroid/content/Intent;)I

    move-result v3

    iput v3, p0, Ldxm;->q:I

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Ldxm;->p:Ljava/lang/String;

    iget-object v3, p0, Ldxm;->p:Ljava/lang/String;

    if-nez v3, :cond_0

    const-string v1, "ClientUiFragAct"

    const-string v2, "Client UI activities must be started with startActivityForResult"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, Ldxm;->p:Ljava/lang/String;

    const-string v4, "com.google.android.gms"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldxm;->p:Ljava/lang/String;

    iget-object v1, p0, Ldxm;->p:Ljava/lang/String;

    const-string v3, "EXTRA_GAME_PACKAGE_NAME missing when coming from Games UI"

    invoke-static {v1, v3}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Ldxm;->p:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "ClientUiFragAct"

    const-string v2, "EXTRA_GAME_PACKAGE_NAME missing when coming from Games UI"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v4, Lbdw;

    invoke-direct {v4, p0, p0, p0}, Lbdw;-><init>(Landroid/content/Context;Lbdx;Lbdy;)V

    iget-object v0, p0, Ldxm;->p:Ljava/lang/String;

    iput-object v0, v4, Lbdw;->b:Ljava/lang/String;

    invoke-virtual {p0}, Ldxm;->g()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvo;

    iget-object v1, v0, Ldvo;->a:Lbdm;

    iget-object v7, v0, Ldvo;->b:Lbdv;

    invoke-virtual {v4, v1, v7}, Lbdw;->a(Lbdm;Lbdv;)Lbdw;

    iget-object v1, v0, Ldvo;->c:[Lbem;

    array-length v7, v1

    move v1, v2

    :goto_2
    if-ge v1, v7, :cond_2

    iget-object v8, v0, Ldvo;->c:[Lbem;

    aget-object v8, v8, v1

    invoke-virtual {v4, v8}, Lbdw;->a(Lbem;)Lbdw;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v4}, Lbdw;->a()Lbdu;

    move-result-object v0

    goto :goto_0
.end method

.method protected final g()Ljava/util/ArrayList;
    .locals 4

    invoke-super {p0}, Ldvn;->g()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ldvo;

    sget-object v2, Lcte;->c:Lbdm;

    const/4 v3, 0x0

    new-array v3, v3, [Lbem;

    invoke-direct {v1, v2, v3}, Ldvo;-><init>(Lbdm;[Lbem;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final h()V
    .locals 2

    invoke-virtual {p0}, Ldxm;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcte;->d(Lbdu;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x384

    invoke-virtual {p0, v0, v1}, Ldxm;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ClientUiFragAct"

    const-string v1, "onShowSettings: googleApiClient not connected; ignoring menu click"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 4

    invoke-virtual {p0}, Ldxm;->j()Lbdu;

    move-result-object v0

    sget-object v1, Lcte;->f:Lctl;

    invoke-interface {v1, v0}, Lctl;->a(Lbdu;)Lcom/google/android/gms/games/Game;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Ldxm;->a(Lcom/google/android/gms/games/Game;)V

    :goto_0
    iget-object v0, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldxm;->j()Lbdu;

    move-result-object v0

    invoke-static {v0}, Lcte;->b(Lbdu;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldxm;->p:Ljava/lang/String;

    iget-object v2, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/GameEntity;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ldxm;->p()I

    move-result v3

    invoke-static {p0, v1, v2, v0, v3}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    return-void

    :cond_1
    sget-object v1, Lcte;->f:Lctl;

    invoke-interface {v1, v0}, Lctl;->b(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Ldxn;

    invoke-direct {v1, p0}, Ldxn;-><init>(Ldxm;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final m()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final o()Lcom/google/android/gms/games/GameEntity;
    .locals 1

    iget-object v0, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/16 v3, 0x2711

    const/4 v2, -0x1

    const/16 v0, 0x384

    if-ne p1, v0, :cond_0

    if-ne p2, v3, :cond_0

    const-string v0, "ClientUiFragAct"

    const-string v1, "onActivityResult: Reconnect required."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ldxm;->setResult(I)V

    invoke-virtual {p0}, Ldxm;->finish()V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    if-ne p2, v2, :cond_1

    const-string v0, "ClientUiFragAct"

    const-string v1, "onActivityResult: RESULT_OK received from a public items UI. Forwarding..."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2, p3}, Ldxm;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Ldxm;->finish()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Ldvn;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Ldvn;->onCreate(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Ldxm;->v:Z

    if-eqz v0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ldxm;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Ldxm;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0066    # com.google.android.gms.R.dimen.actionbar_app_icon_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljj;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljj;->b(Z)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljj;->a(Z)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Ldvn;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Ldxm;->onBackPressed()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Ldxm;->h()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0    # android.R.id.home
        0x7f0a037d -> :sswitch_1    # com.google.android.gms.R.id.menu_settings
    .end sparse-switch
.end method

.method protected abstract p()I
.end method

.method public final q()I
    .locals 1

    iget v0, p0, Ldxm;->q:I

    return v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldxm;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final s()V
    .locals 2

    iget-object v0, p0, Ldxm;->w:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldxm;->w:Ljava/util/ArrayList;

    :goto_0
    return-void

    :cond_0
    const-string v0, "ClientUiFragAct"

    const-string v1, "trackFragments: should only be called once per activity"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final t()Z
    .locals 3

    iget-object v0, p0, Ldxm;->w:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldxm;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->T_()Lo;

    move-result-object v2

    if-ne v2, p0, :cond_0

    instance-of v2, v0, Ldxd;

    if-eqz v2, :cond_0

    check-cast v0, Ldxd;

    invoke-interface {v0}, Ldxd;->d()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const-string v0, "ClientUiFragAct"

    const-string v1, "onFragmentSearchRequested: need to call trackFragments first"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
