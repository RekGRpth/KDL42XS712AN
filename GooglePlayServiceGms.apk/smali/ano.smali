.class public final Lano;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Z

.field public final b:Lanr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljci;->a()Z

    move-result v0

    iput-boolean v0, p0, Lano;->a:Z

    new-instance v0, Lanr;

    invoke-direct {v0, p1}, Lanr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lano;->b:Lanr;

    const-string v0, "AuthZen"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AuthZen"

    const-string v1, "Initiate PRNG security fix..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lblx;->a()V

    return-void
.end method

.method private c(Ljava/lang/String;)Ljava/security/KeyPair;
    .locals 6

    const/4 v5, 0x1

    :try_start_0
    iget-object v0, p0, Lano;->b:Lanr;

    invoke-virtual {v0, p1}, Lanr;->a(Ljava/lang/String;)Lanq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lanq;->b:Ljava/security/KeyPair;
    :try_end_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "AuthZen"

    const-string v2, "Unable to parse stored key. Deleating the old key."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lano;->b:Lanr;

    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Deleting SigningKey for handle: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, v0, Lanr;->a:Lans;

    invoke-virtual {v0}, Lans;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_1
    const-string v0, "signingkeys"

    const-string v2, "key_handle = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    if-ne v0, v5, :cond_1

    const-string v0, "AuthZen"

    const-string v2, "SigningKey deleted."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_2
    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected number of rows deleted: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v0
.end method


# virtual methods
.method public final a([B)Lanj;
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lano;->b:Lanr;

    invoke-virtual {v0, p1}, Lanr;->a([B)Lanj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/security/KeyPair;
    .locals 7

    const/4 v0, 0x1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, p1}, Lano;->c(Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v2

    :cond_0
    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Creating new signing Keys for handle: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lano;->a:Z

    if-eqz v1, :cond_1

    invoke-static {}, Ljcs;->b()Ljava/security/KeyPair;

    move-result-object v2

    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    const/16 v4, 0x19

    invoke-virtual {v1, v0, v4}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    const-string v1, "handle cannot be empty or null"

    invoke-static {p1, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "keyPair cannot be null"

    invoke-static {v2, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    cmp-long v1, v5, v3

    if-lez v1, :cond_2

    :goto_2
    const-string v1, "expiration time must be greater than creation time"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lanq;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lanq;-><init>(Ljava/lang/String;Ljava/security/KeyPair;JJ)V

    iget-object v1, p0, Lano;->b:Lanr;

    invoke-virtual {v1, v0}, Lanr;->a(Lanq;)Lanq;

    goto :goto_0

    :cond_1
    invoke-static {}, Ljcs;->a()Ljava/security/KeyPair;

    move-result-object v2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)Lanj;
    .locals 6

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lano;->b:Lanr;

    invoke-virtual {v0, p1}, Lanr;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanj;

    iget-object v2, v0, Lanj;->a:Lank;

    iget-wide v2, v2, Lank;->d:J

    iget-object v4, v0, Lanj;->a:Lank;

    iget-wide v4, v4, Lank;->c:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x1d4c0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
