.class public final Lfx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lga;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    new-instance v0, Lgb;

    invoke-direct {v0}, Lgb;-><init>()V

    sput-object v0, Lfx;->a:Lga;

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    new-instance v0, Lfz;

    invoke-direct {v0}, Lfz;-><init>()V

    sput-object v0, Lfx;->a:Lga;

    goto :goto_0

    :cond_1
    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    new-instance v0, Lfy;

    invoke-direct {v0}, Lfy;-><init>()V

    sput-object v0, Lfx;->a:Lga;

    goto :goto_0

    :cond_2
    new-instance v0, Lgc;

    invoke-direct {v0}, Lgc;-><init>()V

    sput-object v0, Lfx;->a:Lga;

    goto :goto_0
.end method

.method public static a(Landroid/view/ViewGroup;)V
    .locals 2

    sget-object v0, Lfx;->a:Lga;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lga;->a(Landroid/view/ViewGroup;Z)V

    return-void
.end method
