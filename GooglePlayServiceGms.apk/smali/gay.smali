.class public final Lgay;
.super Lftf;
.source "SourceFile"


# instance fields
.field private final a:Lgaz;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:Lgax;

.field private final e:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lgax;[Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Lftf;-><init>()V

    iput-object p1, p0, Lgay;->b:Landroid/content/Context;

    iput-object p3, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p4, p0, Lgay;->d:Lgax;

    iput-object p5, p0, Lgay;->e:[Ljava/lang/String;

    new-instance v0, Lgaz;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lgaz;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lgax;[Ljava/lang/String;)V

    iput-object v0, p0, Lgay;->a:Lgaz;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgay;->a:Lgaz;

    invoke-virtual {v0}, Lgaz;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lftb;)V
    .locals 2

    iget-object v0, p0, Lgay;->a:Lgaz;

    new-instance v1, Lgaw;

    invoke-direct {v1, p1}, Lgaw;-><init>(Lftb;)V

    invoke-virtual {v0, v1}, Lgaz;->a(Lfsy;)V

    return-void
.end method

.method public final a(Lftb;IILjava/lang/String;)V
    .locals 7

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->b()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v6, p0, Lgay;->b:Landroid/content/Context;

    new-instance v0, Lgcn;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lgcn;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;Lftb;)V

    invoke-static {v6, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;ILjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgcg;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lgcg;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lgay;->a:Lgaz;

    new-instance v1, Lgaw;

    invoke-direct {v1, p1}, Lgaw;-><init>(Lftb;)V

    invoke-virtual {v0, v1, p2, p3}, Lgaz;->a(Lfsy;Landroid/net/Uri;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lftb;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 3

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgbr;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1}, Lgbr;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    new-instance v0, Lgbz;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1, p2}, Lgbz;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lftb;Lcom/google/android/gms/plus/model/posts/Comment;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    new-instance v0, Lgby;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1, p2}, Lgby;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lgay;->a:Lgaz;

    new-instance v1, Lgaw;

    invoke-direct {v1, p1}, Lgaw;-><init>(Lftb;)V

    invoke-virtual {v0, v1, p2}, Lgaz;->a(Lfsy;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The appId parameter is required."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lgcb;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lgcb;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;Lftb;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lftb;Ljava/lang/String;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 9

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    new-instance v0, Lgcc;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v6, "me"

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v7, p2

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lgcc;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lftb;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 3

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgct;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lgct;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Lftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 3

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgcx;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lgcx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;Lftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lgay;->a:Lgaz;

    new-instance v1, Lgaw;

    invoke-direct {v1, p1}, Lgaw;-><init>(Lftb;)V

    invoke-virtual {v0, v1, p2, p3}, Lgaz;->a(Lfsy;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 9

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v8, p0, Lgay;->b:Landroid/content/Context;

    new-instance v0, Lgbs;

    iget-object v6, p0, Lgay;->e:[Ljava/lang/String;

    sget-object v7, Lgau;->a:Lgau;

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lgbs;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lftb;[Ljava/lang/String;Lgau;)V

    invoke-static {v8, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 7

    if-eqz p5, :cond_0

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgbn;

    invoke-direct {v1, p5}, Lgbn;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    :cond_0
    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v6, p0, Lgay;->b:Landroid/content/Context;

    new-instance v0, Lgbp;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lgbp;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLftb;)V

    invoke-static {v6, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 7

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v6, p0, Lgay;->b:Landroid/content/Context;

    new-instance v0, Lgcs;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lgcs;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;ZLftb;)V

    invoke-static {v6, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    if-eqz p4, :cond_0

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgbn;

    invoke-direct {v1, p4}, Lgbn;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    :cond_0
    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgbp;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lgbp;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lftb;ZZ)V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not implemented."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgcw;

    invoke-direct {v1, p1, p2}, Lgcw;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final b(Lftb;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lgbt;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1}, Lgbt;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lftb;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final b(Lftb;IILjava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lgce;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lgce;-><init>(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;Lftb;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final b(Lftb;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 3

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgcv;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1}, Lgcv;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final b(Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgcm;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p1, p2}, Lgcm;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final b(Lftb;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lgay;->a:Lgaz;

    new-instance v1, Lgaw;

    invoke-direct {v1, p1}, Lgaw;-><init>(Lftb;)V

    invoke-virtual {v0, v1, p2}, Lgaz;->b(Lfsy;Ljava/lang/String;)V

    return-void
.end method

.method public final c(Lftb;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 3

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgcu;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1}, Lgcu;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final c(Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgcl;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p1, p2}, Lgcl;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final c(Lftb;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    new-instance v0, Lgci;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lgci;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lftb;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final d(Lftb;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgcq;

    invoke-direct {v1, p2, p1}, Lgcq;-><init>(Ljava/lang/String;Lftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final e(Lftb;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lgay;->d:Lgax;

    iget-object v0, p0, Lgay;->b:Landroid/content/Context;

    new-instance v1, Lgbq;

    iget-object v2, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1}, Lgbq;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lftb;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final f(Lftb;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The momentId parameter is required."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lgco;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lgco;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lftb;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Lftb;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The momentId parameter is required."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lgcj;

    iget-object v1, p0, Lgay;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lgcj;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lftb;)V

    iget-object v1, p0, Lgay;->d:Lgax;

    iget-object v1, p0, Lgay;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
