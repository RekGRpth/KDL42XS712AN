.class public final Lecl;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbel;
.implements Ldle;
.implements Lebh;


# instance fields
.field private ad:Lebf;

.field private ae:Ldxi;

.field private af:Ldwp;

.field private ag:Lebh;

.field private ah:Ljava/lang/String;

.field private ai:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxe;-><init>()V

    return-void
.end method

.method private V()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lecl;->ad:Lebf;

    invoke-virtual {v0}, Lebf;->d()I

    move-result v3

    iget-object v4, p0, Lecl;->ae:Ldxi;

    if-nez v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ldxi;->b(Z)V

    iget v0, p0, Lecl;->ai:I

    if-ne v0, v1, :cond_0

    iget-object v4, p0, Lecl;->af:Ldwp;

    if-le v3, v1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Ldwp;->a(Z)V

    :cond_0
    iget-object v0, p0, Lecl;->af:Ldwp;

    if-lez v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method private W()V
    .locals 5

    invoke-virtual {p0}, Lecl;->J()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "RequestListFrag"

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lecl;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecl;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->b()V

    sget-object v0, Lcte;->o:Ldlf;

    iget-object v2, p0, Lecl;->ah:Ljava/lang/String;

    invoke-virtual {p0}, Lecl;->N()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lecl;->ai:I

    invoke-interface {v0, v1, v2, v3, v4}, Ldlf;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Lecl;
    .locals 3

    invoke-static {p0}, Lbiq;->a(Ljava/lang/Object;)V

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Request type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lecl;

    invoke-direct {v0}, Lecl;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "external_game_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "request_type"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lecl;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final F_()V
    .locals 0

    invoke-direct {p0}, Lecl;->W()V

    return-void
.end method

.method public final G_()V
    .locals 0

    invoke-direct {p0}, Lecl;->W()V

    return-void
.end method

.method public final M_()V
    .locals 3

    invoke-super {p0}, Ldxe;->M_()V

    invoke-virtual {p0}, Lecl;->K()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "RequestListFrag"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lecl;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->o:Ldlf;

    iget-object v2, p0, Lecl;->ah:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ldlf;->b(Lbdu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final R()V
    .locals 0

    invoke-direct {p0}, Lecl;->W()V

    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040066    # com.google.android.gms.R.layout.games_inbox_list_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;)V
    .locals 4

    iget-object v0, p0, Lecl;->ad:Lebf;

    invoke-virtual {p0}, Lecl;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lebf;->a(Ljava/lang/String;)V

    sget-object v0, Lcte;->o:Ldlf;

    iget-object v1, p0, Lecl;->ah:Ljava/lang/String;

    invoke-virtual {p0}, Lecl;->N()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lecl;->ai:I

    invoke-interface {v0, p1, v1, v2, v3}, Ldlf;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    return-void
.end method

.method public final synthetic a(Lbek;)V
    .locals 4

    check-cast p1, Ldlh;

    invoke-interface {p1}, Ldlh;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    iget v0, p0, Lecl;->ai:I

    invoke-interface {p1, v0}, Ldlh;->a(I)Ldkz;

    move-result-object v2

    :try_start_0
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {v2}, Ldkz;->b()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lecl;->Y:Ldvn;

    invoke-virtual {v0, v1}, Ldvn;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, Ldkz;->b()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v0, p0, Lecl;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecl;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    new-instance v0, Leax;

    invoke-direct {v0, v2}, Leax;-><init>(Lbgo;)V

    iget-object v3, p0, Lecl;->ad:Lebf;

    invoke-virtual {v3, v0}, Lebf;->a(Leax;)V

    invoke-virtual {v2}, Ldkz;->a()I

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v1}, Leee;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lecl;->Z:Leds;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Leds;->a(I)V

    :goto_1
    invoke-direct {p0}, Lecl;->V()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v2}, Ldkz;->b()V

    invoke-virtual {p0}, Lecl;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcte;->o:Ldlf;

    iget-object v2, p0, Lecl;->ah:Ljava/lang/String;

    invoke-interface {v1, v0, p0, v2}, Ldlf;->a(Lbdu;Ldle;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    :try_start_3
    iget-object v0, p0, Lecl;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ldkz;->b()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 1

    iget-object v0, p0, Lecl;->ag:Lebh;

    invoke-interface {v0, p1}, Lebh;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V

    invoke-direct {p0}, Lecl;->V()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lecl;->ag:Lebh;

    invoke-interface {v0, p1, p2}, Lebh;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    iget-object v0, p0, Lecl;->ag:Lebh;

    invoke-interface {v0, p1}, Lebh;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    invoke-direct {p0}, Lecl;->V()V

    return-void
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    iget-object v0, p0, Lecl;->ag:Lebh;

    invoke-interface {v0, p1}, Lebh;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    return-void
.end method

.method public final b_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lecl;->ag:Lebh;

    invoke-interface {v0, p1}, Lebh;->b_(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 10

    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "external_game_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Must specify an external game id!"

    invoke-static {v1, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    const-string v1, "external_game_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lecl;->ah:Ljava/lang/String;

    const-string v1, "request_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Must specify a request type!"

    invoke-static {v1, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    const-string v1, "request_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lecl;->ai:I

    new-instance v0, Ldwp;

    iget-object v1, p0, Lecl;->Y:Ldvn;

    invoke-direct {v0, v1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lecl;->af:Ldwp;

    iget v0, p0, Lecl;->ai:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lecl;->ai:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lecl;->af:Ldwp;

    const v1, 0x7f0b02dc    # com.google.android.gms.R.string.games_request_inbox_header_gifts

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Lecl;->af:Ldwp;

    const v1, 0x7f0b02de    # com.google.android.gms.R.string.games_request_inbox_header_open_all_button

    const-string v2, "openAllButton"

    invoke-virtual {v0, p0, v1, v2}, Ldwp;->a(Landroid/view/View$OnClickListener;ILjava/lang/Object;)V

    iget-object v0, p0, Lecl;->af:Ldwp;

    invoke-virtual {v0, v9}, Ldwp;->a(Z)V

    :goto_0
    new-instance v0, Lebf;

    iget-object v1, p0, Lecl;->Y:Ldvn;

    invoke-direct {v0, v1, p0}, Lebf;-><init>(Ldvn;Lebh;)V

    iput-object v0, p0, Lecl;->ad:Lebf;

    iget-object v0, p0, Lecl;->Y:Ldvn;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecl;->Y:Ldvn;

    instance-of v0, v0, Lebe;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecl;->Y:Ldvn;

    check-cast v0, Lebe;

    invoke-interface {v0}, Lebe;->v()Lebd;

    move-result-object v0

    iput-object v0, p0, Lecl;->ag:Lebh;

    iget-object v0, p0, Lecl;->ag:Lebh;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v0, Ldxi;

    iget-object v1, p0, Lecl;->Y:Ldvn;

    const v2, 0x7f02017b    # com.google.android.gms.R.drawable.illo_gifts

    const v3, 0x7f0b02df    # com.google.android.gms.R.string.games_request_inbox_null_state_text

    const/4 v8, 0x5

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v8}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lecl;->ae:Ldxi;

    iget-object v0, p0, Lecl;->ae:Ldxi;

    invoke-virtual {v0, v4}, Ldxi;->b(Z)V

    new-instance v0, Ldwu;

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    iget-object v2, p0, Lecl;->af:Ldwp;

    aput-object v2, v1, v4

    iget-object v2, p0, Lecl;->ad:Lebf;

    aput-object v2, v1, v9

    const/4 v2, 0x2

    iget-object v3, p0, Lecl;->ae:Ldxi;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0, v0}, Lecl;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lecl;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lecl;->af:Ldwp;

    const v1, 0x7f0b02dd    # com.google.android.gms.R.string.games_request_inbox_header_wishes

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Lecl;->af:Ldwp;

    invoke-virtual {v0, v4}, Ldwp;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lecl;->ad:Lebf;

    invoke-virtual {v0}, Lebf;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    const-string v1, "openAllButton"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecl;->ad:Lebf;

    invoke-virtual {v0}, Lebf;->e()Lbgo;

    move-result-object v0

    invoke-static {v0}, Leay;->a(Lbgo;)[Lcom/google/android/gms/games/request/GameRequest;

    move-result-object v0

    iget-object v1, p0, Lecl;->ag:Lebh;

    invoke-interface {v1, v0}, Lebh;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "RequestListFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
