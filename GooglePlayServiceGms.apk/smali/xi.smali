.class public final Lxi;
.super Lxk;
.source "SourceFile"


# instance fields
.field private final a:Ltz;

.field private final b:Luf;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Lxk;-><init>()V

    invoke-static {p1, p2}, Lud;->a(Ljava/lang/String;Landroid/content/Context;)Lud;

    move-result-object v0

    iput-object v0, p0, Lxi;->a:Ltz;

    new-instance v0, Luf;

    iget-object v1, p0, Lxi;->a:Ltz;

    invoke-direct {v0, v1}, Luf;-><init>(Ltz;)V

    iput-object v0, p0, Lxi;->b:Luf;

    return-void
.end method


# virtual methods
.method public final a(Lcrv;Lcrv;)Lcrv;
    .locals 3

    :try_start_0
    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {p2}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lxi;->b:Luf;

    invoke-virtual {v2, v0, v1}, Luf;->a(Landroid/net/Uri;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;
    :try_end_0
    .catch Lug; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "ms"

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lxi;->b:Luf;

    invoke-virtual {v0, p1}, Luf;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lxi;->b:Luf;

    invoke-virtual {v0, p1, p2}, Luf;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcrv;)Z
    .locals 2

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p0, Lxi;->b:Luf;

    invoke-virtual {v1, v0}, Luf;->a(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcrv;)Z
    .locals 2

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p0, Lxi;->b:Luf;

    invoke-virtual {v1, v0}, Luf;->b(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcrv;)Ljava/lang/String;
    .locals 2

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lxi;->a:Ltz;

    invoke-interface {v1, v0}, Ltz;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
