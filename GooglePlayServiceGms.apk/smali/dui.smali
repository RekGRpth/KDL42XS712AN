.class public final Ldui;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:[B

.field private final g:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldui;->b:Ldad;

    iput-object p3, p0, Ldui;->c:Ljava/lang/String;

    iput-object p4, p0, Ldui;->d:Ljava/lang/String;

    iput-object p5, p0, Ldui;->e:Ljava/lang/String;

    if-nez p6, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Ldui;->f:[B

    :goto_0
    iput-object p7, p0, Ldui;->g:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;

    return-void

    :cond_0
    array-length v0, p6

    new-array v0, v0, [B

    iput-object v0, p0, Ldui;->f:[B

    iget-object v0, p0, Ldui;->f:[B

    array-length v1, p6

    invoke-static {p6, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldui;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->o(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    iget-object v2, p0, Ldui;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldui;->c:Ljava/lang/String;

    iget-object v4, p0, Ldui;->d:Ljava/lang/String;

    iget-object v5, p0, Ldui;->e:Ljava/lang/String;

    iget-object v6, p0, Ldui;->f:[B

    iget-object v7, p0, Ldui;->g:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
