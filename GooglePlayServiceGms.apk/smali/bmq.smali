.class public final Lbmq;
.super Ltf;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 6

    if-nez p3, :cond_1

    const/4 v3, 0x0

    :goto_0
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ltf;-><init>(ILjava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    iput-object p6, p0, Lbmq;->f:Ljava/lang/String;

    iput-object p7, p0, Lbmq;->g:Ljava/util/HashMap;

    iget-object v0, p0, Lbmq;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbmq;->g:Ljava/util/HashMap;

    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OAuth "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbmq;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lbmq;->g:Ljava/util/HashMap;

    const-string v1, "Accept-Encoding"

    const-string v2, "gzip"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lrs;

    const/16 v1, 0x2710

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Lrs;-><init>(IIF)V

    iput-object v0, p0, Lsc;->d:Lsm;

    return-void

    :cond_1
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lrz;)Lsi;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, v0}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lbmq;->g:Ljava/util/HashMap;

    return-object v0
.end method
