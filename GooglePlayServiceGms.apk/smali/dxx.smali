.class public final Ldxx;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Lbel;
.implements Ldgp;
.implements Leag;


# instance fields
.field private ad:Leaf;

.field private ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

.field private af:Landroid/widget/ListView;

.field private ag:Leag;

.field private ah:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxe;-><init>()V

    return-void
.end method

.method private V()V
    .locals 3

    invoke-virtual {p0}, Ldxx;->J()Lbdu;

    move-result-object v1

    iget-object v0, p0, Ldxx;->Y:Ldvn;

    iget-object v2, p0, Ldxx;->Y:Ldvn;

    invoke-virtual {v2}, Ldvn;->l()Z

    move-result v2

    invoke-static {v1, v0, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldxx;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxx;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->b()V

    sget-object v0, Lcte;->j:Ldgs;

    sget-object v2, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a:[I

    invoke-interface {v0, v1, v2}, Ldgs;->a(Lbdu;[I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method public static d(I)Ldxx;
    .locals 3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Match type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ldxx;

    invoke-direct {v0}, Ldxx;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "match_type"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ldxx;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final C_()V
    .locals 0

    invoke-direct {p0}, Ldxx;->V()V

    return-void
.end method

.method public final M_()V
    .locals 2

    invoke-super {p0}, Ldxe;->M_()V

    invoke-virtual {p0}, Ldxx;->T()V

    invoke-virtual {p0}, Ldxx;->K()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "MatchListFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Ldxx;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {v1, v0}, Ldgs;->a(Lbdu;)V

    goto :goto_0
.end method

.method public final R()V
    .locals 0

    invoke-direct {p0}, Ldxx;->V()V

    return-void
.end method

.method protected final U()V
    .locals 0

    invoke-direct {p0}, Ldxx;->V()V

    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040066    # com.google.android.gms.R.layout.games_inbox_list_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    invoke-super {p0, p1, p2, p3}, Ldxe;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0163    # com.google.android.gms.R.id.quick_access_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iput-object v0, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v0, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a()V

    const v0, 0x102000a    # android.R.id.list

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Ldxx;->af:Landroid/widget/ListView;

    return-object v1
.end method

.method public final a(Lbdu;)V
    .locals 3

    iget-object v0, p0, Ldxx;->ad:Leaf;

    sget-object v1, Lcte;->m:Lctw;

    invoke-interface {v1, p1}, Lctw;->a(Lbdu;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcte;->b(Lbdu;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Leaf;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcte;->j:Ldgs;

    sget-object v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a:[I

    invoke-interface {v0, p1, v1}, Ldgs;->a(Lbdu;[I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    invoke-virtual {p0}, Ldxx;->S()V

    sget-object v0, Lcte;->n:Lctp;

    const/4 v1, 0x2

    invoke-interface {v0, p1, v1}, Lctp;->a(Lbdu;I)V

    return-void
.end method

.method public final synthetic a(Lbek;)V
    .locals 6

    check-cast p1, Ldgu;

    invoke-interface {p1}, Ldgu;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-interface {p1}, Ldgu;->g()Ldgo;

    move-result-object v2

    :try_start_0
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {v2}, Ldgo;->a()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Ldxx;->Y:Ldvn;

    invoke-virtual {v0, v1}, Ldvn;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, Ldgo;->a()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v0, p0, Ldxx;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxx;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    iget-object v0, v2, Ldgo;->a:Ldfu;

    invoke-virtual {v0}, Ldfu;->a()I

    move-result v0

    iget-object v3, v2, Ldgo;->b:Ldgq;

    invoke-virtual {v3}, Ldgq;->a()I

    move-result v3

    iget-object v4, v2, Ldgo;->c:Ldgq;

    invoke-virtual {v4}, Ldgq;->a()I

    move-result v4

    iget-object v5, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v5, v0, v3, v4}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(III)V

    iget-object v0, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-static {v0}, Leeh;->a(Landroid/view/View;)V

    iget-object v0, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v3, p0, Ldxx;->af:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Landroid/widget/ListView;)V

    iget v0, p0, Ldxx;->ah:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Match type "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Ldxx;->ah:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is invalid"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ldgo;->a()V

    throw v0

    :pswitch_0
    :try_start_3
    iget-object v0, v2, Ldgo;->a:Ldfu;

    invoke-virtual {v0}, Ldfu;->b()V

    iget-object v0, v2, Ldgo;->b:Ldgq;

    iget-object v3, v2, Ldgo;->c:Ldgq;

    invoke-virtual {v3}, Ldgq;->b()V

    iget-object v3, v2, Ldgo;->d:Ldgq;

    invoke-virtual {v3}, Ldgq;->b()V

    :goto_1
    iget-object v3, p0, Ldxx;->ad:Leaf;

    invoke-virtual {v3, v0}, Leaf;->a(Lbgo;)V

    invoke-virtual {v0}, Ldgq;->a()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Ldxx;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    invoke-virtual {p0}, Ldxx;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {v1, v0, p0}, Ldgs;->a(Lbdu;Ldgp;)V

    goto/16 :goto_0

    :pswitch_1
    :try_start_4
    iget-object v0, v2, Ldgo;->a:Ldfu;

    invoke-virtual {v0}, Ldfu;->b()V

    iget-object v0, v2, Ldgo;->b:Ldgq;

    invoke-virtual {v0}, Ldgq;->b()V

    iget-object v0, v2, Ldgo;->c:Ldgq;

    iget-object v3, v2, Ldgo;->d:Ldgq;

    invoke-virtual {v3}, Ldgq;->b()V

    goto :goto_1

    :pswitch_2
    iget-object v0, v2, Ldgo;->a:Ldfu;

    invoke-virtual {v0}, Ldfu;->b()V

    iget-object v0, v2, Ldgo;->b:Ldgq;

    invoke-virtual {v0}, Ldgq;->b()V

    iget-object v0, v2, Ldgo;->c:Ldgq;

    invoke-virtual {v0}, Ldgq;->b()V

    iget-object v0, v2, Ldgo;->d:Ldgq;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Ldxx;->Z:Leds;

    const v3, 0x7f0b02c8    # com.google.android.gms.R.string.games_match_inbox_list_generic_error

    const v4, 0x7f0b02c7    # com.google.android.gms.R.string.games_match_inbox_list_null_state

    invoke-virtual {v0, v1, v3, v4}, Leds;->a(III)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    iget-object v0, p0, Ldxx;->ag:Leag;

    invoke-interface {v0, p1}, Leag;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Ldxx;->ag:Leag;

    invoke-interface {v0, p1, p2, p3}, Leag;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    iget-object v0, p0, Ldxx;->ag:Leag;

    invoke-interface {v0, p1}, Leag;->b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    iget-object v0, p0, Ldxx;->ad:Leaf;

    invoke-virtual {v0}, Leaf;->d()I

    move-result v1

    iget-object v2, p0, Ldxx;->Z:Leds;

    if-lez v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v2, v0}, Leds;->a(I)V

    iget v0, p0, Ldxx;->ah:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Match type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldxx;->ah:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v2, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->b()I

    move-result v2

    iget-object v3, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->d()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(III)V

    :goto_1
    :pswitch_1
    return-void

    :pswitch_2
    iget-object v0, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v2, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->b()I

    move-result v2

    iget-object v3, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->c()I

    move-result v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(III)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    iget-object v0, p0, Ldxx;->ag:Leag;

    invoke-interface {v0, p1}, Leag;->c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    new-instance v0, Leaf;

    iget-object v1, p0, Ldxx;->Y:Ldvn;

    invoke-direct {v0, v1, p0}, Leaf;-><init>(Ldvn;Leag;)V

    iput-object v0, p0, Ldxx;->ad:Leaf;

    iget-object v0, p0, Ldxx;->Y:Ldvn;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxx;->Y:Ldvn;

    instance-of v0, v0, Ldzy;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxx;->Y:Ldvn;

    check-cast v0, Ldzy;

    invoke-interface {v0}, Ldzy;->D_()Ldzx;

    move-result-object v0

    iput-object v0, p0, Ldxx;->ag:Leag;

    iget-object v1, p0, Ldxx;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v0, p0, Ldxx;->Y:Ldvn;

    check-cast v0, Ldwt;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Ldwt;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "match_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Must specify a match type!"

    invoke-static {v1, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    const-string v1, "match_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldxx;->ah:I

    iget-object v0, p0, Ldxx;->ad:Leaf;

    invoke-virtual {p0, v0}, Ldxx;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Ldxx;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method public final e()V
    .locals 0

    invoke-direct {p0}, Ldxx;->V()V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Ldxx;->ad:Leaf;

    invoke-virtual {v0}, Leaf;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method
