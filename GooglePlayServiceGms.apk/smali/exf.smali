.class public final Lexf;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V
    .locals 8

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lsf;

    move-result-object v7

    new-instance v6, Ljdm;

    invoke-direct {v6}, Ljdm;-><init>()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p2, v6, Ljdm;->b:Ljava/lang/String;

    :cond_0
    iput p0, v6, Ljdm;->c:I

    if-eqz p1, :cond_1

    new-instance v0, Ljdj;

    invoke-direct {v0}, Ljdj;-><init>()V

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    iput v1, v0, Ljdj;->c:F

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    iput-wide v1, v0, Ljdj;->b:D

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    iput-wide v1, v0, Ljdj;->a:D

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    iput-wide v1, v0, Ljdj;->e:J

    iput-object v0, v6, Ljdm;->a:Ljdj;

    :cond_1
    if-eqz p3, :cond_2

    iput-object p3, v6, Ljdm;->e:Ljdk;

    :cond_2
    new-instance v0, Lexg;

    sget-object v1, Lexh;->a:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    const-class v5, Ljdn;

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, Lexg;-><init>(Ljava/lang/String;ZLsk;Lsj;Ljava/lang/Class;Lizs;)V

    invoke-virtual {v7, v0}, Lsf;->a(Lsc;)Lsc;

    return-void
.end method
