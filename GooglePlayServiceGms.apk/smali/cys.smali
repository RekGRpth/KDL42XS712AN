.class final Lcys;
.super Lcwk;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcwm;

.field private final b:Ldgm;

.field private final c:Ldgl;

.field private final d:Ldgg;


# direct methods
.method public constructor <init>(Lcwm;Ldgm;Ldgl;)V
    .locals 1

    iput-object p1, p0, Lcys;->a:Lcwm;

    invoke-direct {p0}, Lcwk;-><init>()V

    const-string v0, "Callbacks must not be null"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgm;

    iput-object v0, p0, Lcys;->b:Ldgm;

    iput-object p3, p0, Lcys;->c:Ldgl;

    const/4 v0, 0x0

    iput-object v0, p0, Lcys;->d:Ldgg;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcya;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1, p2}, Lcya;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcxq;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->d:Ldgg;

    invoke-direct {v1, v2, v3, p1}, Lcxq;-><init>(Lcwm;Ldgg;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcyb;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1, p2}, Lcyb;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final c(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcyc;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1, p2}, Lcyc;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final d(ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcxm;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->b:Ldgm;

    invoke-direct {v1, v2, v3, p1, p2}, Lcxm;-><init>(Lcwm;Ldgm;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final d(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcxy;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1, p2}, Lcxy;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcxv;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1}, Lcxv;-><init>(Lcwm;Ldgl;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final e(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcxx;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1, p2}, Lcxx;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcxw;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1}, Lcxw;-><init>(Lcwm;Ldgl;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final f(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcxz;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1, p2}, Lcxz;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final r(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcyv;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->b:Ldgm;

    invoke-direct {v1, v2, v3, p1}, Lcyv;-><init>(Lcwm;Ldgm;Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final s(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcxh;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->b:Ldgm;

    invoke-direct {v1, v2, v3, p1}, Lcxh;-><init>(Lcwm;Ldgm;Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final t(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcyu;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1}, Lcyu;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final u(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcyr;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1}, Lcyr;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final v(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcyt;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->b:Ldgm;

    invoke-direct {v1, v2, v3, p1}, Lcyt;-><init>(Lcwm;Ldgm;Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final w(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcws;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1}, Lcws;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method

.method public final x(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Lcys;->a:Lcwm;

    new-instance v1, Lcwx;

    iget-object v2, p0, Lcys;->a:Lcwm;

    iget-object v3, p0, Lcys;->c:Ldgl;

    invoke-direct {v1, v2, v3, p1}, Lcwx;-><init>(Lcwm;Ldgl;Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcwm;->a(Lbjg;)V

    return-void
.end method
