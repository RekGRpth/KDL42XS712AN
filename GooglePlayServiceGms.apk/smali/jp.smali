.class public Ljp;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lbp;
.implements Ljk;


# instance fields
.field public n:Ljq;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method


# virtual methods
.method public I_()Z
    .locals 2

    invoke-static {p0}, Lao;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p0, v0}, Lao;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lbo;->a(Landroid/content/Context;)Lbo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbo;->a(Landroid/app/Activity;)Lbo;

    invoke-virtual {v0}, Lbo;->a()V

    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finishAffinity()V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Ljp;->finish()V

    goto :goto_0

    :cond_1
    invoke-static {p0, v0}, Lao;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final a(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Lo;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lo;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method final a(ILandroid/view/Menu;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lo;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method final a(ILandroid/view/MenuItem;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lo;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method final a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lo;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected final a(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 3

    iget-object v0, p0, Ljp;->n:Ljq;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    iget-object v0, v0, Ljq;->a:Ljp;

    invoke-virtual {v0, p2}, Ljp;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, Ljq;->a:Ljp;

    invoke-super {v0, p1, p2}, Lo;->a(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a_()Landroid/content/Intent;
    .locals 1

    invoke-static {p0}, Lao;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1, p2}, Ljq;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method final b(I)V
    .locals 0

    invoke-super {p0, p1}, Lo;->setContentView(I)V

    return-void
.end method

.method final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lo;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->c()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public final i_()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    invoke-super {p0}, Lo;->i_()V

    :cond_0
    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->g()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lo;->onBackPressed()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lo;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->d()V

    return-void
.end method

.method public final onContentChanged()V
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->i()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    new-instance v0, Ljx;

    invoke-direct {v0, p0}, Ljx;-><init>(Ljp;)V

    :goto_0
    iput-object v0, p0, Ljp;->n:Ljq;

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1}, Ljq;->a(Landroid/os/Bundle;)V

    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    new-instance v0, Ljw;

    invoke-direct {v0, p0}, Ljw;-><init>(Ljp;)V

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    new-instance v0, Lju;

    invoke-direct {v0, p0}, Lju;-><init>(Ljp;)V

    goto :goto_0

    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    new-instance v0, Ljt;

    invoke-direct {v0, p0}, Ljt;-><init>(Ljp;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljr;

    invoke-direct {v0, p0}, Ljr;-><init>(Ljp;)V

    goto :goto_0
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1, p2}, Ljq;->a(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1}, Ljq;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lo;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 3

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1, p2}, Ljq;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c    # android.R.id.home

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljj;->b()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljp;->I_()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPostResume()V
    .locals 1

    invoke-super {p0}, Lo;->onPostResume()V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->f()V

    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1, p2, p3}, Ljq;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lo;->onStop()V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->e()V

    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Lo;->onTitleChanged(Ljava/lang/CharSequence;I)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1}, Ljq;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setContentView(I)V
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1}, Ljq;->a(I)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1}, Ljq;->a(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0, p1, p2}, Ljq;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
