.class public final Lgfn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final d:Ljava/util/Set;

.field private e:Ljava/util/List;

.field private f:Z

.field private g:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;

.field private h:Z

.field private i:Ljava/util/List;

.field private j:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lgfn;->d:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Lgfm;
    .locals 11

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;

    iget-object v1, p0, Lgfn;->d:Ljava/util/Set;

    iget-object v2, p0, Lgfn;->e:Ljava/util/List;

    iget-boolean v3, p0, Lgfn;->f:Z

    iget-object v4, p0, Lgfn;->a:Ljava/lang/String;

    iget-object v5, p0, Lgfn;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;

    iget-boolean v6, p0, Lgfn;->h:Z

    iget-object v7, p0, Lgfn;->i:Ljava/util/List;

    iget-object v8, p0, Lgfn;->b:Ljava/lang/String;

    iget-object v9, p0, Lgfn;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    iget-object v10, p0, Lgfn;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;-><init>(Ljava/util/Set;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;ZLjava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lgft;)Lgfn;
    .locals 2

    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    iput-object p1, p0, Lgfn;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    iget-object v0, p0, Lgfn;->d:Ljava/util/Set;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
