.class public final Lrq;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Ljava/util/concurrent/BlockingQueue;

.field private final c:Ljava/util/concurrent/BlockingQueue;

.field private final d:Lro;

.field private final e:Lsl;

.field private volatile f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lsq;->b:Z

    sput-boolean v0, Lrq;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lro;Lsl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lrq;->f:Z

    iput-object p1, p0, Lrq;->b:Ljava/util/concurrent/BlockingQueue;

    iput-object p2, p0, Lrq;->c:Ljava/util/concurrent/BlockingQueue;

    iput-object p3, p0, Lrq;->d:Lro;

    iput-object p4, p0, Lrq;->e:Lsl;

    return-void
.end method

.method static synthetic a(Lrq;)Ljava/util/concurrent/BlockingQueue;
    .locals 1

    iget-object v0, p0, Lrq;->c:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lrq;->f:Z

    invoke-virtual {p0}, Lrq;->interrupt()V

    return-void
.end method

.method public final run()V
    .locals 10

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-boolean v0, Lrq;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "start new dispatcher"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lsq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    iget-object v0, p0, Lrq;->d:Lro;

    invoke-interface {v0}, Lro;->a()V

    :cond_1
    :goto_0
    :try_start_0
    iget-object v0, p0, Lrq;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsc;

    const-string v1, "cache-queue-take"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lsc;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "cache-discard-canceled"

    invoke-virtual {v0, v1}, Lsc;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-boolean v0, p0, Lrq;->f:Z

    if-eqz v0, :cond_1

    return-void

    :cond_2
    :try_start_1
    iget-object v1, p0, Lrq;->d:Lro;

    invoke-virtual {v0}, Lsc;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lro;->a(Ljava/lang/String;)Lrp;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v1, "cache-miss"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lrq;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget-wide v5, v4, Lrp;->d:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    cmp-long v1, v5, v7

    if-gez v1, :cond_4

    move v1, v2

    :goto_1
    if-eqz v1, :cond_5

    const-string v1, "cache-hit-expired"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lsc;->a(Lrp;)Lsc;

    iget-object v1, p0, Lrq;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    const-string v1, "cache-hit"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/String;)V

    new-instance v1, Lrz;

    iget-object v5, v4, Lrp;->a:[B

    iget-object v6, v4, Lrp;->f:Ljava/util/Map;

    invoke-direct {v1, v5, v6}, Lrz;-><init>([BLjava/util/Map;)V

    invoke-virtual {v0, v1}, Lsc;->a(Lrz;)Lsi;

    move-result-object v5

    const-string v1, "cache-hit-parsed"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/String;)V

    iget-wide v6, v4, Lrp;->e:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-gez v1, :cond_6

    move v1, v2

    :goto_2
    if-nez v1, :cond_7

    iget-object v1, p0, Lrq;->e:Lsl;

    invoke-interface {v1, v0, v5}, Lsl;->a(Lsc;Lsi;)V

    goto :goto_0

    :cond_6
    move v1, v3

    goto :goto_2

    :cond_7
    const-string v1, "cache-hit-refresh-needed"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lsc;->a(Lrp;)Lsc;

    const/4 v1, 0x1

    iput-boolean v1, v5, Lsi;->d:Z

    iget-object v1, p0, Lrq;->e:Lsl;

    new-instance v4, Lrr;

    invoke-direct {v4, p0, v0}, Lrr;-><init>(Lrq;Lsc;)V

    invoke-interface {v1, v0, v5, v4}, Lsl;->a(Lsc;Lsi;Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
