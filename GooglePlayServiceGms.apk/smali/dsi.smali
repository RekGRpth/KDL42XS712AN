.class public final Ldsi;
.super Ldrw;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:Ldax;

.field private final h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V
    .locals 1

    invoke-direct {p0, p1}, Ldrw;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldsi;->b:Ldad;

    iput-object p3, p0, Ldsi;->c:Ljava/lang/String;

    iput-object p4, p0, Ldsi;->d:Ljava/lang/String;

    iput-object p5, p0, Ldsi;->e:Ljava/lang/String;

    iput p6, p0, Ldsi;->f:I

    iput-object p7, p0, Ldsi;->g:Ldax;

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ldsi;->h:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    iget-object v0, p0, Ldsi;->b:Ldad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldsi;->b:Ldad;

    iget-object v1, p0, Ldsi;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Ldad;->b(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)I
    .locals 9

    iget-object v2, p0, Ldsi;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsi;->c:Ljava/lang/String;

    iget-object v4, p0, Ldsi;->d:Ljava/lang/String;

    iget-object v5, p0, Ldsi;->e:Ljava/lang/String;

    iget v6, p0, Ldsi;->f:I

    iget-object v7, p0, Ldsi;->g:Ldax;

    iget-boolean v8, p0, Ldsi;->h:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;Z)I

    move-result v0

    return v0
.end method
