.class final Lrv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lrt;

.field private final b:Lsc;

.field private final c:Lsi;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lrt;Lsc;Lsi;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lrv;->a:Lrt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lrv;->b:Lsc;

    iput-object p3, p0, Lrv;->c:Lsi;

    iput-object p4, p0, Lrv;->d:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    iget-object v0, p0, Lrv;->b:Lsc;

    invoke-virtual {v0}, Lsc;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lrv;->b:Lsc;

    const-string v1, "canceled-at-delivery"

    invoke-virtual {v0, v1}, Lsc;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lrv;->c:Lsi;

    invoke-virtual {v0}, Lsi;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lrv;->b:Lsc;

    iget-object v1, p0, Lrv;->c:Lsi;

    iget-object v1, v1, Lsi;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lsc;->b(Ljava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lrv;->c:Lsi;

    iget-boolean v0, v0, Lsi;->d:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lrv;->b:Lsc;

    const-string v1, "intermediate-response"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lrv;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrv;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lrv;->b:Lsc;

    iget-object v1, p0, Lrv;->c:Lsi;

    iget-object v1, v1, Lsi;->c:Lsp;

    invoke-virtual {v0, v1}, Lsc;->b(Lsp;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lrv;->b:Lsc;

    const-string v1, "done"

    invoke-virtual {v0, v1}, Lsc;->b(Ljava/lang/String;)V

    goto :goto_2
.end method
