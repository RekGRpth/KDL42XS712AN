.class public final Likn;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Likn;->a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Likn;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const-string v0, "com.google.android.gms.location.reporting.SETTINGS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    const-string v0, "initialization"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "initialization"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Likn;->a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;I)I

    :cond_0
    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GoogleLocationSettingsActivity received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " => mInitialization="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Likn;->a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v2}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Likn;->a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
