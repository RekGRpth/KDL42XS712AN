.class public final Lbie;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbid;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Landroid/os/ResultReceiver;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/os/ResultReceiver;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbie;->a:Landroid/net/Uri;

    iput-object p2, p0, Lbie;->b:Landroid/os/ResultReceiver;

    iput p3, p0, Lbie;->c:I

    return-void
.end method

.method private a(Landroid/os/ParcelFileDescriptor;)V
    .locals 3

    iget-object v0, p0, Lbie;->b:Landroid/os/ResultReceiver;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.google.android.gms.extra.fileDescriptor"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lbie;->b:Landroid/os/ResultReceiver;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lbhw;)V
    .locals 3

    iget-object v0, p0, Lbie;->a:Landroid/net/Uri;

    invoke-virtual {p2, p1, v0}, Lbhw;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "ImageMultiThreadedInten"

    const-string v2, "Failed LoadImageOperation"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lbie;->a(Landroid/os/ParcelFileDescriptor;)V

    :goto_0
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-direct {p0, v1}, Lbie;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ImageMultiThreadedInten"

    const-string v2, "Failed to close file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
