.class public final Lgqe;
.super Lk;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lk;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/CharSequence;)Lgqe;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lgqe;->a(Ljava/lang/CharSequence;Z)Lgqe;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;Z)Lgqe;
    .locals 3

    new-instance v0, Lgqe;

    invoke-direct {v0}, Lgqe;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string v2, "cancelable"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lgqe;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lk;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "cancelable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lgqe;->b(Z)V

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    new-instance v0, Landroid/app/ProgressDialog;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v3, 0x7f100188    # com.google.android.gms.R.style.common_Activity_Light_Dialog

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "cancelable"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    invoke-super {p0, p1}, Lk;->onCancel(Landroid/content/DialogInterface;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    instance-of v1, v0, Lgqf;

    if-eqz v1, :cond_0

    check-cast v0, Lgqf;

    invoke-interface {v0}, Lgqf;->n()V

    :cond_0
    return-void
.end method
