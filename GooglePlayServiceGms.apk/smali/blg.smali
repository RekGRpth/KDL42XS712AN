.class public final Lblg;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lbli;

.field private c:Z

.field private d:Lblh;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-object v0, p0, Lblg;->d:Lblh;

    iput-object v0, p0, Lblg;->b:Lbli;

    const/4 v0, -0x1

    iput v0, p0, Lblg;->e:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lblg;->e:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lblg;->b()I

    :cond_0
    iget v0, p0, Lblg;->e:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lblh;

    invoke-direct {v0}, Lblh;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Lblg;->c:Z

    iput-object v0, p0, Lblg;->d:Lblh;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lbli;

    invoke-direct {v0}, Lbli;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Lblg;->a:Z

    iput-object v0, p0, Lblg;->b:Lbli;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Lblg;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lblg;->d:Lblh;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-boolean v0, p0, Lblg;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lblg;->b:Lbli;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lblg;->c:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lblg;->d:Lblh;

    invoke-static {v0, v1}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lblg;->a:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lblg;->b:Lbli;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lblg;->e:I

    return v0
.end method
