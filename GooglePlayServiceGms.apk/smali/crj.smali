.class final Lcrj;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "SourceFile"


# instance fields
.field private final a:Lcpa;


# direct methods
.method public constructor <init>(Lcpa;)V
    .locals 1

    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    iput-object p1, p0, Lcrj;->a:Lcpa;

    const-string v0, "application/x-protobuf"

    invoke-virtual {p0, v0}, Lcrj;->setContentType(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getContent() not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getContentLength()J
    .locals 2

    iget-object v0, p0, Lcrj;->a:Lcpa;

    invoke-virtual {v0}, Lcpa;->b()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    invoke-static {p1}, Lizh;->a(Ljava/io/OutputStream;)Lizh;

    move-result-object v0

    iget-object v1, p0, Lcrj;->a:Lcpa;

    invoke-virtual {v1, v0}, Lcpa;->a(Lizh;)V

    invoke-virtual {v0}, Lizh;->a()V

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    return-void
.end method
