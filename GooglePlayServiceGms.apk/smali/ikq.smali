.class public final Likq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic a:Landroid/app/AlertDialog;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Lcom/google/android/location/settings/LocationHistorySettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/location/settings/LocationHistorySettingsActivity;Landroid/app/AlertDialog;Landroid/widget/CheckBox;)V
    .locals 0

    iput-object p1, p0, Likq;->c:Lcom/google/android/location/settings/LocationHistorySettingsActivity;

    iput-object p2, p0, Likq;->a:Landroid/app/AlertDialog;

    iput-object p3, p0, Likq;->b:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onShow(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Likq;->a:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Likq;->b:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
