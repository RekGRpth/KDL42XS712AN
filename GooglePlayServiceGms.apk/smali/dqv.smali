.class public final Ldqv;
.super Ldah;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private g:Ldrq;

.field private h:Landroid/os/IBinder;

.field private i:J

.field private j:Ldqz;

.field private final k:Ljava/util/ArrayList;

.field private l:Landroid/content/ContentValues;

.field private m:Landroid/content/ContentValues;

.field private final n:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldqv;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ldah;-><init>()V

    iput-object v2, p0, Ldqv;->g:Ldrq;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldqv;->i:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldqv;->k:Ljava/util/ArrayList;

    iput-object v2, p0, Ldqv;->l:Landroid/content/ContentValues;

    iput-object v2, p0, Ldqv;->m:Landroid/content/ContentValues;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldqv;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbfy;->a(Landroid/content/Context;)V

    iput-object p1, p0, Ldqv;->b:Landroid/content/Context;

    iput-object p2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iput p3, p0, Ldqv;->d:I

    iput-object p4, p0, Ldqv;->e:Ljava/lang/String;

    iput-boolean p5, p0, Ldqv;->f:Z

    return-void
.end method

.method private static A()V
    .locals 2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method private B()V
    .locals 2

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbbv;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    return-void
.end method

.method private C()V
    .locals 2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Ldqv;->B()V

    :cond_0
    return-void
.end method

.method private D()V
    .locals 2

    invoke-direct {p0}, Ldqv;->B()V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v1, "https://www.googleapis.com/auth/games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_1
    return-void
.end method

.method private E()V
    .locals 1

    iget-boolean v0, p0, Ldqv;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Ldqv;->F()V

    :cond_0
    return-void
.end method

.method private F()V
    .locals 2

    iget-boolean v0, p0, Ldqv;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be headless when checking if signed in"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lduj;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_1
    return-void
.end method

.method private G()Z
    .locals 2

    iget-object v0, p0, Ldqv;->g:Ldrq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqv;->g:Ldrq;

    iget-object v1, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldrq;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private H()V
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v1}, Ldrq;->b()V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Ldqv;->i:J

    iget-object v1, p0, Ldqv;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_0
    iget-object v1, p0, Ldqv;->j:Ldqz;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldqv;->j:Ldqz;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ldqz;->cancel(Z)Z

    const/4 v1, 0x0

    iput-object v1, p0, Ldqv;->j:Ldqz;

    :cond_1
    sget-object v2, Ldqv;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Ldqv;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Ldqv;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqy;

    const/4 v4, 0x0

    iput-boolean v4, v0, Ldqy;->c:Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private I()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Ldqv;->h:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqv;->h:Landroid/os/IBinder;

    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Ldqv;->h:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Ldqv;->h:Landroid/os/IBinder;

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "GamesService"

    const-string v1, "The Games Android Service has not been registered with the RealTimeGameDeathBinder."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v2, p0, Ldqv;->h:Landroid/os/IBinder;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Ldqv;->h:Landroid/os/IBinder;

    throw v0
.end method

.method static synthetic a(Ldqv;Ldqz;)Ldqz;
    .locals 0

    iput-object p1, p0, Ldqv;->j:Ldqz;

    return-object p1
.end method

.method static synthetic a(Ldqv;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Ldqv;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static a([I)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    array-length v0, p0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must provide at least one turn status!"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    move v0, v2

    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_2

    aget v4, p0, v0

    if-ltz v4, :cond_1

    const/4 v3, 0x4

    if-ge v4, v3, :cond_1

    move v3, v1

    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Not a valid turn status: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbkm;->a(ZLjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_2

    :cond_2
    return-void
.end method

.method private static a([Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    array-length v0, p0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must provide at least one ID!"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    move v0, v2

    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_2

    aget-object v3, p0, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Not a valid external ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, p0, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbkm;->a(ZLjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_2

    :cond_2
    return-void
.end method

.method private a(Ldrs;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Attempting to bind to service while already bound"

    invoke-static {v0, v3}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v3, p0, Ldqv;->g:Ldrq;

    invoke-static {v0, p1, v3}, Ldrq;->a(Landroid/content/Context;Ldrs;Ldrq;)Ldrq;

    move-result-object v0

    iput-object v0, p0, Ldqv;->g:Ldrq;

    iget-object v0, p0, Ldqv;->g:Ldrq;

    if-eqz v0, :cond_1

    const-string v0, "GamesService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Connected to RoomAndroidService using RoomServiceClient instance: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Ldqv;->g:Ldrq;

    if-eqz v0, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const-string v0, "GamesService"

    const-string v3, "Bind to RoomAndroidService returned null RoomServiceClient."

    invoke-static {v0, v3}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public static synthetic b(Ldqv;)Ldrq;
    .locals 1

    iget-object v0, p0, Ldqv;->g:Ldrq;

    return-object v0
.end method

.method private static b(I)Ljava/util/ArrayList;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method

.method private b(Landroid/os/IBinder;)V
    .locals 2

    iput-object p1, p0, Ldqv;->h:Landroid/os/IBinder;

    :try_start_0
    iget-object v0, p0, Ldqv;->h:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesService"

    const-string v1, "Failed to register for death notification for the binder."

    invoke-static {v0, v1}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic c(Ldqv;)V
    .locals 0

    invoke-direct {p0}, Ldqv;->H()V

    return-void
.end method

.method static synthetic d(Ldqv;)Ldqz;
    .locals 1

    iget-object v0, p0, Ldqv;->j:Ldqz;

    return-object v0
.end method

.method static synthetic e(Ldqv;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Ldqv;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 1

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    return-object v0
.end method

.method static synthetic g(Ldqv;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldqv;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Ldqv;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Ldqv;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method public static synthetic y()V
    .locals 0

    invoke-static {}, Ldqv;->A()V

    return-void
.end method

.method static synthetic z()Ljava/lang/Object;
    .locals 1

    sget-object v0, Ldqv;->a:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public final a(Ldad;[BLjava/lang/String;Ljava/lang/String;)I
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Room ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Ldgd;->a(Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid participant ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GamesService"

    const-string v1, "Failed to send message: not currently connected to a room."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v0, p1, p2, p4}, Ldrq;->a(Ldad;[BLjava/lang/String;)I

    move-result v0

    goto :goto_1
.end method

.method public final a([BLjava/lang/String;[Ljava/lang/String;)I
    .locals 6

    const/4 v1, 0x0

    const/4 v2, -0x1

    array-length v0, p1

    const/16 v3, 0x490

    if-le v0, v3, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v3, "Room ID must not be null or empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p3, :cond_2

    :goto_2
    array-length v0, p3

    if-ge v1, v0, :cond_2

    aget-object v0, p3, v1

    invoke-static {v0}, Ldgd;->a(Ljava/lang/String;)Z

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid participant ID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "GamesService"

    const-string v1, "Failed to send message: not currently connected to a room."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v0, p1, p2, p3}, Ldrq;->a([BLjava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(IIZ)Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SELECT_OPPONENTS_TURN_BASED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.MIN_SELECTIONS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.MAX_SELECTIONS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_AUTOMATCH"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.MULTIPLAYER_TYPE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(I[BILjava/lang/String;)Landroid/content/Intent;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ldqv;->F()V

    if-eq p1, v2, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Must provide a valid type."

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    const-string v0, "Must provide a non-null payload."

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, p2

    invoke-virtual {p0}, Ldqv;->s()I

    move-result v3

    if-gt v0, v3, :cond_4

    move v0, v2

    :goto_1
    const-string v3, "Payload is too big"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Ldqv;->t()I

    move-result v0

    if-gt p3, v0, :cond_5

    if-gtz p3, :cond_1

    const/4 v0, -0x1

    if-ne p3, v0, :cond_5

    :cond_1
    move v0, v2

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Request lifetime days must be <= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ldqv;->t()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", greater than zero, or -1 for server default."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_3
    const-string v0, "Must provide a valid item description"

    invoke-static {v2, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SEND_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.REQUEST_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PAYLOAD"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.REQUEST_LIFETIME"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.google.android.gms.games.REQUEST_ITEM_NAME"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->C()V

    invoke-direct {p0}, Ldqv;->E()V

    iget-boolean v0, p0, Ldqv;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_PUBLIC_INVITATIONS_INTERNAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_PUBLIC_INVITATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->C()V

    invoke-direct {p0}, Ldqv;->E()V

    iget-boolean v0, p0, Ldqv;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_PUBLIC_REQUESTS_INTERNAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_PUBLIC_REQUESTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;I)Landroid/content/Intent;
    .locals 2

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Room parameter must not be null"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "minParticipantsToStart must be >= 0"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_REAL_TIME_WAITING_ROOM"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "room"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.MIN_PARTICIPANTS_TO_START"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->C()V

    invoke-direct {p0}, Ldqv;->E()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_PARTICIPANTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PARTICIPANTS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.FEATURED_URI"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.ICON_URI"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_ID"

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    invoke-direct {p0}, Ldqv;->F()V

    invoke-direct {p0}, Ldqv;->D()V

    const-string v0, "Uri cannot be null"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v0}, Lbhw;->a(Landroid/content/Context;)Lbhw;

    move-result-object v0

    iget-object v3, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v0, v3, p1}, Lbhw;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldqv;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game package name must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lbov;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Leej;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v1, v0, v2, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(J)V
    .locals 2

    iget-wide v0, p0, Ldqv;->i:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Ldqv;->H()V

    invoke-direct {p0}, Ldqv;->I()V

    :cond_0
    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcux;->a(J)V

    return-void
.end method

.method public final a(JLjava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p1, p2}, Lcux;->a(Ljava/lang/String;Ljava/lang/String;J)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/ContentValues;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    iput-object v0, p0, Ldqv;->l:Landroid/content/ContentValues;

    return-void
.end method

.method public final a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v1, Ldax;

    invoke-direct {v1, p2, p1}, Ldax;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    const/4 v0, 0x0

    new-instance v2, Lcts;

    invoke-virtual {p0}, Ldqv;->f()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    invoke-direct {v2, v3}, Lcts;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_0
    invoke-virtual {v2}, Lcts;->a()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcts;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v2}, Lcts;->b()V

    if-eqz v0, :cond_1

    iget-object v2, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v2, v1, v0}, Lecs;->a(Landroid/content/Context;Ldax;Lcom/google/android/gms/games/PlayerEntity;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcts;->b()V

    throw v0
.end method

.method public final a(Ldad;)V
    .locals 5

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcux;->a(Ljava/lang/String;)V

    new-instance v0, Ldqy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Ldqy;-><init>(Ldqv;Ldad;Z)V

    sget-object v1, Ldqv;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Ldqv;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldqy;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    iget-object v3, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    invoke-static {v0, v1, v2, v3, v4}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ldad;I)V
    .locals 6

    const/4 v0, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    const-string v1, "Must provide a valid callback object"

    invoke-static {p1, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    const-string v1, "Invalid invitation sort order!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZ)V

    return-void

    :cond_1
    move v0, v5

    goto :goto_0
.end method

.method public final a(Ldad;III)V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    if-ne p4, v2, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Invalid request sort order!"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p2, :cond_1

    if-ne p2, v2, :cond_3

    :cond_1
    move v0, v2

    :goto_1
    const-string v3, "Invalid request direction!"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p3, :cond_4

    :goto_2
    const-string v0, "Must provide at least one request type"

    invoke-static {v2, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Ldqv;->b(I)Ljava/util/ArrayList;

    move-result-object v7

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move v5, p2

    move v6, p4

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_2
.end method

.method public final a(Ldad;IIZZ)V
    .locals 7

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    packed-switch p3, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    const-string v1, "Invalid game collection type"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IIZZ)V

    return-void

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ldad;II[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Invited players must not be null"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    if-lez p2, :cond_2

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Variant must be a positive integer if provided! Input was "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    if-eqz p5, :cond_1

    const-string v0, "min_automatch_players"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v0, "max_automatch_players"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ltz v3, :cond_3

    move v0, v1

    :goto_1
    const-string v5, "Min players must be a positive integer"

    invoke-static {v0, v5}, Lbkm;->a(ZLjava/lang/Object;)V

    if-gt v3, v4, :cond_4

    :goto_2
    const-string v0, "Min players must be less than or equal to max players"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;II[Ljava/lang/String;Landroid/os/Bundle;)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final a(Ldad;IZZ)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-lez p2, :cond_0

    const/16 v0, 0x19

    if-gt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Page size must be between 1 and 25"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;I[I)V
    .locals 6

    const/4 v0, 0x1

    invoke-direct {p0}, Ldqv;->F()V

    const-string v1, "Must provide a valid callback object"

    invoke-static {p1, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    const-string v1, "Invalid match sort order!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Ldqv;->a([I)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I[I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;J)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    move-wide v3, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcux;->a(Ljava/lang/String;Ljava/lang/String;JLdad;)V

    return-void
.end method

.method public final a(Ldad;JLjava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    move-object v2, p4

    move-wide v3, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcux;->a(Ljava/lang/String;Ljava/lang/String;JLdad;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;Landroid/os/Bundle;II)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Must provide a non-null bundle!"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    packed-switch p4, :pswitch_data_0

    move v0, v2

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized page direction "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    sget-object v0, Lcwh;->k:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v0

    if-lez p3, :cond_0

    if-gt p3, v0, :cond_0

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Max results must be between 1 and "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v3, Ldfh;

    invoke-direct {v3, p2}, Ldfh;-><init>(Landroid/os/Bundle;)V

    move-object v2, p1

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ldfh;II)V

    return-void

    :pswitch_0
    move v0, v1

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ldad;Landroid/os/IBinder;I[Ljava/lang/String;Landroid/os/Bundle;ZJ)V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Ldqv;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x1b5f

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Ldad;->r(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesService"

    const-string v1, "The client is not reachable"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Invited players must not be null"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, -0x1

    if-eq p3, v0, :cond_2

    if-lez p3, :cond_5

    move v0, v1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Variant must be a positive integer if provided! Input was "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_2
    if-eqz p5, :cond_3

    const-string v0, "min_automatch_players"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v0, "max_automatch_players"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ltz v3, :cond_6

    move v0, v1

    :goto_2
    const-string v5, "Min players must be a positive integer"

    invoke-static {v0, v5}, Lbkm;->a(ZLjava/lang/Object;)V

    if-gt v3, v4, :cond_7

    :goto_3
    const-string v0, "Min players must be less than or equal to max players"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_3
    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GamesService"

    const-string v1, "Trying to create a new room, while already having an active room. Exited the previous room without calling leaveRoom."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Ldqv;->H()V

    :cond_4
    invoke-direct {p0, p2}, Ldqv;->b(Landroid/os/IBinder;)V

    iput-wide p7, p0, Ldqv;->i:J

    new-instance v0, Ldqw;

    iget-object v1, p0, Ldqv;->e:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move v4, p3

    move-object v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldqw;-><init>(Ldqv;Ldad;Landroid/os/Bundle;I[Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Ldqv;->a(Ldrs;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x1b58

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Ldad;->r(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "GamesService"

    const-string v1, "The client is not reachable"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method public final a(Ldad;Landroid/os/IBinder;Ljava/lang/String;ZJ)V
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Ldqv;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1b5f

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Ldad;->s(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesService"

    const-string v1, "The client is not reachable"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Ldqv;->F()V

    const-string v1, "Must provide a valid callback object"

    invoke-static {p1, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    :goto_1
    const-string v1, "Room ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GamesService"

    const-string v1, "Trying to join a new room, while already having an active room. Exited the previous room without calling leaveRoom."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Ldqv;->H()V

    :cond_2
    invoke-direct {p0, p2}, Ldqv;->b(Landroid/os/IBinder;)V

    iput-wide p5, p0, Ldqv;->i:J

    new-instance v0, Ldqx;

    iget-object v1, p0, Ldqv;->e:Ljava/lang/String;

    invoke-direct {v0, p0, p1, p3, p4}, Ldqx;-><init>(Ldqv;Ldad;Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Ldqv;->a(Ldrs;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x1b58

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Ldad;->s(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "GamesService"

    const-string v1, "The client is not reachable"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ldad;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_0

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ldad;Ljava/lang/String;I)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide at least one request type"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;Ljava/lang/String;IIIZ)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Ldef;->b(I)Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized time span "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Ldea;->b(I)Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized leaderboard collection "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    sget-object v0, Lcwh;->k:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v0

    if-lez p5, :cond_1

    if-gt p5, v0, :cond_1

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Max results must be between 1 and "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final a(Ldad;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Achievement ID must not be null or empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-lez p3, :cond_1

    :goto_1
    const-string v0, "Number of steps must be greater than 0"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v7, Ldax;

    invoke-direct {v7, p5, p4}, Ldax;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p2

    move v6, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final a(Ldad;Ljava/lang/String;IZ)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game ID must not be empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Lbpx;->b(Ljava/lang/String;)Z

    move-result v0

    const-string v3, "Game ID must be numeric!"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    packed-switch p3, :pswitch_data_0

    move v1, v2

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized ROP level "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    const-string v0, "https://www.googleapis.com/auth/plus.circles.members"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZ)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ldad;Ljava/lang/String;IZZ)V
    .locals 7

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const/16 v0, 0x32

    if-gt p3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "We don\'t handle loading more than 50 games simultaneously"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    if-nez p2, :cond_1

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v3

    :goto_1
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v3, p2

    goto :goto_1
.end method

.method public final a(Ldad;Ljava/lang/String;IZZZZ)V
    .locals 9

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZZZ)V

    return-void
.end method

.method public final a(Ldad;Ljava/lang/String;I[I)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p3, :cond_1

    if-ne p3, v2, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "Invalid match sort order!"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Ldqv;->a([I)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I[I)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ldad;Ljava/lang/String;J)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Ldqv;->a(Ldad;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method public final a(Ldad;Ljava/lang/String;JLjava/lang/String;)V
    .locals 9

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p5, :cond_0

    invoke-static {}, Lcom/google/android/gms/games/service/GamesAndroidService;->b()Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Score tags must be no more than 64 URI safe characters. Input was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p2

    move-wide v6, p3

    move-object v8, p5

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 7

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Achievement ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v6, Ldax;

    invoke-direct {v6, p4, p3}, Ldax;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Ldqv;->b(Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Ldad;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 7

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "External leaderboard ID must not be empty or null"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Ldef;->b(I)Z

    move-result v0

    const-string v1, "Must provide a valid time span"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-static {p5}, Ldea;->b(I)Z

    move-result v0

    const-string v1, "Must provide a valid leaderboard collection"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;II)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p6, :cond_1

    if-ne p6, v2, :cond_4

    :cond_1
    move v0, v2

    :goto_1
    const-string v3, "Invalid request sort order!"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p4, :cond_2

    if-ne p4, v2, :cond_5

    :cond_2
    move v0, v2

    :goto_2
    const-string v3, "Invalid request direction!"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p5, :cond_6

    :goto_3
    const-string v0, "Must provide at least one request type"

    invoke-static {v2, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p5}, Ldqv;->b(I)Ljava/util/ArrayList;

    move-result-object v7

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p6

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_3
.end method

.method public final a(Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 9

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Ldef;->b(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized time span "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p5}, Ldea;->b(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized leaderboard collection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    sget-object v0, Lcwh;->k:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v1

    if-lez p6, :cond_2

    if-gt p6, v1, :cond_2

    const/4 v0, 0x1

    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Max results must be between 1 and "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 8

    invoke-direct {p0}, Ldqv;->C()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    const-string v0, "https://www.googleapis.com/auth/plus.circles.read"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V

    return-void
.end method

.method public final a(Ldad;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    :goto_1
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v4, p2

    goto :goto_1
.end method

.method public final a(Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid game ID"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Ldqv;->a([Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;Ljava/lang/String;Z)V
    .locals 2

    invoke-direct {p0}, Ldqv;->C()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldad;Ljava/lang/String;[BLjava/lang/String;[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Match ID must not be empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p4, :cond_0

    invoke-static {p4}, Ldgd;->a(Ljava/lang/String;)Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid participant ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    if-eqz p3, :cond_1

    array-length v0, p3

    invoke-virtual {p0}, Ldqv;->i()I

    move-result v3

    if-gt v0, v3, :cond_3

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Match data is too large ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes). The maximum is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldqv;->i()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p3

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final a(Ldad;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Match ID must not be empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-virtual {p0}, Ldqv;->i()I

    move-result v3

    if-gt v0, v3, :cond_2

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Match data is too large ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes). The maximum is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldqv;->i()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final a(Ldad;Ljava/lang/String;[I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Ldqv;->a(Ldad;Ljava/lang/String;I[I)V

    return-void
.end method

.method public final a(Ldad;Ljava/lang/String;[Ljava/lang/String;I[BI)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    invoke-direct {p0}, Ldqv;->C()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Must provide a valid game ID"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Ldqv;->a([Ljava/lang/String;)V

    packed-switch p4, :pswitch_data_0

    move v0, v2

    :goto_1
    const-string v3, "Must provide a valid request type"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Ldqv;->t()I

    move-result v0

    if-gt p6, v0, :cond_2

    if-gtz p6, :cond_0

    const/4 v0, -0x1

    if-ne p6, v0, :cond_2

    :cond_0
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Request lifetime days must be <= "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ldqv;->t()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " and greater than zero. Use -1 for server default."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v3, p2

    move v5, p4

    move v6, p6

    move-object v7, p5

    move-object v8, p3

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;II[B[Ljava/lang/String;)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_0
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ldad;Z)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Ldad;ZLandroid/os/Bundle;)V
    .locals 5

    invoke-direct {p0}, Ldqv;->B()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldec;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown channel "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbiq;->a(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;ZLandroid/os/Bundle;)V

    return-void
.end method

.method public final a(Ldad;[I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Ldqv;->a(Ldad;I[I)V

    return-void
.end method

.method public final a(Ldad;[Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Ldqv;->a([Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 2

    const-string v0, "  GamesServiceInstance:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "    Game ID: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "    Package name: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->g:Ldrq;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "    RoomServiceClient instance: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Ldqv;->e:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "    RoomServiceClient is null"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invitation ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game package name must not be empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Account name must not be empty"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lbov;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must decline invite on behalf of a valid game"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Invitation ID must not be null or empty"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Ldrc;->a(Landroid/content/Context;Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Ldrc;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method public final b(IIZ)Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SELECT_OPPONENTS_REAL_TIME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.MIN_SELECTIONS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.MAX_SELECTIONS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_AUTOMATCH"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.MULTIPLAYER_TYPE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/os/Bundle;
    .locals 4

    sget-object v1, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/HashMap;

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const-string v2, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p1}, Ldgd;->a(Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid participant ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GamesService"

    const-string v1, "Socket creation failed: room android service is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v0, p1}, Ldrq;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(J)V
    .locals 3

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcux;->a(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public final b(JLjava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p1, p2}, Lcux;->b(Ljava/lang/String;Ljava/lang/String;J)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/ContentValues;)V
    .locals 0

    iput-object p1, p0, Ldqv;->m:Landroid/content/ContentValues;

    return-void
.end method

.method public final b(Ldad;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldqv;->b(Ldad;Z)V

    return-void
.end method

.method public final b(Ldad;IZZ)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-lez p2, :cond_0

    const/16 v0, 0x19

    if-gt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Page size must be between 1 and 25"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    const-string v0, "https://www.googleapis.com/auth/plus.circles.read"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ldad;J)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    move-wide v3, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcux;->b(Ljava/lang/String;Ljava/lang/String;JLdad;)V

    return-void
.end method

.method public final b(Ldad;JLjava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    move-object v2, p4

    move-wide v3, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcux;->b(Ljava/lang/String;Ljava/lang/String;JLdad;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ldad;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Ldqv;->c(Ldad;Ljava/lang/String;Z)V

    return-void
.end method

.method public final b(Ldad;Ljava/lang/String;IIIZ)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Ldef;->b(I)Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized time span "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Ldea;->b(I)Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized leaderboard collection "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    sget-object v0, Lcwh;->k:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v0

    if-lez p5, :cond_1

    if-gt p5, v0, :cond_1

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Max results must be between 1 and "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final b(Ldad;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Achievement ID must not be null or empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-lez p3, :cond_1

    :goto_1
    const-string v0, "Number of steps must be greater than 0"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v7, Ldax;

    invoke-direct {v7, p5, p4}, Ldax;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p2

    move v6, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final b(Ldad;Ljava/lang/String;IZ)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_0

    if-ne p3, v2, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Invalid invitation sort order!"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZ)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ldad;Ljava/lang/String;IZZ)V
    .locals 7

    invoke-direct {p0}, Ldqv;->C()V

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Query must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    const-string v0, "https://www.googleapis.com/auth/plus.circles.read"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ldad;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 7

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Achievement ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v6, Ldax;

    invoke-direct {v6, p4, p3}, Ldax;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Ldqv;->a(Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public final b(Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 9

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Ldef;->b(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized time span "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p5}, Ldea;->b(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized leaderboard collection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    sget-object v0, Lcwh;->k:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v1

    if-lez p6, :cond_2

    if-gt p6, v1, :cond_2

    const/4 v0, 0x1

    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Max results must be between 1 and "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b(Ldad;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game ID must not be empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Leaderboard ID must not be null or empty"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final b(Ldad;Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Ldqv;->b(Ldad;Ljava/lang/String;IZ)V

    return-void
.end method

.method public final b(Ldad;Z)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v2, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public final b(Ldad;[Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Ldqv;->a([Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {p0}, Ldqv;->e()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invitation ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must dismiss match on behalf of a valid game"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Match ID must not be empty"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must dismiss invitation on behalf of a valid game"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Invitation ID must not be null or empty"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final binderDied()V
    .locals 2

    const-string v0, "GamesService"

    const-string v1, "The game process died without disconnecting the games client."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Ldqv;->H()V

    return-void
.end method

.method public final c()V
    .locals 0

    invoke-static {}, Leco;->d()V

    return-void
.end method

.method public final c(J)V
    .locals 3

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcux;->b(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public final c(JLjava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p1, p2}, Lcux;->c(Ljava/lang/String;Ljava/lang/String;J)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ldad;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldqv;->a(Ldad;Z)V

    return-void
.end method

.method public final c(Ldad;IZZ)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    const-string v0, "https://www.googleapis.com/auth/plus.circles.read"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V

    return-void
.end method

.method public final c(Ldad;J)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    move-wide v3, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcux;->c(Ljava/lang/String;Ljava/lang/String;JLdad;)V

    return-void
.end method

.method public final c(Ldad;JLjava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    move-object v2, p4

    move-wide v3, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcux;->c(Ljava/lang/String;Ljava/lang/String;JLdad;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ldad;Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Room ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v0, p1, p2}, Ldrq;->a(Ldad;Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0}, Ldqv;->I()V

    invoke-direct {p0}, Ldqv;->H()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v4, "PLAYER_LEFT"

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ldpc;)V

    goto :goto_1
.end method

.method public final c(Ldad;Ljava/lang/String;IZZ)V
    .locals 7

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Query must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v5, 0x1

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v5

    :goto_0
    const-string v1, "Match ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz p3, :cond_0

    invoke-static {p3}, Ldgd;->a(Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid participant ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ldad;Ljava/lang/String;Z)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    move-object v2, p1

    move-object v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ldad;Z)V
    .locals 6

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v2, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZ)V

    return-void
.end method

.method public final c(Ldad;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    array-length v0, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide at least 1 external player ID."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;[Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Player ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/plus.circles.write"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Leej;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)I
    .locals 1

    invoke-static {}, Ldqv;->A()V

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x1b5e

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v0, p1}, Ldrq;->j(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, v2, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is missing permission android.permission.GET_ACCOUNTS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lduj;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(J)V
    .locals 3

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcux;->c(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public final d(Ldad;)V
    .locals 4

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V

    return-void
.end method

.method public final d(Ldad;IZZ)V
    .locals 8

    const-string v3, "circled"

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v4, 0x0

    move-object v2, p1

    move v5, p2

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V

    return-void
.end method

.method public final d(Ldad;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Ldqv;->d(Ldad;Ljava/lang/String;Z)V

    return-void
.end method

.method public final d(Ldad;Ljava/lang/String;IZZ)V
    .locals 7

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-lez p3, :cond_0

    const/16 v0, 0x19

    if-gt p3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Page size must be between 1 and 25"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must rematch on behalf of a valid game"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Match ID must not be empty"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final d(Ldad;Ljava/lang/String;Z)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v4, 0x0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Player ID must not be empty"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    move v1, v2

    :pswitch_1
    const-string v0, "Can only record positive actions with this API"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/plus.circles.write"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    invoke-direct {p0}, Ldqv;->B()V

    invoke-direct {p0}, Ldqv;->F()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p1}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Ldqv;->F()V

    iget-object v0, p0, Ldqv;->l:Landroid/content/ContentValues;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->l:Landroid/content/ContentValues;

    const-string v1, "external_player_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ldad;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldqv;->a(Ldad;I)V

    return-void
.end method

.method public final e(Ldad;IZZ)V
    .locals 6

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    const-string v0, "https://www.googleapis.com/auth/plus.circles.read"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V

    return-void
.end method

.method public final e(Ldad;Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Game ID must not be empty"

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v2, p1, p2, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must accept turn-based invite on behalf of a valid game"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Match ID must not be empty"

    invoke-static {v1, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final f()Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    invoke-direct {p0}, Ldqv;->F()V

    iget-object v0, p0, Ldqv;->l:Landroid/content/ContentValues;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ldjo;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v0

    iget-object v1, p0, Ldqv;->l:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->a()V

    return-object v0
.end method

.method public final f(Ldad;)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v1, v1}, Ldqv;->d(Ldad;IZZ)V

    return-void
.end method

.method public final f(Ldad;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Match ID must not be null"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final g(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_LEADERBOARD_SCORES"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ldad;)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v1, v1}, Ldqv;->e(Ldad;IZZ)V

    return-void
.end method

.method public final g(Ldad;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Ldqv;->b(Ldad;Ljava/lang/String;Z)V

    return-void
.end method

.method public final g()Z
    .locals 1

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v0}, Ldrc;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final h(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p1}, Ldgd;->a(Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid participant ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GamesService"

    const-string v1, "Socket creation failed: room android service is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v0, p1}, Ldrq;->l(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    sget-object v0, Leep;->a:Ldhu;

    iget-object v0, v0, Ldhu;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v0

    iget-object v1, p0, Ldqv;->m:Landroid/content/ContentValues;

    if-nez v1, :cond_0

    invoke-virtual {v0, v2}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->a()V

    return-object v0

    :cond_0
    iget-object v1, p0, Ldqv;->m:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final h(Ldad;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    invoke-static {}, Ldqv;->A()V

    invoke-direct {p0}, Ldqv;->F()V

    invoke-direct {p0}, Ldqv;->G()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x1b5e

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldqv;->g:Ldrq;

    invoke-virtual {v0, p1, p2}, Ldrq;->b(Ldad;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->a()V

    goto :goto_0
.end method

.method public final h(Ldad;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->B()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;)V

    return-void
.end method

.method public final i()I
    .locals 1

    sget-object v0, Lcwh;->r:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v0

    return v0
.end method

.method public final i(Ldad;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->B()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;)V

    return-void
.end method

.method public final i(Ldad;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->C()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    return-void
.end method

.method public final j(Ldad;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->B()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "ACL data must not be null"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    return-void
.end method

.method public final k()Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_LEADERBOARDS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final k(Ldad;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->E()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Invitation ID"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Landroid/content/Intent;
    .locals 2

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_ACHIEVEMENTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final l(Ldad;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Match ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_MULTIPLAYER_INBOX"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ldad;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Match ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_INVITATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ldad;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Match ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PLAYER_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final o(Ldad;Ljava/lang/String;)V
    .locals 7

    const/4 v5, 0x0

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Match ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldqv;->e:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-void

    :cond_0
    move v0, v5

    goto :goto_0
.end method

.method public final p()Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final p(Ldad;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Match ID must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Landroid/content/Intent;
    .locals 2

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PARCEL_COMPAT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final q(Ldad;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldqv;->D()V

    invoke-direct {p0}, Ldqv;->F()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Query must not be empty"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqv;->b:Landroid/content/Context;

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    iget-object v0, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Ldll;->d(Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    return v0
.end method

.method public final s()I
    .locals 1

    sget-object v0, Lcwh;->s:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v0

    return v0
.end method

.method public final t()I
    .locals 1

    sget-object v0, Lcwh;->t:Lbfy;

    invoke-static {v0}, Lbhv;->c(Lbfy;)I

    move-result v0

    return v0
.end method

.method public final u()Landroid/content/Intent;
    .locals 3

    invoke-direct {p0}, Ldqv;->F()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_REQUEST_INBOX"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldqv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget v1, p0, Ldqv;->d:I

    invoke-static {v0, v1}, Ldab;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldqv;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Z
    .locals 1

    iget-boolean v0, p0, Ldqv;->f:Z

    return v0
.end method

.method public final x()V
    .locals 2

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    iget-object v1, p0, Ldqv;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcux;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Ldqv;->H()V

    return-void
.end method
