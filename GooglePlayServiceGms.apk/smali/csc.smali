.class public final Lcsc;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/widget/ProgressBar;

.field final synthetic b:Landroid/widget/ImageView;

.field final synthetic c:Lcom/google/android/gms/feedback/ErrorReport;

.field final synthetic d:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

.field final synthetic e:I

.field final synthetic f:Landroid/view/View;

.field final synthetic g:Landroid/widget/TextView;

.field final synthetic h:Lcom/google/android/gms/feedback/FeedbackActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/widget/ProgressBar;Landroid/widget/ImageView;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;ILandroid/view/View;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcsc;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    iput-object p2, p0, Lcsc;->a:Landroid/widget/ProgressBar;

    iput-object p3, p0, Lcsc;->b:Landroid/widget/ImageView;

    iput-object p4, p0, Lcsc;->c:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object p5, p0, Lcsc;->d:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    iput p6, p0, Lcsc;->e:I

    iput-object p7, p0, Lcsc;->f:Landroid/view/View;

    iput-object p8, p0, Lcsc;->g:Landroid/widget/TextView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcsc;->c:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, v2, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    if-nez v2, :cond_3

    iget-object v2, p0, Lcsc;->d:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    iget v2, v2, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a:I

    iget v3, p0, Lcsc;->e:I

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_5

    :goto_0
    iget-object v2, p0, Lcsc;->d:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    iget-object v2, v2, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->c:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcsc;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcsc;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    iget-object v2, p0, Lcsc;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a([BZ)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcsc;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcsc;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcsc;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a([BZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    :cond_2
    iget-object v0, p0, Lcsc;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v2, p0, Lcsc;->c:Lcom/google/android/gms/feedback/ErrorReport;

    iget v2, v2, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    iget v3, p0, Lcsc;->e:I

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_4

    :goto_2
    iget-object v1, p0, Lcsc;->c:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, v1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcsc;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcsc;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcsc;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcsc;->b:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcsc;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcsc;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcsc;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcsc;->b:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcsc;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void
.end method
