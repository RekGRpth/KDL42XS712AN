.class public final enum Lalb;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lalb;

.field public static final enum b:Lalb;

.field public static final enum c:Lalb;

.field public static final enum d:Lalb;

.field public static final enum e:Lalb;

.field public static final enum f:Lalb;

.field private static final synthetic g:[Lalb;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lalb;

    const-string v1, "CLIENT_CONTEXTS"

    const-string v2, "client_contexts"

    invoke-direct {v0, v1, v4, v2}, Lalb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lalb;->a:Lalb;

    new-instance v0, Lalb;

    const-string v1, "CLIENT_CONTEXTS_ID"

    const-string v2, "client_contexts/#"

    invoke-direct {v0, v1, v5, v2}, Lalb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lalb;->b:Lalb;

    new-instance v0, Lalb;

    const-string v1, "APP_STATES"

    const-string v2, "app_states/#"

    invoke-direct {v0, v1, v6, v2}, Lalb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lalb;->c:Lalb;

    new-instance v0, Lalb;

    const-string v1, "APP_STATES_ID"

    const-string v2, "app_states/internal_id/#"

    invoke-direct {v0, v1, v7, v2}, Lalb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lalb;->d:Lalb;

    new-instance v0, Lalb;

    const-string v1, "APP_STATES_APP_ID"

    const-string v2, "app_states/#/app_id/*"

    invoke-direct {v0, v1, v8, v2}, Lalb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lalb;->e:Lalb;

    new-instance v0, Lalb;

    const-string v1, "APP_STATES_ACCOUNT_NAME"

    const/4 v2, 0x5

    const-string v3, "app_states/account_name/*"

    invoke-direct {v0, v1, v2, v3}, Lalb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lalb;->f:Lalb;

    const/4 v0, 0x6

    new-array v0, v0, [Lalb;

    sget-object v1, Lalb;->a:Lalb;

    aput-object v1, v0, v4

    sget-object v1, Lalb;->b:Lalb;

    aput-object v1, v0, v5

    sget-object v1, Lalb;->c:Lalb;

    aput-object v1, v0, v6

    sget-object v1, Lalb;->d:Lalb;

    aput-object v1, v0, v7

    sget-object v1, Lalb;->e:Lalb;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lalb;->f:Lalb;

    aput-object v2, v0, v1

    sput-object v0, Lalb;->g:[Lalb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-static {}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->b()Landroid/content/UriMatcher;

    move-result-object v0

    const-string v1, "com.google.android.gms.appstate"

    invoke-virtual {p0}, Lalb;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, p3, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lalb;
    .locals 1

    const-class v0, Lalb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lalb;

    return-object v0
.end method

.method public static values()[Lalb;
    .locals 1

    sget-object v0, Lalb;->g:[Lalb;

    invoke-virtual {v0}, [Lalb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lalb;

    return-object v0
.end method
