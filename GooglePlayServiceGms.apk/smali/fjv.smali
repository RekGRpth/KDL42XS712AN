.class public final Lfjv;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 2

    const/16 v0, 0x50

    const-string v1, "com.google.android.gms"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "com.google.android.gms.test.people"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.test.aspen"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.test.plus"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "com.google.android.talk"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.google.android.apps.babel"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/16 v0, 0x75

    goto :goto_0

    :cond_3
    const-string v0, "com.google.android.play.games"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x76

    goto :goto_0

    :cond_4
    const-string v0, "com.google.android.apps.plus"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Exception;)Lffc;
    .locals 3

    instance-of v0, p1, Lane;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    check-cast p1, Lane;

    invoke-virtual {p1}, Lane;->b()Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v2}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "pendingIntent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v1, 0x4

    invoke-static {v1, v0}, Lffc;->a(ILandroid/os/Bundle;)Lffc;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Lamq;

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lffc;->a(ILandroid/os/Bundle;)Lffc;

    move-result-object v0

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lsp;

    if-eqz v0, :cond_2

    check-cast p1, Lsp;

    invoke-static {p1}, Lfjv;->a(Lsp;)V

    sget-object v0, Lffc;->e:Lffc;

    goto :goto_0

    :cond_2
    sget-object v0, Lffc;->d:Lffc;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 5

    instance-of v0, p0, Lsp;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lsp;

    iget-object v1, v0, Lsp;->a:Lrz;

    if-nez v1, :cond_0

    const-string v0, "none"

    :goto_0
    const-string v1, "%s [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, v0, Lsp;->a:Lrz;

    iget v0, v0, Lrz;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/RuntimeException;Ljava/lang/String;)V
    .locals 4

    if-nez p2, :cond_0

    throw p1

    :cond_0
    invoke-static {p0}, Lfgy;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    throw p1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string v0, "PeopleService"

    const-string v1, "Caught exception, but account doesn\'t exist.  Ignoring."

    invoke-static {v0, v1, p1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "%s (%s [%s])"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v1

    invoke-static {p2}, Lfjv;->a(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    instance-of v3, p2, Lamq;

    if-nez v3, :cond_0

    instance-of v3, p2, Lsp;

    if-nez v3, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    invoke-static {p0, v2, p2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    goto :goto_1
.end method

.method public static a(Lsp;)V
    .locals 2

    sget-object v0, Lfbd;->T:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lsp;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v1, p0, Lsb;

    if-nez v1, :cond_2

    instance-of v0, v0, Lbnu;

    if-eqz v0, :cond_0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Parse error"

    invoke-direct {v0, v1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
