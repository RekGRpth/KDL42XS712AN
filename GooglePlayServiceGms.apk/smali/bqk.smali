.class final Lbqk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Vector;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbqk;->a:Ljava/util/Vector;

    const/16 v0, 0x14

    iput v0, p0, Lbqk;->b:I

    iput v1, p0, Lbqk;->c:I

    iput v1, p0, Lbqk;->d:I

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lbqk;-><init>()V

    return-void
.end method


# virtual methods
.method final declared-synchronized a()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqk;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lbqk;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lbqk;->d:I

    iget-object v0, p0, Lbqk;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Landroid/os/Message;Ljava/lang/String;Lbqh;Lbqh;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lbqk;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbqk;->d:I

    iget-object v0, p0, Lbqk;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    iget v1, p0, Lbqk;->b:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lbqk;->a:Ljava/util/Vector;

    new-instance v1, Lbqj;

    invoke-direct {v1, p1, p2, p3, p4}, Lbqj;-><init>(Landroid/os/Message;Ljava/lang/String;Lbqh;Lbqh;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbqk;->a:Ljava/util/Vector;

    iget v1, p0, Lbqk;->c:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqj;

    iget v1, p0, Lbqk;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbqk;->c:I

    iget v1, p0, Lbqk;->c:I

    iget v2, p0, Lbqk;->b:I

    if-lt v1, v2, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lbqk;->c:I

    :cond_1
    invoke-virtual {v0, p1, p2, p3, p4}, Lbqj;->a(Landroid/os/Message;Ljava/lang/String;Lbqh;Lbqh;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lbqk;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(I)Lbqj;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lbqk;->c:I

    add-int/2addr v0, p1

    iget v1, p0, Lbqk;->b:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lbqk;->b:I

    sub-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lbqk;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lt v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lbqk;->a:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqk;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
