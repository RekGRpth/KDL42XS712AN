.class public final Ledy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ldvn;

.field public b:Landroid/support/v7/widget/SearchView;

.field public c:I

.field public d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field public f:Z

.field g:Landroid/os/Handler;

.field protected h:Ljava/lang/Runnable;

.field private i:Leec;


# direct methods
.method public constructor <init>(Ldvn;Leec;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ledz;

    invoke-direct {v0, p0}, Ledz;-><init>(Ledy;)V

    iput-object v0, p0, Ledy;->h:Ljava/lang/Runnable;

    iput-object p1, p0, Ledy;->a:Ldvn;

    iput-object p2, p0, Ledy;->i:Leec;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ledy;->g:Landroid/os/Handler;

    const v0, 0x7f0b02d4    # com.google.android.gms.R.string.games_search_players_hint

    iput v0, p0, Ledy;->c:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Ledy;->f:Z

    iget-object v0, p0, Ledy;->g:Landroid/os/Handler;

    iget-object v1, p0, Ledy;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Ledy;->a:Ldvn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldvn;->setTitle(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    const-string v0, "savedStatePreviousQuery"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ledy;->d:Ljava/lang/String;

    iget-object v0, p0, Ledy;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ledy;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ledy;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Ledy;->d:Ljava/lang/String;

    iget-object v0, p0, Ledy;->i:Leec;

    invoke-interface {v0, p1}, Leec;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected final b()V
    .locals 3

    iget-object v0, p0, Ledy;->b:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledy;->a:Ldvn;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Ldvn;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Ledy;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 2

    iget-object v0, p0, Ledy;->b:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Ledy;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Ledy;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Ledy;->b()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
