.class public final Leer;
.super Limv;
.source "SourceFile"


# static fields
.field public static b:Landroid/os/Messenger;


# instance fields
.field private Q:Landroid/content/Context;

.field private R:Lefw;

.field private S:Lefq;

.field private T:Leeu;

.field private U:Lefs;

.field private V:Legb;

.field private W:Lcom/google/android/gms/gcm/GcmProvisioning;

.field private X:Landroid/os/PowerManager$WakeLock;

.field a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lefw;Lefq;Lefs;Leeu;Legb;Lcom/google/android/gms/gcm/GcmProvisioning;)V
    .locals 5

    const/4 v4, 0x5

    invoke-direct {p0}, Limv;-><init>()V

    iput v4, p0, Leer;->a:I

    iput-object p2, p0, Leer;->R:Lefw;

    const/4 v0, -0x1

    iput v0, p0, Leer;->d:I

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "GCM_CONN"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Leer;->X:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Leer;->X:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iput-object p3, p0, Leer;->S:Lefq;

    iget-object v0, p0, Leer;->S:Lefq;

    invoke-virtual {v0, p0}, Lefq;->a(Limv;)V

    iput-object p4, p0, Leer;->U:Lefs;

    iget-object v0, p0, Leer;->U:Lefs;

    invoke-virtual {v0, p0}, Lefs;->a(Leer;)V

    iget-object v0, p0, Leer;->U:Lefs;

    invoke-virtual {v0, p5}, Lefs;->a(Leeu;)V

    iput-object p5, p0, Leer;->T:Leeu;

    iget-object v0, p0, Leer;->T:Leeu;

    invoke-virtual {v0, p0}, Leeu;->a(Limv;)V

    iput-object p6, p0, Leer;->V:Legb;

    iget-object v0, p0, Leer;->r:Ljava/util/List;

    iget-object v1, p0, Leer;->R:Lefw;

    invoke-virtual {v1}, Lefw;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object p7, p0, Leer;->W:Lcom/google/android/gms/gcm/GcmProvisioning;

    iput-object p1, p0, Leer;->Q:Landroid/content/Context;

    iget-object v0, p0, Leer;->Q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gtalk_ssl_handshake_timeout_ms"

    const v2, 0xea60

    invoke-static {v0, v1, v2}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Leer;->Q:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gcm_so_timeout"

    iget v3, p0, Leer;->C:I

    invoke-static {v1, v2, v3}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Leer;->C:I

    :try_start_0
    new-instance v1, Landroid/net/SSLSessionCache;

    iget-object v2, p0, Leer;->Q:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/net/SSLSessionCache;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Landroid/net/SSLCertificateSocketFactory;->getDefault(ILandroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Leer;->t:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Leer;->Q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gcm_message_remove_ack_threshold"

    invoke-static {v0, v1, v4}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leer;->a:I

    return-void

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocketFactory;

    iput-object v0, p0, Leer;->t:Ljavax/net/ssl/SSLSocketFactory;

    const-string v0, "GCM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Use default socket factory "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Leer;)Leeu;
    .locals 1

    iget-object v0, p0, Leer;->T:Leeu;

    return-object v0
.end method

.method static a(Ljava/lang/String;Lizk;)V
    .locals 4

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "data"

    invoke-virtual {p1}, Lizk;->d()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "type"

    invoke-static {p1}, Lina;->a(Lizk;)B

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    const-string v2, "tag"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    sget-object v1, Leer;->b:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GCM"

    const-string v2, "Failed to instrument"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    sput-object v0, Leer;->b:Landroid/os/Messenger;

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    iget-object v0, p0, Leer;->R:Lefw;

    iget-object v1, p0, Leer;->T:Leeu;

    invoke-virtual {v0, p0, v1}, Lefw;->a(Limv;Leeu;)V

    return-void
.end method

.method protected final a(ILjava/lang/String;Z)V
    .locals 7

    const/4 v6, -0x1

    iget-object v0, p0, Leer;->S:Lefq;

    invoke-virtual {v0}, Lefq;->c()V

    iget-object v0, p0, Leer;->S:Lefq;

    invoke-virtual {v0}, Lefq;->a()V

    iget-object v0, p0, Leer;->U:Lefs;

    invoke-virtual {v0, p1, p3}, Lefs;->a(IZ)V

    iget-object v0, p0, Leer;->U:Lefs;

    invoke-virtual {v0}, Lefs;->e()I

    move-result v1

    invoke-virtual {p0}, Leer;->l()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    shl-int/lit8 v0, v1, 0x8

    add-int/2addr v0, p1

    const v3, 0x31ce3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    sparse-switch p1, :sswitch_data_0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-ne v1, v6, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Close err:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NO NETWORK  time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    :goto_1
    sget-object v0, Leer;->b:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    const-string v0, "in"

    new-instance v1, Lind;

    invoke-direct {v1}, Lind;-><init>()V

    invoke-static {v0, v1}, Leer;->a(Ljava/lang/String;Lizk;)V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gcm.DISCONNECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Leer;->Q:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :sswitch_0
    const-string v0, "IOException"

    goto :goto_0

    :sswitch_1
    const-string v0, "FIN"

    goto :goto_0

    :sswitch_2
    const-string v0, "RST"

    goto :goto_0

    :sswitch_3
    const-string v0, "FailedHeartbeat"

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Close err:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " net:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Leer;->g()I

    move-result v0

    const/high16 v2, 0x1000000

    shl-int/lit8 v3, v0, 0x10

    add-int/2addr v2, v3

    shl-int/lit8 v3, p1, 0x8

    add-int/2addr v2, v3

    add-int/2addr v2, v1

    const v3, 0x31ce2

    invoke-static {v3, v2}, Landroid/util/EventLog;->writeEvent(II)I

    if-ne v1, v6, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed connection NO NETWORK ev:1"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " err:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed connection ev:1"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " err:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " net:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_3
        0x10 -> :sswitch_0
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method protected final a(Linm;)V
    .locals 9

    const-wide/16 v7, 0x3e8

    const-wide/16 v5, 0x0

    iget-object v0, p0, Leer;->R:Lefw;

    invoke-virtual {v0}, Lefw;->b()V

    iget-object v0, p0, Leer;->U:Lefs;

    invoke-virtual {v0}, Lefs;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Linm;->b(I)Linm;

    iget-object v0, p0, Leer;->V:Legb;

    if-eqz v0, :cond_0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Leer;->V:Legb;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Legb;->a(Ljava/util/Map;I)Z

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v4, Linp;

    invoke-direct {v4}, Linp;-><init>()V

    invoke-virtual {v4, v0}, Linp;->a(Ljava/lang/String;)Linp;

    invoke-virtual {v4, v1}, Linp;->b(Ljava/lang/String;)Linp;

    invoke-virtual {p1, v4}, Linm;->a(Linp;)Linm;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Leer;->U:Lefs;

    iget-wide v0, v0, Lefs;->i:J

    cmp-long v0, v0, v5

    if-lez v0, :cond_1

    new-instance v0, Linp;

    invoke-direct {v0}, Linp;-><init>()V

    const-string v1, "networkOn"

    invoke-virtual {v0, v1}, Linp;->a(Ljava/lang/String;)Linp;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Leer;->U:Lefs;

    iget-wide v3, v3, Lefs;->i:J

    sub-long/2addr v1, v3

    div-long/2addr v1, v7

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Linp;->b(Ljava/lang/String;)Linp;

    :cond_1
    iget-object v0, p0, Leer;->U:Lefs;

    iget-wide v0, v0, Lefs;->j:J

    cmp-long v0, v0, v5

    if-lez v0, :cond_2

    new-instance v0, Linp;

    invoke-direct {v0}, Linp;-><init>()V

    const-string v1, "networkOff"

    invoke-virtual {v0, v1}, Linp;->a(Ljava/lang/String;)Linp;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Leer;->U:Lefs;

    iget-wide v3, v3, Lefs;->j:J

    sub-long/2addr v1, v3

    div-long/2addr v1, v7

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Linp;->b(Ljava/lang/String;)Linp;

    :cond_2
    return-void
.end method

.method protected final a(Linn;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p1, Linn;->a:Linf;

    if-eqz v0, :cond_0

    iget v1, v0, Linf;->a:I

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Login error "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Linf;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Linf;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    iget-object v0, p0, Leer;->U:Lefs;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Lefs;->a(IZ)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "GCM"

    const-string v1, "Connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Leer;->S:Lefq;

    invoke-virtual {v0, p1}, Lefq;->a(Linn;)V

    iget-object v0, p0, Leer;->U:Lefs;

    invoke-virtual {v0}, Lefs;->f()V

    iget-object v0, p0, Leer;->W:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->c()V

    const v0, 0x31ce2

    invoke-static {v0, v3}, Landroid/util/EventLog;->writeEvent(II)I

    const-string v0, "Connected"

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    sget-object v0, Leer;->b:Landroid/os/Messenger;

    if-eqz v0, :cond_1

    const-string v0, "login"

    invoke-static {v0, p1}, Leer;->a(Ljava/lang/String;Lizk;)V

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gcm.CONNECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Leer;->Q:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected final a(Lizk;)V
    .locals 2

    sget-object v0, Leer;->b:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    const-string v0, "out"

    invoke-static {v0, p1}, Leer;->a(Ljava/lang/String;Lizk;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Message written:  "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Leer;->b(Ljava/lang/String;)V

    instance-of v0, p1, Line;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Flushed GCM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    check-cast p1, Line;

    iget-object v1, p1, Line;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/gcm/GcmSenderProxy;->a()V

    :cond_1
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Leer;->R:Lefw;

    invoke-virtual {v0, p1}, Lefw;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    const-string v0, "GCM"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "GCM"

    invoke-static {v0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Leer;->R:Lefw;

    new-instance v1, Lees;

    invoke-direct {v1, p0, p1}, Lees;-><init>(Leer;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lefw;->a(Lefz;)V

    iget-object v0, p0, Leer;->R:Lefw;

    invoke-virtual {v0, p1}, Lefw;->a(Ljava/util/List;)I

    move-result v0

    iget v1, p0, Leer;->a:I

    if-lez v1, :cond_0

    iget v1, p0, Leer;->a:I

    if-lt v0, v1, :cond_0

    invoke-static {}, Lina;->a()Linl;

    move-result-object v0

    invoke-virtual {p0, v0}, Limv;->d(Lizk;)V

    :cond_0
    iget-object v0, p0, Leer;->U:Lefs;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lefs;->c(Z)V

    return-void
.end method

.method public final a(ZILjava/lang/String;II)V
    .locals 4

    if-eqz p1, :cond_0

    neg-int p2, p2

    :cond_0
    const v0, 0x31ce5

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Leer;->X:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    const-string v0, "GCM/Wake"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCM/Wake"

    const-string v1, "Acquired read lock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected final b(Lizk;)V
    .locals 4

    instance-of v0, p1, Line;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Line;

    const-string v1, "GCM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GCM message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Line;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Line;->g:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Leer;->U:Lefs;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lefs;->c(Z)V

    iget-object v0, p0, Leer;->S:Lefq;

    invoke-virtual {v0}, Lefq;->b()V

    sget-object v0, Leer;->b:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    const-string v0, "in"

    invoke-static {v0, p1}, Leer;->a(Ljava/lang/String;Lizk;)V

    :cond_0
    instance-of v0, p1, Line;

    if-eqz v0, :cond_1

    iget-object v0, p0, Leer;->T:Leeu;

    check-cast p1, Line;

    invoke-virtual {v0, p1}, Leeu;->c(Line;)V

    :cond_1
    return-void

    :cond_2
    const-string v0, "GCM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final b(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Leer;->R:Lefw;

    invoke-virtual {v0, p1}, Lefw;->b(Ljava/util/List;)V

    return-void
.end method

.method public final c()V
    .locals 2

    const-string v0, "GCM/Wake"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCM/Wake"

    const-string v1, "Released read lock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Leer;->X:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method

.method protected final c(Lizk;)V
    .locals 5

    const-wide/16 v0, 0x0

    iget-object v2, p0, Leer;->T:Leeu;

    iget-object v2, v2, Leeu;->f:Lega;

    iget-boolean v2, v2, Lega;->h:Z

    iget-object v3, p0, Leer;->T:Leeu;

    iget-object v3, v3, Leeu;->f:Lega;

    invoke-virtual {v3}, Lega;->a()Z

    move-result v4

    if-eqz v2, :cond_0

    move-wide v2, v0

    :goto_0
    if-eqz v4, :cond_1

    :goto_1
    or-long/2addr v0, v2

    invoke-static {p1, v0, v1}, Lina;->a(Lizk;J)V

    return-void

    :cond_0
    const-wide/16 v2, 0x2

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x1

    goto :goto_1
.end method

.method protected final d()V
    .locals 1

    iget-object v0, p0, Leer;->S:Lefq;

    invoke-virtual {v0}, Lefq;->c()V

    iget-object v0, p0, Leer;->T:Leeu;

    invoke-virtual {v0}, Leeu;->e()V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    const-wide/16 v6, 0x3e8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Limv;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Leer;->i:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Leer;->g:Ljava/net/Socket;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\nconnectTime="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Leer;->m()J

    move-result-wide v2

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Leer;->J:J

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\ndisconnectTime="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Leer;->v:J

    sub-long/2addr v2, v4

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Leer;->J:J

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\nlostConnection="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Leer;->v:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
