.class final Lcyn;
.super Lbjg;
.source "SourceFile"

# interfaces
.implements Ldlh;


# instance fields
.field final synthetic a:Lcwm;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcwm;Lbds;Lcom/google/android/gms/common/api/Status;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcyn;->a:Lcwm;

    invoke-direct {p0, p1, p2}, Lbjg;-><init>(Lbje;Ljava/lang/Object;)V

    iput-object p3, p0, Lcyn;->c:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lcyn;->d:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public final L_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcyn;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a(I)Ldkz;
    .locals 2

    invoke-static {p1}, Lded;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcyn;->d:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcyn;->d:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    new-instance v1, Ldkz;

    invoke-direct {v1, v0}, Ldkz;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    move-object v0, v1

    goto :goto_0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lbds;

    invoke-interface {p1, p0}, Lbds;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lcyn;->d:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcyn;->d:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected final r_()V
    .locals 0

    invoke-virtual {p0}, Lcyn;->c()V

    return-void
.end method
