.class public Lfpw;
.super Lfqy;
.source "SourceFile"

# interfaces
.implements Lfpv;


# static fields
.field private static final ac:Ljava/lang/String;


# instance fields
.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:Ljava/lang/String;

.field private aj:Lfro;

.field private ak:Lfrj;

.field private al:Lfrl;

.field private am:Lfrp;

.field private an:Lfrk;

.field private ao:Lfpy;

.field protected i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "account_name = ? AND (data1 LIKE ? OR "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lfqk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE ? OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lfqk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfpw;->ac:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lfqy;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lfpw;->ai:Ljava/lang/String;

    return-void
.end method

.method static synthetic N()Ljava/lang/String;
    .locals 1

    sget-object v0, Lfpw;->ac:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Ljava/lang/String;)Lfpw;
    .locals 2

    new-instance v0, Lfpw;

    invoke-direct {v0}, Lfpw;-><init>()V

    invoke-static/range {p0 .. p9}, Lfpw;->b(Ljava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfpw;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lfpw;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfpw;->ai:Ljava/lang/String;

    return-object v0
.end method

.method protected static b(Ljava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    invoke-static {p0, p1, p8, p9}, Lfqy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "searchGroups"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "searchCircles"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "searchPeople"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "searchWeb"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "searchDevice"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "searchEmail"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Lfqy;->E_()V

    invoke-virtual {p0}, Lfpw;->U()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lfpu;

    invoke-virtual {v0}, Lfpu;->j()V

    return-void
.end method

.method protected final J()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected K()Lfpu;
    .locals 6

    new-instance v0, Lfpu;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v2, Lfqr;

    invoke-interface {v2}, Lfqr;->k()Lfrb;

    move-result-object v2

    iget-object v3, p0, Lfqy;->Z:Ljava/lang/String;

    iget-object v4, p0, Lfqy;->aa:Ljava/lang/String;

    iget-boolean v5, p0, Lfpw;->i:Z

    invoke-direct/range {v0 .. v5}, Lfpu;-><init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, p0}, Lfpu;->a(Lfpv;)Lfpu;

    move-result-object v0

    return-object v0
.end method

.method protected final L()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lfpw;->ad:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfpw;->s()Lak;

    move-result-object v0

    new-instance v1, Lfqa;

    invoke-direct {v1, p0, v3}, Lfqa;-><init>(Lfpw;B)V

    invoke-virtual {v0, v3, v1}, Lak;->a(ILal;)Lcb;

    move-result-object v0

    check-cast v0, Lfrl;

    iput-object v0, p0, Lfpw;->al:Lfrl;

    :cond_0
    iget-boolean v0, p0, Lfpw;->ae:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lfpw;->s()Lak;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lfpx;

    invoke-direct {v2, p0, v3}, Lfpx;-><init>(Lfpw;B)V

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    move-result-object v0

    check-cast v0, Lfrj;

    iput-object v0, p0, Lfpw;->ak:Lfrj;

    :cond_1
    iget-boolean v0, p0, Lfpw;->af:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lfpw;->s()Lak;

    move-result-object v0

    const/4 v1, 0x2

    new-instance v2, Lfqb;

    invoke-direct {v2, p0, v3}, Lfqb;-><init>(Lfpw;B)V

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    move-result-object v0

    check-cast v0, Lfro;

    iput-object v0, p0, Lfpw;->aj:Lfro;

    :cond_2
    iget-boolean v0, p0, Lfpw;->ag:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lfpw;->s()Lak;

    move-result-object v0

    const/4 v1, 0x3

    new-instance v2, Lfqc;

    invoke-direct {v2, p0, v3}, Lfqc;-><init>(Lfpw;B)V

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    move-result-object v0

    check-cast v0, Lfrp;

    iput-object v0, p0, Lfpw;->am:Lfrp;

    :cond_3
    iget-boolean v0, p0, Lfpw;->i:Z

    if-eqz v0, :cond_4

    new-instance v0, Lfpy;

    invoke-direct {v0, p0, v3}, Lfpy;-><init>(Lfpw;B)V

    iput-object v0, p0, Lfpw;->ao:Lfpy;

    invoke-virtual {p0}, Lfpw;->s()Lak;

    move-result-object v0

    const/4 v1, 0x5

    iget-object v2, p0, Lfpw;->ao:Lfpy;

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    :cond_4
    iget-boolean v0, p0, Lfpw;->ah:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lfpw;->s()Lak;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Lfpz;

    invoke-direct {v2, p0, v3}, Lfpz;-><init>(Lfpw;B)V

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    move-result-object v0

    check-cast v0, Lfrk;

    iput-object v0, p0, Lfpw;->an:Lfrk;

    :cond_5
    return-void
.end method

.method protected synthetic M()Landroid/widget/BaseAdapter;
    .locals 1

    invoke-virtual {p0}, Lfpw;->K()Lfpu;

    move-result-object v0

    return-object v0
.end method

.method public final O_()V
    .locals 1

    iget-object v0, p0, Lfpw;->am:Lfrp;

    invoke-virtual {v0}, Lfrp;->l()V

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Lfqy;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfqr;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement AudienceSelectionFragmentHostActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    iput-object p1, p0, Lfpw;->ai:Ljava/lang/String;

    invoke-virtual {p0}, Lfpw;->U()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lfpu;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lfpu;->a(Z)V

    iget-object v0, p0, Lfpw;->al:Lfrl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpw;->al:Lfrl;

    iget-object v1, p0, Lfpw;->ai:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfrl;->a(Ljava/lang/String;)Lfrn;

    :cond_0
    iget-object v0, p0, Lfpw;->ak:Lfrj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfpw;->ak:Lfrj;

    iget-object v1, p0, Lfpw;->ai:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfrj;->a(Ljava/lang/String;)Lfrn;

    :cond_1
    iget-object v0, p0, Lfpw;->aj:Lfro;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfpw;->aj:Lfro;

    iget-object v1, p0, Lfpw;->ai:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfro;->a(Ljava/lang/String;)Lfrn;

    :cond_2
    iget-object v0, p0, Lfpw;->am:Lfrp;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfpw;->am:Lfrp;

    iget-object v1, p0, Lfpw;->ai:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lfrp;->a(Ljava/lang/String;I)Lfrp;

    :cond_3
    iget-object v0, p0, Lfpw;->ao:Lfpy;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lfpw;->s()Lak;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    iget-object v3, p0, Lfpw;->ao:Lfpy;

    invoke-virtual {v0, v1, v2, v3}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    :cond_4
    iget-object v0, p0, Lfpw;->an:Lfrk;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfpw;->an:Lfrk;

    iget-object v1, p0, Lfpw;->ai:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfrk;->a(Ljava/lang/String;)V

    :cond_5
    return-void

    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lfqy;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "searchGroups"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfpw;->ad:Z

    const-string v1, "searchCircles"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfpw;->ae:Z

    const-string v1, "searchPeople"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfpw;->af:Z

    const-string v1, "searchWeb"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfpw;->ag:Z

    const-string v1, "searchDevice"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfpw;->i:Z

    const-string v1, "searchEmail"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfpw;->ah:Z

    return-void
.end method

.method public final g_()V
    .locals 1

    invoke-virtual {p0}, Lfpw;->U()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lfpu;

    invoke-virtual {v0}, Lfpu;->k()V

    invoke-super {p0}, Lfqy;->g_()V

    return-void
.end method
