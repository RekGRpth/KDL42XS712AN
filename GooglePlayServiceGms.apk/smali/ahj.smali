.class public final Lahj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbq;


# instance fields
.field final a:Landroid/os/ConditionVariable;

.field b:Lbbo;

.field public final c:Lajd;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lahj;->a:Landroid/os/ConditionVariable;

    iput-object p1, p0, Lahj;->d:Landroid/content/Context;

    new-instance v0, Lajd;

    new-instance v1, Lahk;

    invoke-direct {v1, p0, v3}, Lahk;-><init>(Lahj;B)V

    new-instance v2, Lahl;

    invoke-direct {v2, p0, v3}, Lahl;-><init>(Lahj;B)V

    invoke-direct {v0, p1, v1, v2}, Lajd;-><init>(Landroid/content/Context;Lbbr;Lbbs;)V

    iput-object v0, p0, Lahj;->c:Lajd;

    return-void
.end method


# virtual methods
.method public final a(J)Lbbo;
    .locals 3

    invoke-virtual {p0}, Lahj;->a()V

    iget-object v0, p0, Lahj;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0, p1, p2}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0}, Lajd;->b()V

    new-instance v0, Lbbo;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbbo;-><init>(ILandroid/app/PendingIntent;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lahj;->b:Lbbo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lahj;->b:Lbbo;

    goto :goto_0

    :cond_1
    sget-object v0, Lbbo;->a:Lbbo;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lahj;->b:Lbbo;

    iget-object v0, p0, Lahj;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0}, Lajd;->a()V

    return-void
.end method

.method public final a(Lbbr;)V
    .locals 1

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0, p1}, Lajd;->a(Lbbr;)V

    return-void
.end method

.method public final a(Lbbs;)V
    .locals 1

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0, p1}, Lajd;->a(Lbbs;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0}, Lajd;->b()V

    return-void
.end method

.method public final b(Lbbr;)Z
    .locals 1

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0, p1}, Lajd;->b(Lbbr;)Z

    move-result v0

    return v0
.end method

.method public final c(Lbbr;)V
    .locals 1

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0, p1}, Lajd;->c(Lbbr;)V

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0}, Lajd;->d()Z

    move-result v0

    return v0
.end method

.method public final d_()Z
    .locals 1

    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0}, Lajd;->d_()Z

    move-result v0

    return v0
.end method

.method public final e()Lcom/google/android/gms/appdatasearch/StorageStats;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0}, Lajd;->e()Laje;

    move-result-object v0

    invoke-interface {v0}, Laje;->c()Lcom/google/android/gms/appdatasearch/StorageStats;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "AppDataSearchClient"

    const-string v2, "Get storage statistics failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0}, Lajd;->e()Laje;

    move-result-object v0

    invoke-interface {v0}, Laje;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "AppDataSearchClient"

    const-string v2, "clearUsageReportData failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method
