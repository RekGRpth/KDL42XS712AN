.class public Lfmf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Z

.field private static final b:Z

.field private static h:Lfmf;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lfmd;

.field private final e:Lfly;

.field private final f:Lfmg;

.field private final g:Limj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lfkv;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lfmf;->a:Z

    sget-object v0, Lfkv;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lfmf;->b:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lfmd;Lfly;Lfmg;Limj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lfmf;->c:Landroid/content/Context;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmd;

    iput-object v0, p0, Lfmf;->d:Lfmd;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfly;

    iput-object v0, p0, Lfmf;->e:Lfly;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmg;

    iput-object v0, p0, Lfmf;->f:Lfmg;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limj;

    iput-object v0, p0, Lfmf;->g:Limj;

    return-void
.end method

.method public static a()Lfmf;
    .locals 8

    const-class v7, Lfmf;

    monitor-enter v7

    :try_start_0
    sget-object v0, Lfmf;->h:Lfmf;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    new-instance v5, Limj;

    invoke-static {v1}, Lfmi;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-direct {v5, v1, v0, v2}, Limj;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    new-instance v0, Lfmf;

    invoke-static {}, Lfmd;->a()Lfmd;

    move-result-object v2

    invoke-static {}, Lfly;->a()Lfly;

    move-result-object v3

    new-instance v4, Lfmg;

    invoke-direct {v4}, Lfmg;-><init>()V

    sget-object v6, Lfkv;->c:Lbfy;

    invoke-virtual {v6}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, v4, Lfmg;->a:Ljava/lang/String;

    sget-object v6, Lfkv;->d:Lbfy;

    invoke-virtual {v6}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, v4, Lfmg;->b:Ljava/lang/String;

    sget-object v6, Lfkv;->i:Lbfy;

    invoke-virtual {v6}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lfmg;->c:I

    invoke-direct/range {v0 .. v5}, Lfmf;-><init>(Landroid/content/Context;Lfmd;Lfly;Lfmg;Limj;)V

    sput-object v0, Lfmf;->h:Lfmf;

    :cond_0
    sget-object v0, Lfmf;->h:Lfmf;

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string v1, "Uploader"

    const-string v2, "No account for auth token provided"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lfmf;->c:Landroid/content/Context;

    invoke-static {v1, p1}, Lbov;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Uploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " no longer exists, so no auth token."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lfmf;->c:Landroid/content/Context;

    iget-object v2, p0, Lfmf;->f:Lfmg;

    iget-object v2, v2, Lfmg;->b:Ljava/lang/String;

    invoke-static {v1, p1, v2}, Lamr;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lanf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get auth token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lanf;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get auth token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get auth token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lamq;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private a(Lfma;)Z
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p1, Lfma;->a:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v2, p1, Lfma;->b:Lflw;

    iget-object v2, v2, Lflw;->b:[B

    new-instance v3, Lflg;

    invoke-direct {v3}, Lflg;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Lflg;->a:J

    new-instance v4, Lfky;

    invoke-direct {v4}, Lfky;-><init>()V

    iget-boolean v5, v1, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->g:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lfmf;->c:Landroid/content/Context;

    invoke-static {}, Lbox;->a()J

    move-result-wide v5

    iput-wide v5, v4, Lfky;->a:J

    :cond_0
    iget-object v5, v1, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->f:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, v1, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->f:Ljava/lang/String;

    iput-object v5, v4, Lfky;->b:Ljava/lang/String;

    :cond_1
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v5, v4, Lfky;->c:I

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v5, v4, Lfky;->d:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v5, v4, Lfky;->e:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    iput-object v5, v4, Lfky;->f:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v5, v4, Lfky;->g:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->ID:Ljava/lang/String;

    iput-object v5, v4, Lfky;->h:Ljava/lang/String;

    iget-object v5, p0, Lfmf;->c:Landroid/content/Context;

    invoke-static {v5}, Lbox;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lfky;->j:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v5, v4, Lfky;->m:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lfky;->k:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lfky;->l:Ljava/lang/String;

    iget v5, v1, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lfky;->i:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iput-object v5, v4, Lfky;->n:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->BOARD:Ljava/lang/String;

    iput-object v5, v4, Lfky;->o:Ljava/lang/String;

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xe

    if-lt v5, v6, :cond_2

    invoke-static {}, Landroid/os/Build;->getRadioVersion()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    iput-object v5, v4, Lfky;->p:Ljava/lang/String;

    :cond_2
    new-instance v5, Lfla;

    invoke-direct {v5}, Lfla;-><init>()V

    const/4 v6, 0x4

    iput v6, v5, Lfla;->a:I

    iput-object v4, v5, Lfla;->b:Lfky;

    iput-object v5, v3, Lflg;->b:Lfla;

    iget v1, v1, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->d:I

    iput v1, v3, Lflg;->c:I

    const/4 v1, 0x1

    new-array v1, v1, [[B

    iput-object v1, v3, Lflg;->e:[[B

    iget-object v1, v3, Lflg;->e:[[B

    aput-object v2, v1, v0

    :try_start_0
    iget-object v1, p1, Lfma;->a:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v1, v1, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->e:Ljava/lang/String;

    iget-object v2, p0, Lfmf;->f:Lfmg;

    iget-object v2, v2, Lfmg;->a:Ljava/lang/String;

    iget-object v4, p0, Lfmf;->f:Lfmg;

    iget v4, v4, Lfmg;->c:I

    invoke-direct {p0, v1, v3, v2, v4}, Lfmf;->a(Ljava/lang/String;Lflg;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Network request failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Network request failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lflg;Ljava/lang/String;I)Z
    .locals 11

    const/16 v10, 0x190

    const/16 v9, 0x12c

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, p3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const-string v3, "https"

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->getURI()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v0, "Uploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Abort attempt to upload logs in plaintext: requestUrl="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1}, Lfmf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v4, "Authorization"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GoogleLogin auth="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v5, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v5, v4}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {p2}, Lizs;->a(Lizs;)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    invoke-virtual {v5}, Ljava/util/zip/GZIPOutputStream;->close()V

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    new-instance v5, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v5, v4}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    const-string v7, "gzip"

    invoke-virtual {v5, v7}, Lorg/apache/http/entity/ByteArrayEntity;->setContentEncoding(Ljava/lang/String;)V

    const-string v7, "application/x-gzip"

    invoke-virtual {v5, v7}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    sget-boolean v5, Lfmf;->a:Z

    if-eqz v5, :cond_2

    const-string v5, "Uploader"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Compressed log request from ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v6, v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] to ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v4, v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v4, p0, Lfmf;->g:Limj;

    invoke-virtual {v4, v2}, Limj;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    const/16 v6, 0xc8

    if-gt v6, v5, :cond_6

    if-ge v5, v9, :cond_6

    sget-boolean v1, Lfmf;->a:Z

    if-eqz v1, :cond_3

    const-string v1, "Uploader"

    const-string v3, "Successfully uploaded logs."

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    sget-boolean v1, Lfmf;->b:Z

    if-nez v1, :cond_5

    :try_start_0
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    const/16 v2, 0x80

    invoke-static {v1, v2}, Lfmi;->a(Ljava/io/InputStream;I)[B

    move-result-object v1

    invoke-static {v1}, Lflh;->a([B)Lflh;

    move-result-object v1

    iget-wide v2, v1, Lflh;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_5

    iget-wide v1, v1, Lflh;->a:J

    sget-boolean v3, Lfmf;->a:Z

    if-eqz v3, :cond_4

    const-string v3, "Uploader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "LogResponse: wait time in millis = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v3, p0, Lfmf;->d:Lfmd;

    invoke-virtual {v3}, Lfmd;->d()V

    iget-object v3, p0, Lfmf;->d:Lfmd;

    invoke-virtual {v3, v1, v2}, Lfmd;->a(J)V
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_5
    :goto_1
    move v1, v0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error parsing content: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lizr;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error getting the content of the response body: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v1

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error reading the content of the response body: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    if-gt v9, v5, :cond_9

    if-ge v5, v10, :cond_9

    if-lez p4, :cond_8

    const-string v1, "Location"

    invoke-interface {v2, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    if-nez v1, :cond_7

    const-string v1, "Uploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Status "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "... redirect: no location header"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_7
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, p4, -0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lfmf;->a(Ljava/lang/String;Lflg;Ljava/lang/String;I)Z

    move-result v0

    goto/16 :goto_1

    :cond_8
    const-string v0, "Uploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Server returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "... redirect, but no more redirects allowed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_1

    :cond_9
    if-ne v5, v10, :cond_a

    const-string v1, "Uploader"

    const-string v2, "Server returned 400... deleting local malformed logs"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_a
    const/16 v6, 0x191

    if-ne v5, v6, :cond_b

    const-string v0, "Uploader"

    const-string v2, "Server returned 401... invalidating auth token"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lfmf;->c:Landroid/content/Context;

    invoke-static {v0, v3}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_1

    :cond_b
    const/16 v3, 0x1f4

    if-ne v5, v3, :cond_c

    const-string v0, "Uploader"

    const-string v2, "Server returned 500... server crashed"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_1

    :cond_c
    const/16 v3, 0x1f5

    if-ne v5, v3, :cond_d

    const-string v0, "Uploader"

    const-string v2, "Server returned 501... service doesn\'t seem to exist"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_1

    :cond_d
    const/16 v3, 0x1f6

    if-ne v5, v3, :cond_e

    const-string v0, "Uploader"

    const-string v2, "Server returned 502... servers are down"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_1

    :cond_e
    const/16 v3, 0x1f7

    if-ne v5, v3, :cond_10

    const-string v3, "Retry-After"

    invoke-interface {v2, v3}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    :try_start_1
    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-string v5, "Uploader"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Server said to retry after "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lfmf;->d:Lfmd;

    invoke-virtual {v3}, Lfmd;->d()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    move v0, v1

    goto/16 :goto_1

    :catch_3
    move-exception v3

    const-string v3, "Uploader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown retry value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    goto :goto_2

    :cond_f
    const-string v1, "Uploader"

    const-string v2, "Status 503 without retry-after header"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_10
    const/16 v2, 0x1f8

    if-ne v5, v2, :cond_11

    const-string v0, "Uploader"

    const-string v2, "Server returned 504... timeout"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_1

    :cond_11
    const-string v1, "Uploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected error received from server: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method


# virtual methods
.method public final b()V
    .locals 3

    const-class v1, Lfmf;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lfmf;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Uploader"

    const-string v2, "uploading all staged data"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lfmf;->e:Lfly;

    invoke-virtual {v0}, Lfly;->c()V

    :goto_0
    iget-object v0, p0, Lfmf;->e:Lfly;

    invoke-virtual {v0}, Lfly;->e()Lfma;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, v0, Lfma;->b:Lflw;

    invoke-virtual {v2}, Lflw;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0, v0}, Lfmf;->a(Lfma;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, v0, Lfma;->b:Lflw;

    iget-boolean v2, v0, Lflw;->c:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Only one call to save() or delete() is permitted."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, v0, Lflw;->c:Z

    iget-object v0, p0, Lfmf;->d:Lfmd;

    invoke-virtual {v0}, Lfmd;->f()V

    :cond_2
    monitor-exit v1

    return-void

    :cond_3
    invoke-virtual {v0}, Lfma;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
