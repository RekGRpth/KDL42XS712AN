.class public final Ldsx;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldsx;->b:Ldad;

    iput-object p3, p0, Ldsx;->c:Ljava/lang/String;

    iput-object p4, p0, Ldsx;->d:Ljava/lang/String;

    iput-boolean p5, p0, Ldsx;->e:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldsx;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->b(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    iget-object v0, p0, Ldsx;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldsx;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Ldsx;->c:Ljava/lang/String;

    iget-boolean v2, p0, Ldsx;->e:Z

    invoke-virtual {p2, p1, v0, v1, v2}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Ldsx;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsx;->c:Ljava/lang/String;

    iget-object v4, p0, Ldsx;->d:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
