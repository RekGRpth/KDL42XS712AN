.class public final Ldtl;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldtl;->b:Ldad;

    iput-object p3, p0, Ldtl;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ldtl;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldtl;->b:Ldad;

    const/4 v0, 0x0

    iput-object v0, p0, Ldtl;->c:Ljava/lang/String;

    iput-object p3, p0, Ldtl;->d:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldtl;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->d(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    iget-object v0, p0, Ldtl;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldtl;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Ldtl;->c:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldtl;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Ldtl;->d:[Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
