.class final Leha;
.super Lorg/apache/http/entity/HttpEntityWrapper;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:J


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpEntity;Ljava/lang/String;IJJJJ)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/http/entity/HttpEntityWrapper;-><init>(Lorg/apache/http/HttpEntity;)V

    iput-object p2, p0, Leha;->a:Ljava/lang/String;

    iput p3, p0, Leha;->b:I

    iput-wide p4, p0, Leha;->c:J

    iput-wide p6, p0, Leha;->d:J

    iput-wide p8, p0, Leha;->e:J

    iput-wide p10, p0, Leha;->f:J

    return-void
.end method

.method static synthetic a(Leha;)J
    .locals 2

    iget-wide v0, p0, Leha;->f:J

    return-wide v0
.end method

.method static synthetic b(Leha;)I
    .locals 1

    iget v0, p0, Leha;->b:I

    return v0
.end method

.method static synthetic c(Leha;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Leha;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Leha;)J
    .locals 2

    iget-wide v0, p0, Leha;->e:J

    return-wide v0
.end method

.method static synthetic e(Leha;)J
    .locals 2

    iget-wide v0, p0, Leha;->c:J

    return-wide v0
.end method

.method static synthetic f(Leha;)J
    .locals 2

    iget-wide v0, p0, Leha;->d:J

    return-wide v0
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 2

    invoke-super {p0}, Lorg/apache/http/entity/HttpEntityWrapper;->getContent()Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lehb;

    invoke-direct {v1, p0, v0}, Lehb;-><init>(Leha;Ljava/io/InputStream;)V

    return-object v1
.end method
