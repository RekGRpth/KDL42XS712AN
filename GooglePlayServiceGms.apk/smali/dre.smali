.class public final Ldre;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldri;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ldaj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldaj;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldre;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Ldre;->b:Ldaj;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcun;)V
    .locals 5

    const/4 v0, 0x1

    new-instance v3, Lbmx;

    iget-object v1, p0, Ldre;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v3, v1, v0}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    const/16 v2, 0x3e8

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v3, p1}, Lbmx;->b(Landroid/content/Context;)Ljava/lang/String;

    iget-object v3, p0, Ldre;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    invoke-virtual {p2, p1, v3}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    :try_start_1
    iget-object v2, p0, Ldre;->b:Ldaj;

    invoke-interface {v2, v0, v1}, Ldaj;->a(ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x6

    goto :goto_0

    :catch_1
    move-exception v0

    instance-of v3, v0, Lane;

    if-eqz v3, :cond_1

    check-cast v0, Lane;

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3e9

    :goto_2
    move-object v4, v0

    move v0, v1

    move-object v1, v4

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v2, "SignInIntentService"

    invoke-virtual {v0}, Ldqq;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0}, Ldqq;->b()I

    move-result v0

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    move v1, v2

    goto :goto_2
.end method
