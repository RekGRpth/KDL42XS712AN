.class public final Lra;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lqz;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lqz;

    invoke-direct {v0}, Lqz;-><init>()V

    iput-object v0, p0, Lra;->a:Lqz;

    return-void
.end method


# virtual methods
.method public final a()Lqz;
    .locals 2

    iget-object v0, p0, Lra;->a:Lqz;

    invoke-virtual {v0}, Lqz;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lra;->a:Lqz;

    invoke-virtual {v1}, Lqz;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    iget-object v1, p0, Lra;->a:Lqz;

    invoke-static {v1, v0}, Lqz;->a(Lqz;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v0, p0, Lra;->a:Lqz;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lra;
    .locals 1

    iget-object v0, p0, Lra;->a:Lqz;

    invoke-static {v0, p1, p1}, Lqz;->a(Lqz;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lra;
    .locals 3

    iget-object v0, p0, Lra;->a:Lqz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lqz;->a(Lqz;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public final a([Ljava/lang/String;)Lra;
    .locals 3

    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lra;->a(Ljava/lang/String;)Lra;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method
