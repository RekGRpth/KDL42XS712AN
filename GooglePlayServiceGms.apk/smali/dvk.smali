.class public final Ldvk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/app/AlertDialog;

.field private final b:Landroid/view/View;

.field private c:Ldvm;

.field private d:Ldvl;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Ldvk;->b:Landroid/view/View;

    iget-object v0, p0, Ldvk;->b:Landroid/view/View;

    sget v1, Lxa;->h:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Ldvk;->b:Landroid/view/View;

    sget v1, Lxa;->i:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Ldvk;->a()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Ldvk;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Ldvk;->b:Landroid/view/View;

    sget v1, Lxa;->j:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Ldvk;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final a(Landroid/app/AlertDialog;)V
    .locals 1

    iput-object p1, p0, Ldvk;->a:Landroid/app/AlertDialog;

    const/4 v0, 0x0

    iput-object v0, p0, Ldvk;->c:Ldvm;

    return-void
.end method

.method public final a(Landroid/content/Context;II)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1, p2}, Leee;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v0, 0x104000a    # android.R.string.ok

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Ldvk;->a:Landroid/app/AlertDialog;

    iput-object v2, p0, Ldvk;->c:Ldvm;

    return-void
.end method

.method public final a(Ldvl;)V
    .locals 0

    iput-object p1, p0, Ldvk;->d:Ldvl;

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lxa;->h:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Ldvk;->a()V

    iget-object v0, p0, Ldvk;->d:Ldvl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldvk;->d:Ldvl;

    invoke-interface {v0}, Ldvl;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v1, Lxa;->i:I

    if-ne v0, v1, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Ldvk;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    const-string v0, "GamesButterbar"

    const-string v1, "showInfoTextDialog() called when no butter bar is visible."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ldvk;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldvk;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, p0, Ldvk;->a:Landroid/app/AlertDialog;

    invoke-static {v1, v0}, Leee;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Ldvk;->c:Ldvm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldvk;->c:Ldvm;

    goto :goto_0

    :cond_5
    const-string v0, "GamesButterbar"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
