.class public final Lgaz;
.super Lftm;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/server/ClientContext;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:Lbhw;

.field private final e:Lgax;

.field private final f:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lgax;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lftm;-><init>()V

    iput-object p1, p0, Lgaz;->a:Landroid/content/Context;

    iput-object p2, p0, Lgaz;->b:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p3, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1}, Lbhw;->a(Landroid/content/Context;)Lbhw;

    move-result-object v0

    iput-object v0, p0, Lgaz;->d:Lbhw;

    iput-object p4, p0, Lgaz;->e:Lgax;

    iput-object p5, p0, Lgaz;->f:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lgaz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.permission.GET_ACCOUNTS"

    iget-object v2, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Missing android.permission.GET_ACCOUNTS"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lgaz;->a(Lfsy;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    return-void
.end method

.method public final a(Lfsy;)V
    .locals 3

    iget-object v0, p0, Lgaz;->e:Lgax;

    iget-object v0, p0, Lgaz;->a:Landroid/content/Context;

    new-instance v1, Lgbx;

    iget-object v2, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p1}, Lgbx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lfsy;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lfsy;IIILjava/lang/String;)V
    .locals 7

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lgcf;

    iget-object v1, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lgcf;-><init>(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;Lfsy;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lfsy;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The userId parameter is required."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lgcd;

    iget-object v1, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v7, "vault"

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lgcd;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfsy;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lfsy;Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    const-string v0, "bounding_box"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :cond_0
    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    new-instance v2, Lgch;

    iget-object v3, p0, Lgaz;->d:Lbhw;

    invoke-direct {v2, v3, p2, v0, p1}, Lgch;-><init>(Lbhw;Landroid/net/Uri;ILfsy;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/service/ImageIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lfsy;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    .locals 3

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "momentJson must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Lgdb;

    iget-object v2, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, v0, p1}, Lgdb;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lfsy;)V

    iget-object v0, p0, Lgaz;->e:Lgax;

    iget-object v0, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "momentJson must be valid JSON"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lfsy;Ljava/lang/String;)V
    .locals 2

    const-string v0, "URL must not be null."

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lgck;

    iget-object v1, p0, Lgaz;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lgck;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lfsy;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lfsy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lgca;

    iget-object v1, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p3, p1}, Lgca;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lfsy;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Lfsy;Ljava/util/List;)V
    .locals 4

    const/4 v1, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "personId cannot be empty."

    invoke-static {v0, v3}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Lgbu;

    iget-object v1, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lgbu;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;Lfsy;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lgcp;

    iget-object v1, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1}, Lgcp;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lgaz;->e:Lgax;

    iget-object v0, p0, Lgaz;->a:Landroid/content/Context;

    new-instance v1, Lgbn;

    iget-object v2, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lgbn;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final b(Lfsy;)V
    .locals 3

    iget-object v0, p0, Lgaz;->e:Lgax;

    iget-object v0, p0, Lgaz;->a:Landroid/content/Context;

    new-instance v1, Lgbn;

    iget-object v2, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lgbn;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    iget-object v0, p0, Lgaz;->e:Lgax;

    iget-object v0, p0, Lgaz;->a:Landroid/content/Context;

    new-instance v1, Lgcr;

    iget-object v2, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p1}, Lgcr;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lfsy;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final b(Lfsy;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lgbo;

    iget-object v1, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lgbo;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lfsy;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Lfsy;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The userId parameter is required."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lgbv;

    iget-object v1, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lgbv;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lfsy;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lfsy;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final e(Lfsy;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The userId parameter is required."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lgbw;

    iget-object v1, p0, Lgaz;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lgbw;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lfsy;)V

    iget-object v1, p0, Lgaz;->e:Lgax;

    iget-object v1, p0, Lgaz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
