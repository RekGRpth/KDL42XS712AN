.class public final Lbw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lbx;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    new-instance v0, Lca;

    invoke-direct {v0}, Lca;-><init>()V

    sput-object v0, Lbw;->a:Lbx;

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    new-instance v0, Lbz;

    invoke-direct {v0}, Lbz;-><init>()V

    sput-object v0, Lbw;->a:Lbx;

    goto :goto_0

    :cond_1
    new-instance v0, Lby;

    invoke-direct {v0}, Lby;-><init>()V

    sput-object v0, Lbw;->a:Lbx;

    goto :goto_0
.end method

.method public static a(Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 1

    sget-object v0, Lbw;->a:Lbx;

    invoke-interface {v0, p0}, Lbx;->a(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
