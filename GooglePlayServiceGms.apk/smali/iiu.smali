.class public final Liiu;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/reporting/service/DispatchingService;

.field private final b:Lhvb;

.field private c:Lcom/google/android/location/reporting/LocationReportingController;

.field private d:Liht;

.field private e:Lihs;

.field private f:Lihl;

.field private g:Lijj;

.field private h:Liju;

.field private final i:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/content/Context;Landroid/os/Looper;)V
    .locals 17

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Liiu;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lihq;

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Lcom/google/android/location/reporting/service/DispatchingService;)Lihy;

    move-result-object v3

    invoke-direct {v2, v3}, Lihq;-><init>(Lihy;)V

    new-instance v4, Lcom/google/android/location/reporting/LocationRecordStore;

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v2}, Lcom/google/android/location/reporting/LocationRecordStore;-><init>(Landroid/content/Context;Lihq;)V

    new-instance v5, Lcom/google/android/location/reporting/DetectedActivityStore;

    move-object/from16 v0, p2

    invoke-direct {v5, v0, v2}, Lcom/google/android/location/reporting/DetectedActivityStore;-><init>(Landroid/content/Context;Lihq;)V

    new-instance v6, Lcom/google/android/location/reporting/ApiMetadataStore;

    move-object/from16 v0, p2

    invoke-direct {v6, v0, v2}, Lcom/google/android/location/reporting/ApiMetadataStore;-><init>(Landroid/content/Context;Lihq;)V

    invoke-static/range {p2 .. p2}, Lhvb;->a(Landroid/content/Context;)Lhvb;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Liiu;->b:Lhvb;

    new-instance v2, Lcom/google/android/location/reporting/LocationReportingController;

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Lcom/google/android/location/reporting/service/DispatchingService;)Lihy;

    move-result-object v7

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Lijb;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Liiu;->b:Lhvb;

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/reporting/LocationReportingController;-><init>(Landroid/content/Context;Lihn;Lihn;Lihn;Lihy;Lijb;Lhvb;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Liiu;->c:Lcom/google/android/location/reporting/LocationReportingController;

    new-instance v2, Liht;

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Lijb;

    move-result-object v3

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Lcom/google/android/location/reporting/service/DispatchingService;)Lihy;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-direct {v2, v0, v3, v7, v6}, Liht;-><init>(Landroid/content/Context;Lijb;Lihy;Lihn;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Liiu;->d:Liht;

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lijt;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Lijb;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v13, v0, Liiu;->c:Lcom/google/android/location/reporting/LocationReportingController;

    move-object/from16 v0, p0

    iget-object v11, v0, Liiu;->d:Liht;

    invoke-static/range {p2 .. p2}, Likh;->a(Landroid/content/Context;)V

    new-instance v10, Lijx;

    move-object/from16 v0, p2

    invoke-direct {v10, v0}, Lijx;-><init>(Landroid/content/Context;)V

    sget-object v2, Liae;->e:Liae;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v2

    iget v14, v2, Liad;->d:I

    const-string v2, "wifi"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/net/wifi/WifiManager;

    new-instance v7, Lihs;

    new-instance v15, Lbpg;

    invoke-direct {v15}, Lbpg;-><init>()V

    move-object v9, v4

    invoke-direct/range {v7 .. v16}, Lihs;-><init>(Lijb;Lihn;Lijx;Liht;Lcom/google/android/location/reporting/StateReporter;Lcom/google/android/location/reporting/LocationReportingController;ILbpe;Landroid/net/wifi/WifiManager;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Liiu;->e:Lihs;

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lijt;

    move-result-object v11

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Lijb;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v12, v0, Liiu;->c:Lcom/google/android/location/reporting/LocationReportingController;

    move-object/from16 v0, p0

    iget-object v10, v0, Liiu;->d:Liht;

    invoke-static/range {p2 .. p2}, Likh;->a(Landroid/content/Context;)V

    new-instance v7, Lihl;

    new-instance v13, Lbpg;

    invoke-direct {v13}, Lbpg;-><init>()V

    move-object v9, v5

    invoke-direct/range {v7 .. v13}, Lihl;-><init>(Lijb;Lihn;Liht;Lcom/google/android/location/reporting/StateReporter;Lcom/google/android/location/reporting/LocationReportingController;Lbpe;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Liiu;->f:Lihl;

    new-instance v2, Lijj;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lijj;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Liiu;->g:Lijj;

    new-instance v7, Liju;

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lijt;

    move-result-object v9

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Lcom/google/android/location/reporting/service/DispatchingService;)Lihy;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Liiu;->c:Lcom/google/android/location/reporting/LocationReportingController;

    move-object/from16 v8, p2

    move-object v12, v4

    move-object v13, v5

    move-object v14, v6

    invoke-direct/range {v7 .. v14}, Liju;-><init>(Landroid/content/Context;Lijt;Lihy;Lcom/google/android/location/reporting/LocationReportingController;Lcom/google/android/location/reporting/LocationRecordStore;Lcom/google/android/location/reporting/DetectedActivityStore;Lcom/google/android/location/reporting/ApiMetadataStore;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Liiu;->h:Liju;

    const-string v2, "power"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/location/reporting/service/DispatchingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    const/4 v3, 0x1

    const-string v4, "UlrDispatchingService"

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Liiu;->i:Landroid/os/PowerManager$WakeLock;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 13

    const-wide/16 v11, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Liiu;->i:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-static {}, Lill;->a()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DispatchingService ignoring intent for non-foreground user: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iget-object v0, p0, Liiu;->i:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v4, "GCoreUlr"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DispatchingService dispatching "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v4, "com.google.android.location.reporting.ACTION_LOCATION"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v1, p0, Liiu;->e:Lihs;

    invoke-virtual {v1, v0}, Lihs;->a(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Liiu;->i:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    :cond_3
    :try_start_2
    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Legacy intent "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", updating active state to cancel it"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Liiu;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->d(Lcom/google/android/location/reporting/service/DispatchingService;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Liiu;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Liiu;->b:Lhvb;

    invoke-virtual {v1, v0}, Lhvb;->b(Landroid/app/PendingIntent;)V

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    goto :goto_0

    :cond_5
    const-string v4, "com.google.android.location.reporting.ACTION_ACTIVITY"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    iget-object v4, p0, Liiu;->f:Lihl;

    const-string v1, "GCoreUlr"

    const/4 v5, 0x3

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ActivityReceiver received "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; lowPowerMode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lhkr;->a()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; mState: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v4, Lihl;->a:Lijb;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-static {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v5

    if-nez v5, :cond_7

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " has no activity"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_7
    iget-object v1, v4, Lihl;->e:Lbpe;

    invoke-interface {v1}, Lbpe;->b()J

    move-result-wide v6

    iget-object v1, v4, Lihl;->a:Lijb;

    invoke-virtual {v1}, Lijb;->d()J

    move-result-wide v8

    invoke-virtual {v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    const/4 v10, 0x6

    if-ne v1, v10, :cond_9

    move v1, v2

    :goto_1
    if-nez v1, :cond_a

    cmp-long v1, v8, v11

    if-eqz v1, :cond_a

    sub-long v8, v6, v8

    sget-object v1, Lijs;->i:Lbfy;

    invoke-virtual {v1}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v1, v8, v10

    if-gtz v1, :cond_a

    const-string v1, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Throttling activity because it\'s only been "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " millis"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move v1, v2

    :goto_2
    if-nez v1, :cond_0

    invoke-static {v0}, Lihs;->b(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v4, Lihl;->c:Lcom/google/android/location/reporting/StateReporter;

    invoke-interface {v0}, Lcom/google/android/location/reporting/StateReporter;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/ReportingConfig;->isReportingActive()Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, v4, Lihl;->d:Lcom/google/android/location/reporting/LocationReportingController;

    iget-object v2, v4, Lihl;->e:Lbpe;

    invoke-static {v0, v1, v2}, Lihs;->a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/location/reporting/LocationReportingController;Lbpe;)V

    goto/16 :goto_0

    :cond_9
    move v1, v3

    goto :goto_1

    :cond_a
    iget-object v1, v4, Lihl;->a:Lijb;

    invoke-virtual {v1, v6, v7}, Lijb;->a(J)V

    move v1, v3

    goto :goto_2

    :cond_b
    invoke-virtual {v4, v5, v0}, Lihl;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Lcom/google/android/location/reporting/service/ReportingConfig;)V

    goto/16 :goto_0

    :cond_c
    const-string v2, "com.google.android.location.reporting.ACTION_UPDATE_WORLD"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v1, p0, Liiu;->h:Liju;

    invoke-virtual {v1, v0}, Liju;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_d
    const-string v2, "com.google.android.location.reporting.ACTION_UPDATE_ACTIVE_STATE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v0, p0, Liiu;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lijt;

    move-result-object v0

    invoke-virtual {v0}, Lijt;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Liiu;->c:Lcom/google/android/location/reporting/LocationReportingController;

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/location/reporting/LocationReportingController;->a(Lcom/google/android/location/reporting/service/ReportingConfig;J)V

    goto/16 :goto_0

    :cond_e
    const-string v2, "com.google.android.location.reporting.ACTION_APPLY_UPLOAD_REQUESTS"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v0, p0, Liiu;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lijt;

    move-result-object v0

    invoke-virtual {v0}, Lijt;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v0

    iget-object v1, p0, Liiu;->d:Liht;

    iget-object v2, p0, Liiu;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v2}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Lijb;

    move-result-object v2

    iget-object v3, p0, Liiu;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v3}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Lijb;

    move-result-object v3

    invoke-virtual {v3}, Lijb;->c()Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Liht;->a(Lijb;Lcom/google/android/location/reporting/service/ReportingConfig;Landroid/location/Location;)Z

    goto/16 :goto_0

    :cond_f
    const-string v2, "com.google.android.location.reporting.ACTION_INSISTENT_SYNC"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v1, p0, Liiu;->g:Lijj;

    invoke-virtual {v1, v0}, Lijj;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_10
    const-string v2, "com.google.android.location.reporting.ACTION_RESET_WIFI_ATTACHMENT"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    iget-object v0, p0, Liiu;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Lijb;

    move-result-object v0

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lijb;->b(J)V

    goto/16 :goto_0

    :cond_11
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported action in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
