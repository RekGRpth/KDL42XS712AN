.class public final Ljbd;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile c:[Ljbd;


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizs;-><init>()V

    const/16 v0, 0xfa

    iput v0, p0, Ljbd;->a:I

    const/16 v0, 0x3ca

    iput v0, p0, Ljbd;->b:I

    const/4 v0, -0x1

    iput v0, p0, Ljbd;->C:I

    return-void
.end method

.method public static c()[Ljbd;
    .locals 2

    sget-object v0, Ljbd;->c:[Ljbd;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljbd;->c:[Ljbd;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljbd;

    sput-object v0, Ljbd;->c:[Ljbd;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljbd;->c:[Ljbd;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Ljbd;->a:I

    const/16 v2, 0xfa

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ljbd;->a:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljbd;->b:I

    const/16 v2, 0x3ca

    if-eq v1, v2, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljbd;->b:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Ljbd;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljbd;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljbd;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0xfa
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3ca
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget v0, p0, Ljbd;->a:I

    const/16 v1, 0xfa

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljbd;->a:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_0
    iget v0, p0, Ljbd;->b:I

    const/16 v1, 0x3ca

    if-eq v0, v1, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljbd;->b:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_1
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljbd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljbd;

    iget v2, p0, Ljbd;->a:I

    iget v3, p1, Ljbd;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Ljbd;->b:I

    iget v3, p1, Ljbd;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Ljbd;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Ljbd;->b:I

    add-int/2addr v0, v1

    return v0
.end method
