.class public final Lgqw;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/security/snet/SnetService;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/security/snet/SnetService;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-boolean v0, p0, Lgqw;->c:Z

    iput-boolean v0, p0, Lgqw;->d:Z

    iput-object p2, p0, Lgqw;->b:Ljava/lang/String;

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->a(Lcom/google/android/gms/security/snet/SnetService;)Z

    move-result v0

    iput-boolean v0, p0, Lgqw;->c:Z

    :try_start_0
    iget-object v0, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lgqn;

    move-result-object v0

    invoke-virtual {v0}, Lgqn;->d()Z

    move-result v0

    iput-boolean v0, p0, Lgqw;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v0, p0, Lgqw;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgqw;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lgqn;

    move-result-object v0

    iget-object v1, p0, Lgqw;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgqn;->b(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v1}, Lcom/google/android/gms/security/snet/SnetService;->c(Lcom/google/android/gms/security/snet/SnetService;)Lgqp;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v3, v2}, Lgqp;->a(I[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lgqw;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 8

    iget-object v0, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lgqn;

    move-result-object v0

    invoke-virtual {v0}, Lgqn;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    iget-object v1, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v1}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lgqn;

    move-result-object v1

    invoke-virtual {v1}, Lgqn;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v2}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lgqn;

    move-result-object v2

    invoke-virtual {v2}, Lgqn;->b()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lgqw;->c:Z

    iget-object v4, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v4}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lgqn;

    move-result-object v4

    invoke-virtual {v4}, Lgqn;->f()J

    move-result-wide v4

    iget-object v6, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v6}, Lcom/google/android/gms/security/snet/SnetService;->d(Lcom/google/android/gms/security/snet/SnetService;)I

    move-result v6

    iget-boolean v7, p0, Lgqw;->d:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/security/snet/SnetLaunchService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZJIZ)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/WatchdogService;->a(Landroid/content/Context;)V

    :cond_0
    iget-object v0, p0, Lgqw;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->e(Lcom/google/android/gms/security/snet/SnetService;)V

    return-void
.end method
