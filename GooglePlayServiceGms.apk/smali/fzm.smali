.class public final Lfzm;
.super Lfzi;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final h:Landroid/net/Uri;

.field public static final i:Landroid/net/Uri;

.field public static final j:Landroid/net/Uri;


# instance fields
.field private A:Lbbo;

.field private final B:Ljava/lang/Runnable;

.field private final C:Lfuk;

.field private final D:Lfuk;

.field private final E:Lfuo;

.field private F:Lbbr;

.field private G:Lbbr;

.field private H:Lbbs;

.field private I:Lbbs;

.field private J:Ljava/lang/String;

.field protected k:Lfsu;

.field protected l:Z

.field protected m:Z

.field protected n:Z

.field protected o:Z

.field private final p:Landroid/content/Context;

.field private final q:Landroid/view/Display;

.field private r:Landroid/widget/PopupWindow;

.field private s:Z

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/widget/ImageView;

.field private v:Landroid/view/View$OnClickListener;

.field private final w:Lftz;

.field private x:Lftx;

.field private y:Lftx;

.field private z:Lfsv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "plus_one_button_popup_beak_up"

    invoke-static {v0}, Lbkn;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfzm;->h:Landroid/net/Uri;

    const-string v0, "plus_one_button_popup_beak_down"

    invoke-static {v0}, Lbkn;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfzm;->i:Landroid/net/Uri;

    const-string v0, "plus_one_button_popup_bg"

    invoke-static {v0}, Lbkn;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfzm;->j:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    sget-object v6, Lftx;->a:Lftz;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lfzm;-><init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Lftz;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Lftz;)V
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lfzi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V

    new-instance v0, Lfzn;

    invoke-direct {v0, p0}, Lfzn;-><init>(Lfzm;)V

    iput-object v0, p0, Lfzm;->B:Ljava/lang/Runnable;

    new-instance v0, Lfzo;

    invoke-direct {v0, p0}, Lfzo;-><init>(Lfzm;)V

    iput-object v0, p0, Lfzm;->C:Lfuk;

    new-instance v0, Lfzp;

    invoke-direct {v0, p0}, Lfzp;-><init>(Lfzm;)V

    iput-object v0, p0, Lfzm;->D:Lfuk;

    new-instance v0, Lfzq;

    invoke-direct {v0, p0}, Lfzq;-><init>(Lfzm;)V

    iput-object v0, p0, Lfzm;->E:Lfuo;

    new-instance v0, Lfzr;

    invoke-direct {v0, p0}, Lfzr;-><init>(Lfzm;)V

    iput-object v0, p0, Lfzm;->F:Lbbr;

    new-instance v0, Lfzs;

    invoke-direct {v0, p0}, Lfzs;-><init>(Lfzm;)V

    iput-object v0, p0, Lfzm;->G:Lbbr;

    new-instance v0, Lfzt;

    invoke-direct {v0, p0}, Lfzt;-><init>(Lfzm;)V

    iput-object v0, p0, Lfzm;->H:Lbbs;

    new-instance v0, Lfzu;

    invoke-direct {v0, p0}, Lfzu;-><init>(Lfzm;)V

    iput-object v0, p0, Lfzm;->I:Lbbs;

    iput-object p6, p0, Lfzm;->w:Lftz;

    iput-object p1, p0, Lfzm;->p:Landroid/content/Context;

    iget-object v0, p0, Lfzm;->p:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lfzm;->q:Landroid/view/Display;

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lfzm;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfzm;->t:Landroid/widget/ImageView;

    iget-object v0, p0, Lfzm;->t:Landroid/widget/ImageView;

    sget-object v1, Lfzm;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lfzm;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfzm;->u:Landroid/widget/ImageView;

    iget-object v0, p0, Lfzm;->u:Landroid/widget/ImageView;

    sget-object v1, Lfzm;->j:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    invoke-virtual {p0, p0}, Lfzm;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object p5, p0, Lfzm;->J:Ljava/lang/String;

    new-instance v0, Lfwa;

    iget-object v1, p0, Lfzm;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Lfwa;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "https://www.googleapis.com/auth/pos"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lfwa;->a([Ljava/lang/String;)Lfwa;

    move-result-object v0

    iget-object v1, p0, Lfzm;->J:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfzm;->J:Ljava/lang/String;

    iput-object v1, v0, Lfwa;->a:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lfzm;->w:Lftz;

    iget-object v2, p0, Lfzm;->p:Landroid/content/Context;

    invoke-virtual {v0}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iget-object v3, p0, Lfzm;->F:Lbbr;

    iget-object v4, p0, Lfzm;->H:Lbbs;

    invoke-interface {v1, v2, v0, v3, v4}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v0

    iget-object v1, p0, Lfzm;->x:Lftx;

    invoke-virtual {p0, v1}, Lfzm;->a(Lftx;)V

    iput-object v0, p0, Lfzm;->x:Lftx;

    new-instance v0, Lfwa;

    iget-object v1, p0, Lfzm;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Lfwa;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lfwa;->a()Lfwa;

    move-result-object v0

    invoke-virtual {v0}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iget-object v1, p0, Lfzm;->w:Lftz;

    iget-object v2, p0, Lfzm;->p:Landroid/content/Context;

    iget-object v3, p0, Lfzm;->G:Lbbr;

    iget-object v4, p0, Lfzm;->I:Lbbs;

    invoke-interface {v1, v2, v0, v3, v4}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lfzm;->y:Lftx;

    iget-object v0, p0, Lfzm;->x:Lftx;

    invoke-virtual {p0, v0}, Lfzm;->a(Lftx;)V

    new-instance v0, Lfzv;

    invoke-direct {v0, p0}, Lfzv;-><init>(Lfzm;)V

    invoke-virtual {p0, v0}, Lfzm;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lfzm;)Lbbo;
    .locals 1

    iget-object v0, p0, Lfzm;->A:Lbbo;

    return-object v0
.end method

.method static synthetic a(Lfzm;Lbbo;)Lbbo;
    .locals 0

    iput-object p1, p0, Lfzm;->A:Lbbo;

    return-object p1
.end method

.method static synthetic a(Lfzm;Lfsv;)Lfsv;
    .locals 0

    iput-object p1, p0, Lfzm;->z:Lfsv;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lfzm;->v:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzm;->v:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/widget/FrameLayout;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x2

    new-instance v1, Lfzw;

    invoke-direct {v1, p0, p1}, Lfzw;-><init>(Lfzm;Landroid/widget/FrameLayout;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v0, p0, Lfzm;->p:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-boolean v0, v1, Lfzw;->a:Z

    if-eqz v0, :cond_1

    sget-object v0, Lfzm;->i:Landroid/net/Uri;

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v1, Lfzw;->f:I

    invoke-direct {v0, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget v3, v1, Lfzw;->b:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v4, v1, Lfzw;->d:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0, v3, v5, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v0, p1, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    iget-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-boolean v0, p0, Lfzm;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    const/16 v2, 0x33

    iget v3, v1, Lfzw;->g:I

    iget v1, v1, Lfzw;->h:I

    invoke-virtual {v0, p0, v2, v3, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object v0, p0, Lfzm;->B:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lfzm;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lfzm;->B:Ljava/lang/Runnable;

    const-wide/16 v1, 0xbb8

    invoke-virtual {p0, v0, v1, v2}, Lfzm;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lfzm;->h:Landroid/net/Uri;

    goto :goto_0
.end method

.method private b(Landroid/view/View;)Landroid/widget/FrameLayout;
    .locals 4

    const/4 v3, -0x1

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lfzm;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method static synthetic b(Lfzm;)Lftx;
    .locals 1

    iget-object v0, p0, Lfzm;->x:Lftx;

    return-object v0
.end method

.method static synthetic c(Lfzm;)Z
    .locals 1

    iget-boolean v0, p0, Lfzm;->s:Z

    return v0
.end method

.method static synthetic d(Lfzm;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic e(Lfzm;)Landroid/widget/PopupWindow;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic f(Lfzm;)Lfuk;
    .locals 1

    iget-object v0, p0, Lfzm;->D:Lfuk;

    return-object v0
.end method

.method static synthetic g(Lfzm;)Lfuk;
    .locals 1

    iget-object v0, p0, Lfzm;->C:Lfuk;

    return-object v0
.end method

.method static synthetic h(Lfzm;)Lfsv;
    .locals 1

    iget-object v0, p0, Lfzm;->z:Lfsv;

    return-object v0
.end method

.method private h()Z
    .locals 1

    iget-object v0, p0, Lfzm;->k:Lfsu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lfzm;)Lfuo;
    .locals 1

    iget-object v0, p0, Lfzm;->E:Lfuo;

    return-object v0
.end method

.method private i()V
    .locals 1

    iget-boolean v0, p0, Lfzm;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method

.method static synthetic j(Lfzm;)Lftx;
    .locals 1

    iget-object v0, p0, Lfzm;->y:Lftx;

    return-object v0
.end method

.method private j()V
    .locals 3

    const/4 v2, 0x3

    iget-boolean v0, p0, Lfzm;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfzm;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzm;->l:Z

    iget-object v0, p0, Lfzm;->k:Lfsu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzm;->k:Lfsu;

    invoke-virtual {v0}, Lfsu;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v0, "plus_popup_text"

    invoke-virtual {p0, v0}, Lfzm;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    const-string v0, "text"

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2}, Lfzm;->b(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v0}, Lfzm;->a(Landroid/widget/FrameLayout;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonView"

    const-string v1, "Text confirmation popup requested but text is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v2}, Lfzm;->a(I)V

    goto :goto_0
.end method

.method static synthetic k(Lfzm;)Landroid/view/Display;
    .locals 1

    iget-object v0, p0, Lfzm;->q:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic l(Lfzm;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lfzm;->t:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final c()V
    .locals 0

    invoke-super {p0}, Lfzi;->c()V

    invoke-direct {p0}, Lfzm;->i()V

    invoke-direct {p0}, Lfzm;->j()V

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lfzi;->d()V

    invoke-direct {p0}, Lfzm;->i()V

    invoke-direct {p0}, Lfzm;->j()V

    return-void
.end method

.method protected final e()Z
    .locals 1

    iget-boolean v0, p0, Lfzm;->n:Z

    return v0
.end method

.method public final f()V
    .locals 8

    const/4 v4, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Lfzi;->f()V

    invoke-direct {p0}, Lfzm;->i()V

    iget-boolean v0, p0, Lfzm;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfzm;->l:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lfzm;->l:Z

    iget-object v0, p0, Lfzm;->k:Lfsu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzm;->z:Lfsv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzm;->z:Lfsv;

    invoke-virtual {v0}, Lfsv;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lfzm;->k:Lfsu;

    invoke-virtual {v0}, Lfsu;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfzm;->z:Lfsv;

    invoke-virtual {v1}, Lfsv;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lfzm;->k:Lfsu;

    iget-object v0, v0, Lfsu;->a:Landroid/os/Bundle;

    const-string v3, "visibility"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-string v0, "plus_popup_confirmation"

    invoke-virtual {p0, v0}, Lfzm;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v0, p0, Lfzm;->p:Landroid/content/Context;

    invoke-direct {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lfzm;->x:Lftx;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lftx;)V

    const/high16 v0, 0x42000000    # 32.0f

    invoke-virtual {p0}, Lfzm;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v0, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    new-instance v5, Lbki;

    invoke-direct {v5, v2}, Lbki;-><init>(Ljava/lang/String;)V

    float-to-int v0, v0

    invoke-virtual {v5, v0}, Lbki;->a(I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lftt;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;)V

    const-string v0, "profile_image"

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    const-string v0, "text"

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v3}, Lfzm;->b(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v0}, Lfzm;->a(Landroid/widget/FrameLayout;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonView"

    const-string v1, "Confirmation popup requested but content view cannot be created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v4}, Lfzm;->a(I)V

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method protected final g()Z
    .locals 1

    iget-object v0, p0, Lfzm;->x:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Lfzi;->onAttachedToWindow()V

    invoke-virtual {p0}, Lfzm;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzm;->x:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzm;->x:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfzm;->s:Z

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/4 v0, 0x0

    const/4 v3, 0x2

    const/4 v1, 0x1

    iget-object v2, p0, Lfzm;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->toggle()V

    iget-boolean v2, p0, Lfzm;->m:Z

    if-eqz v2, :cond_1

    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonView"

    const-string v1, "onClick: result pending, ignoring +1 button click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v2, p0, Lfzm;->o:Z

    if-eqz v2, :cond_3

    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonView"

    const-string v2, "onClick: anonymous button, forwarding to external click listener"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-direct {p0, p1}, Lfzm;->a(Landroid/view/View;)V

    iput-boolean v1, p0, Lfzm;->n:Z

    goto :goto_0

    :cond_3
    iput-boolean v1, p0, Lfzm;->l:Z

    invoke-direct {p0}, Lfzm;->h()Z

    move-result v2

    if-nez v2, :cond_6

    iput-boolean v0, p0, Lfzm;->l:Z

    iget-object v0, p0, Lfzm;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "PlusOneButtonView"

    const-string v2, "onClick: reload +1"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {p0}, Lfzm;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfzm;->x:Lftx;

    iget-object v2, p0, Lfzm;->C:Lfuk;

    iget-object v3, p0, Lfzm;->f:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lftx;->a(Lfuk;Ljava/lang/String;)V

    iput-boolean v1, p0, Lfzm;->m:Z

    :cond_5
    :goto_1
    invoke-direct {p0, p1}, Lfzm;->a(Landroid/view/View;)V

    goto :goto_0

    :cond_6
    invoke-direct {p0}, Lfzm;->h()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lfzm;->k:Lfsu;

    invoke-virtual {v2}, Lfsu;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    move v0, v1

    :cond_7
    if-eqz v0, :cond_9

    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "PlusOneButtonView"

    const-string v2, "onClick: undo +1"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-virtual {p0}, Lfzm;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfzm;->x:Lftx;

    iget-object v2, p0, Lfzm;->C:Lfuk;

    iget-object v3, p0, Lfzm;->f:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lftx;->b(Lfuk;Ljava/lang/String;)V

    iput-boolean v1, p0, Lfzm;->m:Z

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lfzm;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "PlusOneButtonView"

    const-string v2, "onClick: +1"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    invoke-virtual {p0}, Lfzm;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfzm;->x:Lftx;

    iget-object v2, p0, Lfzm;->C:Lfuk;

    iget-object v3, p0, Lfzm;->f:Ljava/lang/String;

    iget-object v4, p0, Lfzm;->k:Lfsu;

    invoke-virtual {v4}, Lfsu;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lftx;->a(Lfuk;Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v1, p0, Lfzm;->m:Z

    goto :goto_1
.end method

.method public final onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lfzi;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lfzm;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzm;->x:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfzm;->x:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzm;->s:Z

    iget-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfzm;->r:Landroid/widget/PopupWindow;

    :cond_2
    return-void
.end method

.method public final setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    if-ne p1, p0, :cond_0

    invoke-super {p0, p0}, Lfzi;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lfzm;->v:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method
