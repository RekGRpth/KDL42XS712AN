.class public final Lddn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldgs;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbdu;I[I)Lbeh;
    .locals 1

    new-instance v0, Lddt;

    invoke-direct {v0, p0, p2, p3}, Lddt;-><init>(Lddn;I[I)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;)Lbeh;
    .locals 1

    new-instance v0, Lddr;

    invoke-direct {v0, p0, p2}, Lddr;-><init>(Lddn;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lbdu;->b(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;Ljava/lang/String;)Lbeh;
    .locals 1

    new-instance v0, Lddo;

    invoke-direct {v0, p0, p2, p3}, Lddo;-><init>(Lddn;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lbdu;->b(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;[I)Lbeh;
    .locals 2

    new-instance v0, Lddq;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p2, v1, p3}, Lddq;-><init>(Lddn;Ljava/lang/String;I[I)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;[I)Lbeh;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lddn;->a(Lbdu;I[I)Lbeh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->n()V

    return-void
.end method

.method public final a(Lbdu;Ldgp;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcwm;->a(Ldgp;)V

    return-void
.end method

.method public final a(Lbdu;Ldgp;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcwm;->a(Ldgp;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lbdu;Ljava/lang/String;)Lbeh;
    .locals 1

    new-instance v0, Ldds;

    invoke-direct {v0, p0, p2}, Ldds;-><init>(Lddn;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lbdu;->b(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbdu;Ljava/lang/String;Ljava/lang/String;)Lbeh;
    .locals 1

    new-instance v0, Lddp;

    invoke-direct {v0, p0, p2, p3}, Lddp;-><init>(Lddn;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lbdu;->b(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lbdu;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcwm;->b(Ljava/lang/String;I)V

    return-void
.end method

.method public final c(Lbdu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Lcwm;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final d(Lbdu;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcwm;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public final d(Lbdu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Lcwm;->b(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final e(Lbdu;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcwm;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final e(Lbdu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcwm;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final f(Lbdu;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcwm;->d(Ljava/lang/String;)V

    return-void
.end method
