.class public final Lgva;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 0

    iput-object p1, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;)V
    .locals 10

    const/4 v2, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v0, v0

    if-lez v0, :cond_3

    iget-object v6, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v7, v6

    move v4, v5

    move v1, v5

    move v3, v5

    :goto_0
    if-ge v4, v7, :cond_2

    aget v0, v6, v4

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v1, "AddInstrumentActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Unexpected error code: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    move v1, v3

    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    instance-of v0, v0, Lgvb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    check-cast v0, Lgvb;

    invoke-virtual {v0}, Lgvb;->b()V

    :cond_0
    if-nez v3, :cond_5

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->i()Z

    move v0, v1

    move v1, v2

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const v8, 0x7f0b0113    # com.google.android.gms.R.string.wallet_error_creditcard_invalid

    invoke-static {v0, v8}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;I)V

    if-nez v3, :cond_5

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    move v0, v1

    move v1, v2

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const v8, 0x7f0b014d    # com.google.android.gms.R.string.wallet_error_creditcard_expiry_date_invalid

    invoke-static {v0, v8}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;I)V

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    instance-of v0, v0, Lgvb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    check-cast v0, Lgvb;

    invoke-virtual {v0}, Lgvb;->L()V

    :cond_1
    if-nez v3, :cond_5

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->i()Z

    move v0, v1

    move v1, v2

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_4

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    :goto_2
    return-void

    :cond_3
    const-string v0, "AddInstrumentActivity"

    const-string v1, "Unexpected create instrument response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;Z)V

    goto :goto_2

    :cond_5
    move v0, v1

    move v1, v3

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lioj;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 3

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.disallowedCardCategories"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    iget-object v1, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, Lioj;->l:I

    invoke-static {v1, v2, v0}, Lgth;->a(Landroid/content/res/Resources;I[I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    instance-of v0, v0, Lgvb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    check-cast v0, Lgvb;

    invoke-virtual {v0, v1}, Lgvb;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->i()Z

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;Z)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.wallet.instrument"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lizs;)V

    const-string v1, "com.google.android.gms.wallet.instrumentId"

    iget-object v2, p1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "serverResponse"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    iget-object v1, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lgva;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    return-void
.end method
