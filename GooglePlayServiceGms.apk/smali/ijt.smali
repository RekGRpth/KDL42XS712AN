.class public final Lijt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/reporting/StateReporter;


# static fields
.field private static final a:J

.field private static final j:Ljava/lang/Object;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Liio;

.field private final e:Liky;

.field private final f:Lbpe;

.field private final g:Liin;

.field private final h:Lika;

.field private final i:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lijt;->a:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lijt;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    const-string v2, "ULR_USER_PREFS"

    new-instance v3, Liip;

    invoke-direct {v3, p1}, Liip;-><init>(Landroid/content/Context;)V

    new-instance v4, Likz;

    invoke-direct {v4, p1}, Likz;-><init>(Landroid/content/Context;)V

    new-instance v5, Lbpg;

    invoke-direct {v5}, Lbpg;-><init>()V

    new-instance v6, Liim;

    invoke-direct {v6, p1}, Liim;-><init>(Landroid/content/Context;)V

    new-instance v7, Likb;

    invoke-direct {v7}, Likb;-><init>()V

    sget-wide v8, Lijt;->a:J

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, Lijt;-><init>(Landroid/content/Context;Ljava/lang/String;Liio;Liky;Lbpe;Liin;Lika;J)V

    invoke-static {p1}, Lijv;->a(Landroid/content/Context;)V

    invoke-static {p1}, Likh;->a(Landroid/content/Context;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Liio;Liky;Lbpe;Liin;Lika;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lijt;->b:Landroid/content/Context;

    iput-object p4, p0, Lijt;->e:Liky;

    iput-object p3, p0, Lijt;->d:Liio;

    iput-object p7, p0, Lijt;->h:Lika;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    iput-object p5, p0, Lijt;->f:Lbpe;

    iput-object p6, p0, Lijt;->g:Liin;

    iput-wide p8, p0, Lijt;->i:J

    return-void
.end method

.method private a(Landroid/content/SharedPreferences$Editor;Liir;)V
    .locals 4

    iget-object v0, p2, Liir;->a:Landroid/accounts/Account;

    iget-boolean v1, p2, Liir;->g:Z

    if-nez v1, :cond_0

    invoke-static {v0}, Lijt;->h(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lijt;->f:Lbpe;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_0
    iget-object v1, p2, Liir;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, "defined"

    iget-object v2, p2, Liir;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_1
    invoke-virtual {p2}, Liir;->a()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lijt;->b(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Liir;->a()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_2
    iget-object v1, p2, Liir;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-static {v0}, Lijt;->e(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Liir;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_3
    iget-object v1, p2, Liir;->k:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-static {v0}, Lijt;->g(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Liir;->k:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_4
    iget-object v1, p2, Liir;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-static {v0}, Lijt;->j(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Liir;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_5
    iget-object v1, p2, Liir;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-static {v0}, Lijt;->k(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Liir;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_6
    iget-object v1, p2, Liir;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-static {v0}, Lijt;->l(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Liir;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_7
    iget-object v1, p2, Liir;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-static {v0}, Lijt;->n(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Liir;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_8
    return-void
.end method

.method private a(Landroid/content/SharedPreferences$Editor;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 12

    sget-object v4, Lijt;->j:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    invoke-virtual {p0}, Lijt;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v5

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {p0}, Lijt;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v6

    iget-object v7, p0, Lijt;->b:Landroid/content/Context;

    iget-object v8, p0, Lijt;->h:Lika;

    const/4 v2, 0x0

    invoke-virtual {v6}, Lcom/google/android/location/reporting/service/ReportingConfig;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-static {v1}, Liik;->a(Lcom/google/android/location/reporting/service/AccountConfig;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_5

    invoke-interface {v8, v7}, Lika;->e(Landroid/content/Context;)V

    :cond_1
    :goto_1
    invoke-direct {p0}, Lijt;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    :goto_2
    invoke-static {v5, v6, v1}, Lijp;->a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/location/reporting/service/ReportingConfig;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lijt;->f:Lbpe;

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "GCoreUlr"

    invoke-static {v2, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lijt;->c:Landroid/content/SharedPreferences;

    const-string v3, "changeHistory"

    const-string v7, ""

    invoke-interface {v2, v3, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_8

    :goto_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x2000

    if-le v2, v3, :cond_3

    add-int/lit16 v2, v2, -0x2000

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_3
    iget-object v2, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "changeHistory"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_4
    move-object/from16 v0, p5

    invoke-static {v5, v6, v0}, Likf;->a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/location/reporting/service/ReportingConfig;Ljava/lang/String;)V

    invoke-direct {p0, p2, p3}, Lijt;->b(ZZ)V

    iget-object v1, p0, Lijt;->h:Lika;

    iget-object v2, p0, Lijt;->b:Landroid/content/Context;

    invoke-interface {v1, v2}, Lika;->b(Landroid/content/Context;)V

    invoke-direct {p0}, Lijt;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lijt;->g:Liin;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Liin;->a(I)V

    :goto_4
    monitor-exit v4

    return-void

    :cond_5
    invoke-virtual {v5}, Lcom/google/android/location/reporting/service/ReportingConfig;->h()Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v6}, Lcom/google/android/location/reporting/service/ReportingConfig;->h()Ljava/util/Map;

    move-result-object v10

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 v3, 0x0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-static {v2}, Liik;->a(Lcom/google/android/location/reporting/service/AccountConfig;)Z

    move-result v2

    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-static {v1}, Liik;->a(Lcom/google/android/location/reporting/service/AccountConfig;)Z

    move-result v1

    if-eqz v1, :cond_6

    if-nez v2, :cond_6

    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_1

    invoke-interface {v8, v7}, Lika;->d(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v4

    throw v1

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_8
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    :cond_9
    iget-object v1, p0, Lijt;->g:Liin;

    invoke-interface {v1}, Liin;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :cond_a
    move v1, v3

    goto :goto_5

    :cond_b
    move v1, v2

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/location/reporting/service/AccountConfig;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for account "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Liir;)V
    .locals 6

    iget-boolean v0, p2, Liir;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p2, Liir;->a:Landroid/accounts/Account;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p2, Liir;->n:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v3}, Lijt;->c(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Liir;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lijt;->h:Lika;

    iget-object v1, p0, Lijt;->b:Landroid/content/Context;

    iget-object v4, p2, Liir;->i:Ljava/lang/Boolean;

    iget-object v5, p2, Liir;->j:Ljava/lang/Boolean;

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lika;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lijt;->h:Lika;

    invoke-interface {v0, v3, p1}, Lika;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/reporting/service/Conditions;)Z
    .locals 1

    invoke-direct {p0}, Lijt;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/Conditions;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(ZZ)Z
    .locals 1

    if-eqz p0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    const-string v0, "null account"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ambiguous_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(ZZ)V
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    invoke-direct {p0}, Lijt;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GCoreUlr"

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Changing settings when ULR ineligible"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    iget-object v0, p0, Lijt;->e:Liky;

    invoke-interface {v0}, Liky;->a()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    invoke-virtual {p0, v4}, Lijt;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;

    move-result-object v5

    invoke-direct {p0, v4}, Lijt;->d(Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-direct {p0, v4}, Lijt;->m(Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-direct {p0, v4}, Lijt;->o(Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz p2, :cond_4

    invoke-static {v4}, Liir;->a(Landroid/accounts/Account;)Liis;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/location/reporting/service/AccountConfig;->c()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Liis;->a(J)Liis;

    move-result-object v6

    invoke-virtual {v6, v1}, Liis;->a(Z)Liis;

    move-result-object v6

    invoke-virtual {v6}, Liis;->a()Liir;

    move-result-object v6

    const-string v7, "may-update-ambiguous"

    const-string v8, "disambiguation_update"

    invoke-virtual {p0, v7, v6, v8}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    :cond_1
    :goto_1
    invoke-direct {p0}, Lijt;->c()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-direct {p0, v4}, Lijt;->m(Landroid/accounts/Account;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-direct {p0, v4}, Lijt;->o(Landroid/accounts/Account;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-direct {p0, v4}, Lijt;->d(Landroid/accounts/Account;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-direct {p0, v4}, Lijt;->f(Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const-string v4, "Settings undefined but have non-default settings"

    invoke-static {v5, v4}, Lijt;->a(Lcom/google/android/location/reporting/service/AccountConfig;Ljava/lang/String;)V

    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    invoke-static {v4}, Liir;->a(Landroid/accounts/Account;)Liis;

    move-result-object v6

    invoke-virtual {v6, v9}, Liis;->b(Z)Liis;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/location/reporting/service/AccountConfig;->c()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Liis;->a(J)Liis;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v6, Liis;->i:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v6, Liis;->j:Ljava/lang/Boolean;

    invoke-virtual {v6, v9}, Liis;->b(Z)Liis;

    move-result-object v6

    invoke-virtual {v6}, Liis;->a()Liir;

    move-result-object v6

    const-string v7, "preserve-ambiguous"

    const-string v8, "preserve_ambiguous_update"

    invoke-virtual {p0, v7, v6, v8}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    const-string v6, "When ambiguous, at least one of reporting or history must be false"

    invoke-static {v5, v6}, Lijt;->a(Lcom/google/android/location/reporting/service/AccountConfig;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    invoke-direct {p0, v4}, Lijt;->i(Landroid/accounts/Account;)J

    goto :goto_2

    :cond_6
    return-void
.end method

.method private c()Z
    .locals 3

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    const-string v1, "defined"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private c(Landroid/accounts/Account;)Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lijt;->e:Liky;

    invoke-interface {v1}, Liky;->a()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private d()Z
    .locals 8

    const/4 v6, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lijv;->a()V

    sget-object v1, Lijt;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lijt;->d:Liio;

    invoke-interface {v2}, Liio;->a()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v2

    invoke-direct {p0, v2}, Lijt;->a(Lcom/google/android/location/reporting/service/Conditions;)Z

    move-result v3

    if-nez v3, :cond_0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    const-string v3, "GCoreUlr"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "undefineIfIneligible() sleeping before reevaluating "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-wide v1, p0, Lijt;->i:J

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    :goto_1
    sget-object v7, Lijt;->j:Ljava/lang/Object;

    monitor-enter v7

    :try_start_2
    iget-object v1, p0, Lijt;->d:Liio;

    invoke-interface {v1}, Liio;->a()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v1

    invoke-direct {p0, v1}, Lijt;->a(Lcom/google/android/location/reporting/service/Conditions;)Z

    move-result v2

    if-nez v2, :cond_3

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v1

    const-string v2, "GCoreUlr"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "GCoreUlr"

    const-string v3, ""

    invoke-static {v2, v3, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    :try_start_3
    const-string v0, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "undefineIfIneligible() calling clear(): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-static {v1}, Likf;->a(Lcom/google/android/location/reporting/service/Conditions;)Ljava/lang/String;

    move-result-object v5

    const-string v4, "undefineIfIneligible()"

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Clearing UserPreferences: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-direct {p0}, Lijt;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "changeHistory"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lijt;->a(Landroid/content/SharedPreferences$Editor;ZZLjava/lang/String;Ljava/lang/String;)V

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v6

    goto/16 :goto_0
.end method

.method private d(Landroid/accounts/Account;)Z
    .locals 3

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lijt;->b(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static e(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    const-string v0, "null account"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dirty_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lijt;->d:Liio;

    invoke-interface {v0}, Liio;->a()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->i()Z

    move-result v0

    return v0
.end method

.method private f()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    const-string v1, "changeHistory"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f(Landroid/accounts/Account;)Z
    .locals 3

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lijt;->e(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static g(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    const-string v0, "null account"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "serverMillis_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static h(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    const-string v0, "null account"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clientMillis_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i(Landroid/accounts/Account;)J
    .locals 4

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lijt;->h(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const-wide/high16 v2, -0x8000000000000000L

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static j(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    const-string v0, "null account"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "restriction_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static k(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    const-string v0, "null account"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "authorized_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static l(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    const-string v0, "null account"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "reportingEnabled_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m(Landroid/accounts/Account;)Z
    .locals 3

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lijt;->l(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static n(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    const-string v0, "null account"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "historyEnabled_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o(Landroid/accounts/Account;)Z
    .locals 3

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lijt;->n(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;
    .locals 6

    sget-object v1, Lijt;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, Lcom/google/android/location/reporting/service/AccountConfig;->a(Landroid/accounts/Account;)Liii;

    move-result-object v2

    invoke-direct {p0}, Lijt;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Liii;->c:Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lijt;->i(Landroid/accounts/Account;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Liii;->b:Ljava/lang/Long;

    invoke-direct {p0, p1}, Lijt;->c(Landroid/accounts/Account;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Liii;->d:Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lijt;->d(Landroid/accounts/Account;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Liii;->e:Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lijt;->m(Landroid/accounts/Account;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Liii;->j:Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lijt;->o(Landroid/accounts/Account;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Liii;->k:Ljava/lang/Boolean;

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lijt;->g(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    const-wide/high16 v4, -0x8000000000000000L

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Liii;->h:Ljava/lang/Long;

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lijt;->j(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Liii;->i:Ljava/lang/Integer;

    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lijt;->k(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Liii;->g:Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lijt;->f(Landroid/accounts/Account;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Liii;->f:Ljava/lang/Boolean;

    iget-object v0, p0, Lijt;->d:Liio;

    invoke-interface {v0}, Liio;->a()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v0

    iput-object v0, v2, Liii;->l:Lcom/google/android/location/reporting/service/Conditions;

    new-instance v0, Lcom/google/android/location/reporting/service/AccountConfig;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/android/location/reporting/service/AccountConfig;-><init>(Liii;B)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Lcom/google/android/location/reporting/service/ReportingConfig;
    .locals 6

    sget-object v1, Lijt;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lijt;->e:Liky;

    invoke-interface {v0}, Liky;->a()[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    invoke-virtual {p0, v5}, Lijt;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lijt;->d:Liio;

    invoke-interface {v0}, Liio;->a()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v0

    new-instance v3, Lcom/google/android/location/reporting/service/ReportingConfig;

    invoke-direct {p0}, Lijt;->c()Z

    move-result v4

    invoke-direct {p0}, Lijt;->f()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v2, v0, v5}, Lcom/google/android/location/reporting/service/ReportingConfig;-><init>(ZLjava/util/List;Lcom/google/android/location/reporting/service/Conditions;Ljava/lang/String;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v3

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Liir;Ljava/lang/String;)Z
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    const/4 v5, 0x3

    const/4 v6, 0x1

    const/4 v0, 0x0

    iget-object v2, p2, Liir;->a:Landroid/accounts/Account;

    sget-object v7, Lijt;->j:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-boolean v1, p2, Liir;->d:Z

    if-nez v1, :cond_1

    iget-object v1, p2, Liir;->c:Ljava/lang/Long;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "update("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") must provide reference version"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p2, Liir;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {p0, v2}, Lijt;->i(Landroid/accounts/Account;)J

    move-result-wide v8

    cmp-long v1, v3, v8

    if-gez v1, :cond_1

    const-string v1, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UserPreferences.updateEditor("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): aborting to preserve local change at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v2}, Lijt;->i(Landroid/accounts/Account;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    monitor-exit v7

    :goto_0
    return v0

    :cond_1
    const-string v1, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UserPreferences.update("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lijt;->a(Landroid/content/SharedPreferences$Editor;Liir;)V

    iget v3, p2, Liir;->h:I

    if-ne v3, v5, :cond_3

    move v3, v6

    :goto_1
    sget-object v8, Lijt;->j:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "termsAccepted_"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lijt;->a(Landroid/content/SharedPreferences$Editor;ZZLjava/lang/String;Ljava/lang/String;)V

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-direct {p0, p1, p2}, Lijt;->a(Ljava/lang/String;Liir;)V

    monitor-exit v7

    move v0, v6

    goto :goto_0

    :cond_3
    move v3, v0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Iterable;)Z
    .locals 8

    const/4 v6, 0x1

    const/4 v0, 0x0

    sget-object v7, Lijt;->j:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    invoke-direct {p0}, Lijt;->e()Z

    move-result v1

    invoke-direct {p0}, Lijt;->c()Z

    move-result v2

    invoke-static {v1, v2}, Lijt;->a(ZZ)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "define() called when shouldDefine()=false: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lijt;->e()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lijt;->c()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lijy;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    monitor-exit v7

    :goto_0
    return v0

    :cond_1
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "defining preferences to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lijt;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liir;

    iget-object v3, v0, Liir;->b:Ljava/lang/Boolean;

    if-nez v3, :cond_3

    iget-boolean v3, v0, Liir;->d:Z

    if-nez v3, :cond_3

    iget-object v3, v0, Liir;->c:Ljava/lang/Long;

    if-eqz v3, :cond_4

    :cond_3
    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "define() ignoring defined/unconditional/referenceVersion in "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lijy;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0, v1, v0}, Lijt;->a(Landroid/content/SharedPreferences$Editor;Liir;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_5
    :try_start_1
    const-string v0, "defined"

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v5, "define"

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lijt;->a(Landroid/content/SharedPreferences$Editor;ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liir;

    invoke-direct {p0, p1, v0}, Lijt;->a(Ljava/lang/String;Liir;)V

    goto :goto_2

    :cond_6
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v6

    goto :goto_0
.end method

.method final b()Z
    .locals 2

    invoke-static {}, Lijv;->a()V

    sget-object v1, Lijt;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lijt;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    invoke-direct {p0}, Lijt;->d()Z

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
