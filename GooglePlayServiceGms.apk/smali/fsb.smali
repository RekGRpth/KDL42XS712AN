.class public Lfsb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lfsb;


# instance fields
.field private final b:Ldl;

.field private final c:Ldl;

.field private final d:Ldl;


# direct methods
.method constructor <init>()V
    .locals 2

    const/16 v1, 0x14

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ldl;

    invoke-direct {v0, v1}, Ldl;-><init>(I)V

    iput-object v0, p0, Lfsb;->b:Ldl;

    new-instance v0, Ldl;

    invoke-direct {v0, v1}, Ldl;-><init>(I)V

    iput-object v0, p0, Lfsb;->c:Ldl;

    new-instance v0, Ldl;

    invoke-direct {v0, v1}, Ldl;-><init>(I)V

    iput-object v0, p0, Lfsb;->d:Ldl;

    return-void
.end method

.method public static a()Lfsb;
    .locals 2

    const-class v1, Lfsb;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lfsb;->a:Lfsb;

    if-nez v0, :cond_0

    new-instance v0, Lfsb;

    invoke-direct {v0}, Lfsb;-><init>()V

    sput-object v0, Lfsb;->a:Lfsb;

    :cond_0
    sget-object v0, Lfsb;->a:Lfsb;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, "<<null account>>"

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lfsb;->c:Ldl;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfsb;->c:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lfsd;
    .locals 3

    iget-object v1, p0, Lfsb;->b:Ldl;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfsb;->b:Ldl;

    invoke-static {p1}, Lfsb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsd;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)Lgdl;
    .locals 7

    iget-object v5, p0, Lfsb;->b:Ldl;

    monitor-enter v5

    :try_start_0
    invoke-static {p1}, Lfsb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v6

    iget-object v1, p0, Lfsb;->b:Ldl;

    invoke-virtual {v1, v6}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfsd;

    if-nez v1, :cond_0

    const/4 v3, 0x0

    monitor-exit v5

    :goto_0
    return-object v3

    :cond_0
    iget-object v3, v1, Lfsd;->a:Lgdl;

    if-eqz p3, :cond_1

    invoke-static {v3}, Lfsc;->a(Lgdl;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    if-nez p3, :cond_3

    invoke-static {v3}, Lfsc;->a(Lgdl;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move-object v0, v3

    check-cast v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object v2, v0

    invoke-static {v2, p3}, Lfsc;->a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity;Z)V

    move-object v0, v3

    check-cast v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object v2, v0

    if-eqz p3, :cond_4

    const/4 v4, 0x1

    :goto_1
    invoke-static {v2, v4}, Lfsc;->a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity;I)V

    iget-object v2, p0, Lfsb;->b:Ldl;

    invoke-virtual {v2, v6, v1}, Ldl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v5

    throw v1

    :cond_4
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;[B)V
    .locals 2

    iget-object v1, p0, Lfsb;->d:Ldl;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfsb;->d:Ldl;

    invoke-virtual {v0, p1, p2}, Ldl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 2

    iget-object v1, p0, Lfsb;->c:Ldl;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfsb;->c:Ldl;

    invoke-virtual {v0, p1, p2}, Ldl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lfsd;)V
    .locals 3

    iget-object v1, p0, Lfsb;->b:Ldl;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfsb;->b:Ldl;

    invoke-static {p1}, Lfsb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, Ldl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/net/Uri;)[B
    .locals 2

    iget-object v1, p0, Lfsb;->d:Ldl;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfsb;->d:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v1, p0, Lfsb;->b:Ldl;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfsb;->b:Ldl;

    invoke-static {p1}, Lfsb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldl;->b(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
