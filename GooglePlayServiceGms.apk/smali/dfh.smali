.class public final Ldfh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    :cond_0
    iput-object p1, p0, Ldfh;->a:Landroid/os/Bundle;

    return-void
.end method

.method private constructor <init>(Ldfi;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "external_game_id"

    iget-object v2, p1, Ldfi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "external_leaderboard_id"

    iget-object v2, p1, Ldfi;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "time_span"

    iget v2, p1, Ldfi;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "leaderboard_collection"

    iget v2, p1, Ldfi;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "page_type"

    iget v2, p1, Ldfi;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "next_page_token"

    iget-object v2, p1, Ldfi;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "prev_page_token"

    iget-object v2, p1, Ldfi;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Ldfi;B)V
    .locals 0

    invoke-direct {p0, p1}, Ldfh;-><init>(Ldfi;)V

    return-void
.end method

.method public static a()Ldfi;
    .locals 2

    new-instance v0, Ldfi;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldfi;-><init>(B)V

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "external_game_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Ldfh;->a:Landroid/os/Bundle;

    const-string v1, "external_leaderboard_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
