.class public abstract enum Lcbz;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcbz;

.field public static final enum b:Lcbz;

.field public static final enum c:Lcbz;

.field private static final synthetic e:[Lcbz;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcca;

    const-string v1, "DISABLE"

    const-string v2, "disable"

    invoke-direct {v0, v1, v2}, Lcca;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcbz;->a:Lcbz;

    new-instance v0, Lccb;

    const-string v1, "WIFI"

    const-string v2, "wifi"

    invoke-direct {v0, v1, v2}, Lccb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcbz;->b:Lcbz;

    new-instance v0, Lccc;

    const-string v1, "ALWAYS"

    const-string v2, "always"

    invoke-direct {v0, v1, v2}, Lccc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcbz;->c:Lcbz;

    const/4 v0, 0x3

    new-array v0, v0, [Lcbz;

    const/4 v1, 0x0

    sget-object v2, Lcbz;->a:Lcbz;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcbz;->b:Lcbz;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcbz;->c:Lcbz;

    aput-object v2, v0, v1

    sput-object v0, Lcbz;->e:[Lcbz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcbz;->d:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcbz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcbz;
    .locals 5

    invoke-static {}, Lcbz;->values()[Lcbz;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lcbz;->d:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal auto-sync option value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcbz;
    .locals 1

    const-class v0, Lcbz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcbz;

    return-object v0
.end method

.method public static values()[Lcbz;
    .locals 1

    sget-object v0, Lcbz;->e:[Lcbz;

    invoke-virtual {v0}, [Lcbz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcbz;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcbz;->d:Ljava/lang/String;

    return-object v0
.end method

.method public abstract a(Lcot;)Z
.end method
