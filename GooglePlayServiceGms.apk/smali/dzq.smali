.class public final Ldzq;
.super Ldxh;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Z

.field private e:Landroid/net/Uri;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    invoke-direct {p0}, Ldxh;-><init>()V

    iput-object p1, p0, Ldzq;->a:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldzq;->b:Landroid/view/LayoutInflater;

    iput-boolean p2, p0, Ldzq;->c:Z

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    iput p1, p0, Ldzq;->h:I

    invoke-virtual {p0}, Ldzq;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Ldzq;->e:Landroid/net/Uri;

    invoke-virtual {p0}, Ldzq;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ldzq;->f:Ljava/lang/String;

    invoke-virtual {p0}, Ldzq;->notifyDataSetChanged()V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ldzq;->g:Ljava/lang/String;

    invoke-virtual {p0}, Ldzq;->notifyDataSetChanged()V

    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    if-nez p2, :cond_1

    iget-object v0, p0, Ldzq;->b:Landroid/view/LayoutInflater;

    sget v1, Lxc;->t:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Ldzr;

    invoke-direct {v0, p0, p2}, Ldzr;-><init>(Ldzq;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v1, v0, Ldzr;->e:Ldzq;

    iget-object v1, v1, Ldzq;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, v0, Ldzr;->e:Ldzq;

    iget-boolean v2, v2, Ldzq;->c:Z

    if-eqz v2, :cond_2

    iget-object v2, v0, Ldzr;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v2, v0, Ldzr;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v3, v0, Ldzr;->e:Ldzq;

    iget-object v3, v3, Ldzq;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;)V

    sget v2, Lwx;->u:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-object v2, v0, Ldzr;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    :goto_1
    iget-object v1, v0, Ldzr;->b:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldzr;->b:Landroid/widget/TextView;

    iget-object v2, v0, Ldzr;->e:Ldzq;

    iget v2, v2, Ldzq;->h:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    iget-object v1, v0, Ldzr;->e:Ldzq;

    iget-object v1, v1, Ldzq;->f:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, v0, Ldzr;->d:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    iget-object v1, v0, Ldzr;->c:Landroid/widget/TextView;

    iget-object v0, v0, Ldzr;->e:Ldzq;

    iget-object v0, v0, Ldzq;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzr;

    goto :goto_0

    :cond_2
    iget-object v2, v0, Ldzr;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    sget v2, Lwx;->t:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-object v2, v0, Ldzr;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto :goto_1

    :cond_3
    iget-object v1, v0, Ldzr;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldzr;->d:Landroid/widget/TextView;

    iget-object v2, v0, Ldzr;->e:Ldzq;

    iget-object v2, v2, Ldzq;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
