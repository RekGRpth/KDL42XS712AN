.class public final Lbfx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lbfy;

.field public static b:Lbfy;

.field public static c:Lbfy;

.field public static d:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "gms:common:allow_pii_logging"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lbfx;->a:Lbfy;

    const-string v0, "gms:common:system_health_log_delay_after_install_millis"

    const-wide/32 v1, 0xea60

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lbfx;->b:Lbfy;

    const-string v0, "gms:common:show_people_settings"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lbfx;->c:Lbfy;

    const-string v0, "gms:common:show_download_settings"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lbfx;->d:Lbfy;

    return-void
.end method
