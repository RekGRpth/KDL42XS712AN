.class public final Ldbr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbdu;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcwm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public final a(Lbdu;)Lbeh;
    .locals 2

    new-instance v0, Ldbs;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ldbs;-><init>(Ldbr;Z)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ldfg;II)Lbeh;
    .locals 1

    new-instance v0, Ldbx;

    invoke-direct {v0, p0, p2, p3, p4}, Ldbx;-><init>(Ldbr;Ldfg;II)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;IIIZ)Lbeh;
    .locals 7

    new-instance v0, Ldbv;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldbv;-><init>(Ldbr;Ljava/lang/String;IIIZ)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;Ljava/lang/String;IIIZ)Lbeh;
    .locals 8

    new-instance v0, Ldbt;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v0 .. v7}, Ldbt;-><init>(Ldbr;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbdu;Ljava/lang/String;IIIZ)Lbeh;
    .locals 7

    new-instance v0, Ldbw;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldbw;-><init>(Ldbr;Ljava/lang/String;IIIZ)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbdu;Ljava/lang/String;Ljava/lang/String;IIIZ)Lbeh;
    .locals 8

    new-instance v0, Ldbu;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v0 .. v7}, Ldbu;-><init>(Ldbr;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method
