.class public final Livk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[Livl;


# instance fields
.field private final a:Livx;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x0

    const/16 v1, 0xa8

    new-array v1, v1, [Livl;

    sput-object v1, Livk;->b:[Livl;

    move v3, v0

    move v1, v0

    :goto_0
    const/4 v0, 0x7

    if-gt v3, v0, :cond_1

    const/16 v0, 0x10

    :goto_1
    const/16 v2, 0x25

    if-ge v0, v2, :cond_0

    sget-object v4, Livk;->b:[Livl;

    add-int/lit8 v2, v1, 0x1

    new-instance v5, Livl;

    shl-int/lit8 v6, v3, 0x8

    add-int/2addr v6, v0

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Livl;-><init>(ILjava/lang/Object;)V

    aput-object v5, v4, v1

    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Livx;

    invoke-direct {v0}, Livx;-><init>()V

    iput-object v0, p0, Livk;->a:Livx;

    const/4 v0, 0x0

    iput-object v0, p0, Livk;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    iget-object v0, p0, Livk;->a:Livx;

    invoke-virtual {v0, p1}, Livx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livl;

    if-nez v0, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Livl;->a:I

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public final a(IILjava/lang/Object;)Livk;
    .locals 3

    iget-object v1, p0, Livk;->a:Livx;

    if-nez p3, :cond_0

    const v0, 0xff00

    and-int/2addr v0, p1

    shr-int/lit8 v0, v0, 0x8

    mul-int/lit8 v0, v0, 0x15

    and-int/lit16 v2, p1, 0xff

    add-int/lit8 v2, v2, -0x10

    add-int/2addr v0, v2

    sget-object v2, Livk;->b:[Livl;

    aget-object v0, v2, v0

    :goto_0
    invoke-virtual {v1, p2, v0}, Livx;->a(ILjava/lang/Object;)V

    return-object p0

    :cond_0
    new-instance v0, Livl;

    invoke-direct {v0, p1, p3}, Livl;-><init>(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Livi;)Z
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Livk;->a:Livx;

    invoke-virtual {v0}, Livx;->a()Livy;

    move-result-object v4

    :cond_0
    invoke-virtual {v4}, Livy;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Livy;->b()I

    move-result v5

    iget-object v0, p0, Livk;->a:Livx;

    invoke-virtual {v0, v5}, Livx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livl;

    if-nez v0, :cond_1

    const/16 v0, 0x600

    :goto_0
    and-int/lit16 v3, v0, 0x100

    if-eqz v3, :cond_2

    invoke-virtual {p1, v5}, Livi;->i(I)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    iget v0, v0, Livl;->a:I

    const v3, 0xff00

    and-int/2addr v0, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v5}, Livi;->k(I)I

    move-result v6

    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_3

    if-le v6, v2, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v5}, Livk;->a(I)I

    move-result v0

    const/16 v3, 0x1b

    if-ne v0, v3, :cond_0

    invoke-virtual {p0, v5}, Livk;->b(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Livk;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v5}, Livk;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livk;

    move v3, v1

    :goto_2
    if-ge v3, v6, :cond_0

    invoke-virtual {p1, v5, v3}, Livi;->c(II)Livi;

    move-result-object v7

    invoke-virtual {v0, v7}, Livk;->a(Livi;)Z

    move-result v7

    if-nez v7, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Livk;->a:Livx;

    invoke-virtual {v0, p1}, Livx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livl;

    if-nez v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Livl;->b:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-ne p0, p1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    check-cast p1, Livk;

    iget-object v0, p0, Livk;->a:Livx;

    iget-object v1, p1, Livk;->a:Livx;

    invoke-virtual {v0, v1}, Livx;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Livk;->a:Livx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Livk;->a:Livx;

    invoke-virtual {v0}, Livx;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ProtoBufType Name: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Livk;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
