.class public final Ljbz;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:Z

.field private d:J

.field private e:Z

.field private f:J

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0}, Lizk;-><init>()V

    iput v0, p0, Ljbz;->b:I

    iput-wide v1, p0, Ljbz;->d:J

    iput-wide v1, p0, Ljbz;->f:J

    iput v0, p0, Ljbz;->g:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljbz;->g:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljbz;->b()I

    :cond_0
    iget v0, p0, Ljbz;->g:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Ljbz;->a(I)Ljbz;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljbz;->a(J)Ljbz;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljbz;->b(J)Ljbz;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(I)Ljbz;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljbz;->a:Z

    iput p1, p0, Ljbz;->b:I

    return-object p0
.end method

.method public final a(J)Ljbz;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljbz;->c:Z

    iput-wide p1, p0, Ljbz;->d:J

    return-object p0
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Ljbz;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljbz;->b:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Ljbz;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v1, p0, Ljbz;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->b(IJ)V

    :cond_1
    iget-boolean v0, p0, Ljbz;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Ljbz;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->b(IJ)V

    :cond_2
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljbz;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljbz;->b:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljbz;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Ljbz;->d:J

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljbz;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Ljbz;->f:J

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Ljbz;->g:I

    return v0
.end method

.method public final b(J)Ljbz;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljbz;->e:Z

    iput-wide p1, p0, Ljbz;->f:J

    return-object p0
.end method
