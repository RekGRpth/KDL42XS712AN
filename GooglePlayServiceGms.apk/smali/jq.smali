.class public abstract Ljq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljp;

.field b:Z

.field c:Z

.field private d:Ljj;

.field private e:Landroid/view/MenuInflater;

.field private f:Z


# direct methods
.method constructor <init>(Ljp;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ljq;->a:Ljp;

    return-void
.end method


# virtual methods
.method abstract a()Ljj;
.end method

.method abstract a(I)V
.end method

.method a(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Ljq;->a:Ljp;

    sget-object v1, Lkq;->c:[I

    invoke-virtual {v0, v1}, Ljp;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Ljq;->b:Z

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Ljq;->c:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v0, p0, Ljq;->a:Ljp;

    invoke-static {v0}, Lao;->b(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljq;->d:Ljj;

    if-nez v0, :cond_2

    iput-boolean v3, p0, Ljq;->f:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Ljq;->d:Ljj;

    invoke-virtual {v0, v3}, Ljj;->a(Z)V

    goto :goto_0
.end method

.method abstract a(Landroid/view/View;)V
.end method

.method abstract a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method abstract a(Ljava/lang/CharSequence;)V
.end method

.method abstract a(ILandroid/view/Menu;)Z
.end method

.method abstract a(ILandroid/view/MenuItem;)Z
.end method

.method abstract a(ILandroid/view/View;Landroid/view/Menu;)Z
.end method

.method abstract b(I)Landroid/view/View;
.end method

.method public final b()Ljj;
    .locals 2

    iget-boolean v0, p0, Ljq;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ljq;->c:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Ljq;->d:Ljj;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljq;->a()Ljj;

    move-result-object v0

    iput-object v0, p0, Ljq;->d:Ljj;

    iget-boolean v0, p0, Ljq;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljq;->d:Ljj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljj;->a(Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Ljq;->d:Ljj;

    return-object v0

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Ljq;->d:Ljj;

    goto :goto_0
.end method

.method abstract b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method final c()Landroid/view/MenuInflater;
    .locals 2

    iget-object v0, p0, Ljq;->e:Landroid/view/MenuInflater;

    if-nez v0, :cond_0

    new-instance v0, Lku;

    invoke-virtual {p0}, Ljq;->k()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lku;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljq;->e:Landroid/view/MenuInflater;

    :cond_0
    iget-object v0, p0, Ljq;->e:Landroid/view/MenuInflater;

    return-object v0
.end method

.method abstract d()V
.end method

.method abstract e()V
.end method

.method abstract f()V
.end method

.method abstract g()V
.end method

.method abstract h()Z
.end method

.method abstract i()V
.end method

.method protected final j()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Ljq;->a:Ljp;

    invoke-virtual {v1}, Ljp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Ljq;->a:Ljp;

    invoke-virtual {v2}, Ljp;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    iget-object v2, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "android.support.UI_OPTIONS"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v1, "ActionBarActivityDelegate"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getUiOptionsFromMetadata: Activity \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Ljq;->a:Ljp;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' not in manifest"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final k()Landroid/content/Context;
    .locals 2

    iget-object v0, p0, Ljq;->a:Ljp;

    invoke-virtual {p0}, Ljq;->b()Ljj;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljj;->d()Landroid/content/Context;

    move-result-object v0

    :cond_0
    return-object v0
.end method
