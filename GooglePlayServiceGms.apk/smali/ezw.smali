.class public final Lezw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lfaf;

.field final b:F

.field public final c:F

.field public d:F

.field public final e:F

.field public final f:F

.field public g:F

.field public h:F

.field public final i:Lezp;


# direct methods
.method public constructor <init>(Lfaf;Lezp;)V
    .locals 6

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lezw;->a:Lfaf;

    iput-object p2, p0, Lezw;->i:Lezp;

    iget-object v0, p0, Lezw;->i:Lezp;

    iget v0, v0, Lezp;->h:I

    int-to-float v0, v0

    iget-object v1, p0, Lezw;->i:Lezp;

    iget v1, v1, Lezp;->j:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lezw;->b:F

    iget-object v0, p0, Lezw;->i:Lezp;

    iget v0, v0, Lezp;->i:I

    int-to-float v0, v0

    iget-object v1, p0, Lezw;->i:Lezp;

    iget v1, v1, Lezp;->k:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lezw;->c:F

    iget-object v0, p0, Lezw;->i:Lezp;

    iget v0, v0, Lezp;->l:I

    int-to-float v0, v0

    iget-object v1, p0, Lezw;->i:Lezp;

    iget v1, v1, Lezp;->j:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lezw;->f:F

    iget-object v0, p0, Lezw;->i:Lezp;

    iget v0, v0, Lezp;->m:I

    int-to-float v0, v0

    iget-object v1, p0, Lezw;->i:Lezp;

    iget v1, v1, Lezp;->k:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lezw;->e:F

    return-void
.end method
