.class public abstract Lalu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lalr;


# instance fields
.field protected final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lalu;->b:Ljava/lang/String;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Lalu;->a:Lcom/google/android/gms/common/server/ClientContext;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lakk;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lalu;->b(Landroid/content/Context;Lakk;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Laln; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    :try_start_1
    invoke-virtual {p0, v0}, Lalu;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lalu;->b:Ljava/lang/String;

    const-string v2, "Auth error while performing operation, requesting reconnect"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lalu;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing operation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Laln;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Laln;->a()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v1, p0, Lalu;->b:Ljava/lang/String;

    const-string v2, "Runtime exception while performing operation"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lalu;->b:Ljava/lang/String;

    const-string v2, "Killing (on development devices) due to RuntimeException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v1
.end method

.method protected abstract a(Lcom/google/android/gms/common/data/DataHolder;)V
.end method

.method protected abstract b(Landroid/content/Context;Lakk;)Lcom/google/android/gms/common/data/DataHolder;
.end method
