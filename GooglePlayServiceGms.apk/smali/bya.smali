.class public final Lbya;
.super Lbxf;
.source "SourceFile"


# instance fields
.field private Y:Landroid/widget/EditText;

.field private Z:Lcad;

.field private aa:Z

.field private ab:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lbxf;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbya;->aa:Z

    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)Lbya;
    .locals 3

    new-instance v0, Lbya;

    invoke-direct {v0}, Lbya;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "progressTextId"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Lbya;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lbya;)Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;
    .locals 1

    iget-object v0, p0, Lbya;->ab:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    return-object v0
.end method

.method static synthetic b(Lbya;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbya;->aa:Z

    return v0
.end method

.method static synthetic c(Lbya;)Lcad;
    .locals 1

    iget-object v0, p0, Lbya;->Z:Lcad;

    return-object v0
.end method

.method static synthetic d(Lbya;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lbya;->Y:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lbxf;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "progressTextId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    iput-object v0, p0, Lbya;->ab:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;

    new-instance v1, Lbyb;

    invoke-direct {v1, p0, v0}, Lbyb;-><init>(Lbya;Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)V

    iput-object v1, p0, Lbya;->Z:Lcad;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Lcaa;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lbya;->Y:Landroid/widget/EditText;

    iget-object v1, p0, Lbya;->ab:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbya;->Y:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lbya;->Y:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    iget-object v1, p0, Lbya;->Y:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    iget-object v1, p0, Lbya;->Y:Landroid/widget/EditText;

    const/16 v2, 0x4001

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    invoke-static {v0}, Lcaa;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lbya;->ab:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    iget v0, v0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->d:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lbya;->Y:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v0, 0x104000a    # android.R.string.ok

    new-instance v2, Lbyc;

    invoke-direct {v2, p0}, Lbyc;-><init>(Lbya;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v0, 0x1040000    # android.R.string.cancel

    new-instance v2, Lbyd;

    invoke-direct {v2, p0}, Lbyd;-><init>(Lbya;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget-object v0, Lcae;->a:Lcae;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iget-object v1, p0, Lbya;->Y:Landroid/widget/EditText;

    new-instance v2, Lbye;

    invoke-direct {v2, p0, v0}, Lbye;-><init>(Lbya;Landroid/app/Dialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lbya;->Y:Landroid/widget/EditText;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcab;

    const v3, 0x1020019    # android.R.id.button1

    invoke-direct {v2, v0, v3}, Lcab;-><init>(Landroid/app/Dialog;I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-object v0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    iget-boolean v0, p0, Lbya;->aa:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbya;->Z:Lcad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbya;->Z:Lcad;

    invoke-interface {v0}, Lcad;->a()V

    :cond_0
    invoke-super {p0, p1}, Lbxf;->onDismiss(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public final w()V
    .locals 2

    invoke-super {p0}, Lbxf;->w()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbya;->aa:Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lbya;->ab:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->b:I

    invoke-virtual {v0, v1}, Lo;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbya;->Y:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
