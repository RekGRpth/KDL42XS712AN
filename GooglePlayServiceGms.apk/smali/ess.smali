.class public final Less;
.super Lbje;
.source "SourceFile"


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Lbbr;Lbbs;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbje;-><init>(Landroid/content/Context;Lbbr;Lbbs;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Leso;->a(Landroid/os/IBinder;)Lesn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 2

    invoke-virtual {p0}, Less;->h()V

    :try_start_0
    invoke-virtual {p0}, Less;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lesn;

    invoke-interface {v0, p1}, Lesn;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
.end method

.method protected final a(Lbjy;Lbjj;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x41fe88

    iget-object v2, p0, Lbje;->b_:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lbjy;->d(Lbjv;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.reporting.service.START"

    return-object v0
.end method

.method protected final c_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.reporting.internal.IReportingService"

    return-object v0
.end method
