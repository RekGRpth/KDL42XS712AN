.class public abstract Laoa;
.super Lo;
.source "SourceFile"

# interfaces
.implements Laod;


# instance fields
.field protected o:Ljava/lang/String;

.field protected p:Ljby;

.field protected q:Lano;

.field protected r:J

.field protected s:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lo;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Laoa;->s:Z

    return-void
.end method

.method private i()Landroid/app/PendingIntent;
    .locals 3

    invoke-virtual {p0}, Laoa;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "cancel_prompt"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private j()Ljava/security/KeyPair;
    .locals 2

    :try_start_0
    iget-object v0, p0, Laoa;->q:Lano;

    const-string v1, "device_key"

    invoke-virtual {v0, v1}, Lano;->a(Ljava/lang/String;)Ljava/security/KeyPair;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lanp; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final a(I)V
    .locals 5

    const/4 v0, 0x1

    iput-boolean v0, p0, Laoa;->s:Z

    new-instance v0, Laob;

    iget-object v1, p0, Laoa;->o:Ljava/lang/String;

    invoke-direct {p0}, Laoa;->j()Ljava/security/KeyPair;

    move-result-object v2

    invoke-virtual {p0}, Laoa;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "encryption_key_handle"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    iget-object v4, p0, Laoa;->q:Lano;

    invoke-virtual {v4, v3}, Lano;->a([B)Lanj;

    move-result-object v3

    iget-object v3, v3, Lanj;->a:Lank;

    iget-object v3, v3, Lank;->b:Ljavax/crypto/SecretKey;

    invoke-direct {v0, p0, v1, v2, v3}, Laob;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/security/KeyPair;Ljavax/crypto/SecretKey;)V

    new-instance v1, Ljbz;

    invoke-direct {v1}, Ljbz;-><init>()V

    invoke-virtual {v1, p1}, Ljbz;->a(I)Ljbz;

    move-result-object v1

    iget-wide v2, p0, Laoa;->r:J

    invoke-virtual {v1, v2, v3}, Ljbz;->a(J)Ljbz;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljbz;->b(J)Ljbz;

    move-result-object v1

    new-instance v2, Ljbw;

    invoke-direct {v2}, Ljbw;-><init>()V

    iget-object v3, p0, Laoa;->p:Ljby;

    invoke-virtual {v2, v3}, Ljbw;->a(Ljby;)Ljbw;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljbw;->a(Ljbz;)Ljbw;

    move-result-object v1

    new-instance v2, Ljcn;

    sget-object v3, Ljco;->d:Ljco;

    invoke-virtual {v1}, Ljbw;->d()[B

    move-result-object v1

    invoke-direct {v2, v3, v1}, Ljcn;-><init>(Ljco;[B)V

    invoke-virtual {v0, v2}, Laob;->a(Ljcn;)V

    return-void
.end method

.method protected final f()Z
    .locals 1

    iget-boolean v0, p0, Laoa;->s:Z

    return v0
.end method

.method protected final g()Ljby;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Laoa;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tx_request"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Ljby;->a([B)Ljby;
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to parse TxRequest"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final h()I
    .locals 6

    const-wide/32 v4, 0xea60

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Laoa;->r:J

    sub-long/2addr v0, v2

    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    div-long/2addr v0, v4

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Laoa;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "cancel_prompt"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AuthZen"

    const-string v1, "Prompt canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v4}, Laoa;->setResult(I)V

    invoke-virtual {p0}, Laoa;->finish()V

    :goto_0
    return-void

    :cond_0
    iput-boolean v4, p0, Laoa;->s:Z

    iget-object v0, p0, Laoa;->q:Lano;

    if-nez v0, :cond_1

    new-instance v0, Lano;

    invoke-direct {v0, p0}, Lano;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laoa;->q:Lano;

    :cond_1
    invoke-virtual {p0}, Laoa;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoa;->o:Ljava/lang/String;

    invoke-virtual {p0}, Laoa;->g()Ljby;

    move-result-object v0

    iput-object v0, p0, Laoa;->p:Ljby;

    if-nez p1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_1
    iput-wide v0, p0, Laoa;->r:J

    iget-object v0, p0, Laoa;->p:Ljby;

    iget-wide v0, v0, Ljby;->a:J

    iget-object v4, p0, Laoa;->p:Ljby;

    iget-object v4, v4, Ljby;->c:Ljbx;

    iget-wide v4, v4, Ljbx;->a:J

    sub-long v0, v4, v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-string v6, "AuthZen"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Prompt lifetime in millis: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    cmp-long v6, v0, v2

    if-gez v6, :cond_3

    move-wide v1, v2

    :goto_2
    invoke-direct {p0}, Laoa;->i()Landroid/app/PendingIntent;

    move-result-object v3

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Laoa;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v6, 0x2

    add-long/2addr v1, v4

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    :cond_2
    const-string v0, "creation_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_1

    :cond_3
    move-wide v1, v0

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lo;->onDestroy()V

    invoke-direct {p0}, Laoa;->i()Landroid/app/PendingIntent;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Laoa;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method
