.class public final Lgut;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lorg/apache/http/client/HttpClient;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgut;->a:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 10

    const/16 v8, 0x6000

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v3, 0x0

    if-gez p2, :cond_0

    const/4 p2, 0x4

    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "application/x-protobuf"

    move-object v4, v2

    :goto_0
    if-eqz p0, :cond_9

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    const-string v2, "ServerConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "HTTP response with sc="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v2, 0x191

    if-ne v5, v2, :cond_c

    const/4 v2, 0x5

    :goto_1
    if-eqz v4, :cond_1

    const-string v0, "Content-Type"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_1
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    long-to-int v4, v6

    if-le v4, v8, :cond_3

    new-instance v0, Ljava/io/IOException;

    const-string v3, "Http Response content longer than expected 24KB limit"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    const-string v3, "ServerConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Response Content unavailable for expected response type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v2

    :goto_3
    :sswitch_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v2, "ServerConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected serverResponseType="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    :pswitch_1
    new-instance v2, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v2, p1, v0, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(I[BLjava/lang/Class;)V

    return-object v2

    :cond_2
    move-object v4, v1

    goto/16 :goto_0

    :cond_3
    if-lez v4, :cond_6

    :try_start_1
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    new-array v0, v4, [B
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_5
    if-ge v3, v4, :cond_4

    sub-int v7, v4, v3

    :try_start_2
    invoke-virtual {v6, v0, v3, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    if-lez v7, :cond_4

    add-int/2addr v3, v7

    goto :goto_5

    :cond_4
    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    move-result v7

    if-eqz v7, :cond_5

    new-instance v3, Ljava/lang/IllegalStateException;

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v7, "Stream still has %d bytes left after reading Http content length of %d bytes"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v8, v9

    const/4 v6, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v6

    invoke-static {v5, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_1
    move-exception v3

    goto :goto_2

    :cond_5
    if-eq v3, v4, :cond_8

    new-instance v5, Ljava/lang/IllegalStateException;

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v7, "Http Content Length of %d doesn\'t match input stream length of %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v4, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v4

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v3

    :goto_6
    const-string v3, "ServerConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Response Content unavailable for expected response type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_7
    move p1, v2

    goto/16 :goto_3

    :cond_6
    :try_start_3
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v4, 0x6000

    new-array v4, v4, [B

    :goto_8
    const/4 v6, 0x0

    array-length v7, v4

    invoke-virtual {v0, v4, v6, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_7

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_6

    :cond_7
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->flush()V

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    :cond_8
    :goto_9
    sparse-switch v5, :sswitch_data_0

    move p1, p2

    goto/16 :goto_3

    :cond_9
    const-string v2, "ServerConnection"

    const-string v3, "Null HTTP response"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v0

    move-object v0, v1

    goto/16 :goto_3

    :pswitch_2
    const-class v1, Lipe;

    goto/16 :goto_4

    :pswitch_3
    const-class v1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    goto/16 :goto_4

    :pswitch_4
    const-class v1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    goto/16 :goto_4

    :pswitch_5
    const-class v1, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    goto/16 :goto_4

    :pswitch_6
    const-class v1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    goto/16 :goto_4

    :pswitch_7
    const-class v1, Ljaz;

    goto/16 :goto_4

    :pswitch_8
    const-class v1, Ljax;

    goto/16 :goto_4

    :pswitch_9
    const-class v1, Ljbb;

    goto/16 :goto_4

    :pswitch_a
    const-class v1, Ljav;

    goto/16 :goto_4

    :pswitch_b
    const-class v1, Lipy;

    goto/16 :goto_4

    :pswitch_c
    const-class v1, Lizz;

    goto/16 :goto_4

    :pswitch_d
    const-class v1, Ljap;

    goto/16 :goto_4

    :pswitch_e
    const-class v1, Ljar;

    goto/16 :goto_4

    :pswitch_f
    const-class v1, Lipl;

    goto/16 :goto_4

    :pswitch_10
    const-class v1, Lipn;

    goto/16 :goto_4

    :pswitch_11
    const-class v1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    goto/16 :goto_4

    :pswitch_12
    const-class v1, Lipg;

    goto/16 :goto_4

    :pswitch_13
    const-class v1, Lipd;

    goto/16 :goto_4

    :pswitch_14
    const-class v1, Liot;

    goto/16 :goto_4

    :pswitch_15
    const-class v1, Liox;

    goto/16 :goto_4

    :pswitch_16
    const-class v1, Liov;

    goto/16 :goto_4

    :pswitch_17
    const-class v1, Ljat;

    goto/16 :goto_4

    :cond_a
    move-object v0, v1

    goto :goto_9

    :cond_b
    move-object v0, v1

    goto/16 :goto_7

    :cond_c
    move v2, v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_10
        :pswitch_f
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_11
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x191 -> :sswitch_0
    .end sparse-switch
.end method

.method private static a(Lhgm;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    const-string v1, "Content-Type"

    invoke-static {v1, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p0, :cond_1

    const-string v1, "Authorization"

    invoke-virtual {p0}, Lhgm;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const-string v1, "X-Modality"

    const-string v2, "ANDROID_NATIVE"

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "X-Version"

    const v2, 0x41fea6

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;Lorg/apache/http/HttpEntity;)Lorg/apache/http/HttpResponse;
    .locals 5

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot make network request from UI thread!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2, p3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    const-string v0, "ServerConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Http request sent to url="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3

    :try_start_0
    iget-object v0, p0, Lgut;->a:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lhgm;Ljava/util/List;I)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    const/4 v4, 0x0

    const/16 v6, 0x11

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Lgut;->a(Ljava/lang/String;Lhgm;Ljava/util/List;Ljava/util/ArrayList;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lhgm;Ljava/util/List;Ljava/util/ArrayList;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 3

    :try_start_0
    const-string v0, "application/x-www-form-urlencoded"

    invoke-static {p2, v0}, Lgut;->a(Lhgm;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz p4, :cond_0

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    new-instance v1, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v2, "utf-8"

    invoke-direct {v1, p3, v2}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1}, Lgut;->a(Ljava/lang/String;Ljava/util/ArrayList;Lorg/apache/http/HttpEntity;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {v0, p5, p6}, Lgut;->a(Lorg/apache/http/HttpResponse;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "ServerConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception sending HTTP request to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lhgm;[BI)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    const/4 v4, 0x0

    const/16 v6, 0x11

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Lgut;->a(Ljava/lang/String;Lhgm;[BLjava/util/ArrayList;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lhgm;[BLjava/util/ArrayList;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 3

    :try_start_0
    const-string v0, "application/x-protobuf"

    invoke-static {p2, v0}, Lgut;->a(Lhgm;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz p4, :cond_0

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, p3}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-direct {p0, p1, v0, v1}, Lgut;->a(Ljava/lang/String;Ljava/util/ArrayList;Lorg/apache/http/HttpEntity;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {v0, p5, p6}, Lgut;->a(Lorg/apache/http/HttpResponse;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "ServerConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception sending HTTP request to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0
.end method
