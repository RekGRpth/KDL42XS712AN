.class public final Lauk;
.super Laut;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/login/LoginActivityTask;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/login/LoginActivityTask;Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    iput-object p1, p0, Lauk;->a:Lcom/google/android/gms/auth/login/LoginActivityTask;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Laut;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-super {p0, p1}, Laut;->onPostExecute(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v0

    invoke-virtual {v0}, Laso;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GLSActivity"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lauk;->a:Lcom/google/android/gms/auth/login/LoginActivityTask;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/LoginActivityTask;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " #onPostExecute(Intent) - status = %s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "GLSActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "token_response"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lauk;->a:Lcom/google/android/gms/auth/login/LoginActivityTask;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lauk;->a:Lcom/google/android/gms/auth/login/LoginActivityTask;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->finish()V

    return-void
.end method
