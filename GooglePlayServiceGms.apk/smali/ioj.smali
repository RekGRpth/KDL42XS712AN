.class public final Lioj;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile n:[Lioj;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:Lipv;

.field public f:Z

.field public g:[I

.field public h:I

.field public i:Ljava/lang/String;

.field public j:I

.field public k:I

.field public l:I

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lioj;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lioj;->b:Ljava/lang/String;

    iput v1, p0, Lioj;->c:I

    const/16 v0, 0x64

    iput v0, p0, Lioj;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lioj;->e:Lipv;

    iput-boolean v1, p0, Lioj;->f:Z

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Lioj;->g:[I

    iput v1, p0, Lioj;->h:I

    const-string v0, ""

    iput-object v0, p0, Lioj;->i:Ljava/lang/String;

    iput v1, p0, Lioj;->j:I

    iput v1, p0, Lioj;->k:I

    iput v1, p0, Lioj;->l:I

    const-string v0, ""

    iput-object v0, p0, Lioj;->m:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lioj;->C:I

    return-void
.end method

.method public static c()[Lioj;
    .locals 2

    sget-object v0, Lioj;->n:[Lioj;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lioj;->n:[Lioj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lioj;

    sput-object v0, Lioj;->n:[Lioj;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lioj;->n:[Lioj;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v2, p0, Lioj;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Lioj;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lioj;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Lioj;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget v2, p0, Lioj;->c:I

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iget v3, p0, Lioj;->c:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lioj;->d:I

    const/16 v3, 0x64

    if-eq v2, v3, :cond_3

    const/4 v2, 0x4

    iget v3, p0, Lioj;->d:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-boolean v2, p0, Lioj;->f:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-boolean v3, p0, Lioj;->f:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lioj;->g:[I

    if-eqz v2, :cond_6

    iget-object v2, p0, Lioj;->g:[I

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    :goto_0
    iget-object v3, p0, Lioj;->g:[I

    array-length v3, v3

    if-ge v1, v3, :cond_5

    iget-object v3, p0, Lioj;->g:[I

    aget v3, v3, v1

    invoke-static {v3}, Lizn;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    add-int/2addr v0, v2

    iget-object v1, p0, Lioj;->g:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lioj;->h:I

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget v2, p0, Lioj;->h:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lioj;->e:Lipv;

    if-eqz v1, :cond_8

    const/16 v1, 0xc

    iget-object v2, p0, Lioj;->e:Lipv;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lioj;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/16 v1, 0xd

    iget-object v2, p0, Lioj;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lioj;->j:I

    if-eqz v1, :cond_a

    const/16 v1, 0xe

    iget v2, p0, Lioj;->j:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lioj;->k:I

    if-eqz v1, :cond_b

    const/16 v1, 0xf

    iget v2, p0, Lioj;->k:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lioj;->l:I

    if-eqz v1, :cond_c

    const/16 v1, 0x10

    iget v2, p0, Lioj;->l:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lioj;->m:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const/16 v1, 0x11

    iget-object v2, p0, Lioj;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iput v0, p0, Lioj;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 7

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lioj;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lioj;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_4
    iput v0, p0, Lioj;->c:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    goto :goto_0

    :sswitch_6
    iput v0, p0, Lioj;->d:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lioj;->f:Z

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lizm;->a()I

    :cond_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lioj;->g:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lioj;->g:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lioj;->g:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lioj;->g:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lioj;->g:[I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lizm;->e(I)V

    iget-object v1, p0, Lioj;->g:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lioj;->g:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lizm;->k()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lizm;->g()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lioj;->g:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lioj;->g:[I

    :cond_a
    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    iput v0, p0, Lioj;->h:I

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lioj;->e:Lipv;

    if-nez v0, :cond_b

    new-instance v0, Lipv;

    invoke-direct {v0}, Lipv;-><init>()V

    iput-object v0, p0, Lioj;->e:Lipv;

    :cond_b
    iget-object v0, p0, Lioj;->e:Lipv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lioj;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lioj;->j:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lioj;->k:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_4
    iput v0, p0, Lioj;->l:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lioj;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_5
        0x30 -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x8a -> :sswitch_10
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x3 -> :sswitch_4
        0x4 -> :sswitch_4
        0x5 -> :sswitch_4
        0x6 -> :sswitch_4
        0x7 -> :sswitch_4
        0x1b -> :sswitch_4
        0x20 -> :sswitch_4
        0x21 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_6
        0x7 -> :sswitch_6
        0x64 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    iget-object v0, p0, Lioj;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lioj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lioj;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lioj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Lioj;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lioj;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_2
    iget v0, p0, Lioj;->d:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lioj;->d:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_3
    iget-boolean v0, p0, Lioj;->f:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    iget-boolean v1, p0, Lioj;->f:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_4
    iget-object v0, p0, Lioj;->g:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lioj;->g:[I

    array-length v0, v0

    if-lez v0, :cond_5

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lioj;->g:[I

    array-length v1, v1

    if-ge v0, v1, :cond_5

    const/16 v1, 0x8

    iget-object v2, p0, Lioj;->g:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lizn;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget v0, p0, Lioj;->h:I

    if-eqz v0, :cond_6

    const/16 v0, 0x9

    iget v1, p0, Lioj;->h:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_6
    iget-object v0, p0, Lioj;->e:Lipv;

    if-eqz v0, :cond_7

    const/16 v0, 0xc

    iget-object v1, p0, Lioj;->e:Lipv;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_7
    iget-object v0, p0, Lioj;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const/16 v0, 0xd

    iget-object v1, p0, Lioj;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_8
    iget v0, p0, Lioj;->j:I

    if-eqz v0, :cond_9

    const/16 v0, 0xe

    iget v1, p0, Lioj;->j:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_9
    iget v0, p0, Lioj;->k:I

    if-eqz v0, :cond_a

    const/16 v0, 0xf

    iget v1, p0, Lioj;->k:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_a
    iget v0, p0, Lioj;->l:I

    if-eqz v0, :cond_b

    const/16 v0, 0x10

    iget v1, p0, Lioj;->l:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_b
    iget-object v0, p0, Lioj;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0x11

    iget-object v1, p0, Lioj;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_c
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lioj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lioj;

    iget-object v2, p0, Lioj;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, p1, Lioj;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lioj;->a:Ljava/lang/String;

    iget-object v3, p1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lioj;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lioj;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lioj;->b:Ljava/lang/String;

    iget-object v3, p1, Lioj;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lioj;->c:I

    iget v3, p1, Lioj;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lioj;->d:I

    iget v3, p1, Lioj;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lioj;->e:Lipv;

    if-nez v2, :cond_9

    iget-object v2, p1, Lioj;->e:Lipv;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lioj;->e:Lipv;

    iget-object v3, p1, Lioj;->e:Lipv;

    invoke-virtual {v2, v3}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-boolean v2, p0, Lioj;->f:Z

    iget-boolean v3, p1, Lioj;->f:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lioj;->g:[I

    iget-object v3, p1, Lioj;->g:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget v2, p0, Lioj;->h:I

    iget v3, p1, Lioj;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Lioj;->i:Ljava/lang/String;

    if-nez v2, :cond_e

    iget-object v2, p1, Lioj;->i:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    goto :goto_0

    :cond_e
    iget-object v2, p0, Lioj;->i:Ljava/lang/String;

    iget-object v3, p1, Lioj;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget v2, p0, Lioj;->j:I

    iget v3, p1, Lioj;->j:I

    if-eq v2, v3, :cond_10

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget v2, p0, Lioj;->k:I

    iget v3, p1, Lioj;->k:I

    if-eq v2, v3, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget v2, p0, Lioj;->l:I

    iget v3, p1, Lioj;->l:I

    if-eq v2, v3, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-object v2, p0, Lioj;->m:Ljava/lang/String;

    if-nez v2, :cond_13

    iget-object v2, p1, Lioj;->m:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Lioj;->m:Ljava/lang/String;

    iget-object v3, p1, Lioj;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lioj;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lioj;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lioj;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lioj;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lioj;->e:Lipv;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lioj;->f:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lioj;->g:[I

    invoke-static {v2}, Lizq;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lioj;->h:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lioj;->i:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lioj;->j:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lioj;->k:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lioj;->l:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lioj;->m:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lioj;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lioj;->e:Lipv;

    invoke-virtual {v0}, Lipv;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lioj;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lioj;->m:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method
