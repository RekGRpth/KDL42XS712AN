.class final Lcqq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcrc;


# instance fields
.field final a:Ljava/util/Map;

.field final b:Lcqo;

.field private final c:Lcrc;


# direct methods
.method public constructor <init>(Lcqo;Lcrc;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcqq;->a:Ljava/util/Map;

    iput-object p1, p0, Lcqq;->b:Lcqo;

    iput-object p2, p0, Lcqq;->c:Lcrc;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcqw;
    .locals 3

    new-instance v1, Lcqw;

    invoke-direct {v1}, Lcqw;-><init>()V

    :try_start_0
    iget-object v0, p0, Lcqq;->b:Lcqo;

    invoke-virtual {v0, p1}, Lcqo;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lcqw;->a(Ljava/lang/Object;)Lcqw;

    :goto_0
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    iget-object v2, p0, Lcqq;->a:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catch Lcqp; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v0, p0, Lcqq;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqw;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcqq;->a:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v2

    iget-object v0, p0, Lcqq;->c:Lcrc;

    invoke-interface {v0, p1}, Lcrc;->a(Ljava/lang/Object;)Lcqw;

    move-result-object v0

    new-instance v2, Lcqr;

    invoke-direct {v2, p0, p1, v1}, Lcqr;-><init>(Lcqq;Ljava/lang/Object;Lcqw;)V

    invoke-virtual {v0, v2}, Lcqw;->a(Lcqy;)Lcqw;
    :try_end_2
    .catch Lcqp; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v1, v0}, Lcqw;->a(Ljava/lang/Throwable;)Lcqw;

    goto :goto_0

    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2

    throw v0
    :try_end_4
    .catch Lcqp; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    invoke-virtual {v1, v0}, Lcqw;->a(Ljava/lang/Throwable;)Lcqw;

    iget-object v2, p0, Lcqq;->a:Ljava/util/Map;

    monitor-enter v2

    :try_start_5
    iget-object v0, p0, Lcqq;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method
