.class public final Lcph;
.super Lcqm;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/droidguard/DroidGuardService;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/droidguard/DroidGuardService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-direct {p0}, Lcqm;-><init>()V

    iput-object p2, p0, Lcph;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcqi;Ljava/lang/String;Ljava/util/Map;)V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;

    move-result-object v0

    new-instance v1, Lcpl;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcpl;-><init>(B)V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->b(Lcom/google/android/gms/droidguard/DroidGuardService;)Landroid/os/Handler;

    move-result-object v1

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->c(Lcom/google/android/gms/droidguard/DroidGuardService;)Lcqs;

    move-result-object v0

    iget-object v1, p0, Lcph;->b:Ljava/lang/String;

    new-instance v2, Lcqw;

    invoke-direct {v2}, Lcqw;-><init>()V

    iget-object v3, v0, Lcqs;->c:Lcrc;

    new-instance v4, Lcrl;

    invoke-direct {v4, p2, v1}, Lcrl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcrc;->a(Ljava/lang/Object;)Lcqw;

    move-result-object v1

    new-instance v3, Lcqt;

    invoke-direct {v3, v0, p2, p3, v2}, Lcqt;-><init>(Lcqs;Ljava/lang/String;Ljava/util/Map;Lcqw;)V

    invoke-virtual {v1, v3}, Lcqw;->a(Lcqy;)Lcqw;

    new-instance v0, Lcpi;

    invoke-direct {v0, p0, p1, p2, p3}, Lcpi;-><init>(Lcph;Lcqi;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v2, v0}, Lcqw;->a(Lcqy;)Lcqw;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->b(Lcom/google/android/gms/droidguard/DroidGuardService;)Landroid/os/Handler;

    move-result-object v1

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-interface {p1, v0}, Lcqi;->a([B)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->b(Lcom/google/android/gms/droidguard/DroidGuardService;)Landroid/os/Handler;

    move-result-object v1

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v1, "DroidGuardService"

    const-string v2, "Remote callback failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v0

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->b(Lcom/google/android/gms/droidguard/DroidGuardService;)Landroid/os/Handler;

    move-result-object v2

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    throw v1
.end method
