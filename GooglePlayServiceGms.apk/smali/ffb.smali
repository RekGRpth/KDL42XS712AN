.class public abstract Lffb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/Context;

.field final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    iput-object p1, p0, Lffb;->a:Landroid/content/Context;

    iput-object p2, p0, Lffb;->c:Ljava/lang/String;

    iput p3, p0, Lffb;->d:I

    iput-object p4, p0, Lffb;->b:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/server/ClientContext;
    .locals 6

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget v1, p0, Lffb;->d:I

    iget-object v2, p0, Lffb;->b:Ljava/lang/String;

    iget-object v3, p0, Lffb;->b:Ljava/lang/String;

    iget-object v4, p0, Lffb;->c:Ljava/lang/String;

    iget-object v5, p0, Lffb;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public abstract b()V
.end method
