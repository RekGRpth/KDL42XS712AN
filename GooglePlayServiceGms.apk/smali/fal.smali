.class public final Lfal;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lfal;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/Collection;

.field private d:I

.field private e:Z

.field private f:J

.field private g:Ljava/lang/String;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfal;

    invoke-direct {v0}, Lfal;-><init>()V

    sput-object v0, Lfal;->a:Lfal;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x3fff

    iput v0, p0, Lfal;->d:I

    const/4 v0, 0x7

    iput v0, p0, Lfal;->h:I

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lfal;
    .locals 0

    iput-object p1, p0, Lfal;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/util/Collection;)Lfal;
    .locals 0

    iput-object p1, p0, Lfal;->c:Ljava/util/Collection;

    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfal;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lfal;->c:Ljava/util/Collection;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lfal;->d:I

    return v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 3

    new-instance v0, Lfal;

    invoke-direct {v0}, Lfal;-><init>()V

    iget-object v1, p0, Lfal;->b:Ljava/lang/String;

    iput-object v1, v0, Lfal;->b:Ljava/lang/String;

    iget-object v1, p0, Lfal;->c:Ljava/util/Collection;

    iput-object v1, v0, Lfal;->c:Ljava/util/Collection;

    iget v1, p0, Lfal;->d:I

    iput v1, v0, Lfal;->d:I

    iget-boolean v1, p0, Lfal;->e:Z

    iput-boolean v1, v0, Lfal;->e:Z

    iget-wide v1, p0, Lfal;->f:J

    iput-wide v1, v0, Lfal;->f:J

    iget-object v1, p0, Lfal;->g:Ljava/lang/String;

    iput-object v1, v0, Lfal;->g:Ljava/lang/String;

    iget v1, p0, Lfal;->h:I

    iput v1, v0, Lfal;->h:I

    return-object v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lfal;->e:Z

    return v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lfal;->f:J

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfal;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lfal;->h:I

    return v0
.end method
