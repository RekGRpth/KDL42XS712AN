.class public final Laos;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljby;)Laor;
    .locals 4

    const/4 v3, 0x2

    iget-object v0, p0, Ljby;->c:Ljbx;

    iget v0, v0, Ljbx;->b:I

    if-ne v0, v3, :cond_0

    new-instance v0, Laon;

    new-instance v1, Laol;

    invoke-direct {v1, p0}, Laol;-><init>(Ljby;)V

    new-instance v2, Laok;

    invoke-direct {v2}, Laok;-><init>()V

    invoke-direct {v0, v1, v2}, Laon;-><init>(Laor;Laor;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    iget-boolean v1, p0, Ljby;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljby;->c:Ljbx;

    iget-boolean v1, v1, Ljbx;->c:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljby;->c:Ljbx;

    iget-object v1, v1, Ljbx;->d:Ljbn;

    iget-boolean v1, v1, Ljbn;->b:Z

    if-nez v1, :cond_2

    :cond_1
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    new-instance v2, Lbhv;

    invoke-direct {v2, v1}, Lbhv;-><init>(Landroid/content/Context;)V

    const-string v1, "minutemaid_enable"

    invoke-virtual {v2, v1}, Lbhv;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Laot;

    const-string v1, "Request proto is missing usecase data"

    invoke-direct {v0, v1}, Laot;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, p0, Ljby;->c:Ljbx;

    iget-object v1, v1, Ljbx;->d:Ljbn;

    iget v1, v1, Ljbn;->c:I

    if-ne v1, v3, :cond_3

    new-instance v0, Laon;

    new-instance v1, Laoo;

    invoke-direct {v1, p0}, Laoo;-><init>(Ljby;)V

    new-instance v2, Laok;

    invoke-direct {v2}, Laok;-><init>()V

    invoke-direct {v0, v1, v2}, Laon;-><init>(Laor;Laor;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    new-instance v0, Laon;

    new-instance v1, Laoo;

    invoke-direct {v1, p0}, Laoo;-><init>(Ljby;)V

    new-instance v2, Laop;

    invoke-direct {v2}, Laop;-><init>()V

    invoke-direct {v0, v1, v2}, Laon;-><init>(Laor;Laor;)V

    goto :goto_0

    :cond_4
    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    new-instance v0, Laon;

    new-instance v1, Laoo;

    invoke-direct {v1, p0}, Laoo;-><init>(Ljby;)V

    new-instance v2, Laou;

    invoke-direct {v2}, Laou;-><init>()V

    invoke-direct {v0, v1, v2}, Laon;-><init>(Laor;Laor;)V

    goto :goto_0

    :cond_5
    const/4 v2, 0x5

    if-ne v1, v2, :cond_7

    new-instance v1, Laoq;

    invoke-direct {v1}, Laoq;-><init>()V

    new-instance v2, Laoo;

    invoke-direct {v2, p0}, Laoo;-><init>(Ljby;)V

    sget-object v0, Laoh;->b:Ljava/lang/String;

    invoke-interface {v2, v0}, Laor;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Laot;

    const-string v1, "Request proto missing fields required by Reauth usecase."

    invoke-direct {v0, v1}, Laot;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Laon;

    invoke-direct {v0, v2, v1}, Laon;-><init>(Laor;Laor;)V

    goto/16 :goto_0

    :cond_7
    new-instance v1, Laot;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported prompt type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Laot;-><init>(Ljava/lang/String;)V

    throw v1
.end method
