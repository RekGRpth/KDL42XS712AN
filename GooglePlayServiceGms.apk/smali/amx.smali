.class public final enum Lamx;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lamx;

.field public static final enum b:Lamx;

.field private static final synthetic c:[Lamx;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lamx;

    const-string v1, "ACCOUNT_LEVEL"

    invoke-direct {v0, v1, v2}, Lamx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamx;->a:Lamx;

    new-instance v0, Lamx;

    const-string v1, "SCOPE_LEVEL"

    invoke-direct {v0, v1, v3}, Lamx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamx;->b:Lamx;

    const/4 v0, 0x2

    new-array v0, v0, [Lamx;

    sget-object v1, Lamx;->a:Lamx;

    aput-object v1, v0, v2

    sget-object v1, Lamx;->b:Lamx;

    aput-object v1, v0, v3

    sput-object v0, Lamx;->c:[Lamx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lamx;
    .locals 1

    const-class v0, Lamx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lamx;

    return-object v0
.end method

.method public static values()[Lamx;
    .locals 1

    sget-object v0, Lamx;->c:[Lamx;

    invoke-virtual {v0}, [Lamx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamx;

    return-object v0
.end method
