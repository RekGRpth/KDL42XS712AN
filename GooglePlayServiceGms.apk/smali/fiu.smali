.class public final Lfiu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final A:Ljava/lang/String;

.field private static final B:Ljava/lang/String;

.field private static final C:Ljava/lang/String;

.field private static final D:Ljava/lang/String;

.field private static final P:Landroid/net/Uri;

.field private static final Q:Landroid/net/Uri;

.field private static final R:[Ljava/lang/String;

.field private static final S:[Ljava/lang/String;

.field private static final T:[Ljava/lang/String;

.field private static final U:[Ljava/lang/String;

.field private static final V:[Ljava/lang/String;

.field private static final W:[Ljava/lang/String;

.field private static final a:Z

.field private static final aa:[Ljava/lang/String;

.field private static final ac:[Ljava/lang/String;

.field private static final ad:[Ljava/lang/String;

.field private static final ae:[Ljava/lang/String;

.field private static final af:[Ljava/lang/String;

.field private static final ag:[Ljava/lang/String;

.field private static final ah:[Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;

.field private static final m:Ljava/lang/String;

.field private static final n:Ljava/lang/String;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;

.field private static final q:Ljava/lang/String;

.field private static final r:Ljava/lang/String;

.field private static final s:Ljava/lang/String;

.field private static final t:Ljava/lang/String;

.field private static final u:Ljava/lang/String;

.field private static final v:Ljava/lang/String;

.field private static final w:Ljava/lang/String;

.field private static final x:Ljava/lang/String;

.field private static final y:Ljava/lang/String;

.field private static final z:Ljava/lang/String;


# instance fields
.field private final E:Landroid/content/Context;

.field private final F:Landroid/content/ContentResolver;

.field private final G:Lfbn;

.field private final H:Ljava/lang/String;

.field private final I:J

.field private final J:Ljava/lang/String;

.field private final K:Landroid/net/Uri;

.field private final L:Landroid/net/Uri;

.field private final M:Landroid/net/Uri;

.field private final N:Landroid/net/Uri;

.field private final O:Landroid/net/Uri;

.field private final X:Lfjd;

.field private final Y:Lfjd;

.field private final Z:Lfjd;

.field private final ab:Lfji;

.field private final ai:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "PeopleService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lfiu;->a:Z

    const-string v0, "0"

    sput-object v0, Lfiu;->b:Ljava/lang/String;

    const-string v0, "1"

    sput-object v0, Lfiu;->c:Ljava/lang/String;

    const-string v0, "2"

    sput-object v0, Lfiu;->d:Ljava/lang/String;

    const-string v0, "3"

    sput-object v0, Lfiu;->e:Ljava/lang/String;

    const-string v0, "0"

    sput-object v0, Lfiu;->f:Ljava/lang/String;

    const-string v0, "1"

    sput-object v0, Lfiu;->g:Ljava/lang/String;

    const-string v0, "2"

    sput-object v0, Lfiu;->h:Ljava/lang/String;

    const-string v0, "3"

    sput-object v0, Lfiu;->i:Ljava/lang/String;

    const-string v0, "4"

    sput-object v0, Lfiu;->j:Ljava/lang/String;

    const-string v0, "5"

    sput-object v0, Lfiu;->k:Ljava/lang/String;

    const-string v0, "6"

    sput-object v0, Lfiu;->l:Ljava/lang/String;

    const-string v0, "7"

    sput-object v0, Lfiu;->m:Ljava/lang/String;

    const-string v0, "8"

    sput-object v0, Lfiu;->n:Ljava/lang/String;

    const-string v0, "9"

    sput-object v0, Lfiu;->o:Ljava/lang/String;

    const-string v0, "10"

    sput-object v0, Lfiu;->p:Ljava/lang/String;

    const-string v0, "11"

    sput-object v0, Lfiu;->q:Ljava/lang/String;

    const-string v0, "12"

    sput-object v0, Lfiu;->r:Ljava/lang/String;

    const-string v0, "13"

    sput-object v0, Lfiu;->s:Ljava/lang/String;

    const-string v0, "14"

    sput-object v0, Lfiu;->t:Ljava/lang/String;

    const-string v0, "15"

    sput-object v0, Lfiu;->u:Ljava/lang/String;

    const-string v0, "16"

    sput-object v0, Lfiu;->v:Ljava/lang/String;

    const-string v0, "17"

    sput-object v0, Lfiu;->w:Ljava/lang/String;

    const-string v0, "18"

    sput-object v0, Lfiu;->x:Ljava/lang/String;

    const-string v0, "19"

    sput-object v0, Lfiu;->y:Ljava/lang/String;

    const-string v0, "20"

    sput-object v0, Lfiu;->z:Ljava/lang/String;

    const-string v0, "0"

    sput-object v0, Lfiu;->A:Ljava/lang/String;

    const-string v0, "1"

    sput-object v0, Lfiu;->B:Ljava/lang/String;

    const-string v0, "2"

    sput-object v0, Lfiu;->C:Ljava/lang/String;

    const-string v0, "3"

    sput-object v0, Lfiu;->D:Ljava/lang/String;

    invoke-static {}, Lfiu;->e()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfiu;->P:Landroid/net/Uri;

    invoke-static {}, Lfiu;->f()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfiu;->Q:Landroid/net/Uri;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sync1"

    aput-object v1, v0, v4

    const-string v1, "sync2"

    aput-object v1, v0, v5

    sput-object v0, Lfiu;->R:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mimetype"

    aput-object v1, v0, v3

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    const-string v1, "data2"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    sput-object v0, Lfiu;->S:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    sput-object v0, Lfiu;->T:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    const-string v1, "sync1"

    aput-object v1, v0, v5

    const-string v1, "sync3"

    aput-object v1, v0, v6

    sput-object v0, Lfiu;->U:[Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "account_name"

    aput-object v1, v0, v3

    const-string v1, "account_type"

    aput-object v1, v0, v4

    const-string v1, "data_set"

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, "sourceid"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "sync2"

    aput-object v2, v0, v1

    sput-object v0, Lfiu;->V:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "avatar"

    aput-object v1, v0, v3

    sput-object v0, Lfiu;->W:[Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "mimetype"

    aput-object v1, v0, v4

    const-string v1, "data_id"

    aput-object v1, v0, v5

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data3"

    aput-object v2, v0, v1

    sput-object v0, Lfiu;->aa:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lfiu;->ac:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    const-string v1, "data_id"

    aput-object v1, v0, v5

    const-string v1, "mimetype"

    aput-object v1, v0, v6

    const-string v1, "sync3"

    aput-object v1, v0, v7

    sput-object v0, Lfiu;->ad:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    sput-object v0, Lfiu;->ae:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "qualified_id"

    aput-object v1, v0, v3

    const-string v1, "avatar"

    aput-object v1, v0, v4

    sput-object v0, Lfiu;->af:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "raw_contact_id"

    aput-object v1, v0, v3

    sput-object v0, Lfiu;->ag:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "raw_contact_id"

    aput-object v1, v0, v4

    const-string v1, "data5"

    aput-object v1, v0, v5

    const-string v1, "data4"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    sput-object v0, Lfiu;->ah:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfiv;

    invoke-direct {v0, p0}, Lfiv;-><init>(Lfiu;)V

    iput-object v0, p0, Lfiu;->X:Lfjd;

    new-instance v0, Lfiw;

    invoke-direct {v0, p0}, Lfiw;-><init>(Lfiu;)V

    iput-object v0, p0, Lfiu;->Y:Lfjd;

    new-instance v0, Lfix;

    invoke-direct {v0, p0}, Lfix;-><init>(Lfiu;)V

    iput-object v0, p0, Lfiu;->Z:Lfjd;

    new-instance v0, Lfji;

    invoke-direct {v0}, Lfji;-><init>()V

    iput-object v0, p0, Lfiu;->ab:Lfji;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lfiu;->ai:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfiu;->E:Landroid/content/Context;

    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->c()Lfbn;

    move-result-object v0

    iput-object v0, p0, Lfiu;->G:Lfbn;

    iput-object p2, p0, Lfiu;->H:Ljava/lang/String;

    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->e()Lfbk;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lfiu;->I:J

    iget-wide v0, p0, Lfiu;->I:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Lfjc;

    invoke-direct {v0}, Lfjc;-><init>()V

    throw v0

    :cond_0
    iget-wide v0, p0, Lfiu;->I:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfiu;->J:Ljava/lang/String;

    iget-object v0, p0, Lfiu;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfiu;->K:Landroid/net/Uri;

    iget-object v0, p0, Lfiu;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfiu;->L:Landroid/net/Uri;

    iget-object v0, p0, Lfiu;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfiu;->M:Landroid/net/Uri;

    iget-object v0, p0, Lfiu;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfiu;->N:Landroid/net/Uri;

    iget-object v0, p0, Lfiu;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "raw_contacts"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfiu;->O:Landroid/net/Uri;

    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    const/4 v1, 0x1

    if-nez p0, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-boolean v0, Lfiu;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "http"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Avatar URL is compressed"

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    if-eqz v0, :cond_3

    if-ne v0, v1, :cond_0

    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private a(JLjava/lang/String;IZLfia;)Lfjb;
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p6}, Lfia;->c()V

    new-instance v1, Lfjb;

    invoke-direct {v1, v0}, Lfjb;-><init>(B)V

    iget-object v2, p0, Lfiu;->L:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "PeopleService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "PeopleContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "downloadLargeAvatar: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p3, :cond_2

    :try_start_0
    iget-object v3, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v3}, Lfiu;->e(Landroid/content/Context;)I

    move-result v3

    iget-object v4, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v4, p3, v3}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;I)[B

    move-result-object v3

    const-string v4, "display_photo"

    invoke-static {v2, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget v4, v1, Lfjb;->a:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lfjb;->a:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v3, :cond_2

    :try_start_1
    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v0, Lfja;

    const-string v2, "Cannot open stream to write image to Contacts"

    invoke-direct {v0, v2}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v2, Lfja;

    const-string v3, "Cannot upload image to Contacts"

    invoke-direct {v2, v3, v0}, Lfja;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    const-string v2, "PeopleContactsSync"

    const-string v3, "Cannot download avatar: "

    invoke-static {v2, v3, p3, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object v1

    :cond_1
    :try_start_3
    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    const/4 v0, 0x1

    iget v3, v1, Lfjb;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lfjb;->b:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    if-nez v0, :cond_3

    :try_start_5
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "data15"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    if-eqz p5, :cond_4

    invoke-static {}, Lfiu;->f()Landroid/net/Uri;

    move-result-object v0

    :goto_1
    iget-object v3, p0, Lfiu;->F:Landroid/content/ContentResolver;

    const-string v4, "(mimetype =\'vnd.android.cursor.item/photo\') AND raw_contact_id=?"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "sync2"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "sync3"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v3, p0, Lfiu;->F:Landroid/content/ContentResolver;

    if-eqz p5, :cond_5

    iget-object v0, p0, Lfiu;->O:Landroid/net/Uri;

    :goto_2
    const-string v4, "_id=?"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_6
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1

    :cond_4
    :try_start_7
    invoke-static {}, Lfiu;->e()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lfiu;->L:Landroid/net/Uri;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2
.end method

.method static synthetic a(Lfjh;Ljava/lang/String;Ljava/lang/String;)Lfjf;
    .locals 1

    invoke-static {p0, p1, p2}, Lfiu;->b(Lfjh;Ljava/lang/String;Ljava/lang/String;)Lfjf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lfiu;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfiu;->J:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Lfiu;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lfiu;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync not supported."

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync enabled."

    const/4 v2, 0x0

    invoke-static {p0, v0, v2, v1}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    const-string v1, "gplusInstalled"

    invoke-virtual {v0, v1, v3}, Lfbo;->b(Ljava/lang/String;Z)V

    const-string v1, "stopContactsSyncAfterCleanup"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfbo;->b(Ljava/lang/String;Z)V

    const-string v1, "contactStreamsCleanupPending"

    invoke-virtual {v0, v1, v3}, Lfbo;->b(Ljava/lang/String;Z)V

    const-string v1, "babelActionsRewritePending"

    invoke-virtual {v0, v1, v3}, Lfbo;->b(Ljava/lang/String;Z)V

    const-string v1, "contactsCleanupPending"

    invoke-virtual {v0, v1, v3}, Lfbo;->b(Ljava/lang/String;Z)V

    invoke-static {p0}, Lfgy;->a(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v2

    invoke-virtual {v2}, Lfbc;->h()Lfhz;

    move-result-object v2

    invoke-virtual {v2, v0}, Lfhz;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 17

    invoke-static {}, Lfiu;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static/range {p0 .. p0}, Lfiu;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "syncRawContact: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x0

    const-wide/16 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lfiu;->V:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    if-eqz v14, :cond_7

    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x2

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x3

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v1, 0x4

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x5

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_1
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    move-object v8, v2

    move-object v13, v7

    move v7, v1

    move-wide v15, v3

    move-wide v2, v15

    :goto_2
    const-string v1, "com.google"

    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "plus"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v8, :cond_4

    :cond_3
    const-string v1, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot sync raw contact: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_4
    :try_start_1
    new-instance v1, Lfiu;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v13}, Lfiu;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v4, v1, Lfiu;->E:Landroid/content/Context;

    invoke-static {v4}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v4

    invoke-virtual {v4}, Lfbo;->c()Lfbn;

    move-result-object v4

    const-string v5, "people"

    sget-object v6, Lfiu;->W:[Ljava/lang/String;

    const-string v9, "owner_id=? AND qualified_id=?"

    iget-object v10, v1, Lfiu;->J:Ljava/lang/String;

    invoke-static {v10, v8}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v5, v6, v9, v10}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Lfjc; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_5

    const-string v1, "PeopleContactsSync"

    const-string v2, "Cannot find person by qualified ID: "

    const/4 v3, 0x0

    invoke-static {v1, v2, v8, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Lfjc; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const-string v1, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot sync raw contact: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lfdk;->a()Z

    invoke-static {v2, v13}, Lfdk;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_5
    const/4 v4, 0x0

    :try_start_4
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfiu;->a(Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v5

    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Lfjc; {:try_start_5 .. :try_end_5} :catch_0

    if-eq v5, v7, :cond_0

    const/4 v6, 0x0

    :try_start_6
    sget-object v7, Lfia;->a:Lfia;

    invoke-direct/range {v1 .. v7}, Lfiu;->a(JLjava/lang/String;IZLfia;)Lfjb;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lfjc; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    :catch_1
    move-exception v1

    :try_start_7
    const-string v2, "PeopleContactsSync"

    const-string v3, "Cannot download large avatar for person: "

    invoke-static {v2, v3, v8, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_7
    .catch Lfjc; {:try_start_7 .. :try_end_7} :catch_0

    :cond_6
    move v1, v7

    move-object v2, v8

    move-wide v3, v9

    move-object v5, v11

    move-object v6, v12

    move-object v7, v13

    goto/16 :goto_1

    :cond_7
    move-wide v2, v9

    move-object v5, v11

    move-object v6, v12

    goto/16 :goto_2
.end method

.method private static a(Landroid/content/Context;Lfia;)V
    .locals 11

    const/4 v9, 0x0

    const/4 v4, 0x0

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    const-string v1, "  removeContactsForNonSyncAccounts"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lfia;->c()V

    invoke-static {p0}, Lfiu;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const-string v3, "data_set=\'plus\' AND account_type=\'com.google\'"

    if-eqz v6, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND account_name NOT IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v9

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v0, Lfja;

    const-string v1, "Cannot query profiles"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    const-string v3, "_id=?"

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-virtual {p1}, Lfia;->c()V

    invoke-static {p0, v6}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1}, Lfia;->c()V

    invoke-static {p0, v6, p1}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;Lfia;)V

    invoke-virtual {p1}, Lfia;->c()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "PeopleContactsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "CP2 cleanup done, kept="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " duration="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long/2addr v0, v7

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v4, v0}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lfia;Landroid/database/Cursor;)V
    .locals 6

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "stream_items"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x80

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lfia;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    const/4 v1, 0x1

    :try_start_1
    invoke-static {p0, v0, v1}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-string v3, "data_set=\'plus\' AND account_type=\'com.google\'"

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND account_name NOT IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v0, Lfja;

    const-string v1, "Cannot query groups"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-string v5, "_id=?"

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v5, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lfia;)V
    .locals 9

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "data_set=\'plus\' AND account_type=\'com.google\'"

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND account_name NOT IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_0
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v7

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v0, Lfja;

    const-string v1, "Cannot query raw contacts"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v6, v8, [Ljava/lang/String;

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p0, v2, v7}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lfia;->c()V

    goto :goto_1

    :cond_4
    invoke-static {p0, v2, v8}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    return-void
.end method

.method private a(Lfia;Ljava/util/List;Lfev;)V
    .locals 11

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lfjp;->a(Landroid/content/Context;I)I

    move-result v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    move v3, v4

    :goto_0
    if-ge v3, v2, :cond_4

    add-int/lit8 v0, v3, 0x8

    if-le v0, v2, :cond_5

    move v1, v2

    :goto_1
    if-ge v3, v1, :cond_3

    invoke-virtual {p1}, Lfia;->c()V

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfiy;

    iget-object v9, v0, Lfiy;->b:Ljava/lang/String;

    if-nez v9, :cond_1

    const/4 v9, 0x0

    invoke-direct {p0, v8, v0, v9}, Lfiu;->a(Ljava/util/ArrayList;Lfiy;[B)V

    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    iget-boolean v9, v0, Lfiy;->e:Z

    if-nez v9, :cond_0

    iget-object v9, p0, Lfiu;->E:Landroid/content/Context;

    iget-object v10, v0, Lfiy;->b:Ljava/lang/String;

    invoke-static {v9, v10, v7}, Lfiu;->a(Landroid/content/Context;Ljava/lang/String;I)[B

    move-result-object v9

    iget v10, p3, Lfev;->d:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p3, Lfev;->d:I

    if-eqz v9, :cond_2

    iget v10, p3, Lfev;->e:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p3, Lfev;->e:I

    :cond_2
    invoke-direct {p0, v8, v0, v9}, Lfiu;->a(Ljava/util/ArrayList;Lfiy;[B)V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    const/16 v3, 0x8

    invoke-static {v0, v8, v3, v4}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move v3, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v8, v1}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v5

    iput-wide v0, p3, Lfev;->f:J

    return-void

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method private a(Lfje;Lfia;)V
    .locals 14

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    const-string v1, "  computeMeProfileDiffs"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual/range {p2 .. p2}, Lfia;->c()V

    iget-object v1, p0, Lfiu;->O:Landroid/net/Uri;

    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    sget-object v2, Lfiu;->R:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    if-nez v5, :cond_1

    new-instance v0, Lfja;

    const-string v1, "Error reading profile data"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v0, 0x0

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v0, 0x1

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x2

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :cond_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-lez v5, :cond_3

    const-string v5, "PeopleContactsSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "    ME already exists in CP2.  rid="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v5, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v5}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v5

    invoke-virtual {v5}, Lfbo;->c()Lfbn;

    move-result-object v6

    const-string v5, "SELECT display_name,avatar,etag,gaia_id FROM owners WHERE _id=?"

    iget-object v7, p0, Lfiu;->J:Ljava/lang/String;

    invoke-static {v7}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-nez v7, :cond_4

    const-string v0, "PeopleContactsSync"

    const-string v1, "Owner not found.  Account removed?  Stopping sync."

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lfic;

    const-string v1, "[cp2sync] Owner not found"

    invoke-direct {v0, v1}, Lfic;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    const/4 v7, 0x0

    :try_start_1
    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v5, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    const-wide/16 v11, 0x0

    cmp-long v5, v3, v11

    if-nez v5, :cond_6

    const/4 v5, 0x1

    :goto_0
    const-string v11, "PeopleService"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_5

    const-string v11, "PeopleContactsSync"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "    ME: last-etag="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, " new-etag="

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, " lastAvatarFP="

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    if-nez v5, :cond_9

    const-wide/16 v11, 0x0

    cmp-long v2, v0, v11

    if-eqz v2, :cond_9

    const-string v0, "PeopleContactsSync"

    const-string v1, "    ME unchanged."

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :catchall_1
    move-exception v0

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_6
    if-nez v2, :cond_7

    const/4 v5, 0x1

    goto :goto_0

    :cond_7
    invoke-static {v2, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    const/4 v5, 0x1

    goto :goto_0

    :cond_8
    const/4 v5, 0x0

    goto :goto_0

    :cond_9
    new-instance v11, Lfjh;

    invoke-direct {v11}, Lfjh;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v11, Lfjh;->e:Z

    iput-wide v3, v11, Lfjh;->a:J

    iput-object v9, v11, Lfjh;->c:Ljava/lang/String;

    invoke-static {v10}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v11, Lfjh;->b:Ljava/lang/String;

    iput-object v7, v11, Lfjh;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    iput-object v8, v11, Lfjh;->g:Ljava/lang/String;

    iget-object v2, v11, Lfjh;->g:Ljava/lang/String;

    invoke-static {v2}, Lfiu;->a(Ljava/lang/String;)I

    move-result v2

    :cond_a
    int-to-long v7, v2

    cmp-long v2, v7, v0

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    iput-boolean v2, v11, Lfjh;->i:Z

    :cond_b
    const-string v2, "PeopleService"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "PeopleContactsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "    ME: name="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v11, Lfjh;->f:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " avatar="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v11, Lfjh;->g:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " avatarFP="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const-string v0, "SELECT email,type,custom_label FROM owner_emails WHERE owner_id=?"

    iget-object v1, p0, Lfiu;->J:Ljava/lang/String;

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_2
    const-string v0, "PeopleService"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "    Emails found: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v5, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "vnd.android.cursor.item/email_v2"

    invoke-static {v11, v7, v0}, Lfiu;->b(Lfjh;Ljava/lang/String;Ljava/lang/String;)Lfjf;

    move-result-object v0

    invoke-static {v0, v2, v5}, Lfiu;->a(Lfjf;ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    :catchall_2
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_e
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v0, "SELECT phone,type,custom_label FROM owner_phones WHERE owner_id=?"

    iget-object v1, p0, Lfiu;->J:Ljava/lang/String;

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_3
    const-string v0, "PeopleService"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "    Phones found: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v5, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "vnd.android.cursor.item/phone_v2"

    invoke-static {v11, v7, v0}, Lfiu;->b(Lfjh;Ljava/lang/String;Ljava/lang/String;)Lfjf;

    move-result-object v0

    iget-object v7, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v0, v2, v5, v7}, Lfiu;->a(Lfjf;ILjava/lang/String;Landroid/content/Context;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_3

    :catchall_3
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_10
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v0, "SELECT postal_address,type,custom_label FROM owner_postal_address WHERE owner_id=?"

    iget-object v1, p0, Lfiu;->J:Ljava/lang/String;

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_4
    const-string v0, "PeopleService"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "    Addresses found: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v5, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "vnd.android.cursor.item/postal-address_v2"

    invoke-static {v11, v6, v0}, Lfiu;->b(Lfjh;Ljava/lang/String;Ljava/lang/String;)Lfjf;

    move-result-object v0

    invoke-static {v0, v2, v5}, Lfiu;->b(Lfjf;ILjava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto :goto_4

    :catchall_4
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_12
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-wide/16 v0, 0x0

    cmp-long v0, v3, v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lfiu;->ai:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    if-nez v1, :cond_13

    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v5, "com.google"

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v5, "plus"

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    iget-object v5, p0, Lfiu;->H:Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lfiu;->ai:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_13
    sget-object v2, Lfiu;->S:[Ljava/lang/String;

    const-string v3, "mimetype IN (\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\',\'vnd.android.cursor.item/group_membership\')"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_14

    new-instance v0, Lfja;

    const-string v1, "Cannot query profile data"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    :goto_5
    :try_start_5
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x4

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, v11

    invoke-static/range {v0 .. v6}, Lfiu;->a(Lfjh;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    goto :goto_5

    :catchall_5
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_15
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_16
    iput-object v11, p1, Lfje;->g:Lfjh;

    goto/16 :goto_1
.end method

.method static a(Lfjf;ILjava/lang/String;)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    sget-object v0, Lfiu;->b:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    iput-object p2, p0, Lfjf;->e:Ljava/lang/CharSequence;

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lfiu;->c:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lfiu;->d:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static a(Lfjf;ILjava/lang/String;Landroid/content/Context;)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    sget-object v0, Lfiu;->f:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    iput-object p2, p0, Lfjf;->e:Ljava/lang/CharSequence;

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lfiu;->g:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lfiu;->i:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lfiu;->h:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lfiu;->k:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lfiu;->j:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lfiu;->s:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lfiu;->l:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lfiu;->p:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lfiu;->y:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lfiu;->o:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_a
    sget-object v0, Lfiu;->t:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_b
    sget-object v0, Lfiu;->q:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_c
    sget-object v0, Lfiu;->n:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_d
    sget-object v0, Lfiu;->u:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_e
    sget-object v0, Lfiu;->v:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_f
    sget-object v0, Lfiu;->w:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_10
    sget-object v0, Lfiu;->x:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_11
    sget-object v0, Lfiu;->r:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_12
    sget-object v0, Lfiu;->f:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01c3    # com.google.android.gms.R.string.people_google_voice_phone_label

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfjf;->e:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method private static a(Lfjh;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    const-string v1, "PeopleService"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    reconcileData: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object p4, v0

    :cond_1
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object p5, v0

    :cond_2
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object p6, v0

    :cond_3
    iget-object v0, p0, Lfjh;->k:Lfjg;

    invoke-virtual {v0}, Lfjg;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjf;

    iget-object v2, v0, Lfjf;->a:Ljava/lang/String;

    invoke-static {v2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lfjf;->c:Ljava/lang/CharSequence;

    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, v0, Lfjf;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iput-wide p1, v0, Lfjf;->b:J

    iget-object v1, v0, Lfjf;->d:Ljava/lang/CharSequence;

    invoke-static {v1, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lfjf;->e:Ljava/lang/CharSequence;

    invoke-static {v1, p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    :cond_5
    iput-boolean v6, v0, Lfjf;->g:Z

    :cond_6
    :goto_0
    return-void

    :cond_7
    new-instance v0, Lfjf;

    invoke-direct {v0}, Lfjf;-><init>()V

    iput-wide p1, v0, Lfjf;->b:J

    iput-boolean v6, v0, Lfjf;->f:Z

    iget-object v1, p0, Lfjh;->k:Lfjg;

    invoke-virtual {v1, v0}, Lfjg;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Lfji;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lfiu;->L:Landroid/net/Uri;

    sget-object v2, Lfiu;->U:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Lfja;

    const-string v1, "Cannot query raw contacts"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    const-string v0, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PeopleContactsSync"

    const-string v2, "    %d people found in CP2"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lfjh;

    invoke-direct {v0}, Lfjh;-><init>()V

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lfjh;->a:J

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfjh;->b:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfjh;->c:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lfjh;->h:I

    const/4 v2, 0x1

    iput-boolean v2, v0, Lfjh;->d:Z

    iget-object v2, v0, Lfjh;->b:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lfji;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Lfiy;[B)V
    .locals 4

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "buildThumbnailContentProviderOperations: rid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p2, Lfiy;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " url="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lfiy;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p2, Lfiy;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " image="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfiu;->L:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "_id=?"

    iget-wide v2, p2, Lfiy;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "sync3"

    iget v2, p2, Lfiy;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "sync2"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v0, p2, Lfiy;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    sget-object v0, Lfiu;->P:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "_id=?"

    iget-wide v2, p2, Lfiy;->d:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data15"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p3, :cond_1

    sget-object v0, Lfiu;->P:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    iget-wide v2, p2, Lfiy;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/photo"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data15"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Lfjh;ZZ)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p2, Lfjh;->k:Lfjg;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-boolean v0, p2, Lfjh;->e:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfiu;->O:Landroid/net/Uri;

    :goto_2
    iget-boolean v3, p2, Lfjh;->e:Z

    if-eqz v3, :cond_7

    sget-object v3, Lfiu;->Q:Landroid/net/Uri;

    :goto_3
    iget-object v4, p2, Lfjh;->b:Ljava/lang/String;

    invoke-static {v4}, Lfdl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p2, Lfjh;->f:Ljava/lang/String;

    if-nez v5, :cond_0

    const-string v5, ""

    iput-object v5, p2, Lfjh;->f:Ljava/lang/String;

    :cond_0
    if-eqz p3, :cond_9

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "sourceid"

    iget-object v6, p2, Lfjh;->b:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "sync1"

    iget-object v6, p2, Lfjh;->c:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_is_read_only"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v5, "vnd.android.cursor.item/name"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data1"

    iget-object v5, p2, Lfjh;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p2, Lfjh;->e:Z

    if-nez v0, :cond_2

    if-eqz v4, :cond_1

    if-eqz p4, :cond_8

    const-string v0, "vnd.android.cursor.item/vnd.googleplus.profile.comm"

    :goto_4
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "raw_contact_id"

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "mimetype"

    invoke-virtual {v1, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "data4"

    const/16 v6, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "data5"

    const-string v6, "conversation"

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "data3"

    iget-object v6, p0, Lfiu;->E:Landroid/content/Context;

    const v7, 0x7f0b01ae    # com.google.android.gms.R.string.people_start_conversation_action_label

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "raw_contact_id"

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "mimetype"

    invoke-virtual {v1, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data4"

    const/16 v5, 0xe

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data5"

    const-string v5, "hangout"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data3"

    iget-object v5, p0, Lfiu;->E:Landroid/content/Context;

    const v6, 0x7f0b01af    # com.google.android.gms.R.string.people_start_hangout_action_label

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v5, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data4"

    const/16 v5, 0x14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data5"

    const-string v5, "addtocircle"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data3"

    iget-object v5, p0, Lfiu;->E:Landroid/content/Context;

    const v6, 0x7f0b01b0    # com.google.android.gms.R.string.people_add_to_circle_action_label

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v5, "vnd.android.cursor.item/identity"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data2"

    const-string v5, "com.google"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data1"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "gprofile:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v4, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data4"

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data5"

    const-string v4, "view"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data3"

    iget-object v4, p0, Lfiu;->E:Landroid/content/Context;

    const v5, 0x7f0b01ad    # com.google.android.gms.R.string.people_view_profile_action_label

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_5
    iget-object v0, p2, Lfjh;->k:Lfjg;

    invoke-virtual {v0}, Lfjg;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjf;

    iget-boolean v1, v0, Lfjf;->f:Z

    if-eqz v1, :cond_c

    iget-wide v0, v0, Lfjf;->b:J

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    :goto_7
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lfiu;->L:Landroid/net/Uri;

    goto/16 :goto_2

    :cond_7
    sget-object v3, Lfiu;->P:Landroid/net/Uri;

    goto/16 :goto_3

    :cond_8
    const-string v0, "vnd.android.cursor.item/vnd.googleplus.profile"

    goto/16 :goto_4

    :cond_9
    iget-wide v4, p2, Lfjh;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "_id=?"

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "sync1"

    iget-object v5, p2, Lfjh;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    iget-boolean v1, p2, Lfjh;->i:Z

    if-eqz v1, :cond_a

    const-string v1, "sync2"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "sync3"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_a
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p2, Lfjh;->i:Z

    if-eqz v0, :cond_b

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype=\'vnd.android.cursor.item/photo\' AND raw_contact_id=?"

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype=\'vnd.android.cursor.item/name\' AND raw_contact_id=? AND data1!=?"

    iget-object v5, p2, Lfjh;->f:Ljava/lang/String;

    invoke-static {v4, v5}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data1"

    iget-object v4, p2, Lfjh;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_c
    iget-wide v5, v0, Lfjf;->b:J

    const-wide/16 v7, 0x0

    cmp-long v1, v5, v7

    if-nez v1, :cond_e

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "mimetype"

    iget-object v6, v0, Lfjf;->a:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    if-eqz p3, :cond_d

    const-string v5, "raw_contact_id"

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    :goto_8
    const-string v5, "data1"

    iget-object v6, v0, Lfjf;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v5, "data2"

    iget-object v6, v0, Lfjf;->d:Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v5, "data3"

    iget-object v0, v0, Lfjf;->e:Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    goto/16 :goto_7

    :cond_d
    const-string v5, "raw_contact_id"

    iget-wide v6, p2, Lfjh;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_8

    :cond_e
    iget-boolean v1, v0, Lfjf;->g:Z

    if-eqz v1, :cond_3

    sget-object v1, Lfiu;->P:Landroid/net/Uri;

    iget-wide v5, v0, Lfjf;->b:J

    invoke-static {v1, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    goto :goto_8

    :cond_f
    const/4 v0, 0x0

    iput-object v0, p2, Lfjh;->k:Lfjg;

    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/List;JZZLfia;)V
    .locals 14

    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PeopleContactsSync"

    const-string v3, "  updateContacts"

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    const/4 v9, 0x0

    move v3, v9

    :goto_0
    if-ge v3, v10, :cond_9

    invoke-virtual/range {p7 .. p7}, Lfia;->c()V

    add-int/lit8 v2, v3, 0x20

    if-le v2, v10, :cond_a

    move v9, v10

    :goto_1
    move-object/from16 v0, p2

    invoke-interface {v0, v3, v9}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v11

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfjh;

    iget-boolean v6, v2, Lfjh;->j:Z

    if-eqz v6, :cond_1

    const-string v6, "vnd.android.cursor.item/group_membership"

    invoke-static {v2, v6, v4}, Lfiu;->b(Lfjh;Ljava/lang/String;Ljava/lang/String;)Lfjf;

    :cond_1
    iget-object v6, p0, Lfiu;->X:Lfjd;

    invoke-virtual {v6, v2}, Lfjd;->a(Lfjh;)V

    iget-object v6, p0, Lfiu;->Y:Lfjd;

    invoke-virtual {v6, v2}, Lfjd;->a(Lfjh;)V

    iget-object v6, p0, Lfiu;->Z:Lfjd;

    invoke-virtual {v6, v2}, Lfjd;->a(Lfjh;)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_2
    if-nez p5, :cond_8

    iget-object v3, p0, Lfiu;->N:Landroid/net/Uri;

    const-string v2, "PeopleService"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "PeopleContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  reconcileContacts: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lfiu;->ab:Lfji;

    invoke-virtual {v2}, Lfji;->clear()V

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v5

    new-array v6, v5, [Ljava/lang/String;

    invoke-static {}, Lfdl;->b()Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v2, "_id IN ("

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    move v4, v2

    :goto_3
    if-ge v4, v5, :cond_5

    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfjh;

    iget-object v8, p0, Lfiu;->ab:Lfji;

    iget-object v12, v2, Lfjh;->b:Ljava/lang/String;

    invoke-virtual {v8, v12, v2}, Lfji;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v12, v2, Lfjh;->a:J

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v4

    if-eqz v4, :cond_4

    const/16 v2, 0x2c

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    const/16 v2, 0x3f

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    :cond_5
    const-string v2, ") AND mimetype IN (\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\',\'vnd.android.cursor.item/group_membership\')"

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lfiu;->F:Landroid/content/ContentResolver;

    sget-object v4, Lfiu;->aa:[Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-nez v12, :cond_6

    new-instance v2, Lfja;

    const-string v3, "Cannot query raw contact entities"

    invoke-direct {v2, v3}, Lfja;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    :goto_4
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfiu;->ab:Lfji;

    invoke-virtual {v3, v2}, Lfji;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfjh;

    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v5, 0x1

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    invoke-interface {v12, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    invoke-interface {v12, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x5

    invoke-interface {v12, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v2 .. v8}, Lfiu;->a(Lfjh;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    iget-object v3, p0, Lfiu;->ab:Lfji;

    invoke-virtual {v3}, Lfji;->clear()V

    throw v2

    :cond_7
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    iget-object v2, p0, Lfiu;->ab:Lfji;

    invoke-virtual {v2}, Lfji;->clear()V

    :cond_8
    move/from16 v0, p5

    move/from16 v1, p6

    invoke-direct {p0, p1, v11, v0, v1}, Lfiu;->a(Ljava/util/ArrayList;Ljava/util/List;ZZ)V

    iget-object v2, p0, Lfiu;->E:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, p1, v3}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move v3, v9

    goto/16 :goto_0

    :cond_9
    return-void

    :cond_a
    move v9, v2

    goto/16 :goto_1
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/List;Lfia;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    const-string v0, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    const-string v2, "  deleteDeletedContacts"

    invoke-static {v0, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p3}, Lfia;->c()V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lfiu;->L:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    new-array v5, v7, [Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v0, p1, v1}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/List;ZZ)V
    .locals 3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjh;

    invoke-direct {p0, p1, v0, p3, p4}, Lfiu;->a(Ljava/util/ArrayList;Lfjh;ZZ)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Ljava/util/HashSet;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    sget-object v2, Lfiu;->ag:[Ljava/lang/String;

    const/4 v4, 0x0

    move-object v1, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Lfia;)V
    .locals 8

    const/4 v7, 0x0

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    const-string v1, "  deleteExtraneousGroups"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Lfia;->c()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v3, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v4, p0, Lfiu;->K:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;I)[B
    .locals 3

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    new-instance v0, Lbki;

    invoke-direct {v0, p1}, Lbki;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lbki;->d:Z

    invoke-virtual {v0, p2}, Lbki;->a(I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lfft;->a(Landroid/content/Context;)Lfft;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lfft;->b(Ljava/lang/String;Z)[B

    move-result-object v0

    sget-object v1, Lffl;->a:[B

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;
    .locals 7

    const/4 v1, 0x0

    const/4 v6, 0x3

    const-string v0, "PeopleService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "flushBatch: force="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " minsize="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "PeopleService"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    const-string v3, "PeopleContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    if-nez p3, :cond_2

    if-ge v0, p2, :cond_2

    move-object v0, v1

    goto :goto_1

    :cond_2
    const-string v0, "PeopleService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PeopleContactsSync"

    const-string v1, "  applying batch"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.android.contacts"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lfja;

    const-string v2, "Error appying batch of CPOs"

    invoke-direct {v1, v2, v0}, Lfja;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;
    .locals 1

    const/16 v0, 0x80

    invoke-static {p0, p1, v0, p2}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lfiu;)Lfbn;
    .locals 1

    iget-object v0, p0, Lfiu;->G:Lfbn;

    return-object v0
.end method

.method private static b(Lfjh;Ljava/lang/String;Ljava/lang/String;)Lfjf;
    .locals 2

    new-instance v0, Lfjf;

    invoke-direct {v0}, Lfjf;-><init>()V

    iput-object p1, v0, Lfjf;->a:Ljava/lang/String;

    iput-object p2, v0, Lfjf;->c:Ljava/lang/CharSequence;

    iget-object v1, p0, Lfjh;->k:Lfjg;

    invoke-virtual {v1, v0}, Lfjg;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {}, Lfiu;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lfiu;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    const-string v3, "triggerPendingContactsCleanup"

    invoke-static {v0, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lfgy;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v0, v3

    if-nez v0, :cond_2

    const-string v0, "PeopleContactsSync"

    const-string v1, "triggerPendingContactsCleanup: no accounts"

    invoke-static {v0, v1}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    const-string v4, "contactStreamsCleanupPending"

    invoke-virtual {v0, v4, v2}, Lfbo;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "contactsCleanupPending"

    invoke-virtual {v0, v4, v2}, Lfbo;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "babelActionsRewritePending"

    invoke-virtual {v0, v4, v2}, Lfbo;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    const-string v0, "PeopleContactsSync"

    const-string v4, "  Starting sync..."

    invoke-static {v0, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    aget-object v1, v3, v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v3, "PeopleSync"

    const-string v4, "Contacts cleanup sync requested"

    const/4 v5, 0x0

    invoke-static {v3, v4, v1, v5}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v2, v2}, Lfhz;->a(ZZ)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "gms.people.contacts_sync"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "gms.people.skip_main_sync"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, v0, Lfhz;->a:Landroid/content/Context;

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->e()Lfbk;

    move-result-object v2

    invoke-virtual {v2, v1}, Lfbk;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "com.google.android.gms.people"

    invoke-virtual {v0, v1, v2, v3}, Lfhz;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    const-string v0, "PeopleContactsSync"

    const-string v1, "  No pending cleanups."

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private b(Lfia;Ljava/util/List;Lfev;)V
    .locals 10

    const-string v0, "PeopleContactsSync"

    const-string v1, "    Syncing large avatars..."

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfiy;

    iget-boolean v1, v0, Lfiy;->e:Z

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lfiy;->a:J

    iget-object v3, v0, Lfiy;->b:Ljava/lang/String;

    iget v4, v0, Lfiy;->c:I

    const/4 v5, 0x0

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lfiu;->a(JLjava/lang/String;IZLfia;)Lfjb;

    move-result-object v0

    iget v1, p3, Lfev;->b:I

    iget v2, v0, Lfjb;->a:I

    add-int/2addr v1, v2

    iput v1, p3, Lfev;->b:I

    iget v1, p3, Lfev;->c:I

    iget v0, v0, Lfjb;->b:I

    add-int/2addr v0, v1

    iput v0, p3, Lfev;->c:I

    invoke-virtual {p1}, Lfia;->c()V

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v7

    iput-wide v0, p3, Lfev;->a:J

    return-void
.end method

.method private b(Lfje;Lfia;)V
    .locals 6

    const/4 v3, 0x0

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    const-string v1, "  computeGroupDiffs"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Lfia;->c()V

    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lfiu;->K:Landroid/net/Uri;

    sget-object v2, Lfiu;->T:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Lfja;

    const-string v1, "Cannot query groups"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v4, "plus"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-wide v2, p1, Lfje;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p1, Lfje;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lfje;->c:Ljava/util/ArrayList;

    :cond_3
    iget-object v0, p1, Lfje;->c:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method static b(Lfjf;ILjava/lang/String;)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    sget-object v0, Lfiu;->A:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    iput-object p2, p0, Lfjf;->e:Ljava/lang/CharSequence;

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lfiu;->B:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lfiu;->C:Ljava/lang/String;

    iput-object v0, p0, Lfjf;->d:Ljava/lang/CharSequence;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Lfji;)V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lfiu;->G:Lfbn;

    const-string v4, "SELECT qualified_id,etag,name,in_circle,avatar FROM people P WHERE owner_id=?1 AND name IS NOT NULL AND profile_type=1 AND (( EXISTS (SELECT 1 FROM owners WHERE _id=?1 AND sync_circles_to_contacts=1) AND qualified_id IN (SELECT qualified_id FROM circle_members WHERE owner_id=?1 AND circle_id IN (SELECT circle_id FROM circles WHERE owner_id=?1 AND type=-1 AND for_sharing=1)))OR( EXISTS (SELECT 1 FROM owners WHERE _id=?1 AND sync_evergreen_to_contacts=1) AND in_contacts=1 AND (?2 OR EXISTS (SELECT 1 FROM gaia_id_map G  WHERE G.owner_id=P.owner_id AND G.type!=2 AND G.gaia_id=P.gaia_id))))"

    iget-object v5, p0, Lfiu;->J:Ljava/lang/String;

    sget-object v0, Lfbd;->W:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-static {v5, v0}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    :try_start_0
    const-string v0, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    const-string v3, "    %d people found in db"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x2

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x3

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v4, v1

    :goto_2
    const/4 v0, 0x4

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v3}, Lfji;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjh;

    if-nez v0, :cond_3

    new-instance v0, Lfjh;

    invoke-direct {v0}, Lfjh;-><init>()V

    iput-object v3, v0, Lfjh;->b:Ljava/lang/String;

    iget-object v3, v0, Lfjh;->b:Ljava/lang/String;

    invoke-virtual {p1, v3, v0}, Lfji;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v0

    :goto_3
    iput-object v6, v3, Lfjh;->c:Ljava/lang/String;

    iput-object v7, v3, Lfjh;->f:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, v3, Lfjh;->d:Z

    iget v0, v3, Lfjh;->h:I

    invoke-static {v8}, Lfiu;->a(Ljava/lang/String;)I

    move-result v6

    if-eq v0, v6, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, v3, Lfjh;->i:Z

    iput-boolean v4, v3, Lfjh;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    const-string v0, "0"

    goto/16 :goto_0

    :cond_2
    move v4, v2

    goto :goto_2

    :cond_3
    :try_start_1
    iget-object v9, v0, Lfjh;->c:Ljava/lang/String;

    invoke-static {v9, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-virtual {p1, v3}, Lfji;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    return-void

    :cond_6
    move-object v3, v0

    goto :goto_3
.end method

.method private static b()Z
    .locals 2

    sget-object v0, Lfbd;->ab:Lbfy;

    invoke-static {v0}, Lbhv;->a(Lbfy;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync disabled by gservices."

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lfia;)Z
    .locals 13

    const/4 v12, 0x1

    const/4 v4, 0x0

    const/4 v11, 0x0

    invoke-virtual {p1}, Lfia;->c()V

    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lfiu;->L:Landroid/net/Uri;

    sget-object v2, Lfiu;->ac:[Ljava/lang/String;

    const-string v3, "sync3=0 OR sync3 IS NULL"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lfja;

    const-string v1, "Cannot obtain count of raw contacts requiring avatars"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move v11, v12

    :cond_1
    :goto_0
    return v11

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    invoke-direct {p0}, Lfiu;->d()[Ljava/lang/String;

    move-result-object v9

    array-length v0, v9

    if-eqz v0, :cond_1

    invoke-static {}, Lfdl;->b()Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "_id IN ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v11

    :goto_1
    array-length v2, v9

    if-ge v0, v2, :cond_4

    if-eqz v0, :cond_3

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    const/16 v2, 0x3f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_4
    const-string v0, ") AND (sync2=0 OR sync2 IS NULL)"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v6, p0, Lfiu;->L:Landroid/net/Uri;

    sget-object v7, Lfiu;->ac:[Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Lfja;

    const-string v1, "Cannot obtain count of raw contacts requiring large avatars"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-lez v1, :cond_6

    move v11, v12

    :cond_6
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_1
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method static synthetic c(Lfiu;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    return-object v0
.end method

.method private c()Lfiz;
    .locals 10

    new-instance v9, Lfiz;

    invoke-direct {v9}, Lfiz;-><init>()V

    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lfiu;->N:Landroid/net/Uri;

    sget-object v2, Lfiu;->ad:[Ljava/lang/String;

    const-string v3, "(mimetype=\'vnd.android.cursor.item/photo\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\')AND(sync3=0 OR sync3 IS NULL)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Lfja;

    const-string v1, "Cannot obtain a list of missing thumbnails"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lfiz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfiy;

    if-nez v0, :cond_1

    new-instance v0, Lfiy;

    invoke-direct {v0}, Lfiy;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v0, Lfiy;->a:J

    const/4 v3, 0x4

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lfiy;->c:I

    invoke-virtual {v9, v2, v0}, Lfiz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "vnd.android.cursor.item/photo"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lfiy;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    invoke-direct {p0}, Lfiu;->d()[Ljava/lang/String;

    move-result-object v4

    array-length v0, v4

    if-eqz v0, :cond_8

    invoke-static {}, Lfdl;->b()Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v0, "_id IN ("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :goto_1
    array-length v1, v4

    if-ge v0, v1, :cond_4

    if-eqz v0, :cond_3

    const/16 v1, 0x2c

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    const/16 v1, 0x3f

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const-string v0, ") AND (sync2=0 OR sync2 IS NULL)"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lfiu;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lfiu;->L:Landroid/net/Uri;

    sget-object v2, Lfiu;->ae:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_5

    new-instance v0, Lfja;

    const-string v1, "Cannot obtain raw contacts w/o large avatars"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lfiz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfiy;

    if-nez v0, :cond_6

    new-instance v0, Lfiy;

    invoke-direct {v0}, Lfiy;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v0, Lfiy;->a:J

    invoke-virtual {v9, v2, v0}, Lfiz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    const/4 v2, 0x1

    iput-boolean v2, v0, Lfiy;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->c()Lfbn;

    move-result-object v0

    const-string v1, "people"

    sget-object v2, Lfiu;->af:[Ljava/lang/String;

    const-string v3, "owner_id=?"

    iget-object v4, p0, Lfiu;->J:Ljava/lang/String;

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :cond_9
    :goto_3
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lfiu;->a(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v9, v2}, Lfiz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfiy;

    if-eqz v0, :cond_9

    iget v5, v0, Lfiy;->c:I

    if-ne v5, v4, :cond_a

    invoke-virtual {v9, v2}, Lfiz;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_a
    :try_start_3
    iput v4, v0, Lfiy;->c:I

    iput-object v3, v0, Lfiy;->b:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_3

    :cond_b
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "PeopleContactsSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    Avatar state map: size="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lfiz;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Lfiz;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "PeopleContactsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "      "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_c
    return-object v9
.end method

.method public static c(Landroid/content/Context;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "PeopleContactsSync"

    const-string v3, "onPackageChanged"

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lfiu;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lfiu;->d(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync disabled"

    invoke-static {v0, v1}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->d()Lfbn;

    move-result-object v3

    invoke-virtual {v3}, Lfbn;->b()V

    :try_start_0
    const-string v4, "babelInstalled"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lfbo;->a(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v5

    invoke-virtual {v5}, Lfbc;->o()Lfjq;

    move-result-object v5

    invoke-virtual {v5}, Lfjq;->a()Z

    move-result v5

    if-eq v4, v5, :cond_2

    const-string v4, "PeopleContactsSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Hangout changed: installed="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {p0, v4, v7, v6}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "babelInstalled"

    invoke-virtual {v2, v4, v5}, Lfbo;->b(Ljava/lang/String;Z)V

    const-string v4, "babelActionsRewritePending"

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lfbo;->b(Ljava/lang/String;Z)V

    :cond_2
    invoke-virtual {v3}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Lfbn;->e()V

    invoke-static {p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->d()Lfbn;

    move-result-object v3

    invoke-virtual {v3}, Lfbn;->b()V

    :try_start_1
    const-string v4, "gplusInstalled"

    invoke-virtual {v2, v4}, Lfbo;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v4

    invoke-virtual {v4}, Lfbc;->o()Lfjq;

    move-result-object v4

    iget-object v4, v4, Lfjq;->a:Landroid/content/Context;

    const-string v5, "com.google.android.apps.plus"

    const-string v6, "gcore_cp2_sync_supported"

    invoke-static {v4, v5, v6}, Lfjq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    move v0, v1

    :goto_1
    :pswitch_0
    if-nez v0, :cond_3

    const-string v0, "PeopleContactsSync"

    const-string v1, "G+ app uninstalled."

    const/4 v4, 0x0

    invoke-static {p0, v0, v4, v1}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lfbo;->p()Z

    const-string v0, "contactsCleanupPending"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Lfbo;->b(Ljava/lang/String;Z)V

    const-string v0, "stopContactsSyncAfterCleanup"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Lfbo;->b(Ljava/lang/String;Z)V

    const-string v0, "gplusInstalled"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Lfbo;->b(Ljava/lang/String;Z)V

    :cond_3
    invoke-virtual {v3}, Lfbn;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v3}, Lfbn;->e()V

    invoke-static {p0}, Lfiu;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lfbn;->e()V

    throw v0

    :pswitch_1
    :try_start_2
    sget-object v0, Lfbd;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    goto :goto_1

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lfbn;->e()V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static d(Landroid/content/Context;)Z
    .locals 1

    invoke-static {}, Lfiu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v0

    invoke-virtual {v0}, Lfbe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()[Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lfiu;->M:Landroid/net/Uri;

    const-string v2, "starred!=0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lfiu;->a(Ljava/util/HashSet;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lfiu;->M:Landroid/net/Uri;

    const-string v2, "starred=0 AND times_contacted>0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const-string v3, "times_contacted DESC LIMIT 8"

    invoke-direct {p0, v0, v1, v2, v3}, Lfiu;->a(Ljava/util/HashSet;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lfiu;->M:Landroid/net/Uri;

    const-string v2, "starred=0 AND times_contacted>0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const-string v3, "last_time_contacted DESC LIMIT 8"

    invoke-direct {p0, v0, v1, v2, v3}, Lfiu;->a(Ljava/util/HashSet;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lfdl;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private static e(Landroid/content/Context;)I
    .locals 7

    const/4 v6, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$DisplayPhoto;->CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "display_max_dim"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_1
    if-ne v0, v6, :cond_0

    new-instance v0, Lfja;

    const-string v1, "Cannot obtain avatar dimension"

    invoke-direct {v0, v1}, Lfja;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    return v0

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1
.end method

.method private static e()Landroid/net/Uri;
    .locals 3

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static f()Landroid/net/Uri;
    .locals 3

    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "profile"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->c()Lfbn;

    move-result-object v0

    const-string v1, "SELECT account_name FROM owners WHERE page_gaia_id IS NULL AND (sync_to_contacts!=0) ORDER BY _id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lfdl;->b()Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v0, ""

    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    const-string v0, ","

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Lfia;)Lfeu;
    .locals 18

    invoke-static {}, Lfiu;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v2}, Lfiu;->d(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    const-string v3, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v4, v0, Lfiu;->H:Ljava/lang/String;

    const-string v5, "CP2 sync start"

    invoke-static {v2, v3, v4, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v11

    const-string v2, "PeopleContactsSync"

    const-string v3, "  performPendingContactsCleanup"

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v9

    const-string v2, "contactStreamsCleanupPending"

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Lfbo;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v8, v0, Lfiu;->E:Landroid/content/Context;

    const-string v2, "PeopleContactsSync"

    const-string v3, "removeAllStreamItemsFromContacts"

    invoke-static {v2, v3}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "stream_items"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "account_name"

    aput-object v6, v4, v5

    const-string v5, "account_type=\'com.google\' AND data_set=\'plus\'"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v2, Lfja;

    const-string v3, "Cannot select stream items"

    invoke-direct {v2, v3}, Lfja;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object/from16 v0, p1

    invoke-static {v8, v0, v2}, Lfiu;->a(Landroid/content/Context;Lfia;Landroid/database/Cursor;)V

    const-string v2, "PeopleContactsSync"

    const-string v3, "removeAllStreamItemsFromMeProfile"

    invoke-static {v2, v3}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Profile;->CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const-string v5, "account_type=\'com.google\' AND data_set=\'plus\'"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-nez v10, :cond_4

    new-instance v2, Lfja;

    const-string v3, "Cannot select profile raw contacts"

    invoke-direct {v2, v3}, Lfja;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v8, v0, v2}, Lfiu;->a(Landroid/content/Context;Lfia;Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "stream_items"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "account_name"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v2, Lfja;

    const-string v3, "Cannot select stream items"

    invoke-direct {v2, v3}, Lfja;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2

    :catchall_1
    move-exception v3

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_5
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const-string v2, "contactStreamsCleanupPending"

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Lfbo;->b(Ljava/lang/String;Z)V

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    :cond_6
    const-string v2, "contactsCleanupPending"

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Lfbo;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lfiu;->a(Landroid/content/Context;Lfia;)V

    const-string v2, "contactsCleanupPending"

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Lfbo;->b(Ljava/lang/String;Z)V

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    :cond_7
    const-string v2, "babelActionsRewritePending"

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Lfbo;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v10, v0, Lfiu;->E:Landroid/content/Context;

    const-string v2, "PeopleContactsSync"

    const-string v3, "  updateBabelActionMimeType"

    invoke-static {v2, v3}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v10}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v2

    invoke-virtual {v2}, Lfbc;->o()Lfjq;

    move-result-object v2

    invoke-virtual {v2}, Lfjq;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "vnd.android.cursor.item/vnd.googleplus.profile.comm"

    move-object v8, v2

    :goto_1
    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "PeopleContactsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  new mimetype="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lfiu;->P:Landroid/net/Uri;

    sget-object v4, Lfiu;->ah:[Ljava/lang/String;

    const-string v5, "account_type=\'com.google\' AND data_set=\'plus\' AND mimetype IN (\'vnd.android.cursor.item/vnd.googleplus.profile\',\'vnd.android.cursor.item/vnd.googleplus.profile.comm\') AND data5 IN (\'conversation\',\'hangout\')"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    if-nez v3, :cond_a

    new-instance v2, Lfja;

    const-string v3, "Cannot obtain babel actions"

    invoke-direct {v2, v3}, Lfja;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_9
    const-string v2, "vnd.android.cursor.item/vnd.googleplus.profile"

    move-object v8, v2

    goto :goto_1

    :cond_a
    :goto_2
    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v2, 0x1

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v2, 0x2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v14, 0x3

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x4

    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lfiu;->P:Landroid/net/Uri;

    invoke-static/range {v16 .. v16}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v16

    const-string v17, "_id=?"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v4, Lfiu;->P:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "raw_contact_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "mimetype"

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data5"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "data4"

    invoke-virtual {v2, v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v4, "data3"

    invoke-virtual {v2, v4, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v2, 0x80

    const/4 v4, 0x0

    invoke-static {v10, v13, v2, v4}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catchall_2
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_b
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    const/4 v2, 0x1

    invoke-static {v10, v13, v2}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    const-string v2, "PeopleContactsSync"

    const-string v3, "Updated babel action mime type"

    invoke-static {v2, v3}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "babelActionsRewritePending"

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Lfbo;->b(Ljava/lang/String;Z)V

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    const-string v3, "stopContactsSyncAfterCleanup"

    invoke-virtual {v2, v3}, Lfbo;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v2}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v2

    invoke-virtual {v2}, Lfbc;->a()Lfbe;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lfbe;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    const-string v3, "PeopleContactsSync"

    const-string v4, "CP2 sync disabled.  (reverse-handover)"

    const/4 v5, 0x0

    invoke-static {v2, v3, v5, v4}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_d
    new-instance v10, Lfeu;

    invoke-direct {v10}, Lfeu;-><init>()V

    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "PeopleContactsSync"

    const-string v3, "computePeopleSyncDiffs"

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    new-instance v13, Lfje;

    invoke-direct {v13}, Lfje;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->c()Lfbn;

    move-result-object v2

    const-string v3, "SELECT sync_to_contacts FROM owners WHERE _id=?"

    move-object/from16 v0, p0

    iget-object v4, v0, Lfiu;->J:Ljava/lang/String;

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_14

    const/4 v2, 0x1

    :goto_3
    iput-boolean v2, v13, Lfje;->a:Z

    iget-boolean v2, v13, Lfje;->a:Z

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    iput-boolean v2, v10, Lfeu;->d:Z

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v13, v1}, Lfiu;->a(Lfje;Lfia;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v13, v1}, Lfiu;->b(Lfje;Lfia;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-string v4, "PeopleService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_f

    const-string v4, "PeopleContactsSync"

    const-string v5, "  computeRawContactDiffs"

    invoke-static {v4, v5}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    new-instance v4, Lfji;

    invoke-direct {v4}, Lfji;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lfiu;->a(Lfji;)V

    invoke-virtual {v4}, Lfji;->size()I

    move-result v5

    iput v5, v10, Lfeu;->e:I

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lfiu;->b(Lfji;)V

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    invoke-virtual {v4, v13}, Lfji;->a(Lfje;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    iput-wide v2, v10, Lfeu;->b:J

    iget-object v2, v13, Lfje;->d:Ljava/util/ArrayList;

    if-nez v2, :cond_10

    iget-object v2, v13, Lfje;->e:Ljava/util/ArrayList;

    if-eqz v2, :cond_15

    :cond_10
    const-string v2, "PeopleContactsSync"

    const-string v3, "  People added or updated.  Need to update avatars."

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    iput-boolean v2, v13, Lfje;->h:Z

    :cond_11
    :goto_4
    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_12

    const-string v2, "PeopleContactsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "computePeopleSyncDiffs done: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lfje;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    const-string v3, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v4, v0, Lfiu;->H:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CP2 sync: diff="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lfje;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    const-string v2, "PeopleContactsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Syncing people to contacts: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lfje;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lfiu;->H:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-boolean v2, v13, Lfje;->a:Z

    if-nez v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lfiu;->a(Landroid/content/Context;Lfia;)V

    :cond_13
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    const-string v3, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v4, v0, Lfiu;->H:Ljava/lang/String;

    const-string v5, "CP2 sync finished"

    invoke-static {v2, v3, v4, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v11

    iput-wide v2, v10, Lfeu;->a:J

    move-object v2, v10

    goto/16 :goto_0

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_15
    invoke-direct/range {p0 .. p1}, Lfiu;->b(Lfia;)Z

    move-result v2

    if-eqz v2, :cond_16

    const-string v3, "PeopleContactsSync"

    const-string v4, "  Finishing incomplete avatar sync."

    invoke-static {v3, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_6
    iput-boolean v2, v13, Lfje;->h:Z

    goto/16 :goto_4

    :cond_16
    const-string v3, "PeopleContactsSync"

    const-string v4, "  No need for avatar sync."

    invoke-static {v3, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_17
    invoke-virtual {v13}, Lfje;->a()Z

    move-result v2

    if-nez v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v2}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v2

    invoke-virtual {v2}, Lfbc;->o()Lfjq;

    move-result-object v2

    invoke-virtual {v2}, Lfjq;->a()Z

    move-result v14

    iget-object v6, v13, Lfje;->g:Lfjh;

    if-eqz v6, :cond_1a

    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_18

    const-string v2, "PeopleContactsSync"

    const-string v3, "  updateMeProfile"

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-wide v4, v6, Lfjh;->a:J

    const-wide/16 v7, 0x0

    cmp-long v2, v4, v7

    if-nez v2, :cond_1e

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v2, v14}, Lfiu;->a(Ljava/util/ArrayList;Lfjh;ZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move-result-object v2

    iget-wide v3, v6, Lfjh;->a:J

    const-wide/16 v7, 0x0

    cmp-long v3, v3, v7

    if-nez v3, :cond_19

    if-eqz v2, :cond_19

    array-length v3, v2

    if-lez v3, :cond_19

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    iput-wide v2, v6, Lfjh;->a:J

    :cond_19
    iget-boolean v2, v6, Lfjh;->i:Z

    if-eqz v2, :cond_1a

    iget-wide v2, v6, Lfjh;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1a

    iget-wide v3, v6, Lfjh;->a:J

    iget-object v5, v6, Lfjh;->g:Ljava/lang/String;

    iget-object v2, v6, Lfjh;->g:Ljava/lang/String;

    invoke-static {v2}, Lfiu;->a(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x1

    move-object/from16 v2, p0

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v8}, Lfiu;->a(JLjava/lang/String;IZLfia;)Lfjb;

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    :cond_1a
    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1b

    const-string v2, "PeopleContactsSync"

    const-string v3, "  syncPeopleToContacts"

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1b
    iget-object v2, v13, Lfje;->c:Ljava/util/ArrayList;

    if-eqz v2, :cond_1c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, Lfiu;->a(Ljava/util/List;Lfia;)V

    :cond_1c
    iget-wide v2, v13, Lfje;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_20

    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1d

    const-string v2, "PeopleContactsSync"

    const-string v3, "  insertGooglePlusGroup"

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "sourceid"

    const-string v4, "plus"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "title"

    move-object/from16 v0, p0

    iget-object v4, v0, Lfiu;->E:Landroid/content/Context;

    const v5, 0x7f0b01b2    # com.google.android.gms.R.string.people_google_plus_group

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "group_is_read_only"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lfiu;->F:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfiu;->K:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_1f

    new-instance v2, Lfja;

    const-string v3, "Cannot insert group into Contacts"

    invoke-direct {v2, v3}, Lfja;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1e
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_1f
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    iput-wide v2, v13, Lfje;->b:J

    :cond_20
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v15

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v13, Lfje;->f:Ljava/util/ArrayList;

    if-eqz v2, :cond_21

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v10, Lfeu;->g:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v2, v1}, Lfiu;->a(Ljava/util/ArrayList;Ljava/util/List;Lfia;)V

    :cond_21
    iget-object v4, v13, Lfje;->d:Ljava/util/ArrayList;

    if-eqz v4, :cond_22

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    iput v2, v10, Lfeu;->h:I

    iget-wide v5, v13, Lfje;->b:J

    const/4 v7, 0x1

    move-object/from16 v2, p0

    move v8, v14

    move-object/from16 v9, p1

    invoke-direct/range {v2 .. v9}, Lfiu;->a(Ljava/util/ArrayList;Ljava/util/List;JZZLfia;)V

    :cond_22
    iget-object v4, v13, Lfje;->e:Ljava/util/ArrayList;

    if-eqz v4, :cond_23

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    iput v2, v10, Lfeu;->f:I

    iget-wide v5, v13, Lfje;->b:J

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move v8, v14

    move-object/from16 v9, p1

    invoke-direct/range {v2 .. v9}, Lfiu;->a(Ljava/util/ArrayList;Ljava/util/List;JZZLfia;)V

    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lfiu;->E:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lfiu;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    iget-boolean v2, v13, Lfje;->h:Z

    if-eqz v2, :cond_24

    const-string v2, "PeopleContactsSync"

    const-string v3, "  syncAvatars"

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PeopleContactsSync"

    const-string v3, "    Syncing thumbnails..."

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lfiu;->c()Lfiz;

    move-result-object v2

    if-eqz v2, :cond_24

    invoke-virtual {v2}, Lfiz;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_25

    :cond_24
    :goto_8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v15

    iput-wide v2, v10, Lfeu;->c:J

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    goto/16 :goto_5

    :cond_25
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lfiz;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lfev;

    invoke-direct {v2}, Lfev;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v2}, Lfiu;->a(Lfia;Ljava/util/List;Lfev;)V

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v2}, Lfiu;->b(Lfia;Ljava/util/List;Lfev;)V

    invoke-virtual/range {p1 .. p1}, Lfia;->c()V

    iput-object v2, v10, Lfeu;->i:Lfev;

    goto :goto_8
.end method

.method public final a(Z[Ljava/lang/String;)V
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lfiu;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lfiu;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync not supported."

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lfiu;->E:Landroid/content/Context;

    const-string v4, "PeopleContactsSync"

    iget-object v5, p0, Lfiu;->H:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Changing contact sync settings enabled:"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " target:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez p2, :cond_3

    const-string v0, "null"

    :goto_1
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v4, v5, v0}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->d()Lfbn;

    move-result-object v5

    invoke-virtual {v5}, Lfbn;->b()V

    :try_start_0
    array-length v6, p2

    move v4, v3

    move v0, v3

    move v1, v3

    :goto_2
    if-ge v4, v6, :cond_6

    aget-object v7, p2, v4

    const-string v8, "$$mycircles$$"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-eqz v8, :cond_4

    move v1, p1

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_4
    :try_start_1
    const-string v8, "$$everrgreen$$"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    move v0, p1

    goto :goto_3

    :cond_5
    const-string v8, "PeopleContactsSync"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Contacts sync settings.  Ignoring invalid target="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lfbn;->e()V

    throw v0

    :cond_6
    :try_start_2
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "sync_to_contacts"

    if-eqz p1, :cond_7

    move v4, v2

    :goto_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "sync_circles_to_contacts"

    if-eqz v1, :cond_8

    move v1, v2

    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "sync_evergreen_to_contacts"

    if-eqz v0, :cond_9

    move v0, v2

    :goto_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "owners"

    const-string v1, "_id=?"

    iget-object v2, p0, Lfiu;->J:Ljava/lang/String;

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v0, v6, v1, v2}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v5}, Lfbn;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v5}, Lfbn;->e()V

    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v0}, Lfiu;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfiu;->E:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    iget-object v1, p0, Lfiu;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfhz;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    move v4, v3

    goto :goto_4

    :cond_8
    move v1, v3

    goto :goto_5

    :cond_9
    move v0, v3

    goto :goto_6
.end method
