.class public final Ledf;
.super Lac;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Landroid/util/SparseArray;

.field private c:Landroid/content/Context;

.field private d:Lu;

.field private e:Z

.field private f:Ledj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lu;[Ledi;Ledj;)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, p2}, Lac;-><init>(Lu;)V

    iput-boolean v0, p0, Ledf;->e:Z

    iput-object p1, p0, Ledf;->c:Landroid/content/Context;

    iput-object p2, p0, Ledf;->d:Lu;

    iput-object p4, p0, Ledf;->f:Ledj;

    invoke-static {p3}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Ledf;->a:Ljava/util/ArrayList;

    array-length v1, p3

    iget-object v2, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    new-instance v0, Landroid/util/SparseArray;

    array-length v1, p3

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Ledf;->b:Landroid/util/SparseArray;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ltz p1, :cond_1

    move v0, v2

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    move v0, v2

    :goto_1
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledi;

    iget-object v1, v0, Ledi;->a:Ljava/lang/Class;

    invoke-static {v1}, Lbiq;->a(Ljava/lang/Object;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    iget-object v0, v0, Ledi;->c:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->g(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object v0, p0, Ledf;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    const-string v4, "GFragmentPagerAdapter"

    const-string v5, "getItem(): fragment at this position was already instantiated!"

    invoke-static {v4, v5}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "GFragmentPagerAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  - position: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "GFragmentPagerAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  - previous instance: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "GFragmentPagerAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  - new instance:      "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez v0, :cond_3

    :goto_2
    const-string v0, "getItem(): fragment at this position was already instantiated!"

    invoke-static {v2, v0}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ledf;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    return-object v1

    :cond_1
    move v0, v3

    goto/16 :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t instantiate Fragment at pos "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t instantiate Fragment at pos "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    move v2, v3

    goto :goto_2
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 5

    check-cast p1, Landroid/os/Bundle;

    const-string v0, "FRAGMENT_TAGS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    if-eqz v2, :cond_2

    iget-object v3, p0, Ledf;->b:Landroid/util/SparseArray;

    iget-object v4, p0, Ledf;->d:Lu;

    invoke-virtual {v4, v2}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b()Landroid/os/Parcelable;
    .locals 5

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Ledf;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    aput-object v0, v4, v1

    goto :goto_1

    :cond_1
    const-string v0, "FRAGMENT_TAGS"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    return-object v2
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, Lbiq;->a(Z)V

    iget-object v0, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledi;

    iget v0, v0, Ledi;->b:I

    iget-object v1, p0, Ledf;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final c(I)F
    .locals 1

    iget-object v0, p0, Ledf;->f:Ledj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledf;->f:Ledj;

    invoke-interface {v0, p1}, Ledj;->e(I)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final d()Ljava/util/ArrayList;
    .locals 4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    iget-object v1, p0, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Ledf;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method
