.class public final Lgrn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/MaskedWalletRequest;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;)V
    .locals 0

    iput-object p1, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgrn;-><init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/identity/intents/model/CountrySpecification;)Lgrn;
    .locals 2

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v0, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v0, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->b:Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Z)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c:Z

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->f:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Z)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->d:Z

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Z)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->i:Z

    return-object p0
.end method

.method public final e(Z)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j:Z

    return-object p0
.end method

.method public final f(Z)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->l:Z

    return-object p0
.end method

.method public final g(Z)Lgrn;
    .locals 1

    iget-object v0, p0, Lgrn;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->m:Z

    return-object p0
.end method
