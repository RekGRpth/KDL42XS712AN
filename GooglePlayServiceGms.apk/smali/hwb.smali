.class public final Lhwb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lhwl;


# instance fields
.field private final b:Lhwz;

.field private final c:Lhxd;

.field private final d:Lhxh;

.field private final e:Lhxc;

.field private final f:Lhxb;

.field private final g:Lhwy;

.field private final h:Lhwc;

.field private final i:Lhwd;

.field private final j:Lhwe;

.field private final k:Ljava/util/ArrayList;

.field private final l:Ljava/util/ArrayList;

.field private final m:Ljava/util/ArrayList;

.field private volatile n:Ljava/util/Collection;

.field private o:Z

.field private p:Z

.field private q:Ljava/lang/Iterable;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Lhwl;

    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    const-wide/32 v3, 0x493e0

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lhwl;-><init>(Lcom/google/android/gms/location/LocationRequest;ZZLjava/util/Collection;Z)V

    sput-object v0, Lhwb;->a:Lhwl;

    return-void
.end method

.method public constructor <init>(Lhwz;Lhxd;Lhxh;Lhxc;Lhxb;Lhwy;Landroid/hardware/SensorManager;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhwb;->k:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhwb;->l:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhwb;->m:Ljava/util/ArrayList;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lhwb;->n:Ljava/util/Collection;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwb;->o:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwb;->p:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lhwb;->q:Ljava/lang/Iterable;

    new-instance v0, Lhwc;

    new-instance v1, Lhxf;

    new-instance v2, Lhlf;

    invoke-direct {v2, p7}, Lhlf;-><init>(Landroid/hardware/SensorManager;)V

    invoke-direct {v1, v2}, Lhxf;-><init>(Lhlf;)V

    invoke-direct {v0, p0, v1}, Lhwc;-><init>(Lhwb;Lhxf;)V

    iput-object v0, p0, Lhwb;->h:Lhwc;

    new-instance v0, Lhwd;

    new-instance v1, Lhxf;

    new-instance v2, Lhlf;

    invoke-direct {v2, p7}, Lhlf;-><init>(Landroid/hardware/SensorManager;)V

    invoke-direct {v1, v2}, Lhxf;-><init>(Lhlf;)V

    invoke-direct {v0, p0, v1}, Lhwd;-><init>(Lhwb;Lhxf;)V

    iput-object v0, p0, Lhwb;->i:Lhwd;

    new-instance v0, Lhwe;

    new-instance v1, Lhxf;

    new-instance v2, Lhlf;

    invoke-direct {v2, p7}, Lhlf;-><init>(Landroid/hardware/SensorManager;)V

    invoke-direct {v1, v2}, Lhxf;-><init>(Lhlf;)V

    invoke-direct {v0, p0, v1}, Lhwe;-><init>(Lhwb;Lhxf;)V

    iput-object v0, p0, Lhwb;->j:Lhwe;

    iput-object p1, p0, Lhwb;->b:Lhwz;

    iput-object p2, p0, Lhwb;->c:Lhxd;

    iput-object p3, p0, Lhwb;->d:Lhxh;

    iput-object p4, p0, Lhwb;->e:Lhxc;

    iput-object p5, p0, Lhwb;->f:Lhxb;

    iput-object p6, p0, Lhwb;->g:Lhwy;

    return-void
.end method

.method static synthetic a(Lhwb;)Lhwz;
    .locals 1

    iget-object v0, p0, Lhwb;->b:Lhwz;

    return-object v0
.end method

.method static synthetic b(Lhwb;)Lhwy;
    .locals 1

    iget-object v0, p0, Lhwb;->g:Lhwy;

    return-object v0
.end method

.method static synthetic c(Lhwb;)Lhxh;
    .locals 1

    iget-object v0, p0, Lhwb;->d:Lhxh;

    return-object v0
.end method

.method static synthetic d(Lhwb;)Lhxc;
    .locals 1

    iget-object v0, p0, Lhwb;->e:Lhxc;

    return-object v0
.end method

.method static synthetic e(Lhwb;)Lhxb;
    .locals 1

    iget-object v0, p0, Lhwb;->f:Lhxb;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lhwb;->n:Ljava/util/Collection;

    return-object v0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lhwb;->h:Lhwc;

    invoke-virtual {v0, p1}, Lhwc;->a(Landroid/location/Location;)V

    iget-object v0, p0, Lhwb;->i:Lhwd;

    invoke-virtual {v0, p1}, Lhwd;->a(Landroid/location/Location;)V

    iget-object v0, p0, Lhwb;->j:Lhwe;

    invoke-virtual {v0, p1}, Lhwe;->a(Landroid/location/Location;)V

    return-void
.end method

.method public final a(Lhwr;)V
    .locals 2

    iget-boolean v0, p0, Lhwb;->p:Z

    invoke-virtual {p1}, Lhwr;->b()Z

    move-result v1

    iput-boolean v1, p0, Lhwb;->p:Z

    iget-boolean v1, p0, Lhwb;->p:Z

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lhwb;->q:Ljava/lang/Iterable;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhwb;->a(Ljava/lang/Iterable;Z)V

    :cond_0
    return-void
.end method

.method final a(Ljava/io/PrintWriter;)V
    .locals 4

    iget-boolean v0, p0, Lhwb;->o:Z

    if-eqz v0, :cond_0

    const-string v0, "Fused Location Provider Is Primed"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lhwb;->b:Lhwz;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Lhwb;->c:Lhxd;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Lhwb;->d:Lhxh;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Lhwb;->e:Lhxc;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Lhwb;->f:Lhxb;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Lhwb;->g:Lhwy;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    const-string v0, "\nLocation Requests (GCore + Platform Overlay):"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lhwb;->q:Ljava/lang/Iterable;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhwl;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const-string v0, "Fused Location Provider Is Not Primed"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Iterable;Z)V
    .locals 9

    iput-object p1, p0, Lhwb;->q:Ljava/lang/Iterable;

    iget-object v0, p0, Lhwb;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lhwb;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lhwb;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lhwl;

    iget-object v0, v5, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lhwb;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lhwb;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhwb;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lhwb;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwb;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/google/android/gms/location/LocationRequest;

    iget-object v0, v5, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v1, v0}, Lcom/google/android/gms/location/LocationRequest;-><init>(Lcom/google/android/gms/location/LocationRequest;)V

    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v2

    const-wide/32 v7, 0x493e0

    invoke-static {v2, v3, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    new-instance v0, Lhwl;

    iget-boolean v2, v5, Lhwl;->b:Z

    iget-boolean v3, v5, Lhwl;->c:Z

    iget-object v4, v5, Lhwl;->e:Ljava/util/Collection;

    iget-boolean v5, v5, Lhwl;->d:Z

    invoke-direct/range {v0 .. v5}, Lhwl;-><init>(Lcom/google/android/gms/location/LocationRequest;ZZLjava/util/Collection;Z)V

    iget-object v1, p0, Lhwb;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lhwb;->o:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lhwb;->p:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhwb;->m:Ljava/util/ArrayList;

    sget-object v1, Lhwb;->a:Lhwl;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    iget-object v0, p0, Lhwb;->h:Lhwc;

    iget-object v1, p0, Lhwb;->k:Ljava/util/ArrayList;

    invoke-static {v1}, Lhwm;->a(Ljava/util/Collection;)Lhwm;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lhwc;->a(Lhwm;Z)V

    iget-object v0, p0, Lhwb;->l:Ljava/util/ArrayList;

    invoke-static {v0}, Lhwm;->a(Ljava/util/Collection;)Lhwm;

    move-result-object v0

    iget-object v1, p0, Lhwb;->m:Ljava/util/ArrayList;

    invoke-static {v1}, Lhwm;->a(Ljava/util/Collection;)Lhwm;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhwm;->a(Lhwm;)Lhwm;

    move-result-object v1

    iget-object v2, p0, Lhwb;->i:Lhwd;

    invoke-virtual {v2, v0, p2}, Lhwd;->a(Lhwm;Z)V

    iget-object v2, p0, Lhwb;->j:Lhwe;

    invoke-virtual {v2, v1, p2}, Lhwe;->a(Lhwm;Z)V

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lhwm;->f:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    iget-object v4, v1, Lhwm;->f:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lhwm;->f:Ljava/util/Collection;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, v1, Lhwm;->f:Ljava/util/Collection;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lhwb;->n:Ljava/util/Collection;

    return-void

    :cond_3
    iget-object v0, p0, Lhwb;->l:Ljava/util/ArrayList;

    sget-object v1, Lhwb;->a:Lhwl;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lhwb;->h:Lhwc;

    iput-boolean p1, v0, Lhwc;->a:Z

    iget-object v0, v0, Lhws;->f:Lhww;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhww;->a(Z)V

    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    iget-boolean v0, p0, Lhwb;->o:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lhwb;->o:Z

    iget-object v0, p0, Lhwb;->q:Ljava/lang/Iterable;

    invoke-virtual {p0, v0, p2}, Lhwb;->a(Ljava/lang/Iterable;Z)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lhwb;->h:Lhwc;

    iget-object v0, v0, Lhws;->f:Lhww;

    invoke-virtual {v0}, Lhww;->a()V

    iget-object v0, p0, Lhwb;->i:Lhwd;

    iget-object v0, v0, Lhws;->f:Lhww;

    invoke-virtual {v0}, Lhww;->a()V

    iget-object v0, p0, Lhwb;->j:Lhwe;

    iget-object v0, v0, Lhws;->f:Lhww;

    invoke-virtual {v0}, Lhww;->a()V

    iget-object v0, p0, Lhwb;->c:Lhxd;

    invoke-virtual {v0}, Lhxd;->b()V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lhwb;->h:Lhwc;

    iget-object v0, v0, Lhws;->f:Lhww;

    invoke-virtual {v0}, Lhww;->f()V

    iget-object v0, p0, Lhwb;->i:Lhwd;

    iget-object v0, v0, Lhws;->f:Lhww;

    invoke-virtual {v0}, Lhww;->f()V

    iget-object v0, p0, Lhwb;->j:Lhwe;

    iget-object v0, v0, Lhws;->f:Lhww;

    invoke-virtual {v0}, Lhww;->f()V

    iget-object v0, p0, Lhwb;->c:Lhxd;

    invoke-virtual {v0}, Lhxd;->c()V

    return-void
.end method
