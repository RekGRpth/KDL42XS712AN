.class public abstract Ldwr;
.super Ldvn;
.source "SourceFile"


# instance fields
.field public final p:Z

.field private final q:I

.field private final r:I

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>(II)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ldwr;-><init>(IIZ)V

    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ldvn;-><init>()V

    if-lez p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    if-gtz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, Lbiq;->a(Z)V

    iput p1, p0, Ldwr;->q:I

    iput p2, p0, Ldwr;->r:I

    iput-boolean p3, p0, Ldwr;->p:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method protected final f()Lbdu;
    .locals 3

    iget-object v0, p0, Ldwr;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldwr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldwr;->s:Ljava/lang/String;

    iget-object v0, p0, Ldwr;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HeadlessFragment"

    const-string v1, "Account name not found in the Intent! Bailing!"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ldwr;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->b()V

    :cond_1
    invoke-static {}, Lcti;->a()Lctj;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lctj;->a:Z

    invoke-virtual {v0}, Lctj;->a()Lcti;

    move-result-object v0

    new-instance v1, Lbdw;

    invoke-direct {v1, p0, p0, p0}, Lbdw;-><init>(Landroid/content/Context;Lbdx;Lbdy;)V

    sget-object v2, Lcte;->e:Lbdm;

    invoke-virtual {v1, v2, v0}, Lbdw;->a(Lbdm;Lbdv;)Lbdw;

    move-result-object v0

    iget-object v1, p0, Ldwr;->s:Ljava/lang/String;

    iput-object v1, v0, Lbdw;->a:Ljava/lang/String;

    const-string v1, "com.google.android.gms"

    iput-object v1, v0, Lbdw;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lbdw;->a()Lbdu;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_GOOGLE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    iget-object v2, p0, Ldwr;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.play.games"

    invoke-static {p0, v1}, Ledw;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.games.DEST_APP_VERSION"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Ldwr;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final l()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final m()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldwr;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Ldwr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/16 v1, 0x4e21

    const/16 v0, 0x384

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    invoke-virtual {p0, v1}, Ldwr;->setResult(I)V

    invoke-virtual {p0}, Ldwr;->finish()V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Ldvn;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldvn;->onCreate(Landroid/os/Bundle;)V

    iget v0, p0, Ldwr;->q:I

    invoke-virtual {p0, v0}, Ldwr;->setContentView(I)V

    iget-boolean v0, p0, Ldwr;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljj;->b(Z)V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Ldwr;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget v1, p0, Ldwr;->r:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Ldvn;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Ldvn;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Ldwr;->onBackPressed()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Ldwr;->h()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0    # android.R.id.home
        0x7f0a037d -> :sswitch_1    # com.google.android.gms.R.id.menu_settings
    .end sparse-switch
.end method
