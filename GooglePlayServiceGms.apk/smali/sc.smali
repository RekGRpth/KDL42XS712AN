.class public abstract Lsc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public c:Z

.field public d:Lsm;

.field public e:Lrp;

.field private final f:Lsr;

.field private final g:I

.field private final h:Lsj;

.field private i:Ljava/lang/Integer;

.field private j:Lsf;

.field private k:Z

.field private l:Z

.field private m:J

.field private n:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lsj;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lsr;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lsr;

    invoke-direct {v0}, Lsr;-><init>()V

    :goto_0
    iput-object v0, p0, Lsc;->f:Lsr;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lsc;->c:Z

    iput-boolean v2, p0, Lsc;->k:Z

    iput-boolean v2, p0, Lsc;->l:Z

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lsc;->m:J

    iput-object v1, p0, Lsc;->e:Lrp;

    iput p1, p0, Lsc;->a:I

    iput-object p2, p0, Lsc;->b:Ljava/lang/String;

    iput-object p3, p0, Lsc;->h:Lsj;

    new-instance v0, Lrs;

    invoke-direct {v0}, Lrs;-><init>()V

    iput-object v0, p0, Lsc;->d:Lsm;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    iput v0, p0, Lsc;->g:I

    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Lsj;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, -0x1

    invoke-direct {p0, v0, p1, p2}, Lsc;-><init>(ILjava/lang/String;Lsj;)V

    return-void
.end method

.method protected static a(Lsp;)Lsp;
    .locals 0

    return-object p0
.end method

.method static synthetic a(Lsc;)Lsr;
    .locals 1

    iget-object v0, p0, Lsc;->f:Lsr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lsc;->a:I

    return v0
.end method

.method public final a(I)Lsc;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lsc;->i:Ljava/lang/Integer;

    return-object p0
.end method

.method public final a(Ljava/lang/Object;)Lsc;
    .locals 0

    iput-object p1, p0, Lsc;->n:Ljava/lang/Object;

    return-object p0
.end method

.method public final a(Lrp;)Lsc;
    .locals 0

    iput-object p1, p0, Lsc;->e:Lrp;

    return-object p0
.end method

.method public final a(Lsf;)Lsc;
    .locals 0

    iput-object p1, p0, Lsc;->j:Lsf;

    return-object p0
.end method

.method public final a(Lsm;)Lsc;
    .locals 0

    iput-object p1, p0, Lsc;->d:Lsm;

    return-object p0
.end method

.method public final a(Z)Lsc;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lsc;->c:Z

    return-object p0
.end method

.method protected abstract a(Lrz;)Lsi;
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    sget-boolean v0, Lsr;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsc;->f:Lsr;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lsr;->a(Ljava/lang/String;J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lsc;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lsc;->m:J

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lsc;->n:Ljava/lang/Object;

    return-object v0
.end method

.method protected abstract b(Ljava/lang/Object;)V
.end method

.method final b(Ljava/lang/String;)V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lsc;->j:Lsf;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lsc;->j:Lsf;

    iget-object v2, v1, Lsf;->b:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lsf;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lsc;->c:Z

    if-eqz v0, :cond_2

    iget-object v2, v1, Lsf;->a:Ljava/util/Map;

    monitor-enter v2

    :try_start_1
    invoke-virtual {p0}, Lsc;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lsf;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    if-eqz v0, :cond_1

    sget-boolean v4, Lsq;->b:Z

    if-eqz v4, :cond_0

    const-string v4, "Releasing %d waiting requests for cacheKey=%s."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lsq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v1, v1, Lsf;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->addAll(Ljava/util/Collection;)Z

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    sget-boolean v0, Lsr;->a:Z

    if-eqz v0, :cond_5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v2, v3, :cond_4

    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lsd;

    invoke-direct {v3, p0, p1, v0, v1}, Lsd;-><init>(Lsc;Ljava/lang/String;J)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    iget-object v2, p0, Lsc;->f:Lsr;

    invoke-virtual {v2, p1, v0, v1}, Lsr;->a(Ljava/lang/String;J)V

    iget-object v0, p0, Lsc;->f:Lsr;

    invoke-virtual {p0}, Lsc;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsr;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lsc;->m:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v2, v0, v2

    if-ltz v2, :cond_3

    const-string v2, "%d ms: %s"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-virtual {p0}, Lsc;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    invoke-static {v2, v3}, Lsq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public b(Lsp;)V
    .locals 1

    iget-object v0, p0, Lsc;->h:Lsj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsc;->h:Lsj;

    invoke-interface {v0, p1}, Lsj;->a(Lsp;)V

    :cond_0
    return-void
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lsc;->g:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lsc;

    invoke-virtual {p0}, Lsc;->o()Lse;

    move-result-object v0

    invoke-virtual {p1}, Lsc;->o()Lse;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lsc;->i:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Lsc;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Lse;->ordinal()I

    move-result v1

    invoke-virtual {v0}, Lse;->ordinal()I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lsc;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lsc;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Lrp;
    .locals 1

    iget-object v0, p0, Lsc;->e:Lrp;

    return-object v0
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lsc;->k:Z

    return-void
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lsc;->k:Z

    return v0
.end method

.method public i()Ljava/util/Map;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Lsc;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()[B
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "application/x-www-form-urlencoded; charset=UTF-8"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Lsc;->c:Z

    return v0
.end method

.method public o()Lse;
    .locals 1

    sget-object v0, Lse;->b:Lse;

    return-object v0
.end method

.method public final p()I
    .locals 1

    iget-object v0, p0, Lsc;->d:Lsm;

    invoke-interface {v0}, Lsm;->a()I

    move-result v0

    return v0
.end method

.method public final q()Lsm;
    .locals 1

    iget-object v0, p0, Lsc;->d:Lsm;

    return-object v0
.end method

.method public final r()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lsc;->l:Z

    return-void
.end method

.method public final s()Z
    .locals 1

    iget-boolean v0, p0, Lsc;->l:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lsc;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, Lsc;->k:Z

    if-eqz v0, :cond_0

    const-string v0, "[X] "

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lsc;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lsc;->o()Lse;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsc;->i:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "[ ] "

    goto :goto_0
.end method
