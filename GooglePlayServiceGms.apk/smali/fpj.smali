.class public final Lfpj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lfpj;


# instance fields
.field private final b:Ldl;

.field private final c:Landroid/content/res/Resources;

.field private d:Landroid/graphics/drawable/Drawable;


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfpj;->c:Landroid/content/res/Resources;

    new-instance v0, Ldl;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ldl;-><init>(I)V

    iput-object v0, p0, Lfpj;->b:Ldl;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lfpj;
    .locals 2

    sget-object v0, Lfpj;->a:Lfpj;

    if-nez v0, :cond_0

    new-instance v0, Lfpj;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lfpj;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lfpj;->a:Lfpj;

    :cond_0
    sget-object v0, Lfpj;->a:Lfpj;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lfpj;->b:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfpj;->d:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lfpj;->c:Landroid/content/res/Resources;

    const v1, 0x7f020200    # com.google.android.gms.R.drawable.plus_ic_apps_lightgrey_24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfpj;->d:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget-object v0, p0, Lfpj;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lfpj;->b:Ldl;

    invoke-virtual {v0, p1, p2}, Ldl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
