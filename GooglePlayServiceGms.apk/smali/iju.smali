.class public final Liju;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Collection;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lijt;

.field private final d:Lihy;

.field private final e:Lcom/google/android/location/reporting/LocationReportingController;

.field private final f:Lcom/google/android/location/reporting/LocationRecordStore;

.field private final g:Lcom/google/android/location/reporting/DetectedActivityStore;

.field private final h:Lcom/google/android/location/reporting/ApiMetadataStore;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Liju;->a:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lijt;Lihy;Lcom/google/android/location/reporting/LocationReportingController;Lcom/google/android/location/reporting/LocationRecordStore;Lcom/google/android/location/reporting/DetectedActivityStore;Lcom/google/android/location/reporting/ApiMetadataStore;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Likh;->a(Landroid/content/Context;)V

    iput-object p1, p0, Liju;->b:Landroid/content/Context;

    iput-object p2, p0, Liju;->c:Lijt;

    iput-object p3, p0, Liju;->d:Lihy;

    iput-object p4, p0, Liju;->e:Lcom/google/android/location/reporting/LocationReportingController;

    iput-object p5, p0, Liju;->f:Lcom/google/android/location/reporting/LocationRecordStore;

    iput-object p6, p0, Liju;->g:Lcom/google/android/location/reporting/DetectedActivityStore;

    iput-object p7, p0, Liju;->h:Lcom/google/android/location/reporting/ApiMetadataStore;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 7

    const-wide/32 v4, 0x36ee80

    const/4 v1, 0x0

    const-string v0, "com.google.android.location.reporting.ACTION_UPDATE_WORLD"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v1, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    add-long v2, v1, v4

    const/4 v1, 0x2

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "WorldUpdater scheduled for every 3600000 millis"

    invoke-static {v0, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const-string v0, "com.google.android.location.reporting.ACTION_UPDATE_WORLD"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v1, "receiverAction"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    invoke-static {p0, v0}, Likh;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    return-void

    :cond_0
    const-string v1, "GCoreUlr"

    const-string v2, ""

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Missing receiver intent"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Lihn;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lihn;->a()I
    :try_end_0
    .catch Liho; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_0
    sget-object v1, Liju;->a:Ljava/util/Collection;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "GCoreUlr"

    const-string v2, ""

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Called for unexpected action"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const-string v1, "com.google.android.apps.maps"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, p1}, Liju;->a(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring non-Maps package in package broadcast: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 10

    const-string v0, "receiverAction"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "GCoreUlr"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WorldUpdater received intent "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with receiverAction "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Liju;->f:Lcom/google/android/location/reporting/LocationRecordStore;

    invoke-static {v1}, Liju;->a(Lihn;)V

    iget-object v1, p0, Liju;->g:Lcom/google/android/location/reporting/DetectedActivityStore;

    invoke-static {v1}, Liju;->a(Lihn;)V

    iget-object v1, p0, Liju;->h:Lcom/google/android/location/reporting/ApiMetadataStore;

    invoke-static {v1}, Liju;->a(Lihn;)V

    iget-object v1, p0, Liju;->c:Lijt;

    invoke-virtual {v1}, Lijt;->b()Z

    iget-object v1, p0, Liju;->b:Landroid/content/Context;

    invoke-static {v1}, Likh;->a(Landroid/content/Context;)V

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Liju;->d:Lihy;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lihy;->a(J)V

    :cond_1
    iget-object v1, p0, Liju;->c:Lijt;

    invoke-virtual {v1}, Lijt;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v2

    iget-object v1, p0, Liju;->b:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/service/InitializerService;->a(Landroid/content/Context;Lcom/google/android/location/reporting/service/ReportingConfig;)I

    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/ReportingConfig;->g()Z

    move-result v4

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/ReportingConfig;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v6

    if-nez v6, :cond_3

    const-string v1, "GCoreUlr"

    const-string v6, ""

    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "null account in "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v6, v7}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    if-eqz v4, :cond_5

    sget-object v1, Lijs;->n:Lbfy;

    invoke-virtual {v1}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v6, v7, v8}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;J)V

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->j()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v6}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "GCoreUlr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Triggering manual sync to test re-auth for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v0, "WorldUpdater"

    invoke-static {v6, v0}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-static {v6}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;)V

    goto :goto_0

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Liju;->e:Lcom/google/android/location/reporting/LocationReportingController;

    invoke-virtual {v3, v2, v0, v1}, Lcom/google/android/location/reporting/LocationReportingController;->a(Lcom/google/android/location/reporting/service/ReportingConfig;J)V

    iget-object v0, p0, Liju;->b:Landroid/content/Context;

    invoke-static {v0}, Liim;->a(Landroid/content/Context;)V

    return-void
.end method
