.class public final Ldsy;
.super Ldrv;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ldfh;

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ldfh;II)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Ldrv;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    iput-object p2, p0, Ldsy;->b:Ldad;

    iput-object p3, p0, Ldsy;->c:Ldfh;

    iput p4, p0, Ldsy;->d:I

    iput p5, p0, Ldsy;->e:I

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lcun;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    const/4 v5, 0x1

    if-nez p3, :cond_0

    iget-object v2, p0, Ldsy;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsy;->c:Ldfh;

    iget v4, p0, Ldsy;->d:I

    iget v5, p0, Ldsy;->e:I

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldfh;II)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-ne p3, v5, :cond_1

    iget-object v2, p0, Ldsy;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, p0, Ldsy;->c:Ldfh;

    invoke-virtual {v0}, Ldfh;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Ldsy;->c:Ldfh;

    invoke-virtual {v0}, Ldfh;->c()Ljava/lang/String;

    move-result-object v4

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incorrect number of data holders requested!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    array-length v0, p1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldsy;->b:Ldad;

    aget-object v1, p1, v1

    aget-object v2, p1, v2

    invoke-interface {v0, v1, v2}, Ldad;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method
