.class public final Livd;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field a:[Ljava/io/InputStream;

.field b:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/InputStream;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-direct {p0, v0}, Livd;-><init>([Ljava/io/InputStream;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/io/InputStream;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    invoke-direct {p0, v0}, Livd;-><init>([Ljava/io/InputStream;)V

    return-void
.end method

.method public constructor <init>([Ljava/io/InputStream;)V
    .locals 1

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Livd;->a:[Ljava/io/InputStream;

    const/4 v0, 0x0

    iput v0, p0, Livd;->b:I

    iput-object p1, p0, Livd;->a:[Ljava/io/InputStream;

    return-void
.end method

.method private a()Ljava/io/InputStream;
    .locals 2

    iget-object v0, p0, Livd;->a:[Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget v0, p0, Livd;->b:I

    iget-object v1, p0, Livd;->a:[Ljava/io/InputStream;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Livd;->a:[Ljava/io/InputStream;

    iget v1, p0, Livd;->b:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Livd;->a:[Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget v0, p0, Livd;->b:I

    iget-object v1, p0, Livd;->a:[Ljava/io/InputStream;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Livd;->a:[Ljava/io/InputStream;

    iget v1, p0, Livd;->b:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Livd;->a:[Ljava/io/InputStream;

    iget v1, p0, Livd;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Livd;->b:I

    aput-object v3, v0, v1

    iget v0, p0, Livd;->b:I

    iget-object v1, p0, Livd;->a:[Ljava/io/InputStream;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iput-object v3, p0, Livd;->a:[Ljava/io/InputStream;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized available()I
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Livd;->a()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized close()V
    .locals 4

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Livd;->a:[Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    :try_start_1
    iget v2, p0, Livd;->b:I

    iget-object v3, p0, Livd;->a:[Ljava/io/InputStream;

    array-length v3, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ge v2, v3, :cond_2

    :try_start_2
    iget-object v2, p0, Livd;->a:[Ljava/io/InputStream;

    iget v3, p0, Livd;->b:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    iget v2, p0, Livd;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Livd;->b:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :try_start_4
    iput-object v2, p0, Livd;->a:[Ljava/io/InputStream;

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public final declared-synchronized read()I
    .locals 2

    const/4 v0, -0x1

    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-direct {p0}, Livd;->a()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-eq v1, v0, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Livd;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized read([BII)I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, -0x1

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-nez p3, :cond_2

    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    if-ltz p2, :cond_3

    if-ltz p3, :cond_3

    add-int v2, p2, p3

    :try_start_1
    array-length v3, p1

    if-le v2, v3, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_4
    iget-object v2, p0, Livd;->a:[Ljava/io/InputStream;

    if-eqz v2, :cond_5

    iget v2, p0, Livd;->b:I

    iget-object v3, p0, Livd;->a:[Ljava/io/InputStream;

    array-length v3, v3

    if-lt v2, v3, :cond_6

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    if-eqz p3, :cond_1

    :goto_1
    if-lez p3, :cond_8

    invoke-direct {p0}, Livd;->a()Ljava/io/InputStream;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-eq v2, v1, :cond_7

    add-int/2addr p2, v2

    sub-int/2addr p3, v2

    add-int/2addr v0, v2

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Livd;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_8
    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized skip(J)J
    .locals 10

    const-wide/16 v8, 0x1

    const-wide/16 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Livd;->a:[Ljava/io/InputStream;

    if-eqz v2, :cond_0

    iget v2, p0, Livd;->b:I

    iget-object v3, p0, Livd;->a:[Ljava/io/InputStream;

    array-length v3, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v2, v3, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_1
    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    move-wide v2, v0

    :goto_1
    cmp-long v4, p1, v0

    if-lez v4, :cond_4

    :try_start_1
    invoke-direct {p0}, Livd;->a()Ljava/io/InputStream;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v4, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v5

    cmp-long v7, v5, v0

    if-nez v7, :cond_3

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    invoke-direct {p0}, Livd;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    sub-long/2addr p1, v8

    add-long/2addr v2, v8

    goto :goto_1

    :cond_3
    sub-long/2addr p1, v5

    add-long/2addr v2, v5

    goto :goto_1

    :cond_4
    move-wide v0, v2

    goto :goto_0
.end method
