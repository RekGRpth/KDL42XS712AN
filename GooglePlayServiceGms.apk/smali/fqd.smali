.class public abstract Lfqd;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lal;
.implements Landroid/view/View$OnClickListener;
.implements Lfqr;


# instance fields
.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Z

.field public s:Lfrb;

.field private t:Z

.field private u:Z

.field private v:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "audienceSelectionList"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lfqd;->a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lfqy;

    move-result-object v1

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v2, 0x7f0a01a7    # com.google.android.gms.R.id.fragment_container

    const-string v3, "audienceSelectionList"

    invoke-virtual {v0, v2, v1, v3}, Lag;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    return-void
.end method

.method private m()Z
    .locals 2

    invoke-virtual {p0}, Lfqd;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfqd;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final N_()V
    .locals 0

    return-void
.end method

.method public final a(ILandroid/os/Bundle;)Lcb;
    .locals 6

    if-nez p1, :cond_0

    new-instance v0, Lfrm;

    iget-object v2, p0, Lfqd;->n:Ljava/lang/String;

    iget-object v3, p0, Lfqd;->o:Ljava/lang/String;

    iget-object v4, p0, Lfqd;->q:Ljava/lang/String;

    iget-object v5, p0, Lfqd;->p:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfrm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lfqy;
.end method

.method protected final a(ILandroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lfqd;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lfqd;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfqd;->h()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    sget-object v1, Lbdd;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v0, v1}, Lfqd;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lfqd;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lfqd;->finish()V

    return-void
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lfqd;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public a(Lcb;Lfed;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget v0, p1, Lcb;->m:I

    if-nez v0, :cond_0

    check-cast p1, Lfrm;

    invoke-virtual {p1}, Lfrm;->b()Lbbo;

    move-result-object v0

    invoke-virtual {v0}, Lbbo;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfed;->a()I

    move-result v1

    if-ne v1, v7, :cond_2

    invoke-virtual {p2, v6}, Lfed;->b(I)Lfec;

    move-result-object v0

    invoke-interface {v0}, Lfec;->d()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v0, p0, Lfqd;->t:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lfqd;->n:Ljava/lang/String;

    iget-object v3, p0, Lfqd;->n:Ljava/lang/String;

    iget-object v4, p0, Lfqd;->p:Ljava/lang/String;

    invoke-virtual {p0}, Lfqd;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lfmu;

    invoke-direct {v1, p0, v0}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput v7, v1, Lfmu;->b:I

    invoke-virtual {v1}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v6}, Lfqd;->startActivityForResult(Landroid/content/Intent;I)V

    iput-boolean v7, p0, Lfqd;->t:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lfqd;->a(Lfec;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lbbo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lfqd;->t:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, p0, v1}, Lbbo;->a(Landroid/app/Activity;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqd;->t:Z
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lfqd;->J_()Lak;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1, p0}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    goto :goto_0
.end method

.method public bridge synthetic a(Lcb;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lfed;

    invoke-virtual {p0, p1, p2}, Lfqd;->a(Lcb;Lfed;)V

    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    iget-object v0, p0, Lfqd;->n:Ljava/lang/String;

    iget-object v1, p0, Lfqd;->p:Ljava/lang/String;

    invoke-static {p0, v0, p1, p2, v1}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V
    .locals 7

    iget-object v1, p0, Lfqd;->n:Ljava/lang/String;

    invoke-virtual {p0}, Lfqd;->h()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v4

    iget-object v5, p0, Lfqd;->p:Ljava/lang/String;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    return-void
.end method

.method public a(Lfec;)V
    .locals 0

    return-void
.end method

.method protected abstract e()I
.end method

.method public f()V
    .locals 2

    invoke-virtual {p0}, Lfqd;->i()Lbew;

    move-result-object v0

    iget-object v0, v0, Lbew;->a:Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lfqd;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public g()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lfqd;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method protected abstract h()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
.end method

.method public i()Lbew;
    .locals 6

    new-instance v0, Lbew;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-direct {v0, v1}, Lbew;-><init>(Landroid/content/Intent;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lfqd;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lbew;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lfqd;->s:Lfrb;

    iget-object v2, v2, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, v1}, Lbew;->c(Ljava/util/List;)Lbew;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbew;->a(Ljava/util/ArrayList;)Lbew;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbew;->b(Ljava/util/ArrayList;)Lbew;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lbew;->a(I)Lbew;

    move-result-object v0

    return-object v0
.end method

.method protected final j()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lfqd;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public final k()Lfrb;
    .locals 1

    iget-object v0, p0, Lfqd;->s:Lfrb;

    return-object v0
.end method

.method protected final l()Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfqd;->s:Lfrb;

    iget-object v1, v1, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbcl;->a(Ljava/util/List;Z)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    invoke-virtual {p0}, Lfqd;->g()V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-virtual {p0}, Lfqd;->g()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a025c    # com.google.android.gms.R.id.ok

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lfqd;->f()V

    :cond_0
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/16 v7, 0xb

    const/4 v6, 0x7

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lfqd;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "com.google.android.gms.common.acl.EXTRA_TITLE_TEXT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v3}, Lfqd;->requestWindowFeature(I)Z

    iput-boolean v5, p0, Lfqd;->r:Z

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "AudienceSelectionActivi"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v5, v4}, Lfqd;->a(ILandroid/content/Intent;)V

    :goto_1
    return-void

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v7, :cond_0

    invoke-virtual {p0, v6}, Lfqd;->requestWindowFeature(I)Z

    iput-boolean v3, p0, Lfqd;->r:Z

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-static {p0}, Lbox;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfqd;->p:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v0, "com.google.android.gms.common.acl.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfqd;->n:Ljava/lang/String;

    const-string v0, "com.google.android.gms.common.acl.EXTRA_PLUS_PAGE_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfqd;->o:Ljava/lang/String;

    const-string v0, "com.google.android.gms.common.acl.EXTRA_CLIENT_APPLICATION_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfqd;->q:Ljava/lang/String;

    const-string v0, "SHOW_CANCEL_VISIBLE"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lfqd;->u:Z

    const-string v0, "OK_TEXT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfqd;->v:Ljava/lang/String;

    iget-object v0, p0, Lfqd;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "AudienceSelectionActivi"

    const-string v1, "Account name must not be empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v5, v4}, Lfqd;->a(ILandroid/content/Intent;)V

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {p0, v5, v4}, Lfqd;->a(ILandroid/content/Intent;)V

    goto :goto_1

    :cond_3
    if-nez p1, :cond_4

    invoke-direct {p0}, Lfqd;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lbdd;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lfqd;->h()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lfqd;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_4
    const v0, 0x7f0400c0    # com.google.android.gms.R.layout.plus_audience_selection_activity

    invoke-virtual {p0, v0}, Lfqd;->setContentView(I)V

    iget-boolean v0, p0, Lfqd;->r:Z

    if-eqz v0, :cond_6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v7, :cond_6

    invoke-virtual {p0}, Lfqd;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v3, 0x7f0400cc    # com.google.android.gms.R.layout.plus_audience_selection_title_custom

    invoke-virtual {v0, v6, v3}, Landroid/view/Window;->setFeatureInt(II)V

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p0, v0}, Lfqd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_5

    const v3, 0x106000b    # android.R.color.white

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_5
    const v0, 0x7f0a0259    # com.google.android.gms.R.id.plus_auth_title

    invoke-virtual {p0, v0}, Lfqd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    const-wide v3, 0x3fe570a3d70a3d71L    # 0.67

    invoke-static {p0, v3, v4}, Lbow;->a(Landroid/app/Activity;D)V

    const v0, 0x7f0a025c    # com.google.android.gms.R.id.ok

    invoke-virtual {p0, v0}, Lfqd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lfqd;->v:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lfqd;->v:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    invoke-direct {p0, v1}, Lfqd;->b(Landroid/content/Intent;)V

    if-nez p1, :cond_9

    iput-boolean v5, p0, Lfqd;->t:Z

    new-instance v0, Lbkw;

    invoke-direct {v0}, Lbkw;-><init>()V

    invoke-virtual {p0}, Lfqd;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lbew;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v0

    invoke-virtual {p0}, Lfqd;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "com.google.android.gms.common.acl.EXTRA_DOMAIN_RESTRICTED"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbkw;->a(I)Lbkw;

    move-result-object v0

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    new-instance v1, Lfrb;

    invoke-direct {v1, v0}, Lfrb;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iput-object v1, p0, Lfqd;->s:Lfrb;

    :goto_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0, v2}, Lfqd;->setTitle(Ljava/lang/CharSequence;)V

    :cond_8
    :goto_3
    invoke-virtual {p0}, Lfqd;->J_()Lak;

    move-result-object v0

    invoke-virtual {v0, v5, p0}, Lak;->a(ILal;)Lcb;

    invoke-virtual {p0, p1}, Lfqd;->a(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :cond_9
    const-string v0, "resolvingError"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lfqd;->t:Z

    new-instance v1, Lfrb;

    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    invoke-direct {v1, v0}, Lfrb;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iput-object v1, p0, Lfqd;->s:Lfrb;

    goto :goto_2

    :cond_a
    iget-boolean v0, p0, Lfqd;->r:Z

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lfqd;->e()I

    move-result v0

    invoke-virtual {p0, v0}, Lfqd;->setTitle(I)V

    goto :goto_3
.end method

.method protected onPostResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lo;->onPostResume()V

    iget-boolean v0, p0, Lfqd;->t:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfqd;->J_()Lak;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    invoke-virtual {p0}, Lfqd;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lfqd;->b(Landroid/content/Intent;)V

    iput-boolean v2, p0, Lfqd;->t:Z

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "audience"

    iget-object v1, p0, Lfqd;->s:Lfrb;

    iget-object v1, v1, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "resolvingError"

    iget-boolean v1, p0, Lfqd;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setTitle(I)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p0, v0}, Lfqd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lo;->setTitle(I)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p0, v0}, Lfqd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lo;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
