.class public final Lfff;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Landroid/os/Bundle;

.field public static b:Landroid/os/Bundle;

.field private static c:Ljava/util/Locale;


# direct methods
.method public static a(Landroid/content/Context;)V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Lfff;->c:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Email;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v4, v3}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    invoke-static {v2, v6, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Email;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v6, v2}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    sput-object v1, Lfff;->a:Landroid/os/Bundle;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v4, v3}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    invoke-static {v2, v7, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v6, v3}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    invoke-static {v2, v6, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/4 v3, 0x5

    invoke-static {v2, v3, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v8, v3}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/4 v3, 0x5

    invoke-static {v2, v8, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/4 v3, 0x6

    const/16 v4, 0xd

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/4 v3, 0x7

    const/4 v4, 0x6

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x8

    const/16 v4, 0xa

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x9

    const/16 v4, 0x13

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xa

    const/16 v4, 0x9

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xb

    const/16 v4, 0xe

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xc

    const/16 v4, 0xb

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xd

    const/16 v4, 0x8

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xe

    const/16 v4, 0xf

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xf

    const/16 v4, 0x10

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x10

    const/16 v4, 0x11

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x11

    const/16 v4, 0x12

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x12

    const/16 v4, 0xc

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x13

    const v4, 0x7f0b01c3    # com.google.android.gms.R.string.people_google_voice_phone_label

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lfff;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    sput-object v1, Lfff;->b:Landroid/os/Bundle;

    const-string v1, "PeopleService"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "DataTypeLabelMap"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "email types="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lfff;->a:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "DataTypeLabelMap"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "phone types="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lfff;->b:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    sput-object v0, Lfff;->c:Ljava/util/Locale;

    goto/16 :goto_0
.end method

.method private static a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V
    .locals 2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
