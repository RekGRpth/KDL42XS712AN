.class public final Lfto;
.super Lbje;
.source "SourceFile"

# interfaces
.implements Lbbq;


# instance fields
.field private f:Lfxl;

.field private final g:Lcom/google/android/gms/plus/internal/PlusSession;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;Lcom/google/android/gms/plus/internal/PlusSession;)V
    .locals 6

    invoke-virtual {p5}, Lcom/google/android/gms/plus/internal/PlusSession;->c()[Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbje;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;[Ljava/lang/String;)V

    iput-object p5, p0, Lfto;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lftm;->a(Landroid/os/IBinder;)Lftl;

    move-result-object v0

    return-object v0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "loaded_person"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "loaded_person"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->b([B)Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    move-result-object v0

    iput-object v0, p0, Lfto;->f:Lfxl;

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lbje;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method protected final a(Lbjy;Lbjj;)V
    .locals 8

    iget-object v0, p0, Lfto;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->k()Landroid/os/Bundle;

    move-result-object v7

    const-string v0, "request_visible_actions"

    iget-object v1, p0, Lfto;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/PlusSession;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const v2, 0x41fe88

    iget-object v0, p0, Lfto;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lfto;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lbje;->c:[Ljava/lang/String;

    iget-object v0, p0, Lfto;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->b()Ljava/lang/String;

    move-result-object v6

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lbjy;->a(Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.service.START"

    return-object v0
.end method

.method protected final c_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.internal.IPlusService"

    return-object v0
.end method
