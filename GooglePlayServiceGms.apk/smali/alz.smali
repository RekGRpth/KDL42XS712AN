.class public final Lalz;
.super Lalu;
.source "SourceFile"


# instance fields
.field private final b:Laku;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:[B


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;ILjava/lang/String;[B)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "ResolveStateOp"

    invoke-direct {p0, v0, p1}, Lalu;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Lalz;->b:Laku;

    iput-object p3, p0, Lalz;->c:Ljava/lang/String;

    iput p4, p0, Lalz;->d:I

    iput-object p5, p0, Lalz;->e:Ljava/lang/String;

    if-nez p6, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lalz;->f:[B

    :goto_0
    return-void

    :cond_0
    array-length v0, p6

    new-array v0, v0, [B

    iput-object v0, p0, Lalz;->f:[B

    iget-object v0, p0, Lalz;->f:[B

    array-length v1, p6

    invoke-static {p6, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    iget-object v0, p0, Lalz;->b:Laku;

    iget v1, p0, Lalz;->d:I

    invoke-interface {v0, v1, p1}, Laku;->a(ILcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lakk;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    iget-object v2, p0, Lalz;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lalz;->c:Ljava/lang/String;

    iget v4, p0, Lalz;->d:I

    iget-object v5, p0, Lalz;->e:Ljava/lang/String;

    iget-object v6, p0, Lalz;->f:[B

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lakk;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
