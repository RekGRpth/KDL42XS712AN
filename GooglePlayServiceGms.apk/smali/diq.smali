.class public final enum Ldiq;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Ldiq;

.field public static final enum B:Ldiq;

.field public static final enum C:Ldiq;

.field public static final enum D:Ldiq;

.field public static final enum E:Ldiq;

.field public static final enum F:Ldiq;

.field public static final enum G:Ldiq;

.field public static final enum H:Ldiq;

.field public static final enum I:Ldiq;

.field public static final enum J:Ldiq;

.field public static final enum K:Ldiq;

.field public static final enum L:Ldiq;

.field public static final enum M:Ldiq;

.field public static final enum N:Ldiq;

.field public static final enum O:Ldiq;

.field public static final enum P:Ldiq;

.field public static final enum Q:Ldiq;

.field public static final enum R:Ldiq;

.field public static final enum S:Ldiq;

.field public static final enum T:Ldiq;

.field public static final enum U:Ldiq;

.field public static final enum V:Ldiq;

.field public static final enum W:Ldiq;

.field public static final enum X:Ldiq;

.field public static final enum Y:Ldiq;

.field public static final enum Z:Ldiq;

.field public static final enum a:Ldiq;

.field public static final enum aA:Ldiq;

.field public static final enum aB:Ldiq;

.field public static final enum aC:Ldiq;

.field public static final enum aD:Ldiq;

.field public static final enum aE:Ldiq;

.field public static final enum aF:Ldiq;

.field public static final enum aG:Ldiq;

.field public static final enum aH:Ldiq;

.field public static final enum aI:Ldiq;

.field public static final enum aJ:Ldiq;

.field public static final enum aK:Ldiq;

.field public static final enum aL:Ldiq;

.field private static final synthetic aM:[Ldiq;

.field public static final enum aa:Ldiq;

.field public static final enum ab:Ldiq;

.field public static final enum ac:Ldiq;

.field public static final enum ad:Ldiq;

.field public static final enum ae:Ldiq;

.field public static final enum af:Ldiq;

.field public static final enum ag:Ldiq;

.field public static final enum ah:Ldiq;

.field public static final enum ai:Ldiq;

.field public static final enum aj:Ldiq;

.field public static final enum ak:Ldiq;

.field public static final enum al:Ldiq;

.field public static final enum am:Ldiq;

.field public static final enum an:Ldiq;

.field public static final enum ao:Ldiq;

.field public static final enum ap:Ldiq;

.field public static final enum aq:Ldiq;

.field public static final enum ar:Ldiq;

.field public static final enum as:Ldiq;

.field public static final enum at:Ldiq;

.field public static final enum au:Ldiq;

.field public static final enum av:Ldiq;

.field public static final enum aw:Ldiq;

.field public static final enum ax:Ldiq;

.field public static final enum ay:Ldiq;

.field public static final enum az:Ldiq;

.field public static final enum b:Ldiq;

.field public static final enum c:Ldiq;

.field public static final enum d:Ldiq;

.field public static final enum e:Ldiq;

.field public static final enum f:Ldiq;

.field public static final enum g:Ldiq;

.field public static final enum h:Ldiq;

.field public static final enum i:Ldiq;

.field public static final enum j:Ldiq;

.field public static final enum k:Ldiq;

.field public static final enum l:Ldiq;

.field public static final enum m:Ldiq;

.field public static final enum n:Ldiq;

.field public static final enum o:Ldiq;

.field public static final enum p:Ldiq;

.field public static final enum q:Ldiq;

.field public static final enum r:Ldiq;

.field public static final enum s:Ldiq;

.field public static final enum t:Ldiq;

.field public static final enum u:Ldiq;

.field public static final enum v:Ldiq;

.field public static final enum w:Ldiq;

.field public static final enum x:Ldiq;

.field public static final enum y:Ldiq;

.field public static final enum z:Ldiq;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Ldiq;

    const-string v1, "GAMES"

    const-string v2, "games/*"

    invoke-direct {v0, v1, v4, v2}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->a:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAMES_ID"

    const-string v2, "games/*/#"

    invoke-direct {v0, v1, v5, v2}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->b:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAMES_EXT_GAME_ID"

    const-string v2, "games/*/ext_game/*"

    invoke-direct {v0, v1, v6, v2}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->c:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAMES_ID_ICON_IMAGE"

    const-string v2, "games/*/#/icon_image"

    invoke-direct {v0, v1, v7, v2}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->d:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAMES_EXT_GAME_ID_ICON_IMAGE"

    const-string v2, "games/*/ext_game/*/icon_image"

    invoke-direct {v0, v1, v8, v2}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->e:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAMES_ID_HI_RES_IMAGE"

    const/4 v2, 0x5

    const-string v3, "games/*/#/hi_res_image"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->f:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAMES_ID_FEATURED_IMAGE"

    const/4 v2, 0x6

    const-string v3, "games/*/#/featured_image"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->g:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_BADGES"

    const/4 v2, 0x7

    const-string v3, "game_badges/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->h:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_BADGES_ID"

    const/16 v2, 0x8

    const-string v3, "game_badges/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->i:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_BADGES_GAME_ID"

    const/16 v2, 0x9

    const-string v3, "game_badges/*/game/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->j:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_BADGES_EXT_GAME_ID"

    const/16 v2, 0xa

    const-string v3, "game_badges/*/ext_game/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->k:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_BADGES_ICON_IMAGE"

    const/16 v2, 0xb

    const-string v3, "game_badges/*/#/icon_image"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->l:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_INSTANCES"

    const/16 v2, 0xc

    const-string v3, "game_instances/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->m:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_INSTANCES_ID"

    const/16 v2, 0xd

    const-string v3, "game_instances/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->n:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_INSTANCES_GAME_ID"

    const/16 v2, 0xe

    const-string v3, "game_instances/*/game/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->o:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "GAME_INSTANCES_EXT_GAME_ID"

    const/16 v2, 0xf

    const-string v3, "game_instances/*/ext_game/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->p:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PLAYERS"

    const/16 v2, 0x10

    const-string v3, "players/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->q:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PLAYERS_ID"

    const/16 v2, 0x11

    const-string v3, "players/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->r:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PLAYERS_EXT_PLAYER_ID"

    const/16 v2, 0x12

    const-string v3, "players/*/ext_player/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->s:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PLAYERS_ID_PROFILE_ICON_IMAGE"

    const/16 v2, 0x13

    const-string v3, "players/*/#/profile_icon_image"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->t:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PLAYERS_ID_PROFILE_HI_RES_IMAGE"

    const/16 v2, 0x14

    const-string v3, "players/*/#/profile_hi_res_image"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->u:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "RECENT_PLAYERS"

    const/16 v2, 0x15

    const-string v3, "recent_players/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->v:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_DEFINITIONS"

    const/16 v2, 0x16

    const-string v3, "achievement_definitions/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->w:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_DEFINITIONS_ID"

    const/16 v2, 0x17

    const-string v3, "achievement_definitions/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->x:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_DEFINITIONS_EXT_ACHIEVEMENT_ID"

    const/16 v2, 0x18

    const-string v3, "achievement_definitions/*/ext_achievement/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->y:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_DEFINITIONS_GAME_ID"

    const/16 v2, 0x19

    const-string v3, "achievement_definitions/*/game/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->z:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_DEFINITIONS_EXT_GAME_ID"

    const/16 v2, 0x1a

    const-string v3, "achievement_definitions/*/ext_game/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->A:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_DEFINITIONS_ID_UNLOCKED_IMAGE"

    const/16 v2, 0x1b

    const-string v3, "achievement_definitions/*/#/unlocked_icon_image"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->B:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_DEFINITIONS_ID_REVEALED_IMAGE"

    const/16 v2, 0x1c

    const-string v3, "achievement_definitions/*/#/revealed_icon_image"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->C:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_PENDING_OPS"

    const/16 v2, 0x1d

    const-string v3, "achievement_pending_ops/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->D:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_PENDING_OPS_ID"

    const/16 v2, 0x1e

    const-string v3, "achievement_pending_ops/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->E:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_INSTANCES"

    const/16 v2, 0x1f

    const-string v3, "achievement_instances/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->F:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_INSTANCES_ID"

    const/16 v2, 0x20

    const-string v3, "achievement_instances/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->G:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_INSTANCES_PLAYER_ID"

    const/16 v2, 0x21

    const-string v3, "achievement_instances/*/player/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->H:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACHIEVEMENT_INSTANCES_EXT_PLAYER_ID"

    const/16 v2, 0x22

    const-string v3, "achievement_instances/*/ext_player/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->I:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "CLIENT_CONTEXT"

    const/16 v2, 0x23

    const-string v3, "client_contexts/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->J:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "CLIENT_CONTEXT_ID"

    const/16 v2, 0x24

    const-string v3, "client_contexts/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->K:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARDS"

    const/16 v2, 0x25

    const-string v3, "leaderboards/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->L:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARDS_ID"

    const/16 v2, 0x26

    const-string v3, "leaderboards/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->M:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARDS_EXT_LEADERBOARD_ID"

    const/16 v2, 0x27

    const-string v3, "leaderboards/*/ext_leaderboard/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->N:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARDS_GAME_ID"

    const/16 v2, 0x28

    const-string v3, "leaderboards/*/game/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->O:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARDS_EXT_GAME_ID"

    const/16 v2, 0x29

    const-string v3, "leaderboards/*/ext_game/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->P:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_INSTANCES"

    const/16 v2, 0x2a

    const-string v3, "leaderboard_instances/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->Q:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_INSTANCES_ID"

    const/16 v2, 0x2b

    const-string v3, "leaderboard_instances/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->R:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_INSTANCES_EXT_GAME_ID"

    const/16 v2, 0x2c

    const-string v3, "leaderboard_instances/*/ext_game/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->S:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_INSTANCES_EXT_LEADERBOARD_ID"

    const/16 v2, 0x2d

    const-string v3, "leaderboard_instances/*/ext_leaderboard/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->T:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_SCORES"

    const/16 v2, 0x2e

    const-string v3, "leaderboard_scores/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->U:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_SCORES_ID"

    const/16 v2, 0x2f

    const-string v3, "leaderboard_scores/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->V:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_SCORES_INSTANCE_ID"

    const/16 v2, 0x30

    const-string v3, "leaderboard_scores/*/instance/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->W:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_PENDING_SCORES"

    const/16 v2, 0x31

    const-string v3, "leaderboard_pending_scores/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->X:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "LEADERBOARD_PENDING_SCORES_ID"

    const/16 v2, 0x32

    const-string v3, "leaderboard_pending_scores/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->Y:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "IMAGES"

    const/16 v2, 0x33

    const-string v3, "images/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->Z:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "IMAGES_ID"

    const/16 v2, 0x34

    const-string v3, "images/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aa:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "INVITATIONS"

    const/16 v2, 0x35

    const-string v3, "invitations/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ab:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "INVITATIONS_ID"

    const/16 v2, 0x36

    const-string v3, "invitations/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ac:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "INVITATIONS_RAW_ID"

    const/16 v2, 0x37

    const-string v3, "invitations/*/raw_id/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ad:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "INVITATIONS_EXT_INVITATION_ID"

    const/16 v2, 0x38

    const-string v3, "invitations/*/ext_invitation/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ae:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUESTS"

    const/16 v2, 0x39

    const-string v3, "requests/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->af:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUESTS_ID"

    const/16 v2, 0x3a

    const-string v3, "requests/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ag:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUESTS_RAW_ID"

    const/16 v2, 0x3b

    const-string v3, "requests/*/raw_id/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ah:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUESTS_EXT_REQ_ID"

    const/16 v2, 0x3c

    const-string v3, "requests/*/ext_request/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ai:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUEST_RECIPIENTS"

    const/16 v2, 0x3d

    const-string v3, "request_recipients/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aj:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUEST_RECIPIENTS_ID"

    const/16 v2, 0x3e

    const-string v3, "request_recipients/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ak:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUEST_RECIPIENTS_REQ_ID"

    const/16 v2, 0x3f

    const-string v3, "request_recipients/*/request/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->al:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUESTS_ENTITIES"

    const/16 v2, 0x40

    const-string v3, "request_entities/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->am:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUESTS_ENTITIES_REQUEST_ID"

    const/16 v2, 0x41

    const-string v3, "request_entities/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->an:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "REQUESTS_ENTITIES_EXT_REQUEST_ID"

    const/16 v2, 0x42

    const-string v3, "request_entities/*/ext_request/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ao:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES"

    const/16 v2, 0x43

    const-string v3, "matches/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ap:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES_ID"

    const/16 v2, 0x44

    const-string v3, "matches/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aq:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES_RAW_ID"

    const/16 v2, 0x45

    const-string v3, "matches/*/raw_id/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ar:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES_EXT_MATCH_ID"

    const/16 v2, 0x46

    const-string v3, "matches/*/ext_match/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->as:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES_GAME_ID"

    const/16 v2, 0x47

    const-string v3, "matches/*/game/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->at:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES_EXT_GAME_ID"

    const/16 v2, 0x48

    const-string v3, "matches/*/ext_game/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->au:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES_PENDING_OPS"

    const/16 v2, 0x49

    const-string v3, "matches_pending_ops/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->av:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES_PENDING_OPS_ID"

    const/16 v2, 0x4a

    const-string v3, "matches_pending_ops/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aw:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCHES_PENDING_OPS_EXT_MATCH_ID"

    const/16 v2, 0x4b

    const-string v3, "matches_pending_ops/*/ext_match/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ax:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCH_ENTITIES"

    const/16 v2, 0x4c

    const-string v3, "match_entities/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->ay:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCH_ENTITIES_MATCH_ID"

    const/16 v2, 0x4d

    const-string v3, "match_entities/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->az:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "MATCH_ENTITIES_EXT_MATCH_ID"

    const/16 v2, 0x4e

    const-string v3, "match_entities/*/ext_match/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aA:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "NOTIFICATIONS"

    const/16 v2, 0x4f

    const-string v3, "notifications/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aB:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "NOTIFICATIONS_ID"

    const/16 v2, 0x50

    const-string v3, "notifications/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aC:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "NOTIFICATIONS_GAME_ID"

    const/16 v2, 0x51

    const-string v3, "notifications/*/game/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aD:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "NOTIFICATIONS_EXT_GAME_ID"

    const/16 v2, 0x52

    const-string v3, "notifications/*/ext_game/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aE:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PARTICIPANTS"

    const/16 v2, 0x53

    const-string v3, "participants/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aF:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PARTICIPANTS_ID"

    const/16 v2, 0x54

    const-string v3, "participants/*/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aG:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PARTICIPANTS_EXT_PARTICIPANT_ID"

    const/16 v2, 0x55

    const-string v3, "participants/*/ext_participant/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aH:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PARTICIPANTS_MATCH_ID"

    const/16 v2, 0x56

    const-string v3, "participants/*/match/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aI:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "PARTICIPANTS_INVITATION_ID"

    const/16 v2, 0x57

    const-string v3, "participants/*/invitation/#"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aJ:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "ACCOUNT_METADATA"

    const/16 v2, 0x58

    const-string v3, "account_metadata/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aK:Ldiq;

    new-instance v0, Ldiq;

    const-string v1, "CLEAR_DATA_FOR_LOCALE_CHANGE"

    const/16 v2, 0x59

    const-string v3, "clear_for_locale_change/*"

    invoke-direct {v0, v1, v2, v3}, Ldiq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldiq;->aL:Ldiq;

    const/16 v0, 0x5a

    new-array v0, v0, [Ldiq;

    sget-object v1, Ldiq;->a:Ldiq;

    aput-object v1, v0, v4

    sget-object v1, Ldiq;->b:Ldiq;

    aput-object v1, v0, v5

    sget-object v1, Ldiq;->c:Ldiq;

    aput-object v1, v0, v6

    sget-object v1, Ldiq;->d:Ldiq;

    aput-object v1, v0, v7

    sget-object v1, Ldiq;->e:Ldiq;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Ldiq;->f:Ldiq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldiq;->g:Ldiq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldiq;->h:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldiq;->i:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ldiq;->j:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Ldiq;->k:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Ldiq;->l:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Ldiq;->m:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Ldiq;->n:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Ldiq;->o:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Ldiq;->p:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Ldiq;->q:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Ldiq;->r:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Ldiq;->s:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Ldiq;->t:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Ldiq;->u:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Ldiq;->v:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Ldiq;->w:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Ldiq;->x:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Ldiq;->y:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Ldiq;->z:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Ldiq;->A:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Ldiq;->B:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Ldiq;->C:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Ldiq;->D:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Ldiq;->E:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Ldiq;->F:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Ldiq;->G:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Ldiq;->H:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Ldiq;->I:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Ldiq;->J:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Ldiq;->K:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Ldiq;->L:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Ldiq;->M:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Ldiq;->N:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Ldiq;->O:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Ldiq;->P:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Ldiq;->Q:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Ldiq;->R:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Ldiq;->S:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Ldiq;->T:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Ldiq;->U:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Ldiq;->V:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Ldiq;->W:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Ldiq;->X:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Ldiq;->Y:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Ldiq;->Z:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Ldiq;->aa:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Ldiq;->ab:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Ldiq;->ac:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Ldiq;->ad:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Ldiq;->ae:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Ldiq;->af:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Ldiq;->ag:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Ldiq;->ah:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Ldiq;->ai:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Ldiq;->aj:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Ldiq;->ak:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Ldiq;->al:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Ldiq;->am:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Ldiq;->an:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Ldiq;->ao:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Ldiq;->ap:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Ldiq;->aq:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Ldiq;->ar:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Ldiq;->as:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Ldiq;->at:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Ldiq;->au:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Ldiq;->av:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Ldiq;->aw:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Ldiq;->ax:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Ldiq;->ay:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Ldiq;->az:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Ldiq;->aA:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Ldiq;->aB:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Ldiq;->aC:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Ldiq;->aD:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Ldiq;->aE:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Ldiq;->aF:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Ldiq;->aG:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Ldiq;->aH:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Ldiq;->aI:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Ldiq;->aJ:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Ldiq;->aK:Ldiq;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Ldiq;->aL:Ldiq;

    aput-object v2, v0, v1

    sput-object v0, Ldiq;->aM:[Ldiq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-static {}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b()Landroid/content/UriMatcher;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.background"

    invoke-virtual {p0}, Ldiq;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, p3, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldiq;
    .locals 1

    const-class v0, Ldiq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldiq;

    return-object v0
.end method

.method public static values()[Ldiq;
    .locals 1

    sget-object v0, Ldiq;->aM:[Ldiq;

    invoke-virtual {v0}, [Ldiq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldiq;

    return-object v0
.end method
