.class public final Lbmx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbmz;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/String;

.field protected final d:Landroid/os/Bundle;

.field protected e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Z)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmx;->b:Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbmx;->d:Landroid/os/Bundle;

    iget-object v0, p0, Lbmx;->d:Landroid/os/Bundle;

    sget-object v1, Lamr;->b:Ljava/lang/String;

    iget-object v2, p0, Lbmx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbmx;->d:Landroid/os/Bundle;

    const-string v1, "callerUid"

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->j()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbmx;->d:Landroid/os/Bundle;

    const-string v1, "request_visible_actions"

    const-string v2, " "

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->j()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lbmx;->d:Landroid/os/Bundle;

    const-string v1, "suppressProgressScreen"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbmx;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbmx;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lbmx;->a:Ljava/lang/String;

    iget-object v2, p0, Lbmx;->c:Ljava/lang/String;

    iget-object v3, p0, Lbmx;->d:Landroid/os/Bundle;

    invoke-static {p1, v1, v2, v3}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbmx;->e:Ljava/lang/String;

    invoke-static {}, Lbmy;->a()Lbmy;

    move-result-object v1

    iget-object v2, p0, Lbmx;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbmy;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lbmx;->e:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "AuthSessionAuthenticato"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "AuthSessionAuthenticato"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lamq;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lbmx;->a:Ljava/lang/String;

    iget-object v1, p0, Lbmx;->c:Ljava/lang/String;

    iget-object v2, p0, Lbmx;->d:Landroid/os/Bundle;

    invoke-static {p1, v0, v1, v2}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmx;->e:Ljava/lang/String;

    invoke-static {}, Lbmy;->a()Lbmy;

    move-result-object v0

    iget-object v1, p0, Lbmx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbmy;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lbmx;->e:Ljava/lang/String;

    return-object v0
.end method
