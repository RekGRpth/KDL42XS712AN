.class public final Lgih;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field public b:Ljava/util/List;

.field public final c:Ljava/util/Set;

.field private d:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;

.field private e:Ljava/util/List;

.field private f:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lgih;->c:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Lgig;
    .locals 7

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iget-object v1, p0, Lgih;->c:Ljava/util/Set;

    iget-object v2, p0, Lgih;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;

    iget-object v3, p0, Lgih;->e:Ljava/util/List;

    iget-object v4, p0, Lgih;->a:Ljava/util/List;

    iget-object v5, p0, Lgih;->b:Ljava/util/List;

    iget-object v6, p0, Lgih;->f:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;)V

    return-object v0
.end method

.method public final a(Lgid;)Lgih;
    .locals 2

    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;

    iput-object p1, p0, Lgih;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;

    iget-object v0, p0, Lgih;->c:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lgip;)Lgih;
    .locals 2

    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;

    iput-object p1, p0, Lgih;->f:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;

    iget-object v0, p0, Lgih;->c:Ljava/util/Set;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lgih;
    .locals 2

    iput-object p1, p0, Lgih;->e:Ljava/util/List;

    iget-object v0, p0, Lgih;->c:Ljava/util/Set;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
