.class public final Lst;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lrw;


# static fields
.field protected static final a:Z

.field private static d:I

.field private static e:I


# instance fields
.field protected final b:Ltc;

.field protected final c:Lsu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lsq;->b:Z

    sput-boolean v0, Lst;->a:Z

    const/16 v0, 0xbb8

    sput v0, Lst;->d:I

    const/16 v0, 0x1000

    sput v0, Lst;->e:I

    return-void
.end method

.method public constructor <init>(Ltc;)V
    .locals 2

    new-instance v0, Lsu;

    sget v1, Lst;->e:I

    invoke-direct {v0, v1}, Lsu;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lst;-><init>(Ltc;Lsu;)V

    return-void
.end method

.method private constructor <init>(Ltc;Lsu;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lst;->b:Ltc;

    iput-object p2, p0, Lst;->c:Lsu;

    return-void
.end method

.method private static a([Lorg/apache/http/Header;)Ljava/util/Map;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    aget-object v2, p0, v0

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v2

    aget-object v3, p0, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static a(Ljava/lang/String;Lsc;Lsp;)V
    .locals 6

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lsc;->q()Lsm;

    move-result-object v0

    invoke-virtual {p1}, Lsc;->p()I

    move-result v1

    :try_start_0
    invoke-interface {v0, p2}, Lsm;->a(Lsp;)V
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    const-string v0, "%s-retry [timeout=%s]"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p0, v2, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lsc;->a(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    const-string v2, "%s-timeout-giveup [timeout=%s]"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lsc;->a(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lorg/apache/http/HttpEntity;)[B
    .locals 6

    const/4 v5, 0x0

    new-instance v2, Ltg;

    iget-object v0, p0, Lst;->c:Lsu;

    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    long-to-int v1, v3

    invoke-direct {v2, v0, v1}, Ltg;-><init>(Lsu;I)V

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lsn;

    invoke-direct {v0}, Lsn;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_1
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    iget-object v3, p0, Lst;->c:Lsu;

    invoke-virtual {v3, v1}, Lsu;->a([B)V

    invoke-virtual {v2}, Ltg;->close()V

    throw v0

    :cond_0
    :try_start_2
    iget-object v3, p0, Lst;->c:Lsu;

    const/16 v4, 0x400

    invoke-virtual {v3, v4}, Lsu;->a(I)[B

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4, v3}, Ltg;->write([BII)V

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ltg;->toByteArray()[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :try_start_3
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_2
    iget-object v3, p0, Lst;->c:Lsu;

    invoke-virtual {v3, v1}, Lsu;->a([B)V

    invoke-virtual {v2}, Ltg;->close()V

    return-object v0

    :catch_0
    move-exception v3

    const-string v3, "Error occured when calling consumingContent"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lsq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :catch_1
    move-exception v3

    const-string v3, "Error occured when calling consumingContent"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lsq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lsc;)Lrz;
    .locals 12

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    :goto_0
    const/4 v3, 0x0

    const/4 v2, 0x0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lsc;->f()Lrp;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v7, v4, Lrp;->b:Ljava/lang/String;

    if-eqz v7, :cond_0

    const-string v7, "If-None-Match"

    iget-object v8, v4, Lrp;->b:Ljava/lang/String;

    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-wide v7, v4, Lrp;->c:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_1

    new-instance v7, Ljava/util/Date;

    iget-wide v8, v4, Lrp;->c:J

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    const-string v4, "If-Modified-Since"

    invoke-static {v7}, Lorg/apache/http/impl/cookie/DateUtils;->formatDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v4, p0, Lst;->b:Ltc;

    invoke-interface {v4, p1, v0}, Ltc;->a(Lsc;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    invoke-static {v0}, Lst;->a([Lorg/apache/http/Header;)Ljava/util/Map;

    move-result-object v1

    const/16 v0, 0x130

    if-ne v7, v0, :cond_3

    new-instance v0, Lrz;

    const/16 v7, 0x130

    invoke-virtual {p1}, Lsc;->f()Lrp;

    move-result-object v4

    if-nez v4, :cond_2

    const/4 v4, 0x0

    :goto_1
    const/4 v8, 0x1

    invoke-direct {v0, v7, v4, v1, v8}, Lrz;-><init>(I[BLjava/util/Map;Z)V

    :goto_2
    return-object v0

    :cond_2
    invoke-virtual {p1}, Lsc;->f()Lrp;

    move-result-object v4

    iget-object v4, v4, Lrp;->a:[B

    goto :goto_1

    :cond_3
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-direct {p0, v0}, Lst;->a(Lorg/apache/http/HttpEntity;)[B

    move-result-object v2

    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sub-long/2addr v8, v5

    sget-boolean v0, Lst;->a:Z

    if-nez v0, :cond_4

    sget v0, Lst;->d:I

    int-to-long v10, v0

    cmp-long v0, v8, v10

    if-lez v0, :cond_5

    :cond_4
    const-string v10, "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]"

    const/4 v0, 0x5

    new-array v11, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, v11, v0

    const/4 v0, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v11, v0

    const/4 v8, 0x2

    if-eqz v2, :cond_8

    array-length v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_4
    aput-object v0, v11, v8

    const/4 v0, 0x3

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v11, v0

    const/4 v0, 0x4

    invoke-virtual {p1}, Lsc;->q()Lsm;

    move-result-object v4

    invoke-interface {v4}, Lsm;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v11, v0

    invoke-static {v10, v11}, Lsq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    const/16 v0, 0xc8

    if-lt v7, v0, :cond_6

    const/16 v0, 0x12b

    if-le v7, v0, :cond_9

    :cond_6
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    :catch_0
    move-exception v0

    const-string v0, "socket"

    new-instance v1, Lso;

    invoke-direct {v1}, Lso;-><init>()V

    invoke-static {v0, p1, v1}, Lst;->a(Ljava/lang/String;Lsc;Lsp;)V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    :try_start_1
    new-array v2, v0, [B

    goto :goto_3

    :cond_8
    const-string v0, "null"

    goto :goto_4

    :cond_9
    new-instance v0, Lrz;

    const/4 v4, 0x0

    invoke-direct {v0, v7, v2, v1, v4}, Lrz;-><init>(I[BLjava/util/Map;Z)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_2

    :catch_1
    move-exception v0

    const-string v0, "connection"

    new-instance v1, Lso;

    invoke-direct {v1}, Lso;-><init>()V

    invoke-static {v0, p1, v1}, Lst;->a(Ljava/lang/String;Lsc;Lsp;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad URL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lsc;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    if-eqz v3, :cond_b

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    const-string v3, "Unexpected response code %d for %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x1

    invoke-virtual {p1}, Lsc;->d()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-static {v3, v4}, Lsq;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v2, :cond_d

    new-instance v3, Lrz;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v2, v1, v4}, Lrz;-><init>(I[BLjava/util/Map;Z)V

    const/16 v1, 0x191

    if-eq v0, v1, :cond_a

    const/16 v1, 0x193

    if-ne v0, v1, :cond_c

    :cond_a
    const-string v0, "auth"

    new-instance v1, Lrn;

    invoke-direct {v1, v3}, Lrn;-><init>(Lrz;)V

    invoke-static {v0, p1, v1}, Lst;->a(Ljava/lang/String;Lsc;Lsp;)V

    goto/16 :goto_0

    :cond_b
    new-instance v1, Lsa;

    invoke-direct {v1, v0}, Lsa;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_c
    new-instance v0, Lsn;

    invoke-direct {v0, v3}, Lsn;-><init>(Lrz;)V

    throw v0

    :cond_d
    new-instance v0, Lry;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lry;-><init>(B)V

    throw v0
.end method
