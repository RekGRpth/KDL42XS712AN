.class public final Lff;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lfi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Lfh;

    invoke-direct {v0}, Lfh;-><init>()V

    sput-object v0, Lff;->a:Lfi;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lfg;

    invoke-direct {v0}, Lfg;-><init>()V

    sput-object v0, Lff;->a:Lfi;

    goto :goto_0
.end method

.method public static a(Landroid/view/VelocityTracker;I)F
    .locals 1

    sget-object v0, Lff;->a:Lfi;

    invoke-interface {v0, p0, p1}, Lfi;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/VelocityTracker;I)F
    .locals 1

    sget-object v0, Lff;->a:Lfi;

    invoke-interface {v0, p0, p1}, Lfi;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    return v0
.end method
