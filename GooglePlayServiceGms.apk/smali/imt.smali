.class public final Limt;
.super Lorg/apache/http/impl/conn/tsccm/ConnPoolByRoute;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/apache/http/conn/ClientConnectionOperator;Lorg/apache/http/params/HttpParams;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/conn/tsccm/ConnPoolByRoute;-><init>(Lorg/apache/http/conn/ClientConnectionOperator;Lorg/apache/http/params/HttpParams;)V

    return-void
.end method

.method static synthetic a(Limt;)Ljava/util/concurrent/locks/Lock;
    .locals 1

    iget-object v0, p0, Limt;->poolLock:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method static synthetic b(Limt;)Ljava/util/concurrent/locks/Lock;
    .locals 1

    iget-object v0, p0, Limt;->poolLock:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method


# virtual methods
.method protected final getEntryBlocking(Lorg/apache/http/conn/routing/HttpRoute;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;
    .locals 14

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    cmp-long v4, p3, v4

    if-lez v4, :cond_0

    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p5

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    :cond_0
    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-object v4, p0, Limt;->poolLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v4}, Limt;->getRoutePool(Lorg/apache/http/conn/routing/HttpRoute;Z)Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;

    move-result-object v9

    const/4 v4, 0x0

    move-object v6, v4

    move-object v4, v5

    :goto_0
    if-nez v4, :cond_7

    iget-boolean v4, p0, Limt;->isShutDown:Z

    if-eqz v4, :cond_2

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Connection pool shut down."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v3

    iget-object v4, p0, Limt;->poolLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v9, v4, v7

    const-wide/16 v11, 0xa

    cmp-long v6, v9, v11

    if-lez v6, :cond_1

    const-string v6, "GetEntryBlocking() took %s ms"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sub-long/2addr v4, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-static {v6, v9}, Lsq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    throw v3

    :cond_2
    :try_start_1
    move-object/from16 v0, p2

    invoke-virtual {p0, v9, v0}, Limt;->getFreeEntry(Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;Ljava/lang/Object;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;

    move-result-object v4

    if-nez v4, :cond_7

    const-string v5, "Constructed new connection to route=[%s]"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    invoke-static {v5, v10}, Lsq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v9}, Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;->getCapacity()I

    move-result v5

    if-lez v5, :cond_3

    const/4 v5, 0x1

    :goto_1
    if-eqz v5, :cond_4

    iget v10, p0, Limt;->numConnections:I

    iget v11, p0, Limt;->maxTotalConnections:I

    if-ge v10, v11, :cond_4

    iget-object v4, p0, Limt;->operator:Lorg/apache/http/conn/ClientConnectionOperator;

    invoke-virtual {p0, v9, v4}, Limt;->createEntry(Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;Lorg/apache/http/conn/ClientConnectionOperator;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;

    move-result-object v4

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    :cond_4
    if-eqz v5, :cond_5

    iget-object v5, p0, Limt;->freeConnections:Ljava/util/Queue;

    invoke-interface {v5}, Ljava/util/Queue;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {p0}, Limt;->deleteLeastUsedEntry()V

    iget-object v4, p0, Limt;->operator:Lorg/apache/http/conn/ClientConnectionOperator;

    invoke-virtual {p0, v9, v4}, Limt;->createEntry(Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;Lorg/apache/http/conn/ClientConnectionOperator;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;

    move-result-object v4

    goto :goto_0

    :cond_5
    if-nez v6, :cond_9

    iget-object v5, p0, Limt;->poolLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v5

    invoke-virtual {p0, v5, v9}, Limt;->newWaitingThread(Ljava/util/concurrent/locks/Condition;Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;)Lorg/apache/http/impl/conn/tsccm/WaitingThread;

    move-result-object v5

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;->setWaitingThread(Lorg/apache/http/impl/conn/tsccm/WaitingThread;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    invoke-virtual {v9, v5}, Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;->queueThread(Lorg/apache/http/impl/conn/tsccm/WaitingThread;)V

    iget-object v6, p0, Limt;->waitingThreads:Ljava/util/Queue;

    invoke-interface {v6, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5, v3}, Lorg/apache/http/impl/conn/tsccm/WaitingThread;->await(Ljava/util/Date;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v6

    :try_start_3
    invoke-virtual {v9, v5}, Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;->removeThread(Lorg/apache/http/impl/conn/tsccm/WaitingThread;)V

    iget-object v10, p0, Limt;->waitingThreads:Ljava/util/Queue;

    invoke-interface {v10, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    if-nez v6, :cond_6

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    cmp-long v6, v10, v12

    if-gtz v6, :cond_6

    new-instance v3, Lorg/apache/http/conn/ConnectionPoolTimeoutException;

    const-string v4, "Timeout waiting for connection"

    invoke-direct {v3, v4}, Lorg/apache/http/conn/ConnectionPoolTimeoutException;-><init>(Ljava/lang/String;)V

    throw v3

    :catchall_1
    move-exception v3

    invoke-virtual {v9, v5}, Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;->removeThread(Lorg/apache/http/impl/conn/tsccm/WaitingThread;)V

    iget-object v4, p0, Limt;->waitingThreads:Ljava/util/Queue;

    invoke-interface {v4, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_6
    move-object v6, v5

    goto/16 :goto_0

    :cond_7
    move-object v3, v4

    iget-object v4, p0, Limt;->poolLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v9, v4, v7

    const-wide/16 v11, 0xa

    cmp-long v6, v9, v11

    if-lez v6, :cond_8

    const-string v6, "GetEntryBlocking() took %s ms"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sub-long/2addr v4, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-static {v6, v9}, Lsq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    return-object v3

    :cond_9
    move-object v5, v6

    goto :goto_2
.end method

.method public final requestPoolEntry(Lorg/apache/http/conn/routing/HttpRoute;Ljava/lang/Object;)Lorg/apache/http/impl/conn/tsccm/PoolEntryRequest;
    .locals 2

    new-instance v0, Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;

    invoke-direct {v0}, Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;-><init>()V

    new-instance v1, Limu;

    invoke-direct {v1, p0, v0, p1, p2}, Limu;-><init>(Limt;Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;Lorg/apache/http/conn/routing/HttpRoute;Ljava/lang/Object;)V

    return-object v1
.end method
