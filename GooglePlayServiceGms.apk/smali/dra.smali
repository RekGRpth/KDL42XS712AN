.class public final Ldra;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/service/GamesAndroidService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/GamesAndroidService;)V
    .locals 0

    iput-object p1, p0, Ldra;->a:Lcom/google/android/gms/games/service/GamesAndroidService;

    invoke-direct {p0}, Lbip;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 14

    const-string v1, "GamesService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "client connected with version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "invalid package name"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Ldra;->a:Lcom/google/android/gms/games/service/GamesAndroidService;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/GamesAndroidService;->a(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.key.isHeadless"

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    const-string v1, "com.google.android.gms.games.key.retryingSignIn"

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    const-string v1, "com.google.android.play.games"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v2, "com.google.android.gms.games.key.showConnectingPopup"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    const-string v1, "com.google.android.gms.games.key.connectingPopupGravity"

    const/16 v2, 0x11

    move-object/from16 v0, p9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    const-string v1, "com.google.android.gms.games.key.sdkVariant"

    const/16 v2, 0x1110

    move-object/from16 v0, p9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    iget-object v1, p0, Ldra;->a:Lcom/google/android/gms/games/service/GamesAndroidService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    move-object v2, p1

    move-object/from16 v4, p3

    move/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p5

    invoke-static/range {v1 .. v13}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lbjv;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZIZI)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
