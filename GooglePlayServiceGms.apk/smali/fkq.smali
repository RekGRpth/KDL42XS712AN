.class public final Lfkq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lflq;

.field private b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lfkr;Z)V
    .locals 7

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    const/4 v5, 0x0

    move v3, p2

    move-object v4, p3

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lfkq;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    new-instance v0, Lflq;

    new-instance v1, Lflo;

    invoke-direct {v1, p5}, Lflo;-><init>(Lfkr;)V

    invoke-direct {v0, p1, v1}, Lflq;-><init>(Landroid/content/Context;Lflo;)V

    iput-object v0, p0, Lfkq;->a:Lflq;

    return-void

    :catch_0
    move-exception v0

    const-string v0, "PlayLogger"

    const-string v3, "This can\'t happen."

    invoke-static {v0, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lfkr;)V
    .locals 7

    const/4 v3, 0x0

    const/16 v2, 0xc

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lfkq;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lfkr;Z)V

    return-void
.end method


# virtual methods
.method public final varargs a(Ljava/lang/String;[B[Ljava/lang/String;)V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v6, p0, Lfkq;->a:Lflq;

    iget-object v7, p0, Lfkq;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    new-instance v0, Lcom/google/android/gms/playlog/internal/LogEvent;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/playlog/internal/LogEvent;-><init>(JLjava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lflq;->a(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V

    return-void
.end method
