.class public final Ldtr;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldqy;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Landroid/os/Bundle;

.field private final f:[Ljava/lang/String;

.field private final g:Lcom/google/android/gms/games/internal/ConnectionInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldqy;Ljava/lang/String;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldtr;->b:Ldqy;

    iput-object p3, p0, Ldtr;->c:Ljava/lang/String;

    iput p4, p0, Ldtr;->d:I

    iput-object p5, p0, Ldtr;->f:[Ljava/lang/String;

    iput-object p6, p0, Ldtr;->e:Landroid/os/Bundle;

    iput-object p7, p0, Ldtr;->g:Lcom/google/android/gms/games/internal/ConnectionInfo;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Ldtr;->b:Ldqy;

    invoke-virtual {v0}, Ldqy;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Ldqy;->d:Ldqv;

    invoke-static {v1}, Ldqv;->b(Ldqv;)Ldrq;

    move-result-object v1

    iget-object v2, v0, Ldqy;->a:Ldad;

    iget-boolean v3, v0, Ldqy;->b:Z

    invoke-virtual {v1, v2, p1, v3}, Ldrq;->a(Ldad;Lcom/google/android/gms/common/data/DataHolder;Z)V

    :goto_0
    iget-object v0, v0, Ldqy;->a:Ldad;

    invoke-interface {v0, p1}, Ldad;->r(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_0
    return-void

    :cond_1
    :try_start_1
    const-string v1, "GamesService"

    const-string v2, "Failed to create a room"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Ldqy;->d:Ldqv;

    invoke-static {v1}, Ldqv;->c(Ldqv;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    iget-object v2, p0, Ldtr;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldtr;->c:Ljava/lang/String;

    iget v4, p0, Ldtr;->d:I

    iget-object v5, p0, Ldtr;->f:[Ljava/lang/String;

    iget-object v6, p0, Ldtr;->e:Landroid/os/Bundle;

    iget-object v7, p0, Ldtr;->g:Lcom/google/android/gms/games/internal/ConnectionInfo;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
