.class public final Lexh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lbfy;

.field public static final b:Lbfy;

.field public static final c:Lbfy;

.field public static final d:Lbfy;

.field public static final e:Lbfy;

.field public static final f:Lbfy;

.field public static final g:Lbfy;

.field public static final h:Lbfy;

.field public static final i:Lbfy;

.field public static final j:Lbfy;

.field public static final k:Lbfy;

.field public static final l:Lbfy;

.field public static final m:Lbfy;

.field public static final n:Lbfy;

.field public static final o:Lbfy;

.field public static final p:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "mdm.response_url"

    const-string v1, "https://android.googleapis.com/nova/remote_payload"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->a:Lbfy;

    const-string v0, "mdm.device_admin_state_url"

    const-string v1, "https://android.googleapis.com/nova/device_admin_state"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->b:Lbfy;

    const-string v0, "mdm.sitrep_url"

    const-string v1, "https://android.googleapis.com/nova/sitrep"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->c:Lbfy;

    const-string v0, "mdm.location_enabled_default"

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->d:Lbfy;

    const-string v0, "mdm.location_collection_duration_ms"

    const-wide/32 v1, 0xea60

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->e:Lbfy;

    const-string v0, "mdm.pre_wipe_location_timeout_ms"

    const-wide/16 v1, 0x2710

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->f:Lbfy;

    const-string v0, "mdm.location_collection_max_updates"

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->g:Lbfy;

    const-string v0, "mdm.location_accuracy_m"

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->h:Lbfy;

    const-string v0, "mdm.noise_timeout_ms"

    const-wide/32 v1, 0x493e0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->i:Lbfy;

    const-string v0, "mdm.tone_loop_interval_ms"

    const-wide/16 v1, 0x7d0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->j:Lbfy;

    const-string v0, "mdm.target_ringtone"

    const-string v1, "Orion"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->k:Lbfy;

    const-string v0, "mdm.smartlink_component"

    const-string v1, "google_settings_remote"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->l:Lbfy;

    const-string v0, "mdm.restrict_to_primary_user"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->m:Lbfy;

    const-string v0, "mdm.oauth_scope"

    const-string v1, "https://www.googleapis.com/auth/android_device_manager"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->n:Lbfy;

    const-string v0, "mdm.send_debug_info"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->o:Lbfy;

    const-string v0, "mdm.ostensible_gmail_domains"

    const-string v1, "@googlemail.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lexh;->p:Lbfy;

    return-void
.end method
