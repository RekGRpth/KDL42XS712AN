.class public final Ldgo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ldfu;

.field public final b:Ldgq;

.field public final c:Ldgq;

.field public final d:Ldgq;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-static {p1, v0}, Ldgo;->a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ldfu;

    invoke-direct {v1, v0}, Ldfu;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v1, p0, Ldgo;->a:Ldfu;

    :goto_0
    const/4 v0, 0x1

    invoke-static {p1, v0}, Ldgo;->a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Ldgq;

    invoke-direct {v1, v0}, Ldgq;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v1, p0, Ldgo;->b:Ldgq;

    :goto_1
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ldgo;->a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v1, Ldgq;

    invoke-direct {v1, v0}, Ldgq;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v1, p0, Ldgo;->c:Ldgq;

    :goto_2
    const/4 v0, 0x3

    invoke-static {p1, v0}, Ldgo;->a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v1, Ldgq;

    invoke-direct {v1, v0}, Ldgq;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v1, p0, Ldgo;->d:Ldgq;

    :goto_3
    return-void

    :cond_0
    iput-object v2, p0, Ldgo;->a:Ldfu;

    goto :goto_0

    :cond_1
    iput-object v2, p0, Ldgo;->b:Ldgq;

    goto :goto_1

    :cond_2
    iput-object v2, p0, Ldgo;->c:Ldgq;

    goto :goto_2

    :cond_3
    iput-object v2, p0, Ldgo;->d:Ldgq;

    goto :goto_3
.end method

.method private static a(Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    invoke-static {p1}, Ldeh;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Ldgo;->a:Ldfu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgo;->a:Ldfu;

    invoke-virtual {v0}, Ldfu;->b()V

    :cond_0
    iget-object v0, p0, Ldgo;->b:Ldgq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldgo;->b:Ldgq;

    invoke-virtual {v0}, Ldgq;->b()V

    :cond_1
    iget-object v0, p0, Ldgo;->c:Ldgq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldgo;->c:Ldgq;

    invoke-virtual {v0}, Ldgq;->b()V

    :cond_2
    iget-object v0, p0, Ldgo;->d:Ldgq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldgo;->d:Ldgq;

    invoke-virtual {v0}, Ldgq;->b()V

    :cond_3
    return-void
.end method
