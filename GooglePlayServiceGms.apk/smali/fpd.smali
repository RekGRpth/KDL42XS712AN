.class public final Lfpd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static a:Lfpd;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Ljava/util/HashSet;

.field private final e:Lbmi;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/graphics/BitmapFactory$Options;

.field private final h:Landroid/content/res/Resources;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lfpd;->b:Landroid/os/Handler;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lfpd;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfpd;->d:Ljava/util/HashSet;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfpd;->f:Landroid/content/Context;

    new-instance v0, Lbmi;

    iget-object v1, p0, Lfpd;->f:Landroid/content/Context;

    const/4 v5, 0x0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lfpd;->e:Lbmi;

    iget-object v0, p0, Lfpd;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lfpd;->h:Landroid/content/res/Resources;

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lfpd;->g:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, p0, Lfpd;->g:Landroid/graphics/BitmapFactory$Options;

    const/16 v1, 0x140

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iget-object v0, p0, Lfpd;->g:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, p0, Lfpd;->h:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    iget-object v0, p0, Lfpd;->g:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    return-void
.end method

.method static synthetic a(Lfpd;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lfpd;->f:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lfpd;
    .locals 1

    sget-object v0, Lfpd;->a:Lfpd;

    if-nez v0, :cond_0

    new-instance v0, Lfpd;

    invoke-direct {v0, p0}, Lfpd;-><init>(Landroid/content/Context;)V

    sput-object v0, Lfpd;->a:Lfpd;

    :cond_0
    sget-object v0, Lfpd;->a:Lfpd;

    return-object v0
.end method

.method static synthetic b(Lfpd;)Lbmi;
    .locals 1

    iget-object v0, p0, Lfpd;->e:Lbmi;

    return-object v0
.end method

.method static synthetic c(Lfpd;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lfpd;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lfpd;)Landroid/graphics/BitmapFactory$Options;
    .locals 1

    iget-object v0, p0, Lfpd;->g:Landroid/graphics/BitmapFactory$Options;

    return-object v0
.end method


# virtual methods
.method public final a(Lfpf;)V
    .locals 1

    iget-object v0, p0, Lfpd;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lfxh;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lfpd;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lfpe;

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lfxh;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v2

    invoke-direct {v1, p0, v2, p2}, Lfpe;-><init>(Lfpd;Lfxh;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b(Lfpf;)V
    .locals 1

    iget-object v0, p0, Lfpd;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 6

    const/4 v2, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lfpe;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lfpd;->h:Landroid/content/res/Resources;

    invoke-static {v0}, Lfpe;->a(Lfpe;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lfpd;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfpf;

    invoke-static {v0}, Lfpe;->b(Lfpe;)Lfxh;

    move-result-object v5

    invoke-interface {v1, v5, v3}, Lfpf;->a(Lfxh;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lfpe;

    iget-object v1, p0, Lfpd;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfpf;

    invoke-static {v0}, Lfpe;->b(Lfpe;)Lfxh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Lfpf;->a(Lfxh;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
