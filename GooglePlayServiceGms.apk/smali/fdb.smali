.class final Lfdb;
.super Lfbx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lfch;

.field private final b:Lfas;


# direct methods
.method public constructor <init>(Lfch;Lfas;)V
    .locals 0

    iput-object p1, p0, Lfdb;->a:Lfch;

    invoke-direct {p0}, Lfbx;-><init>()V

    iput-object p2, p0, Lfdb;->b:Lfas;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 5

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Avatar callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pfd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1, p2}, Lfch;->a(ILandroid/os/Bundle;)Lbbo;

    move-result-object v0

    iget-object v1, p0, Lfdb;->a:Lfch;

    new-instance v2, Lfdh;

    iget-object v3, p0, Lfdb;->a:Lfch;

    iget-object v4, p0, Lfdb;->b:Lfas;

    invoke-direct {v2, v3, v4, v0, p3}, Lfdh;-><init>(Lfch;Lfas;Lbbo;Landroid/os/ParcelFileDescriptor;)V

    invoke-virtual {v1, v2}, Lfch;->b(Lbjg;)V

    return-void
.end method
