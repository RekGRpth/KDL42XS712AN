.class public final Lax;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/content/Context;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Landroid/app/PendingIntent;

.field e:Landroid/app/PendingIntent;

.field f:Landroid/widget/RemoteViews;

.field public g:Landroid/graphics/Bitmap;

.field public h:Ljava/lang/CharSequence;

.field i:I

.field public j:I

.field k:Z

.field l:Lbf;

.field m:Ljava/lang/CharSequence;

.field n:I

.field o:I

.field p:Z

.field q:Ljava/util/ArrayList;

.field public r:Landroid/app/Notification;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lax;->q:Ljava/util/ArrayList;

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lax;->r:Landroid/app/Notification;

    iput-object p1, p0, Lax;->a:Landroid/content/Context;

    iget-object v0, p0, Lax;->r:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    iget-object v0, p0, Lax;->r:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    const/4 v0, 0x0

    iput v0, p0, Lax;->j:I

    return-void
.end method


# virtual methods
.method public final a()Lax;
    .locals 2

    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lax;->a(IZ)V

    return-object p0
.end method

.method public final a(I)Lax;
    .locals 1

    iget-object v0, p0, Lax;->r:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Lax;
    .locals 1

    iget-object v0, p0, Lax;->r:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    return-object p0
.end method

.method public final a(Lbf;)Lax;
    .locals 2

    iget-object v0, p0, Lax;->l:Lbf;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lax;->l:Lbf;

    iget-object v0, p0, Lax;->l:Lbf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lax;->l:Lbf;

    iget-object v1, v0, Lbf;->d:Lax;

    if-eq v1, p0, :cond_0

    iput-object p0, v0, Lbf;->d:Lax;

    iget-object v1, v0, Lbf;->d:Lax;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbf;->d:Lax;

    invoke-virtual {v1, v0}, Lax;->a(Lbf;)Lax;

    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lax;
    .locals 1

    iget-object v0, p0, Lax;->r:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/app/PendingIntent;)Lax;
    .locals 3

    iget-object v0, p0, Lax;->q:Ljava/util/ArrayList;

    new-instance v1, Lau;

    const v2, 0x1080038    # android.R.drawable.ic_menu_close_clear_cancel

    invoke-direct {v1, v2, p1, p2}, Lau;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(IZ)V
    .locals 2

    iget-object v0, p0, Lax;->r:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    return-void
.end method

.method public final b()Lax;
    .locals 2

    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lax;->a(IZ)V

    return-object p0
.end method

.method public final c()Lax;
    .locals 2

    iget-object v0, p0, Lax;->r:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->defaults:I

    iget-object v0, p0, Lax;->r:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/app/Notification;->flags:I

    return-object p0
.end method

.method public final d()Landroid/app/Notification;
    .locals 1

    invoke-static {}, Lat;->a()Laz;

    move-result-object v0

    invoke-interface {v0, p0}, Laz;->a(Lax;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method
