.class public final Lhzj;
.super Lcom/android/location/provider/GeofenceProvider;
.source "SourceFile"


# static fields
.field private static final a:Lhzj;


# instance fields
.field private final b:Ljava/lang/Object;

.field private c:Lhyo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhzj;

    invoke-direct {v0}, Lhzj;-><init>()V

    sput-object v0, Lhzj;->a:Lhzj;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/location/provider/GeofenceProvider;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhzj;->b:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lhzj;->c:Lhyo;

    return-void
.end method

.method public static a()Lhzj;
    .locals 1

    sget-object v0, Lhzj;->a:Lhzj;

    return-object v0
.end method


# virtual methods
.method public final b()Lhyo;
    .locals 2

    iget-object v1, p0, Lhzj;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhzj;->c:Lhyo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onGeofenceHardwareChange(Landroid/hardware/location/GeofenceHardware;)V
    .locals 4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    const-string v1, "GmsCoreGeofenceProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring calls from non-system server. Uid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lhzj;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "GmsCoreGeofenceProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got hardware geofence service: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lhyt;->c()Lhyt;

    move-result-object v2

    if-nez p1, :cond_3

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lhzj;->c:Lhyo;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lhyt;->f()V

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_1
    new-instance v0, Lhyo;

    invoke-direct {v0, p1}, Lhyo;-><init>(Landroid/hardware/location/GeofenceHardware;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
