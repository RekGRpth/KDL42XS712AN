.class public final Lamd;
.super Lk;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lbdx;
.implements Lbdy;


# instance fields
.field private Y:Landroid/widget/Spinner;

.field private Z:[Landroid/accounts/Account;

.field private aa:I

.field private ab:Landroid/widget/TextView;

.field private ac:Lbdu;

.field private ad:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lk;-><init>()V

    return-void
.end method


# virtual methods
.method public final J()V
    .locals 1

    iget-object v0, p0, Lamd;->ac:Lbdu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lamd;->ac:Lbdu;

    invoke-interface {v0}, Lbdu;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lamd;->ac:Lbdu;

    :cond_0
    iget-object v0, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_1
    return-void
.end method

.method public final a(Lbbo;)V
    .locals 2

    iget-object v0, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_1

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0b0315    # com.google.android.gms.R.string.appstate_could_not_delete_data

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a    # android.R.string.ok

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v0, v3, :cond_0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v3, 0x103000b    # android.R.style.Theme_Dialog

    invoke-direct {v0, v1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    :goto_0
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040037    # com.google.android.gms.R.layout.clear_app_data

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a00c7    # com.google.android.gms.R.id.account_spinner

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lamd;->Y:Landroid/widget/Spinner;

    const v0, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lamd;->ab:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0311    # com.google.android.gms.R.string.appstate_clear_app_data_dialog_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000    # android.R.string.cancel

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a    # android.R.string.ok

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lamd;->ac:Lbdu;

    invoke-static {v0}, Lakb;->a(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Lame;

    invoke-direct {v1, p0}, Lame;-><init>(Lamd;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lk;->f:Landroid/app/Dialog;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lamd;->ac:Lbdu;

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v1, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    const v2, 0x7f0b0314    # com.google.android.gms.R.string.appstate_deleting_app_data

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    iget-object v1, p0, Lamd;->ad:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    new-instance v1, Lbdw;

    invoke-direct {v1, v0, p0, p0}, Lbdw;-><init>(Landroid/content/Context;Lbdx;Lbdy;)V

    sget-object v0, Lakb;->c:Lbdm;

    invoke-virtual {v1, v0}, Lbdw;->a(Lbdm;)Lbdw;

    move-result-object v0

    sget-object v1, Lakb;->b:Lbem;

    invoke-virtual {v0, v1}, Lbdw;->a(Lbem;)Lbdw;

    move-result-object v0

    iget-object v1, p0, Lamd;->Z:[Landroid/accounts/Account;

    iget v2, p0, Lamd;->aa:I

    aget-object v1, v1, v2

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v1, v0, Lbdw;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lbdw;->a()Lbdu;

    move-result-object v0

    iput-object v0, p0, Lamd;->ac:Lbdu;

    iget-object v0, p0, Lamd;->ac:Lbdu;

    invoke-interface {v0}, Lbdu;->a()V

    :cond_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    iget-object v0, p0, Lamd;->Y:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lamd;->aa:I

    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public final w()V
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lk;->w()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lamd;->Z:[Landroid/accounts/Account;

    iget-object v0, p0, Lamd;->Z:[Landroid/accounts/Account;

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/String;

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lamd;->Z:[Landroid/accounts/Account;

    aget-object v3, v3, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget v0, p0, Lamd;->aa:I

    iget-object v3, p0, Lamd;->Z:[Landroid/accounts/Account;

    array-length v3, v3

    if-lt v0, v3, :cond_1

    iput v1, p0, Lamd;->aa:I

    :cond_1
    iget-object v0, p0, Lamd;->Z:[Landroid/accounts/Account;

    array-length v0, v0

    if-le v0, v5, :cond_2

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v4, 0x1090008    # android.R.layout.simple_spinner_item

    invoke-direct {v0, v3, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v2, 0x1090009    # android.R.layout.simple_spinner_dropdown_item

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v2, p0, Lamd;->Y:Landroid/widget/Spinner;

    invoke-virtual {v2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v2, p0, Lamd;->Y:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lamd;->Y:Landroid/widget/Spinner;

    iget v2, p0, Lamd;->aa:I

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lamd;->Y:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lamd;->Z:[Landroid/accounts/Account;

    array-length v0, v0

    if-ne v0, v5, :cond_3

    invoke-virtual {p0}, Lamd;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0313    # com.google.android.gms.R.string.appstate_clear_app_data_dialog_description_single_account

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lamd;->Z:[Landroid/accounts/Account;

    aget-object v4, v4, v1

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lamd;->ab:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    invoke-virtual {p0}, Lamd;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0312    # com.google.android.gms.R.string.appstate_clear_app_data_dialog_description_multi_account

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
