.class public final Lfzk;
.super Lfzi;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbbr;
.implements Lbbs;


# instance fields
.field h:Lfzl;

.field private final i:Lftz;

.field private j:Lftx;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;)V
    .locals 6

    sget-object v5, Lftx;->a:Lftz;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfzk;-><init>(Landroid/content/Context;IILjava/lang/String;Lftz;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;IILjava/lang/String;Lftz;)V
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lfzi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V

    new-instance v0, Lfzl;

    invoke-direct {v0, p0}, Lfzl;-><init>(Lfzk;)V

    iput-object v0, p0, Lfzk;->h:Lfzl;

    new-instance v0, Lfwa;

    invoke-direct {v0, p1}, Lfwa;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lfwa;->a()Lfwa;

    move-result-object v0

    invoke-virtual {v0}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iput-object p5, p0, Lfzk;->i:Lftz;

    iget-object v1, p0, Lfzk;->i:Lftz;

    invoke-interface {v1, p1, v0, p0, p0}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lfzk;->j:Lftx;

    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-interface {v0, p0}, Lftx;->a(Lbbr;)V

    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-interface {v0, p0}, Lftx;->a(Lbbs;)V

    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-virtual {p0, v0}, Lfzk;->a(Lftx;)V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 0

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 3

    const-string v0, "PlusOneButtonView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to establish connection with status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbbo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lfzk;->c()V

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lfzk;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzk;->j:Lftx;

    iget-object v1, p0, Lfzk;->h:Lfzl;

    iget-object v2, p0, Lfzk;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lftx;->a(Lfuk;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Lfzi;->onAttachedToWindow()V

    iget-object v0, p0, Lfzk;->j:Lftx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_0
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lfzi;->onDetachedFromWindow()V

    iget-object v0, p0, Lfzk;->j:Lftx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfzk;->j:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    return-void
.end method
