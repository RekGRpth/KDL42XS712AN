.class public abstract Laxx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field protected final a:Laye;

.field private final c:Ljava/lang/String;

.field private d:Layh;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Laxx;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laxx;->c:Ljava/lang/String;

    new-instance v0, Laye;

    invoke-direct {v0, p2}, Laye;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Laxx;->a:Laye;

    iget-object v0, p0, Laxx;->a:Laye;

    const-string v1, "instance-%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Laxx;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laye;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Layh;)V
    .locals 1

    iput-object p1, p0, Laxx;->d:Layh;

    iget-object v0, p0, Laxx;->d:Layh;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Laxx;->g()V

    :cond_0
    return-void
.end method

.method protected final a(Ljava/lang/String;J)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Laxx;->a:Laye;

    const-string v1, "Sending text message: %s to: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v5, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Laxx;->d:Layh;

    iget-object v1, p0, Laxx;->c:Ljava/lang/String;

    move-object v2, p1

    move-wide v3, p2

    invoke-interface/range {v0 .. v5}, Layh;->b(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method protected final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 6

    iget-object v0, p0, Laxx;->a:Laye;

    const-string v1, "Sending text message: %s to: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Laxx;->d:Layh;

    iget-object v1, p0, Laxx;->c:Ljava/lang/String;

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Layh;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method public a([B)V
    .locals 0

    return-void
.end method

.method protected final a([BLjava/lang/String;)V
    .locals 6

    iget-object v0, p0, Laxx;->a:Laye;

    const-string v1, "Sending binary message to: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Laxx;->d:Layh;

    iget-object v1, p0, Laxx;->c:Ljava/lang/String;

    const-wide/16 v3, 0x0

    move-object v2, p1

    move-object v5, p2

    invoke-interface/range {v0 .. v5}, Layh;->a(Ljava/lang/String;[BJLjava/lang/String;)V

    return-void
.end method

.method public a_(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Laxx;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected final f()J
    .locals 2

    iget-object v0, p0, Laxx;->d:Layh;

    invoke-interface {v0}, Layh;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()V
    .locals 0

    return-void
.end method
