.class public final Lefu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lefv;

.field final b:Landroid/content/Context;

.field c:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lefu;->c:I

    new-instance v0, Lefv;

    invoke-direct {v0, p0, p1}, Lefv;-><init>(Lefu;Landroid/content/Context;)V

    iput-object v0, p0, Lefu;->a:Lefv;

    iput-object p1, p0, Lefu;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lefu;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lefu;->a:Lefv;

    invoke-virtual {v1}, Lefv;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "SELECT reg_id FROM registrations WHERE package_name = ? AND sender_id = ? AND uid = ?"

    invoke-virtual {v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method final a(Ljava/lang/String;I)V
    .locals 6

    invoke-virtual {p0}, Lefu;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lefu;->a:Lefv;

    invoke-virtual {v0}, Lefv;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "registrations"

    const-string v2, "package_name = ? AND uid = ?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method final a()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lefu;->c:I

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lefu;->c:I

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lefu;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method
