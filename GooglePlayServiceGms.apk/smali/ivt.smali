.class public Livt;
.super Livr;
.source "SourceFile"


# instance fields
.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Livs;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Livt;-><init>(Livs;Ljava/lang/Runnable;)V

    return-void
.end method

.method public constructor <init>(Livs;Ljava/lang/Runnable;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Livt;-><init>(Livs;Ljava/lang/Runnable;B)V

    return-void
.end method

.method private constructor <init>(Livs;Ljava/lang/Runnable;B)V
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Livr;-><init>(Livs;Ljava/lang/Runnable;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Livt;->d:J

    iput-wide v2, p0, Livt;->e:J

    iput-wide v2, p0, Livt;->f:J

    iput-wide v2, p0, Livt;->g:J

    const/4 v0, -0x1

    iput v0, p0, Livt;->h:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Livt;->i:Z

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Livt;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b()I
    .locals 4

    const-wide/16 v2, -0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Livt;->a:Livs;

    invoke-virtual {v0, p0}, Livs;->c(Livq;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Livt;->g:J

    iget v0, p0, Livt;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-wide v0, p0, Livt;->g:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Livt;->g:J

    iget v0, p0, Livt;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Livt;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 4

    const-wide/16 v2, -0x1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Livt;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Livt;->h:I

    if-lez v0, :cond_1

    :cond_0
    iget-wide v0, p0, Livt;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-wide v0, p0, Livt;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    invoke-static {}, Liun;->a()Liun;

    move-result-object v0

    invoke-virtual {v0}, Liun;->b()Liuo;

    move-result-object v0

    invoke-interface {v0}, Liuo;->a()J

    move-result-wide v0

    iget-wide v2, p0, Livt;->d:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Livt;->g:J

    :cond_1
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Livt;->a:Livs;

    invoke-virtual {v0, p0}, Livs;->a(Livq;)V

    return-void

    :cond_2
    :try_start_1
    iget-wide v0, p0, Livt;->e:J

    iget-wide v2, p0, Livt;->d:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Livt;->g:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized e()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Livt;->g:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Livt;->a:Livs;

    invoke-virtual {v0, p0}, Livs;->a(Livt;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final g()V
    .locals 4

    const-wide/16 v2, -0x1

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Livt;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Livt;->g:J

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Livr;->g()V

    iget-object v0, p0, Livt;->a:Livs;

    invoke-virtual {v0, p0}, Livs;->a(Livq;)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Livt;->h:I

    if-lez v0, :cond_1

    iget v0, p0, Livt;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Livt;->h:I

    :cond_1
    iget v0, p0, Livt;->h:I

    if-nez v0, :cond_2

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Livt;->g:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-boolean v0, p0, Livt;->i:Z

    if-eqz v0, :cond_3

    iget-wide v0, p0, Livt;->g:J

    iget-wide v2, p0, Livt;->f:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Livt;->g:J

    goto :goto_0

    :cond_3
    invoke-static {}, Liun;->a()Liun;

    move-result-object v0

    invoke-virtual {v0}, Liun;->b()Liuo;

    move-result-object v0

    invoke-interface {v0}, Liuo;->a()J

    move-result-wide v0

    iget-wide v2, p0, Livt;->f:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Livt;->g:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized i()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Livt;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
