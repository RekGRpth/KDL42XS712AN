.class final Lfzq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfuo;


# instance fields
.field final synthetic a:Lfzm;


# direct methods
.method constructor <init>(Lfzm;)V
    .locals 0

    iput-object p1, p0, Lfzq;->a:Lfzm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbbo;Lfsv;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSignUpStateLoaded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lfzq;->a:Lfzm;

    invoke-static {v0, p2}, Lfzm;->a(Lfzm;Lfsv;)Lfsv;

    iget-object v0, p0, Lfzq;->a:Lfzm;

    invoke-static {v0}, Lfzm;->b(Lfzm;)Lftx;

    move-result-object v0

    iget-object v1, p0, Lfzq;->a:Lfzm;

    invoke-static {v1}, Lfzm;->f(Lfzm;)Lfuk;

    move-result-object v1

    iget-object v2, p0, Lfzq;->a:Lfzm;

    iget-object v2, v2, Lfzm;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lftx;->a(Lfuk;Ljava/lang/String;)V

    iget-object v0, p0, Lfzq;->a:Lfzm;

    invoke-virtual {v0}, Lfzm;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PlusOneButtonView"

    const-string v1, "onSignUpStateLoaded: performing pending click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lfzq;->a:Lfzm;

    iput-boolean v3, v0, Lfzm;->a:Z

    iget-object v0, p0, Lfzq;->a:Lfzm;

    invoke-virtual {v0}, Lfzm;->performClick()Z

    iget-object v0, p0, Lfzq;->a:Lfzm;

    iput-boolean v3, v0, Lfzm;->n:Z

    :cond_2
    return-void
.end method
