.class public final Lckw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lckw;->a:Ljava/util/Map;

    sget-object v0, Lcle;->a:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->b:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->c:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->d:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->e:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->f:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->g:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->h:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->i:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->j:Lcja;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->k:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->l:Lcja;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->m:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->n:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->o:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->p:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->q:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->r:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->s:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->t:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->u:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->v:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->w:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->x:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcle;->y:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lclg;->c:Lcjf;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lclg;->a:Lcjf;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lclg;->b:Lcjf;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lclg;->d:Lcjf;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lclg;->e:Lcjf;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcli;->a:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    sget-object v0, Lcli;->b:Lcje;

    invoke-static {v0}, Lckw;->a(Lcje;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcje;
    .locals 1

    sget-object v0, Lckw;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcje;

    return-object v0
.end method

.method public static a()Ljava/util/Collection;
    .locals 1

    sget-object v0, Lckw;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcje;)V
    .locals 3

    sget-object v0, Lckw;->a:Ljava/util/Map;

    iget-object v1, p0, Lcje;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate field name registered: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcje;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lckw;->a:Ljava/util/Map;

    iget-object v1, p0, Lcje;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
