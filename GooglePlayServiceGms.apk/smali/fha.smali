.class public final Lfha;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/service/PeopleService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/service/PeopleService;)V
    .locals 0

    iput-object p1, p0, Lfha;->a:Lcom/google/android/gms/people/service/PeopleService;

    invoke-direct {p0}, Lbip;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/people/service/PeopleService;B)V
    .locals 0

    invoke-direct {p0, p1}, Lfha;-><init>(Lcom/google/android/gms/people/service/PeopleService;)V

    return-void
.end method


# virtual methods
.method public final c(Lbjv;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x0

    if-gtz p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "clientVersion too old"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid callingPackage"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lfha;->a:Lcom/google/android/gms/people/service/PeopleService;

    invoke-static {v0, p3}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lfha;->a:Lcom/google/android/gms/people/service/PeopleService;

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/PeopleService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lbbv;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    invoke-static {p3, p4}, Lcom/google/android/gms/people/service/PeopleService;->a(Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "real_client_package_name"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "PeopleService"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PeopleService"

    const-string v1, "OrigAppId=%s  ResolvedAppID=%s  Caller=%s  RealCaller=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "social_client_application_id"

    invoke-virtual {p4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v6

    const/4 v5, 0x1

    aput-object v4, v2, v5

    const/4 v5, 0x2

    aput-object p3, v2, v5

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance v0, Lfgz;

    iget-object v1, p0, Lfha;->a:Lcom/google/android/gms/people/service/PeopleService;

    sget-object v5, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->b:Lffd;

    sget-object v6, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->a:Lffd;

    move-object v2, p3

    invoke-direct/range {v0 .. v6}, Lfgz;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lffd;Lffd;)V

    invoke-virtual {v0}, Lfgz;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "post_init_configuration"

    iget-object v3, p0, Lfha;->a:Lcom/google/android/gms/people/service/PeopleService;

    invoke-static {v3}, Lcom/google/android/gms/people/service/PeopleService;->a(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v2, "post_init_resolution"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v2, v0, v1}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
