.class public Livr;
.super Livq;
.source "SourceFile"


# instance fields
.field private d:I


# direct methods
.method public constructor <init>(Livs;)V
    .locals 1

    invoke-virtual {p1}, Livs;->a()I

    move-result v0

    invoke-direct {p0, p1, v0}, Livr;-><init>(Livs;I)V

    return-void
.end method

.method private constructor <init>(Livs;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0, p2}, Livr;-><init>(Livs;Ljava/lang/Runnable;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Livs;Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1}, Livs;->a()I

    move-result v1

    invoke-direct {p0, p1, p2, v0, v1}, Livr;-><init>(Livs;Ljava/lang/Runnable;Ljava/lang/String;I)V

    return-void
.end method

.method private constructor <init>(Livs;Ljava/lang/Runnable;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Livq;-><init>(Livs;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput p4, p0, Livr;->d:I

    return-void
.end method


# virtual methods
.method b()I
    .locals 1

    iget-object v0, p0, Livr;->a:Livs;

    invoke-virtual {v0, p0}, Livs;->c(Livq;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()V
    .locals 1

    iget-object v0, p0, Livr;->a:Livs;

    invoke-virtual {v0, p0}, Livs;->a(Livr;)V

    return-void
.end method

.method public final declared-synchronized h()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Livr;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
