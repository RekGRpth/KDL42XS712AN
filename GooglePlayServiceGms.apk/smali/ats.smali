.class public final Lats;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfas;
.implements Lfaw;


# instance fields
.field private a:Lfaj;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lats;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "appName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lats;

    invoke-direct {v1}, Lats;-><init>()V

    invoke-virtual {v1, v0}, Lats;->g(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-object v0, p0, Lats;->a:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const v4, 0x7f0a0270    # com.google.android.gms.R.id.user_icon

    const v3, 0x7f0a0080    # com.google.android.gms.R.id.app_icon

    const v0, 0x7f0400ce    # com.google.android.gms.R.layout.plus_auth_app_user_info

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Latr;

    invoke-interface {v0}, Latr;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/CircleImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/CircleImageView;->a(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iget-object v0, p0, Lats;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/CircleImageView;

    iget-object v2, p0, Lats;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/CircleImageView;->a(Landroid/graphics/Bitmap;)V

    :goto_1
    const v0, 0x7f0a0172    # com.google.android.gms.R.id.app_name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lats;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b03fe    # com.google.android.gms.R.string.plus_auth_info_fragment_app_name

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lats;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v1

    :cond_0
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/CircleImageView;

    invoke-virtual {p0}, Lats;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1080093    # android.R.drawable.sym_def_app_icon

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/CircleImageView;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/CircleImageView;

    invoke-virtual {p0}, Lats;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020201    # com.google.android.gms.R.drawable.plus_ic_avatar

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/CircleImageView;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final a(Lbbo;)V
    .locals 0

    return-void
.end method

.method public final a(Lbbo;Landroid/os/ParcelFileDescriptor;)V
    .locals 2

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Lfba;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lats;->d:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lats;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v1, 0x7f0a0270    # com.google.android.gms.R.id.user_icon

    invoke-virtual {v0, v1}, Lo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/CircleImageView;

    iget-object v1, p0, Lats;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/CircleImageView;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public final a(Lbbo;Lfed;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2}, Lfed;->a()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p2, v0}, Lfed;->b(I)Lfec;

    move-result-object v2

    invoke-interface {v2}, Lfec;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lats;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2, v0}, Lfed;->b(I)Lfec;

    move-result-object v0

    invoke-interface {v0}, Lfec;->c()Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lats;->a:Lfaj;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v1, p0, v0, v2, v3}, Lfaj;->a(Lfas;Ljava/lang/String;II)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "appName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lats;->b:Ljava/lang/String;

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lats;->c:Ljava/lang/String;

    new-instance v0, Lfaj;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Lats;->a:Lfaj;

    if-eqz p1, :cond_0

    const-string v0, "userIcon"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lats;->d:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lats;->a:Lfaj;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lfaj;->a(Lfaw;Z)V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "userIcon"

    iget-object v1, p0, Lats;->d:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public final w()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget-object v0, p0, Lats;->a:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method public final x()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->x()V

    iget-object v0, p0, Lats;->a:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    return-void
.end method
