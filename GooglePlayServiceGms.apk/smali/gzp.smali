.class public final Lgzp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lbfy;

.field public static final b:Lbfy;

.field public static final c:Lbfy;

.field public static final d:Lbfy;

.field public static final e:Lbfy;

.field public static final f:Lbfy;

.field public static final g:Lbfy;

.field public static final h:Lbfy;

.field public static final i:Lbfy;

.field public static final j:Lbfy;

.field public static final k:Lbfy;

.field public static final l:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x1

    const-string v0, "wallet.wallet_persistent_cache_enabled"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->a:Lbfy;

    const-string v0, "wallet.wallet_address_metadata_cache_ttl_ms"

    const v1, 0x34fd9000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->b:Lbfy;

    const-string v0, "wallet.min_address_whitelist"

    const-string v1, "AU,BR,DE,DK,ES,FR,GB,JP,KR,MX,NL,SE,TW,US"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->c:Lbfy;

    const-string v0, "wallet.analytics_enabled"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->d:Lbfy;

    const-string v0, "wallet.csi_enabled"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->e:Lbfy;

    const-string v0, "wallet.card_bin_length"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->f:Lbfy;

    const-string v0, "wallet.merchant_error_logging_enabled"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->g:Lbfy;

    const-string v0, "wallet.supported_shipping_countries"

    const-string v1, "AC,AD,AE,AF,AG,AI,AL,AM,AN,AO,AQ,AR,AS,AT,AU,AW,AX,AZ,BA,BB,BD,BE,BF,BG,BH,BI,BJ,BL,BM,BN,BO,BR,BS,BT,BV,BW,BY,BZ,CA,CC,CD,CF,CG,CH,CI,CK,CL,CM,CN,CO,CR,CS,CV,CX,CY,CZ,DE,DJ,DK,DM,DO,DZ,EC,EE,EG,EH,ER,ES,ET,FI,FJ,FK,FM,FO,FR,GA,GB,GD,GE,GF,GG,GH,GI,GL,GM,GN,GP,GQ,GR,GS,GT,GU,GW,GY,HK,HM,HN,HR,HT,HU,ID,IE,IL,IM,IN,IO,IQ,IS,IT,JE,JM,JO,JP,KE,KG,KH,KI,KM,KN,KR,KW,KY,KZ,LA,LB,LC,LI,LK,LR,LS,LT,LU,LV,LY,MA,MC,MD,ME,MF,MG,MH,MK,ML,MN,MO,MP,MQ,MR,MS,MT,MU,MV,MW,MX,MY,MZ,NA,NC,NE,NF,NG,NI,NL,NO,NP,NR,NU,NZ,OM,PA,PE,PF,PG,PH,PK,PL,PM,PN,PR,PS,PT,PW,PY,QA,RE,RO,RS,RU,RW,SA,SB,SC,SE,SG,SH,SI,SJ,SK,SL,SM,SN,SO,SR,ST,SV,SZ,TA,TC,TD,TF,TG,TH,TJ,TK,TL,TM,TN,TO,TR,TT,TV,TW,TZ,UA,UG,UM,US,UY,UZ,VA,VC,VE,VG,VI,VN,VU,WF,WS,YE,YT,YU,ZA,ZM,ZW"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->h:Lbfy;

    const-string v0, "wallet.max_apk_bytes_to_read"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->i:Lbfy;

    const-string v0, "wallet.force_ui_for_pre_authorized_apps"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->j:Lbfy;

    const-string v0, "wallet.service_cache_num_entries"

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->k:Lbfy;

    const-string v0, "wallet.validation.price_format"

    const-string v1, "[0-9]*(\\.[0-9][0-9]?)?"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzp;->l:Lbfy;

    return-void
.end method
