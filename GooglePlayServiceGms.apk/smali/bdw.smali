.class public final Lbdw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private final c:Ljava/util/Set;

.field private d:I

.field private e:Landroid/view/View;

.field private final f:Landroid/content/Context;

.field private final g:Ljava/util/Map;

.field private h:Landroid/os/Looper;

.field private final i:Ljava/util/Set;

.field private final j:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbdw;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbdw;->g:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbdw;->i:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbdw;->j:Ljava/util/Set;

    iput-object p1, p0, Lbdw;->f:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lbdw;->h:Landroid/os/Looper;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdw;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbdx;Lbdy;)V
    .locals 1

    invoke-direct {p0, p1}, Lbdw;-><init>(Landroid/content/Context;)V

    const-string v0, "Must provide a connected listener"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbdw;->i:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v0, "Must provide a connection failed listener"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbdw;->j:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final a()Lbdu;
    .locals 11

    new-instance v8, Lbdz;

    iget-object v9, p0, Lbdw;->f:Landroid/content/Context;

    iget-object v10, p0, Lbdw;->h:Landroid/os/Looper;

    new-instance v0, Lcom/google/android/gms/common/internal/ClientSettings;

    iget-object v1, p0, Lbdw;->a:Ljava/lang/String;

    iget-object v2, p0, Lbdw;->c:Ljava/util/Set;

    iget v3, p0, Lbdw;->d:I

    iget-object v4, p0, Lbdw;->e:Landroid/view/View;

    iget-object v5, p0, Lbdw;->b:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/ClientSettings;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;)V

    iget-object v5, p0, Lbdw;->g:Ljava/util/Map;

    iget-object v6, p0, Lbdw;->i:Ljava/util/Set;

    iget-object v7, p0, Lbdw;->j:Ljava/util/Set;

    move-object v1, v8

    move-object v2, v9

    move-object v3, v10

    move-object v4, v0

    invoke-direct/range {v1 .. v7}, Lbdz;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)V

    return-object v8
.end method

.method public final a(Lbdm;)Lbdw;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbdw;->a(Lbdm;Lbdv;)Lbdw;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdm;Lbdv;)Lbdw;
    .locals 5

    iget-object v0, p0, Lbdw;->g:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lbdm;->b:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lbdw;->c:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbem;

    iget-object v0, v0, Lbem;->a:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public final a(Lbdx;)Lbdw;
    .locals 1

    iget-object v0, p0, Lbdw;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lbdy;)Lbdw;
    .locals 1

    iget-object v0, p0, Lbdw;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lbem;)Lbdw;
    .locals 2

    iget-object v0, p0, Lbdw;->c:Ljava/util/Set;

    iget-object v1, p1, Lbem;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
