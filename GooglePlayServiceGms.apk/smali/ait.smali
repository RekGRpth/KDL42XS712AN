.class public final Lait;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lcom/google/android/gms/appdatasearch/Feature;)D
    .locals 2

    const-string v0, "factor"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D
    .locals 2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(I)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Lait;->a(Lcom/google/android/gms/appdatasearch/Feature;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static a()Lcom/google/android/gms/appdatasearch/Feature;
    .locals 2

    new-instance v0, Lcom/google/android/gms/appdatasearch/Feature;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    return-object v0
.end method

.method public static b(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D
    .locals 2

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(I)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Lait;->a(Lcom/google/android/gms/appdatasearch/Feature;)D

    move-result-wide v0

    goto :goto_0
.end method
