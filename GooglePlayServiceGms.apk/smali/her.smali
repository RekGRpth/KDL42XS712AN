.class public final Lher;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lhfb;

.field private final b:Lhep;

.field private final c:Ldl;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhfb;Lhep;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lher;->d:Landroid/content/Context;

    iput-object p3, p0, Lher;->b:Lhep;

    iput-object p2, p0, Lher;->a:Lhfb;

    new-instance v0, Lhes;

    invoke-direct {v0, p0}, Lhes;-><init>(Lher;)V

    iput-object v0, p0, Lher;->c:Ldl;

    return-void
.end method

.method private static a(Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lheu;
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Ljau;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Ljau;

    move-result-object v5

    iget-object v0, v4, Ljau;->p:Ljae;

    if-eqz v0, :cond_0

    iget-object v0, v4, Ljau;->p:Ljae;

    iget-object v0, v0, Ljae;->a:Ljava/lang/String;

    iget-object v4, v5, Ljau;->p:Ljae;

    if-eqz v4, :cond_2

    iget-object v4, v5, Ljau;->p:Ljae;

    iget-object v4, v4, Ljae;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_0
    new-instance v4, Lheu;

    invoke-direct {v4, v3}, Lheu;-><init>(B)V

    iput-boolean v2, v4, Lheu;->a:Z

    iput-object v1, v4, Lheu;->b:Ljava/lang/Long;

    iput-object v0, v4, Lheu;->c:Ljava/lang/String;

    return-object v4

    :cond_0
    iget-object v0, v4, Ljau;->d:Ljaf;

    iget-object v0, v0, Ljaf;->b:Ljava/lang/String;

    iget-object v6, v5, Ljau;->d:Ljaf;

    if-eqz v6, :cond_2

    iget-object v6, v5, Ljau;->d:Ljaf;

    iget-object v6, v6, Ljaf;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    :try_start_0
    new-instance v6, Ljava/math/BigDecimal;

    iget-object v4, v4, Ljau;->q:Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/math/BigDecimal;

    iget-object v5, v5, Ljau;->d:Ljaf;

    iget-object v5, v5, Ljaf;->a:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v5, v4}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-gtz v5, :cond_1

    :goto_1
    const/4 v5, 0x3

    :try_start_1
    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->movePointRight(I)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1

    :catch_0
    move-exception v2

    move-object v4, v2

    move v2, v3

    :goto_2
    const-string v5, "FullWalletRequester"

    const-string v6, "Exception parsing amount from full wallet request"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v4

    goto :goto_2

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private static a(Lgsm;Lheu;Lheo;Lheo;)V
    .locals 5

    if-nez p2, :cond_1

    const-string v0, "FullWalletRequester"

    const-string v1, "GetFullWallet cache miss (no entry) :("

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "get_full_wallet"

    const-string v1, "cache_miss"

    const-string v2, "null_entry"

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lheo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-ne p2, p3, :cond_3

    const-string v0, "FullWalletRequester"

    const-string v1, "GetFullWallet cache hit!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "cache_hit"

    invoke-virtual {p2}, Lheo;->c()Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Ljau;

    move-result-object v0

    iget-object v0, v0, Ljau;->p:Ljae;

    if-eqz v0, :cond_2

    const-string v0, "billing_agreement"

    :goto_1
    const-string v2, "get_full_wallet"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p1, Lheu;->c:Ljava/lang/String;

    aput-object v4, v3, v0

    iget-object v0, p1, Lheu;->b:Ljava/lang/Long;

    iget-object v4, p0, Lgsm;->a:Landroid/content/Context;

    invoke-static {v4}, Lgsl;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, ":"

    invoke-static {v4, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v1, v3, v0}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    :cond_2
    const-string v0, "one_time_card"

    goto :goto_1

    :cond_3
    const-string v1, "cache_miss"

    iget-boolean v0, p1, Lheu;->a:Z

    if-nez v0, :cond_4

    const-string v0, "FullWalletRequester"

    const-string v2, "GetFullWallet cache miss (amount insufficient) :("

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "prefetch_insufficient"

    goto :goto_1

    :cond_4
    const-string v0, "prefetch_failed"

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Landroid/os/Bundle;Lgsm;)Landroid/util/Pair;
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lher;->c:Ldl;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lher;->c:Ldl;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Ljau;

    move-result-object v4

    iget-object v4, v4, Ljau;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-enter v4

    :try_start_1
    new-instance v2, Lheq;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Ljau;

    move-result-object v3

    invoke-direct {v2, v3}, Lheq;-><init>(Ljau;)V

    iget-object v3, p0, Lher;->b:Lhep;

    invoke-virtual {v3, v2}, Lhep;->a(Lheq;)Lheo;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lheo;->g()V

    invoke-virtual {v5}, Lheo;->a()J

    move-result-wide v2

    sget-object v0, Lgzq;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v2, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v6

    sub-long/2addr v2, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-lez v0, :cond_0

    :try_start_2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3, v0}, Lheo;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    :goto_0
    :try_start_3
    invoke-virtual {v5}, Lheo;->c()Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    move-result-object v0

    invoke-static {v0, p1}, Lher;->a(Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lheu;

    move-result-object v0

    invoke-virtual {v5}, Lheo;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lheu;->a:Z

    if-eqz v2, :cond_1

    invoke-virtual {v5}, Lheo;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p3, v0, v5, v5}, Lher;->a(Lgsm;Lheu;Lheo;Lheo;)V

    const-string v0, "used"

    invoke-virtual {v5, v0}, Lheo;->a(Ljava/lang/String;)V

    invoke-virtual {v5}, Lheo;->d()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    invoke-virtual {v5}, Lheo;->e()Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    :goto_1
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_2
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_1
    move-exception v0

    :try_start_6
    const-string v0, "FullWalletRequester"

    const-string v1, "getFullWallet interrupted!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    sget-object v2, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v1, v2}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    :catch_2
    move-exception v0

    :try_start_7
    const-string v2, "FullWalletRequester"

    const-string v3, "Unexpected exception in getFullWallet"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    iget-boolean v2, v0, Lheu;->a:Z

    if-nez v2, :cond_2

    const-string v2, "unused(insufficient_prefetch_amount)"

    invoke-virtual {v5, v2}, Lheo;->a(Ljava/lang/String;)V

    :cond_2
    move-object v3, v0

    :goto_3
    iget-object v0, p0, Lher;->d:Landroid/content/Context;

    iget-object v2, p0, Lher;->a:Lhfb;

    invoke-static {p2}, Lhfx;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v6

    invoke-static {v0, v2, v6, p1}, Lheo;->a(Landroid/content/Context;Lhfb;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lheo;

    move-result-object v2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-static {v2, v0}, Lbox;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    if-eqz v5, :cond_3

    iget-boolean v0, v3, Lheu;->a:Z

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    new-array v0, v0, [Lheo;

    const/4 v6, 0x0

    aput-object v2, v0, v6

    move-object v2, v0

    :goto_4
    new-instance v0, Lhet;

    invoke-direct {v0, v2}, Lhet;-><init>([Lheo;)V

    invoke-virtual {v0}, Lhet;->a()Lheo;

    move-result-object v6

    invoke-static {p3, v3, v5, v6}, Lher;->a(Lgsm;Lheu;Lheo;Lheo;)V

    array-length v3, v2

    move v0, v1

    :goto_5
    if-ge v0, v3, :cond_6

    aget-object v1, v2, v0

    if-ne v1, v6, :cond_5

    const-string v5, "used"

    invoke-virtual {v1, v5}, Lheo;->a(Ljava/lang/String;)V

    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_4
    const/4 v0, 0x2

    new-array v0, v0, [Lheo;

    const/4 v6, 0x0

    aput-object v2, v0, v6

    const/4 v2, 0x1

    aput-object v5, v0, v2

    move-object v2, v0

    goto :goto_4

    :cond_5
    const-string v5, "unused(other_reason)"

    invoke-virtual {v1, v5}, Lheo;->a(Ljava/lang/String;)V

    goto :goto_6

    :cond_6
    invoke-virtual {v6}, Lheo;->d()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    invoke-virtual {v6}, Lheo;->e()Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move-object v3, v0

    goto :goto_3
.end method
