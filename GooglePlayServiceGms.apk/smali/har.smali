.class public final Lhar;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgwv;
.implements Lgyc;
.implements Lhbf;


# static fields
.field static final a:Ljava/util/ArrayList;

.field private static final as:[I


# instance fields
.field Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

.field Z:Landroid/widget/TextView;

.field private aA:Ljau;

.field private aB:I

.field private aC:Z

.field private aD:Z

.field private aE:Ljava/util/ArrayList;

.field private aF:Z

.field private aG:Z

.field private aH:Z

.field private aI:Z

.field private aJ:[I

.field private aK:[I

.field private aL:Z

.field private aM:Luu;

.field private aN:Lut;

.field private aO:Luu;

.field private aP:Lut;

.field private aQ:Lhba;

.field private aR:Z

.field private aS:Z

.field private aT:Ljbg;

.field private aU:I

.field private aV:I

.field private aW:Lhbp;

.field private aX:I

.field private aY:Landroid/content/Intent;

.field private aZ:Lbpj;

.field aa:Landroid/widget/TextView;

.field ab:Lgwr;

.field ac:Lgwr;

.field ad:Lgxe;

.field ae:Lgwr;

.field af:Lgya;

.field ag:Landroid/view/ViewGroup;

.field ah:Landroid/view/View;

.field ai:Landroid/widget/TextView;

.field aj:Lhca;

.field ak:Lcom/google/android/gms/wallet/common/PaymentModel;

.field al:Z

.field am:Z

.field an:Lizz;

.field ao:I

.field ap:Lgxh;

.field aq:Lgvu;

.field protected final ar:Lhcb;

.field private at:Landroid/view/View;

.field private au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

.field private aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

.field private ax:Landroid/accounts/Account;

.field private ay:[Ljbe;

.field private az:Ljava/util/ArrayList;

.field b:Landroid/view/View;

.field c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

.field d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field e:Lgxg;

.field f:Lgvt;

.field g:Landroid/view/View;

.field h:Landroid/widget/CheckBox;

.field i:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "US"

    aput-object v2, v1, v0

    array-length v2, v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sput-object v3, Lhar;->a:Ljava/util/ArrayList;

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhar;->as:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-boolean v0, p0, Lhar;->aC:Z

    iput-boolean v0, p0, Lhar;->aD:Z

    iput-boolean v0, p0, Lhar;->aF:Z

    iput-boolean v0, p0, Lhar;->aG:Z

    iput-boolean v0, p0, Lhar;->aH:Z

    iput-boolean v0, p0, Lhar;->al:Z

    iput-object v1, p0, Lhar;->aJ:[I

    iput-boolean v0, p0, Lhar;->am:Z

    iput-object v1, p0, Lhar;->aQ:Lhba;

    iput-boolean v0, p0, Lhar;->aR:Z

    iput-boolean v0, p0, Lhar;->aS:Z

    const/4 v0, -0x1

    iput v0, p0, Lhar;->ao:I

    const/4 v0, -0x2

    iput v0, p0, Lhar;->aX:I

    iput-object v1, p0, Lhar;->aY:Landroid/content/Intent;

    new-instance v0, Lhau;

    invoke-direct {v0, p0}, Lhau;-><init>(Lhar;)V

    iput-object v0, p0, Lhar;->ap:Lgxh;

    new-instance v0, Lhav;

    invoke-direct {v0, p0}, Lhav;-><init>(Lhar;)V

    iput-object v0, p0, Lhar;->aq:Lgvu;

    new-instance v0, Lhaw;

    invoke-direct {v0, p0}, Lhaw;-><init>(Lhar;)V

    iput-object v0, p0, Lhar;->aZ:Lbpj;

    new-instance v0, Lhay;

    invoke-direct {v0, p0}, Lhay;-><init>(Lhar;)V

    iput-object v0, p0, Lhar;->ar:Lhcb;

    return-void
.end method

.method static synthetic A(Lhar;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhar;->aF:Z

    return v0
.end method

.method static synthetic B(Lhar;)V
    .locals 3

    iget-object v0, p0, Lhar;->ab:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhar;->ab:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lhar;->ab:Lgwr;

    iget-object v0, p0, Lhar;->ab:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lhar;->ab:Lgwr;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "ChooseMethodsFragment.MaskedWalletNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic C(Lhar;)V
    .locals 3

    iget-object v0, p0, Lhar;->ac:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhar;->ac:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lhar;->ac:Lgwr;

    iget-object v0, p0, Lhar;->ac:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lhar;->ac:Lgwr;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "ChooseMethodsFragment.WalletItemsNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic D(Lhar;)Z
    .locals 1

    invoke-direct {p0}, Lhar;->O()Z

    move-result v0

    return v0
.end method

.method private L()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lhar;->aS:Z

    iput-boolean v3, p0, Lhar;->am:Z

    invoke-direct {p0}, Lhar;->S()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "ChooseMethodsFragment.WalletItemsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lhar;->ac:Lgwr;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "ChooseMethodsFragment.MaskedWalletNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lhar;->ab:Lgwr;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "ChooseMethodsFragment.OwErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lhar;->ae:Lgwr;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "ChooseMethodsFragment.DefaultWalletInfoDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgxe;

    iput-object v0, p0, Lhar;->ad:Lgxe;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "ChooseMethodsFragment.RequiredActionDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgya;

    iput-object v0, p0, Lhar;->af:Lgya;

    iget-object v0, p0, Lhar;->aj:Lhca;

    iget-object v1, p0, Lhar;->ar:Lhcb;

    invoke-interface {v0, v1}, Lhca;->a(Lhcb;)V

    iget-object v0, p0, Lhar;->ac:Lgwr;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhar;->ac:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :goto_0
    iget-object v0, p0, Lhar;->ab:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->ab:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    iget-object v0, p0, Lhar;->ae:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhar;->ae:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    iget-object v0, p0, Lhar;->af:Lgya;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhar;->af:Lgya;

    invoke-virtual {v0, p0}, Lgya;->a(Lgyc;)V

    :cond_2
    iget-object v0, p0, Lhar;->aj:Lhca;

    iget-object v1, p0, Lhar;->ar:Lhcb;

    iget v2, p0, Lhar;->ao:I

    invoke-interface {v0, v1, v2}, Lhca;->a(Lhcb;I)V

    const/4 v0, -0x1

    iput v0, p0, Lhar;->ao:I

    return-void

    :cond_3
    iget-object v0, p0, Lhar;->an:Lizz;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhar;->ar:Lhcb;

    iget-object v1, p0, Lhar;->an:Lizz;

    invoke-virtual {v0, v1}, Lhcb;->a(Lizz;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lhar;->aG:Z

    invoke-direct {p0, v3, v0}, Lhar;->a(ZZ)V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v2, v2}, Lhar;->a(ZZ)V

    goto :goto_0
.end method

.method private M()V
    .locals 2

    iget v0, p0, Lhar;->ao:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lhar;->aj:Lhca;

    iget-object v1, p0, Lhar;->ar:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lhar;->ao:I

    :cond_0
    return-void
.end method

.method private N()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lhar;->R()V

    invoke-direct {p0}, Lhar;->O()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lhar;->aM:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aN:Lut;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Luu;

    const-string v3, "get_masked_wallet"

    invoke-direct {v0, v3}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhar;->aM:Luu;

    iget-object v0, p0, Lhar;->aM:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lhar;->aN:Lut;

    :cond_1
    new-instance v3, Ljaw;

    invoke-direct {v3}, Ljaw;-><init>()V

    iget-object v0, p0, Lhar;->aT:Ljbg;

    iput-object v0, v3, Ljaw;->a:Ljbg;

    iget-object v0, v3, Ljaw;->a:Ljbg;

    iget-object v4, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v4, v4, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-object v4, v0, Ljbg;->b:Ljava/lang/String;

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v0, v0, Lioj;->a:Ljava/lang/String;

    iput-object v0, v3, Ljaw;->c:Ljava/lang/String;

    iget-boolean v0, p0, Lhar;->aC:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v0, v0, Lipv;->b:Ljava/lang/String;

    iput-object v0, v3, Ljaw;->d:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lhar;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lhar;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, v3, Ljaw;->i:Z

    :cond_3
    iget-object v0, p0, Lhar;->ay:[Ljbe;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhar;->ay:[Ljbe;

    invoke-static {v0}, Lgth;->a([Ljbe;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ljaw;->h:[Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Ljai;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lhar;->aj:Lhca;

    invoke-direct {p0}, Lhar;->P()Lcom/google/android/gms/wallet/Cart;

    move-result-object v5

    invoke-interface {v4, v3, v5, v0, v2}, Lhca;->a(Ljaw;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Z)V

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    :goto_1
    return-void

    :cond_5
    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Ljai;

    iget-object v0, v0, Ljai;->a:Ljava/lang/String;

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lhar;->aM:Luu;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhar;->aN:Lut;

    if-nez v0, :cond_8

    :cond_7
    new-instance v0, Luu;

    const-string v3, "encrypt_otp_and_get_full_wallet"

    invoke-direct {v0, v3}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhar;->aM:Luu;

    iget-object v0, p0, Lhar;->aM:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lhar;->aN:Lut;

    :cond_8
    new-instance v0, Ljau;

    invoke-direct {v0}, Ljau;-><init>()V

    iput-object v0, p0, Lhar;->aA:Ljau;

    iget-object v0, p0, Lhar;->aA:Ljau;

    iget-object v3, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-object v3, v0, Ljau;->f:Ljava/lang/String;

    iget-object v0, p0, Lhar;->aA:Ljau;

    new-instance v3, Ljae;

    invoke-direct {v3}, Ljae;-><init>()V

    iput-object v3, v0, Ljau;->p:Ljae;

    iget-object v0, p0, Lhar;->aA:Ljau;

    iget-object v0, v0, Ljau;->p:Ljae;

    const-string v3, "USD"

    iput-object v3, v0, Ljae;->a:Ljava/lang/String;

    iget-object v0, p0, Lhar;->aA:Ljau;

    iput-boolean v2, v0, Ljau;->m:Z

    iget-object v0, p0, Lhar;->aA:Ljau;

    iget-boolean v3, p0, Lhar;->aD:Z

    iput-boolean v3, v0, Ljau;->h:Z

    iget-object v0, p0, Lhar;->aA:Ljau;

    iget-object v3, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v3, v3, Lioj;->a:Ljava/lang/String;

    iput-object v3, v0, Ljau;->b:Ljava/lang/String;

    iget-object v3, p0, Lhar;->aA:Ljau;

    iget-boolean v0, p0, Lhar;->aI:Z

    if-nez v0, :cond_b

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Ljau;->i:Z

    iget-object v0, p0, Lhar;->aA:Ljau;

    iget-object v3, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Ljau;->y:Ljava/lang/String;

    iget-object v0, p0, Lhar;->aA:Ljau;

    iget-boolean v3, p0, Lhar;->aH:Z

    iput-boolean v3, v0, Ljau;->w:Z

    iget-boolean v0, p0, Lhar;->aC:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lhar;->aA:Ljau;

    iget-object v3, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v3, v3, Lipv;->b:Ljava/lang/String;

    iput-object v3, v0, Ljau;->c:Ljava/lang/String;

    :cond_9
    iget-object v0, p0, Lhar;->ay:[Ljbe;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhar;->aA:Ljau;

    iget-object v3, p0, Lhar;->ay:[Ljbe;

    invoke-static {v3}, Lgth;->a([Ljbe;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Ljau;->l:[Ljava/lang/String;

    :cond_a
    iget-object v0, p0, Lhar;->aj:Lhca;

    iget-object v3, p0, Lhar;->aA:Ljau;

    invoke-interface {v0, v3, v2}, Lhca;->a(Ljau;Z)V

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    goto/16 :goto_1

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method private O()Z
    .locals 1

    iget-object v0, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private P()Lcom/google/android/gms/wallet/Cart;
    .locals 1

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private R()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lhar;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->ah:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lhar;->ah:Landroid/view/View;

    const v1, 0x7f0a0328    # com.google.android.gms.R.id.credit_card_generation_view

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a()V

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v1, v1, Lioj;->e:Lipv;

    iget-object v1, v1, Lipv;->a:Lixo;

    iget-object v1, v1, Lixo;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lhar;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lhar;->d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->setVisibility(I)V

    invoke-direct {p0}, Lhar;->S()V

    :goto_0
    iget-object v0, p0, Lhar;->aW:Lhbp;

    invoke-interface {v0, v2}, Lhbp;->b(Z)V

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0, v2}, Lhar;->a(ZI)V

    goto :goto_0
.end method

.method private S()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lhar;->aB:I

    invoke-direct {p0, v0}, Lhar;->c(I)V

    return-void
.end method

.method private T()Z
    .locals 1

    iget-object v0, p0, Lhar;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private U()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lhar;->T()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhar;->ah:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhar;->aC:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_1
    iget-object v3, p0, Lhar;->d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez v0, :cond_3

    :goto_2
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2
.end method

.method private V()V
    .locals 2

    invoke-direct {p0}, Lhar;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhar;->aT:Ljbg;

    iget-boolean v0, v0, Ljbg;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->ai:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhar;->ai:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private W()V
    .locals 2

    iget-object v0, p0, Lhar;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lhar;->at:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    return-void
.end method

.method private X()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lhar;->af:Lgya;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhar;->af:Lgya;

    invoke-virtual {v0}, Lgya;->a()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhar;->af:Lgya;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    const/4 v0, 0x0

    iput-object v0, p0, Lhar;->af:Lgya;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhar;->aL:Z

    invoke-direct {p0, v2, v2}, Lhar;->a(ZI)V

    goto :goto_0
.end method

.method static synthetic a(Lhar;Lioj;)I
    .locals 1

    invoke-direct {p0, p1}, Lhar;->b(Lioj;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lhar;Lipv;)I
    .locals 1

    invoke-direct {p0, p1}, Lhar;->e(Lipv;)I

    move-result v0

    return v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/accounts/Account;II)Lhar;
    .locals 3

    new-instance v0, Lhar;

    invoke-direct {v0}, Lhar;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "immediateFullWalletRequest"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "startingContentHeight"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "startingContentWidth"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lhar;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lhar;
    .locals 3

    new-instance v0, Lhar;

    invoke-direct {v0}, Lhar;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "maskedWalletRequest"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "googleTransactionId"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "selectedInstrumentId"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "selectedAddressId"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "startingContentHeight"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "startingContentWidth"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lhar;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private a([Lioj;)Lioj;
    .locals 5

    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    invoke-direct {p0, v0}, Lhar;->b(Lioj;)I

    move-result v3

    const/16 v4, 0x7f

    if-ne v3, v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a([Lioj;Lioj;)Lioj;
    .locals 1

    invoke-static {p1, p2}, Lgth;->a([Lioj;Lioj;)Lioj;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhar;->aZ:Lbpj;

    invoke-static {p1, p2, v0}, Lgth;->a([Lioj;Lioj;Lbpj;)Lioj;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a([Lipv;)Lipv;
    .locals 5

    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    invoke-direct {p0, v0}, Lhar;->e(Lipv;)I

    move-result v3

    const/16 v4, 0x7f

    if-ne v3, v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v0, :cond_1

    iput p1, p0, Lhar;->aX:I

    iput-object p2, p0, Lhar;->aY:Landroid/content/Intent;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    iget-object v0, p0, Lhar;->aM:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aN:Lut;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aM:Luu;

    iget-object v1, p0, Lhar;->aN:Lut;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "click_to_activity_result"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhar;->aM:Luu;

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iput-object v5, p0, Lhar;->aM:Luu;

    iput-object v5, p0, Lhar;->aN:Lut;

    goto :goto_0
.end method

.method static synthetic a(Lhar;)V
    .locals 3

    iget-object v0, p0, Lhar;->ad:Lgxe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhar;->ad:Lgxe;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {}, Lgxe;->J()Lgxe;

    move-result-object v0

    iput-object v0, p0, Lhar;->ad:Lgxe;

    iget-object v0, p0, Lhar;->ad:Lgxe;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "ChooseMethodsFragment.DefaultWalletInfoDialog"

    invoke-virtual {v0, v1, v2}, Lgxe;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lhar;I)V
    .locals 0

    invoke-direct {p0, p1}, Lhar;->d(I)V

    return-void
.end method

.method static synthetic a(Lhar;ILandroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lhar;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lhar;Lgya;)V
    .locals 3

    iget-object v0, p0, Lhar;->af:Lgya;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhar;->af:Lgya;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    iput-object p1, p0, Lhar;->af:Lgya;

    iget-object v0, p0, Lhar;->af:Lgya;

    invoke-virtual {v0, p0}, Lgya;->a(Lgyc;)V

    iget-object v0, p0, Lhar;->af:Lgya;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "ChooseMethodsFragment.RequiredActionDialog"

    invoke-virtual {v0, v1, v2}, Lgya;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lhar;Lizz;)V
    .locals 5

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lhar;->a(Lizz;)V

    iget-object v0, p1, Lizz;->b:[Lioj;

    array-length v0, v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhar;->aC:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lizz;->c:[Lipv;

    array-length v0, v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, p1, v3}, Lhar;->a(Lizz;Lgrz;)V

    invoke-direct {p0, p1, v3}, Lhar;->b(Lizz;Lgrz;)V

    iget-object v0, p1, Lizz;->a:Ljbb;

    invoke-direct {p0, v0}, Lhar;->a(Ljbb;)V

    invoke-direct {p0}, Lhar;->V()V

    invoke-direct {p0, v0}, Lhar;->b(Ljbb;)V

    invoke-direct {p0, v0}, Lhar;->c(Ljbb;)V

    invoke-direct {p0}, Lhar;->W()V

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v2}, Lhar;->a(ZI)V

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    iget-object v0, p0, Lhar;->aO:Luu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhar;->aP:Lut;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhar;->aO:Luu;

    iget-object v3, p0, Lhar;->aP:Lut;

    new-array v1, v1, [Ljava/lang/String;

    const-string v4, "create_to_ui_loaded_from_cache"

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Luu;->a(Lut;[Ljava/lang/String;)Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lhar;Lizz;Lgrz;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lhar;->a(Lizz;Lgrz;)V

    return-void
.end method

.method static synthetic a(Lhar;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lhar;->ae:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhar;->ae:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {p1, p2, p3}, Lgwr;->a(Ljava/lang/String;Ljava/lang/String;I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lhar;->ae:Lgwr;

    iget-object v0, p0, Lhar;->ae:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lhar;->ae:Lgwr;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "ChooseMethodsFragment.OwErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lhar;Ljbb;Lgrz;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p1, Ljbb;->a:[I

    array-length v0, v0

    if-lez v0, :cond_4

    iget-object v0, p1, Ljbb;->a:[I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lboz;->a([II)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v0, v4, :cond_3

    if-eqz p2, :cond_0

    iget-object v0, p2, Lgrz;->b:[Ljava/lang/String;

    const/4 v1, 0x5

    const-string v2, "updatedLegalDocs"

    aput-object v2, v0, v1

    :cond_0
    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v0, v4, :cond_3

    if-eqz p2, :cond_1

    iput-boolean v3, p2, Lgrz;->a:Z

    :cond_1
    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-nez v0, :cond_2

    new-instance v0, Lhba;

    const/16 v1, 0xcb

    iget-object v2, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2}, Lgya;->f(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, v3}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v0, p0, Lhar;->aQ:Lhba;

    :cond_2
    iput-boolean v3, p0, Lhar;->aR:Z

    :cond_3
    iget-object v0, p0, Lhar;->aa:Landroid/widget/TextView;

    const-string v1, "wallet_tos_activity"

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    new-instance v2, Lhax;

    invoke-direct {v2, p0}, Lhax;-><init>(Lhar;)V

    invoke-static {v0, v1, v2}, Lgwg;->a(Landroid/widget/TextView;Ljava/util/Set;Lgwh;)V

    iget-object v0, p0, Lhar;->aa:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Lhar;->aa:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lhar;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhar;->a(ZI)V

    return-void
.end method

.method private a(Lioj;I[Lioj;)V
    .locals 9

    const/16 v8, 0xc9

    const/4 v7, 0x0

    const/16 v0, 0x7f

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v7, p0, Lhar;->aR:Z

    packed-switch p2, :pswitch_data_0

    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-nez v0, :cond_0

    new-instance v6, Lhba;

    iget-boolean v1, p0, Lhar;->aI:Z

    iget-boolean v2, p0, Lhar;->aD:Z

    iget-object v3, p0, Lhar;->aJ:[I

    iget-object v4, p0, Lhar;->aK:[I

    iget-object v5, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lgya;->b([Lioj;ZZ[I[ILcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    invoke-direct {v6, p0, v8, v0, v7}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v6, p0, Lhar;->aQ:Lhba;

    goto :goto_0

    :pswitch_0
    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-nez v0, :cond_0

    new-instance v6, Lhba;

    iget-boolean v1, p0, Lhar;->aI:Z

    iget-boolean v2, p0, Lhar;->aD:Z

    iget-object v3, p0, Lhar;->aJ:[I

    iget-object v4, p0, Lhar;->aK:[I

    iget-object v5, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lgya;->a([Lioj;ZZ[I[ILcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    invoke-direct {v6, p0, v8, v0, v7}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v6, p0, Lhar;->aQ:Lhba;

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-nez v0, :cond_0

    new-instance v0, Lhba;

    iget-object v1, p0, Lhar;->az:Ljava/util/ArrayList;

    iget-boolean v2, p0, Lhar;->aD:Z

    iget-object v3, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p1, p3, v1, v2, v3}, Lgya;->a(Lioj;[Lioj;Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-direct {v0, p0, v8, v1, v7}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v0, p0, Lhar;->aQ:Lhba;

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-nez v0, :cond_0

    new-instance v0, Lhba;

    iget-object v1, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lgya;->c(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-direct {v0, p0, v8, v1, v7}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v0, p0, Lhar;->aQ:Lhba;

    goto/16 :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-nez v0, :cond_0

    new-instance v0, Lhba;

    iget-object v1, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lgya;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-direct {v0, p0, v8, v1, v7}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v0, p0, Lhar;->aQ:Lhba;

    goto/16 :goto_0

    :pswitch_4
    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-nez v0, :cond_0

    new-instance v0, Lhba;

    iget-object v1, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lgya;->b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-direct {v0, p0, v8, v1, v7}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v0, p0, Lhar;->aQ:Lhba;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x79
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lipv;[Lipv;Z)V
    .locals 7

    const/16 v6, 0xca

    const/4 v2, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhar;->al:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhar;->aQ:Lhba;

    iget v0, v0, Lhba;->b:I

    const/16 v1, 0xc9

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eq v0, v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lhar;->aQ:Lhba;

    iput-boolean v2, p0, Lhar;->aL:Z

    goto :goto_0

    :cond_3
    array-length v0, p2

    if-nez v0, :cond_4

    new-instance v0, Lhba;

    iget-object v1, p0, Lhar;->az:Ljava/util/ArrayList;

    iget-object v2, p0, Lhar;->aE:Ljava/util/ArrayList;

    iget-boolean v3, p0, Lhar;->aD:Z

    iget-object v4, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p1, v1, v2, v3, v4}, Lgya;->a(Lipv;Ljava/util/List;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-direct {v0, p0, v6, v1, v5}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v0, p0, Lhar;->aQ:Lhba;

    goto :goto_0

    :cond_4
    if-eqz p3, :cond_5

    new-instance v0, Lhba;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-boolean v2, p0, Lhar;->aD:Z

    iget-object v3, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1, v2, v3}, Lgya;->a(Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-direct {v0, p0, v6, v1, v5}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v0, p0, Lhar;->aQ:Lhba;

    goto :goto_0

    :cond_5
    new-instance v0, Lhba;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-boolean v2, p0, Lhar;->aD:Z

    iget-object v3, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1, v2, v3}, Lgya;->b(Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-direct {v0, p0, v6, v1, v5}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v0, p0, Lhar;->aQ:Lhba;

    goto :goto_0
.end method

.method private a(Lizz;)V
    .locals 7

    const/4 v0, 0x0

    iget-object v2, p1, Lizz;->a:Ljbb;

    iget-object v1, v2, Ljbb;->a:[I

    const/16 v3, 0x8

    invoke-static {v1, v3}, Lboz;->a([II)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lhar;->aI:Z

    :cond_0
    iget-boolean v1, v2, Ljbb;->i:Z

    if-eqz v1, :cond_1

    sget-object v1, Lhar;->as:[I

    array-length v1, v1

    sget-object v3, Lhar;->as:[I

    add-int/lit8 v4, v1, 0x1

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v3

    iput-object v3, p0, Lhar;->aJ:[I

    iget-object v3, p0, Lhar;->aJ:[I

    const/4 v4, 0x3

    aput v4, v3, v1

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhar;->az:Ljava/util/ArrayList;

    iget-object v1, p1, Lizz;->c:[Lipv;

    array-length v1, v1

    if-lez v1, :cond_2

    iget-object v3, p1, Lizz;->c:[Lipv;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    iget-object v6, p0, Lhar;->az:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    sget-object v1, Lhar;->as:[I

    iput-object v1, p0, Lhar;->aJ:[I

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lizz;->b:[Lioj;

    array-length v1, v1

    if-lez v1, :cond_4

    iget-object v1, p1, Lizz;->b:[Lioj;

    array-length v3, v1

    :goto_2
    if-ge v0, v3, :cond_4

    aget-object v4, v1, v0

    iget-object v5, v4, Lioj;->e:Lipv;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lhar;->az:Ljava/util/ArrayList;

    iget-object v4, v4, Lioj;->e:Lipv;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, v2, Ljbb;->g:[Ljbe;

    array-length v0, v0

    if-lez v0, :cond_5

    iget-object v0, v2, Ljbb;->g:[Ljbe;

    iput-object v0, p0, Lhar;->ay:[Ljbe;

    :goto_3
    return-void

    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lhar;->ay:[Ljbe;

    goto :goto_3
.end method

.method private a(Lizz;Lgrz;)V
    .locals 11

    const/16 v6, 0x75

    const/4 v4, 0x0

    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v7, p1, Lizz;->b:[Lioj;

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    invoke-static {v7, v0}, Lgth;->a([Lioj;Lioj;)Lioj;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v3, v0

    move v0, v1

    :goto_0
    invoke-direct {p0, v3}, Lhar;->b(Lioj;)I

    move-result v5

    if-eqz p2, :cond_4

    iget-object v8, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v8, v8, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v5, v6, :cond_3

    const-string v0, "notFound"

    const-string v6, ""

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    iput-boolean v2, p2, Lgrz;->a:Z

    move v0, v5

    :goto_1
    if-nez v3, :cond_7

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    packed-switch v1, :pswitch_data_0

    :goto_2
    move v1, v0

    move-object v0, v3

    :goto_3
    const/16 v2, 0x7f

    if-eq v1, v2, :cond_0

    move-object v0, v4

    :cond_0
    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-boolean v1, p0, Lhar;->aF:Z

    if-eqz v1, :cond_d

    :cond_1
    :goto_4
    return-void

    :cond_2
    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    iget-object v3, p0, Lhar;->aZ:Lbpj;

    invoke-static {v7, v0, v3}, Lgth;->a([Lioj;Lioj;Lbpj;)Lioj;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_5

    const-string v0, "found"

    :goto_5
    packed-switch v5, :pswitch_data_1

    :pswitch_0
    const-string v0, "ChooseMethodsFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "unknown instrument score "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v0, v5

    goto :goto_1

    :cond_5
    const-string v0, "matchFound"

    goto :goto_5

    :pswitch_1
    const-string v6, "valid"

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    move v0, v5

    goto :goto_1

    :pswitch_2
    const-string v6, "expired"

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    if-eq v8, v9, :cond_4

    iput-boolean v2, p2, Lgrz;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_3
    const-string v6, "minAddress"

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    if-eq v8, v9, :cond_4

    iput-boolean v2, p2, Lgrz;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_4
    const-string v6, "missingPhone"

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    if-eq v8, v9, :cond_4

    iput-boolean v2, p2, Lgrz;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_5
    const-string v6, "amexDisallowed"

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    iput-boolean v2, p2, Lgrz;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_6
    const-string v6, "otherDisallowed"

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    iput-boolean v2, p2, Lgrz;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_7
    const-string v6, "declined"

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    iput-boolean v2, p2, Lgrz;->a:Z

    move v0, v5

    goto/16 :goto_1

    :pswitch_8
    const-string v6, "otherInvalid"

    invoke-virtual {p2, v0, v6}, Lgrz;->a(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    iput-boolean v2, p2, Lgrz;->a:Z

    move v0, v5

    goto/16 :goto_1

    :pswitch_9
    iget-boolean v1, p0, Lhar;->aL:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lhar;->al:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lhar;->aQ:Lhba;

    if-nez v1, :cond_6

    new-instance v1, Lhba;

    const/16 v5, 0xc9

    iget-object v6, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v6}, Lgya;->d(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v6

    invoke-direct {v1, p0, v5, v6, v2}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v1, p0, Lhar;->aQ:Lhba;

    :cond_6
    iput-boolean v2, p0, Lhar;->aR:Z

    goto/16 :goto_2

    :cond_7
    iget-object v2, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v2, v9, :cond_8

    packed-switch v0, :pswitch_data_2

    move-object v1, v3

    :goto_6
    move v10, v0

    move-object v0, v1

    move v1, v10

    goto/16 :goto_3

    :pswitch_a
    invoke-direct {p0, v3, v4}, Lhar;->b(Lioj;Lipv;)V

    iput-boolean v1, p0, Lhar;->aL:Z

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-direct {p0, v7, v0}, Lhar;->a([Lioj;Lioj;)Lioj;

    move-result-object v1

    invoke-direct {p0, v1}, Lhar;->b(Lioj;)I

    move-result v0

    goto :goto_6

    :cond_8
    iget-object v2, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v2, v1, :cond_e

    invoke-direct {p0, v3, v0, v7}, Lhar;->a(Lioj;I[Lioj;)V

    move v1, v0

    move-object v0, v3

    goto/16 :goto_3

    :cond_9
    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-direct {p0, v7, v0}, Lhar;->a([Lioj;Lioj;)Lioj;

    move-result-object v3

    invoke-direct {p0, v3}, Lhar;->b(Lioj;)I

    move-result v0

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-nez v1, :cond_e

    array-length v1, v7

    if-nez v1, :cond_b

    iget-boolean v1, p0, Lhar;->aL:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lhar;->al:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lhar;->aQ:Lhba;

    if-nez v1, :cond_a

    new-instance v1, Lhba;

    const/16 v5, 0xc9

    iget-object v6, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v6}, Lgya;->g(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v6

    invoke-direct {v1, p0, v5, v6, v2}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v1, p0, Lhar;->aQ:Lhba;

    :cond_a
    move v1, v0

    move-object v0, v3

    goto/16 :goto_3

    :cond_b
    if-nez v3, :cond_e

    invoke-static {v7}, Lhar;->b([Lioj;)Lioj;

    move-result-object v0

    if-nez v0, :cond_c

    invoke-direct {p0, v7}, Lhar;->a([Lioj;)Lioj;

    move-result-object v0

    :cond_c
    invoke-direct {p0, v0}, Lhar;->b(Lioj;)I

    move-result v1

    invoke-direct {p0, v0, v1, v7}, Lhar;->a(Lioj;I[Lioj;)V

    goto/16 :goto_3

    :cond_d
    iget-object v1, p0, Lhar;->e:Lgxg;

    invoke-direct {p0}, Lhar;->O()Z

    move-result v2

    invoke-interface {v1, v2}, Lgxg;->c(Z)V

    iget-object v1, p0, Lhar;->e:Lgxg;

    iget-boolean v2, p0, Lhar;->aI:Z

    invoke-interface {v1, v2}, Lgxg;->a(Z)V

    iget-object v1, p0, Lhar;->e:Lgxg;

    iget-boolean v2, p0, Lhar;->aD:Z

    invoke-interface {v1, v2}, Lgxg;->b(Z)V

    iget-object v1, p0, Lhar;->e:Lgxg;

    iget-object v2, p0, Lhar;->aJ:[I

    invoke-interface {v1, v2}, Lgxg;->a([I)V

    iget-object v1, p0, Lhar;->e:Lgxg;

    iget-object v2, p0, Lhar;->aK:[I

    invoke-interface {v1, v2}, Lgxg;->b([I)V

    iget-object v1, p0, Lhar;->e:Lgxg;

    invoke-interface {v1, v7}, Lgxg;->a([Lioj;)V

    iget-object v1, p0, Lhar;->e:Lgxg;

    invoke-interface {v1, v0}, Lgxg;->a(Lioj;)V

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    goto/16 :goto_4

    :cond_e
    move v1, v0

    move-object v0, v3

    goto/16 :goto_3

    :cond_f
    move v0, v6

    move-object v3, v4

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_9
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x76
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7c
        :pswitch_a
        :pswitch_a
        :pswitch_a
    .end packed-switch
.end method

.method private a(Ljbb;)V
    .locals 2

    invoke-direct {p0}, Lhar;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->ag:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Ljbb;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhar;->Z:Landroid/widget/TextView;

    iget-object v1, p1, Ljbb;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhar;->ag:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhar;->ag:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(ZI)V
    .locals 3

    const/4 v1, 0x0

    iget v2, p0, Lhar;->aB:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lhar;->aB:I

    invoke-direct {p0, p2}, Lhar;->c(I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 2

    iget-boolean v0, p0, Lhar;->aF:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    const v1, 0x7f0b0118    # com.google.android.gms.R.string.wallet_retrieving_wallet_information

    invoke-direct {p0, v0, v1}, Lhar;->a(ZI)V

    :cond_0
    if-eqz p1, :cond_2

    sget-object v0, Lgzp;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhar;->aT:Ljbg;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-object v1, v0, Ljbg;->b:Ljava/lang/String;

    :cond_3
    new-instance v0, Ljba;

    invoke-direct {v0}, Ljba;-><init>()V

    iget-object v1, p0, Lhar;->aT:Ljbg;

    iput-object v1, v0, Ljba;->a:Ljbg;

    iget-object v1, p0, Lhar;->aj:Lhca;

    invoke-interface {v1, v0, p1}, Lhca;->a(Ljba;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lhar;Ljbb;)Z
    .locals 14

    const/4 v13, 0x1

    iget-object v0, p1, Ljbb;->a:[I

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p1, Ljbb;->a:[I

    invoke-static {v0, v13}, Lboz;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    const-string v1, "US"

    iget-object v2, p0, Lhar;->ay:[Ljbe;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;-><init>(Ljava/lang/String;[Ljbe;)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lhar;->aT:Ljbg;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-object v1, v0, Ljbg;->b:Ljava/lang/String;

    iget-object v0, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhar;->aT:Ljbg;

    invoke-direct {p0}, Lhar;->P()Lcom/google/android/gms/wallet/Cart;

    move-result-object v2

    iget-object v3, p0, Lhar;->ax:Landroid/accounts/Account;

    const-string v4, "US"

    invoke-direct {p0}, Lhar;->O()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    iget-boolean v7, p0, Lhar;->aI:Z

    iget-object v8, p0, Lhar;->aJ:[I

    iget-object v9, p0, Lhar;->aK:[I

    iget-object v10, p0, Lhar;->az:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lhar;->d(Ljbb;)Z

    move-result v11

    iget-object v12, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static/range {v0 .. v12}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljbg;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f5

    invoke-virtual {p0, v0, v1}, Lhar;->a(Landroid/content/Intent;I)V

    move v0, v13

    :goto_1
    return v0

    :cond_0
    sget-object v5, Lhar;->a:Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lhar;[I)Z
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p1, v1

    const-string v4, "ChooseMethodsFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Required action: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch v3, :sswitch_data_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :sswitch_0
    const/16 v0, 0x199

    invoke-direct {p0, v0}, Lhar;->d(I)V

    const/4 v0, 0x1

    :cond_0
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x9 -> :sswitch_0
    .end sparse-switch
.end method

.method private b(Lioj;)I
    .locals 5

    const/16 v0, 0x77

    if-nez p1, :cond_1

    const/16 v0, 0x75

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Lioj;->l:I

    iget v2, p1, Lioj;->c:I

    iget-object v3, p0, Lhar;->aK:[I

    invoke-static {v3, v1}, Lboz;->a([II)Z

    move-result v3

    if-eqz v3, :cond_2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected instrument category: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v0, 0x7b

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x7a

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhar;->aJ:[I

    invoke-static {v1, v2}, Lboz;->a([II)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x3

    if-ne v2, v0, :cond_3

    const/16 v0, 0x79

    goto :goto_0

    :cond_3
    const/16 v0, 0x78

    goto :goto_0

    :cond_4
    iget-object v1, p1, Lioj;->g:[I

    array-length v1, v1

    if-lez v1, :cond_6

    iget-object v1, p1, Lioj;->g:[I

    array-length v4, v1

    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_5

    iget-object v1, p1, Lioj;->g:[I

    aget v1, v1, v2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    const/16 v3, 0x7d

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget v1, p1, Lioj;->h:I

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lhar;->aI:Z

    if-eqz v0, :cond_7

    iget-object v0, p1, Lioj;->e:Lipv;

    invoke-static {v0}, Lgth;->c(Lipv;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x7c

    goto :goto_0

    :cond_7
    iget-boolean v0, p0, Lhar;->aD:Z

    if-eqz v0, :cond_8

    iget-object v0, p1, Lioj;->e:Lipv;

    invoke-static {v0}, Lgth;->a(Lipv;)Z

    move-result v0

    if-nez v0, :cond_8

    const/16 v0, 0x7e

    goto :goto_0

    :cond_8
    const/16 v0, 0x7f

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x76

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static b([Lioj;)Lioj;
    .locals 4

    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p0, v1

    iget-boolean v3, v0, Lioj;->f:Z

    if-eqz v3, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b([Lipv;)Lipv;
    .locals 4

    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p0, v1

    iget-boolean v3, v0, Lipv;->h:Z

    if-eqz v3, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic b(Lhar;)V
    .locals 0

    invoke-direct {p0}, Lhar;->R()V

    return-void
.end method

.method static synthetic b(Lhar;Lioj;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhar;->b(Lioj;Lipv;)V

    return-void
.end method

.method static synthetic b(Lhar;Lipv;)V
    .locals 0

    invoke-direct {p0, p1}, Lhar;->d(Lipv;)V

    return-void
.end method

.method static synthetic b(Lhar;Lizz;)V
    .locals 0

    invoke-direct {p0, p1}, Lhar;->a(Lizz;)V

    return-void
.end method

.method static synthetic b(Lhar;Lizz;Lgrz;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lhar;->b(Lizz;Lgrz;)V

    return-void
.end method

.method static synthetic b(Lhar;Ljbb;)V
    .locals 0

    invoke-direct {p0, p1}, Lhar;->a(Ljbb;)V

    return-void
.end method

.method private b(Lioj;Lipv;)V
    .locals 9

    const/4 v5, 0x0

    iget-object v0, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhar;->ax:Landroid/accounts/Account;

    iget-boolean v3, p0, Lhar;->aI:Z

    iget-boolean v4, p0, Lhar;->aD:Z

    iget-object v7, p0, Lhar;->az:Ljava/util/ArrayList;

    move-object v2, p1

    move-object v6, v5

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;ZZLjava/lang/String;Lioq;Ljava/util/Collection;Lipv;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f8

    invoke-virtual {p0, v0, v1}, Lhar;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method private b(Lizz;Lgrz;)V
    .locals 12

    const/4 v10, 0x3

    const/4 v3, 0x0

    const/16 v9, 0x7f

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lhar;->aC:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lhar;->f:Lgvt;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lgvt;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p1, Lizz;->c:[Lipv;

    const/16 v0, 0x7b

    iget-object v4, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v4, v4, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v4, v2, :cond_b

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lipv;

    invoke-static {v6, v0}, Lgth;->a([Lipv;Lipv;)Lipv;

    move-result-object v4

    invoke-direct {p0, v4}, Lhar;->e(Lipv;)I

    move-result v0

    if-eqz p2, :cond_2

    iget-object v5, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v5, v5, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lipv;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v5, v5, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    const/16 v7, 0x7b

    if-ne v0, v7, :cond_5

    const-string v5, "notFound"

    const-string v7, ""

    invoke-virtual {p2, v5, v7}, Lgrz;->b(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    iput-boolean v1, p2, Lgrz;->a:Z

    :cond_2
    :goto_1
    const/16 v5, 0x7d

    if-eq v0, v5, :cond_3

    const/16 v5, 0x7e

    if-ne v0, v5, :cond_6

    :cond_3
    move v5, v2

    :goto_2
    if-nez v4, :cond_8

    iget-object v2, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    packed-switch v2, :pswitch_data_0

    :goto_3
    :pswitch_0
    move v1, v0

    move-object v0, v4

    :goto_4
    if-eq v1, v9, :cond_4

    move-object v0, v3

    :cond_4
    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-boolean v1, p0, Lhar;->aF:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lhar;->f:Lgvt;

    invoke-interface {v1, v6}, Lgvt;->a([Lipv;)V

    iget-object v1, p0, Lhar;->f:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    iget-object v1, p0, Lhar;->f:Lgvt;

    iget-boolean v2, p0, Lhar;->aD:Z

    invoke-interface {v1, v2}, Lgvt;->a(Z)V

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lipv;

    goto :goto_0

    :cond_5
    const-string v7, "found"

    packed-switch v0, :pswitch_data_1

    const-string v5, "ChooseMethodsFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unknown address score "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_1
    const-string v5, "valid"

    invoke-virtual {p2, v7, v5}, Lgrz;->b(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    goto :goto_1

    :pswitch_2
    const-string v8, "minShippingAddress"

    invoke-virtual {p2, v7, v8}, Lgrz;->b(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    if-eq v5, v10, :cond_2

    iput-boolean v1, p2, Lgrz;->a:Z

    goto :goto_1

    :pswitch_3
    const-string v5, "missingPhone"

    invoke-virtual {p2, v7, v5}, Lgrz;->b(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    goto :goto_1

    :pswitch_4
    const-string v5, "otherInvalid"

    invoke-virtual {p2, v7, v5}, Lgrz;->b(Ljava/lang/String;Ljava/lang/String;)Lgrz;

    iput-boolean v1, p2, Lgrz;->a:Z

    goto :goto_1

    :cond_6
    move v5, v1

    goto :goto_2

    :pswitch_5
    iget-boolean v2, p0, Lhar;->aL:Z

    if-nez v2, :cond_7

    iget-boolean v2, p0, Lhar;->al:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lhar;->aQ:Lhba;

    if-nez v2, :cond_7

    new-instance v2, Lhba;

    const/16 v5, 0xca

    iget-object v7, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v7}, Lgya;->e(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v7

    invoke-direct {v2, p0, v5, v7, v1}, Lhba;-><init>(Lhar;ILgya;B)V

    iput-object v2, p0, Lhar;->aQ:Lhba;

    :cond_7
    iput-boolean v1, p0, Lhar;->aR:Z

    goto/16 :goto_3

    :cond_8
    iget-object v7, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v7, v7, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v7, v10, :cond_9

    if-eqz v5, :cond_9

    invoke-direct {p0, v4}, Lhar;->d(Lipv;)V

    iput-boolean v2, p0, Lhar;->aL:Z

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-static {v6, v0}, Lgth;->a([Lipv;Lipv;)Lipv;

    move-result-object v1

    invoke-direct {p0, v1}, Lhar;->e(Lipv;)I

    move-result v0

    move v11, v0

    move-object v0, v1

    move v1, v11

    goto/16 :goto_4

    :cond_9
    iget-object v7, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v7, v7, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v7, v2, :cond_a

    if-eq v0, v9, :cond_a

    iput-boolean v1, p0, Lhar;->aR:Z

    invoke-direct {p0, v4, v6, v5}, Lhar;->a(Lipv;[Lipv;Z)V

    :cond_a
    move v1, v0

    move-object v0, v4

    goto/16 :goto_4

    :cond_b
    array-length v4, v6

    if-nez v4, :cond_c

    invoke-direct {p0, v3, v6, v1}, Lhar;->a(Lipv;[Lipv;Z)V

    move v1, v0

    move-object v0, v3

    goto/16 :goto_4

    :cond_c
    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-static {v6, v0}, Lgth;->a([Lipv;Lipv;)Lipv;

    move-result-object v4

    invoke-direct {p0, v4}, Lhar;->e(Lipv;)I

    move-result v0

    if-nez v4, :cond_11

    iget-object v5, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v5, v5, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-nez v5, :cond_11

    invoke-static {v6}, Lhar;->b([Lipv;)Lipv;

    move-result-object v0

    if-nez v0, :cond_d

    invoke-direct {p0, v6}, Lhar;->a([Lipv;)Lipv;

    move-result-object v0

    :cond_d
    invoke-direct {p0, v0}, Lhar;->e(Lipv;)I

    move-result v4

    if-eq v4, v9, :cond_10

    const/16 v5, 0x7d

    if-eq v4, v5, :cond_e

    const/16 v5, 0x7e

    if-ne v4, v5, :cond_f

    :cond_e
    move v1, v2

    :cond_f
    invoke-direct {p0, v0, v6, v1}, Lhar;->a(Lipv;[Lipv;Z)V

    :cond_10
    move v1, v4

    goto/16 :goto_4

    :cond_11
    move v1, v0

    move-object v0, v4

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7c
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private b(Ljbb;)V
    .locals 2

    invoke-direct {p0, p1}, Lhar;->d(Ljbb;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhar;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private c(I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lhar;->aB:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lhar;->T()Z

    move-result v3

    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lhar;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v1, p0, Lhar;->e:Lgxg;

    invoke-interface {v1, v2}, Lgxg;->setEnabled(Z)V

    iget-object v1, p0, Lhar;->f:Lgvt;

    invoke-interface {v1, v2}, Lgvt;->setEnabled(Z)V

    iget-object v1, p0, Lhar;->Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->setEnabled(Z)V

    iget-object v1, p0, Lhar;->h:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v1, p0, Lhar;->i:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lhar;->aa:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lhar;->U()V

    if-eqz v0, :cond_1

    if-nez p1, :cond_4

    :cond_1
    iget-object v0, p0, Lhar;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->b()V

    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lhar;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v2, p0, Lhar;->e:Lgxg;

    invoke-interface {v2, v1}, Lgxg;->setEnabled(Z)V

    iget-object v2, p0, Lhar;->f:Lgvt;

    invoke-interface {v2, v1}, Lgvt;->setEnabled(Z)V

    iget-object v2, p0, Lhar;->Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->setEnabled(Z)V

    iget-object v2, p0, Lhar;->h:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v2, p0, Lhar;->i:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lhar;->aa:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lhar;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->a(I)V

    goto :goto_2
.end method

.method static synthetic c(Lhar;)V
    .locals 0

    invoke-direct {p0}, Lhar;->N()V

    return-void
.end method

.method static synthetic c(Lhar;Ljbb;)V
    .locals 0

    invoke-direct {p0, p1}, Lhar;->c(Ljbb;)V

    return-void
.end method

.method private c(Lipv;)V
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhar;->ax:Landroid/accounts/Account;

    iget-object v3, p0, Lhar;->aE:Ljava/util/ArrayList;

    iget-object v6, p0, Lhar;->az:Ljava/util/ArrayList;

    iget-boolean v8, p0, Lhar;->aD:Z

    move-object v4, v2

    move-object v5, v2

    move-object v7, p1

    invoke-static/range {v0 .. v8}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lioq;Ljava/util/Collection;Lipv;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f7

    invoke-virtual {p0, v0, v1}, Lhar;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method private c(Ljbb;)V
    .locals 3

    iget-object v0, p0, Lhar;->aT:Ljbg;

    iget-boolean v0, v0, Ljbg;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Ljbb;->l:[Ljai;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Ljai;

    iget-object v1, p0, Lhar;->Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    iget-object v2, p1, Ljbb;->l:[Ljai;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->a([Ljai;)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lhar;->Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->a(Ljai;)V

    :cond_0
    iget-object v0, p0, Lhar;->Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhar;->Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->setVisibility(I)V

    goto :goto_0
.end method

.method private d(I)V
    .locals 4

    const/4 v0, 0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lgrl;

    move-result-object v2

    invoke-direct {p0}, Lhar;->Q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lgrl;->b(Ljava/lang/String;)Lgrl;

    move-result-object v2

    iget-object v3, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lgrl;->a(Ljava/lang/String;)Lgrl;

    :cond_0
    const-string v3, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    iget-object v2, v2, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-direct {p0, v0, v1}, Lhar;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic d(Lhar;)V
    .locals 0

    invoke-direct {p0}, Lhar;->U()V

    return-void
.end method

.method static synthetic d(Lhar;Ljbb;)V
    .locals 0

    invoke-direct {p0, p1}, Lhar;->b(Ljbb;)V

    return-void
.end method

.method private d(Lipv;)V
    .locals 8

    const/4 v4, 0x0

    iget-object v0, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhar;->ax:Landroid/accounts/Account;

    iget-object v3, p0, Lhar;->aE:Ljava/util/ArrayList;

    iget-object v6, p0, Lhar;->az:Ljava/util/ArrayList;

    iget-boolean v7, p0, Lhar;->aD:Z

    move-object v2, p1

    move-object v5, v4

    invoke-static/range {v0 .. v7}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lipv;Ljava/util/ArrayList;Ljava/lang/String;Lioq;Ljava/util/Collection;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f9

    invoke-virtual {p0, v0, v1}, Lhar;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method private d(Ljbb;)Z
    .locals 3

    iget-boolean v0, p1, Ljbb;->j:Z

    iget-boolean v1, p1, Ljbb;->k:Z

    invoke-direct {p0}, Lhar;->O()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lipv;)I
    .locals 1

    if-nez p1, :cond_0

    const/16 v0, 0x7b

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lgth;->b(Lipv;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x7c

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lgth;->c(Lipv;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x7d

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lhar;->aD:Z

    if-eqz v0, :cond_3

    invoke-static {p1}, Lgth;->a(Lipv;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x7e

    goto :goto_0

    :cond_3
    const/16 v0, 0x7f

    goto :goto_0
.end method

.method private e(I)V
    .locals 12

    const/4 v9, 0x0

    iget-object v0, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lhar;->ax:Landroid/accounts/Account;

    const-string v2, "US"

    sget-object v3, Lhar;->a:Ljava/util/ArrayList;

    iget-boolean v4, p0, Lhar;->aI:Z

    iget-boolean v5, p0, Lhar;->aD:Z

    iget-object v6, p0, Lhar;->aJ:[I

    iget-object v7, p0, Lhar;->aK:[I

    const/4 v8, 0x0

    iget-object v11, p0, Lhar;->az:Ljava/util/ArrayList;

    move-object v10, v9

    invoke-static/range {v0 .. v11}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lioq;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f6

    invoke-virtual {p0, v0, v1}, Lhar;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic e(Lhar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhar;->e(I)V

    return-void
.end method

.method static synthetic f(Lhar;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method static synthetic g(Lhar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhar;->c(Lipv;)V

    return-void
.end method

.method static synthetic h(Lhar;)[Ljbe;
    .locals 1

    iget-object v0, p0, Lhar;->ay:[Ljbe;

    return-object v0
.end method

.method static synthetic i(Lhar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lhar;->a(ZZ)V

    return-void
.end method

.method static synthetic j(Lhar;)Z
    .locals 1

    iget-boolean v0, p0, Lhar;->aS:Z

    return v0
.end method

.method static synthetic k(Lhar;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhar;->aR:Z

    return v0
.end method

.method static synthetic l(Lhar;)V
    .locals 2

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    return-void
.end method

.method static synthetic m(Lhar;)V
    .locals 0

    invoke-direct {p0}, Lhar;->V()V

    return-void
.end method

.method static synthetic n(Lhar;)V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, Lhar;->aL:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lhar;->af:Lgya;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhar;->aQ:Lhba;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhar;->aQ:Lhba;

    iget v1, v0, Lhba;->a:I

    packed-switch v1, :pswitch_data_0

    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lhar;->aQ:Lhba;

    goto :goto_0

    :pswitch_0
    iget-object v1, v0, Lhba;->f:Lhar;

    iget-object v0, v0, Lhba;->c:Lgya;

    invoke-static {v1, v0}, Lhar;->a(Lhar;Lgya;)V

    goto :goto_1

    :pswitch_1
    iget-object v1, v0, Lhba;->f:Lhar;

    iget-object v2, v0, Lhba;->d:Landroid/content/Intent;

    iget v3, v0, Lhba;->e:I

    invoke-virtual {v1, v2, v3}, Lhar;->a(Landroid/content/Intent;I)V

    iget-object v0, v0, Lhba;->f:Lhar;

    iput-boolean v4, v0, Lhar;->aL:Z

    goto :goto_1

    :cond_2
    iput-boolean v4, p0, Lhar;->aL:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic o(Lhar;)V
    .locals 0

    invoke-direct {p0}, Lhar;->W()V

    return-void
.end method

.method static synthetic p(Lhar;)Z
    .locals 1

    iget-boolean v0, p0, Lhar;->aG:Z

    return v0
.end method

.method static synthetic q(Lhar;)Z
    .locals 1

    iget-boolean v0, p0, Lhar;->aR:Z

    return v0
.end method

.method static synthetic r(Lhar;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhar;->aG:Z

    return v0
.end method

.method static synthetic s(Lhar;)Luu;
    .locals 1

    iget-object v0, p0, Lhar;->aO:Luu;

    return-object v0
.end method

.method static synthetic t(Lhar;)Lut;
    .locals 1

    iget-object v0, p0, Lhar;->aP:Lut;

    return-object v0
.end method

.method static synthetic u(Lhar;)Luu;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhar;->aO:Luu;

    return-object v0
.end method

.method static synthetic v(Lhar;)Lut;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhar;->aP:Lut;

    return-object v0
.end method

.method static synthetic w(Lhar;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lhar;->Q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lhar;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lhar;->ax:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic y(Lhar;)Ljau;
    .locals 1

    iget-object v0, p0, Lhar;->aA:Ljau;

    return-object v0
.end method

.method static synthetic z(Lhar;)Lhbp;
    .locals 1

    iget-object v0, p0, Lhar;->aW:Lhbp;

    return-object v0
.end method


# virtual methods
.method public final J()I
    .locals 1

    iget-object v0, p0, Lhar;->at:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->at:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhar;->at:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final K()I
    .locals 1

    iget-object v0, p0, Lhar;->at:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->at:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhar;->at:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhar;->aW:Lhbp;

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    const v0, 0x7f04012e    # com.google.android.gms.R.layout.wallet_fragment_choose_methods

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0a030c    # com.google.android.gms.R.id.content_wrapper

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhar;->at:Landroid/view/View;

    const v0, 0x7f0a031f    # com.google.android.gms.R.id.choose_methods_content

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhar;->b:Landroid/view/View;

    const v0, 0x7f0a02fe    # com.google.android.gms.R.id.prog_bar_view

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iput-object v0, p0, Lhar;->c:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iget v0, p0, Lhar;->aU:I

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_0
    iget v3, p0, Lhar;->aV:I

    if-ne v3, v5, :cond_2

    move v3, v1

    :goto_1
    if-ne v0, v3, :cond_3

    :goto_2
    invoke-static {v1}, Lbkm;->b(Z)V

    iget v0, p0, Lhar;->aU:I

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lhar;->at:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lhar;->aU:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v1, p0, Lhar;->aV:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v5, p0, Lhar;->aU:I

    iput v5, p0, Lhar;->aV:I

    :cond_0
    const v0, 0x7f0a0320    # com.google.android.gms.R.id.pay_to_line

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lhar;->ag:Landroid/view/ViewGroup;

    const v0, 0x7f0a02fb    # com.google.android.gms.R.id.instrument_selector

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgxg;

    iput-object v0, p0, Lhar;->e:Lgxg;

    iget-object v0, p0, Lhar;->e:Lgxg;

    iget-object v1, p0, Lhar;->ap:Lgxh;

    invoke-interface {v0, v1}, Lgxg;->a(Lgxh;)V

    const v0, 0x7f0a0322    # com.google.android.gms.R.id.address_selector

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgvt;

    iput-object v0, p0, Lhar;->f:Lgvt;

    iget-object v0, p0, Lhar;->f:Lgvt;

    iget-object v1, p0, Lhar;->aq:Lgvu;

    invoke-interface {v0, v1}, Lgvt;->a(Lgvu;)V

    const v0, 0x7f0a0323    # com.google.android.gms.R.id.loyalty_wob_selector

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    iput-object v0, p0, Lhar;->Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    iget-object v0, p0, Lhar;->Y:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->a(Lhbf;)V

    const v0, 0x7f0a0324    # com.google.android.gms.R.id.default_google_wallet_container

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhar;->g:Landroid/view/View;

    const v0, 0x7f0a0325    # com.google.android.gms.R.id.default_google_wallet_check_box

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lhar;->h:Landroid/widget/CheckBox;

    const v0, 0x7f0a0326    # com.google.android.gms.R.id.default_google_wallet_info_clickable

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhar;->i:Landroid/view/View;

    const v0, 0x7f0a0327    # com.google.android.gms.R.id.credit_card_generation_screen

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhar;->ah:Landroid/view/View;

    iget-object v0, p0, Lhar;->i:Landroid/view/View;

    new-instance v1, Lhas;

    invoke-direct {v1, p0}, Lhas;-><init>(Lhar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0321    # com.google.android.gms.R.id.merchant_name_text

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhar;->Z:Landroid/widget/TextView;

    const v0, 0x7f0a0309    # com.google.android.gms.R.id.multi_use_card_details_text

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhar;->ai:Landroid/widget/TextView;

    const v0, 0x7f0a030a    # com.google.android.gms.R.id.tos_text

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhar;->aa:Landroid/widget/TextView;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lhar;->d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lhar;->d:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    new-instance v1, Lhat;

    invoke-direct {v1, p0}, Lhat;-><init>(Lhar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    return-object v4

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    move v3, v2

    goto/16 :goto_1

    :cond_3
    move v1, v2

    goto/16 :goto_2
.end method

.method public final a()V
    .locals 1

    invoke-direct {p0}, Lhar;->X()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhar;->e(I)V

    return-void
.end method

.method public final a(II)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_1

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, v1, v1}, Lhar;->a(ZI)V

    :goto_0
    return-void

    :pswitch_0
    iget-boolean v0, p0, Lhar;->al:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lhar;->N()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lhar;->a(ZZ)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lhar;->a(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lhar;->O()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhar;->aW:Lhbp;

    sget-object v1, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lhbp;->a(Landroid/accounts/Account;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2}, Lhar;->d(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x8

    iput-boolean v3, p0, Lhar;->aF:Z

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "choose_methods"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lhar;->am:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lhar;->L()V

    :cond_1
    invoke-direct {p0, v3, v3}, Lhar;->a(ZI)V

    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    :goto_1
    return-void

    :pswitch_0
    packed-switch p2, :pswitch_data_1

    :pswitch_1
    if-eqz p3, :cond_7

    const-string v0, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lhar;->d(I)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lhar;->O()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    invoke-direct {p0, v0, p3}, Lhar;->a(ILandroid/content/Intent;)V

    goto :goto_1

    :cond_3
    iput-object v2, p0, Lhar;->an:Lizz;

    const-string v0, "EXTRA_SIGN_UP_RESULT"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Ljava/lang/Class;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljax;

    invoke-direct {p0}, Lhar;->O()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, v0, Ljax;->b:Ljbh;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v0, Ljbh;->d:Lipv;

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v0, Ljbh;->e:Ljbi;

    invoke-static {v2}, Lgtk;->a(Ljbi;)Lioj;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Ljbh;->a:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-boolean v4, p0, Lhar;->aH:Z

    iget-boolean v0, p0, Lhar;->aS:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lhar;->aC:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhar;->f:Lgvt;

    invoke-interface {v0}, Lgvt;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhar;->f:Lgvt;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-interface {v0, v1}, Lgvt;->a(Lipv;)V

    :cond_4
    iget-object v0, p0, Lhar;->e:Lgxg;

    invoke-interface {v0}, Lgxg;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhar;->e:Lgxg;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-interface {v0, v1}, Lgxg;->a(Lioj;)V

    invoke-direct {p0}, Lhar;->N()V

    :cond_5
    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-nez v0, :cond_0

    iput-boolean v4, p0, Lhar;->aG:Z

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lhar;->ar:Lhcb;

    invoke-virtual {v1, v0}, Lhcb;->a(Ljax;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v1, p0, Lhar;->aW:Lhbp;

    const-string v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-interface {v1, v0}, Lhbp;->a(Landroid/accounts/Account;)V

    goto/16 :goto_1

    :pswitch_4
    invoke-direct {p0, v3, v2}, Lhar;->a(ILandroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0, v1}, Lhar;->d(I)V

    goto/16 :goto_0

    :pswitch_5
    packed-switch p2, :pswitch_data_2

    invoke-direct {p0, v1}, Lhar;->d(I)V

    goto/16 :goto_0

    :pswitch_6
    iput-object v2, p0, Lhar;->an:Lizz;

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lioj;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    iget-boolean v1, p0, Lhar;->aS:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhar;->e:Lgxg;

    invoke-interface {v1}, Lgxg;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhar;->e:Lgxg;

    invoke-interface {v1, v0}, Lgxg;->a(Lioj;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lhar;->e:Lgxg;

    invoke-interface {v0}, Lgxg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->e:Lgxg;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-interface {v0, v1}, Lgxg;->a(Lioj;)V

    goto/16 :goto_0

    :pswitch_8
    packed-switch p2, :pswitch_data_3

    invoke-direct {p0, v1}, Lhar;->d(I)V

    goto/16 :goto_0

    :pswitch_9
    iput-object v2, p0, Lhar;->an:Lizz;

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lioj;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    iget-boolean v1, p0, Lhar;->aS:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhar;->e:Lgxg;

    invoke-interface {v1}, Lgxg;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhar;->e:Lgxg;

    invoke-interface {v1, v0}, Lgxg;->a(Lioj;)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lhar;->e:Lgxg;

    invoke-interface {v0}, Lgxg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->e:Lgxg;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-interface {v0, v1}, Lgxg;->a(Lioj;)V

    goto/16 :goto_0

    :pswitch_b
    packed-switch p2, :pswitch_data_4

    invoke-direct {p0, v1}, Lhar;->d(I)V

    goto/16 :goto_0

    :pswitch_c
    iput-object v2, p0, Lhar;->an:Lizz;

    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lipv;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lipv;

    iget-boolean v1, p0, Lhar;->aS:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhar;->f:Lgvt;

    invoke-interface {v1}, Lgvt;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhar;->f:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lhar;->f:Lgvt;

    invoke-interface {v0}, Lgvt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->f:Lgvt;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-interface {v0, v1}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_e
    packed-switch p2, :pswitch_data_5

    invoke-direct {p0, v1}, Lhar;->d(I)V

    goto/16 :goto_0

    :pswitch_f
    iput-object v2, p0, Lhar;->an:Lizz;

    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lipv;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lipv;

    iget-boolean v1, p0, Lhar;->aS:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhar;->f:Lgvt;

    invoke-interface {v1}, Lgvt;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhar;->f:Lgvt;

    invoke-interface {v1, v0}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_10
    iget-object v0, p0, Lhar;->f:Lgvt;

    invoke-interface {v0}, Lgvt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhar;->f:Lgvt;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-interface {v0, v1}, Lgvt;->a(Lipv;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_5
        :pswitch_b
        :pswitch_8
        :pswitch_e
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_9
        :pswitch_a
    .end packed-switch

    :pswitch_data_4
    .packed-switch -0x1
        :pswitch_c
        :pswitch_d
    .end packed-switch

    :pswitch_data_5
    .packed-switch -0x1
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    iget v0, p0, Lhar;->aX:I

    const/4 v3, -0x2

    if-eq v0, v3, :cond_0

    iget v0, p0, Lhar;->aX:I

    iget-object v1, p0, Lhar;->aY:Landroid/content/Intent;

    invoke-direct {p0, v0, v1}, Lhar;->a(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "buyFlowConfig"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v0, "maskedWalletRequest"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "maskedWalletRequest"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->f()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lhar;->aI:Z

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhar;->aC:Z

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->d()Z

    move-result v0

    iput-boolean v0, p0, Lhar;->aD:Z

    iget-boolean v0, p0, Lhar;->aC:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->p()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lhfx;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lhar;->aE:Ljava/util/ArrayList;

    :cond_1
    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_2
    iget-object v4, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->o()Z

    move-result v4

    if-nez v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    new-array v0, v0, [I

    iput-object v0, p0, Lhar;->aK:[I

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lhar;->aK:[I

    aput v1, v0, v2

    :goto_3
    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->o()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lhar;->aK:[I

    aput v5, v0, v1

    :cond_3
    :goto_4
    const-string v0, "account"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lhar;->ax:Landroid/accounts/Account;

    iget-object v0, p0, Lhar;->aj:Lhca;

    if-nez v0, :cond_4

    new-instance v0, Lhbu;

    iget-object v1, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lhar;->ax:Landroid/accounts/Account;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v3}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v5, v1, v2, v3}, Lhbu;-><init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput-object v0, p0, Lhar;->aj:Lhca;

    iget-object v0, p0, Lhar;->aj:Lhca;

    invoke-interface {v0}, Lhca;->a()V

    :cond_4
    :try_start_0
    check-cast p1, Lhbp;

    iput-object p1, p0, Lhar;->aW:Lhbp;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Activity must implement PaymentsDialogEventListener"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    const-string v0, "immediateFullWalletRequest"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "immediateFullWalletRequest"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v0, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v0, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g()Z

    move-result v0

    if-nez v0, :cond_7

    :goto_5
    iput-boolean v1, p0, Lhar;->aI:Z

    iget-object v0, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f()Z

    move-result v0

    iput-boolean v0, p0, Lhar;->aC:Z

    iget-object v0, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhar;->aD:Z

    iget-boolean v0, p0, Lhar;->aC:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lhfx;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lhar;->aE:Ljava/util/ArrayList;

    goto :goto_4

    :cond_7
    move v1, v2

    goto :goto_5

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must specify masked wallet request or immediate full wallet request"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    move v1, v2

    goto/16 :goto_3

    :cond_a
    move v0, v2

    goto/16 :goto_2
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v2, 0x0

    iput-boolean v0, p0, Lhar;->aF:Z

    iput-boolean v2, p0, Lhar;->aS:Z

    invoke-direct {p0, v0, v2}, Lhar;->a(ZI)V

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Intent;I)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v1, 0x7f050011    # com.google.android.gms.R.anim.wallet_push_up_in

    invoke-virtual {v0, v1, v2}, Lo;->overridePendingTransition(II)V

    return-void
.end method

.method public final a(Lioj;)V
    .locals 1

    invoke-direct {p0}, Lhar;->X()V

    iget-object v0, p0, Lhar;->e:Lgxg;

    invoke-interface {v0, p1}, Lgxg;->a(Lioj;)V

    return-void
.end method

.method public final a(Lioj;Lipv;)V
    .locals 0

    invoke-direct {p0}, Lhar;->X()V

    invoke-direct {p0, p1, p2}, Lhar;->b(Lioj;Lipv;)V

    return-void
.end method

.method public final a(Lipv;)V
    .locals 0

    invoke-direct {p0}, Lhar;->X()V

    invoke-direct {p0, p1}, Lhar;->c(Lipv;)V

    return-void
.end method

.method public final a(Ljai;)V
    .locals 1

    iget-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Ljai;

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, -0x1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lhar;->q()V

    if-eqz p1, :cond_5

    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhar;->aF:Z

    const-string v0, "dismissedRequiredAction"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhar;->aL:Z

    const-string v0, "walletItemsReceived"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhar;->al:Z

    const-string v0, "performContinueClickAfterWalletItemsReceived"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhar;->aG:Z

    const-string v0, "isChromeSignUpFlow"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhar;->aH:Z

    const-string v0, "getWalletItemsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "getWalletItemsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lizz;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lizz;

    iput-object v0, p0, Lhar;->an:Lizz;

    :cond_0
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lhar;->ao:I

    :cond_1
    const-string v0, "pendingResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "pendingResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lhar;->aX:I

    :cond_2
    const-string v0, "pendingResultData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "pendingResultData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lhar;->aY:Landroid/content/Intent;

    :cond_3
    const-string v0, "fullWalletRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "fullWalletRequest"

    const-class v1, Ljau;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljau;

    iput-object v0, p0, Lhar;->aA:Ljau;

    :cond_4
    :goto_0
    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhar;->av:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhfx;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Ljan;)Ljbg;

    move-result-object v0

    iput-object v0, p0, Lhar;->aT:Ljbg;

    :goto_1
    return-void

    :cond_5
    new-instance v0, Luu;

    const-string v1, "get_wallet_items"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhar;->aO:Luu;

    iget-object v0, p0, Lhar;->aO:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lhar;->aP:Lut;

    new-instance v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    iput-object v0, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    const-string v2, "googleTransactionId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    const-string v1, "selectedInstrumentId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v2, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    new-instance v3, Lioj;

    invoke-direct {v3}, Lioj;-><init>()V

    iput-object v3, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v2, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iput-object v1, v2, Lioj;->a:Ljava/lang/String;

    :cond_6
    const-string v1, "selectedAddressId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v2, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    new-instance v3, Lipv;

    invoke-direct {v3}, Lipv;-><init>()V

    iput-object v3, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v2, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iput-object v1, v2, Lipv;->b:Ljava/lang/String;

    :cond_7
    const-string v1, "startingContentHeight"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lhar;->aU:I

    const-string v1, "startingContentHeight"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string v1, "startingContentWidth"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lhar;->aV:I

    const-string v1, "startingContentWidth"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhar;->au:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "choose_methods"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lhar;->aw:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v0}, Lhfx;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Ljbg;

    move-result-object v0

    iput-object v0, p0, Lhar;->aT:Ljbg;

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 0

    invoke-direct {p0}, Lhar;->X()V

    return-void
.end method

.method public final b(Lipv;)V
    .locals 1

    invoke-direct {p0}, Lhar;->X()V

    iget-object v0, p0, Lhar;->f:Lgvt;

    invoke-interface {v0, p1}, Lgvt;->a(Lipv;)V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lhar;->M()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lhar;->ao:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "model"

    iget-object v1, p0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lhar;->aF:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "dismissedRequiredAction"

    iget-boolean v1, p0, Lhar;->aL:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "walletItemsReceived"

    iget-boolean v1, p0, Lhar;->al:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "performContinueClickAfterWalletItemsReceived"

    iget-boolean v1, p0, Lhar;->aG:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "isChromeSignUpFlow"

    iget-boolean v1, p0, Lhar;->aH:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lhar;->an:Lizz;

    if-eqz v0, :cond_0

    const-string v0, "getWalletItemsResponse"

    iget-object v1, p0, Lhar;->an:Lizz;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    iget v0, p0, Lhar;->aX:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_1

    const-string v0, "pendingResultCode"

    iget v1, p0, Lhar;->aX:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lhar;->aY:Landroid/content/Intent;

    if-eqz v0, :cond_2

    const-string v0, "pendingResultData"

    iget-object v1, p0, Lhar;->aY:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    iget-object v0, p0, Lhar;->aA:Ljau;

    if-eqz v0, :cond_3

    const-string v0, "fullWalletRequest"

    iget-object v1, p0, Lhar;->aA:Ljau;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_3
    return-void
.end method

.method public final w()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget-boolean v0, p0, Lhar;->aF:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhar;->am:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lhar;->L()V

    goto :goto_0
.end method

.method public final x()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhar;->aS:Z

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->x()V

    invoke-direct {p0}, Lhar;->M()V

    return-void
.end method
