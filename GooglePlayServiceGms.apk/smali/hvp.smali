.class public final Lhvp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field b:Landroid/location/Location;

.field c:Landroid/location/Location;

.field d:Landroid/location/Location;

.field e:J

.field f:Landroid/location/Location;

.field g:Landroid/location/Location;

.field h:Z

.field i:Z

.field j:Z

.field k:Liwp;

.field final l:Lhwr;

.field final m:Lhvw;

.field public final n:Lhwb;

.field private final o:Liuo;

.field private final p:Ljava/lang/String;

.field private final q:Landroid/os/Handler;

.field private final r:Lhuz;

.field private s:Lhvv;

.field private t:J

.field private u:J

.field private v:Lhwh;

.field private w:I

.field private x:Lhwh;

.field private final y:Lhwf;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lhwk;Landroid/hardware/SensorManager;Lhuz;Liuo;Lhvv;)V
    .locals 11

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lhvp;->i:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lhvp;->j:Z

    new-instance v1, Lhvw;

    invoke-direct {v1}, Lhvw;-><init>()V

    iput-object v1, p0, Lhvp;->m:Lhvw;

    move-object/from16 v0, p7

    iput-object v0, p0, Lhvp;->o:Liuo;

    iput-object p1, p0, Lhvp;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhvp;->p:Ljava/lang/String;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lhvp;->q:Landroid/os/Handler;

    move-object/from16 v0, p6

    iput-object v0, p0, Lhvp;->r:Lhuz;

    new-instance v1, Lhwr;

    move-object/from16 v0, p6

    invoke-direct {v1, v0}, Lhwr;-><init>(Lhuz;)V

    iput-object v1, p0, Lhvp;->l:Lhwr;

    move-object/from16 v0, p8

    iput-object v0, p0, Lhvp;->s:Lhvv;

    const/4 v1, 0x0

    iput-object v1, p0, Lhvp;->v:Lhwh;

    const/4 v1, 0x0

    iput-object v1, p0, Lhvp;->x:Lhwh;

    const/4 v1, 0x0

    iput v1, p0, Lhvp;->w:I

    new-instance v1, Liwp;

    invoke-direct {v1}, Liwp;-><init>()V

    iput-object v1, p0, Lhvp;->k:Liwp;

    new-instance v1, Lhwf;

    invoke-direct {v1, p3}, Lhwf;-><init>(Landroid/location/LocationManager;)V

    iput-object v1, p0, Lhvp;->y:Lhwf;

    new-instance v9, Lhwp;

    invoke-direct {v9, p1}, Lhwp;-><init>(Landroid/content/Context;)V

    new-instance v7, Lhux;

    iget-object v1, p0, Lhvp;->q:Landroid/os/Handler;

    move-object/from16 v0, p5

    invoke-direct {v7, p1, v0, v1}, Lhux;-><init>(Landroid/content/Context;Landroid/hardware/SensorManager;Landroid/os/Handler;)V

    new-instance v8, Lhvq;

    invoke-direct {v8, p0, v7}, Lhvq;-><init>(Lhvp;Lhux;)V

    new-instance v10, Lhwb;

    new-instance v1, Lhwz;

    new-instance v3, Lhvt;

    invoke-direct {v3, p0}, Lhvt;-><init>(Lhvp;)V

    iget-object v4, p0, Lhvp;->y:Lhwf;

    iget-object v5, p0, Lhvp;->m:Lhvw;

    move-object v2, p4

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lhwz;-><init>(Lhwk;Landroid/location/LocationListener;Lhwf;Lhvw;Landroid/os/Looper;)V

    new-instance v4, Lhxd;

    new-instance v2, Lhvu;

    invoke-direct {v2, p0}, Lhvu;-><init>(Lhvp;)V

    invoke-direct {v4, p4, v2, p2}, Lhxd;-><init>(Lhwk;Landroid/location/LocationListener;Landroid/os/Looper;)V

    new-instance v5, Lhxh;

    new-instance v2, Lhwx;

    iget-object v3, p0, Lhvp;->q:Landroid/os/Handler;

    move-object/from16 v0, p5

    invoke-direct {v2, v0, v8, v3}, Lhwx;-><init>(Landroid/hardware/SensorManager;Lixd;Landroid/os/Handler;)V

    invoke-direct {v5, v2, v7}, Lhxh;-><init>(Lhwx;Lhux;)V

    new-instance v6, Lhxc;

    iget-object v2, p0, Lhvp;->m:Lhvw;

    iget-object v3, p0, Lhvp;->l:Lhwr;

    invoke-direct {v6, v9, v2, v3}, Lhxc;-><init>(Lhwp;Lhvw;Lhwr;)V

    new-instance v7, Lhxb;

    iget-object v2, p0, Lhvp;->m:Lhvw;

    iget-object v3, p0, Lhvp;->l:Lhwr;

    invoke-direct {v7, v9, v2, v3}, Lhxb;-><init>(Lhwp;Lhvw;Lhwr;)V

    new-instance v8, Lhwy;

    iget-object v2, p0, Lhvp;->m:Lhvw;

    invoke-direct {v8, v9, v2}, Lhwy;-><init>(Lhwp;Lhvw;)V

    move-object v2, v10

    move-object v3, v1

    move-object/from16 v9, p5

    invoke-direct/range {v2 .. v9}, Lhwb;-><init>(Lhwz;Lhxd;Lhxh;Lhxc;Lhxb;Lhwy;Landroid/hardware/SensorManager;)V

    iput-object v10, p0, Lhvp;->n:Lhwb;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_providers_allowed"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    new-instance v4, Lhvr;

    iget-object v5, p0, Lhvp;->q:Landroid/os/Handler;

    invoke-direct {v4, p0, v5, v1}, Lhvr;-><init>(Lhvp;Landroid/os/Handler;Landroid/content/ContentResolver;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string v2, "mock_location"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    new-instance v4, Lhvs;

    iget-object v5, p0, Lhvp;->q:Landroid/os/Handler;

    invoke-direct {v4, p0, v5, v1}, Lhvs;-><init>(Lhvp;Landroid/os/Handler;Landroid/content/ContentResolver;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lhvv;)V
    .locals 9

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/LocationManager;

    new-instance v4, Lhwk;

    invoke-direct {v4, p1}, Lhwk;-><init>(Landroid/content/Context;)V

    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/SensorManager;

    new-instance v6, Lhuz;

    invoke-direct {v6}, Lhuz;-><init>()V

    new-instance v7, Liup;

    invoke-direct {v7}, Liup;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lhvp;-><init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lhwk;Landroid/hardware/SensorManager;Lhuz;Liuo;Lhvv;)V

    return-void
.end method

.method static synthetic a(Lhvp;)V
    .locals 2

    iget-object v0, p0, Lhvp;->d:Landroid/location/Location;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lhvp;->d:Landroid/location/Location;

    iget-object v0, p0, Lhvp;->b:Landroid/location/Location;

    iget-object v1, p0, Lhvp;->f:Landroid/location/Location;

    invoke-virtual {p0, v0, v1}, Lhvp;->a(Landroid/location/Location;Landroid/location/Location;)V

    :cond_0
    return-void
.end method

.method private static a(I)Z
    .locals 1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/location/Location;Landroid/location/Location;)V
    .locals 2

    if-eqz p0, :cond_0

    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    const-string v1, "fused"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    const-string v1, "noGPSLocation"

    invoke-static {p1, v1, v0}, Lilk;->a(Landroid/location/Location;Ljava/lang/String;Landroid/location/Location;)V

    :cond_0
    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lhvp;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhvp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbfy;->a(Landroid/content/Context;)V

    iput-boolean v3, p0, Lhvp;->h:Z

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhvp;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lhvp;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "GCoreFlp"

    const-string v1, "Unable to start the NLPLocationReceiverService"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lhvp;->n:Lhwb;

    invoke-virtual {v0}, Lhwb;->b()V

    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Engine enabled (%s)"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lhvp;->p:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final a(II)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p1, p2, :cond_1

    invoke-static {p2}, Lhvp;->a(I)Z

    move-result v2

    const-string v3, "GCoreFlp"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "Filter state changed from %s to %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v3, p0, Lhvp;->m:Lhvw;

    invoke-virtual {v3, p2}, Lhvw;->b(I)V

    invoke-static {p1}, Lhvp;->a(I)Z

    move-result v3

    if-eq v3, v2, :cond_1

    iget-object v3, p0, Lhvp;->n:Lhwb;

    if-nez v2, :cond_2

    :goto_0
    invoke-virtual {v3, v0}, Lhwb;->a(Z)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method final a(Landroid/location/Location;Landroid/location/Location;)V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lhvp;->c:Landroid/location/Location;

    iput-object v0, p0, Lhvp;->g:Landroid/location/Location;

    if-eqz p1, :cond_0

    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    const-string v1, "fused"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    iget-object v1, p0, Lhvp;->b:Landroid/location/Location;

    invoke-static {v1, v0}, Lhvp;->b(Landroid/location/Location;Landroid/location/Location;)V

    iput-object v0, p0, Lhvp;->c:Landroid/location/Location;

    :cond_0
    if-eqz p2, :cond_1

    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p2}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    const-string v1, "fused"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    iget-object v1, p0, Lhvp;->f:Landroid/location/Location;

    invoke-static {v1, v0}, Lhvp;->b(Landroid/location/Location;Landroid/location/Location;)V

    iput-object v0, p0, Lhvp;->g:Landroid/location/Location;

    :cond_1
    iget-object v0, p0, Lhvp;->k:Liwp;

    iget-object v1, v0, Liwp;->c:Liwg;

    invoke-virtual {v1}, Liwg;->b()V

    iget-object v1, v0, Liwp;->b:Lixa;

    invoke-virtual {v1}, Lixa;->c()V

    iget-object v1, v0, Liwp;->a:Liwm;

    invoke-virtual {v1}, Liwm;->f()V

    new-instance v1, Liwr;

    invoke-direct {v1, v0}, Liwr;-><init>(Liwp;)V

    iput-object v1, v0, Liwp;->i:Liws;

    return-void
.end method

.method final a(Landroid/location/Location;Liwf;)V
    .locals 16

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lhvp;->h:Z

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->s:Lhvv;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lhvp;->r:Lhuz;

    invoke-static/range {p1 .. p1}, Lhuz;->a(Landroid/location/Location;)J

    move-result-wide v13

    const-string v3, "network"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->b:Landroid/location/Location;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->b:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    const-string v1, "GCoreFlp"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Dropping duplicate NLP location"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const-string v1, "cell"

    invoke-static/range {p1 .. p1}, Lilk;->c(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->m:Lhvw;

    invoke-virtual {v1}, Lhvw;->c()V

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lhvp;->i:Z

    if-nez v1, :cond_4

    const-string v1, "GCoreFlp"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Dropping NLP location because NLP provider disabled"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->m:Lhvw;

    invoke-virtual {v1}, Lhvw;->b()V

    goto :goto_1

    :cond_4
    new-instance v1, Landroid/location/Location;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lhvp;->b:Landroid/location/Location;

    move-object/from16 v0, p0

    iput-wide v13, v0, Lhvp;->t:J

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->r:Lhuz;

    invoke-static/range {p1 .. p1}, Lhuz;->a(Landroid/location/Location;)J

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_11

    const-string v5, "levelId"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "levelNumberE3"

    const v7, 0x7fffffff

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    if-eqz v5, :cond_11

    const v1, 0x7fffffff

    if-eq v6, v1, :cond_10

    new-instance v1, Lhwh;

    invoke-direct {v1, v5, v6, v3, v4}, Lhwh;-><init>(Ljava/lang/String;IJ)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lhvp;->v:Lhwh;

    invoke-static {v1, v5}, Lirf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget v5, v0, Lhvp;->w:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lhvp;->w:I

    :goto_3
    move-object/from16 v0, p0

    iput-object v1, v0, Lhvp;->v:Lhwh;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->x:Lhwh;

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget v1, v0, Lhvp;->w:I

    const/4 v5, 0x2

    if-ge v1, v5, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->x:Lhwh;

    iget-wide v5, v1, Lhwh;->d:J

    sub-long/2addr v3, v5

    const-wide v5, 0x4a817c800L

    cmp-long v1, v3, v5

    if-lez v1, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->v:Lhwh;

    move-object/from16 v0, p0

    iput-object v1, v0, Lhvp;->x:Lhwh;

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->l:Lhwr;

    iget-object v3, v1, Lhwr;->a:Ljava/lang/Object;

    monitor-enter v3

    const/4 v4, 0x0

    :try_start_0
    iput-boolean v4, v1, Lhwr;->f:Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v12, v2

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->x:Lhwh;

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->x:Lhwh;

    iget-wide v1, v1, Lhwh;->d:J

    sub-long v1, v13, v1

    const-wide v3, 0x9502f9000L

    cmp-long v1, v1, v3

    if-lez v1, :cond_7

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lhvp;->x:Lhwh;

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->k:Liwp;

    invoke-virtual {v1}, Liwp;->b()I

    move-result v15

    if-nez p1, :cond_18

    const/4 v1, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lhvp;->k:Liwp;

    const-wide v2, 0x3ffb333333333333L    # 1.7

    iget-wide v5, v4, Liwp;->d:J

    long-to-double v5, v5

    mul-double/2addr v2, v5

    double-to-long v2, v2

    iget-object v5, v1, Liwe;->b:Liwf;

    sget-object v6, Liwf;->a:Liwf;

    if-ne v5, v6, :cond_8

    iget-wide v5, v4, Liwp;->f:J

    sub-long v2, v13, v2

    cmp-long v2, v5, v2

    if-gez v2, :cond_a

    :cond_8
    iget-object v2, v4, Liwp;->i:Liws;

    invoke-interface {v2, v13, v14, v1}, Liws;->a(JLiwe;)V

    sget-object v2, Liwq;->a:[I

    iget-object v3, v1, Liwe;->b:Liwf;

    invoke-virtual {v3}, Liwf;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_9
    :goto_6
    iget-object v1, v4, Liwp;->i:Liws;

    invoke-interface {v1, v13, v14}, Liws;->a(J)Liws;

    move-result-object v1

    iput-object v1, v4, Liwp;->i:Liws;

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->k:Liwp;

    invoke-virtual {v1}, Liwp;->a()Liwe;

    move-result-object v3

    const-string v1, "GCoreFlp"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Filtered position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->k:Liwp;

    invoke-virtual {v1}, Liwp;->b()I

    move-result v4

    const-string v5, "fused"

    invoke-static {v4}, Lhvp;->a(I)Z

    move-result v1

    if-eqz v1, :cond_25

    const/4 v1, 0x0

    :goto_7
    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    if-nez v3, :cond_26

    const/4 v1, 0x0

    move-object v2, v1

    :goto_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v4}, Lhvp;->a(II)V

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->b:Landroid/location/Location;

    invoke-static {v1, v2}, Lhvp;->b(Landroid/location/Location;Landroid/location/Location;)V

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhvp;->t:J

    sub-long v3, v13, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->b:Landroid/location/Location;

    if-eqz v1, :cond_c

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    sget-object v1, Lijs;->m:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-gtz v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->b:Landroid/location/Location;

    if-eqz v1, :cond_c

    const-string v3, "dbgProtoBuf"

    invoke-static {v1, v3}, Lilk;->b(Landroid/location/Location;Ljava/lang/String;)[B

    move-result-object v1

    const-string v3, "dbgProtoBuf"

    invoke-static {v2, v3, v1}, Lilk;->a(Landroid/location/Location;Ljava/lang/String;[B)V

    :cond_c
    invoke-static/range {p1 .. p1}, Lilk;->e(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-static {v2}, Lilk;->f(Landroid/location/Location;)V

    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->s:Lhvv;

    invoke-interface {v1, v2}, Lhvv;->a(Landroid/location/Location;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->n:Lhwb;

    invoke-virtual {v1, v2}, Lhwb;->a(Landroid/location/Location;)V

    :cond_e
    move-object/from16 v0, p0

    iput-object v2, v0, Lhvp;->c:Landroid/location/Location;

    move-object/from16 v0, p0

    iput-wide v13, v0, Lhvp;->e:J

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->g:Landroid/location/Location;

    if-eqz v1, :cond_f

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhvp;->u:J

    sub-long v3, v13, v3

    const-wide v5, 0x8bb2c97000L

    cmp-long v1, v3, v5

    if-lez v1, :cond_0

    :cond_f
    move-object/from16 v0, p0

    iput-object v2, v0, Lhvp;->g:Landroid/location/Location;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->b:Landroid/location/Location;

    move-object/from16 v0, p0

    iput-object v1, v0, Lhvp;->f:Landroid/location/Location;

    move-object/from16 v0, p0

    iput-wide v13, v0, Lhvp;->u:J

    goto/16 :goto_0

    :cond_10
    new-instance v1, Lhwh;

    invoke-direct {v1, v5, v3, v4}, Lhwh;-><init>(Ljava/lang/String;J)V

    goto/16 :goto_2

    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_12
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lhvp;->w:I

    goto/16 :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1

    :cond_13
    const-string v3, "gps"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lhvp;->d:Landroid/location/Location;

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lhvp;->d:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_14

    const-string v1, "GCoreFlp"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Dropping duplicate GPS location"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvp;->m:Lhvw;

    invoke-virtual {v2}, Lhvw;->a()V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhvp;->j:Z

    if-nez v2, :cond_15

    const-string v1, "GCoreFlp"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Dropping GPS location because GPS provider disabled"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lhvp;->o:Liuo;

    invoke-interface {v2}, Liuo;->a()J

    move-result-wide v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhvp;->y:Lhwf;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lhwf;->a(Landroid/location/Location;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Landroid/location/Location;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhvp;->d:Landroid/location/Location;

    move v12, v1

    goto/16 :goto_4

    :cond_16
    const-string v1, "fused"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->m:Lhvw;

    invoke-virtual {v1}, Lhvw;->d()V

    :cond_17
    const-string v1, "GCoreFlp"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Dropping location from unknown provider"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_18
    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    const-wide v3, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v1, v3

    double-to-int v2, v1

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    const-wide v5, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    const/high16 v4, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    const/4 v7, -0x1

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    float-to-int v7, v1

    :cond_19
    const-wide/16 v9, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v9

    :cond_1a
    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getSpeed()F

    move-result v8

    :cond_1b
    if-nez p2, :cond_2d

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    const-string v5, "gps"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1d

    sget-object v11, Liwf;->a:Liwf;

    :cond_1c
    :goto_9
    new-instance v1, Liwe;

    const-string v5, ""

    const-string v6, ""

    invoke-direct/range {v1 .. v11}, Liwe;-><init>(IIILjava/lang/String;Ljava/lang/String;IFDLiwf;)V

    goto/16 :goto_5

    :cond_1d
    const-string v5, "network"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    sget-object v11, Liwf;->b:Liwf;

    const-string v1, "cell"

    invoke-static/range {p1 .. p1}, Lilk;->c(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    sget-object v11, Liwf;->c:Liwf;

    goto :goto_9

    :cond_1e
    sget-object v11, Liwf;->e:Liwf;

    goto :goto_9

    :pswitch_0
    iget-wide v2, v4, Liwp;->e:J

    const-wide/16 v5, 0x0

    cmp-long v2, v2, v5

    if-lez v2, :cond_20

    iget-wide v2, v4, Liwp;->e:J

    sub-long v2, v13, v2

    :goto_a
    const-wide v5, 0x37e11d600L

    cmp-long v2, v2, v5

    if-lez v2, :cond_1f

    iget-object v2, v4, Liwp;->c:Liwg;

    invoke-virtual {v2}, Liwg;->b()V

    :cond_1f
    iget-object v2, v4, Liwp;->c:Liwg;

    iput-object v1, v2, Liwg;->a:Liwe;

    iput-wide v13, v2, Liwg;->b:J

    iput-wide v13, v4, Liwp;->e:J

    iget-object v1, v1, Liwe;->b:Liwf;

    sget-object v2, Liwf;->d:Liwf;

    if-ne v1, v2, :cond_9

    iput-wide v13, v4, Liwp;->f:J

    goto/16 :goto_6

    :cond_20
    const-wide/16 v2, 0x0

    goto :goto_a

    :pswitch_1
    iget-wide v2, v4, Liwp;->g:J

    const-wide/16 v5, 0x0

    cmp-long v2, v2, v5

    if-lez v2, :cond_22

    iget-wide v2, v4, Liwp;->g:J

    sub-long v2, v13, v2

    :goto_b
    const-wide v5, 0x37e11d600L

    cmp-long v2, v2, v5

    if-lez v2, :cond_21

    iget-object v2, v4, Liwp;->b:Lixa;

    invoke-virtual {v2}, Lixa;->c()V

    :cond_21
    iget-object v2, v4, Liwp;->b:Lixa;

    invoke-virtual {v2, v13, v14, v1}, Lixa;->a(JLiwe;)V

    iput-wide v13, v4, Liwp;->g:J

    goto/16 :goto_6

    :cond_22
    const-wide/16 v2, 0x0

    goto :goto_b

    :pswitch_2
    iget-wide v2, v4, Liwp;->h:J

    const-wide/16 v5, 0x0

    cmp-long v2, v2, v5

    if-lez v2, :cond_24

    iget-wide v2, v4, Liwp;->h:J

    sub-long v2, v13, v2

    :goto_c
    const-wide v5, 0x37e11d600L

    cmp-long v2, v2, v5

    if-lez v2, :cond_23

    iget-object v2, v4, Liwp;->a:Liwm;

    invoke-virtual {v2}, Liwm;->f()V

    :cond_23
    iget-object v2, v4, Liwp;->a:Liwm;

    invoke-virtual {v2, v13, v14, v1}, Liwm;->a(JLiwe;)V

    iput-wide v13, v4, Liwp;->h:J

    goto/16 :goto_6

    :cond_24
    const-wide/16 v2, 0x0

    goto :goto_c

    :cond_25
    move-object/from16 v0, p0

    iget-object v1, v0, Lhvp;->x:Lhwh;

    goto/16 :goto_7

    :cond_26
    new-instance v2, Landroid/location/Location;

    invoke-direct {v2, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget v5, v3, Liwe;->e:I

    int-to-float v5, v5

    const/high16 v8, 0x447a0000    # 1000.0f

    div-float/2addr v5, v8

    invoke-virtual {v2, v5}, Landroid/location/Location;->setAccuracy(F)V

    iget v5, v3, Liwe;->j:I

    const/4 v8, -0x1

    if-eq v5, v8, :cond_27

    iget v5, v3, Liwe;->j:I

    int-to-float v5, v5

    invoke-virtual {v2, v5}, Landroid/location/Location;->setBearing(F)V

    :cond_27
    iget v5, v3, Liwe;->f:F

    const/4 v8, 0x0

    cmpl-float v5, v5, v8

    if-eqz v5, :cond_28

    iget v5, v3, Liwe;->f:F

    invoke-virtual {v2, v5}, Landroid/location/Location;->setSpeed(F)V

    :cond_28
    iget-wide v8, v3, Liwe;->g:D

    const-wide/16 v10, 0x0

    cmpl-double v5, v8, v10

    if-eqz v5, :cond_29

    iget-wide v8, v3, Liwe;->g:D

    invoke-virtual {v2, v8, v9}, Landroid/location/Location;->setAltitude(D)V

    :cond_29
    iget v5, v3, Liwe;->c:I

    int-to-double v8, v5

    const-wide v10, 0x416312d000000000L    # 1.0E7

    div-double/2addr v8, v10

    invoke-virtual {v2, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    iget v5, v3, Liwe;->d:I

    int-to-double v8, v5

    const-wide v10, 0x416312d000000000L    # 1.0E7

    div-double/2addr v8, v10

    invoke-virtual {v2, v8, v9}, Landroid/location/Location;->setLongitude(D)V

    invoke-virtual {v2, v6, v7}, Landroid/location/Location;->setTime(J)V

    const/16 v5, 0x11

    invoke-static {v5}, Lbpz;->a(I)Z

    move-result v5

    if-eqz v5, :cond_2a

    invoke-virtual {v2, v13, v14}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    :cond_2a
    if-eqz v1, :cond_2c

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "levelId"

    iget-object v7, v1, Lhwh;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v6, v1, Lhwh;->c:Z

    if-eqz v6, :cond_2b

    const-string v6, "levelNumberE3"

    iget v1, v1, Lhwh;->b:I

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2b
    invoke-virtual {v2, v5}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    :cond_2c
    const/4 v1, 0x0

    sget-object v5, Lhwj;->a:[I

    iget-object v3, v3, Liwe;->b:Liwf;

    invoke-virtual {v3}, Liwf;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_1

    :goto_d
    invoke-static {v2, v1}, Lilk;->d(Landroid/location/Location;Ljava/lang/String;)V

    goto/16 :goto_8

    :pswitch_3
    const-string v1, "gps"

    goto :goto_d

    :pswitch_4
    const-string v1, "wifi"

    goto :goto_d

    :pswitch_5
    const-string v1, "cell"

    goto :goto_d

    :cond_2d
    move-object/from16 v11, p2

    goto/16 :goto_9

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method final a(Ljava/lang/Iterable;Z)V
    .locals 3

    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Location requests set in engine: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lhvp;->n:Lhwb;

    invoke-virtual {v0, p1, p2}, Lhwb;->a(Ljava/lang/Iterable;Z)V

    return-void
.end method

.method final b()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lhvp;->h:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v3, p0, Lhvp;->h:Z

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhvp;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lhvp;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    iget-object v0, p0, Lhvp;->n:Lhwb;

    invoke-virtual {v0}, Lhwb;->c()V

    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Engine disabled (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lhvp;->p:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
