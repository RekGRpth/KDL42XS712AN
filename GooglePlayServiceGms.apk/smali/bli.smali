.class public final Lbli;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field private b:Z

.field private c:Lblk;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizk;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbli;->a:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lbli;->c:Lblk;

    const/4 v0, -0x1

    iput v0, p0, Lbli;->d:I

    return-void
.end method

.method public static a([B)Lbli;
    .locals 2

    new-instance v0, Lbli;

    invoke-direct {v0}, Lbli;-><init>()V

    array-length v1, p0

    invoke-super {v0, p0, v1}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Lbli;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lbli;->d:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lbli;->b()I

    :cond_0
    iget v0, p0, Lbli;->d:I

    return v0
.end method

.method public final a(Lblk;)Lbli;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lbli;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbli;->a:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lbli;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(I)Lblk;
    .locals 1

    iget-object v0, p0, Lbli;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblk;

    return-object v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lblk;

    invoke-direct {v0}, Lblk;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lbli;->a(Lblk;)Lbli;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lblk;

    invoke-direct {v0}, Lblk;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lbli;->b:Z

    iput-object v0, p0, Lbli;->c:Lblk;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-object v0, p0, Lbli;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblk;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILizk;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lbli;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lbli;->c:Lblk;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_1
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lbli;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblk;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lbli;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v2, p0, Lbli;->c:Lblk;

    invoke-static {v0, v2}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_1
    iput v1, p0, Lbli;->d:I

    return v1
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lbli;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
