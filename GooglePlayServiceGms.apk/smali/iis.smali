.class public final Liis;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/accounts/Account;

.field b:Ljava/lang/Boolean;

.field c:Ljava/lang/Long;

.field public d:Z

.field e:Z

.field f:Z

.field public g:Z

.field h:I

.field i:Ljava/lang/Boolean;

.field j:Ljava/lang/Boolean;

.field k:Ljava/lang/Long;

.field l:Ljava/lang/Integer;

.field public m:Ljava/lang/Boolean;

.field n:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Landroid/accounts/Account;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Liis;->d:Z

    iput-boolean v0, p0, Liis;->e:Z

    iput-boolean v0, p0, Liis;->f:Z

    iput-boolean v0, p0, Liis;->g:Z

    iput v0, p0, Liis;->h:I

    const-string v0, "null account"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Liis;->a:Landroid/accounts/Account;

    return-void
.end method

.method synthetic constructor <init>(Landroid/accounts/Account;B)V
    .locals 0

    invoke-direct {p0, p1}, Liis;-><init>(Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a()Liir;
    .locals 1

    new-instance v0, Liir;

    invoke-direct {v0, p0}, Liir;-><init>(Liis;)V

    return-object v0
.end method

.method public final a(I)Liis;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Liis;->l:Ljava/lang/Integer;

    return-object p0
.end method

.method public final a(J)Liis;
    .locals 1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Liis;->c:Ljava/lang/Long;

    return-object p0
.end method

.method final a(Lihk;)Liis;
    .locals 1

    iget-object v0, p1, Lihk;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Liis;->i:Ljava/lang/Boolean;

    iget-object v0, p1, Lihk;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Liis;->j:Ljava/lang/Boolean;

    iget-object v0, p1, Lihk;->a:Ljava/lang/Long;

    iput-object v0, p0, Liis;->k:Ljava/lang/Long;

    iget v0, p1, Lihk;->d:I

    invoke-virtual {p0, v0}, Liis;->a(I)Liis;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Liis;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Liis;->h:I

    return-object p0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Liis;
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Liis;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Liis;->b:Ljava/lang/Boolean;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Liis;->a(Z)Liis;

    move-result-object v0

    invoke-virtual {v0, v1}, Liis;->b(Z)Liis;

    move-result-object v0

    iput-boolean v1, v0, Liis;->d:Z

    return-object v0
.end method

.method public final b(Z)Liis;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Liis;->n:Ljava/lang/Boolean;

    return-object p0
.end method
