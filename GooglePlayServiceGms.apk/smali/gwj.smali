.class public final Lgwj;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgxf;
.implements Lgyq;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field Y:Lgwn;

.field Z:Lgwd;

.field private aa:Z

.field private ab:I

.field private ac:Lgyi;

.field private ad:Ljava/util/HashMap;

.field private ae:Ljava/lang/String;

.field private af:Ljava/util/ArrayList;

.field private ag:J

.field private ah:I

.field private ai:[I

.field private aj:[I

.field private final ak:Landroid/text/TextWatcher;

.field private final al:Lhcb;

.field b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

.field c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field f:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

.field g:Landroid/widget/TextView;

.field h:Lgwx;

.field i:Lgwy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "ccEntry"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgwj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgwj;->aa:Z

    const/4 v0, -0x1

    iput v0, p0, Lgwj;->ab:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgwj;->ad:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lgwj;->ae:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgwj;->af:Ljava/util/ArrayList;

    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lgwj;->ag:J

    const/4 v0, 0x0

    iput v0, p0, Lgwj;->ah:I

    new-instance v0, Lgwk;

    invoke-direct {v0, p0}, Lgwk;-><init>(Lgwj;)V

    iput-object v0, p0, Lgwj;->ak:Landroid/text/TextWatcher;

    new-instance v0, Lgwl;

    invoke-direct {v0, p0}, Lgwl;-><init>(Lgwj;)V

    iput-object v0, p0, Lgwj;->al:Lhcb;

    return-void
.end method

.method private L()V
    .locals 2

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-boolean v1, p0, Lgwj;->aa:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgwj;->aa:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgwj;->aa:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgwj;->aa:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private M()Lint;
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, Lgth;->a(Ljava/lang/String;)I

    move-result v5

    :try_start_0
    iget-object v0, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    :try_start_1
    iget-object v6, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit16 v6, v6, 0x7d0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    :goto_1
    new-instance v6, Lint;

    invoke-direct {v6}, Lint;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v6, Lint;->b:I

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v6, Lint;->c:I

    :cond_1
    if-eqz v5, :cond_2

    iput v5, v6, Lint;->f:I

    :cond_2
    iput-object v4, v6, Lint;->e:Ljava/lang/String;

    new-instance v0, Linu;

    invoke-direct {v0}, Linu;-><init>()V

    iput-object v0, v6, Lint;->a:Linu;

    iget-object v0, v6, Lint;->a:Linu;

    iput-object v3, v0, Linu;->b:Ljava/lang/String;

    iget-object v0, v6, Lint;->a:Linu;

    iput-object v2, v0, Linu;->a:Ljava/lang/String;

    return-object v6

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v6

    goto :goto_1
.end method

.method private N()V
    .locals 2

    iget v0, p0, Lgwj;->ab:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lgwj;->Q()Lgyi;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lgwj;->Q()Lgyi;

    move-result-object v0

    iget-object v0, v0, Lgyi;->a:Lhca;

    iget-object v1, p0, Lgwj;->al:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lgwj;->ab:I

    :cond_0
    return-void
.end method

.method private O()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private P()V
    .locals 1

    invoke-direct {p0}, Lgwj;->O()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgwj;->b(Ljava/lang/String;)V

    return-void
.end method

.method private Q()Lgyi;
    .locals 2

    iget-object v0, p0, Lgwj;->ac:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->K_()Lu;

    move-result-object v0

    sget-object v1, Lgwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lgwj;->ac:Lgyi;

    :cond_0
    iget-object v0, p0, Lgwj;->ac:Lgyi;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;I[I[I)Lgwj;
    .locals 3

    new-instance v0, Lgwj;

    invoke-direct {v0}, Lgwj;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "cardEntryContext"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lgwj;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lgwj;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lgwj;->O()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lgwj;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lgwj;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    if-nez p1, :cond_1

    invoke-direct {p0, v1}, Lgwj;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgwj;->ad:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgwj;->ad:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lgwj;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1}, Lgwj;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lgwj;->ae:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lgwj;->Q()Lgyi;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lgwj;->ae:Ljava/lang/String;

    invoke-direct {p0}, Lgwj;->Q()Lgyi;

    move-result-object v0

    iget-object v0, v0, Lgyi;->a:Lhca;

    iget v1, p0, Lgwj;->ah:I

    invoke-interface {v0, p1, v1}, Lhca;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private b(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x4

    new-array v4, v0, [Lgyo;

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    aput-object v0, v4, v2

    iget-object v0, p0, Lgwj;->h:Lgwx;

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lgwj;->i:Lgwy;

    aput-object v3, v4, v0

    const/4 v0, 0x3

    iget-object v3, p0, Lgwj;->Y:Lgwn;

    aput-object v3, v4, v0

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method

.method static synthetic b(Lgwj;)[I
    .locals 1

    iget-object v0, p0, Lgwj;->aj:[I

    return-object v0
.end method

.method static synthetic c(Lgwj;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgwj;->ae:Ljava/lang/String;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lgwj;->g:Landroid/widget/TextView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgwj;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lgwj;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgwj;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lgwj;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic d(Lgwj;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lgwj;->ad:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic e(Lgwj;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lgwj;->ae:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lgwj;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lgwj;->af:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lgwj;)J
    .locals 2

    iget-wide v0, p0, Lgwj;->ag:J

    return-wide v0
.end method

.method static synthetic h(Lgwj;)Lhcb;
    .locals 1

    iget-object v0, p0, Lgwj;->al:Lhcb;

    return-object v0
.end method

.method static synthetic i(Lgwj;)J
    .locals 2

    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lgwj;->ag:J

    return-wide v0
.end method

.method static synthetic j(Lgwj;)V
    .locals 0

    invoke-direct {p0}, Lgwj;->P()V

    return-void
.end method

.method static synthetic k(Lgwj;)J
    .locals 4

    iget-wide v0, p0, Lgwj;->ag:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lgwj;->ag:J

    return-wide v0
.end method


# virtual methods
.method public final J()V
    .locals 4

    iget-object v0, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    const v2, 0x7f0b014d    # com.google.android.gms.R.string.wallet_error_creditcard_expiry_date_invalid

    invoke-virtual {p0, v2}, Lgwj;->b(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    iget-object v0, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    return-void
.end method

.method public final K()V
    .locals 4

    iget-object v0, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    const v2, 0x7f0b015d    # com.google.android.gms.R.string.wallet_error_cvc_invalid

    invoke-virtual {p0, v2}, Lgwj;->b(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    iget-object v0, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    return-void
.end method

.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgwj;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgwj;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v7, 0x7f0a0333    # com.google.android.gms.R.id.exp_year

    const v6, 0x7f0a0332    # com.google.android.gms.R.id.exp_month

    const v5, 0x7f0a031d    # com.google.android.gms.R.id.cvc

    const v0, 0x7f040130    # com.google.android.gms.R.layout.wallet_fragment_credit_card_entry

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a032d    # com.google.android.gms.R.id.card_number

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iput-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const v0, 0x7f0a031e    # com.google.android.gms.R.id.cvc_hint

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    iput-object v0, p0, Lgwj;->f:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    const v0, 0x7f0a032f    # com.google.android.gms.R.id.bin_warning

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgwj;->g:Landroid/widget/TextView;

    new-instance v0, Lgwn;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-direct {v0, v2, v3, v4}, Lgwn;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lgwj;->Y:Lgwn;

    new-instance v0, Lgww;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v2, v3, v4}, Lgww;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    new-instance v2, Lgwx;

    iget-object v3, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v4, v0}, Lgwx;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V

    iput-object v2, p0, Lgwj;->h:Lgwx;

    new-instance v2, Lgwy;

    iget-object v3, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v0}, Lgwy;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V

    iput-object v2, p0, Lgwj;->i:Lgwy;

    new-instance v0, Lgwd;

    iget-object v2, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-direct {v0, v2}, Lgwd;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lgwj;->Z:Lgwd;

    iget-object v0, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->Y:Lgwn;

    iget-object v3, p0, Lgwj;->Y:Lgwn;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->h:Lgwx;

    iget-object v3, p0, Lgwj;->h:Lgwx;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->i:Lgwy;

    iget-object v3, p0, Lgwj;->i:Lgwy;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v2, p0, Lgwj;->Z:Lgwd;

    iget-object v3, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->Y:Lgwn;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->h:Lgwx;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->i:Lgwy;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->Y:Lgwn;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->h:Lgwx;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->i:Lgwy;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v2, p0, Lgwj;->ak:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b()V

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v2, p0, Lgwj;->ai:[I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a([I)V

    invoke-direct {p0}, Lgwj;->L()V

    return-object v1
.end method

.method public final a()Linv;
    .locals 2

    new-instance v0, Linv;

    invoke-direct {v0}, Linv;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Linv;->a:I

    invoke-direct {p0}, Lgwj;->M()Lint;

    move-result-object v1

    iput-object v1, v0, Linv;->b:Lint;

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v2, "cardEntryContext"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lgwj;->ah:I

    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    iput-object v2, p0, Lgwj;->ai:[I

    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    iput-object v2, p0, Lgwj;->aj:[I

    invoke-direct {p0}, Lgwj;->Q()Lgyi;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "onlinewallet"

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    const/4 v2, 0x2

    invoke-static {v2, v0, v1}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lgwj;->ac:Lgyi;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->K_()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgwj;->ac:Lgyi;

    sget-object v2, Lgwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    iget-object v2, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lgzo;)V

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->R_()Z

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lgwj;->aa:Z

    invoke-direct {p0}, Lgwj;->L()V

    return-void
.end method

.method public final a([I)V
    .locals 2

    iget-object v0, p0, Lgwj;->ai:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lgwj;->ai:[I

    iget-object v0, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v1, p0, Lgwj;->ai:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a([I)V

    :cond_0
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgwj;->aa:Z

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    const v0, 0x7f0b0167    # com.google.android.gms.R.string.wallet_error_creditcard_number_invalid

    invoke-virtual {p0, v0}, Lgwj;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgwj;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lgwj;->N()V

    const-string v0, "enabled"

    iget-boolean v1, p0, Lgwj;->aa:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final i()Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x4

    new-array v3, v2, [Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    aput-object v2, v3, v1

    iget-object v2, p0, Lgwj;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lgwj;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    const/4 v2, 0x3

    iget-object v4, p0, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final w()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    invoke-direct {p0}, Lgwj;->Q()Lgyi;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lgwj;->Q()Lgyi;

    move-result-object v0

    iget-object v0, v0, Lgyi;->a:Lhca;

    iget-object v1, p0, Lgwj;->al:Lhcb;

    iget v2, p0, Lgwj;->ab:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    :cond_0
    invoke-direct {p0}, Lgwj;->P()V

    return-void
.end method

.method public final x()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->x()V

    invoke-direct {p0}, Lgwj;->N()V

    return-void
.end method
