.class public final Lhbj;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/SignupActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 0

    iput-object p1, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/16 v1, 0x19b

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    return-void
.end method

.method public final a(Lipy;)V
    .locals 4

    invoke-static {p1}, Lhfx;->a(Lipy;)I

    move-result v0

    iget-object v1, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->l(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z

    move-result v1

    invoke-static {p1, v1}, Lhfx;->a(Lipy;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lipy;->d:Ljak;

    iget-object v1, v1, Ljak;->c:Ljal;

    iget-object v2, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v3, v1, Ljal;->a:Ljava/lang/String;

    iget-object v1, v1, Ljal;->b:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x198

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    goto :goto_0
.end method

.method public final a(Ljav;J)V
    .locals 6

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lgrc;

    move-result-object v0

    iget-object v1, p1, Ljav;->g:Lipv;

    invoke-static {v1}, Lhfx;->a(Lipv;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgrc;->a(Lcom/google/android/gms/wallet/Address;)Lgrc;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/wallet/ProxyCard;

    iget-object v2, p1, Ljav;->e:Ljava/lang/String;

    iget-object v3, p1, Ljav;->f:Ljava/lang/String;

    iget v4, p1, Ljav;->c:I

    iget v5, p1, Ljav;->d:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/wallet/ProxyCard;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lgrc;->a(Lcom/google/android/gms/wallet/ProxyCard;)Lgrc;

    move-result-object v0

    iget-object v1, p1, Ljav;->h:Lipv;

    invoke-static {v1}, Lhfx;->a(Lipv;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgrc;->b(Lcom/google/android/gms/wallet/Address;)Lgrc;

    move-result-object v0

    iget-object v1, p1, Ljav;->g:Lipv;

    iget-object v1, v1, Lipv;->a:Lixo;

    iget-object v1, v1, Lixo;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgrc;->c(Ljava/lang/String;)Lgrc;

    move-result-object v0

    iget-object v0, v0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    sget-object v2, Lgrt;->a:Landroid/accounts/Account;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v2, -0x1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    return-void
.end method

.method public final a(Ljax;)V
    .locals 11

    const v10, 0x7f0b017d    # com.google.android.gms.R.string.wallet_error_address

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->j(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    iget-object v0, p1, Ljax;->c:[Ljbd;

    array-length v0, v0

    if-lez v0, :cond_8

    iget-object v5, p1, Ljax;->c:[Ljbd;

    array-length v6, v5

    move v3, v4

    move v0, v4

    :goto_0
    if-ge v3, v6, :cond_7

    aget-object v7, v5, v3

    iget v8, v7, Ljbd;->a:I

    iget v7, v7, Ljbd;->b:I

    const/16 v9, 0xfa

    if-eq v8, v9, :cond_0

    const/16 v9, 0x3ca

    if-ne v7, v9, :cond_2

    :cond_0
    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    packed-switch v8, :pswitch_data_0

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    goto :goto_1

    :pswitch_0
    packed-switch v7, :pswitch_data_1

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    goto :goto_1

    :pswitch_1
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v7}, Lgvc;->v()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v7}, Lgvc;->J()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0}, Lgvc;->i()Z

    move v0, v1

    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v7}, Lgvb;->b()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->i()Z

    move v0, v1

    goto :goto_2

    :pswitch_2
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v7}, Lgvb;->b()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->i()Z

    move v0, v1

    goto :goto_2

    :pswitch_3
    packed-switch v7, :pswitch_data_2

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    goto :goto_1

    :pswitch_4
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v7}, Lgvc;->v()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v7}, Lgvc;->L()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0}, Lgvc;->i()Z

    move v0, v1

    goto :goto_2

    :cond_5
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v7}, Lgvb;->K()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->i()Z

    move v0, v1

    goto :goto_2

    :pswitch_5
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v7}, Lgvb;->K()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->i()Z

    move v0, v1

    goto :goto_2

    :pswitch_6
    packed-switch v7, :pswitch_data_3

    :pswitch_7
    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :pswitch_8
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v7}, Lgvb;->M()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->i()Z

    move v0, v1

    goto/16 :goto_2

    :pswitch_9
    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const v7, 0x7f0b0113    # com.google.android.gms.R.string.wallet_error_creditcard_invalid

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    move v0, v1

    goto/16 :goto_2

    :pswitch_a
    packed-switch v7, :pswitch_data_4

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :pswitch_b
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v7}, Lgvc;->v()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_6

    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v7}, Lgvc;->K()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0}, Lgvc;->i()Z

    move v0, v1

    goto/16 :goto_2

    :cond_6
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v7}, Lgvb;->J()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->i()Z

    move v0, v1

    goto/16 :goto_2

    :pswitch_c
    iget-object v7, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v7, v7, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v7}, Lgvb;->J()V

    if-nez v0, :cond_3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->i()Z

    move v0, v1

    goto/16 :goto_2

    :pswitch_d
    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v10}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    move v0, v1

    goto/16 :goto_2

    :pswitch_e
    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v10}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    move v0, v1

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Z)V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p1, Ljax;->a:[I

    array-length v0, v0

    if-ne v0, v1, :cond_9

    iget-object v0, p1, Ljax;->a:[I

    aget v0, v0, v4

    const/4 v3, 0x6

    if-ne v0, v3, :cond_9

    iget-object v0, p1, Ljax;->d:Ljbb;

    if-eqz v0, :cond_9

    iget-object v0, p1, Ljax;->d:Ljbb;

    iget-object v0, v0, Ljbb;->c:[Ljbi;

    array-length v0, v0

    if-ne v0, v1, :cond_9

    iget-object v3, p1, Ljax;->d:Ljbb;

    iget-object v0, v3, Ljbb;->c:[Ljbi;

    aget-object v0, v0, v4

    iget v0, v0, Ljbi;->i:I

    packed-switch v0, :pswitch_data_5

    move-object v0, v2

    :goto_3
    if-eqz v0, :cond_9

    iget-object v2, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v2, v0}, Lgvb;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->i()Z

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Z)V

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v3, Ljbb;->e:[Lipv;

    array-length v0, v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v1, v3, Ljbb;->e:[Lipv;

    aget-object v1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Lipv;)Lipv;

    goto/16 :goto_1

    :pswitch_f
    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const v5, 0x7f0b00f4    # com.google.android.gms.R.string.wallet_cannot_use_prepaid

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :pswitch_10
    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const v5, 0x7f0b00f5    # com.google.android.gms.R.string.wallet_cannot_use_debit

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_9
    iget-object v0, p1, Ljax;->a:[I

    array-length v0, v0

    if-lez v0, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Ljax;->a:[I

    invoke-static {v0, v3}, Lboz;->a(Ljava/lang/StringBuilder;[I)V

    const-string v3, "SignupActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unhandled required action(s): "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :cond_a
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "EXTRA_SIGN_UP_RESULT"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lizs;)V

    iget-object v1, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v2, -0x1

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0xfb
        :pswitch_0
        :pswitch_3
        :pswitch_e
        :pswitch_6
        :pswitch_a
        :pswitch_9
        :pswitch_d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3cb
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3cb
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3cb
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x3cb
        :pswitch_c
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0xa
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    return-void
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Lhbj;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V

    return-void
.end method
