.class public final Lfxz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Post;
    .locals 17

    invoke-static/range {p0 .. p0}, Lbkp;->a(Landroid/os/Parcel;)I

    move-result v15

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v1, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v12, v1

    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v1

    if-ge v1, v15, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const v16, 0xffff

    and-int v16, v16, v1

    sparse-switch v16, :sswitch_data_0

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :sswitch_1
    sget-object v4, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p0

    invoke-static {v0, v1, v4}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v4

    goto :goto_0

    :sswitch_2
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p0

    invoke-static {v0, v1, v5}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    move-object v5, v1

    goto :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :sswitch_5
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    :sswitch_6
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->o(Landroid/os/Parcel;I)Landroid/os/Bundle;

    move-result-object v9

    goto :goto_0

    :sswitch_7
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->o(Landroid/os/Parcel;I)Landroid/os/Bundle;

    move-result-object v10

    goto :goto_0

    :sswitch_8
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    :sswitch_9
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->e(Landroid/os/Parcel;I)Ljava/lang/Boolean;

    move-result-object v1

    move-object v12, v1

    goto :goto_0

    :sswitch_a
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v13

    goto :goto_0

    :sswitch_b
    sget-object v14, Lcom/google/android/gms/common/people/data/Audience;->CREATOR:Lbkx;

    move-object/from16 v0, p0

    invoke-static {v0, v1, v14}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/people/data/Audience;

    move-object v14, v1

    goto :goto_0

    :sswitch_c
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lbkp;->g(Landroid/os/Parcel;I)I

    move-result v2

    goto :goto_0

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v1

    if-eq v1, v15, :cond_1

    new-instance v1, Lbkq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overread allowed size end="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v1, v2, v0}, Lbkq;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v1

    :cond_1
    new-instance v1, Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-direct/range {v1 .. v14}, Lcom/google/android/gms/plus/model/posts/Post;-><init>(ILjava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_5
        0x8 -> :sswitch_6
        0x9 -> :sswitch_7
        0xa -> :sswitch_8
        0xb -> :sswitch_9
        0xc -> :sswitch_a
        0xd -> :sswitch_b
        0x3e8 -> :sswitch_c
    .end sparse-switch
.end method

.method public static a(Lcom/google/android/gms/plus/model/posts/Post;Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbkr;->a(Landroid/os/Parcel;I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->b()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v3}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->k()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->m()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->p()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/Boolean;)V

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->q()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v3}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0x3e8

    invoke-virtual {p0}, Lcom/google/android/gms/plus/model/posts/Post;->r()I

    move-result v2

    invoke-static {p1, v1, v2}, Lbkr;->b(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, Lbkr;->b(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lfxz;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/plus/model/posts/Post;

    return-object v0
.end method
