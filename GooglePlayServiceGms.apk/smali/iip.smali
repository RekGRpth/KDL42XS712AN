.class public final Liip;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liio;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Likh;->a(Landroid/content/Context;)V

    iput-object p1, p0, Liip;->a:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 3

    iget-object v0, p0, Liip;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lesr;->a(Landroid/content/pm/PackageManager;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.apps.maps"

    invoke-static {v0, v2}, Lesr;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/location/reporting/service/Conditions;
    .locals 7

    new-instance v0, Lcom/google/android/location/reporting/service/Conditions;

    const/16 v1, 0x9

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v1

    iget-object v2, p0, Liip;->a:Landroid/content/Context;

    invoke-static {v2}, Lbov;->b(Landroid/content/Context;)Z

    move-result v2

    sget-object v3, Lijs;->a:Lbfy;

    invoke-virtual {v3}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {p0}, Liip;->b()Z

    move-result v4

    iget-object v5, p0, Liip;->a:Landroid/content/Context;

    invoke-static {v5}, Lbpl;->a(Landroid/content/Context;)Z

    move-result v5

    iget-object v6, p0, Liip;->a:Landroid/content/Context;

    invoke-static {v6}, Liip;->a(Landroid/content/Context;)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/reporting/service/Conditions;-><init>(ZZZZZZ)V

    return-object v0
.end method
