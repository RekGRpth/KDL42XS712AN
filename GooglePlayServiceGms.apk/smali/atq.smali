.class public final Latq;
.super Lato;
.source "SourceFile"


# instance fields
.field public e:I

.field public f:J

.field public g:J

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lato;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    sget-object v0, Laqc;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sget-object v0, Laqc;->m:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lavr;

    invoke-direct {v0}, Lavr;-><init>()V

    iget v1, p0, Latq;->e:I

    invoke-virtual {v0, v1}, Lavr;->a(I)Lavr;

    iget-wide v1, p0, Latq;->f:J

    invoke-virtual {v0, v1, v2}, Lavr;->a(J)Lavr;

    iget-wide v1, p0, Latq;->g:J

    invoke-virtual {v0, v1, v2}, Lavr;->b(J)Lavr;

    iget-boolean v1, p0, Latq;->h:Z

    invoke-virtual {v0, v1}, Lavr;->a(Z)Lavr;

    iget-boolean v1, p0, Latq;->i:Z

    invoke-virtual {v0, v1}, Lavr;->b(Z)Lavr;

    iget-boolean v1, p0, Latq;->j:Z

    invoke-virtual {v0, v1}, Lavr;->c(Z)Lavr;

    new-instance v1, Lavs;

    invoke-direct {v1}, Lavs;-><init>()V

    iget-object v2, p0, Latq;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lavs;->a(Ljava/lang/String;)Lavs;

    iget-object v2, p0, Latq;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lavs;->b(Ljava/lang/String;)Lavs;

    iget-object v2, p0, Latq;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lavs;->c(Ljava/lang/String;)Lavs;

    invoke-virtual {v0, v1}, Lavr;->a(Lavs;)Lavr;

    new-instance v1, Lavp;

    invoke-direct {v1}, Lavp;-><init>()V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lavp;->a(I)Lavp;

    invoke-virtual {v1, v0}, Lavp;->a(Lavr;)Lavp;

    iget-object v0, p0, Latq;->a:Lfko;

    const-string v2, "GrantCredentialScreen"

    invoke-virtual {v1}, Lavp;->d()[B

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    iget-object v0, p0, Latq;->a:Lfko;

    invoke-virtual {v0}, Lfko;->a()V

    goto :goto_0
.end method
