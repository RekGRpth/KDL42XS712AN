.class public final Liib;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liid;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lijt;

.field private final c:Lijd;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Likh;->a(Landroid/content/Context;)V

    iput-object p1, p0, Liib;->a:Landroid/content/Context;

    new-instance v0, Lijt;

    iget-object v1, p0, Liib;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lijt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Liib;->b:Lijt;

    invoke-static {p1}, Lijd;->a(Landroid/content/Context;)Lijd;

    move-result-object v0

    iput-object v0, p0, Liib;->c:Lijd;

    sget-object v0, Liae;->e:Liae;

    invoke-static {v0, p1}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v0

    iget v0, v0, Liad;->d:I

    iput v0, p0, Liib;->d:I

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 5

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "https://www.googleapis.com/auth/userlocation.reporting"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    return-object v0
.end method

.method private a()Lcom/google/android/ulr/ApiClientInfo;
    .locals 2

    iget-object v0, p0, Liib;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lbbv;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tablet"

    :goto_0
    new-instance v1, Lcom/google/android/ulr/ApiClientInfo;

    invoke-direct {v1, v0}, Lcom/google/android/ulr/ApiClientInfo;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    const-string v0, "phone"

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 16

    const/4 v0, 0x0

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_14

    new-instance v11, Ljava/util/ArrayList;

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Liif;

    iget-boolean v0, v10, Liif;->k:Z

    if-eqz v0, :cond_4

    iget-wide v0, v10, Liif;->l:D

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    iget-boolean v0, v10, Liif;->t:Z

    if-eqz v0, :cond_5

    iget-boolean v0, v10, Liif;->u:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    :goto_2
    iget-boolean v0, v10, Liif;->i:Z

    if-eqz v0, :cond_6

    iget v0, v10, Liif;->j:F

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_3
    iget-boolean v0, v10, Liif;->m:Z

    if-eqz v0, :cond_7

    iget v0, v10, Liif;->n:F

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_4
    iget-boolean v0, v10, Liif;->x:Z

    if-eqz v0, :cond_8

    iget v0, v10, Liif;->y:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_5
    iget-boolean v0, v10, Liif;->v:Z

    if-eqz v0, :cond_9

    iget-object v6, v10, Liif;->w:Ljava/lang/String;

    :goto_6
    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-boolean v0, v10, Liif;->a:Z

    if-eqz v0, :cond_c

    iget-object v7, v10, Liif;->b:Liig;

    iget-boolean v0, v7, Liig;->a:Z

    if-eqz v0, :cond_a

    iget v0, v7, Liig;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_7
    iget-boolean v8, v7, Liig;->c:Z

    if-eqz v8, :cond_b

    iget v7, v7, Liig;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    :goto_8
    move-object v8, v7

    move-object v7, v0

    :goto_9
    iget-boolean v0, v10, Liif;->g:Z

    if-eqz v0, :cond_d

    iget v0, v10, Liif;->h:F

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    :goto_a
    new-instance v0, Lcom/google/android/ulr/ApiLocation;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/ulr/ApiLocation;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    iget-boolean v1, v10, Liif;->r:Z

    if-eqz v1, :cond_e

    iget-boolean v1, v10, Liif;->s:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_b
    iget-boolean v2, v10, Liif;->p:Z

    if-eqz v2, :cond_f

    iget v2, v10, Liif;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_c
    const/4 v3, 0x0

    iget-boolean v4, v10, Liif;->c:Z

    if-eqz v4, :cond_0

    iget v3, v10, Liif;->d:I

    packed-switch v3, :pswitch_data_0

    const-string v3, "unknown"

    :cond_0
    :goto_d
    iget-object v4, v10, Liif;->z:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_10

    iget-object v4, v10, Liif;->z:Ljava/util/List;

    invoke-static {v4}, Liib;->b(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v4

    :goto_e
    const-string v5, "GCoreUlr"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v4, :cond_11

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    :goto_f
    const-string v6, "GCoreUlr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Found wifi scans: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v5, Lcom/google/android/ulr/ApiReadingInfo;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/android/ulr/ApiReadingInfo;-><init>(Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "GCoreUlr"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "apiReadingInfo: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-boolean v1, v10, Liif;->e:Z

    if-eqz v1, :cond_12

    iget-wide v1, v10, Liif;->f:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_10
    if-nez v1, :cond_3

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Location missing timestamp; source="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v10, Liif;->d:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lijy;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    new-instance v2, Lcom/google/android/ulr/ApiLocationReading;

    invoke-direct {v2, v0, v5, v1}, Lcom/google/android/ulr/ApiLocationReading;-><init>(Lcom/google/android/ulr/ApiLocation;Lcom/google/android/ulr/ApiReadingInfo;Ljava/lang/Long;)V

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_5

    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_6

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_8

    :cond_c
    const-string v0, "GCoreUlr"

    new-instance v9, Ljava/lang/IllegalStateException;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Location missing position; timestamp="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v14, v10, Liif;->f:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "; source="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v10, Liif;->d:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v9}, Lijy;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9

    :cond_d
    const/4 v9, 0x0

    goto/16 :goto_a

    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_b

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_c

    :pswitch_0
    const-string v3, "wifi"

    goto/16 :goto_d

    :pswitch_1
    const-string v3, "cell"

    goto/16 :goto_d

    :pswitch_2
    const-string v3, "gps"

    goto/16 :goto_d

    :pswitch_3
    const-string v3, "unknown"

    goto/16 :goto_d

    :pswitch_4
    const-string v3, "manual"

    goto/16 :goto_d

    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_11
    const/4 v5, 0x0

    goto/16 :goto_f

    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_10

    :cond_13
    move-object v0, v11

    :cond_14
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Landroid/accounts/Account;Z)V
    .locals 4

    const/4 v1, 0x1

    invoke-static {p1}, Liir;->a(Landroid/accounts/Account;)Liis;

    move-result-object v0

    iput-boolean v1, v0, Liis;->g:Z

    iput-boolean v1, v0, Liis;->d:Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Liis;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Liis;->a()Liir;

    move-result-object v0

    iget-object v1, p0, Liib;->b:Lijt;

    const-string v2, "RealReportingServer(auth)"

    const-string v3, "auth_update"

    invoke-virtual {v1, v2, v0, v3}, Lijt;->a(Ljava/lang/String;Liir;Ljava/lang/String;)Z

    return-void
.end method

.method private static a(Lsp;)V
    .locals 6

    const/4 v4, 0x6

    const-string v0, "GCoreUlr"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lsp;->a:Lrz;

    const-string v0, "GCoreUlr"

    invoke-static {v0, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v1, "GCoreUlr"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "VolleyError: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", response is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v3, :cond_5

    const-string v0, "null"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz v3, :cond_0

    const-string v0, "GCoreUlr"

    invoke-static {v0, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " networkResponse: status code is :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v3, Lrz;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/4 v2, 0x0

    :try_start_0
    iget-object v0, v3, Lrz;->b:[B

    if-eqz v0, :cond_9

    iget-object v0, v3, Lrz;->b:[B

    invoke-static {v0}, Lbpm;->a([B)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v1, Ljava/util/zip/GZIPInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v4, v3, Lrz;->b:[B

    invoke-direct {v0, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    :goto_2
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->read()I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_7

    int-to-char v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v0

    :goto_3
    :try_start_2
    const-string v2, "GCoreUlr"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " io exception "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_4
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " io exception: cannot close input stream "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v0, "non-null"

    goto/16 :goto_1

    :cond_6
    :try_start_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error in GZIP header, bad magic code"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :cond_7
    :try_start_5
    const-string v2, "GCoreUlr"

    const/4 v4, 0x6

    invoke-static {v2, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " networkResponse data is:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_8
    move-object v2, v1

    :cond_9
    :try_start_6
    iget-object v0, v3, Lrz;->c:Ljava/util/Map;

    if-eqz v0, :cond_b

    iget-object v0, v3, Lrz;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v1, "GCoreUlr"

    const/4 v4, 0x6

    invoke-static {v1, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, " networkResponse header: "

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ":"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    :catch_3
    move-exception v0

    move-object v1, v2

    goto/16 :goto_3

    :cond_b
    if-eqz v2, :cond_0

    :try_start_7
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " io exception: cannot close input stream "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v2, :cond_c

    :try_start_8
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_c
    :goto_6
    throw v0

    :catch_5
    move-exception v1

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " io exception: cannot close input stream "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_5
.end method

.method private static b(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liih;

    iget-boolean v3, v0, Liih;->a:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Liih;->c:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Liih;->e:Z

    if-eqz v3, :cond_3

    iget v3, v0, Liih;->f:I

    packed-switch v3, :pswitch_data_0

    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Unknown value for wifi auth type: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v0, Liih;->f:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lijy;->f(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Likf;->a(Liih;)V

    :cond_3
    move-object v4, v2

    :goto_2
    iget-boolean v3, v0, Liih;->g:Z

    if-eqz v3, :cond_4

    iget-boolean v3, v0, Liih;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    :goto_3
    new-instance v6, Lcom/google/android/ulr/WifiStrengthProto;

    iget-wide v7, v0, Liih;->b:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget v0, v0, Liih;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v6, v3, v7, v0, v4}, Lcom/google/android/ulr/WifiStrengthProto;-><init>(Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "GCoreUlr"

    const-string v3, "Best-effort Wifi scan conversion failed"

    invoke-static {v1, v3, v0}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_0

    :pswitch_0
    :try_start_1
    const-string v3, "unknown"

    const-string v4, "GCoreUlr"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "GCoreUlr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unknown wifi auth type: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v0, Liih;->f:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    goto :goto_2

    :pswitch_1
    const-string v3, "none"

    move-object v4, v3

    goto :goto_2

    :pswitch_2
    const-string v3, "wpaPsk"

    move-object v4, v3

    goto :goto_2

    :pswitch_3
    const-string v3, "wpaEap"

    move-object v4, v3

    goto :goto_2

    :pswitch_4
    const-string v3, "securedOther"
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v4, v3

    goto :goto_2

    :cond_4
    move-object v3, v2

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    move-object v4, v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static c(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    new-instance v4, Lcom/google/android/ulr/ApiReadingInfo;

    invoke-direct {v4, v0, v0, v0, v0}, Lcom/google/android/ulr/ApiReadingInfo;-><init>(Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v6, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/DetectedActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const-string v2, "unknown"

    :goto_2
    new-instance v8, Lcom/google/android/ulr/ApiActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v8, v1, v2}, Lcom/google/android/ulr/ApiActivity;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    const-string v2, "inVehicle"

    goto :goto_2

    :pswitch_2
    const-string v2, "onBicycle"

    goto :goto_2

    :pswitch_3
    const-string v2, "onFoot"

    goto :goto_2

    :pswitch_4
    const-string v2, "still"

    goto :goto_2

    :pswitch_5
    const-string v2, "tilting"

    goto :goto_2

    :pswitch_6
    const-string v2, "exitingVehicle"

    goto :goto_2

    :cond_1
    new-instance v1, Lcom/google/android/ulr/ApiActivityReading;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v1, v6, v4, v0}, Lcom/google/android/ulr/ApiActivityReading;-><init>(Ljava/util/ArrayList;Lcom/google/android/ulr/ApiReadingInfo;Ljava/lang/Long;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v0, v3

    :cond_3
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)Lihk;
    .locals 8

    sget-object v5, Lhri;->a:Ljava/lang/String;

    new-instance v0, Liic;

    iget-object v1, p0, Liib;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Liic;-><init>(Landroid/content/Context;)V

    :try_start_0
    new-instance v6, Limd;

    invoke-direct {v6, v0}, Limd;-><init>(Lbmi;)V

    iget-object v0, p0, Liib;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Liib;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v7

    iget-object v0, p0, Liib;->c:Lijd;

    invoke-virtual {v0}, Lijd;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Liib;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static/range {v0 .. v5}, Limd;->b(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v6, Limd;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/ulr/ApiSettings;

    move-object v1, v7

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/ApiSettings;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received null settings from server for account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, p1, v1}, Liib;->a(Landroid/accounts/Account;Z)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    throw v0

    :cond_0
    :try_start_2
    invoke-static {p1, v0}, Lihk;->a(Landroid/accounts/Account;Lcom/google/android/ulr/ApiSettings;)Lihk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Liib;->a(Landroid/accounts/Account;Z)V
    :try_end_2
    .catch Lamq; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lsp; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Liib;->a(Lsp;)V

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final a(Landroid/accounts/Account;Lihk;)Lihk;
    .locals 9

    sget-object v5, Lhri;->a:Ljava/lang/String;

    new-instance v6, Lcom/google/android/ulr/ApiSettings;

    iget-object v0, p2, Lihk;->c:Ljava/lang/Boolean;

    iget-object v1, p2, Lihk;->a:Ljava/lang/Long;

    iget-object v2, p2, Lihk;->b:Ljava/lang/Boolean;

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/ulr/ApiSettings;-><init>(Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;)V

    new-instance v0, Liic;

    iget-object v1, p0, Liib;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Liic;-><init>(Landroid/content/Context;)V

    :try_start_0
    new-instance v7, Limd;

    invoke-direct {v7, v0}, Limd;-><init>(Lbmi;)V

    iget-object v0, p0, Liib;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Liib;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v8

    iget-object v0, p0, Liib;->c:Lijd;

    invoke-virtual {v0}, Lijd;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Liib;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static/range {v0 .. v5}, Limd;->c(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v7, Limd;->a:Lbmi;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/ulr/ApiSettings;

    move-object v1, v8

    move-object v4, v6

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/ApiSettings;

    invoke-static {p1, v0}, Lihk;->a(Landroid/accounts/Account;Lcom/google/android/ulr/ApiSettings;)Lihk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Liib;->a(Landroid/accounts/Account;Z)V
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, p1, v1}, Liib;->a(Landroid/accounts/Account;Z)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Liib;->a(Lsp;)V

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final a(Landroid/accounts/Account;Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;J)Lihk;
    .locals 9

    invoke-direct {p0}, Liib;->a()Lcom/google/android/ulr/ApiClientInfo;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must have at least 1 location to upload"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/ulr/ApiBatch;

    invoke-static {p3}, Liib;->c(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p2}, Liib;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v0, v2, p4, v3}, Lcom/google/android/ulr/ApiBatch;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    new-instance v6, Lcom/google/android/ulr/ReportApiBatchRequest;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/ulr/ReportApiBatchRequest;-><init>(Lcom/google/android/ulr/ApiBatch;Lcom/google/android/ulr/ApiClientInfo;Ljava/lang/Long;)V

    new-instance v0, Liic;

    iget-object v1, p0, Liib;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Liic;-><init>(Landroid/content/Context;)V

    :try_start_0
    new-instance v7, Limc;

    invoke-direct {v7, v0}, Limc;-><init>(Lbmi;)V

    iget-object v0, p0, Liib;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Liib;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v8

    iget-object v0, p0, Liib;->c:Lijd;

    invoke-virtual {v0}, Lijd;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Liib;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lhri;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static/range {v0 .. v5}, Limc;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v7, Limc;->a:Lbmi;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/ulr/ReportApiBatchReply;

    move-object v1, v8

    move-object v4, v6

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/ReportApiBatchReply;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Liib;->a(Landroid/accounts/Account;Z)V

    invoke-virtual {v0}, Lcom/google/android/ulr/ReportApiBatchReply;->getSettings()Lcom/google/android/ulr/ApiSettings;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p1, v0}, Lihk;->a(Landroid/accounts/Account;Lcom/google/android/ulr/ApiSettings;)Lihk;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, p1, v1}, Liib;->a(Landroid/accounts/Account;Z)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Liib;->a(Lsp;)V

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final b(Landroid/accounts/Account;)Lihk;
    .locals 9

    const/4 v1, 0x0

    invoke-direct {p0}, Liib;->a()Lcom/google/android/ulr/ApiClientInfo;

    move-result-object v0

    sget-object v5, Lhri;->a:Ljava/lang/String;

    new-instance v6, Lcom/google/android/ulr/DeleteApiLocationsRequest;

    invoke-direct {v6, v0, v1, v1, v1}, Lcom/google/android/ulr/DeleteApiLocationsRequest;-><init>(Lcom/google/android/ulr/ApiClientInfo;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    new-instance v0, Liic;

    iget-object v1, p0, Liib;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Liic;-><init>(Landroid/content/Context;)V

    :try_start_0
    new-instance v7, Limd;

    invoke-direct {v7, v0}, Limd;-><init>(Lbmi;)V

    iget-object v0, p0, Liib;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Liib;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v8

    iget-object v0, p0, Liib;->c:Lijd;

    invoke-virtual {v0}, Lijd;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Liib;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static/range {v0 .. v5}, Limd;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v7, Limd;->a:Lbmi;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/ulr/DeleteApiLocationsReply;

    move-object v1, v8

    move-object v4, v6

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/DeleteApiLocationsReply;

    invoke-virtual {v0}, Lcom/google/android/ulr/DeleteApiLocationsReply;->getSettings()Lcom/google/android/ulr/ApiSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Liib;->a(Landroid/accounts/Account;Z)V

    invoke-static {p1, v0}, Lihk;->a(Landroid/accounts/Account;Lcom/google/android/ulr/ApiSettings;)Lihk;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, p1, v1}, Liib;->a(Landroid/accounts/Account;Z)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Liib;->a(Lsp;)V

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
