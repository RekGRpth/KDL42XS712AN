.class public final Lcwh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lbfy;

.field public static b:Lbfy;

.field public static c:Lbfy;

.field public static d:Lbfy;

.field public static e:Lbfy;

.field public static f:Lbfy;

.field public static g:Lbfy;

.field public static h:Lbfy;

.field public static i:Lbfy;

.field public static j:Lbfy;

.field public static k:Lbfy;

.field public static l:Lbfy;

.field public static m:Lbfy;

.field public static n:Lbfy;

.field public static o:Lbfy;

.field public static p:Lbfy;

.field public static q:Lbfy;

.field public static r:Lbfy;

.field public static s:Lbfy;

.field public static t:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0xa

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "games.base_service_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->a:Lbfy;

    const-string v0, "games.server_version"

    const-string v1, "v1"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->b:Lbfy;

    const-string v0, "games.internal_server_version"

    const-string v1, "v1whitelisted"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->c:Lbfy;

    const-string v0, "games.force_server_version"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->d:Lbfy;

    const-string v0, "games.finsky_service_url"

    const-string v1, "https://market.android.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->e:Lbfy;

    const-string v0, "games.cache_enabled"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->f:Lbfy;

    const-string v0, "games.verbose_volley_logging"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->g:Lbfy;

    const-string v0, "games.verbose_cache_logging"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->h:Lbfy;

    const-string v0, "games.revision_check_interval_ms"

    const-wide/32 v1, 0x5265c00

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->i:Lbfy;

    const-string v0, "games.leaderboard_cache_stale_threshold_millis"

    const-wide/32 v1, 0x1b7740

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->j:Lbfy;

    const-string v0, "games.max_scores_per_page"

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->k:Lbfy;

    const-string v0, "games.enable_buzzbot_subscription"

    invoke-static {v0, v4}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->l:Lbfy;

    const-string v0, "games.always_show_achievements"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->m:Lbfy;

    const-string v0, "games.max_completed_matches"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->n:Lbfy;

    const-string v0, "games.max_accepted_outbound_requests"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->o:Lbfy;

    const-string v0, "games.sync_buffer_millis"

    const-wide/16 v1, 0x7530

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->p:Lbfy;

    const-string v0, "games.tickle_sync_threshold_millis"

    const-wide/32 v1, 0xea60

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->q:Lbfy;

    const-string v0, "games.max_turn_based_match_data_bytes"

    const/high16 v1, 0x20000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->r:Lbfy;

    const-string v0, "games.max_request_payload_bytes"

    const/16 v1, 0x800

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->s:Lbfy;

    const-string v0, "games.max_request_lifetime_days"

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lcwh;->t:Lbfy;

    return-void
.end method
