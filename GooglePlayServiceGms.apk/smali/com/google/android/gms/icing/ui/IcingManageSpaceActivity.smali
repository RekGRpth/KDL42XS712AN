.class public Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/ListView;

.field private u:Landroid/widget/TextView;

.field private v:Lenw;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->s:Landroid/widget/Button;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;Lenw;)Lenw;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->v:Lenw;

    return-object p1
.end method

.method public static synthetic b(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Lenw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->v:Lenw;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->u:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->o:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->p:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->t:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0204    # com.google.android.gms.R.id.reclaim_button

    if-ne v0, v1, :cond_0

    new-instance v0, Leoa;

    invoke-direct {v0, p0, v2}, Leoa;-><init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;B)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Leoa;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04009e    # com.google.android.gms.R.layout.icing_manage_space_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->setContentView(I)V

    const v0, 0x7f0a01fc    # com.google.android.gms.R.id.main_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->o:Landroid/view/View;

    const v0, 0x7f0a0205    # com.google.android.gms.R.id.icing_progress_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->p:Landroid/view/View;

    const v0, 0x7f0a01ff    # com.google.android.gms.R.id.total_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->q:Landroid/widget/TextView;

    const v0, 0x7f0a0203    # com.google.android.gms.R.id.reclaimable_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->r:Landroid/widget/TextView;

    const v0, 0x7f0a0204    # com.google.android.gms.R.id.reclaim_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->s:Landroid/widget/Button;

    const v0, 0x7f0a0200    # com.google.android.gms.R.id.apps_list

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->t:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->t:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f0a0201    # com.google.android.gms.R.id.empty_list_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->u:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->t:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->s:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->t:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lenx;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ltz p3, :cond_0

    invoke-virtual {v0}, Lenx;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    invoke-virtual {v0, p3}, Lenx;->a(I)Leoh;

    move-result-object v0

    iget-object v0, v0, Leoh;->a:Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "package"

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->v:Lenw;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lenw;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->v:Lenw;

    invoke-super {p0}, Ljp;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Ljp;->onResume()V

    new-instance v0, Lenw;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lenw;-><init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;Z)V

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->v:Lenw;

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->v:Lenw;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lenw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
