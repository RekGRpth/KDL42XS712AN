.class public Lcom/google/android/gms/icing/service/IndexService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lens;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lens;

    invoke-virtual {v0}, Lens;->c()Lejm;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lejm;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "%s: Binding with intent %s"

    const-string v1, "main"

    invoke-static {v0, v1, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lens;

    invoke-virtual {v0}, Lens;->c()Lejm;

    move-result-object v0

    invoke-virtual {v0}, Lejm;->h()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "%s: IndexService onCreate"

    const-string v1, "main"

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const-string v0, "main"

    invoke-static {v0, p0}, Lens;->a(Ljava/lang/String;Landroid/app/Service;)Lens;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lens;

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "%s: IndexService onDestroy"

    const-string v1, "main"

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lens;

    invoke-virtual {v0}, Lens;->a()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    const-string v0, "%s: IndexService: onStartCommand with %s"

    const-string v1, "main"

    invoke-static {v0, v1, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    if-eqz p1, :cond_0

    const-string v0, "com.google.android.gms.icing.INDEX_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/service/IndexService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "%s: Unbind"

    const-string v1, "main"

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v0, 0x0

    return v0
.end method
