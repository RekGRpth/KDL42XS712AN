.class public Lcom/google/android/gms/wallet/ow/SignupActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lgug;
.implements Lguk;
.implements Lguw;
.implements Lgwv;
.implements Lgxt;
.implements Lgyq;


# static fields
.field static final n:Ljava/lang/String;

.field public static final o:Ljava/lang/Class;


# instance fields
.field A:Lgwr;

.field B:Lgxe;

.field C:Lgwr;

.field D:Landroid/widget/TextView;

.field E:Landroid/view/ViewGroup;

.field F:Landroid/widget/CheckBox;

.field public G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field public H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field public I:Landroid/widget/CheckBox;

.field J:I

.field public K:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

.field L:Ljava/lang/String;

.field public M:Z

.field N:Z

.field private O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private P:Ljbg;

.field private Q:Lcom/google/android/gms/wallet/Cart;

.field private R:Landroid/accounts/Account;

.field private S:Z

.field private T:Lgyi;

.field private U:Ljava/util/ArrayList;

.field private V:Z

.field private W:Luu;

.field private X:Lut;

.field private Y:Lguh;

.field private Z:Lgue;

.field private aa:Z

.field private ab:Z

.field private ac:Ljava/util/List;

.field private ad:I

.field private ae:Z

.field private af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

.field private ag:Z

.field private ah:Lipv;

.field private final ai:Lhcb;

.field p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

.field r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field s:Landroid/widget/ProgressBar;

.field public t:Lgvb;

.field u:Landroid/widget/CheckBox;

.field public v:Lgvc;

.field public w:Landroid/widget/CheckBox;

.field x:Landroid/view/View;

.field y:Landroid/widget/TextView;

.field z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "signup"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Ljava/lang/String;

    const-class v0, Ljax;

    sput-object v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lo;-><init>()V

    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->aa:Z

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ab:Z

    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ad:I

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ae:Z

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->N:Z

    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ag:Z

    new-instance v0, Lhbj;

    invoke-direct {v0, p0}, Lhbj;-><init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ai:Lhcb;

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljbg;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;
    .locals 15

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v7, p5

    move-object/from16 v13, p6

    move/from16 v14, p7

    invoke-static/range {v0 .. v14}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljbg;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljbg;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Landroid/content/Intent;
    .locals 15

    const/4 v12, 0x0

    const/4 v14, 0x1

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move/from16 v11, p11

    move-object/from16 v13, p12

    invoke-static/range {v0 .. v14}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljbg;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljbg;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;
    .locals 4

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "merchantMaskedWalletRequest"

    invoke-static {v1, v2, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lizs;)V

    const-string v2, "cart"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "account"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "allowedBillingCountryCodes"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v2, "defaultCountryCode"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.gms"

    const-class v3, Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "legalDocsForCountry"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v2, "requiresCreditCardFullAddress"

    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v2, "addressHints"

    invoke-static {v1, v2, p10}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v2, "showPreauthorizationPrompt"

    invoke-virtual {v1, v2, p11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "localMode"

    move/from16 v0, p12

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "immediateFullWalletRequest"

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "allowChangeAccounts"

    move/from16 v0, p14

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;Lipv;)Lipv;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ah:Lipv;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;Lut;)Lut;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lut;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;)Luu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Luu;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;Luu;)Luu;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Luu;

    return-object p1
.end method

.method private a(I)V
    .locals 5

    const/4 v1, 0x0

    invoke-static {p1}, Lhgq;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-nez v0, :cond_1

    const-string v0, "SignupActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to find legal docs for region "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lhgq;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Landroid/widget/TextView;

    const-string v1, "wallet_tos_activity"

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    new-instance v2, Lhbi;

    invoke-direct {v2, p0}, Lhbi;-><init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    invoke-static {v0, v1, v2}, Lgwg;->a(Landroid/widget/TextView;Ljava/util/Set;Lgwh;)V

    goto :goto_1
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->finish()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lut;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Luu;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lut;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "click_to_activity_result"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Luu;

    invoke-static {p0, v0}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iput-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Luu;

    iput-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lut;

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 14

    const v13, 0x7f0a0300    # com.google.android.gms.R.id.instrument_fragment_holder

    const/4 v10, 0x1

    const/16 v12, 0x8

    const/4 v11, 0x0

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ae:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const v1, 0x7f0b0134    # com.google.android.gms.R.string.wallet_sign_up_title

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    move v1, v11

    :cond_0
    :goto_0
    const v0, 0x7f0a02ff    # com.google.android.gms.R.id.chrome_checkbox_strip

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/view/ViewGroup;

    const v2, 0x7f0a0368    # com.google.android.gms.R.id.pay_with_google_checkbox

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ae:Z

    if-eqz v0, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->N:Z

    if-eqz v0, :cond_6

    move v0, v12

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    sget-object v2, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    sget-object v2, Lgrt;->a:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Landroid/widget/CheckBox;

    invoke-virtual {v0, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_2
    :goto_2
    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Landroid/widget/ProgressBar;

    const v0, 0x7f0a030a    # com.google.android.gms.R.id.tos_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Landroid/widget/TextView;

    const v0, 0x7f0a033b    # com.google.android.gms.R.id.butter_bar_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    new-instance v1, Lhbg;

    invoke-direct {v1, p0}, Lhbg;-><init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    const-string v0, "disallowedCreditCardTypes"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v7

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0, v13}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgvb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    if-nez v0, :cond_8

    const-string v0, "addressHints"

    const-class v1, Lipv;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v9

    const-string v0, "allowedBillingCountryCodes"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    const-string v0, "disallowedCardCategories"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    const/4 v2, 0x2

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ab:Z

    iget-boolean v6, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->aa:Z

    invoke-static/range {v0 .. v9}, Lgvb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;ILjava/util/ArrayList;Ljava/lang/String;ZZ[I[ILjava/util/Collection;)Lgvb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0, v13, v1}, Lag;->a(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0, p0}, Lgvb;->a(Lgxt;)V

    iget-object v0, p0, Lo;->b:Lw;

    const v1, 0x7f0a0305    # com.google.android.gms.R.id.shipping_fragment_holder

    invoke-virtual {v0, v1}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgvc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    const v0, 0x7f0a0302    # com.google.android.gms.R.id.use_as_shipping_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ac:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :goto_4
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->j()V

    :cond_3
    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljbg;

    iget-boolean v1, v0, Ljbg;->j:Z

    if-nez v1, :cond_b

    const-string v0, "showPreauthorizationPrompt"

    invoke-virtual {p1, v0, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    :goto_6
    const v0, 0x7f0a0307    # com.google.android.gms.R.id.make_default_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:Landroid/widget/CheckBox;

    const v0, 0x7f0a0308    # com.google.android.gms.R.id.make_default_info_clickable

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Landroid/view/View;

    if-eqz v10, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:Landroid/widget/CheckBox;

    invoke-virtual {v0, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Landroid/view/View;

    new-instance v2, Lhbh;

    invoke-direct {v2, p0}, Lhbh;-><init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_7
    const v0, 0x7f0a0309    # com.google.android.gms.R.id.multi_use_card_details_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Landroid/widget/TextView;

    if-eqz v1, :cond_d

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ae:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_8
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:Landroid/widget/CheckBox;

    invoke-virtual {v0, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    return-void

    :cond_5
    const v0, 0x7f0a02e7    # com.google.android.gms.R.id.payment_form_top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0, v12}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Lguw;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v11}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-eqz v0, :cond_f

    const v0, 0x7f0a02e9    # com.google.android.gms.R.id.save_to_chrome_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    invoke-virtual {v0, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v1, v10

    :goto_9
    const v0, 0x7f0a0301    # com.google.android.gms.R.id.billing_email_edit_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v11}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    const v0, 0x7f0a0306    # com.google.android.gms.R.id.shipping_email_edit_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    :goto_a
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ag:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v11}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b()V

    iput-boolean v10, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->N:Z

    goto/16 :goto_0

    :cond_6
    move v0, v11

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/view/ViewGroup;

    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0, v7}, Lgvb;->a([I)V

    goto/16 :goto_3

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_4

    :cond_a
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->f()V

    goto/16 :goto_5

    :cond_b
    move v10, v11

    goto/16 :goto_6

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:Landroid/widget/CheckBox;

    invoke-virtual {v0, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    :cond_e
    move v1, v11

    goto :goto_9

    :cond_f
    move v1, v11

    goto :goto_a
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;ILandroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {p1, p2, p3}, Lgwr;->a(Ljava/lang/String;Ljava/lang/String;I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "SignupActivity.OwErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Z)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/ow/SignupActivity;)Ljbg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljbg;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(I)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v1, 0x7f0b0153    # com.google.android.gms.R.string.wallet_save_label

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v1, 0x7f0b0152    # com.google.android.gms.R.string.wallet_continue_label

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private c(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez p1, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    if-nez p1, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lgvb;->a(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    if-nez p1, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    if-nez p1, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Lgvc;->a(Z)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:Landroid/widget/CheckBox;

    if-nez p1, :cond_9

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Landroid/view/View;

    if-nez p1, :cond_a

    move v0, v1

    :goto_6
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Landroid/widget/TextView;

    if-nez p1, :cond_b

    move v0, v1

    :goto_7
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Landroid/widget/CheckBox;

    if-nez p1, :cond_c

    move v0, v1

    :goto_8
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_d

    move v0, v1

    :goto_9
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_e

    move v0, v1

    :goto_a
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    if-nez p1, :cond_f

    move v0, v1

    :goto_b
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    if-nez p1, :cond_10

    :goto_c
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    :goto_d
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Z

    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_5

    :cond_a
    move v0, v2

    goto :goto_6

    :cond_b
    move v0, v2

    goto :goto_7

    :cond_c
    move v0, v2

    goto :goto_8

    :cond_d
    move v0, v2

    goto :goto_9

    :cond_e
    move v0, v2

    goto :goto_a

    :cond_f
    move v0, v2

    goto :goto_b

    :cond_10
    move v1, v2

    goto :goto_c

    :cond_11
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    if-nez p1, :cond_12

    :goto_e
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    goto :goto_d

    :cond_12
    move v1, v2

    goto :goto_e
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    return v0
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Z

    move-result v0

    return v0
.end method

.method private d(Z)Z
    .locals 7

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    :goto_0
    const/4 v4, 0x4

    new-array v4, v4, [Lgyo;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    aput-object v5, v4, v3

    aput-object v1, v4, v2

    const/4 v1, 0x2

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v5, v4, v1

    const/4 v1, 0x3

    aput-object v0, v4, v1

    array-length v5, v4

    move v1, v3

    move v0, v2

    :goto_1
    if-ge v1, v5, :cond_3

    aget-object v6, v4, v1

    if-eqz v6, :cond_0

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v2

    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v3

    goto :goto_2

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_3
    return v3

    :cond_3
    move v3, v0

    goto :goto_3

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->aa:Z

    return v0
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/ow/SignupActivity;)Lipv;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ah:Lipv;

    return-object v0
.end method

.method private f()V
    .locals 7

    const/4 v6, 0x0

    const v0, 0x7f0a0303    # com.google.android.gms.R.id.shipping_hint

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0304    # com.google.android.gms.R.id.shipping_hint_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Ljava/lang/String;

    new-instance v2, Lixo;

    invoke-direct {v2}, Lixo;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v3}, Lgvb;->o()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v3}, Lgvb;->a()Linv;

    move-result-object v3

    iget v3, v3, Linv;->a:I

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_1
    const-string v3, "addressHints"

    const-class v4, Lipv;

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lgty;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lgvq;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ac:Ljava/util/List;

    invoke-virtual {v4, v5}, Lgvq;->a(Ljava/util/List;)Lgvq;

    move-result-object v4

    invoke-virtual {v4, v0}, Lgvq;->a(Ljava/lang/String;)Lgvq;

    move-result-object v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Lgvq;->a(Ljava/util/ArrayList;)Lgvq;

    move-result-object v0

    iget-object v0, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->aa:Z

    invoke-static {v0, v1, v3, v6}, Lgvc;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;Ljava/util/Collection;ZZ)Lgvc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0, v2}, Lgvc;->a(Lixo;)V

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a0305    # com.google.android.gms.R.id.shipping_fragment_holder

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0, v1, v2}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v0}, Lgvb;->a()Linv;

    move-result-object v0

    iget-object v0, v0, Linv;->b:Lint;

    iget-object v3, v0, Lint;->d:Lixo;

    iget-object v0, v3, Lixo;->a:Ljava/lang/String;

    iget-object v3, v3, Lixo;->s:Ljava/lang/String;

    iput-object v3, v2, Lixo;->s:Ljava/lang/String;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic g(Lcom/google/android/gms/wallet/ow/SignupActivity;)Lcom/google/android/gms/wallet/Cart;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Q:Lcom/google/android/gms/wallet/Cart;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lgxe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lgxe;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {}, Lgxe;->J()Lgxe;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lgxe;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lgxe;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "SignupActivity.InfoDialog"

    invoke-virtual {v0, v1, v2}, Lgxe;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic i(Lcom/google/android/gms/wallet/ow/SignupActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method private j()V
    .locals 2

    const/16 v1, 0x8

    const v0, 0x7f0a0303    # com.google.android.gms.R.id.shipping_hint

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0304    # com.google.android.gms.R.id.shipping_hint_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    return-void
.end method

.method public static synthetic j(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public static synthetic k(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "SignupActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method private k()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v0}, Lgvc;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic l(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ae:Z

    return v0
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->d(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->d(Z)Z

    move-result v0

    return v0
.end method

.method public final a()Lgue;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Z:Lgue;

    if-nez v0, :cond_0

    new-instance v0, Lgue;

    invoke-direct {v0, p0}, Lgue;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Z:Lgue;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Z:Lgue;

    return-object v0
.end method

.method public final a(II)V
    .locals 1

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    invoke-virtual {v0, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, 0x2

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final b()Lguh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Y:Lguh;

    if-nez v0, :cond_0

    new-instance v0, Lguh;

    invoke-direct {v0, p0}, Lguh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Y:Lguh;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Y:Lguh;

    return-object v0
.end method

.method public final c(I)V
    .locals 8

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ae:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Ljava/lang/String;

    invoke-static {v0}, Lhgq;->a(Ljava/lang/String;)I

    move-result v0

    if-eq p1, v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    invoke-static {p1}, Lhgq;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljbg;

    sget-object v2, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Ljava/lang/String;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ab:Z

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-boolean v7, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ag:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljbg;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->setIntent(Landroid/content/Intent;)V

    sget-object v1, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    sget-object v2, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    instance-of v1, v1, Lhbt;

    if-nez v1, :cond_0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    invoke-virtual {v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v1

    invoke-virtual {v1}, Lag;->c()I

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lgyi;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    invoke-virtual {v0, p0}, Lgyi;->a(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ai:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ad:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    goto/16 :goto_1

    :cond_3
    iput p1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(I)V

    goto/16 :goto_1
.end method

.method public final e()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lgvb;

    invoke-virtual {v1}, Lgvb;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Lgvc;

    invoke-virtual {v1}, Lgvc;->i()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_2

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    sget-object v1, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-static {}, Lgry;->a()Lgry;

    invoke-static {p0}, Lgry;->b(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->j()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->f()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v0, "buyFlowConfig"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires buyFlowConfig extra!"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "buyFlowConfig"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v0, "merchantMaskedWalletRequest"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires maskedWalletRequest extra!"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "merchantMaskedWalletRequest"

    const-class v4, Ljbg;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljbg;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljbg;

    const-string v0, "cart"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/Cart;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Q:Lcom/google/android/gms/wallet/Cart;

    const-string v0, "account"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires account extra!"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "account"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    const-string v0, "allowedBillingCountryCodes"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires allowedCountryCodes"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "defaultCountryCode"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires defaultCountryCode"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "defaultCountryCode"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Ljava/lang/String;

    const-string v0, "legalDocsForCountry"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires legalDocsForCountry"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "legalDocsForCountry"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Ljava/util/ArrayList;

    const-string v0, "requiresCreditCardFullAddress"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ab:Z

    const-string v0, "localMode"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    const-string v0, "immediateFullWalletRequest"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ae:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljbg;

    iget-boolean v0, v0, Ljbg;->g:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljbg;

    iget-boolean v0, v0, Ljbg;->i:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->aa:Z

    const-string v0, "allowChangeAccounts"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ag:Z

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljbg;

    iget-object v0, v0, Ljbg;->n:[Ljbj;

    array-length v4, v0

    if-eqz v4, :cond_2

    :goto_1
    invoke-static {v1}, Lbkm;->b(Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ac:Ljava/util/List;

    :goto_2
    if-ge v2, v4, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ac:Ljava/util/List;

    aget-object v5, v0, v2

    iget-object v5, v5, Ljbj;->a:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lgyr;->a:Lgyu;

    invoke-static {p0, v0, v1}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040123    # com.google.android.gms.R.layout.wallet_activity_ow_signup

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->setContentView(I)V

    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e()Lgyi;

    move-result-object v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-nez v0, :cond_5

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    invoke-static {v0, v1, v2}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    :goto_3
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_4
    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->af:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lgyi;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Lgyi;

    goto :goto_3
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lo;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ai:Lhcb;

    invoke-interface {v0, v1}, Lhca;->b(Lhcb;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lo;->onPostCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_2

    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Z

    const-string v0, "regionCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:I

    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ad:I

    const-string v0, "existingSelectedAddress"

    const-class v1, Lipv;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ah:Lipv;

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Z)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onlinewallet_signup"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lo;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lo;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "SignupActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "SignupActivity.OwErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "SignupActivity.InfoDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgxe;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lgxe;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ai:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ad:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ai:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ad:I

    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "regionCode"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ad:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ah:Lipv;

    if-eqz v0, :cond_0

    const-string v0, "existingSelectedAddress"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->ah:Lipv;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    return-void
.end method
