.class public Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:[Ljbe;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhbd;

    invoke-direct {v0}, Lhbd;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljbe;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->a:Ljava/lang/String;

    if-eqz p2, :cond_0

    iput-object p2, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Ljbe;

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljbe;->c()[Ljbe;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Ljbe;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[Ljbe;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Ljbe;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Ljbe;

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Ljbe;

    aget-object v2, v2, v0

    invoke-static {v2}, Lizs;->a(Lizs;)[B

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
