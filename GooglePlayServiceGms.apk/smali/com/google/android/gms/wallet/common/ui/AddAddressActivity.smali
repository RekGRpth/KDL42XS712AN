.class public Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;
.super Lgxn;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lgwv;
.implements Lgyq;


# static fields
.field private static final z:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;

.field private C:Lioq;

.field private D:Z

.field private E:Lgyi;

.field private F:Z

.field private G:I

.field private final H:Lhcb;

.field n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

.field p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field q:Landroid/widget/ProgressBar;

.field public r:Landroid/widget/CheckBox;

.field public s:Lgvc;

.field t:Lgwr;

.field u:Lgwr;

.field public v:Lcom/google/android/gms/wallet/common/ui/FormEditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "addAddress"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->z:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lgxn;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->A:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->F:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->G:I

    new-instance v0, Lguy;

    invoke-direct {v0, p0}, Lguy;-><init>(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->H:Lhcb;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->F:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method private b(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->q:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    if-nez p1, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lgvc;->a(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->A:Z

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    if-nez p1, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->r:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->r:Landroid/widget/CheckBox;

    if-nez p1, :cond_8

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_9

    :goto_6
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->D:Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->q:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    if-nez p1, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_7

    :cond_8
    move v0, v2

    goto :goto_5

    :cond_9
    move v1, v2

    goto :goto_6
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Lioq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->C:Lioq;

    return-object v0
.end method

.method private c(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [Lgyo;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v0, v4, v1

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    if-eqz v6, :cond_0

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method

.method private d(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v1, 0x7f0b0153    # com.google.android.gms.R.string.wallet_save_label

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v1, 0x7f0b0152    # com.google.android.gms.R.string.wallet_continue_label

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->A:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->u:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->u:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {}, Lgwr;->K()Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->u:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->u:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->u:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "AddAddressActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->t:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->t:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->t:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->t:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->t:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "AddAddressActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c(Z)Z

    move-result v0

    return v0
.end method

.method public final a(II)V
    .locals 3

    packed-switch p2, :pswitch_data_0

    const-string v0, "AddAddressActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public finish()V
    .locals 2

    invoke-super {p0}, Lgxn;->finish()V

    const/4 v0, 0x0

    const v1, 0x7f050010    # com.google.android.gms.R.anim.wallet_push_down_out

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public final i()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    invoke-virtual {v1}, Lgvc;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->E:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->E:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->E:Lgyi;

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->A:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d(Z)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    const v8, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lgxn;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->B:Ljava/lang/String;

    const-string v0, "com.google.android.gms.wallet.accountReference"

    const-class v1, Lioq;

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioq;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->C:Lioq;

    const-string v0, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->A:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lgyr;->a:Lgyu;

    invoke-static {p0, v0, v1}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f04011d    # com.google.android.gms.R.layout.wallet_activity_add_address

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->setContentView(I)V

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->A:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const v1, 0x7f0b0160    # com.google.android.gms.R.string.wallet_add_new_address_title

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    move v1, v2

    :goto_0
    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->q:Landroid/widget/ProgressBar;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    new-instance v1, Lgux;

    invoke-direct {v1, p0}, Lgux;-><init>(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    const-string v0, "com.google.android.gms.wallet.allowedCountryCodes"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v0, "com.google.android.gms.wallet.defaultCountryCode"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->F:Z

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0, v8}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgvc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    if-nez v0, :cond_1

    const-string v0, "com.google.android.gms.wallet.addressHints"

    const-class v6, Lipv;

    invoke-static {v4, v0, v6}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-static {v6}, Lgty;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lgvq;

    move-result-object v7

    invoke-virtual {v7, v1}, Lgvq;->a(Ljava/util/List;)Lgvq;

    move-result-object v1

    invoke-virtual {v1, v5}, Lgvq;->a(Ljava/lang/String;)Lgvq;

    move-result-object v1

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lgvq;->a(Ljava/util/ArrayList;)Lgvq;

    move-result-object v0

    iget-object v0, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->F:Z

    invoke-static {v0, v6, v1, v3}, Lgvc;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;Ljava/util/Collection;ZZ)Lgvc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    const-string v0, "com.google.android.gms.wallet.baseAddress"

    const-class v1, Lipv;

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    iget-object v3, v0, Lipv;->a:Lixo;

    invoke-virtual {v1, v3}, Lgvc;->a(Lixo;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    iget-object v0, v0, Lipv;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgvc;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->s:Lgvc;

    invoke-virtual {v0, v8, v1}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j()Lgyi;

    move-result-object v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->A:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->x:Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->E:Lgyi;

    :goto_1
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->E:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_2
    return-void

    :cond_3
    const v0, 0x7f0a02e9    # com.google.android.gms.R.id.save_to_chrome_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->r:Landroid/widget/CheckBox;

    const-string v0, "com.google.android.gms.wallet.allowSaveToChromeOption"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v1, v2

    :goto_2
    const v0, 0x7f0a02e7    # com.google.android.gms.R.id.payment_form_top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    const v5, 0x7f0b0148    # com.google.android.gms.R.string.wallet_local_add_new_address_title

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    const v0, 0x7f0a02e8    # com.google.android.gms.R.id.email_edit_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->v:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    const-string v0, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->x:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lgyi;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->E:Lgyi;

    goto :goto_1
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lgxn;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->H:Lhcb;

    invoke-interface {v0, v1}, Lhca;->b(Lhcb;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lgxn;->onPostCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->D:Z

    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->G:I

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->D:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "add_address"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lgxn;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "AddAddressActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->t:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->t:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->t:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "AddAddressActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->u:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->u:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->u:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->H:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->G:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lgxn;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->H:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->G:I

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->G:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
