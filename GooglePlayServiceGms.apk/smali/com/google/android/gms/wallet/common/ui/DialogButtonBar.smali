.class public Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_3

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->setOrientation(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c002f    # com.google.android.gms.R.color.wallet_button_bar_background

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->setBackgroundColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d001e    # com.google.android.gms.R.dimen.wallet_spacing_tight

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->setPadding(IIII)V

    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040157    # com.google.android.gms.R.layout.wallet_view_dialog_button_bar

    invoke-virtual {v0, v2, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a036d    # com.google.android.gms.R.id.cancel_btn

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->b:Landroid/widget/Button;

    const v0, 0x7f0a036c    # com.google.android.gms.R.id.continue_btn

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a:Landroid/widget/Button;

    sget-object v0, Lxg;->F:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->b:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    :cond_1
    const/4 v0, 0x2

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    const/4 v4, 0x4

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->b:Landroid/widget/Button;

    invoke-virtual {v5, v3}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    if-eqz v4, :cond_4

    move v0, v1

    :goto_1
    const v1, 0x7f0a036e    # com.google.android.gms.R.id.btn_divider

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->b:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v0, Lgwo;

    invoke-direct {v0, p0}, Lgwo;-><init>(Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->b:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_3
    invoke-virtual {p0, v5}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->setOrientation(I)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x8

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
