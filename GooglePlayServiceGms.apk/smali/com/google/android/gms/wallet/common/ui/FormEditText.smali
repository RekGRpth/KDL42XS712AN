.class public Lcom/google/android/gms/wallet/common/ui/FormEditText;
.super Landroid/widget/AutoCompleteTextView;
.source "SourceFile"

# interfaces
.implements Lgyp;


# instance fields
.field a:Ljava/util/LinkedList;

.field final b:Landroid/text/TextWatcher;

.field private final c:Lgza;

.field private final d:Lgza;

.field private e:Lgyw;

.field private f:Lgyo;

.field private g:Lgwi;

.field private h:Lgyo;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Lgzn;

.field private m:Ljava/lang/CharSequence;

.field private n:Z

.field private final o:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Ljava/lang/String;

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:I

    new-instance v0, Lgwz;

    invoke-direct {v0, p0}, Lgwz;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b:Landroid/text/TextWatcher;

    new-instance v0, Lgxa;

    invoke-direct {v0, p0}, Lgxa;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->o:Landroid/text/TextWatcher;

    new-instance v0, Lgyx;

    invoke-direct {v0}, Lgyx;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    new-instance v0, Lgyx;

    invoke-direct {v0}, Lgyx;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lgza;

    iput-object p0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lgyo;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Ljava/lang/String;

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:I

    new-instance v0, Lgwz;

    invoke-direct {v0, p0}, Lgwz;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b:Landroid/text/TextWatcher;

    new-instance v0, Lgxa;

    invoke-direct {v0, p0}, Lgxa;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->o:Landroid/text/TextWatcher;

    new-instance v0, Lgyx;

    invoke-direct {v0}, Lgyx;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    new-instance v0, Lgyx;

    invoke-direct {v0}, Lgyx;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lgza;

    iput-object p0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lgyo;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Ljava/lang/String;

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:I

    new-instance v0, Lgwz;

    invoke-direct {v0, p0}, Lgwz;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b:Landroid/text/TextWatcher;

    new-instance v0, Lgxa;

    invoke-direct {v0, p0}, Lgxa;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->o:Landroid/text/TextWatcher;

    new-instance v0, Lgyx;

    invoke-direct {v0}, Lgyx;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    new-instance v0, Lgyx;

    invoke-direct {v0}, Lgyx;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lgza;

    iput-object p0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lgyo;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Lgza;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lgza;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->m:Ljava/lang/CharSequence;

    return-object p1
.end method

.method private a()Ljava/util/LinkedList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Ljava/util/LinkedList;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Ljava/util/LinkedList;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    const v7, 0x7fffffff

    const/4 v6, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-array v0, v4, [I

    const v1, 0x1010160    # android.R.attr.maxLength

    aput v1, v0, v5

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    sget-object v0, Lxg;->G:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    const/4 v0, 0x4

    invoke-virtual {v2, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    if-eqz v1, :cond_1

    invoke-virtual {v2, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0b0165    # com.google.android.gms.R.string.wallet_error_field_must_not_be_empty

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Ljava/lang/String;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b()V

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    :cond_2
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getImeOptions()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setImeOptions(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->o:Landroid/text/TextWatcher;

    invoke-super {p0, v0}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Landroid/text/TextWatcher;)V

    invoke-virtual {p0, v7}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setThreshold(I)V

    return-void

    :pswitch_0
    new-instance v1, Lgzg;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const v0, 0x7f0b0164    # com.google.android.gms.R.string.wallet_error_only_numeric_digits_allowed

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_3
    invoke-direct {v1, v0}, Lgzg;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    move-object v0, v1

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x2

    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v1, Lgzm;

    invoke-direct {v1, v0, v3}, Lgzm;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :pswitch_2
    new-instance v1, Lgzb;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    const v0, 0x7f0b0167    # com.google.android.gms.R.string.wallet_error_creditcard_number_invalid

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    invoke-direct {v1, v0}, Lgzb;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    new-array v0, v4, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/16 v4, 0x13

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v0, v5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_3
    new-instance v1, Lgze;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    const v0, 0x7f0b0166    # com.google.android.gms.R.string.wallet_error_email_address_invalid

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_5
    invoke-direct {v1, v0}, Lgze;-><init>(Ljava/lang/CharSequence;)V

    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_4
    new-instance v1, Lgzj;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const v0, 0x7f0b0168    # com.google.android.gms.R.string.wallet_error_phone_invalid

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-direct {v1, v0}, Lgzj;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    move-object v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/util/LinkedList;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a()Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:Lgzn;

    if-nez v0, :cond_0

    new-instance v0, Lgzn;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Ljava/lang/String;

    invoke-direct {v0, v1}, Lgzn;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:Lgzn;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:Lgzn;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:Lgzn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:Lgzn;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c(Lgzo;)V

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->m:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method public final R_()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    invoke-virtual {v1}, Lgza;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    invoke-virtual {v1}, Lgza;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final S_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    invoke-virtual {v0, p0}, Lgza;->a(Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lgwi;Lgyo;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e:Lgyw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->g:Lgwi;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->h:Lgyo;

    if-ne v0, p2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lgyw;

    invoke-direct {v0, p0, p1, p2}, Lgyw;-><init>(Landroid/widget/EditText;Lgwi;Lgyo;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e:Lgyw;

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e:Lgyw;

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->g:Lgwi;

    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->h:Lgyo;

    goto :goto_0
.end method

.method public final a(Lgyo;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lgyo;

    return-void
.end method

.method public final a(Lgzo;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    invoke-virtual {v0, p1}, Lgza;->a(Lgzo;)V

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->n:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->replaceText(Ljava/lang/CharSequence;)V

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->n:Z

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b()V

    return-void
.end method

.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lgzo;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lgza;

    invoke-virtual {v0, p1}, Lgza;->a(Lgzo;)V

    return-void
.end method

.method public beginBatchEdit()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->beginBatchEdit()V

    return-void
.end method

.method public final c(Lgzo;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    invoke-virtual {v0, p1}, Lgza;->b(Lgzo;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lgza;

    invoke-virtual {v0, p1}, Lgza;->b(Lgzo;)V

    return-void
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:I

    return v0
.end method

.method public enoughToFilter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->n:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->enoughToFilter()Z

    move-result v0

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lgxb;

    invoke-direct {v0, p0, v1}, Lgxb;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Landroid/view/inputmethod/InputConnection;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lgyo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lgyo;

    invoke-interface {v0}, Lgyo;->R_()Z

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "superInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/AutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    const-string v1, "focusChangeValidators"

    invoke-virtual {v0, p1, v1}, Lgza;->b(Landroid/os/Bundle;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lgza;

    const-string v1, "textChangeValidators"

    invoke-virtual {v0, p1, v1}, Lgza;->b(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "superInstanceState"

    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Lgza;

    const-string v2, "focusChangeValidators"

    invoke-virtual {v1, v0, v2}, Lgza;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lgza;

    const-string v2, "textChangeValidators"

    invoke-virtual {v1, v0, v2}, Lgza;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    return-object v0
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
