.class public Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lgxg;


# static fields
.field private static b:Landroid/graphics/ColorMatrixColorFilter;


# instance fields
.field final a:Lgxi;

.field private final c:Lgxi;

.field private d:Lgxh;

.field private e:Lioj;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:[I

.field private k:[I

.field private l:I

.field private m:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    new-instance v0, Lgxi;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0169    # com.google.android.gms.R.string.wallet_add_new_card

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const v3, 0x7f02024b    # com.google.android.gms.R.drawable.wallet_card_full_add

    invoke-direct {v0, v1, v2, v3}, Lgxi;-><init>(Ljava/lang/String;ZI)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->c:Lgxi;

    new-instance v0, Lgxi;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b016a    # com.google.android.gms.R.string.wallet_select_payment_method

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4, v4}, Lgxi;-><init>(Ljava/lang/String;ZI)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lgxi;

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->h:Z

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->i:Z

    iput v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->l:I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lgxi;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0169    # com.google.android.gms.R.string.wallet_add_new_card

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const v3, 0x7f02024b    # com.google.android.gms.R.drawable.wallet_card_full_add

    invoke-direct {v0, v1, v2, v3}, Lgxi;-><init>(Ljava/lang/String;ZI)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->c:Lgxi;

    new-instance v0, Lgxi;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b016a    # com.google.android.gms.R.string.wallet_select_payment_method

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4, v4}, Lgxi;-><init>(Ljava/lang/String;ZI)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lgxi;

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->h:Z

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->i:Z

    iput v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->l:I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lgxi;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0169    # com.google.android.gms.R.string.wallet_add_new_card

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const v3, 0x7f02024b    # com.google.android.gms.R.drawable.wallet_card_full_add

    invoke-direct {v0, v1, v2, v3}, Lgxi;-><init>(Ljava/lang/String;ZI)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->c:Lgxi;

    new-instance v0, Lgxi;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b016a    # com.google.android.gms.R.string.wallet_select_payment_method

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4, v4}, Lgxi;-><init>(Ljava/lang/String;ZI)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lgxi;

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->h:Z

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->i:Z

    iput v4, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->l:I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static synthetic a(Lgxk;)Ljava/lang/Object;
    .locals 1

    if-eqz p0, :cond_0

    iget-object v0, p0, Lgxk;->a:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lxg;->H:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->f:Z

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->g:Z

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->l:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)[I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->j:[I

    return-object v0
.end method

.method public static synthetic b()Landroid/graphics/ColorMatrixColorFilter;
    .locals 3

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b:Landroid/graphics/ColorMatrixColorFilter;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v1, Landroid/graphics/ColorMatrix;

    const/16 v2, 0x14

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    sput-object v1, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b:Landroid/graphics/ColorMatrixColorFilter;

    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b:Landroid/graphics/ColorMatrixColorFilter;

    return-object v0

    :array_0
    .array-data 4
        0x3f333333    # 0.7f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f333333    # 0.7f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f333333    # 0.7f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f333333    # 0.7f
        0x0
    .end array-data
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)[I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->k:[I

    return-object v0
.end method

.method private c()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    invoke-virtual {v0, v1}, Lgxj;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lgxi;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->i:Z

    return v0
.end method

.method private d()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    new-instance v1, Lgxk;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lgxi;

    invoke-direct {v1, v2}, Lgxk;-><init>(Lgxi;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lgxj;->insert(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->f:Z

    return v0
.end method

.method private e()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->c()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgxj;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgxk;

    invoke-virtual {v0, v1}, Lgxj;->remove(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->g:Z

    return v0
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->h:Z

    return v0
.end method

.method public static synthetic g(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->l:I

    return v0
.end method


# virtual methods
.method public final a(Lgxh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    return-void
.end method

.method public final a(Lioj;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    const-string v1, "Set payment instruments before setting the selected payment instrument"

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    invoke-virtual {v0, p1}, Lgxj;->a(Lioj;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->setSelection(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->setSelection(I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->g:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    invoke-virtual {v0}, Lgxj;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final a([I)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->j:[I

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    invoke-virtual {v0}, Lgxj;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final a([Lioj;)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lgxk;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lgxi;

    invoke-direct {v0, v2}, Lgxk;-><init>(Lgxi;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_0

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    new-instance v4, Lgxk;

    invoke-direct {v4, v3}, Lgxk;-><init>(Lioj;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lgxk;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->c:Lgxi;

    invoke-direct {v0, v2}, Lgxk;-><init>(Lgxi;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    invoke-virtual {v0}, Lgxj;->notifyDataSetChanged()V

    :goto_1
    return-void

    :cond_1
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->m:Ljava/util/ArrayList;

    new-instance v0, Lgxj;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2, v1}, Lgxj;-><init>(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {p0, p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->h:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    invoke-virtual {v0}, Lgxj;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final b([I)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->k:[I

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    invoke-virtual {v0}, Lgxj;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final b(Lioj;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->k:[I

    iget v3, p1, Lioj;->l:I

    invoke-static {v2, v3}, Lboz;->a([II)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->j:[I

    iget v3, p1, Lioj;->c:I

    invoke-static {v2, v3}, Lboz;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Lgth;->d(Lioj;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->f:Z

    if-eqz v2, :cond_0

    invoke-static {p1}, Lgth;->e(Lioj;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->i:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    invoke-virtual {v0}, Lgxj;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lioj;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    check-cast v0, Lioj;

    invoke-interface {v1, v0}, Lgxh;->b(Lioj;)V

    :cond_0
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v0, v1, Lioj;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Lioj;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e:Lioj;

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->j:[I

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->k:[I

    invoke-static {v0, v2, v3}, Lgth;->a(Lioj;[I[I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    invoke-interface {v1, v0}, Lgxh;->b(Lioj;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e:Lioj;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lioj;)V

    goto :goto_0

    :cond_2
    check-cast v1, Lioj;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Lioj;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e:Lioj;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lioj;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    invoke-interface {v1, v0}, Lgxh;->a(Lioj;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e:Lioj;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->c:Lgxi;

    if-ne v1, v0, :cond_5

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e:Lioj;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    invoke-interface {v0}, Lgxh;->a()V

    goto :goto_0

    :cond_5
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e:Lioj;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    invoke-interface {v0, v2}, Lgxh;->a(Lioj;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e:Lioj;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d:Lgxh;

    invoke-interface {v0, v1}, Lgxh;->a(Lioj;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->isEnabled()Z

    move-result v0

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxj;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgxj;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d()V

    const/4 p1, 0x0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public setSelection(IZ)V
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d()V

    const/4 p1, 0x0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/Spinner;->setSelection(IZ)V

    return-void
.end method
