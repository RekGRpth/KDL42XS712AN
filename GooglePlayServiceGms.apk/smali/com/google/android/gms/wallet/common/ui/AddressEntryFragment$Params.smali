.class public final Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Z

.field public b:[I

.field public c:I

.field public d:Z

.field public e:Z

.field public f:[C

.field public g:I

.field public h:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgvp;

    invoke-direct {v0}, Lgvp;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->d:Z

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->e:Z

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    return-void
.end method

.method public static a()Lgvq;
    .locals 3

    new-instance v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;-><init>()V

    new-instance v1, Lgvq;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lgvq;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;B)V

    return-object v1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->e:Z

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Landroid/os/Parcel;)V

    goto :goto_3
.end method
