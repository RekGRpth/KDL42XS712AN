.class public Lcom/google/android/gms/wallet/common/ui/CrashActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms"

    const-class v2, Lcom/google/android/gms/wallet/common/ui/CrashActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lgyr;->b:Lgyu;

    invoke-static {p0, v0, v1}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040120    # com.google.android.gms.R.layout.wallet_activity_crash

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->setContentView(I)V

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    const v0, 0x7f0a02ed    # com.google.android.gms.R.id.exit_btn

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lo;->onPostCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    if-nez v0, :cond_1

    const-string v0, "unknown"

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v1

    const-string v2, "fatal_error"

    invoke-static {v1, v0, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
