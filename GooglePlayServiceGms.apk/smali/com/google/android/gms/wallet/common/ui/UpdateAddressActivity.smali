.class public Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;
.super Lgxn;
.source "SourceFile"

# interfaces
.implements Lgwv;
.implements Lgyq;


# static fields
.field private static final v:Ljava/lang/String;


# instance fields
.field private A:Lipv;

.field private B:Ljava/lang/String;

.field private C:Lioq;

.field private D:Z

.field private E:Lgyi;

.field private F:Z

.field private G:I

.field private final H:Lhcb;

.field n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

.field p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field q:Landroid/widget/ProgressBar;

.field public r:Lgvc;

.field s:Lgwr;

.field t:Lgwr;

.field public u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "updateAddress"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lgxn;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->z:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->F:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->G:I

    new-instance v0, Lgyk;

    invoke-direct {v0, p0}, Lgyk;-><init>(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->H:Lhcb;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lipv;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->q:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    if-nez p1, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lgvc;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->z:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    if-nez p1, :cond_5

    :goto_4
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    :goto_5
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->D:Z

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->q:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    if-nez p1, :cond_7

    :goto_6
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    goto :goto_5

    :cond_7
    move v1, v2

    goto :goto_6
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->F:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method private c(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [Lgyo;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v0, v4, v1

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    if-eqz v6, :cond_0

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lioq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->C:Lioq;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->z:Z

    return v0
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->t:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->t:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {}, Lgwr;->K()Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->t:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->t:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->t:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "UpdateAddressActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->s:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->s:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->s:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->s:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->s:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "UpdateAddressActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c(Z)Z

    move-result v0

    return v0
.end method

.method public final a(II)V
    .locals 3

    packed-switch p2, :pswitch_data_0

    const-string v0, "UpdateAddressActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public finish()V
    .locals 2

    invoke-super {p0}, Lgxn;->finish()V

    const/4 v0, 0x0

    const v1, 0x7f050010    # com.google.android.gms.R.anim.wallet_push_down_out

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public final i()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    invoke-virtual {v1}, Lgvc;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->E:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->E:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->E:Lgyi;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    const v8, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lgxn;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v0, "com.google.android.gms.wallet.address"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires address extra!"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "com.google.android.gms.wallet.address"

    const-class v4, Lipv;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->B:Ljava/lang/String;

    const-string v0, "com.google.android.gms.wallet.accountReference"

    const-class v4, Lioq;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioq;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->C:Lioq;

    const-string v0, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v4, Lgyr;->a:Lgyu;

    invoke-static {p0, v0, v4}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f040128    # com.google.android.gms.R.layout.wallet_activity_update_address

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->setContentView(I)V

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->z:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const v4, 0x7f0b0162    # com.google.android.gms.R.string.wallet_update_address_title

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    :cond_0
    :goto_0
    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->q:Landroid/widget/ProgressBar;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    new-instance v4, Lgyj;

    invoke-direct {v4, p0}, Lgyj;-><init>(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->F:Z

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0, v8}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgvc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    if-nez v0, :cond_1

    const-string v0, "com.google.android.gms.wallet.allowedCountryCodes"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    iget-object v4, v4, Lipv;->a:Lixo;

    iget-object v4, v4, Lixo;->a:Ljava/lang/String;

    const-string v5, "com.google.android.gms.wallet.addressHints"

    const-class v6, Lipv;

    invoke-static {v3, v5, v6}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v5}, Lgty;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v6

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lgvq;

    move-result-object v7

    invoke-virtual {v7, v0}, Lgvq;->a(Ljava/util/List;)Lgvq;

    move-result-object v0

    invoke-virtual {v0, v4}, Lgvq;->a(Ljava/lang/String;)Lgvq;

    move-result-object v4

    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Lgvq;->a(Ljava/util/ArrayList;)Lgvq;

    move-result-object v0

    iget-object v4, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->F:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    invoke-static {v0}, Lgth;->c(Lipv;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    invoke-static {v0}, Lgth;->a(Lipv;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->F:Z

    invoke-static {v4, v5, v2, v0}, Lgvc;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;Ljava/util/Collection;ZZ)Lgvc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    iget-object v2, v2, Lipv;->a:Lixo;

    invoke-virtual {v0, v2}, Lgvc;->a(Lixo;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    iget-object v2, v2, Lipv;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lgvc;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->r:Lgvc;

    invoke-virtual {v0, v8, v2}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j()Lgyi;

    move-result-object v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->z:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->x:Landroid/accounts/Account;

    invoke-static {v1, v0, v2}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->E:Lgyi;

    :goto_2
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->E:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_2
    return-void

    :cond_3
    const v0, 0x7f0a02e7    # com.google.android.gms.R.id.payment_form_top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    const v4, 0x7f0b0149    # com.google.android.gms.R.string.wallet_local_update_address_title

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    const v0, 0x7f0a02e8    # com.google.android.gms.R.id.email_edit_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    iget-object v0, v0, Lipv;->a:Lixo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->A:Lipv;

    iget-object v0, v0, Lipv;->a:Lixo;

    iget-object v4, v0, Lixo;->t:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->u:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, v0, Lixo;->t:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    const-string v0, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->x:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lgyi;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->E:Lgyi;

    goto :goto_2
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lgxn;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->H:Lhcb;

    invoke-interface {v0, v1}, Lhca;->b(Lhcb;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lgxn;->onPostCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->D:Z

    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->G:I

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->D:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "update_address"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lgxn;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "UpdateAddressActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->s:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->s:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->s:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "UpdateAddressActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->t:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->t:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->t:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->H:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->G:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lgxn;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->H:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->G:I

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->G:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
