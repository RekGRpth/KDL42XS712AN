.class public Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;
.super Lgxn;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lgwv;


# static fields
.field private static final z:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;

.field private C:Lioq;

.field private D:Z

.field private E:Lgyi;

.field private F:I

.field private G:I

.field private final H:Lhcb;

.field n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

.field p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field public q:Landroid/widget/TextView;

.field r:Landroid/widget/ProgressBar;

.field public s:Landroid/widget/CheckBox;

.field public t:Lgxf;

.field u:Lgwr;

.field v:Lgwr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "addInstrument"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->z:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lgxn;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->A:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->G:I

    new-instance v0, Lgva;

    invoke-direct {v0, p0}, Lgva;-><init>(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->H:Lhcb;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(Z)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Lioq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->C:Lioq;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->F:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    if-nez p1, :cond_3

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, Lgxf;->a(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->A:Z

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    if-nez p1, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Landroid/widget/CheckBox;

    if-nez p1, :cond_7

    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->D:Z

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->r:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    if-nez p1, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_5
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h()V

    return-void
.end method

.method private c(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v1, 0x7f0b0153    # com.google.android.gms.R.string.wallet_save_label

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v1, 0x7f0b0152    # com.google.android.gms.R.string.wallet_continue_label

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->v:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->v:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {}, Lgwr;->J()Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->v:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->v:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->v:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "AddInstrumentActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->u:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->u:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->u:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->u:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->u:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "AddInstrumentActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method private h()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->F:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    packed-switch p2, :pswitch_data_0

    const-string v0, "AddInstrumentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public finish()V
    .locals 2

    invoke-super {p0}, Lgxn;->finish()V

    const/4 v0, 0x0

    const v1, 0x7f050010    # com.google.android.gms.R.anim.wallet_push_down_out

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public final g()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->E:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->E:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->E:Lgyi;

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->A:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c(Z)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    const v11, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    const/4 v2, 0x1

    const/4 v6, 0x0

    :try_start_0
    invoke-super {p0, p1}, Lgxn;->onCreate(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->B:Ljava/lang/String;

    const-string v0, "com.google.android.gms.wallet.accountReference"

    const-class v1, Lioq;

    invoke-static {v10, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioq;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->C:Lioq;

    const-string v0, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v10, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->A:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lgyr;->a:Lgyu;

    invoke-static {p0, v0, v1}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f04011e    # com.google.android.gms.R.layout.wallet_activity_add_instrument

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->setContentView(I)V

    if-eqz p1, :cond_0

    const-string v0, "errorMessageResourceId"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->F:I

    :cond_0
    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->A:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const v1, 0x7f0b014f    # com.google.android.gms.R.string.wallet_add_new_card_title

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    move v1, v2

    :goto_0
    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->r:Landroid/widget/ProgressBar;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    new-instance v1, Lguz;

    invoke-direct {v1, p0}, Lguz;-><init>(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a033b    # com.google.android.gms.R.id.butter_bar_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:Landroid/widget/TextView;

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->F:I

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->F:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(I)V

    :goto_1
    const-string v0, "com.google.android.gms.wallet.disallowedCreditCardTypes"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v7

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0, v11}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgxf;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->A:Z

    if-nez v0, :cond_7

    const-string v0, "com.google.android.gms.wallet.defaultCountryCode"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "com.google.android.gms.wallet.allowedCountryCodes"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    const-string v0, "com.google.android.gms.wallet.requiresCreditCardFullAddress"

    invoke-virtual {v10, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v0, "com.google.android.gms.wallet.disallowedCardCategories"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v8

    const-string v0, "com.google.android.gms.wallet.addressHints"

    const-class v1, Lipv;

    invoke-static {v10, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v9

    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v10, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->x:Landroid/accounts/Account;

    invoke-static/range {v0 .. v9}, Lgvb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;ILjava/util/ArrayList;Ljava/lang/String;ZZ[I[ILjava/util/Collection;)Lgvb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    :goto_2
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v11, v0}, Lag;->a(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g()Lgyi;

    move-result-object v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->A:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->x:Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->E:Lgyi;

    :goto_3
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->E:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_2
    :goto_4
    return-void

    :catch_0
    move-exception v0

    const-string v1, "AddInstrumentActivity"

    const-string v3, "Exception creating fragment"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(ILandroid/content/Intent;)V

    goto :goto_4

    :cond_3
    const v0, 0x7f0a02e9    # com.google.android.gms.R.id.save_to_chrome_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Landroid/widget/CheckBox;

    const-string v0, "com.google.android.gms.wallet.allowSaveToChromeOption"

    invoke-virtual {v10, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v1, v2

    :goto_5
    const v0, 0x7f0a02e7    # com.google.android.gms.R.id.payment_form_top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    const v3, 0x7f0b0146    # com.google.android.gms.R.string.wallet_local_add_new_card_title

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    move v1, v6

    goto :goto_5

    :cond_5
    const-string v0, "com.google.android.gms.wallet.errorMessageResourceId"

    invoke-virtual {v10, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(I)V

    goto/16 :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h()V

    goto/16 :goto_1

    :cond_7
    invoke-static {v7}, Lgxl;->a([I)Lgxl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->t:Lgxf;

    goto/16 :goto_2

    :cond_8
    const-string v0, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->x:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lgyi;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->E:Lgyi;

    goto/16 :goto_3
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lgxn;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->H:Lhcb;

    invoke-interface {v0, v1}, Lhca;->b(Lhcb;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lgxn;->onPostCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->D:Z

    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->G:I

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->D:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "add_instrument"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lgxn;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "AddInstrumentActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->u:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->u:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->u:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "AddInstrumentActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->v:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->v:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->v:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->H:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->G:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lgxn;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->H:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->G:I

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->G:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "errorMessageResourceId"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->F:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
