.class public final Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:I

.field public b:Landroid/accounts/Account;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:[Lcom/google/android/gms/wallet/CountrySpecification;

.field public k:Ljava/util/ArrayList;

.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgrg;

    invoke-direct {v0}, Lgrg;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    return-void
.end method

.method public constructor <init>(IILandroid/accounts/Account;Ljava/lang/String;IZZZLjava/lang/String;Z[Lcom/google/android/gms/wallet/CountrySpecification;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    iput p2, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    iput-object p3, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d:I

    iput-boolean p6, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    iput-boolean p7, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    iput-boolean p8, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    iput-object p9, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    iput-boolean p10, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    iput-object p11, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j:[Lcom/google/android/gms/wallet/CountrySpecification;

    iput-object p12, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lgrf;
    .locals 4

    new-instance v0, Lgrf;

    new-instance v1, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lgrf;-><init>(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;B)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    iget v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    :cond_0
    iget-object v2, v0, Lgrf;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    return v0
.end method

.method public final c()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    return v0
.end method

.method public final j()[Lcom/google/android/gms/wallet/CountrySpecification;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j:[Lcom/google/android/gms/wallet/CountrySpecification;

    return-object v0
.end method

.method public final k()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lgrg;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/os/Parcel;I)V

    return-void
.end method
