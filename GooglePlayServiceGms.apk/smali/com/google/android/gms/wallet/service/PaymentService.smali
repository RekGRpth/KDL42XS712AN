.class public Lcom/google/android/gms/wallet/service/PaymentService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lhak;

.field private b:Lhfb;

.field private c:Lhct;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private a()Lorg/apache/http/client/HttpClient;
    .locals 4

    new-instance v0, Limj;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    const-string v2, "WalletSdk/4325030"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Limj;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/service/PaymentService;)Lorg/apache/http/client/HttpClient;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/PaymentService;->a()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/PaymentService;->c:Lhct;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.service.BIND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lhcf;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/PaymentService;->a:Lhak;

    invoke-direct {v0, v1}, Lhcf;-><init>(Lhaj;)V

    invoke-virtual {v0}, Lhcf;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.service.ow.IOwInternalService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/PaymentService;->b:Lhfb;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 23

    invoke-super/range {p0 .. p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/service/PaymentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    new-instance v1, Lgut;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/wallet/service/PaymentService;->a()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    invoke-direct {v1, v2}, Lgut;-><init>(Lorg/apache/http/client/HttpClient;)V

    new-instance v21, Lgtm;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Lgtm;-><init>(Landroid/content/Context;)V

    new-instance v2, Lhcv;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v2, v3, v1, v0}, Lhcv;-><init>(Landroid/content/Context;Lgut;Lgtm;)V

    new-instance v4, Lhcg;

    invoke-direct {v4}, Lhcg;-><init>()V

    new-instance v9, Lhga;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lhga;-><init>(Landroid/content/Context;)V

    new-instance v5, Lgsi;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v5, v3}, Lgsi;-><init>(Landroid/content/Context;)V

    new-instance v7, Lgse;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v7, v3}, Lgse;-><init>(Landroid/content/Context;)V

    new-instance v3, Lhdj;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v3, v6, v2}, Lhdj;-><init>(Landroid/content/Context;Lhcv;)V

    new-instance v2, Lhcl;

    invoke-direct {v2, v3, v4, v5}, Lhcl;-><init>(Lhct;Lhcg;Lgsi;)V

    new-instance v3, Lhdy;

    invoke-direct {v3, v2}, Lhdy;-><init>(Lhct;)V

    new-instance v2, Lhcq;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v2, v6, v3}, Lhcq;-><init>(Landroid/content/Context;Lhct;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->c:Lhct;

    new-instance v12, Lhfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v12, v2, v1}, Lhfo;-><init>(Landroid/content/Context;Lgut;)V

    new-instance v6, Lgsg;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v6, v1}, Lgsg;-><init>(Landroid/content/Context;)V

    new-instance v8, Lhef;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v8, v1}, Lhef;-><init>(Landroid/content/Context;)V

    new-instance v3, Lhfd;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v3, v1, v12, v8, v0}, Lhfd;-><init>(Landroid/content/Context;Lhfo;Lhef;Lgtm;)V

    new-instance v1, Lhei;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-static {}, Lhep;->a()Lhep;

    move-result-object v10

    invoke-direct/range {v1 .. v10}, Lhei;-><init>(Landroid/content/Context;Lhfb;Lhcg;Lgsi;Lgsg;Lgse;Lhef;Lhga;Lhep;)V

    new-instance v2, Lhfz;

    invoke-direct {v2, v1}, Lhfz;-><init>(Lhfb;)V

    new-instance v3, Lhel;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v3, v10, v2}, Lhel;-><init>(Landroid/content/Context;Lhfb;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->b:Lhfb;

    new-instance v17, Lher;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-static {}, Lhep;->a()Lhep;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v1, v3}, Lher;-><init>(Landroid/content/Context;Lhfb;Lhep;)V

    new-instance v10, Lhfk;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-static {}, Lhep;->a()Lhep;

    move-result-object v19

    move-object v13, v9

    move-object v14, v6

    move-object v15, v8

    move-object/from16 v16, v5

    move-object/from16 v18, v1

    move-object/from16 v20, v7

    move-object/from16 v22, v4

    invoke-direct/range {v10 .. v22}, Lhfk;-><init>(Landroid/content/Context;Lhfo;Lhga;Lgsg;Lhef;Lgsi;Lher;Lhfb;Lhep;Lgse;Lgtm;Lhcg;)V

    new-instance v1, Lhem;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v1, v2, v10}, Lhem;-><init>(Landroid/content/Context;Lhak;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/wallet/service/PaymentService;->a:Lhak;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-static {v1}, Lbox;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    new-instance v2, Lhbz;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lhbz;-><init>(Lcom/google/android/gms/wallet/service/PaymentService;)V

    const-wide/16 v3, 0x7530

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_0
    return-void
.end method
