.class public Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field public static a:Landroid/accounts/Account;


# instance fields
.field b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

.field c:Landroid/widget/TextView;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x1

    const v0, 0x7f0b0143    # com.google.android.gms.R.string.wallet_disable_google_wallet

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setOrientation(I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04015e    # com.google.android.gms.R.layout.wallet_view_payment_form_top_bar

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a0376    # com.google.android.gms.R.id.payment_form_account_selector

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    const v0, 0x7f0a0375    # com.google.android.gms.R.id.payment_form_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->c:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->d:I

    return v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    :cond_0
    return-void
.end method

.method public final a(Lguw;)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Lguw;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->setEnabled(Z)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->setVisibility(I)V

    return-void
.end method

.method public final b(I)V
    .locals 7

    const/4 v4, 0x0

    iput p1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->d:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a()Landroid/accounts/Account;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->d:I

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lgry;->a()Lgry;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lgry;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lgth;->a([Landroid/accounts/Account;)[Lguu;

    move-result-object v1

    array-length v2, v1

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Lguu;

    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    new-instance v3, Lguu;

    sget-object v4, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b0144    # com.google.android.gms.R.string.wallet_google_wallet_disabled

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lguu;-><init>(Landroid/accounts/Account;Ljava/lang/String;)V

    aput-object v3, v2, v1

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lguu;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/accounts/Account;

    aput-object v0, v1, v4

    invoke-static {v1}, Lgth;->a([Landroid/accounts/Account;)[Lguu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lguu;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
