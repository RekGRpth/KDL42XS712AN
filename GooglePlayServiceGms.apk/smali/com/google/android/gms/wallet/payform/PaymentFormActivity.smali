.class public Lcom/google/android/gms/wallet/payform/PaymentFormActivity;
.super Lgyg;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lguw;
.implements Lgwv;
.implements Lgyf;
.implements Lhbp;


# instance fields
.field n:Lgwr;

.field o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

.field p:Landroid/view/ViewGroup;

.field q:Landroid/widget/CheckBox;

.field r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private s:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

.field private t:Z

.field private u:Landroid/accounts/Account;

.field private v:Ljava/util/HashSet;

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lgyg;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->w:Z

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 2

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->finish()V

    return-void
.end method

.method private b(I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private b(Landroid/support/v4/app/Fragment;)V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    invoke-virtual {v0, v1, p1}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    return-void
.end method

.method private c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->q:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private d(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    :cond_0
    return-void
.end method

.method private h()Landroid/support/v4/app/Fragment;
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const v1, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    invoke-virtual {v0, v1}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 6

    const/4 v5, -0x1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v0, v1, Lhar;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lhar;

    invoke-virtual {v0}, Lhar;->J()I

    move-result v4

    invoke-virtual {v0}, Lhar;->K()I

    move-result v5

    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    if-nez v0, :cond_5

    invoke-static {p0}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->n:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->n:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->n:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->n:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->n:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "PaymentFormActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_2
    instance-of v0, v1, Lhbk;

    if-eqz v0, :cond_7

    move-object v0, v1

    check-cast v0, Lhbk;

    invoke-virtual {v0}, Lhbk;->a()I

    move-result v4

    invoke-virtual {v0}, Lhbk;->b()I

    move-result v5

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->v:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->v:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-static {v1}, Lhgi;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgyd;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lgyd;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->s:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    invoke-static {v0, v1, v2, v4, v5}, Lhar;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/accounts/Account;II)Lhar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->g()V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a()I

    move-result v0

    if-nez v0, :cond_6

    const/4 v3, 0x1

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->s:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    invoke-static/range {v0 .. v5}, Lhbk;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/accounts/Account;ZII)Lhbk;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    :cond_6
    const/4 v3, 0x0

    goto :goto_3

    :cond_7
    move v4, v5

    goto/16 :goto_0
.end method

.method private j()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->p:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    if-nez v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x19b

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbkm;->b(Z)V

    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-virtual {v0, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    :goto_2
    iput-object p1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    if-nez v0, :cond_3

    :goto_3
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i()V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method public final a(Ljau;Lioj;)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->d(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->p:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->s:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v5

    move-object v2, p2

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lhao;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;Ljau;ZLjava/lang/String;)Lhao;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method public final b(Landroid/accounts/Account;)V
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/accounts/Account;)V

    invoke-static {}, Lgry;->a()Lgry;

    invoke-static {p0}, Lgry;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v1, v0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->w:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->j()V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->d(Z)V

    return-void
.end method

.method public final e()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->g()V

    return-void
.end method

.method public final f()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    if-nez p2, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/accounts/Account;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lgry;->a()Lgry;

    invoke-static {p0}, Lgry;->b(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "buyFlowConfig"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v3, Lgyr;->b:Lgyu;

    invoke-static {p0, v0, v3}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    invoke-super {p0, p1}, Lgyg;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040124    # com.google.android.gms.R.layout.wallet_activity_payment_form

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->setContentView(I)V

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    const v0, 0x7f0a02ff    # com.google.android.gms.R.id.chrome_checkbox_strip

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->p:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->p:Landroid/view/ViewGroup;

    const v3, 0x7f0a0368    # com.google.android.gms.R.id.pay_with_google_checkbox

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->q:Landroid/widget/CheckBox;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->v:Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->s:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    if-eqz p1, :cond_2

    const-string v0, "localMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->v:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    const-string v0, "walletModeNotPossible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->w:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->j()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Lguw;)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i()V

    :cond_1
    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    sget-object v0, Lgrt;->a:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    invoke-virtual {v0, v3}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->r:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v3

    const-string v4, "payment_form_activity"

    invoke-static {v0, v3, v4}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    instance-of v0, v0, Lhao;

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->d(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->p:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lgyg;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "PaymentFormActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->n:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->n:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->n:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lgyg;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->u:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->v:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "localMode"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "walletModeNotPossible"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
