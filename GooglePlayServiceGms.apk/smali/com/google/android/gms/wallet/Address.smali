.class public final Lcom/google/android/gms/wallet/Address;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Ljava/lang/String;

.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgqy;

    invoke-direct {v0}, Lgqy;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/Address;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/Address;->l:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/wallet/Address;->l:I

    iput-object p2, p0, Lcom/google/android/gms/wallet/Address;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/wallet/Address;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/wallet/Address;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/wallet/Address;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/wallet/Address;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/wallet/Address;->f:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/wallet/Address;->g:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/wallet/Address;->h:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/wallet/Address;->i:Ljava/lang/String;

    iput-boolean p11, p0, Lcom/google/android/gms/wallet/Address;->j:Z

    iput-object p12, p0, Lcom/google/android/gms/wallet/Address;->k:Ljava/lang/String;

    return-void
.end method

.method public static a()Lgqx;
    .locals 3

    new-instance v0, Lgqx;

    new-instance v1, Lcom/google/android/gms/wallet/Address;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/Address;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lgqx;-><init>(Lcom/google/android/gms/wallet/Address;B)V

    return-object v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/Address;->l:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lgqy;->a(Lcom/google/android/gms/wallet/Address;Landroid/os/Parcel;)V

    return-void
.end method
