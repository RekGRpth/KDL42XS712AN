.class public Lcom/google/android/gms/wallet/address/GoogleTopBarView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->setOrientation(I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04015a    # com.google.android.gms.R.layout.wallet_view_google_top_bar

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a036f    # com.google.android.gms.R.id.google_account_selector

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    return-void
.end method

.method public final a(Lguw;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {}, Lgry;->a()Lgry;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lgry;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lgth;->a([Landroid/accounts/Account;)[Lguu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lguu;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Lguw;)V

    return-void
.end method
