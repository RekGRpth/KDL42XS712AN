.class public Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lgsd;
.implements Lguw;
.implements Lgwv;
.implements Lgyf;


# instance fields
.field private A:I

.field n:Lgvu;

.field private o:Landroid/accounts/Account;

.field private p:Ljava/util/HashSet;

.field private q:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private r:Lipv;

.field private s:Lgsi;

.field private t:Lcom/google/android/gms/identity/intents/UserAddressRequest;

.field private u:Lgwr;

.field private v:Lcom/google/android/gms/wallet/address/GoogleTopBarView;

.field private w:Lgvt;

.field private x:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

.field private y:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lo;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->A:I

    new-instance v0, Lgrx;

    invoke-direct {v0, p0}, Lgrx;-><init>(Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->n:Lgvu;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/identity/intents/UserAddressRequest;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "request"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;Lipv;)Lipv;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    return-object p1
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 0

    if-nez p2, :cond_0

    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->finish()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j()V

    return-void
.end method

.method private a(ZI)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->A:I

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->A:I

    iget v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->A:I

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i()Z

    move-result v3

    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->x:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->w:Lgvt;

    invoke-interface {v1, v2}, Lgvt;->setEnabled(Z)V

    :cond_0
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j()V

    if-eqz v0, :cond_1

    if-nez p2, :cond_5

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->x:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->b()V

    :goto_3
    return-void

    :cond_2
    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->x:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->w:Lgvt;

    invoke-interface {v2, v1}, Lgvt;->setEnabled(Z)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->x:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->a(I)V

    goto :goto_3
.end method

.method private a(ZZ)V
    .locals 7

    const/4 v4, 0x1

    const/4 v1, 0x0

    move-object v5, p0

    :goto_0
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_0

    const v0, 0x7f0b0118    # com.google.android.gms.R.string.wallet_retrieving_wallet_information

    invoke-direct {v5, v4, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ZI)V

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lgzp;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move p2, v4

    move p1, v1

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_5

    iget-object v0, v5, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->s:Lgsi;

    iget-object v2, v5, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    invoke-virtual {v0, v2, v4}, Lgsi;->a(Landroid/accounts/Account;I)Lizx;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v2, v5, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->t:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    invoke-static {v0, v2}, Lgty;->a(Lizx;Lcom/google/android/gms/identity/intents/UserAddressRequest;)[Lipv;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a([Lipv;)V

    array-length v0, v0

    if-nez v0, :cond_3

    :goto_1
    invoke-direct {v5, v1, v4}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ZZ)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    move v4, v1

    goto :goto_1

    :cond_4
    invoke-direct {v5, v1, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ZI)V

    move p2, v4

    move p1, v1

    goto :goto_0

    :cond_5
    new-instance v0, Lgsb;

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lsf;

    move-result-object v2

    iget-object v3, v5, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lgsb;-><init>(Landroid/content/Context;Lsf;Landroid/accounts/Account;ILgsd;Landroid/os/Looper;)V

    invoke-virtual {v0}, Lgsb;->run()V

    goto :goto_2
.end method

.method private a([Lipv;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, p1

    if-lez v0, :cond_1

    invoke-direct {p0, v2, v2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ZI)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->w:Lgvt;

    invoke-interface {v0, p1}, Lgvt;->a([Lipv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    if-nez v0, :cond_0

    invoke-static {p1}, Lgth;->a([Lipv;)Lipv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->w:Lgvt;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    invoke-interface {v0, v1}, Lgvt;->a(Lipv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->w:Lgvt;

    invoke-interface {v0, v2}, Lgvt;->a(Z)V

    :cond_1
    return-void
.end method

.method private b(I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.identity.intents.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private g()V
    .locals 3

    invoke-static {p0}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->u:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->u:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->u:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->u:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->u:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "RequestUserAddressActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->q:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-static {v1}, Lhgi;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgyd;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lgyd;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :goto_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ZZ)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h()V

    goto :goto_1
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    :cond_0
    return-void
.end method

.method private i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->x:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->y:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method private k()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lhfx;->a(Lipv;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.identity.intents.EXTRA_ADDRESS"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private l()Ljava/lang/CharSequence;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->q:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "App Label not found"

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x19b

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, v1, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ZI)V

    const-string v0, "RequestUserAddressActivity"

    const-string v1, "Unexpected result from error dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->k()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    invoke-static {p1, v0}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g()V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;IILipi;)V
    .locals 3

    const/16 v2, 0x22b

    if-eqz p4, :cond_0

    iget-object v0, p4, Lipi;->b:[Lioj;

    array-length v0, v0

    if-nez v0, :cond_1

    iget-object v0, p4, Lipi;->c:[Lipv;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->s:Lgsi;

    invoke-virtual {v0, p1, p2, p4}, Lgsi;->a(Landroid/accounts/Account;ILipi;)V

    new-instance v0, Lizx;

    invoke-direct {v0}, Lizx;-><init>()V

    iget-object v1, p4, Lipi;->b:[Lioj;

    iput-object v1, v0, Lizx;->a:[Lioj;

    iget-object v1, p4, Lipi;->c:[Lipv;

    iput-object v1, v0, Lizx;->b:[Lipv;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->t:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    invoke-static {v0, v1}, Lgty;->a(Lizx;Lcom/google/android/gms/identity/intents/UserAddressRequest;)[Lipv;

    move-result-object v0

    if-eqz v0, :cond_2

    array-length v1, v0

    if-nez v1, :cond_3

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a([Lipv;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h()V

    return-void
.end method

.method public final f()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->k()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const v2, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->q:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "request"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/UserAddressRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->t:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->q:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->t:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->q:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lgyr;->b:Lgyu;

    invoke-static {p0, v0, v1}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040125    # com.google.android.gms.R.layout.wallet_activity_request_user_address

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->setContentView(I)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:Ljava/util/HashSet;

    const v0, 0x7f0a030b    # com.google.android.gms.R.id.google_top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->v:Lcom/google/android/gms/wallet/address/GoogleTopBarView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->v:Lcom/google/android/gms/wallet/address/GoogleTopBarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Lguw;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->q:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->q:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_2

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    const-string v0, "selectedAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "selectedAddress"

    const-class v1, Lipv;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->v:Lcom/google/android/gms/wallet/address/GoogleTopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Landroid/accounts/Account;)V

    const v0, 0x7f0a030f    # com.google.android.gms.R.id.google_address_selector

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgvt;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->w:Lgvt;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->w:Lgvt;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->n:Lgvu;

    invoke-interface {v0, v1}, Lgvt;->a(Lgvu;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->w:Lgvt;

    invoke-interface {v0}, Lgvt;->b()V

    new-instance v0, Lgsi;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgsi;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->s:Lgsi;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->y:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v0, 0x7f0a030e    # com.google.android.gms.R.id.app_name_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->z:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->z:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->l()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a02fe    # com.google.android.gms.R.id.prog_bar_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->x:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->y:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->y:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g()V

    return-void

    :cond_2
    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lo;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "RequestUserAddressActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->u:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->u:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->u:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    if-eqz v0, :cond_0

    const-string v0, "selectedAddress"

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->r:Lipv;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    return-void
.end method
