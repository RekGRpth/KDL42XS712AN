.class public final Lcom/google/android/gms/wallet/shared/service/ServerResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

.field public static final b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

.field public static final c:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

.field public static final d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;


# instance fields
.field public final e:I

.field public f:I

.field public g:[B

.field public h:Ljava/lang/String;

.field private i:Ljava/lang/Class;

.field private j:Lizs;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v1, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v1, 0x16

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->c:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    new-instance v0, Lhhj;

    invoke-direct {v0}, Lhhj;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->e:I

    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    return-void
.end method

.method public constructor <init>(II[BLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->e:I

    iput p2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    iput-object p4, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILizs;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->e:I

    iput p1, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lizs;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    :cond_0
    return-void
.end method

.method public constructor <init>(I[BLjava/lang/Class;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->e:I

    iput p1, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    return-void
.end method

.method public static a(I)Z
    .locals 1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lizs;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Lizm;->a([BII)Lizm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lizs;->a(Lizm;)Lizs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lizs;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse a known parcelable proto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse a known parcelable proto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse a known parcelable proto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown proto class type for responseType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private d()Ljava/lang/Class;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lizs;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a proto"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " class not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No proto class instance and unknown proto class name for responseType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->c()V

    iget v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    return v0
.end method

.method public final b()Lizs;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->c()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lizs;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lizs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lizs;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    invoke-static {p0, p1}, Lhhj;->a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;Landroid/os/Parcel;)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lizs;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lizs;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
