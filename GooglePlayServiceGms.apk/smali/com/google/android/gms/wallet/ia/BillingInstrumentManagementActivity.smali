.class public Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;
.super Lgxn;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lgwv;


# static fields
.field private static final E:Ljava/lang/String;


# instance fields
.field A:Lgwr;

.field B:Lgwr;

.field C:Lgxh;

.field D:Lgxh;

.field private F:Lioq;

.field private G:I

.field private H:Lgyi;

.field private I:Z

.field private J:Z

.field private K:Ljava/util/ArrayList;

.field private L:Z

.field private M:Luu;

.field private N:Lut;

.field private O:Luu;

.field private P:Lut;

.field private final Q:Lhcb;

.field public n:Liot;

.field public o:Lioj;

.field public p:Lioj;

.field q:I

.field r:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field s:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field t:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

.field public u:Landroid/view/View;

.field v:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

.field public z:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "billingInstrumentManagement"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lgxn;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->I:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->J:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:I

    new-instance v0, Lgzt;

    invoke-direct {v0, p0}, Lgzt;-><init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->C:Lgxh;

    new-instance v0, Lgzu;

    invoke-direct {v0, p0}, Lgzu;-><init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->D:Lgxh;

    new-instance v0, Lgzv;

    invoke-direct {v0, p0}, Lgzv;-><init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->Q:Lhcb;

    return-void
.end method

.method public static synthetic A(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->I:Z

    return v0
.end method

.method public static synthetic B(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lgwr;

    const-string v1, "inapp.BillingInstrumentManagementActivity.UpdatePaymentSettingsNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lgwr;Ljava/lang/String;)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lgwr;

    return-void
.end method

.method public static synthetic C(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lgwr;

    const-string v1, "inapp.BillingInstrumentManagementActivity.PaymentOptionsNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lgwr;Ljava/lang/String;)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lgwr;

    return-void
.end method

.method public static synthetic D(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic E(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic F(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private static a(Lioj;)I
    .locals 5

    const/16 v0, 0x79

    if-nez p0, :cond_1

    const/16 v0, 0x77

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lioj;->g:[I

    array-length v1, v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lioj;->g:[I

    array-length v4, v1

    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v1, p0, Lioj;->g:[I

    aget v1, v1, v2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    const/16 v3, 0x7d

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget v1, p0, Lioj;->h:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lioj;->e:Lipv;

    invoke-static {v0}, Lgth;->c(Lipv;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x7c

    goto :goto_0

    :cond_4
    const/16 v0, 0x7f

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x78

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method private a(Lgwr;Ljava/lang/String;)Lgwr;
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    invoke-virtual {v0, p1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v0, v1, p2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->K:Ljava/util/ArrayList;

    return-object p1
.end method

.method public static synthetic a(Liot;)Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Liot;->a:[Lioj;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, p0, Liot;->a:[Lioj;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v4, v4, Lioj;->e:Lipv;

    if-eqz v4, :cond_0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Liot;)V
    .locals 8

    const/4 v2, 0x0

    const/16 v7, 0x7f

    const/4 v6, 0x0

    iget-object v4, p1, Liot;->a:[Lioj;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    invoke-static {v4, v0}, Lgth;->a([Lioj;Lioj;)Lioj;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p1, Liot;->b:Ljava/lang/String;

    invoke-static {v4, v0}, Lgth;->a([Lioj;Ljava/lang/String;)Lioj;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lioj;)I

    move-result v1

    if-eq v1, v7, :cond_4

    move-object v1, v2

    :goto_0
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    const/4 v0, 0x1

    new-array v0, v0, [Lioj;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    aput-object v3, v0, v6

    invoke-static {v4, v0}, Lboz;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lioj;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    invoke-static {v0, v3}, Lgth;->a([Lioj;Lioj;)Lioj;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v3, p1, Liot;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lgth;->a([Lioj;Ljava/lang/String;)Lioj;

    move-result-object v3

    :cond_1
    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lioj;)I

    move-result v5

    if-eq v5, v7, :cond_3

    :goto_1
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->I:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v3, v6}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a([Lioj;)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lioj;)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v3, v6}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a([Lioj;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lioj;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    :cond_2
    return-void

    :cond_3
    move-object v2, v3

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ZI)V

    return-void
.end method

.method private a(ZI)V
    .locals 3

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->G:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->G:I

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Lioj;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lioj;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7c
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Landroid/accounts/Account;

    return-object v0
.end method

.method private b(I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->G:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    :goto_1
    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    if-nez v0, :cond_5

    move v3, v1

    :goto_3
    invoke-virtual {v4, v3}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    if-nez v0, :cond_6

    :goto_4
    invoke-virtual {v3, v1}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i()V

    :cond_0
    if-eqz v0, :cond_1

    if-nez p1, :cond_7

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->b()V

    :goto_5
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->a(I)V

    goto :goto_5
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lioq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->F:Lioq;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->K:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Landroid/accounts/Account;

    return-object v0
.end method

.method private g()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->L:Z

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->G:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(I)V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.BillingInstrumentManagementActivity.PaymentOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lgwr;

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.BillingInstrumentManagementActivity.UpdatePaymentSettingsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lgwr;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->Q:Lhcb;

    invoke-interface {v0, v1}, Lhca;->a(Lhcb;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->Q:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:I

    invoke-interface {v0, v1, v2}, Lhca;->a(Lhcb;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->Q:Lhcb;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    invoke-virtual {v0, v1}, Lhcb;->a(Liot;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l()V

    goto :goto_0
.end method

.method public static synthetic h(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method private h()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->H:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->H:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->H:Lgyi;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Landroid/accounts/Account;

    return-object v0
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic j(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method private j()V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->Q:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:I

    :cond_0
    return-void
.end method

.method public static synthetic k(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Landroid/accounts/Account;

    return-object v0
.end method

.method private k()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->O:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->P:Lut;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Luu;

    const-string v1, "billing_update_payment_settings"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->O:Luu;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->O:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->P:Lut;

    :cond_1
    new-instance v0, Liow;

    invoke-direct {v0}, Liow;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->F:Lioq;

    iput-object v1, v0, Liow;->a:Lioq;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    iget-object v1, v1, Lioj;->a:Ljava/lang/String;

    iput-object v1, v0, Liow;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    iget-object v1, v1, Lioj;->a:Ljava/lang/String;

    iput-object v1, v0, Liow;->c:Ljava/lang/String;

    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ZI)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    invoke-interface {v1, v0}, Lhca;->a(Liow;)V

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->J:Z

    return-void
.end method

.method private l()V
    .locals 2

    new-instance v0, Lios;

    invoke-direct {v0}, Lios;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->F:Lioq;

    iput-object v1, v0, Lios;->a:Lioq;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    invoke-interface {v1, v0}, Lhca;->a(Lios;)V

    const/4 v0, 0x1

    const v1, 0x7f0b0118    # com.google.android.gms.R.string.wallet_retrieving_wallet_information

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ZI)V

    return-void
.end method

.method public static synthetic l(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->J:Z

    return v0
.end method

.method public static synthetic m(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic n(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic o(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic p(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->O:Luu;

    return-object v0
.end method

.method public static synthetic q(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->P:Lut;

    return-object v0
.end method

.method public static synthetic r(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->O:Luu;

    return-object v0
.end method

.method public static synthetic s(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->P:Lut;

    return-object v0
.end method

.method public static synthetic t(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->J:Z

    return v0
.end method

.method public static synthetic u(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->M:Luu;

    return-object v0
.end method

.method public static synthetic v(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->N:Lut;

    return-object v0
.end method

.method public static synthetic w(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->M:Luu;

    return-object v0
.end method

.method public static synthetic x(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->N:Lut;

    return-object v0
.end method

.method public static synthetic y(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic z(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_1

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l()V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->I:Z

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "instrument_management"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->L:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g()V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lgxn;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    :pswitch_0
    if-ne p2, v1, :cond_1

    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "Successfully added a primary instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lioj;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "User canceled adding a primary instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "BillingInstrumentManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding a primary instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    if-ne p2, v1, :cond_3

    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "Successfully added a backup instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lioj;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    goto :goto_0

    :cond_3
    if-nez p2, :cond_4

    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "User canceled adding a backup instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v0, "BillingInstrumentManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding a backup instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_2
    packed-switch p2, :pswitch_data_1

    const-string v0, "BillingInstrumentManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating a primary instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "Successfully updated a primary instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lioj;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    goto/16 :goto_0

    :pswitch_4
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "User canceled updating a primary instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_5
    packed-switch p2, :pswitch_data_2

    const-string v0, "BillingInstrumentManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating a backup instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_6
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "Successfully updated a backup instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lioj;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    goto/16 :goto_0

    :pswitch_7
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "User canceled updating a backup instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lgxn;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Activity requires pcid extra!"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v1, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lioq;

    invoke-direct {v1}, Lioq;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->F:Lioq;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->F:Lioq;

    iput-object v0, v1, Lioq;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lgyr;->b:Lgyu;

    invoke-static {p0, v0, v1}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f04011f    # com.google.android.gms.R.layout.wallet_activity_billing_instrument_management

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->setContentView(I)V

    const v0, 0x7f0a02eb    # com.google.android.gms.R.id.primary_instrument_selector

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->C:Lgxh;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lgxh;)V

    const v0, 0x7f0a02ec    # com.google.android.gms.R.id.backup_instrument_selector

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->D:Lgxh;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lgxh;)V

    const v0, 0x7f0a02ea    # com.google.android.gms.R.id.payment_options_content

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Landroid/view/View;

    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_1

    const-string v0, "hasRequestedToUpdatePaymentSettings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->J:Z

    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:I

    const-string v0, "paymentInstrumentsResponse"

    const-class v1, Liot;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Liot;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    const-string v0, "selectedPrimaryInstrument"

    const-class v1, Lioj;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    const-string v0, "selectedBackupInstrument"

    const-class v1, Lioj;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->I:Z

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h()Lgyi;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Landroid/accounts/Account;

    invoke-static {v0, v1, v2}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->H:Lgyi;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->H:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    return-void

    :cond_1
    new-instance v0, Luu;

    const-string v1, "billing_get_payment_options"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->M:Luu;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->M:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->N:Lut;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "instrument_management"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lgxn;->onPause()V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->j()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lgxn;->onResume()V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->I:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->L:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lgxn;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->j()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "hasRequestedToUpdatePaymentSettings"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->J:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    if-eqz v0, :cond_0

    const-string v0, "paymentInstrumentsResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    if-eqz v0, :cond_1

    const-string v0, "selectedPrimaryInstrument"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    if-eqz v0, :cond_2

    const-string v0, "selectedBackupInstrument"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lioj;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_2
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->I:Z

    invoke-super {p0, p1, p2}, Lgxn;->startActivityForResult(Landroid/content/Intent;I)V

    const v0, 0x7f050011    # com.google.android.gms.R.anim.wallet_push_up_in

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->overridePendingTransition(II)V

    return-void
.end method
