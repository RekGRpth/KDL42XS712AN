.class public Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lgyq;


# instance fields
.field a:Landroid/widget/Spinner;

.field b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

.field private c:Lioh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04015d    # com.google.android.gms.R.layout.wallet_view_payment_amount_input

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a0373    # com.google.android.gms.R.id.payment_amount_spinner

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v0, 0x7f0a0374    # com.google.android.gms.R.id.money_amount_input

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->S_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->R_()Z

    :cond_0
    return v0
.end method

.method public final S_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->S_()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a()Lioh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a()Lioh;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->c:Lioh;

    goto :goto_0
.end method

.method public final a(Lioh;Lioh;Lioh;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iput-object p2, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->c:Lioh;

    if-eqz p3, :cond_0

    iget-wide v0, p3, Lioh;->a:J

    :goto_0
    iget-wide v2, p2, Lioh;->a:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b019b    # com.google.android.gms.R.string.wallet_pay_amount_due

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->c:Lioh;

    invoke-static {v4}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b019c    # com.google.android.gms.R.string.wallet_pay_other_amount

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f04014d    # com.google.android.gms.R.layout.wallet_row_payment_amount_spinner

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->c:Lioh;

    iget-object v1, v1, Lioh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Lioh;Lioh;)V

    return-void

    :cond_0
    iget-wide v0, p1, Lioh;->a:J

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->i()Z

    move-result v0

    return v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    packed-switch p3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->setEnabled(Z)V

    return-void
.end method
