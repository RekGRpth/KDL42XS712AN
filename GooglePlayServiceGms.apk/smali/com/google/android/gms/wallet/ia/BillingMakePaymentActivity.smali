.class public Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;
.super Lgxn;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lgwv;
.implements Lgxh;


# static fields
.field private static final F:Ljava/lang/String;


# instance fields
.field public A:Lcom/google/android/gms/wallet/common/PaymentModel;

.field B:Ljava/lang/String;

.field C:Z

.field D:I

.field public E:Liot;

.field private G:Lioq;

.field private H:Lioh;

.field private I:Lgyi;

.field private J:I

.field private K:Z

.field private L:Luu;

.field private M:Lut;

.field private N:Luu;

.field private O:Lut;

.field private P:Ljava/util/ArrayList;

.field private final Q:Lhcb;

.field n:Landroid/widget/TextView;

.field o:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

.field p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field q:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field public r:Lgxg;

.field s:Landroid/widget/CheckBox;

.field t:Landroid/widget/TextView;

.field public u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

.field v:Lgwr;

.field z:Lgwr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "billingMakePayment"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->F:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lgxn;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->B:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:I

    new-instance v0, Lgzw;

    invoke-direct {v0, p0}, Lgzw;-><init>(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->Q:Lhcb;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lioh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->H:Lioh;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->P:Ljava/util/ArrayList;

    return-object p1
.end method

.method public static a(Liot;)Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Liot;->a:[Lioj;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, p0, Liot;->a:[Lioj;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v4, v4, Lioj;->e:Lipv;

    if-eqz v4, :cond_0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Liot;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p1, Liot;->a:[Lioj;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-static {v1, v0}, Lgth;->a([Lioj;Lioj;)Lioj;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Lgxg;

    invoke-interface {v2, v1}, Lgxg;->a([Lioj;)V

    if-nez v0, :cond_0

    invoke-static {p1}, Lgth;->a(Liot;)Lioj;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c(Lioj;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Lgxg;

    invoke-interface {v1, v0}, Lgxg;->a(Lioj;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Lgxg;

    invoke-interface {v0, v3}, Lgxg;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->setVisibility(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ZI)V

    return-void
.end method

.method private a(ZI)V
    .locals 3

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->J:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->J:I

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->N:Luu;

    return-object v0
.end method

.method private b(I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->J:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j()Z

    move-result v3

    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Lgxg;

    invoke-interface {v1, v2}, Lgxg;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->setEnabled(Z)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k()V

    :cond_0
    if-eqz v0, :cond_1

    if-nez p1, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->b()V

    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Lgxg;

    invoke-interface {v2, v1}, Lgxg;->setEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->setEnabled(Z)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->a(I)V

    goto :goto_2
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->O:Lut;

    return-object v0
.end method

.method private static c(Lioj;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lioj;->h:I

    if-ne v2, v1, :cond_3

    iget-object v2, p0, Lioj;->e:Lipv;

    invoke-static {v2}, Lgth;->c(Lipv;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-nez v2, :cond_2

    invoke-static {p0}, Lgth;->e(Lioj;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->N:Luu;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->O:Lut;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l()V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method private g()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->C:Z

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->J:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(I)V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.BillingMakePaymentActivity.PaymentOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lgwr;

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.BillingMakePaymentActivity.MakePaymentNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lgwr;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->Q:Lhcb;

    invoke-interface {v0, v1}, Lhca;->a(Lhcb;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->Q:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:I

    invoke-interface {v0, v1, v2}, Lhca;->a(Lhcb;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:I

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l()V

    goto :goto_0
.end method

.method public static synthetic h(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method private h()V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->Q:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:I

    :cond_0
    return-void
.end method

.method private i()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->I:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->I:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->I:Lgyi;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->L:Luu;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->M:Lut;

    return-object v0
.end method

.method private j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic k(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Luu;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->L:Luu;

    return-object v0
.end method

.method private k()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public static synthetic l(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lut;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->M:Lut;

    return-object v0
.end method

.method private l()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->Q:Lhcb;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    invoke-virtual {v0, v1}, Lhcb;->a(Liot;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lios;

    invoke-direct {v0}, Lios;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->G:Lioq;

    iput-object v1, v0, Lios;->a:Lioq;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    invoke-interface {v1, v0}, Lhca;->a(Lios;)V

    const/4 v0, 0x1

    const v1, 0x7f0b0118    # com.google.android.gms.R.string.wallet_retrieving_wallet_information

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ZI)V

    goto :goto_0
.end method

.method public static synthetic m(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic n(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic o(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic p(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic q(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic r(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic s(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static synthetic t(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic u(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    return v0
.end method

.method public static synthetic v(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "inapp.BillingMakePaymentActivity.MakePaymentNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic w(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "inapp.BillingMakePaymentActivity.PaymentOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic x(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic y(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    const/4 v5, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x:Landroid/accounts/Account;

    const/4 v4, 0x1

    iget-object v10, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->G:Lioq;

    iget-object v11, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->P:Ljava/util/ArrayList;

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    move v8, v5

    move-object v9, v2

    invoke-static/range {v0 .. v11}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lioq;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final a(II)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, v1, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ZI)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lioj;)V
    .locals 9

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c(Lioj;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x:Landroid/accounts/Account;

    const/4 v3, 0x1

    iget-object v6, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->G:Lioq;

    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->P:Ljava/util/ArrayList;

    move-object v2, p1

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;ZZLjava/lang/String;Lioq;Ljava/util/Collection;Lipv;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    iget-object v0, v0, Liot;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    iget-object v0, v0, Liot;->b:Ljava/lang/String;

    iget-object v1, p1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Lioj;)V
    .locals 0

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->t:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->t:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->B:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->t:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "make_payment"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->C:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g()V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lgxn;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const-string v0, "BillingMakePaymentActiv"

    const-string v1, "Successfully added an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lioj;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    const-string v0, "BillingMakePaymentActiv"

    const-string v1, "User canceled adding an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "BillingMakePaymentActiv"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding an instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    packed-switch p2, :pswitch_data_1

    const-string v0, "BillingMakePaymentActiv"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating an instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    const-string v0, "BillingMakePaymentActiv"

    const-string v1, "Successfully updated an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lioj;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "BillingMakePaymentActiv"

    const-string v1, "User canceled updating an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->R_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->i()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->L:Luu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->M:Lut;

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Luu;

    const-string v1, "billing_make_payment"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->L:Luu;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->L:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->M:Lut;

    :cond_2
    new-instance v0, Liou;

    invoke-direct {v0}, Liou;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->G:Lioq;

    iput-object v1, v0, Liou;->a:Lioq;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v1, v1, Lioj;->a:Ljava/lang/String;

    iput-object v1, v0, Liou;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a()Lioh;

    move-result-object v1

    iput-object v1, v0, Liou;->c:Lioh;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iput-boolean v2, v0, Liou;->d:Z

    :cond_3
    const/4 v1, 0x0

    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ZI)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    invoke-interface {v1, v0}, Lhca;->a(Liou;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lgxn;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "PCID is required"

    invoke-static {v3, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v3, "com.google.android.gms.wallet.currencyCode"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "Currency code is required"

    invoke-static {v3, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v3, "com.google.android.gms.wallet.amountDueMicros"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "Micro amount is required"

    invoke-static {v3, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v3, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lioq;

    invoke-direct {v4}, Lioq;-><init>()V

    iput-object v4, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->G:Lioq;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->G:Lioq;

    iput-object v3, v4, Lioq;->a:Ljava/lang/String;

    const-string v3, "com.google.android.gms.wallet.currencyCode"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google.android.gms.wallet.amountDueMicros"

    const-wide/16 v5, -0x1

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_3

    move v0, v1

    :goto_0
    const-string v6, "Micro amount must be >= 0"

    invoke-static {v0, v6}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lioh;

    invoke-direct {v0}, Lioh;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->H:Lioh;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->H:Lioh;

    iput-object v3, v0, Lioh;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->H:Lioh;

    iput-wide v4, v0, Lioh;->a:J

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v3, Lgyr;->b:Lgyu;

    invoke-static {p0, v0, v3}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f040122    # com.google.android.gms.R.layout.wallet_activity_make_payment

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->setContentView(I)V

    const v0, 0x7f0a02f9    # com.google.android.gms.R.id.make_payment_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->n:Landroid/widget/TextView;

    const v0, 0x7f0a02fe    # com.google.android.gms.R.id.prog_bar_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    const v0, 0x7f0a02fb    # com.google.android.gms.R.id.instrument_selector

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgxg;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Lgxg;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Lgxg;

    invoke-interface {v0, p0}, Lgxg;->a(Lgxh;)V

    const v0, 0x7f0a02fc    # com.google.android.gms.R.id.make_primary_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s:Landroid/widget/CheckBox;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v0, 0x7f0a02fa    # com.google.android.gms.R.id.instrument_error_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->t:Landroid/widget/TextView;

    const v0, 0x7f0a02fd    # com.google.android.gms.R.id.payment_amount_input

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_4

    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    const-string v0, "paymentOptionsPostResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "paymentOptionsPostResponse"

    const-class v2, Liot;

    invoke-static {p1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Liot;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    :cond_0
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "serviceConnectionSavePoint"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:I

    :cond_1
    const-string v0, "instrumentErrorTextData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i()Lgyi;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x:Landroid/accounts/Account;

    invoke-static {v1, v0, v2}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->I:Lgyi;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->I:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto/16 :goto_0

    :cond_4
    new-instance v0, Luu;

    const-string v3, "billing_get_payment_options"

    invoke-direct {v0, v3}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->N:Luu;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->N:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->O:Lut;

    new-instance v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "make_payment"

    invoke-static {v0, v2, v3}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lgxn;->onPause()V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lgxn;->onResume()V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->C:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lgxn;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "model"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    if-eqz v0, :cond_0

    const-string v0, "paymentOptionsPostResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Liot;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    const-string v0, "instrumentErrorTextData"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->K:Z

    invoke-super {p0, p1, p2}, Lgxn;->startActivityForResult(Landroid/content/Intent;I)V

    const v0, 0x7f050011    # com.google.android.gms.R.anim.wallet_push_up_in

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->overridePendingTransition(II)V

    return-void
.end method
