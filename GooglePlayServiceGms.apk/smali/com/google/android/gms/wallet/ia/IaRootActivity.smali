.class public Lcom/google/android/gms/wallet/ia/IaRootActivity;
.super Lgyg;
.source "SourceFile"

# interfaces
.implements Lgwv;
.implements Lgyf;
.implements Lhah;


# instance fields
.field n:Lgwr;

.field private o:Landroid/accounts/Account;

.field private p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field private r:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lgyg;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 2

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wallet/ia/IaRootActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method private a(Landroid/accounts/Account;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->o:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->o:Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    :cond_0
    return-void
.end method

.method private b(Landroid/support/v4/app/Fragment;)V
    .locals 3

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lag;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.libraries.inapp.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->finish()V

    return-void
.end method

.method private h()V
    .locals 4

    invoke-static {p0}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->n:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->n:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->n:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->n:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->n:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "inapp.RootActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->r:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->r:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->o:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-static {v1}, Lhgi;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgyd;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lgyd;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.wallet.ACTION_START_BILLING_ENROLLMENT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v0, "RootActivity"

    const-string v1, "Missing PCID in launching intent for billing enrollment"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.libraries.inapp.ERROR_CODE_INVALID_PARAMETER"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->i()V

    goto :goto_1

    :cond_3
    const-string v2, "com.google.android.gms.wallet.infoText"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->o:Landroid/accounts/Account;

    invoke-static {v1, v0, v2, v3}, Lhad;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lhad;

    move-result-object v0

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    :cond_4
    const-string v1, "com.google.android.libraries.inapp.EXTRA_JWT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v0, "RootActivity"

    const-string v1, "Missing JWT in launching intent for purchase"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.libraries.inapp.ERROR_CODE_INVALID_PARAMETER"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->o:Landroid/accounts/Account;

    invoke-static {v0, v1, v2}, Lhad;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lhad;

    move-result-object v0

    goto :goto_2
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const-string v0, "com.google.android.libraries.inapp.ERROR_CODE_NETWORK_ERROR"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "com.google.android.libraries.inapp.ERROR_CODE_CANNOT_AUTHENTICATE"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 3

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->j()V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->h()V

    goto :goto_0

    :cond_0
    const-string v0, "RootActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    new-instance v0, Lhaa;

    invoke-direct {v0}, Lhaa;-><init>()V

    invoke-virtual {v0, p2}, Lhaa;->a(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lhaa;->a(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, ""

    if-eqz p1, :cond_3

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v2

    :goto_0
    if-eqz v3, :cond_4

    const-string v0, "jwt"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "request"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v1, "response"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    :goto_1
    invoke-static {v2}, Lgsv;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v5, "rawJson"

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {v1}, Lgsv;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz v1, :cond_1

    const-string v5, "rawJson"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v1, "com.google.android.libraries.inapp.EXTRA_JWT"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.libraries.inapp.EXTRA_REQUEST"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v0, "com.google.android.libraries.inapp.EXTRA_RESPONSE"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_2
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->finish()V

    return-void

    :catch_0
    move-exception v2

    const-string v2, "JwtUtils"

    const-string v3, "Invalid JSON in JWT body"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object v3, v1

    goto :goto_0

    :cond_4
    move-object v2, v1

    goto :goto_1
.end method

.method public final e()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->i()V

    return-void
.end method

.method public final f()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->j()V

    return-void
.end method

.method public final g()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->j()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1}, Lgyg;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lgyr;->b:Lgyu;

    invoke-static {p0, v0, v1}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f040126    # com.google.android.gms.R.layout.wallet_activity_simple_dialog

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setContentView(I)V

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    if-eqz p1, :cond_0

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Landroid/accounts/Account;)V

    const-string v0, "hasAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->r:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->h()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "inapp_root_activity"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lgyg;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.RootActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->n:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->n:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->n:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "RootActivity"

    const-string v1, "Saving instance state..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lgyg;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->o:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "hasAuthTokens"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->r:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
