.class public Lcom/google/android/gms/wallet/ia/CreateProfileActivity;
.super Lgxn;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lgug;
.implements Lguk;
.implements Lgwv;
.implements Lgxt;
.implements Lgyq;


# static fields
.field private static final F:Ljava/lang/String;


# instance fields
.field public A:Lgvc;

.field B:Landroid/widget/TextView;

.field C:Landroid/widget/TextView;

.field D:Lgwr;

.field E:Lgwr;

.field private G:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private H:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

.field private I:Ljava/lang/String;

.field private J:Z

.field private K:Lgyi;

.field private L:Ljava/util/ArrayList;

.field private M:Ljava/util/ArrayList;

.field private N:I

.field private O:Ljava/util/ArrayList;

.field private P:Landroid/util/Pair;

.field private Q:I

.field private R:Lguh;

.field private S:Lgue;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Lipg;

.field private X:[Ljava/lang/String;

.field private Y:Lioq;

.field private final Z:Lhcb;

.field n:I

.field public o:Z

.field p:Landroid/view/View;

.field q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field s:Landroid/widget/TextView;

.field t:Landroid/view/View;

.field public u:Lgvc;

.field public v:Lgwj;

.field z:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "createProfile"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->F:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lgxn;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:Z

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->U:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->V:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:I

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Z

    new-instance v0, Lgzy;

    invoke-direct {v0, p0}, Lgzy;-><init>(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Z:Lhcb;

    return-void
.end method

.method private a(Lgwr;Ljava/lang/String;)Lgwr;
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    invoke-virtual {v0, p1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v0, v1, p2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Lipg;)Lipg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->W:Lipg;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->I:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Landroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 11

    const v10, 0x7f0a02f3    # com.google.android.gms.R.id.instrument_entry_fragment

    const v9, 0x7f0a02ef    # com.google.android.gms.R.id.legal_address_fragment

    const/4 v1, 0x1

    const/16 v8, 0x8

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Ljava/util/ArrayList;

    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:I

    invoke-static {v0}, Lhgq;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v4

    move v2, v4

    :goto_0
    if-ge v5, v6, :cond_0

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto :goto_0

    :cond_0
    if-nez v2, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    :goto_2
    invoke-static {v2}, Lhgq;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:I

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(I)V

    :goto_3
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0, v9}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgvc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lgvq;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->U:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Lgvq;->a(Z)Lgvq;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Lgvq;->a(Ljava/util/List;)Lgvq;

    move-result-object v0

    invoke-virtual {v0, v2}, Lgvq;->a(Ljava/lang/String;)Lgvq;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->P:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Lgvq;->a(Ljava/util/ArrayList;)Lgvq;

    move-result-object v0

    iget-object v0, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Ljava/util/ArrayList;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:Z

    invoke-static {v0, v2, v3, v4}, Lgvc;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;Ljava/util/Collection;ZZ)Lgvc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    invoke-virtual {v0, v9, v2}, Lag;->a(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    invoke-virtual {v0, p0}, Lgvc;->a(Lgxt;)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->V:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0, v10}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwj;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->x:Landroid/accounts/Account;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static {v0, v1, v2, v3, v5}, Lgwj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;I[I[I)Lgwj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    invoke-virtual {v0, v10, v1}, Lag;->a(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lo;->b:Lw;

    const v1, 0x7f0a02f7    # com.google.android.gms.R.id.billing_address_fragment

    invoke-virtual {v0, v1}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgvc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(Z)V

    :cond_3
    :goto_5
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:I

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    return-void

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n()V

    goto/16 :goto_3

    :cond_6
    move v0, v4

    goto/16 :goto_4

    :cond_7
    const v0, 0x7f0a02f0    # com.google.android.gms.R.id.payment_method_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a02f1    # com.google.android.gms.R.id.payment_method_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_8
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Landroid/widget/CheckBox;

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_5

    :cond_9
    move-object v2, v3

    goto/16 :goto_2

    :cond_a
    move v0, v2

    goto/16 :goto_1
.end method

.method private b(I)V
    .locals 2

    iput p1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n()V

    return-void
.end method

.method private b(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    :cond_0
    const/4 v3, 0x3

    new-array v4, v3, [Lgyo;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    aput-object v3, v4, v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    aput-object v3, v4, v1

    const/4 v3, 0x2

    aput-object v0, v4, v3

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    if-eqz v6, :cond_1

    if-eqz p1, :cond_3

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v0, :cond_2

    move v0, v1

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_1

    :goto_2
    return v2

    :cond_4
    move v2, v0

    goto :goto_2
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private c(Z)V
    .locals 5

    const v3, 0x7f0a02f6    # com.google.android.gms.R.id.billing_address_divider

    const v0, 0x7f0a02f5    # com.google.android.gms.R.id.billing_address_text

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    invoke-virtual {v0}, Lgvc;->a()Lixo;

    move-result-object v0

    iget-object v2, v0, Lixo;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    invoke-virtual {v0}, Lgvc;->a()Lixo;

    move-result-object v0

    iget-object v3, v0, Lixo;->s:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lgvq;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->U:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v4, v0}, Lgvq;->a(Z)Lgvq;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Lgvq;->a(Ljava/util/List;)Lgvq;

    move-result-object v0

    invoke-virtual {v0, v2}, Lgvq;->a(Ljava/lang/String;)Lgvq;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->P:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Lgvq;->a(Ljava/util/ArrayList;)Lgvq;

    move-result-object v0

    iget-object v0, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Ljava/util/ArrayList;

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:Z

    invoke-static {v0, v2, v4, v1}, Lgvc;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;Ljava/util/Collection;ZZ)Lgvc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    new-instance v0, Lixo;

    invoke-direct {v0}, Lixo;-><init>()V

    iput-object v3, v0, Lixo;->s:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    invoke-virtual {v1, v0}, Lgvc;->a(Lixo;)V

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a02f7    # com.google.android.gms.R.id.billing_address_fragment

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    invoke-virtual {v0, v1, v2}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private d(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez p1, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    if-nez p1, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lgvc;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    if-nez p1, :cond_6

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lgwj;->a(Z)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Landroid/widget/CheckBox;

    if-nez p1, :cond_7

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    if-nez p1, :cond_8

    :goto_5
    invoke-virtual {v0, v1}, Lgvc;->a(Z)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->t:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_5
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic h(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic i(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Lipg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->W:Lipg;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Ljava/util/ArrayList;

    return-object v0
.end method

.method private j()V
    .locals 2

    new-instance v0, Lipf;

    invoke-direct {v0}, Lipf;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v1}, Lhab;->a([Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;)[Lioy;

    move-result-object v1

    iput-object v1, v0, Lipf;->b:[Lioy;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    invoke-interface {v1, v0}, Lhca;->a(Lipf;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->G:Ljava/lang/String;

    iput-object v1, v0, Lipf;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private k()V
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    invoke-virtual {v0}, Lgvc;->a()Lixo;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    invoke-virtual {v0}, Lgvc;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->V:Z

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v1, v2

    move-object v3, v4

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    invoke-virtual {v0}, Lgwj;->a()Linv;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Linv;

    iget-object v5, v0, Linv;->b:Lint;

    iput-object v3, v5, Lint;->d:Lixo;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:Z

    if-eqz v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Linv;->b:Lint;

    iput-object v1, v3, Lint;->g:Ljava/lang/String;

    :cond_0
    new-instance v1, Lipb;

    invoke-direct {v1}, Lipb;-><init>()V

    new-instance v3, Linx;

    invoke-direct {v3}, Linx;-><init>()V

    iput-object v4, v3, Linx;->a:Lixo;

    if-eqz v0, :cond_1

    iput-object v0, v3, Linx;->c:Linv;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:Z

    if-eqz v0, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-object v2, v3, Linx;->b:Ljava/lang/String;

    :cond_2
    iput-object v3, v1, Lipb;->b:Linx;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->I:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->I:Ljava/lang/String;

    iput-object v0, v1, Lipb;->c:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v0}, Lhab;->a([Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;)[Lioy;

    move-result-object v0

    iput-object v0, v1, Lipb;->e:[Lioy;

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Y:Lioq;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Y:Lioq;

    iput-object v0, v1, Lipb;->a:Lioq;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->X:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->X:[Ljava/lang/String;

    iput-object v0, v1, Lipb;->d:[Ljava/lang/String;

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    invoke-interface {v0, v1}, Lhca;->a(Lipb;)V

    iput-boolean v6, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->J:Z

    :goto_2
    return-void

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    invoke-virtual {v0}, Lgvc;->a()Lixo;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    invoke-virtual {v0}, Lgvc;->b()Ljava/lang/String;

    move-result-object v0

    move-object v3, v1

    move-object v1, v0

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->G:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->G:Ljava/lang/String;

    iput-object v0, v1, Lipb;->f:Ljava/lang/String;

    goto :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i()Z

    goto :goto_2
.end method

.method public static synthetic k(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:Lgwr;

    const-string v1, "inapp.SignupActivity.CreateProfileNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lgwr;Ljava/lang/String;)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:Lgwr;

    return-void
.end method

.method private l()V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Z:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:I

    :cond_0
    return-void
.end method

.method public static synthetic l(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Lgwr;

    const-string v1, "inapp.SignupActivity.LegalDocsNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lgwr;Ljava/lang/String;)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Lgwr;

    return-void
.end method

.method private m()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Lgyi;

    return-object v0
.end method

.method public static synthetic m(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private n()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final a()Lgue;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->S:Lgue;

    if-nez v0, :cond_0

    new-instance v0, Lgue;

    invoke-direct {v0, p0}, Lgue;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->S:Lgue;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->S:Lgue;

    return-object v0
.end method

.method public final a(II)V
    .locals 3

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->W:Lipg;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j()V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Lguh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->R:Lguh;

    if-nez v0, :cond_0

    new-instance v0, Lguh;

    invoke-direct {v0, p0}, Lguh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->R:Lguh;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->R:Lguh;

    return-object v0
.end method

.method public final c(I)V
    .locals 6

    iput p1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:I

    invoke-static {p1}, Lhgq;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b011b    # com.google.android.gms.R.string.wallet_terms_of_service

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lgth;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b012d    # com.google.android.gms.R.string.wallet_privacy_policy_link

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0b0139    # com.google.android.gms.R.string.wallet_privacy_policy_display_text

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lgth;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0b013a    # com.google.android.gms.R.string.wallet_tos_and_privacy_format

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Landroid/widget/TextView;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->X:[Ljava/lang/String;

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No legal documents for selected country"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final i()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    :cond_0
    const/4 v3, 0x3

    new-array v3, v3, [Lgyq;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    aput-object v4, v3, v2

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    aput-object v4, v3, v1

    const/4 v4, 0x2

    aput-object v0, v3, v4

    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    if-eqz v5, :cond_1

    invoke-interface {v5}, Lgyq;->i()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lgxn;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v0, "com.google.android.gms.wallet.brokerId"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->G:Ljava/lang/String;

    const-string v0, "com.google.android.gms.wallet.brokerAndRelationships"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v4, v0

    if-lez v4, :cond_0

    array-length v4, v0

    const-class v5, [Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    :cond_0
    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->I:Ljava/lang/String;

    const-string v0, "com.google.android.gms.wallet.addressHints"

    const-class v4, Lipv;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Ljava/util/ArrayList;

    invoke-static {v0}, Lgty;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->P:Landroid/util/Pair;

    const-string v0, "legalDocsForCountries"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Ljava/util/ArrayList;

    const-string v0, "com.google.android.gms.wallet.defaultCountryCode"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhgq;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:I

    const-string v0, "com.google.android.gms.wallet.requiresInstrument"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->V:Z

    const-string v0, "com.google.android.gms.wallet.requiresCreditCardFullAddress"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->U:Z

    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:Z

    const-string v0, "com.google.android.gms.wallet.accountReference"

    const-class v4, Lioq;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioq;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Y:Lioq;

    const-string v0, "com.google.android.gms.wallet.infoText"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->G:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v4, Lgyr;->a:Lgyu;

    invoke-static {p0, v0, v4}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f040121    # com.google.android.gms.R.layout.wallet_activity_create_profile

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->setContentView(I)V

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const v0, 0x7f0a02ee    # com.google.android.gms.R.id.create_profile_content

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->p:Landroid/view/View;

    const v0, 0x7f0a02f8    # com.google.android.gms.R.id.enrollment_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Landroid/widget/TextView;

    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->t:Landroid/view/View;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v0, 0x7f0a02f4    # com.google.android.gms.R.id.billing_same_as_legal_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Landroid/widget/CheckBox;

    const v0, 0x7f0a033b    # com.google.android.gms.R.id.butter_bar_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Y:Lioq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    const v4, 0x7f0b012c    # com.google.android.gms.R.string.wallet_accept_label

    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f0a02f2    # com.google.android.gms.R.id.info_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    if-eqz p1, :cond_6

    const-string v0, "errorMessageResourceId"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:I

    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->J:Z

    const-string v0, "regionCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:I

    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:I

    const-string v0, "legalDocumentsResponse"

    const-class v1, Lipg;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipg;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->W:Lipg;

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const v1, 0x7f0b0134    # com.google.android.gms.R.string.wallet_sign_up_title

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->q:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m()Lgyi;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->x:Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Lgyi;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v3, "create_profile"

    invoke-static {v0, v1, v3}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lgxn;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lgxn;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Z:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:I

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.SignupActivity.LegalDocsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Lgwr;

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.SignupActivity.CreateProfileNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Lgwr;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->W:Lipg;

    if-eqz v0, :cond_3

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Z:Lhcb;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->W:Lipg;

    invoke-virtual {v0, v1}, Lhcb;->a(Lipg;)V

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->J:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lgxn;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->J:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "regionCode"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "errorMessageResourceId"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->W:Lipg;

    if-eqz v0, :cond_0

    const-string v0, "legalDocumentsResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->W:Lipg;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    return-void
.end method
