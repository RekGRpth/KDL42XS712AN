.class public final Lcom/google/android/gms/playlog/service/PlayLogBrokerService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Z


# instance fields
.field private b:Lfly;

.field private c:Lfmd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lfku;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/playlog/service/PlayLogBrokerService;)Lfly;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->b:Lfly;

    return-object v0
.end method

.method public static synthetic a()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->a:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/gms/playlog/service/PlayLogBrokerService;)Lfmd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->c:Lfmd;

    return-object v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.gms.playlog.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lfls;

    invoke-direct {v0, p0}, Lfls;-><init>(Lcom/google/android/gms/playlog/service/PlayLogBrokerService;)V

    invoke-virtual {v0}, Lfls;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 2

    invoke-static {}, Lfly;->a()Lfly;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->b:Lfly;

    invoke-static {}, Lfmd;->a()Lfmd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->c:Lfmd;

    iget-object v0, p0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->b:Lfly;

    invoke-virtual {v0}, Lfly;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "PlayLogBrokerService"

    const-string v1, "Log store has data, so request immediate upload."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->c:Lfmd;

    invoke-virtual {v0}, Lfmd;->e()V

    :cond_1
    return-void
.end method
