.class public final Lcom/google/android/gms/init/InitializeIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/HashSet;


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, "InitializeIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/init/InitializeIntentService;->a:Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/init/InitializeIntentService;->a:Ljava/util/HashSet;

    const-string v1, "com.google.android.gms.INITIALIZE"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/init/InitializeIntentService;->a:Ljava/util/HashSet;

    const-string v1, "com.google.android.gms.GMS_UPDATED"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    const/4 v5, 0x3

    const/4 v9, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/gms/init/InitializeIntentService;->a:Ljava/util/HashSet;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-static {p0}, Lbox;->e(Landroid/content/Context;)I

    move-result v0

    invoke-static {}, Lbqe;->a()I

    move-result v3

    if-le v0, v3, :cond_6

    invoke-static {v0}, Lbqe;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/init/InitializeIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lbcm;->b()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v0, v4, :cond_7

    const-string v0, "SecurityFix"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SecurityFix"

    const-string v4, "Framework version is ICS or greator. No need for fix."

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-static {v3}, Lcun;->a(Landroid/content/Context;)Lcun;

    move-result-object v4

    :try_start_0
    invoke-virtual {v4, v3}, Lcun;->c(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, Lcun;->e(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-static {v3, v0}, Ldrc;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v4}, Lcun;->a()V

    const-string v0, "People"

    const-string v4, "initialize"

    invoke-static {v0, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "People"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Package updated, version="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lbox;->e(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v0, v4, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v3}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfhz;->a(Z)Z

    invoke-static {v3}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    invoke-static {v3}, Lfms;->a(Landroid/content/Context;)V

    const-string v0, "LocationInitializer"

    const-string v4, "Initiliazing location."

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v0, Lcom/google/android/location/fused/FusedLocationService;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    new-instance v5, Landroid/content/ComponentName;

    invoke-direct {v5, v3, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v0

    if-eqz v0, :cond_8

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_1
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x12

    if-lt v6, v7, :cond_9

    if-nez v0, :cond_1

    invoke-virtual {v4, v5, v1, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_1
    :goto_2
    invoke-static {v3}, Lcom/google/android/location/internal/NlpNetworkProviderSettingsUpdateReceiver;->a(Landroid/content/Context;)V

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "gps"

    invoke-static {v0, v4}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "network"

    invoke-static {v0, v4}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_2
    move v0, v1

    :goto_3
    if-eqz v0, :cond_b

    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_4
    invoke-static {}, Lxv;->a()Lxv;

    move-result-object v4

    const/16 v0, 0x12

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v5, "com.google.android.apps.enterprise.dmagent"

    const-string v0, "device_policy"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, v5}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwnerApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ladh;

    invoke-direct {v0}, Ladh;-><init>()V

    sput-object v0, Ladg;->b:Ladm;

    new-instance v0, Ladk;

    invoke-direct {v0, v3}, Ladk;-><init>(Landroid/content/Context;)V

    sput-object v0, Ladg;->a:Ladk;

    sget-object v5, Ladg;->b:Ladm;

    invoke-virtual {v0, v5}, Ladk;->a(Ladm;)V

    sget-object v0, Ladg;->a:Ladk;

    invoke-virtual {v0}, Ladk;->a()V

    sget-object v0, Ladg;->a:Ladk;

    invoke-virtual {v0, v2}, Ladk;->a(Z)V

    invoke-virtual {v4, v1}, Lxv;->b(Z)V

    :cond_3
    invoke-virtual {v4}, Lxv;->d()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Ladi;->a()Ladi;

    move-result-object v0

    invoke-virtual {v0, v4}, Ladi;->a(Lxv;)V

    :cond_4
    invoke-static {v3}, Lcom/google/android/gms/analytics/service/RefreshEnabledStateService;->a(Landroid/content/Context;)V

    invoke-static {v3}, Lcom/google/android/gms/analytics/service/PlayLogMonitorIntervalService;->a(Landroid/content/Context;)V

    const/4 v0, 0x6

    invoke-static {v3, v2, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZI)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v3, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;

    invoke-direct {v0, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.GMS_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/authzen/GcmReceiverService;

    invoke-direct {v0, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.GMS_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/google/android/gms/common/download/DownloadAlarmReceiver;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {v3}, Lfkp;->a(Landroid/content/Context;)V

    invoke-static {}, Lbcm;->d()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.lockbox.LockboxAlarmReceiver"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/init/InitializeIntentService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_6
    return-void

    :cond_7
    :try_start_1
    const-string v0, "fakedb"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x7

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v4, "android.os.FileUtils"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setPermissions"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    const/4 v0, 0x1

    const/16 v7, 0x1c0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x2

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x3

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    const-string v4, "SecurityFix"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FileUtils.setPermissions failed with error code "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v4, "SecurityFix"

    const-string v5, "Failed to restrict readability of the dir to owner."

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcun;->a()V

    throw v0

    :cond_8
    move v0, v2

    goto/16 :goto_1

    :cond_9
    if-eqz v0, :cond_1

    invoke-virtual {v4, v5, v9, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto/16 :goto_2

    :cond_a
    move v0, v2

    goto/16 :goto_3

    :cond_b
    const-string v0, "LocationInitializer"

    const-string v4, "Location services disabled."

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4
.end method
