.class public Lcom/google/android/gms/games/service/GamesIntentService;
.super Lbeo;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/games/service/GamesIntentService;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, Lbeo;-><init>(Ljava/util/HashMap;)V

    return-void
.end method

.method private static a()Ljava/util/HashMap;
    .locals 5

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldry;

    invoke-direct {v1}, Ldry;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method private static a(Landroid/content/Context;ILdrb;)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No operation queue found for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    const-string v0, "com.google.android.gms.games.service.INTENT"

    invoke-static {v0}, Lbox;->f(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "intent_thread_affinity"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lbjv;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZIZI)V
    .locals 14

    const/4 v13, 0x1

    new-instance v0, Ldse;

    move-object v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    invoke-direct/range {v0 .. v12}, Ldse;-><init>(Lbjv;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZIZI)V

    invoke-static {p0, v13, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldte;

    invoke-direct {v1, p1}, Ldte;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;)V
    .locals 2

    const/4 v0, 0x7

    new-instance v1, Ldrz;

    invoke-direct {v1, p1, p2}, Ldrz;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IIZZ)V
    .locals 8

    const/4 v7, -0x1

    new-instance v0, Ldsm;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldsm;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;IIZZ)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V
    .locals 7

    const/4 v6, -0x1

    new-instance v0, Ldtk;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ldtk;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ldfh;II)V
    .locals 7

    const/4 v6, 0x2

    new-instance v0, Ldsy;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ldsy;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ldfh;II)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldtl;

    invoke-direct {v1, p1, p2, p3}, Ldtl;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I)V
    .locals 2

    const/16 v0, 0x8

    new-instance v1, Ldtw;

    invoke-direct {v1, p1, p2, p3, p4}, Ldtw;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;II[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 9

    const/4 v8, 0x6

    new-instance v0, Ldub;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Ldub;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;II[Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZ)V
    .locals 7

    const/4 v6, -0x1

    new-instance v0, Ldti;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ldti;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZ)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V
    .locals 8

    const/4 v7, -0x1

    new-instance v0, Ldtn;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldtn;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZZZ)V
    .locals 10

    const/4 v9, -0x1

    new-instance v0, Ldsq;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Ldsq;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZZZ)V

    invoke-static {p0, v9, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I[I)V
    .locals 7

    const/4 v6, 0x6

    new-instance v0, Ldug;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ldug;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I[I)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x6

    new-instance v1, Lduh;

    invoke-direct {v1, p1, p2, p3, p4}, Lduh;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 8

    const/4 v7, 0x2

    new-instance v0, Ldsw;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldsw;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 11

    const/4 v10, 0x2

    new-instance v0, Ldsz;

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Ldsz;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIIZ)V

    invoke-static {p0, v10, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V
    .locals 9

    const/16 v8, 0x8

    new-instance v0, Ldtx;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Ldtx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;II[B[Ljava/lang/String;)V
    .locals 10

    const/16 v9, 0x8

    new-instance v0, Ldty;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Ldty;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;II[B[Ljava/lang/String;)V

    invoke-static {p0, v9, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 9

    const/4 v8, -0x1

    new-instance v0, Ldtj;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Ldtj;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ldpc;)V
    .locals 7

    const/4 v6, 0x5

    new-instance v0, Ldtt;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ldtt;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ldpc;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V
    .locals 9

    const/4 v8, 0x3

    new-instance v0, Ldsf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Ldsf;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 12

    const/4 v11, 0x2

    new-instance v0, Ldta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Ldta;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    invoke-static {p0, v11, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)V
    .locals 9

    const/4 v8, 0x3

    new-instance v0, Ldsj;

    const/4 v3, 0x2

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Ldsj;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 9

    const/4 v8, 0x6

    new-instance v0, Ldui;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Ldui;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    const/4 v6, 0x2

    new-instance v0, Ldsx;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ldsx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8

    const/4 v7, 0x6

    new-instance v0, Lduf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lduf;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 8

    const/4 v7, 0x6

    new-instance v0, Ldud;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Ldud;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7

    const/16 v6, 0x8

    new-instance v0, Ldtu;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ldtu;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldso;

    invoke-direct {v1, p1, p2, p3, p4}, Ldso;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;ZLandroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x7

    new-instance v1, Ldsd;

    invoke-direct {v1, p1, p2, p3, p4}, Ldsd;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;ZLandroid/os/Bundle;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;[Ljava/lang/String;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldtl;

    invoke-direct {v1, p1, p2, p3}, Ldtl;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;[Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldqy;)V
    .locals 2

    const/4 v0, 0x1

    new-instance v1, Ldsc;

    invoke-direct {v1, p1, p2}, Ldsc;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldqy;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldqy;Ljava/lang/String;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 9

    const/4 v8, 0x5

    new-instance v0, Ldtr;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Ldtr;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldqy;Ljava/lang/String;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldqy;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 7

    const/4 v6, 0x5

    new-instance v0, Ldts;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ldts;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldqy;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldtg;

    invoke-direct {v1, p1, p2}, Ldtg;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldtp;

    invoke-direct {v1, p1, p2, p3}, Ldtp;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x6

    new-instance v1, Lduc;

    invoke-direct {v1, p1, p2, p3}, Lduc;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    const/4 v0, 0x4

    new-instance v1, Ldsu;

    invoke-direct {v1, p1, p2, p3, p4}, Ldsu;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldtb;

    invoke-direct {v1, p1}, Ldtb;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    const/4 v0, -0x1

    new-instance v1, Ldtc;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2, p3}, Ldtc;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldrx;

    invoke-direct {v1}, Ldrx;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;)V
    .locals 2

    const/4 v0, 0x7

    new-instance v1, Ldsk;

    invoke-direct {v1, p1, p2}, Ldsk;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V
    .locals 7

    const/4 v6, -0x1

    new-instance v0, Ldth;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ldth;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x4

    new-instance v0, Ldsv;

    const/4 v3, 0x0

    move-object v1, p1

    move-object v2, p2

    move v5, v4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ldsv;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZLjava/lang/String;)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZ)V
    .locals 8

    const/4 v7, 0x4

    new-instance v0, Ldsv;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Ldsv;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZLjava/lang/String;)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V
    .locals 8

    const/4 v7, -0x1

    new-instance v0, Ldtq;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldtq;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x6

    new-instance v1, Ldtz;

    invoke-direct {v1, p1, p2, p3, p4}, Ldtz;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 11

    const/4 v10, 0x2

    new-instance v0, Ldsz;

    const/4 v8, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Ldsz;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIIZ)V

    invoke-static {p0, v10, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 9

    const/4 v8, -0x1

    new-instance v0, Ldtm;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Ldtm;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IZZ)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V
    .locals 9

    const/4 v8, 0x3

    new-instance v0, Ldsi;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Ldsi;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)V
    .locals 9

    const/4 v8, 0x3

    new-instance v0, Ldsj;

    const/4 v3, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Ldsj;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)V

    invoke-static {p0, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    const/4 v6, 0x3

    new-instance v0, Ldsh;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ldsh;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7

    const/16 v6, 0x8

    new-instance v0, Ldtv;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ldtv;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V
    .locals 2

    const/4 v0, 0x7

    new-instance v1, Ldtf;

    invoke-direct {v1, p1, p2, p3, p4}, Ldtf;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    const/4 v0, 0x4

    new-instance v1, Ldst;

    invoke-direct {v1, p1, p2, p3, p4}, Ldst;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    const/4 v0, -0x1

    new-instance v1, Ldsa;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Ldsa;-><init>(Ljava/lang/String;Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldsb;

    invoke-direct {v1}, Ldsb;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V
    .locals 7

    const/4 v6, -0x1

    new-instance v0, Ldto;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ldto;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;IZZ)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x7

    new-instance v1, Ldtd;

    invoke-direct {v1, p1, p2, p3}, Ldtd;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V
    .locals 8

    const/4 v7, -0x1

    new-instance v0, Ldsr;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldsr;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x6

    new-instance v1, Ldua;

    invoke-direct {v1, p1, p2, p3, p4}, Ldua;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    const/4 v6, 0x3

    new-instance v0, Ldsg;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ldsg;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    const/4 v0, -0x1

    new-instance v1, Ldsa;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Ldsa;-><init>(Ljava/lang/String;Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldsn;

    invoke-direct {v1, p1, p2, p3}, Ldsn;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V
    .locals 8

    const/4 v7, -0x1

    new-instance v0, Ldss;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Ldss;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZ)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x6

    new-instance v1, Ldue;

    invoke-direct {v1, p1, p2, p3, p4}, Ldue;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V
    .locals 2

    const/4 v0, -0x1

    new-instance v1, Ldsp;

    invoke-direct {v1, p1, p2, p3}, Ldsp;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method

.method public static f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x7

    new-instance v1, Ldsl;

    invoke-direct {v1, p1, p2, p3}, Ldsl;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILdrb;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 4

    const-string v0, "intent_thread_affinity"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No op queue for affinity "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldrb;

    if-nez v0, :cond_0

    const-string v0, "GamesIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcun;->a(Landroid/content/Context;)Lcun;

    move-result-object v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    invoke-interface {v0, p0, v1}, Ldrb;->a(Landroid/content/Context;Lcun;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcun;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcun;->a()V

    throw v0
.end method
