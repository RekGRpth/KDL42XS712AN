.class public final Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 3

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbgz;

    invoke-interface {v0}, Lbgz;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;

    iget-object v1, p0, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->b:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p4, p2, v0, v1, v2}, Lbis;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to write client safe class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Ldab;->a(Landroid/content/Intent;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a:I

    invoke-static {p0}, Lbox;->b(Landroid/app/Activity;)Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->b:Landroid/content/Context;

    const-string v2, "ParcelTestCompat must be started for result!"

    invoke-static {v1, v2}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbbv;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->b:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbis;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/Integer;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "No extras found!"

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "invitation"

    const-class v3, Lcom/google/android/gms/games/multiplayer/InvitationEntity;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    const-string v2, "room"

    const-class v3, Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    const-string v2, "com.google.android.gms.games.GAME"

    const-class v3, Lcom/google/android/gms/games/GameEntity;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    const-string v2, "com.google.android.gms.games.EXTENDED_GAME"

    const-class v3, Lcom/google/android/gms/games/internal/game/ExtendedGameEntity;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    const-string v2, "com.google.android.gms.games.PLAYER"

    const-class v3, Lcom/google/android/gms/games/PlayerEntity;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    const-string v2, "com.google.android.gms.games.GAME_BADGE"

    const-class v3, Lcom/google/android/gms/games/internal/game/GameBadgeEntity;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    const-string v2, "com.google.android.gms.games.PARTICIPANT"

    const-class v3, Lcom/google/android/gms/games/multiplayer/ParticipantEntity;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/testcompat/ParcelTestCompatActivity;->finish()V

    return-void
.end method
