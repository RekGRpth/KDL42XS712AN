.class public Lcom/google/android/gms/games/provider/GamesContentProvider;
.super Lbls;
.source "SourceFile"


# static fields
.field private static final A:Lqz;

.field private static final B:Ljava/lang/String;

.field private static final C:Ljava/lang/String;

.field private static final D:Ljava/lang/String;

.field private static final E:Lqz;

.field private static final F:Ljava/lang/String;

.field private static final G:Lqz;

.field private static final H:Ljava/lang/String;

.field private static final I:Ljava/lang/String;

.field private static final J:Ljava/lang/String;

.field private static final K:Ljava/lang/String;

.field private static final L:Ljava/lang/String;

.field private static final M:Ldhu;

.field private static final a:Landroid/content/UriMatcher;

.field private static final b:[Ljava/lang/String;

.field private static final h:Lqz;

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;

.field private static final m:Ljava/lang/String;

.field private static final n:Lqz;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;

.field private static final q:Lqz;

.field private static final r:Lqz;

.field private static final s:Ljava/lang/String;

.field private static final t:Ljava/lang/String;

.field private static final u:Ljava/lang/String;

.field private static final v:Ljava/lang/String;

.field private static final w:Ljava/lang/String;

.field private static final x:Ljava/lang/String;

.field private static final y:Lqz;

.field private static final z:Ljava/lang/String;


# instance fields
.field private N:Ldio;

.field private O:Ldio;

.field private P:Ldio;

.field private Q:Ldio;

.field private R:Ldio;

.field private S:Ldio;

.field private T:Ldio;

.field private U:Ldio;

.field private V:Ldio;

.field private W:Ldio;

.field private X:Ldio;

.field private Y:Ldio;

.field private Z:Ldio;

.field private aa:Ldio;

.field private ab:Ldio;

.field private c:Landroid/os/HandlerThread;

.field private d:Landroid/os/Handler;

.field private e:J

.field private f:Lbpe;

.field private final g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->a:Landroid/content/UriMatcher;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:[Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_count"

    const-string v2, "COUNT(*)"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->h:Lqz;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "game_badges AS Badges"

    const-string v2, "games._id"

    const-string v3, "Badges._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->i:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "game_badges"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "badge_game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->j:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "game_instances"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "instance_game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->k:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "players"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "most_recent_game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->l:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "achievement_definitions"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->m:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "achievement_pending_ops._id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldiv;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldix;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->n:Lqz;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "achievement_pending_ops"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->o:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "achievement_instances"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "achievement_definitions"

    const-string v2, "definition_id"

    const-string v3, "achievement_definitions._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->p:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "client_contexts._id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldix;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->q:Lqz;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "images._id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldjd;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->r:Lqz;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "images"

    const-string v2, "game_icon_image_id"

    const-string v3, "images._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->s:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "images"

    const-string v2, "game_hi_res_image_id"

    const-string v3, "images._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->t:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "images"

    const-string v2, "featured_image_id"

    const-string v3, "images._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->u:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "leaderboards"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->v:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "leaderboard_instances"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "leaderboards"

    const-string v2, "leaderboard_id"

    const-string v3, "leaderboards._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->w:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "leaderboard_scores"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "leaderboard_instances"

    const-string v2, "instance_id"

    const-string v3, "leaderboard_instances._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "leaderboards"

    const-string v2, "leaderboard_id"

    const-string v3, "leaderboards._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->x:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "leaderboard_pending_scores._id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldjg;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldix;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->y:Lqz;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "leaderboard_pending_scores"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->z:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "requests._id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldjr;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->A:Lqz;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "requests"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->B:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "request_recipients"

    const-string v2, "request_id"

    const-string v3, "requests._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "players AS RequestRecipientPlayer"

    const-string v2, "player_id"

    const-string v3, "RequestRecipientPlayer._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "players AS RequestSenderPlayer"

    const-string v2, "sender_id"

    const-string v3, "RequestSenderPlayer._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->C:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "request_recipients"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->D:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "matches._id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldjk;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->E:Lqz;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "matches"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->F:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "matches_pending_ops._id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldjl;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldix;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->G:Lqz;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "matches_pending_ops"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->H:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "participants"

    const-string v2, "match_id"

    const-string v3, "matches._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "players AS ParticipantPlayer"

    const-string v2, "player_id"

    const-string v3, "ParticipantPlayer._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->I:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "invitations"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "participants"

    const-string v2, "invitation_id"

    const-string v3, "invitations._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "players AS ParticipantPlayer"

    const-string v2, "player_id"

    const-string v3, "ParticipantPlayer._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->J:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "notifications"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->K:Ljava/lang/String;

    new-instance v0, Lblw;

    invoke-direct {v0}, Lblw;-><init>()V

    const-string v1, "participants"

    invoke-virtual {v0, v1}, Lblw;->a(Ljava/lang/String;)Lblw;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lblw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lblw;

    move-result-object v0

    invoke-virtual {v0}, Lblw;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->L:Ljava/lang/String;

    invoke-static {}, Ldhu;->a()Ldhv;

    move-result-object v0

    const-string v1, "account_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "external_player_id"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "match_sync_token"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "last_package_scan_timestamp"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "request_sync_token"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    invoke-virtual {v0}, Ldhv;->a()Ldhu;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->M:Ldhu;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lbls;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->e:J

    new-instance v0, Ldig;

    invoke-direct {v0, p0}, Ldig;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->N:Ldio;

    new-instance v0, Ldih;

    invoke-direct {v0, p0}, Ldih;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->O:Ldio;

    new-instance v0, Ldii;

    invoke-direct {v0, p0}, Ldii;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->P:Ldio;

    new-instance v0, Ldij;

    invoke-direct {v0, p0}, Ldij;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Q:Ldio;

    new-instance v0, Ldik;

    invoke-direct {v0, p0}, Ldik;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->R:Ldio;

    new-instance v0, Ldil;

    invoke-direct {v0, p0}, Ldil;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->S:Ldio;

    new-instance v0, Ldim;

    invoke-direct {v0, p0}, Ldim;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->T:Ldio;

    new-instance v0, Ldin;

    invoke-direct {v0, p0}, Ldin;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->U:Ldio;

    new-instance v0, Ldhy;

    invoke-direct {v0, p0}, Ldhy;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->V:Ldio;

    new-instance v0, Ldhz;

    invoke-direct {v0, p0}, Ldhz;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->W:Ldio;

    new-instance v0, Ldia;

    invoke-direct {v0, p0}, Ldia;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->X:Ldio;

    new-instance v0, Ldib;

    invoke-direct {v0, p0}, Ldib;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Y:Ldio;

    new-instance v0, Ldic;

    invoke-direct {v0, p0}, Ldic;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Z:Ldio;

    new-instance v0, Ldid;

    invoke-direct {v0, p0}, Ldid;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aa:Ldio;

    new-instance v0, Ldie;

    invoke-direct {v0, p0}, Ldie;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ab:Ldio;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Lbpe;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/HashMap;

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "%s IN (SELECT %s FROM %s WHERE %s=?)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p3, v1, v3

    const-string v2, "_id"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    aput-object p5, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    aput-object p1, v1, v3

    invoke-virtual {p0, p2, p6, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I
    .locals 8

    const/4 v6, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/net/Uri;)Ldjs;

    move-result-object v7

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p4, v2, v6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move v0, v6

    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-interface {p5, v7, v2, v3}, Ldio;->a(Ldjs;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/gms/games/provider/GamesContentProvider;Ldjs;J)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Ldjs;J)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Ldjs;J)I
    .locals 4

    iget-object v0, p0, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "achievement_pending_ops"

    const-string v3, "client_context_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "leaderboard_pending_scores"

    const-string v3, "client_context_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "matches_pending_ops"

    const-string v3, "client_context_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "client_contexts"

    const-string v3, "_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(Ldju;JLandroid/content/ContentValues;)I
    .locals 7

    const/4 v6, 0x0

    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No image ID specified for image data update"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "image_data"

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "image_data"

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Lbpe;

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v4

    move-object v0, p1

    move-wide v1, p2

    invoke-virtual/range {v0 .. v5}, Ldju;->a(J[BJ)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const-string v0, "GamesContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to update image data for image ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)J
    .locals 4

    const-wide/16 v0, -0x1

    sget-object v2, Ldif;->b:[I

    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Ldiq;

    move-result-object v3

    invoke-virtual {v3}, Ldiq;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    :goto_0
    return-wide v0

    :sswitch_0
    const-string v0, "games"

    const-string v1, "game_icon_image_id"

    invoke-static {p2}, Leej;->c(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :sswitch_1
    const-string v0, "external_game_id=?"

    const-string v1, "games"

    const-string v2, "game_icon_image_id"

    invoke-static {p2}, Leej;->d(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v1, v2, v3, v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :sswitch_2
    const-string v0, "games"

    const-string v1, "game_hi_res_image_id"

    invoke-static {p2}, Leej;->c(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :sswitch_3
    const-string v0, "games"

    const-string v1, "featured_image_id"

    invoke-static {p2}, Leej;->c(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :sswitch_4
    const-string v0, "game_badges"

    const-string v1, "badge_icon_image_id"

    invoke-static {p2}, Leej;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :sswitch_5
    const-string v0, "players"

    const-string v1, "profile_icon_image_id"

    invoke-static {p2}, Leej;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :sswitch_6
    const-string v0, "players"

    const-string v1, "profile_hi_res_image_id"

    invoke-static {p2}, Leej;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :sswitch_7
    const-string v0, "achievement_definitions"

    const-string v1, "unlocked_icon_image_id"

    invoke-static {p2}, Leej;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :sswitch_8
    const-string v0, "achievement_definitions"

    const-string v1, "revealed_icon_image_id"

    invoke-static {p2}, Leej;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_0

    :sswitch_9
    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x30 -> :sswitch_9
        0x56 -> :sswitch_4
        0x57 -> :sswitch_5
        0x58 -> :sswitch_6
        0x59 -> :sswitch_7
        0x5a -> :sswitch_8
    .end sparse-switch
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J
    .locals 10

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v8, -0x1

    const-string v1, "images"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v3, "url=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-wide v0, v8

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 2

    const-string v0, "_id=?"

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8

    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    new-array v2, v1, [Ljava/lang/String;

    aput-object p2, v2, v0

    new-array v4, v1, [Ljava/lang/String;

    aput-object p3, v4, v0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No image found for non-existent "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " row with ID "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Ldju;J)Landroid/content/res/AssetFileDescriptor;
    .locals 6

    invoke-virtual {p0, p1, p2}, Ldju;->a(J)Ldjv;

    move-result-object v4

    if-eqz v4, :cond_0

    :try_start_0
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    new-instance v1, Ljava/io/File;

    iget-object v2, v4, Ldjv;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-static {v1, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    const-wide/16 v2, 0x0

    iget-wide v4, v4, Ldjv;->b:J

    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :cond_0
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No image found for ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "(SELECT url FROM images innerimage WHERE innerimage._id=%s.%s LIMIT 1)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Object;J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->d:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->o:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->n:Lqz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->i:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "games._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjb;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldiy;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "badge_icon_image_uri"

    const-string v3, "badge_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "availability"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "owned"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "achievement_unlocked_count"

    const-string v3, "(SELECT COUNT(1) FROM achievement_instances, achievement_definitions, games AS SubGame WHERE SubGame._id=games._id AND games._id=achievement_definitions.game_id AND achievement_definitions._id=achievement_instances.definition_id AND 0=achievement_instances.state)"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "price_micros"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "formatted_price"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "full_price_micros"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "formatted_full_price"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    return-void
.end method

.method private a(Ldjs;Landroid/content/ContentValues;)V
    .locals 2

    const-string v0, "profile_icon_image_url"

    const-string v1, "profile_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "profile_hi_res_image_url"

    const-string v1, "profile_hi_res_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget-object v0, p1, Ldjs;->b:Ldju;

    iget-object v1, p1, Ldjs;->a:Ldjt;

    invoke-virtual {v1}, Ldjt;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p2, p4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v1, v3, v5

    if-eqz v1, :cond_2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Lbpe;

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Ldju;->a([BLjava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    const-string v1, "Unable to store image."

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2, p4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p1, Ldjs;->b:Ldju;

    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v1

    array-length v2, v1

    if-nez v2, :cond_2

    invoke-virtual {p2, p5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Lbpe;

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Ldju;->a([BLjava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, p5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    :cond_3
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    const-string v1, "Unable to store image."

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjs;

    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldjs;->b(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, v0, Ldjs;->e:I

    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    const-string v0, "GamesContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Retrying data store creation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-wide/16 v1, 0xc8

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(ILjava/lang/Object;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to initialize data store "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private static varargs a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z
    .locals 10

    const/4 v9, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    const-string v3, "_id=?"

    new-array v4, v9, [Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    move-object v0, p0

    move-object v1, p2

    move-object v2, p5

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GamesContentProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attempting to delete a row from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " that doesn\'t exist.  ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v8

    :goto_0
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    array-length v0, p5

    if-ge v8, v0, :cond_2

    invoke-interface {v1, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Ldju;->b(J)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v9

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static synthetic b(Lcom/google/android/gms/games/provider/GamesContentProvider;Ldjs;J)I
    .locals 1

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Ldjs;J)I

    move-result v0

    return v0
.end method

.method public static synthetic b(Ldjs;J)I
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Ldjs;J)I

    move-result v0

    return v0
.end method

.method public static synthetic b()Landroid/content/UriMatcher;
    .locals 1

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->a:Landroid/content/UriMatcher;

    return-object v0
.end method

.method private static b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ldjd;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/\'||"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    const-string v0, "client_contexts"

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->q:Lqz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->j:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "game_badges._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldiy;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjb;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "badge_icon_image_uri"

    const-string v3, "badge_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private b(Ldjs;Landroid/content/ContentValues;)V
    .locals 6

    const-string v3, "icon_image_bytes"

    const-string v4, "game_icon_image_url"

    const-string v5, "game_icon_image_id"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "game_hi_res_image_url"

    const-string v1, "game_hi_res_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "featured_image_url"

    const-string v1, "featured_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/gms/games/provider/GamesContentProvider;Ldjs;J)I
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p1, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p1, Ldjs;->b:Ldju;

    const-string v2, "players"

    new-array v5, v7, [Ljava/lang/String;

    const-string v3, "profile_icon_image_id"

    aput-object v3, v5, v6

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    const-string v2, "players"

    new-array v5, v7, [Ljava/lang/String;

    const-string v3, "profile_hi_res_image_id"

    aput-object v3, v5, v6

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    move v0, v6

    goto :goto_0

    :cond_1
    new-array v1, v7, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "achievement_instances"

    const-string v3, "player_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "leaderboard_scores"

    const-string v3, "player_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "players"

    const-string v3, "_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static synthetic c(Ldjs;J)I
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Ldjs;J)I

    move-result v0

    return v0
.end method

.method private static c(Landroid/net/Uri;)Ldiq;
    .locals 4

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized URI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    const-class v0, Ldiq;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldiq;

    aget-object v0, v0, v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized c()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/HashMap;

    if-nez v0, :cond_1

    const-string v0, "GamesContentProvider"

    const-string v1, "No data stores when running cleanAllImageStores() background task"

    invoke-static {v0, v1}, Ldac;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldjs;

    if-nez v1, :cond_3

    const-string v1, "GamesContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Null data store found for store name "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldjs;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ldip;

    invoke-direct {v0, p0, v1}, Ldip;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ldjs;)V

    const-string v1, "games"

    const-string v3, "game_icon_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "games"

    const-string v3, "game_hi_res_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "games"

    const-string v3, "featured_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "game_badges"

    const-string v3, "badge_icon_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "players"

    const-string v3, "profile_icon_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "players"

    const-string v3, "profile_hi_res_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "achievement_definitions"

    const-string v3, "unlocked_icon_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "achievement_definitions"

    const-string v3, "revealed_icon_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "leaderboards"

    const-string v3, "board_icon_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "leaderboard_scores"

    const-string v3, "default_display_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "notifications"

    const-string v3, "image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "participants"

    const-string v3, "default_display_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "participants"

    const-string v3, "default_display_hi_res_image_id"

    invoke-virtual {v0, v1, v3}, Ldip;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ldip;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private static c(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    const-string v0, "images"

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->r:Lqz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->k:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "game_instances._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldiz;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjb;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private c(Ldjs;Landroid/content/ContentValues;)V
    .locals 6

    const-string v3, "badge_icon_image_bytes"

    const-string v4, "badge_icon_image_url"

    const-string v5, "badge_icon_image_id"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/gms/games/provider/GamesContentProvider;Ldjs;J)I
    .locals 1

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Ldjs;J)I

    move-result v0

    return v0
.end method

.method private d(Ldjs;J)I
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    iget-object v0, p1, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p1, Ldjs;->b:Ldju;

    const-string v2, "games"

    new-array v5, v10, [Ljava/lang/String;

    const-string v3, "game_icon_image_id"

    aput-object v3, v5, v8

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v8

    :goto_0
    return v0

    :cond_0
    const-string v2, "games"

    new-array v5, v10, [Ljava/lang/String;

    const-string v3, "game_hi_res_image_id"

    aput-object v3, v5, v8

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v8

    goto :goto_0

    :cond_1
    const-string v2, "games"

    new-array v5, v10, [Ljava/lang/String;

    const-string v3, "featured_image_id"

    aput-object v3, v5, v8

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v8

    goto :goto_0

    :cond_2
    new-array v4, v10, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    const-string v1, "game_badges"

    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:[Ljava/lang/String;

    const-string v3, "badge_game_id=?"

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Ldjs;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "game_instances"

    const-string v2, "instance_game_id=?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v1, "achievement_definitions"

    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:[Ljava/lang/String;

    const-string v3, "game_id=?"

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Ldjs;J)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "leaderboards"

    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:[Ljava/lang/String;

    const-string v3, "game_id=?"

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_3
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Ldjs;J)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "matches"

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v8

    const-string v3, "game_id=?"

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_4
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Ldjs;J)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_4

    :catchall_3
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "games"

    const-string v2, "_id=?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0
.end method

.method private static d(Landroid/net/Uri;)Lqz;
    .locals 4

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "players._id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    sget-object v1, Ldjo;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v0

    const-string v1, "profile_icon_image_uri"

    const-string v2, "profile_icon_image_id"

    invoke-static {p0, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    const-string v1, "profile_icon_image_url"

    const-string v2, "players"

    const-string v3, "profile_icon_image_id"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    const-string v1, "profile_hi_res_image_uri"

    const-string v2, "profile_hi_res_image_id"

    invoke-static {p0, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    const-string v1, "profile_hi_res_image_url"

    const-string v2, "players"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    const-string v1, "most_recent_external_game_id"

    const-string v2, "external_game_id"

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    const-string v1, "most_recent_activity_timestamp"

    invoke-virtual {v0, v1}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v0

    const-string v1, "game_icon_image_uri"

    const-string v2, "game_icon_image_id"

    invoke-static {p0, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    const-string v1, "game_icon_image_url"

    const-string v2, "games"

    const-string v3, "game_icon_image_id"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v0

    invoke-virtual {v0}, Lra;->a()Lqz;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->s:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->r:Lqz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static d(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    const-string v0, "players"

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjo;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private d(Ldjs;Landroid/content/ContentValues;)V
    .locals 2

    const-string v0, "unlocked_icon_image_url"

    const-string v1, "unlocked_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "revealed_icon_image_url"

    const-string v1, "revealed_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/gms/games/provider/GamesContentProvider;Ldjs;J)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Ldjs;J)I

    move-result v0

    return v0
.end method

.method private static e(Ldjs;J)I
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Ldjs;->b:Ldju;

    const-string v2, "game_badges"

    new-array v5, v7, [Ljava/lang/String;

    const-string v3, "badge_icon_image_id"

    aput-object v3, v5, v6

    move-wide v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    new-array v1, v7, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "game_badges"

    const-string v3, "_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private declared-synchronized e(Landroid/net/Uri;)Ldjs;
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Leej;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjs;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v0, Ldjs;

    invoke-direct {v0, v2, v1}, Ldjs;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    invoke-direct {p0, v2, v1, v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(ILjava/lang/Object;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static e(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->B:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->A:Lqz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->m:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "achievement_definitions._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldit;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "unlocked_icon_image_uri"

    const-string v3, "unlocked_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "unlocked_icon_image_url"

    const-string v3, "achievement_definitions"

    const-string v4, "unlocked_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "revealed_icon_image_uri"

    const-string v3, "revealed_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "revealed_icon_image_url"

    const-string v3, "achievement_definitions"

    const-string v4, "revealed_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjb;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private e(Ldjs;Landroid/content/ContentValues;)V
    .locals 2

    const-string v0, "board_icon_image_url"

    const-string v1, "board_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/gms/games/provider/GamesContentProvider;Ldjs;J)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Ldjs;J)I

    move-result v0

    return v0
.end method

.method private static f(Ldjs;J)I
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Ldjs;->b:Ldju;

    const-string v2, "achievement_definitions"

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const-string v3, "unlocked_icon_image_id"

    aput-object v3, v5, v6

    const-string v3, "revealed_icon_image_id"

    aput-object v3, v5, v7

    move-wide v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    new-array v1, v7, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "achievement_instances"

    const-string v3, "definition_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "achievement_definitions"

    const-string v3, "_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static f(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->F:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->E:Lqz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->p:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "achievement_instances._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldiu;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldit;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "unlocked_icon_image_uri"

    const-string v3, "unlocked_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "unlocked_icon_image_url"

    const-string v3, "achievement_definitions"

    const-string v4, "unlocked_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "revealed_icon_image_uri"

    const-string v3, "revealed_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "revealed_icon_image_url"

    const-string v3, "achievement_definitions"

    const-string v4, "revealed_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjo;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private f(Ldjs;Landroid/content/ContentValues;)V
    .locals 2

    const-string v0, "default_display_image_url"

    const-string v1, "default_display_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/gms/games/provider/GamesContentProvider;Ldjs;J)I
    .locals 1

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Ldjs;J)I

    move-result v0

    return v0
.end method

.method private g(Ldjs;J)I
    .locals 9

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    iget-object v0, p1, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p1, Ldjs;->b:Ldju;

    const-string v2, "leaderboards"

    new-array v5, v7, [Ljava/lang/String;

    const-string v3, "board_icon_image_id"

    aput-object v3, v5, v6

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    new-array v4, v7, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v6

    const-string v1, "leaderboard_instances"

    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:[Ljava/lang/String;

    const-string v3, "leaderboard_id=?"

    move-object v5, v8

    move-object v6, v8

    move-object v7, v8

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Ldjs;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "leaderboards"

    const-string v2, "_id=?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static g(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->H:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->G:Lqz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->J:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "invitations._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldje;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "most_recent_invitation"

    const-string v3, "(SELECT MAX(last_modified_timestamp) FROM invitations innerinvite WHERE innerinvite.external_inviter_id=invitations.external_inviter_id)"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjn;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjb;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "external_player_id"

    const-string v3, "ParticipantPlayer.external_player_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_name"

    const-string v3, "ParticipantPlayer.profile_name"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_id"

    const-string v3, "ParticipantPlayer.profile_icon_image_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "ParticipantPlayer"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_id"

    const-string v3, "ParticipantPlayer.profile_hi_res_image_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "ParticipantPlayer"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "last_updated"

    const-string v3, "ParticipantPlayer.last_updated"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "is_in_circles"

    const-string v3, "ParticipantPlayer.is_in_circles"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_image_uri"

    const-string v3, "default_display_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_uri"

    const-string v3, "default_display_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private g(Ldjs;Landroid/content/ContentValues;)V
    .locals 2

    const-string v0, "default_display_image_url"

    const-string v1, "default_display_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "default_display_hi_res_image_url"

    const-string v1, "default_display_hi_res_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private h(Ldjs;J)I
    .locals 8

    const/4 v2, 0x0

    const/4 v5, 0x0

    iget-object v0, p1, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    const-string v1, "leaderboard_scores"

    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:[Ljava/lang/String;

    const-string v3, "instance_id=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Ldjs;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "leaderboard_instances"

    const-string v2, "_id=?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->v:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "leaderboards._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldji;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "board_icon_image_uri"

    const-string v3, "board_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "board_icon_image_url"

    const-string v3, "leaderboards"

    const-string v4, "board_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static i(Ldjs;J)I
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Ldjs;->b:Ldju;

    const-string v2, "leaderboard_scores"

    new-array v5, v7, [Ljava/lang/String;

    const-string v3, "default_display_image_id"

    aput-object v3, v5, v6

    move-wide v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ldju;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    new-array v1, v7, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "leaderboard_scores"

    const-string v3, "_id=?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->w:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "leaderboard_instances._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjf;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldji;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "board_icon_image_uri"

    const-string v3, "board_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "board_icon_image_url"

    const-string v3, "leaderboards"

    const-string v4, "board_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjb;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static j(Ldjs;J)I
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-array v4, v2, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v6

    const-string v1, "matches"

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "external_match_id"

    aput-object v3, v2, v6

    const-string v3, "_id=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "notifications"

    const-string v3, "external_sub_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "participants"

    const-string v2, "match_id=?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v1, "matches"

    const-string v2, "_id=?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static j(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->x:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "leaderboard_scores._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjh;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_image_uri"

    const-string v3, "default_display_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_image_url"

    const-string v3, "leaderboard_scores"

    const-string v4, "default_display_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjo;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static k(Ldjs;J)I
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-array v4, v2, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v6

    const-string v1, "invitations"

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "external_invitation_id"

    aput-object v3, v2, v6

    const-string v3, "_id=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "notifications"

    const-string v3, "external_sub_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "participants"

    const-string v2, "invitation_id=?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v1, "invitations"

    const-string v2, "_id=?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->D:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "request_recipients._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjo;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->C:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    sget-object v2, Ldjq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjr;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "next_expiring_request"

    const-string v3, "(SELECT MIN(expiration_timestamp) FROM requests innerrequests WHERE innerrequests.sender_id=requests.sender_id)"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjb;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_external_player_id"

    const-string v3, "RequestSenderPlayer.external_player_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_profile_name"

    const-string v3, "RequestSenderPlayer.profile_name"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_profile_icon_image_id"

    const-string v3, "RequestSenderPlayer.profile_icon_image_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_profile_icon_image_url"

    const-string v3, "RequestSenderPlayer"

    const-string v4, "RequestSenderPlayer.profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_profile_hi_res_image_id"

    const-string v3, "RequestSenderPlayer.profile_hi_res_image_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_profile_hi_res_image_url"

    const-string v3, "RequestSenderPlayer"

    const-string v4, "RequestSenderPlayer.profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_last_updated"

    const-string v3, "RequestSenderPlayer.last_updated"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_is_in_circles"

    const-string v3, "RequestSenderPlayer.is_in_circles"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_profile_icon_image_uri"

    const-string v3, "RequestSenderPlayer.profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "sender_profile_hi_res_image_uri"

    const-string v3, "RequestSenderPlayer.profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_external_player_id"

    const-string v3, "RequestRecipientPlayer.external_player_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_profile_name"

    const-string v3, "RequestRecipientPlayer.profile_name"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_profile_icon_image_id"

    const-string v3, "RequestRecipientPlayer.profile_icon_image_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_profile_icon_image_url"

    const-string v3, "RequestRecipientPlayer"

    const-string v4, "RequestRecipientPlayer.profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_profile_hi_res_image_url"

    const-string v3, "RequestRecipientPlayer"

    const-string v4, "RequestRecipientPlayer.profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_profile_hi_res_image_id"

    const-string v3, "RequestRecipientPlayer.profile_hi_res_image_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_last_updated"

    const-string v3, "RequestRecipientPlayer.last_updated"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_is_in_circles"

    const-string v3, "RequestRecipientPlayer.is_in_circles"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_profile_icon_image_uri"

    const-string v3, "RequestRecipientPlayer.profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "recipient_profile_hi_res_image_uri"

    const-string v3, "RequestRecipientPlayer.profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static m(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->I:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    sget-object v2, Ldjn;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjk;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjb;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "external_player_id"

    const-string v3, "ParticipantPlayer.external_player_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_name"

    const-string v3, "ParticipantPlayer.profile_name"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_id"

    const-string v3, "ParticipantPlayer.profile_icon_image_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "ParticipantPlayer"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_id"

    const-string v3, "ParticipantPlayer.profile_hi_res_image_id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "ParticipantPlayer"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "last_updated"

    const-string v3, "ParticipantPlayer.last_updated"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "is_in_circles"

    const-string v3, "ParticipantPlayer.is_in_circles"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_image_uri"

    const-string v3, "default_display_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_uri"

    const-string v3, "default_display_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 4

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->K:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "notifications._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjm;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "image_uri"

    const-string v3, "image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "external_game_id"

    invoke-virtual {v1, v2}, Lra;->a(Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method

.method private static o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->L:Ljava/lang/String;

    invoke-static {}, Lqz;->a()Lra;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "participants._id"

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjn;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    sget-object v2, Ldjo;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lra;->a([Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_image_uri"

    const-string v3, "default_display_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_uri"

    const-string v3, "default_display_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lra;->a(Ljava/lang/String;Ljava/lang/String;)Lra;

    move-result-object v1

    invoke-virtual {v1}, Lra;->a()Lqz;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7

    invoke-direct {p0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/net/Uri;)Ldjs;

    move-result-object v0

    iget-object v1, v0, Ldjs;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v1}, Ldjs;->a(Ljava/util/concurrent/CountDownLatch;)V

    iget-object v1, v0, Ldjs;->b:Ldju;

    sget-object v2, Ldif;->b:[I

    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Ldiq;

    move-result-object v3

    invoke-virtual {v3}, Ldiq;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid update URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const-string v1, "external_game_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "games"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :pswitch_2
    const-string v1, "external_game_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "games"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    :pswitch_3
    const-string v1, "external_game_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "games"

    const-string v1, "external_game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "game_badges"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "game_badges"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    :pswitch_6
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "game_badges"

    const-string v1, "badge_game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    :pswitch_7
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Ldjs;Landroid/content/ContentValues;)V

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const-string v2, "game_badges"

    const-string v3, "badge_game_id"

    const-string v4, "games"

    const-string v5, "external_game_id"

    move-object v0, p1

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    :pswitch_8
    const-string v0, "game_instances"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_9
    const-string v0, "game_instances"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    :pswitch_a
    const-string v0, "game_instances"

    const-string v1, "instance_game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    :pswitch_b
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const-string v2, "game_instances"

    const-string v3, "instance_game_id"

    const-string v4, "games"

    const-string v5, "external_game_id"

    move-object v0, p1

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_c
    const-string v1, "external_player_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v1, "profile_name"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "players"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_d
    const-string v1, "external_player_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v1, "profile_name"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "players"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_e
    const-string v1, "external_player_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v1, "profile_name"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "players"

    const-string v1, "external_player_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_f
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldjs;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {p3}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/games/provider/GamesContentProvider;->M:Ldhu;

    invoke-virtual {v4, v1}, Ldhu;->a(Ljava/lang/String;)Ldhw;

    move-result-object v4

    sget-object v5, Ldif;->a:[I

    invoke-virtual {v4}, Ldhw;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported metadata column type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_10
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :pswitch_11
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :cond_0
    invoke-static {v2}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :pswitch_12
    const-string v1, "external_achievement_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "achievement_definitions"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_13
    const-string v1, "external_achievement_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "achievement_definitions"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_14
    const-string v1, "external_achievement_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "achievement_definitions"

    const-string v1, "external_achievement_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_15
    const-string v0, "achievement_pending_ops"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_16
    const-string v0, "achievement_pending_ops"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_17
    const-string v0, "achievement_instances"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_18
    const-string v0, "achievement_instances"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_19
    const-string v0, "package_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "package_uid"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "client_contexts"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1a
    const-string v0, "package_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "package_uid"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "client_contexts"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1b
    const-string v1, "external_leaderboard_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "leaderboards"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1c
    const-string v1, "external_leaderboard_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "leaderboards"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1d
    const-string v1, "external_leaderboard_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "leaderboards"

    const-string v1, "external_leaderboard_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1e
    const-string v1, "external_leaderboard_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "leaderboards"

    const-string v1, "game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1f
    const-string v0, "leaderboard_instances"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_20
    const-string v0, "leaderboard_instances"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_21
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "leaderboard_scores"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_22
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "leaderboard_scores"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_23
    const-string v0, "leaderboard_pending_scores"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_24
    const-string v0, "_id"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldju;JLandroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_25
    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldju;JLandroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_26
    const-string v0, "external_invitation_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "invitations"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_27
    const-string v0, "external_invitation_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "invitations"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_28
    const-string v0, "external_invitation_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "invitations"

    const-string v1, "external_invitation_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_29
    const-string v0, "external_request_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "requests"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_2a
    const-string v0, "external_request_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "requests"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_2b
    const-string v0, "external_request_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "requests"

    const-string v1, "external_request_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_2c
    const-string v0, "player_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "request_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "request_recipients"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_2d
    const-string v0, "player_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "request_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "request_recipients"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_2e
    const-string v0, "request_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "player_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "request_recipients"

    const-string v1, "request_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_2f
    const-string v0, "external_match_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "matches"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_30
    const-string v0, "external_match_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "matches"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_31
    const-string v0, "external_match_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "matches"

    const-string v1, "external_match_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_32
    const-string v0, "external_match_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "matches"

    const-string v1, "game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_33
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcwj;->a(I)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    :cond_1
    const-string v0, "matches_pending_ops"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_34
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcwj;->a(I)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    :cond_2
    const-string v0, "matches_pending_ops"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_35
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcwj;->a(I)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    :cond_3
    const-string v0, "matches_pending_ops"

    const-string v1, "external_match_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_36
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "participants"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_37
    const-string v1, "external_participant_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "participants"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_38
    const-string v1, "external_participant_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "participants"

    const-string v1, "external_participant_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_39
    const-string v1, "external_participant_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "participants"

    const-string v1, "match_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_3a
    const-string v1, "external_participant_id"

    invoke-static {v1, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "participants"

    const-string v1, "invitation_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_3b
    const-string v0, "game_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "external_sub_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "type"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "notifications"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_3c
    const-string v0, "game_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "external_sub_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "type"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "notifications"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_3d
    const-string v0, "game_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "external_sub_id"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "type"

    invoke-static {v0, p3}, Lbkm;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v0, "notifications"

    const-string v1, "game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_0
        :pswitch_21
        :pswitch_22
        :pswitch_0
        :pswitch_0
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_0
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_0
        :pswitch_2f
        :pswitch_30
        :pswitch_0
        :pswitch_31
        :pswitch_32
        :pswitch_0
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_0
        :pswitch_23
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/net/Uri;)Ldjs;

    move-result-object v2

    iget-object v3, v2, Ldjs;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v3}, Ldjs;->a(Ljava/util/concurrent/CountDownLatch;)V

    sget-object v3, Ldif;->b:[I

    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Ldiq;

    move-result-object v4

    invoke-virtual {v4}, Ldiq;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid delete URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->N:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :pswitch_2
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->O:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto :goto_0

    :pswitch_3
    const-string v0, "game_instances"

    invoke-virtual {p1, v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_4
    const-string v2, "game_instances"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_5
    const-string v2, "game_instances"

    const-string v3, "instance_game_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t delete game instances by external id!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_7
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->P:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldjs;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-static {v2}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    if-nez v3, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_9
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Q:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto :goto_0

    :pswitch_a
    const-string v2, "achievement_instances"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_b
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->R:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_c
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->S:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_d
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->T:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_e
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->U:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_f
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->V:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_10
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->W:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_11
    const-string v2, "leaderboard_pending_scores"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_12
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->X:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_13
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ab:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_14
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Y:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_15
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Z:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_16
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aa:Ldio;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ldio;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_17
    const-string v0, "participants"

    invoke-virtual {p1, v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_18
    const-string v2, "participants"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_19
    const-string v2, "participants"

    const-string v3, "external_participant_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1a
    const-string v2, "participants"

    const-string v3, "match_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1b
    const-string v2, "participants"

    const-string v3, "invitation_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1c
    const-string v0, "notifications"

    invoke-virtual {p1, v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1d
    const-string v2, "notifications"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1e
    const-string v2, "notifications"

    const-string v3, "game_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :pswitch_1f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t delete notifications by external id!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_20
    iget-object v0, v2, Ldjs;->a:Ldjt;

    invoke-virtual {v0}, Ldjt;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "achievement_definitions"

    invoke-virtual {v0, v1, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    const-string v3, "achievement_instances"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "games"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "game_badges"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "game_instances"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "invitations"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "leaderboards"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "leaderboard_instances"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "leaderboard_scores"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "matches"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "notifications"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "participants"

    invoke-virtual {v0, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v2, v1}, Ldjs;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "match_sync_token"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "request_sync_token"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_a
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_13
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_15
        :pswitch_0
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_11
        :pswitch_20
    .end packed-switch
.end method

.method protected final a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 5

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/net/Uri;)Ldjs;

    move-result-object v0

    const-string v1, "r"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldjs;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v1}, Ldjs;->a(Ljava/util/concurrent/CountDownLatch;)V

    iget-object v1, v0, Ldjs;->a:Ldjt;

    invoke-virtual {v1}, Ldjt;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-eqz v3, :cond_1

    iget-object v0, v0, Ldjs;->b:Ldju;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldju;J)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No image found for URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    const/4 v1, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/net/Uri;)Ldjs;

    move-result-object v2

    iget-object v0, v2, Ldjs;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0}, Ldjs;->a(Ljava/util/concurrent/CountDownLatch;)V

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    new-instance v4, Lblt;

    invoke-direct {v4, p2, p4, p5}, Lblt;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    sget-object v3, Ldif;->b:[I

    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Ldiq;

    move-result-object v6

    invoke-virtual {v6}, Ldiq;->ordinal()I

    move-result v6

    aget v3, v3, v6

    packed-switch v3, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid query URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    :goto_0
    if-eqz p3, :cond_0

    array-length v2, p3

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const-string v2, "_count"

    aget-object v1, p3, v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->h:Lqz;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    :cond_0
    invoke-virtual {v4}, Lblt;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v4, Lblt;->c:[Ljava/lang/String;

    move-object v1, p1

    move-object v2, p3

    move-object v6, v5

    move-object v7, p6

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    instance-of v1, v0, Landroid/database/AbstractWindowedCursor;

    invoke-static {v1}, Lbiq;->a(Z)V

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Ldir;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    :cond_1
    :goto_1
    return-object v0

    :pswitch_1
    const-string v2, "games._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_2
    const-string v2, "games._id"

    invoke-static {p2}, Leej;->c(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lblt;->a(Ljava/lang/String;J)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto :goto_0

    :pswitch_3
    invoke-static {p2}, Leej;->d(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "external_game_id"

    invoke-virtual {v4, v3, v2}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto :goto_0

    :pswitch_4
    const-string v2, "games._id"

    invoke-static {p2}, Leej;->c(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lblt;->a(Ljava/lang/String;J)V

    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->t:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/games/provider/GamesContentProvider;->r:Lqz;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    goto :goto_0

    :pswitch_5
    const-string v2, "games._id"

    invoke-static {p2}, Leej;->c(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lblt;->a(Ljava/lang/String;J)V

    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->u:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/games/provider/GamesContentProvider;->r:Lqz;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    goto :goto_0

    :pswitch_6
    const-string v2, "external_game_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_7
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_8
    const-string v2, "game_badges._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_9
    const-string v2, "badge_game_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_a
    const-string v2, "external_game_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_b
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_c
    const-string v2, "game_instances._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_d
    const-string v2, "instance_game_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_e
    const-string v2, "external_game_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_f
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_10
    const-string v2, "_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_11
    const-string v2, "external_player_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_12
    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->l:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/net/Uri;)Lqz;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lqz;)V

    goto/16 :goto_0

    :pswitch_13
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldjs;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    if-eqz p3, :cond_2

    :goto_2
    array-length v0, p3

    new-array v3, v0, [Ljava/lang/Object;

    move v0, v1

    :goto_3
    array-length v4, p3

    if-ge v0, v4, :cond_3

    aget-object v4, p3, v0

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    or-int/2addr v1, v4

    sget-object v4, Lcom/google/android/gms/games/provider/GamesContentProvider;->M:Ldhu;

    aget-object v6, p3, v0

    invoke-virtual {v4, v6}, Ldhu;->a(Ljava/lang/String;)Ldhw;

    move-result-object v4

    sget-object v6, Ldif;->a:[I

    invoke-virtual {v4}, Ldhw;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported metadata column type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->M:Ldhu;

    iget-object p3, v0, Ldhu;->a:[Ljava/lang/String;

    goto :goto_2

    :pswitch_14
    aget-object v4, p3, v0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_15
    aget-object v4, p3, v0

    const-wide/16 v6, -0x1

    invoke-interface {v2, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    goto :goto_4

    :cond_3
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, p3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz v1, :cond_1

    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    :pswitch_16
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_17
    const-string v2, "achievement_definitions._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_18
    const-string v2, "external_achievement_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_19
    const-string v2, "game_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_1a
    const-string v2, "external_game_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_1b
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_1c
    const-string v2, "achievement_pending_ops._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_1d
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_1e
    const-string v2, "achievement_instances._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_1f
    const-string v2, "player_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    const-string v2, "external_game_id"

    const-string v3, "external_game_id"

    invoke-virtual {v4, v2, v3}, Lblt;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_20
    const-string v2, "external_player_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    const-string v2, "external_game_id"

    const-string v3, "external_game_id"

    invoke-virtual {v4, v2, v3}, Lblt;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_21
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_22
    const-string v2, "client_contexts._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_23
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_24
    const-string v2, "leaderboards._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_25
    const-string v2, "external_leaderboard_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_26
    const-string v2, "game_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_27
    const-string v2, "external_game_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_28
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_29
    const-string v2, "leaderboard_instances._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_2a
    const-string v2, "external_game_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_2b
    const-string v2, "external_leaderboard_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_2c
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_2d
    const-string v2, "leaderboard_scores._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_2e
    const-string v2, "leaderboard_instances._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_2f
    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->z:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/games/provider/GamesContentProvider;->y:Lqz;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_0

    :pswitch_30
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_31
    const-string v2, "_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_32
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_33
    const-string v2, "invitations._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_34
    const-string v2, "_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    const-string v2, "invitations"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_35
    const-string v2, "external_invitation_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_36
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_37
    const-string v2, "requests._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_38
    const-string v2, "external_request_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_39
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_3a
    const-string v2, "request_recipients._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_3b
    const-string v2, "request_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_3c
    const-string v2, "requests._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    const-string v2, "requests"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_3d
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_3e
    const-string v2, "matches._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_3f
    const-string v2, "matches._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    const-string v2, "matches"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_40
    const-string v2, "external_match_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_41
    const-string v2, "game_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_42
    const-string v2, "external_game_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_43
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_44
    const-string v2, "matches_pending_ops._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_45
    const-string v2, "external_match_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    :pswitch_46
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_47
    const-string v2, "participants._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_48
    const-string v2, "external_participant_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_49
    const-string v2, "match_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_4a
    const-string v2, "invitation_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_4b
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_4c
    const-string v2, "requests._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_4d
    const-string v2, "external_request_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_4e
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->m(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_4f
    const-string v2, "matches._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->m(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_50
    const-string v2, "external_match_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->m(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_51
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_52
    const-string v2, "notifications._id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_53
    const-string v2, "game_id"

    invoke-virtual {v4, v2}, Lblt;->a(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    :pswitch_54
    const-string v2, "external_game_id"

    invoke-virtual {v4, v2}, Lblt;->b(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method protected final declared-synchronized a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/net/Uri;)Ldjs;

    move-result-object v0

    iget-object v0, v0, Ldjs;->a:Ldjt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 11

    move-object v2, p2

    move-object v1, p1

    move-object v0, p0

    :goto_0
    invoke-direct {v0, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/net/Uri;)Ldjs;

    move-result-object v5

    iget-object v3, v5, Ldjs;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v3}, Ldjs;->a(Ljava/util/concurrent/CountDownLatch;)V

    iget-object v6, v5, Ldjs;->b:Ldju;

    const-wide/16 v3, -0x1

    sget-object v7, Ldif;->b:[I

    invoke-static {v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Ldiq;

    move-result-object v8

    invoke-virtual {v8}, Ldiq;->ordinal()I

    move-result v8

    aget v7, v7, v8

    sparse-switch v7, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid insert URI: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v3, "external_game_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v0, v5, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Ldjs;Landroid/content/ContentValues;)V

    invoke-static {v2, v3}, Ldjb;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "games"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    :goto_1
    return-object v2

    :sswitch_1
    :try_start_0
    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    :goto_2
    const-wide/16 v5, -0x1

    cmp-long v5, v3, v5

    if-nez v5, :cond_0

    const-string v0, "GamesContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t find image to write to for URI "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v3

    :goto_3
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-gez v3, :cond_6

    new-instance v0, Landroid/database/sqlite/SQLiteException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Error occured while inserting "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to uri "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v2}, Ldjd;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_2
    invoke-direct {v0, v5, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "game_badges"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_3

    :sswitch_3
    const-string v0, "game_instances"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_3

    :sswitch_4
    const-string v3, "external_player_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v0, v5, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p3}, Lcum;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    invoke-static {v2, v3}, Ldjo;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "players"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    :sswitch_5
    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v5, v0}, Ldjs;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-virtual {p3}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/games/provider/GamesContentProvider;->M:Ldhu;

    invoke-virtual {v5, v1}, Ldhu;->a(Ljava/lang/String;)Ldhw;

    move-result-object v5

    sget-object v6, Ldif;->a:[I

    invoke-virtual {v5}, Ldhw;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported metadata column type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_4

    :pswitch_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-interface {v3, v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_4

    :cond_1
    invoke-static {v3}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_1

    :sswitch_6
    const-string v3, "external_achievement_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v0, v5, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Ldjs;Landroid/content/ContentValues;)V

    invoke-static {v2, v3}, Ldit;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "achievement_definitions"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :sswitch_7
    const-string v0, "achievement_pending_ops"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    :sswitch_8
    const-string v0, "achievement_instances"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    :sswitch_9
    const-string v3, "package_name"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "package_uid"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lez v5, :cond_2

    const/4 v4, 0x1

    :goto_5
    invoke-static {v4}, Lbkm;->a(Z)V

    const-string v4, "account_name"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    new-instance v6, Lblt;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v2, v7, v8}, Lblt;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    const-string v7, "package_name"

    invoke-virtual {v6, v7, v3}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "package_uid"

    int-to-long v7, v5

    invoke-virtual {v6, v3, v7, v8}, Lblt;->a(Ljava/lang/String;J)V

    const-string v3, "account_name"

    invoke-virtual {v6, v3, v4}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "client_contexts"

    const/4 v5, 0x0

    invoke-virtual {v6}, Lblt;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Lblt;->c:[Ljava/lang/String;

    move-object v3, v2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_5

    :sswitch_a
    const-string v3, "external_leaderboard_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v0, v5, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Ldjs;Landroid/content/ContentValues;)V

    invoke-static {v2, v3}, Ldji;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "leaderboards"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :sswitch_b
    const-string v3, "leaderboard_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    const/4 v3, 0x1

    :goto_6
    invoke-static {v3}, Lbkm;->a(Z)V

    const-string v3, "collection"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lbkm;->a(Z)V

    const-string v3, "collection"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v6, "timespan"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    invoke-static {v6}, Lbkm;->a(Z)V

    const-string v6, "timespan"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    new-instance v8, Lblt;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct {v8, v2, v7, v9}, Lblt;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    const-string v7, "leaderboard_id"

    invoke-virtual {v8, v7, v4, v5}, Lblt;->a(Ljava/lang/String;J)V

    const-string v4, "collection"

    int-to-long v9, v3

    invoke-virtual {v8, v4, v9, v10}, Lblt;->a(Ljava/lang/String;J)V

    const-string v3, "timespan"

    int-to-long v4, v6

    invoke-virtual {v8, v3, v4, v5}, Lblt;->a(Ljava/lang/String;J)V

    const-string v4, "leaderboard_instances"

    const/4 v5, 0x0

    invoke-virtual {v8}, Lblt;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v8, Lblt;->c:[Ljava/lang/String;

    move-object v3, v2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_6

    :sswitch_c
    invoke-direct {v0, v5, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "leaderboard_scores"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    :sswitch_d
    const-string v0, "leaderboard_pending_scores"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    :sswitch_e
    const-string v1, "url"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "Must provide an image URL when inserting"

    invoke-static {v1, v3}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "url"

    const-string v3, "_id"

    invoke-direct {v0, v5, p3, v1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldjs;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v0, v6, v3, v4, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ldju;JLandroid/content/ContentValues;)I

    move-wide v0, v3

    goto/16 :goto_3

    :sswitch_f
    const-string v3, "external_invitation_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v2, v3}, Ldje;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "invitations"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :sswitch_10
    const-string v3, "external_match_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v2, v3}, Ldjk;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "matches"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :sswitch_11
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcwj;->a(I)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    const-string v0, "matches_pending_ops"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    :sswitch_12
    const-string v3, "game_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lbkm;->a(Z)V

    const-string v3, "game_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-string v5, "external_sub_id"

    invoke-virtual {p3, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "type"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    invoke-static {v6}, Lbkm;->a(Z)V

    new-instance v6, Lblt;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v2, v7, v8}, Lblt;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    const-string v7, "game_id"

    invoke-virtual {v6, v7, v3, v4}, Lblt;->a(Ljava/lang/String;J)V

    const-string v3, "external_sub_id"

    invoke-virtual {v6, v3, v5}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "notifications"

    const/4 v5, 0x0

    invoke-virtual {v6}, Lblt;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Lblt;->c:[Ljava/lang/String;

    move-object v3, v2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :sswitch_13
    const-string v3, "placing"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    const-string v3, "result_type"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lbiq;->a(Ljava/lang/Object;)V

    :cond_4
    invoke-direct {v0, v5, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Ldjs;Landroid/content/ContentValues;)V

    const-string v0, "participants"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    :sswitch_14
    const-string v3, "external_request_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v2, v3}, Ldjr;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "requests"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :sswitch_15
    const-string v3, "request_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    const-string v4, "player_id"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Ldjq;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "request_recipients"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :cond_5
    const-string v4, "player_id"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v6, Lblt;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v2, v7, v8}, Lblt;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    const-string v7, "player_id"

    invoke-virtual {v6, v7, v4, v5}, Lblt;->a(Ljava/lang/String;J)V

    const-string v4, "request_id"

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v6, v4, v7, v8}, Lblt;->a(Ljava/lang/String;J)V

    const-string v4, "request_recipients"

    const/4 v5, 0x0

    invoke-virtual {v6}, Lblt;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Lblt;->c:[Ljava/lang/String;

    move-object v3, v2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :cond_6
    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :catch_0
    move-exception v5

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x6 -> :sswitch_1
        0x8 -> :sswitch_2
        0xc -> :sswitch_3
        0x10 -> :sswitch_4
        0x14 -> :sswitch_5
        0x15 -> :sswitch_6
        0x1a -> :sswitch_7
        0x1c -> :sswitch_8
        0x20 -> :sswitch_9
        0x22 -> :sswitch_a
        0x27 -> :sswitch_b
        0x2b -> :sswitch_c
        0x2e -> :sswitch_d
        0x2f -> :sswitch_e
        0x30 -> :sswitch_e
        0x31 -> :sswitch_f
        0x35 -> :sswitch_14
        0x38 -> :sswitch_15
        0x3c -> :sswitch_10
        0x42 -> :sswitch_11
        0x45 -> :sswitch_13
        0x50 -> :sswitch_12
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Ldir;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 6

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->e:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iput-wide v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->e:J

    invoke-direct {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final b(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Leej;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    sget-object v0, Ldif;->b:[I

    invoke-static {p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Ldiq;

    move-result-object v1

    invoke-virtual {v1}, Ldiq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.games"

    :goto_0
    return-object v0

    :pswitch_2
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.game"

    goto :goto_0

    :pswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.game_badges"

    goto :goto_0

    :pswitch_4
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.game_badge"

    goto :goto_0

    :pswitch_5
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.game_instances"

    goto :goto_0

    :pswitch_6
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.game_instance"

    goto :goto_0

    :pswitch_7
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.players"

    goto :goto_0

    :pswitch_8
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.player"

    goto :goto_0

    :pswitch_9
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.extended_players"

    goto :goto_0

    :pswitch_a
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.account_metadata"

    goto :goto_0

    :pswitch_b
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.achievements"

    goto :goto_0

    :pswitch_c
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.achievement"

    goto :goto_0

    :pswitch_d
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.achievement_pending_ops"

    goto :goto_0

    :pswitch_e
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.achievement_pending_op"

    goto :goto_0

    :pswitch_f
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.achievement_states"

    goto :goto_0

    :pswitch_10
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.achievement_state"

    goto :goto_0

    :pswitch_11
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.client_contexts"

    goto :goto_0

    :pswitch_12
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.client_context"

    goto :goto_0

    :pswitch_13
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.invitations"

    goto :goto_0

    :pswitch_14
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.invitation"

    goto :goto_0

    :pswitch_15
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.leaderboards"

    goto :goto_0

    :pswitch_16
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.leaderboard"

    goto :goto_0

    :pswitch_17
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.leaderboard_instances"

    goto :goto_0

    :pswitch_18
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.leaderboard_instance"

    goto :goto_0

    :pswitch_19
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.leaderboard_scores"

    goto :goto_0

    :pswitch_1a
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.leaderboard_score"

    goto :goto_0

    :pswitch_1b
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.leaderboard_pending_scores"

    goto :goto_0

    :pswitch_1c
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.leaderboard_pending_score"

    goto :goto_0

    :pswitch_1d
    const-string v0, "image/png"

    goto :goto_0

    :pswitch_1e
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.images"

    goto :goto_0

    :pswitch_1f
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.matches"

    goto :goto_0

    :pswitch_20
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.match"

    goto :goto_0

    :pswitch_21
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.match_pending_ops"

    goto :goto_0

    :pswitch_22
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.match_pending_op"

    goto :goto_0

    :pswitch_23
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.notifications"

    goto :goto_0

    :pswitch_24
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.notification"

    goto :goto_0

    :pswitch_25
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.participants"

    goto :goto_0

    :pswitch_26
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.participant"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_f
        :pswitch_f
        :pswitch_11
        :pswitch_12
        :pswitch_15
        :pswitch_16
        :pswitch_16
        :pswitch_15
        :pswitch_15
        :pswitch_17
        :pswitch_18
        :pswitch_17
        :pswitch_17
        :pswitch_19
        :pswitch_1a
        :pswitch_19
        :pswitch_1b
        :pswitch_1e
        :pswitch_1d
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_20
        :pswitch_1f
        :pswitch_1f
        :pswitch_21
        :pswitch_22
        :pswitch_22
        :pswitch_25
        :pswitch_26
        :pswitch_26
        :pswitch_25
        :pswitch_25
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_23
        :pswitch_24
        :pswitch_23
        :pswitch_23
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lbls;->onCreate()Z

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "GamesProviderWorker"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Ldhx;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ldhx;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->d:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->d:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v3
.end method

.method public shutdown()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->d:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/HandlerThread;

    return-void
.end method
