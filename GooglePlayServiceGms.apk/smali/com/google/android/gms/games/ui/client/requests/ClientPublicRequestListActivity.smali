.class public final Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Leaz;
.implements Lebe;


# instance fields
.field private v:Ldzd;

.field private w:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

.field private x:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxm;-><init>()V

    return-void
.end method


# virtual methods
.method public final n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04007b    # com.google.android.gms.R.layout.games_public_request_list_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->setContentView(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->s:Z

    new-instance v0, Ldzd;

    invoke-direct {v0, p0}, Ldzd;-><init>(Ldxm;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->v:Ldzd;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->w:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->x:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->w:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const v0, 0x7f0b02dc    # com.google.android.gms.R.string.games_request_inbox_header_gifts

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->setTitle(I)V

    :goto_0
    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->w:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->p_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljj;->b(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_1
    const v0, 0x7f0b02dd    # com.google.android.gms.R.string.games_request_inbox_header_wishes

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->setTitle(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final p()I
    .locals 1

    const/16 v0, 0xe

    return v0
.end method

.method public final u()Lcom/google/android/gms/games/internal/request/GameRequestCluster;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->w:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    return-object v0
.end method

.method public final v()Lebd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestListActivity;->v:Ldzd;

    return-object v0
.end method
