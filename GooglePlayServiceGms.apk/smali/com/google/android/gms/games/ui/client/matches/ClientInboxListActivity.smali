.class public final Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Ldwt;
.implements Ldxc;
.implements Ldzy;


# instance fields
.field private A:Landroid/support/v4/app/Fragment;

.field private v:I

.field private w:I

.field private x:Ldxs;

.field private y:Landroid/widget/ProgressBar;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxm;-><init>()V

    return-void
.end method

.method private b(Z)V
    .locals 3

    const v2, 0x7f0a0152    # com.google.android.gms.R.id.container

    iget-object v1, p0, Lo;->b:Lw;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->A:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->A:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v2, v1}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    :goto_1
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    new-instance v0, Ldxw;

    invoke-direct {v0}, Ldxw;-><init>()V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-static {v0}, Ldxx;->d(I)Ldxx;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    invoke-static {v0}, Ldxx;->d(I)Ldxx;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    invoke-static {v0}, Ldxx;->d(I)Ldxx;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v2}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->A:Landroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->A:Landroid/support/v4/app/Fragment;

    const-string v1, "Failed to find fragment during resume!"

    invoke-static {v0, v1}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_5
    const v0, 0x7f0b02ba    # com.google.android.gms.R.string.games_match_inbox_title

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->setTitle(I)V

    return-void

    :pswitch_6
    const v0, 0x7f0b02bb    # com.google.android.gms.R.string.games_match_inbox_header_invitations

    goto :goto_2

    :pswitch_7
    const v0, 0x7f0b02bd    # com.google.android.gms.R.string.games_match_inbox_header_my_turn

    goto :goto_2

    :pswitch_8
    const v0, 0x7f0b02be    # com.google.android.gms.R.string.games_match_inbox_header_their_turn

    goto :goto_2

    :pswitch_9
    const v0, 0x7f0b02bf    # com.google.android.gms.R.string.games_match_inbox_header_completed_matches

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public final D_()Ldzx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->x:Ldxs;

    return-object v0
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->y:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->y:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final d_(Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, -0x1

    const-string v0, "matchesButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    if-eq v0, v2, :cond_0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->b(Z)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "invitationsButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "myTurnButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const-string v0, "theirTurnButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_0

    :cond_4
    const-string v0, "completedMatchesButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_2

    const/16 v0, 0x384

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->A:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Ldxv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->A:Landroid/support/v4/app/Fragment;

    check-cast v0, Ldxv;

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    const-string v2, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ldxv;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->A:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Ldxw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->A:Landroid/support/v4/app/Fragment;

    check-cast v0, Ldxw;

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    const-string v2, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ldxw;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Ldxm;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->v:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid entry mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->v:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iput v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->b(Z)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_1
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-super {p0}, Ldxm;->onBackPressed()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040065    # com.google.android.gms.R.layout.games_inbox_list_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->setContentView(I)V

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->s:Z

    new-instance v0, Ldxs;

    invoke-direct {v0, p0}, Ldxs;-><init>(Ldxm;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->x:Ldxs;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.google.android.gms.games.SHOW_MULTIPLAYER_INBOX"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->v:I

    :goto_0
    if-nez p1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->v:I

    packed-switch v3, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid entry mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->v:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v3, "com.google.android.gms.games.SHOW_INVITATIONS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->v:I

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Intent action is invalid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move v0, v2

    goto :goto_1

    :pswitch_0
    iput v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->b(Z)V

    return-void

    :pswitch_1
    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    goto :goto_2

    :cond_3
    const-string v1, "savedStateCurrentFragmentIndex"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v6, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v2, 0x7f12000f    # com.google.android.gms.R.menu.games_inbox_menu

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Ldxm;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->y:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v6

    :cond_0
    const v0, 0x7f0a037e    # com.google.android.gms.R.id.menu_progress_bar

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04001a    # com.google.android.gms.R.layout.actionbar_indeterminate_progress

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v0, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f0a007a    # com.google.android.gms.R.id.progress_bar

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->y:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->y:Landroid/widget/ProgressBar;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->z:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-static {v2, v3}, Leu;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_1
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldxm;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "savedStateCurrentFragmentIndex"

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->w:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method protected final p()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method public final u()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->v:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x_()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->y:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;->y:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method
