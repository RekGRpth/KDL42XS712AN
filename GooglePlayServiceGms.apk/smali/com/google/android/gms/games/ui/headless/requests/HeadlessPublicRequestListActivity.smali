.class public final Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;
.super Ldwr;
.source "SourceFile"

# interfaces
.implements Leaz;
.implements Lebe;
.implements Lecb;


# instance fields
.field private q:Lecj;

.field private r:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const v0, 0x7f04007b    # com.google.android.gms.R.layout.games_public_request_list_activity

    const v1, 0x7f12000e    # com.google.android.gms.R.menu.games_headless_public_list_menu

    invoke-direct {p0, v0, v1}, Ldwr;-><init>(II)V

    return-void
.end method


# virtual methods
.method public final a()Leca;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->q:Lecj;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Ldwr;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->r:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->s:Ljava/lang/String;

    new-instance v0, Lecj;

    invoke-direct {v0, p0}, Lecj;-><init>(Ldwr;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->q:Lecj;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->r:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const v0, 0x7f0b02dc    # com.google.android.gms.R.string.games_request_inbox_header_gifts

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->setTitle(I)V

    :goto_1
    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->r:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->p_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljj;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b02dd    # com.google.android.gms.R.string.games_request_inbox_header_wishes

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->setTitle(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final u()Lcom/google/android/gms/games/internal/request/GameRequestCluster;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->r:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    return-object v0
.end method

.method public final v()Lebd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestListActivity;->q:Lecj;

    return-object v0
.end method
