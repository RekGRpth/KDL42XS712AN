.class public final Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Leat;


# instance fields
.field private v:Ledy;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxm;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 3

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "player_search_results"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->finish()V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->getIntent()Landroid/content/Intent;

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04005d    # com.google.android.gms.R.layout.games_client_player_search_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->setContentView(I)V

    const v0, 0x7f0b02d2    # com.google.android.gms.R.string.games_player_search_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->setTitle(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->s:Z

    iget-object v0, p0, Lo;->b:Lw;

    const v1, 0x7f0a0139    # com.google.android.gms.R.id.player_search_results_fragment

    invoke-virtual {v0, v1}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v1, Ledy;

    invoke-direct {v1, p0, v0}, Ledy;-><init>(Ldvn;Leec;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->v:Ledy;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->v:Ledy;

    invoke-virtual {v0, p1}, Ledy;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f12000d    # com.google.android.gms.R.menu.games_generic_search_screen_menu

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->v:Ledy;

    iget-object v0, v1, Ledy;->a:Ldvn;

    invoke-virtual {v0}, Ldvn;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v2, Lxd;->j:I

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v0, Lxa;->aS:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Leu;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    iput-object v0, v1, Ledy;->b:Landroid/support/v7/widget/SearchView;

    iget-object v0, v1, Ledy;->b:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, v1, Ledy;->b:Landroid/support/v7/widget/SearchView;

    if-nez v0, :cond_0

    const-string v0, "SearchHelper"

    const-string v1, "got passed a null searchView!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-super {p0, p1}, Ldxm;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    :cond_0
    new-instance v2, Leea;

    invoke-direct {v2, v1}, Leea;-><init>(Ledy;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Lpm;)V

    new-instance v2, Leeb;

    invoke-direct {v2, v1, v0}, Leeb;-><init>(Ledy;Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView;->setOnCloseListener(Lpl;)V

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/SearchView;->setIconified(Z)V

    iget-object v2, v1, Ledy;->a:Ldvn;

    invoke-virtual {v2}, Ldvn;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v1, Ledy;->c:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Ledy;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v1, v1, Ledy;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getImeOptions()I

    move-result v1

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setImeOptions(I)V

    goto :goto_0
.end method

.method protected final onNewIntent(Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->setIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->v:Ledy;

    const-string v0, "SearchHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onNewIntent: Unexpected \'relaunch\' of search activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onNewIntent: Unexpected \'relaunch\' of search activity: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    return-void
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldxm;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->v:Ledy;

    const-string v1, "savedStatePreviousQuery"

    iget-object v0, v0, Ledy;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->v:Ledy;

    invoke-virtual {v0}, Ledy;->c()Z

    move-result v0

    return v0
.end method

.method public final onStart()V
    .locals 2

    invoke-super {p0}, Ldxm;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->v:Ledy;

    const/4 v1, 0x0

    iput-boolean v1, v0, Ledy;->f:Z

    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Ldxm;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->v:Ledy;

    invoke-virtual {v0}, Ledy;->a()V

    return-void
.end method

.method protected final p()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method
