.class public final Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final n:Ljava/util/HashMap;


# instance fields
.field private o:Landroid/widget/ListView;

.field private p:I

.field private q:Landroid/view/View;

.field private r:Landroid/widget/CheckBox;

.field private s:Landroid/view/View;

.field private t:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method private b(Z)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcwh;->g:Lbfy;

    invoke-virtual {v1}, Lbfy;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lbfx;->a:Lbfy;

    invoke-virtual {v1}, Lbfy;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sput-boolean p1, Lsq;->b:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static e()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->f()V

    new-instance v3, Ldvu;

    sget-object v0, Lcwh;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcwh;->b:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcwh;->c:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v0, v1, v2}, Ldvu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldvu;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Ldvu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f()V
    .locals 4

    sget-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ldvu;

    const-string v1, "https://www-googleapis-staging.sandbox.google.com"

    const-string v2, "vdev"

    const-string v3, "vwhitelisteddev"

    invoke-direct {v0, v1, v2, v3}, Ldvu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    const-string v2, "Dev"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ldvu;

    const-string v1, "https://www-googleapis-staging.sandbox.google.com"

    const-string v2, "v1"

    const-string v3, "v1whitelisted"

    invoke-direct {v0, v1, v2, v3}, Ldvu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    const-string v2, "Staging"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ldvu;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "v1"

    const-string v3, "v1whitelisted"

    invoke-direct {v0, v1, v2, v3}, Ldvu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    const-string v2, "Prod"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static g()Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvu;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-interface {v4, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget v4, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->p:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    iget v5, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->p:I

    invoke-virtual {v4, v5, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    invoke-virtual {v1, v3, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setSelection(I)V

    iput v3, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->p:I

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcwh;->a:Lbfy;

    invoke-virtual {v3}, Lbfy;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Ldvu;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcwh;->b:Lbfy;

    invoke-virtual {v3}, Lbfy;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Ldvu;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcwh;->c:Lbfy;

    invoke-virtual {v3}, Lbfy;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Ldvu;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lakl;->a:Lbfy;

    invoke-virtual {v3}, Lbfy;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldvu;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    move v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->q:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->r:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcwh;->m:Lbfy;

    invoke-virtual {v1}, Lbfy;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->r:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->s:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->t:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->t:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->t:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b(Z)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    const v8, 0x7f0a01ae    # com.google.android.gms.R.id.widget_frame

    const v7, 0x7f0a0060    # com.google.android.gms.R.id.checkbox

    const v6, 0x7f0a005e    # com.google.android.gms.R.id.title

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v0, v3}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->finish()V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const v0, 0x7f040083    # com.google.android.gms.R.layout.games_settings_debug_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->setContentView(I)V

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->f()V

    const v0, 0x7f0a01a9    # com.google.android.gms.R.id.games_settings_reshow_achievements

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->q:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->q:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->q:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0b0218    # com.google.android.gms.R.string.games_settings_debug_achievements_reshow

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->q:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->q:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->r:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->r:Landroid/widget/CheckBox;

    sget-object v0, Lcwh;->m:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    const v0, 0x7f0a01ac    # com.google.android.gms.R.id.games_settings_debug_server_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "Available Servers"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->p:I

    const v0, 0x7f0a01ad    # com.google.android.gms.R.id.server_list_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x109000f    # android.R.layout.simple_list_item_single_choice

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->g()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a(Ljava/lang/CharSequence;)Z

    const v0, 0x7f0a01ab    # com.google.android.gms.R.id.games_settings_volley_logging

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->s:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->s:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->s:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0b021b    # com.google.android.gms.R.string.games_settings_debug_enable_volley_logging

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->s:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lcwh;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lbfx;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lsq;->b:Z

    if-eqz v0, :cond_2

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->s:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->t:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->t:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b(Z)V

    goto/16 :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->o:Landroid/widget/ListView;

    if-ne p1, v0, :cond_0

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    check-cast p2, Landroid/widget/CheckedTextView;

    const v0, 0x7f0b0219    # com.google.android.gms.R.string.games_settings_debug_clear_data_title

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b021a    # com.google.android.gms.R.string.games_settings_debug_clear_data_body

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a    # android.R.string.ok

    new-instance v3, Ldvt;

    invoke-direct {v3, p0, p2}, Ldvt;-><init>(Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000    # android.R.string.cancel

    new-instance v3, Ldvs;

    invoke-direct {v3, p0}, Ldvs;-><init>(Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    :cond_0
    return-void
.end method
