.class public final Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Ldgn;
.implements Ledt;


# static fields
.field private static final v:[I


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/view/View;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/view/animation/Animation;

.field private F:Landroid/view/animation/Animation;

.field private G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Landroid/os/Handler;

.field private K:Ljava/lang/Runnable;

.field private L:Ljava/lang/Runnable;

.field private M:I

.field private N:I

.field private O:Landroid/content/Intent;

.field private P:Z

.field private Q:Z

.field private R:Z

.field private w:Leds;

.field private x:Ldye;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x78
        0x1e0
        0x4b0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ldxm;-><init>()V

    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->N:I

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->P:Z

    return-void
.end method

.method private A()Z
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B()I

    move-result v6

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-interface {v7}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->d()Z

    move-result v0

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    if-lt v6, v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private B()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_1

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->a()I

    move-result v5

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    if-eq v5, v0, :cond_2

    const/4 v0, 0x4

    if-eq v5, v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private C()Z
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->a()I

    move-result v0

    const/4 v7, 0x3

    if-eq v0, v7, :cond_0

    const/4 v7, 0x4

    if-ne v0, v7, :cond_3

    :cond_0
    add-int/lit8 v0, v1, 0x1

    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_1
    sub-int v0, v5, v1

    if-ge v0, v6, :cond_2

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private D()V
    .locals 2

    const-string v0, "WaitingRoom"

    const-string v1, "Stale room! We\'re being restarted after having been previously stopped."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "WaitingRoom"

    const-string v1, "We were disconnected from the games service while stopped, so our Room object may now be out of date."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "WaitingRoom"

    const-string v1, "We can\'t display the room correctly any more, so bailing out with RESULT_CANCELED..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    return-void
.end method

.method private E()V
    .locals 8

    const/16 v1, 0x400

    const/4 v7, 0x0

    const/16 v6, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0}, Ljj;->c()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Landroid/view/View;

    const v1, 0x7f0a0147    # com.google.android.gms.R.id.popup_text_label

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Landroid/view/View;

    const v2, 0x7f0a0155    # com.google.android.gms.R.id.popup_text_data

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Landroid/view/View;

    const v3, 0x7f0a0156    # com.google.android.gms.R.id.popup_text_single_line_message

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Landroid/view/View;

    const v4, 0x7f0a0145    # com.google.android.gms.R.id.popup_icon

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Landroid/view/View;

    const v5, 0x7f0a018b    # com.google.android.gms.R.id.overlay_game_image

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v5, 0x7f0b0235    # com.google.android.gms.R.string.games_waiting_room_prepare_to_play

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v0

    :cond_0
    const v1, 0x7f0200d5    # com.google.android.gms.R.drawable.games_default_game_img

    invoke-virtual {v4, v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    invoke-virtual {v4, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Landroid/view/View;

    invoke-static {v0}, Leeh;->a(Landroid/view/View;)V

    new-instance v0, Ldyd;

    invoke-direct {v0, p0}, Ldyd;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_1
    const/4 v0, 0x4

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    iput p1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->N:I

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->N:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->O:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(I)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A()Z

    move-result v0

    return v0
.end method

.method public static synthetic c(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v1, Ldyb;

    invoke-direct {v1, p0}, Ldyb;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    const v0, 0x7f0b023b    # com.google.android.gms.R.string.games_waiting_room_waiting_for_players

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0012    # com.google.android.gms.R.plurals.games_waiting_room_waited_start_early

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f040061    # com.google.android.gms.R.layout.games_dialog_custom_body

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0a0149    # com.google.android.gms.R.id.games_dialog_custom_body_text

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0b0238    # com.google.android.gms.R.string.games_waiting_room_keep_waiting

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0b0239    # com.google.android.gms.R.string.games_waiting_room_start_now

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 6

    new-instance v1, Ldyc;

    invoke-direct {v1, p0}, Ldyc;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    const v0, 0x7f0b023b    # com.google.android.gms.R.string.games_waiting_room_waiting_for_players

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0b023c    # com.google.android.gms.R.string.games_waiting_room_waited_cant_start

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f040061    # com.google.android.gms.R.layout.games_dialog_custom_body

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0a0149    # com.google.android.gms.R.id.games_dialog_custom_body_text

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0b0238    # com.google.android.gms.R.string.games_waiting_room_keep_waiting

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0b023a    # com.google.android.gms.R.string.games_waiting_room_cancel_match

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    return-void
.end method

.method private e(I)Landroid/view/animation/Animation;
    .locals 2

    invoke-static {p0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y()V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E()V

    return-void
.end method

.method public static synthetic h(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n()V

    return-void
.end method

.method public static synthetic i(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->P:Z

    return v0
.end method

.method public static synthetic m(Lcom/google/android/gms/games/multiplayer/realtime/Room;)I
    .locals 6

    const/4 v2, 0x0

    invoke-static {p0}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-interface {p0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->i()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "max_automatch_players"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private n()V
    .locals 6

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->M:I

    sget-object v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ldxy;

    invoke-direct {v0, p0}, Ldxy;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Ljava/lang/Runnable;

    sget-object v2, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:[I

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->M:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->M:I

    aget v2, v2, v3

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 5

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "WaitingRoom"

    const-string v1, "updateRoom: room changed out from under us!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "- previous: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "-      new: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "updateRoom: room changed out from under us!"

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->o(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Ldye;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v0, v1}, Ldye;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->z()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u()V

    new-instance v1, Ldya;

    invoke-direct {v1, p0}, Ldya;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    const v0, 0x7f0b0236    # com.google.android.gms.R.string.games_waiting_room_cant_start_match

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f040061    # com.google.android.gms.R.layout.games_dialog_custom_body

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a0149    # com.google.android.gms.R.id.games_dialog_custom_body_text

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v4, 0x7f0b0237    # com.google.android.gms.R.string.games_waiting_room_cant_start_match_body

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v2, 0x104000a    # android.R.string.ok

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v()V

    new-instance v0, Ldxz;

    invoke-direct {v0, p0}, Ldxz;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y()V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x()V

    goto :goto_0
.end method

.method private o(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 5

    invoke-static {p0}, Lbox;->b(Landroid/app/Activity;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "room"

    iget v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v0, v1, v4}, Lbis;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "WaitingRoom"

    const-string v1, "Unable to return room to game. Something has gone very wrong."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->O:Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->O:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->N:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->O:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private u()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Ljava/lang/Runnable;

    return-void
.end method

.method private v()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Ljava/lang/Runnable;

    return-void
.end method

.method private w()V
    .locals 9

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "WaitingRoom"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "updateHeader: unexpected room status: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "updateHeader: unexpected room status: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    move v0, v1

    move-object v2, v3

    :goto_1
    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->z:Landroid/view/View;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B()I

    move-result v0

    if-le v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0f0014    # com.google.android.gms.R.plurals.games_waiting_room_players_ready

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B()I

    move-result v5

    new-array v6, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    move v8, v2

    move-object v2, v0

    move v0, v8

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b0230    # com.google.android.gms.R.string.games_waiting_room_room_status_inviting

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b0231    # com.google.android.gms.R.string.games_waiting_room_room_status_auto_matching

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v8, v2

    move-object v2, v0

    move v0, v8

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b0232    # com.google.android.gms.R.string.games_waiting_room_room_status_connecting

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v8, v2

    move-object v2, v0

    move v0, v8

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0233    # com.google.android.gms.R.string.games_waiting_room_room_status_active

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    goto :goto_1

    :pswitch_4
    const-string v0, "WaitingRoom"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "updateHeader: unexpected DELETED status: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    move-object v2, v3

    goto/16 :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_3
    const/16 v0, 0x8

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private x()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Z

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Z

    if-nez v0, :cond_3

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:Z

    goto :goto_0
.end method

.method private y()V
    .locals 7

    const/4 v6, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v1, v0

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C:Landroid/widget/TextView;

    const/16 v2, 0x50

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0013    # com.google.android.gms.R.plurals.games_waiting_room_start_with

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private z()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->j()Lbdu;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w:Leds;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leds;->a(I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "WaitingRoom"

    const-string v1, "updateLoadingDataView: couldn\'t get GoogleApiClient"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "WaitingRoom"

    const-string v1, "displayParticipants: room has no participants!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w:Leds;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Leds;->a(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public final R()V
    .locals 0

    return-void
.end method

.method public final a(ILcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 3

    const-string v0, "WaitingRoom"

    const-string v1, "CALLBACK: onRoomConnected()..."

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onRoomConnected: statusCode = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for room ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v()V

    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final e(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final e_(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unexpected callback: onLeftRoom: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final f(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final g(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final h(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 2

    const-string v0, "WaitingRoom"

    const-string v1, "CALLBACK: onDisconnectedFromRoom()..."

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final i(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final j(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    return-void
.end method

.method public final k(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unexpected callback: onRoomCreated: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Ldxm;->l(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    const-string v0, "WaitingRoom"

    const-string v1, "onConnected: no valid room; ignoring this callback..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->j()Lbdu;

    move-result-object v3

    invoke-interface {v3}, Lbdu;->d()Z

    move-result v0

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbiq;->a(Z)V

    sget-object v0, Lcte;->m:Lctw;

    invoke-interface {v0, v3}, Lctw;->a(Lbdu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "WaitingRoom"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Ljava/lang/String;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Ldye;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ldye;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Ldye;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v0, v4}, Ldye;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    :goto_2
    invoke-static {v1}, Lbiq;->a(Z)V

    sget-object v1, Lcte;->k:Ldgh;

    invoke-interface {v1, v3, p0, v0}, Ldgh;->a(Lbdu;Ldgn;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/Room;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "WaitingRoom"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Room status after registering listener: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->d()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E()V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->z()V

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final l(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unexpected callback: onJoinedRoom: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a018d
        :pswitch_0    # com.google.android.gms.R.id.waiting_room_start_now_button_container
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbox;->b(Landroid/app/Activity;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "WaitingRoom"

    const-string v1, "Could not find calling context. Aborting activity."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "room"

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->q:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lbis;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;Ljava/lang/Integer;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    if-eqz p1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->o(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D()V

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    const-string v0, "WaitingRoom"

    const-string v1, "EXTRA_ROOM extra missing; bailing out..."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    goto :goto_0

    :cond_2
    const-string v2, "com.google.android.gms.games.MIN_PARTICIPANTS_TO_START"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    if-gez v1, :cond_3

    const-string v0, "WaitingRoom"

    const-string v1, "EXTRA_MIN_PARTICIPANTS_TO_START extra missing; bailing out..."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    if-ge v1, v5, :cond_4

    iput v5, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    :cond_4
    const v1, 0x7f04007c    # com.google.android.gms.R.layout.games_real_time_waiting_room_activity

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->setContentView(I)V

    const v1, 0x7f0a0193    # com.google.android.gms.R.id.status_text

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y:Landroid/widget/TextView;

    const v1, 0x7f0a0194    # com.google.android.gms.R.id.progress_spinner

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->z:Landroid/view/View;

    const v1, 0x7f0a0189    # com.google.android.gms.R.id.real_time_waiting_room_overlay

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Landroid/view/View;

    const v1, 0x7f0a018c    # com.google.android.gms.R.id.button_bar_container

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:Landroid/view/View;

    const v1, 0x7f0a018d    # com.google.android.gms.R.id.waiting_room_start_now_button_container

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a0190    # com.google.android.gms.R.id.waiting_room_start_now_text

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C:Landroid/widget/TextView;

    const v1, 0x7f0a0191    # com.google.android.gms.R.id.waiting_room_start_now_subtext

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/widget/TextView;

    const v1, 0x7f05000a    # com.google.android.gms.R.anim.games_play_button_slidein

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->e(I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Landroid/view/animation/Animation;

    const v1, 0x7f05000b    # com.google.android.gms.R.anim.games_play_button_slideout

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->e(I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Z

    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:Z

    iput v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->M:I

    const v1, 0x7f0a0188    # com.google.android.gms.R.id.players_list_container

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Leds;

    invoke-direct {v2, v1, p0}, Leds;-><init>(Landroid/view/View;Ledt;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w:Leds;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w:Leds;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Leds;->a(I)V

    const v2, 0x102000a    # android.R.id.list

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setFocusable(Z)V

    new-instance v2, Ldye;

    invoke-direct {v2, p0}, Ldye;-><init>(Ldxm;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Ldye;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Ldye;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Landroid/os/Handler;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n()V

    goto/16 :goto_0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120006    # com.google.android.gms.R.menu.games_client_waiting_room_menu

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Ldxm;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onDestroy()V
    .locals 0

    invoke-super {p0}, Ldxm;->onDestroy()V

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Ldxm;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User explicitly asked to leave the room! item = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x2715

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a0382
        :pswitch_0    # com.google.android.gms.R.id.menu_waiting_room_leave_room
    .end packed-switch
.end method

.method public final onPause()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->P:Z

    invoke-super {p0}, Ldxm;->onPause()V

    return-void
.end method

.method public final onResume()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->P:Z

    invoke-super {p0}, Ldxm;->onResume()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w()V

    goto :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldxm;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "savedStateRecreatedFlag"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onStart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D()V

    :cond_0
    invoke-super {p0}, Ldxm;->onStart()V

    return-void
.end method

.method public final onStop()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    invoke-super {p0}, Ldxm;->onStop()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcte;->k:Ldgh;

    invoke-interface {v2, v0, v1}, Ldgh;->c(Lbdu;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-super {p0}, Ldxm;->onStop()V

    goto :goto_0
.end method

.method protected final p()I
    .locals 1

    const/16 v0, 0x9

    return v0
.end method
