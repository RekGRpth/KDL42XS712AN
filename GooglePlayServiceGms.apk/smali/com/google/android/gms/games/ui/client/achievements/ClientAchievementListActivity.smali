.class public final Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListActivity;
.super Ldxm;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxm;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040056    # com.google.android.gms.R.layout.games_achievement_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListActivity;->setContentView(I)V

    const v0, 0x7f0b028d    # com.google.android.gms.R.string.games_achievement_list_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListActivity;->setTitle(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListActivity;->s:Z

    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120001    # com.google.android.gms.R.menu.games_client_achievements_menu

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Ldxm;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected final p()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
