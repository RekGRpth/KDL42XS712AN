.class public final Lcom/google/android/gms/games/ui/signin/SignInActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lduw;
.implements Ldux;


# static fields
.field static final n:Ljava/util/HashMap;

.field static final o:Ljava/util/ArrayList;


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:I

.field private G:I

.field private H:I

.field private I:J

.field private p:Lduk;

.field private q:Landroid/view/View;

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:[Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Integer;

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    const/16 v0, 0x9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Ljava/util/ArrayList;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Ljava/util/ArrayList;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lo;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->A:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->C:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->D:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->E:Z

    const/16 v0, 0x11

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->F:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->G:I

    iput v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->H:I

    return-void
.end method

.method private a(Ljava/lang/Class;)V
    .locals 4

    const v0, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1, v0}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v2, v0, Lecz;

    if-eqz v2, :cond_0

    check-cast v0, Lecz;

    invoke-virtual {v0}, Lecz;->a()I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    invoke-virtual {v0, v1}, Lecz;->b(Lduk;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o()V

    :try_start_0
    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const v2, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v2, v0}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v1}, Lag;->c()I
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static varargs a(Ljava/lang/Integer;[Ljava/lang/Integer;)V
    .locals 3

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/util/HashMap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private q()V
    .locals 4

    const/4 v1, 0x1

    const-string v0, "SignInActivity"

    const-string v2, "onSignInFailed()..."

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SignInActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "==> Returning non-OK result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->G:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->d(I)V

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->G:I

    const/16 v2, 0x4e20

    if-ne v0, v2, :cond_0

    const v0, 0x7f0b0204    # com.google.android.gms.R.string.games_api_access_denied

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "SignInActivity"

    const-string v2, "No account on this device can access the Games APIs"

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x2712

    :cond_0
    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->G:I

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->G:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->G:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "SignInActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Transition from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown state to be transitioning to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->D:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    :goto_0
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->v:Z

    return-void

    :cond_0
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r:Z

    const-class v0, Lecy;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->C:Z

    if-eqz v0, :cond_3

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->C:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->z:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->E:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    goto :goto_0

    :cond_2
    const-class v0, Lecv;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->A:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    goto :goto_0

    :pswitch_2
    const-class v0, Lecu;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    :pswitch_3
    const-class v0, Ledb;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    :pswitch_4
    const-class v0, Lecw;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    :pswitch_5
    const-class v0, Lecx;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    :pswitch_6
    const-class v0, Leda;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    :pswitch_7
    const-class v0, Ledc;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    :pswitch_8
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->d(I)V

    const-string v0, "games.oob_state"

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "oob_complete"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    goto :goto_0

    :pswitch_9
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r()V

    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    invoke-virtual {v1}, Lduk;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    if-ne v1, p1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->v:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    const-string v0, "SignInActivity"

    const-string v1, "Failed to connect to sign-in service"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q()V

    return-void
.end method

.method public final b(I)V
    .locals 3

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/util/HashMap;

    iget v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid transition: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput p1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->v:Z

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s:Z

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    invoke-virtual {v0}, Lduk;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    invoke-virtual {v0}, Lduk;->a()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->z:Ljava/lang/String;

    return-void
.end method

.method public final c(I)V
    .locals 1

    iput p1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->G:I

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->B:Ljava/lang/String;

    return-void
.end method

.method public final d(I)V
    .locals 7

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->z:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->I:J

    move-object v0, p0

    move v6, p1

    invoke-static/range {v0 .. v6}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    return-void
.end method

.method public final e()Lduk;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    invoke-virtual {v0}, Lduk;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Sign-in client not connected!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    return-object v0
.end method

.method public final f()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->H:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final i()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->y:[Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->A:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->F:I

    return v0
.end method

.method public final n()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final o()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    if-ne v0, v2, :cond_2

    :cond_0
    invoke-super {p0}, Lo;->onBackPressed()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11

    const/16 v3, 0x9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040086    # com.google.android.gms.R.layout.games_signin_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setContentView(I)V

    const v0, 0x7f0a01b0    # com.google.android.gms.R.id.loading

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "SignInActivity"

    const-string v1, "SignInActivity must be started with startActivityForResult"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "SignInActivity"

    const-string v1, "You must pass a game ID in the GamesIntents.EXTRA_GAME_ID extra."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getRequestedOrientation()I

    move-result v6

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v7

    iget v8, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v9, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v0, 0x8

    invoke-static {v3}, Lbpz;->a(I)Z

    move-result v4

    if-nez v4, :cond_11

    move v0, v1

    move v3, v2

    :goto_1
    if-eqz v7, :cond_2

    const/4 v4, 0x2

    if-ne v7, v4, :cond_8

    :cond_2
    move v4, v1

    :goto_2
    if-eqz v4, :cond_3

    if-gt v8, v9, :cond_4

    :cond_3
    if-nez v4, :cond_9

    if-le v9, v8, :cond_9

    :cond_4
    move v4, v1

    :goto_3
    packed-switch v7, :pswitch_data_0

    const/4 v0, 0x5

    :cond_5
    :goto_4
    if-eq v6, v0, :cond_6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setRequestedOrientation(I)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:Ljava/lang/String;

    invoke-static {v0, v1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    const-string v1, "com.google.android.play.games"

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    if-nez v0, :cond_c

    :cond_7
    const-string v0, "SignInActivity"

    const-string v1, "Invalid (empty) game ID found in the EXTRA_GAME_ID extra."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    goto/16 :goto_0

    :cond_8
    move v4, v2

    goto :goto_2

    :cond_9
    move v4, v2

    goto :goto_3

    :pswitch_0
    if-eqz v4, :cond_a

    move v0, v1

    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_4

    :pswitch_1
    if-eqz v4, :cond_5

    move v0, v2

    goto :goto_4

    :pswitch_2
    if-nez v4, :cond_5

    move v0, v3

    goto :goto_4

    :pswitch_3
    if-eqz v4, :cond_b

    :goto_5
    move v0, v3

    goto :goto_4

    :cond_b
    move v3, v1

    goto :goto_5

    :cond_c
    const-string v0, "com.google.android.gms"

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:Ljava/lang/String;

    :cond_d
    const-string v0, "com.google.android.gms.games.USE_DESIRED_ACCOUNT_NAME"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->A:Z

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->v:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    iput v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    if-eqz p1, :cond_e

    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->z:Ljava/lang/String;

    const-string v0, "player_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->B:Ljava/lang/String;

    const-string v0, "desired_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    const-string v0, "account_selector_bypassed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->C:Z

    :cond_e
    const-string v0, "com.google.android.gms.games.SCOPES"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->y:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->y:[Ljava/lang/String;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->y:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_10

    :cond_f
    const-string v0, "SignInActivity"

    const-string v1, "Must specify at least one scope in order to sign in!"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    goto/16 :goto_0

    :cond_10
    const-string v0, "com.google.android.gms.games.SHOW_CONNECTING_POPUP"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->D:Z

    const-string v0, "com.google.android.gms.games.CONNECTING_POPUP_GRAVITY"

    const/16 v1, 0x11

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->F:I

    const-string v0, "com.google.android.gms.games.RETRYING_SIGN_IN"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->E:Z

    new-instance v0, Lduk;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->y:[Ljava/lang/String;

    move-object v1, p0

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lduk;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Lduw;Ldux;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->I:J

    goto/16 :goto_0

    :cond_11
    move v10, v3

    move v3, v0

    move v0, v10

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final onPause()V
    .locals 1

    invoke-super {p0}, Lo;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s:Z

    return-void
.end method

.method protected final onResume()V
    .locals 1

    invoke-super {p0}, Lo;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    invoke-virtual {v0}, Lduk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->v:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r()V

    :cond_0
    return-void
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "player_id"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "desired_state"

    iget v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "account_selector_bypassed"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->C:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lo;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    invoke-virtual {v0}, Lduk;->a()V

    return-void
.end method

.method public final onStop()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lo;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Lduk;

    iput-object v3, v0, Lduk;->f:Ldam;

    iget-object v1, v0, Lduk;->g:Ldvd;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lduk;->a:Landroid/content/Context;

    iget-object v2, v0, Lduk;->g:Ldvd;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v3, v0, Lduk;->g:Ldvd;

    :cond_0
    return-void
.end method

.method public final p()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r:Z

    return-void
.end method
