.class public Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;
.super Lerf;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lerv;


# instance fields
.field public final a:I

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lerv;

    invoke-direct {v0}, Lerv;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;->CREATOR:Lerv;

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    invoke-direct {p0}, Lerf;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;->a:I

    iput p2, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;->b:I

    iput p3, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;->c:I

    iput p4, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;->d:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;->b:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;->d:I

    return v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lerv;->a(Lcom/google/android/gms/location/places/internal/OpeningHoursImpl$PeriodImpl;Landroid/os/Parcel;)V

    return-void
.end method
