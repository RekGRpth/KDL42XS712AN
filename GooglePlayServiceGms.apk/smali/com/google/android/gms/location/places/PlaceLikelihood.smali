.class public Lcom/google/android/gms/location/places/PlaceLikelihood;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/location/places/internal/PlaceImpl;

.field public final c:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lerl;

    invoke-direct {v0}, Lerl;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/PlaceLikelihood;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/location/places/internal/PlaceImpl;F)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/places/PlaceLikelihood;->a:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/PlaceLikelihood;->b:Lcom/google/android/gms/location/places/internal/PlaceImpl;

    iput p3, p0, Lcom/google/android/gms/location/places/PlaceLikelihood;->c:F

    return-void
.end method

.method public static a(Lcom/google/android/gms/location/places/internal/PlaceImpl;F)Lcom/google/android/gms/location/places/PlaceLikelihood;
    .locals 3

    new-instance v1, Lcom/google/android/gms/location/places/PlaceLikelihood;

    const/4 v2, 0x0

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/PlaceImpl;

    invoke-direct {v1, v2, v0, p1}, Lcom/google/android/gms/location/places/PlaceLikelihood;-><init>(ILcom/google/android/gms/location/places/internal/PlaceImpl;F)V

    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lerl;->a(Lcom/google/android/gms/location/places/PlaceLikelihood;Landroid/os/Parcel;I)V

    return-void
.end method
