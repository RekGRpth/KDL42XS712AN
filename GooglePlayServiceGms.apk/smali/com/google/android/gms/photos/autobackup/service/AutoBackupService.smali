.class public final Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lfkh;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;)Lfkh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a:Lfkh;

    return-object v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.gms.photos.autobackup.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lfkh;

    invoke-direct {v0, p0}, Lfkh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a:Lfkh;

    new-instance v0, Lfkg;

    invoke-direct {v0, p0}, Lfkg;-><init>(Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;)V

    invoke-virtual {v0}, Lfkg;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onUnbind(Landroid/content/Intent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a:Lfkh;

    invoke-virtual {v0}, Lfkh;->d()V

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
