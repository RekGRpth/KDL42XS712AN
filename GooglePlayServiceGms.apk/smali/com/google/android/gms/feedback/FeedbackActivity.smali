.class public Lcom/google/android/gms/feedback/FeedbackActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcsx;


# static fields
.field private static o:Lcom/google/android/gms/feedback/FeedbackSession;


# instance fields
.field private p:Landroid/graphics/Bitmap;

.field private q:Landroid/graphics/Bitmap;

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->p:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->p:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private a(Landroid/content/Intent;)Lcom/google/android/gms/feedback/ErrorReport;
    .locals 2

    new-instance v1, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v1}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    const-string v0, "android.intent.extra.BUG_REPORT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.extra.BUG_REPORT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/ApplicationErrorReport;

    iput-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    :cond_0
    const-string v0, "com.android.feedback.SAFEPARCELABLE_REPORT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "com.android.feedback.SAFEPARCELABLE_REPORT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/ErrorReport;->a([B)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->r:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Landroid/os/Parcelable;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 13

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const v0, 0x7f0a0116    # com.google.android.gms.R.id.gf_issue_description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-static {p2}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v3, p2, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v3, v3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    if-eqz p1, :cond_7

    check-cast p1, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    move-object v0, p1

    :goto_0
    iget-object v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    if-eqz v2, :cond_0

    iget-object v0, p2, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    iget v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    iget v5, p2, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    invoke-static {v0, v2, v5}, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a([BII)Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    move-result-object v0

    :cond_0
    iget-object v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    iput-object v3, v2, Landroid/app/ApplicationErrorReport;->processName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    :cond_1
    if-nez v2, :cond_5

    new-instance v0, Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/feedback/FeedbackSession;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;)V

    :goto_1
    sput-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->c()V

    const v0, 0x7f0a0115    # com.google.android.gms.R.id.gf_account_spinner

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    sget-object v2, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v2}, Lcom/google/android/gms/feedback/FeedbackSession;->a()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v7, v2, [Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v2}, Lcom/google/android/gms/feedback/FeedbackSession;->a()[Ljava/lang/String;

    move-result-object v8

    array-length v9, v8

    move v5, v1

    move v3, v1

    move v2, v1

    move v1, v4

    :goto_2
    if-ge v5, v9, :cond_6

    aget-object v6, v8, v5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0b00e4    # com.google.android.gms.R.string.gf_from

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v2

    if-nez v3, :cond_2

    const-string v10, "google.com"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    move v1, v2

    :cond_2
    iget-object v10, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->r:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    iget-object v10, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->r:Ljava/lang/String;

    invoke-static {v6, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v1, v2

    move v3, v4

    :cond_3
    add-int/lit8 v6, v2, 0x1

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v6

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    :cond_4
    :goto_3
    return-void

    :cond_5
    new-instance v0, Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-direct {v0, p0, p2, v2}, Lcom/google/android/gms/feedback/FeedbackSession;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V

    goto :goto_1

    :cond_6
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x7f04009c    # com.google.android.gms.R.layout.gf_account_spinner

    invoke-direct {v2, p0, v3, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v3, 0x1090009    # android.R.layout.simple_spinner_dropdown_item

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    array-length v2, v7

    if-le v2, v4, :cond_4

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_3

    :cond_7
    move-object v0, v2

    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/gms/feedback/FeedbackSession;)Z
    .locals 1

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->q:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->q:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private static b(Lcom/google/android/gms/feedback/ErrorReport;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v1, v1, Landroid/app/ApplicationErrorReport;->type:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static g()Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h()Lcom/google/android/gms/feedback/FeedbackSession;
    .locals 1

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    new-instance v0, Lcsg;

    invoke-direct {v0, p0, p1}, Lcsg;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method final a(Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcsn;

    invoke-direct {v0, p0}, Lcsn;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcse;

    invoke-direct {v1, p0, v0, p1, p0}, Lcse;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcsn;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackActivity;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcsp;

    invoke-direct {v0, p0, p1}, Lcsp;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;)V

    invoke-virtual {v0}, Lcsp;->start()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V
    .locals 12

    const v5, 0x7f0a0118    # com.google.android.gms.R.id.gf_screenshot_preview

    const/16 v11, 0x21

    const/16 v4, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    const v0, 0x7f0a011a    # com.google.android.gms.R.id.gf_screenshot_icon

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    const v0, 0x7f0a0119    # com.google.android.gms.R.id.gf_progress_spinner

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    invoke-virtual {p0, v5}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v0, 0x7f0a011b    # com.google.android.gms.R.id.gf_touch_to_preview

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v0, 0x7f0a0117    # com.google.android.gms.R.id.gf_include_pii

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-nez p2, :cond_0

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    if-eqz v1, :cond_4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xd

    if-lt v4, v5, :cond_3

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v1, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v6, v4, Landroid/graphics/Point;->x:I

    :goto_0
    const v1, 0x7f0d0015    # com.google.android.gms.R.dimen.preview_height

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setMaxHeight(I)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0097    # com.google.android.gms.R.string.gf_include_private_data

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    new-instance v0, Lcsc;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v8}, Lcsc;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/widget/ProgressBar;Landroid/widget/ImageView;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;ILandroid/view/View;Landroid/widget/TextView;)V

    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0a011c    # com.google.android.gms.R.id.gf_preview_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<a href="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v4, 0x7f0b009f    # com.google.android.gms.R.string.gf_privacy_link

    new-array v5, v10, [Ljava/lang/Object;

    aput-object v2, v5, v9

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0b009d    # com.google.android.gms.R.string.gf_privacy_policy

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</a>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<a href="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v5, 0x7f0b00a0    # com.google.android.gms.R.string.gf_tos_link

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v2, v6, v9

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v4, 0x7f0b009e    # com.google.android.gms.R.string.gf_tos

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "</a>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f0b009c    # com.google.android.gms.R.string.gf_product_information

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0b009b    # com.google.android.gms.R.string.gf_email_address

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0b00a1    # com.google.android.gms.R.string.gf_new_policy

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v9

    aput-object v5, v7, v10

    const/4 v5, 0x2

    aput-object v3, v7, v5

    const/4 v3, 0x3

    aput-object v2, v7, v3

    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLinksClickable(Z)V

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    new-instance v2, Lcsd;

    invoke-direct {v2, p0, p0}, Lcsd;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/app/Activity;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcsh;

    invoke-direct {v5, p0, v2}, Lcsh;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcsi;)V

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    instance-of v4, v1, Landroid/text/Spannable;

    if-eqz v4, :cond_5

    check-cast v1, Landroid/text/Spannable;

    invoke-interface {v1, v5, v2, v3, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :goto_2
    invoke-virtual {v0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v1

    if-eqz v1, :cond_1

    instance-of v1, v1, Landroid/text/method/LinkMovementMethod;

    if-nez v1, :cond_2

    :cond_1
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v6

    goto/16 :goto_0

    :cond_4
    const v1, 0x7f0b0098    # com.google.android.gms.R.string.gf_include_private_data_logs_only

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f0a011b    # com.google.android.gms.R.id.gf_touch_to_preview

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v5}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v1, v5, v2, v3, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljj;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p2}, Ljj;->b(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljj;->a(Z)V

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3}, Ljj;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/feedback/FeedbackSession;->a([Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->g()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->i_()V

    :cond_0
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    return-void
.end method

.method public final b(Z)V
    .locals 5

    const/4 v2, 0x4

    const/4 v1, 0x0

    const v0, 0x7f0a011d    # com.google.android.gms.R.id.gf_progress

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a0114    # com.google.android.gms.R.id.gf_feedback_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    return-void
.end method

.method public final i()V
    .locals 1

    new-instance v0, Lcsf;

    invoke-direct {v0, p0}, Lcsf;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a011a    # com.google.android.gms.R.id.gf_screenshot_icon

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/feedback/PreviewScreenshotActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->p:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    if-lez v1, :cond_0

    const-string v1, "com.android.feedback.SCREENSHOT_EXTRA"

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->p:Landroid/graphics/Bitmap;

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {v2, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Ljp;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->i_()V

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const v1, 0x7f0a0116    # com.google.android.gms.R.id.gf_issue_description

    invoke-virtual {p0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcst;->a(Lcsx;)V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/content/Intent;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    const v2, 0x7f040055    # com.google.android.gms.R.layout.feedback_ui

    invoke-virtual {p0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->setContentView(I)V

    if-nez p1, :cond_0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/os/Parcelable;Lcom/google/android/gms/feedback/ErrorReport;)V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120015    # com.google.android.gms.R.menu.menu

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Ljp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcst;->a(Lcsx;)V

    invoke-super {p0}, Ljp;->onDestroy()V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    if-nez p3, :cond_0

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    const-string v1, ""

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->a()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p3

    sget-object v1, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v1}, Lcom/google/android/gms/feedback/FeedbackSession;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Ljp;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->g()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b00a3    # com.google.android.gms.R.string.gf_ok

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0095    # com.google.android.gms.R.string.gf_invalid_description_text

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->p:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->q:Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/content/Intent;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-super {p0, p1}, Ljp;->onNewIntent(Landroid/content/Intent;)V

    const-string v1, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/os/Parcelable;Lcom/google/android/gms/feedback/ErrorReport;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Ljp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    sget-object v1, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v1}, Lcom/google/android/gms/feedback/FeedbackSession;->b()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1    # android.R.id.home
        0x7f0a038d -> :sswitch_0    # com.google.android.gms.R.id.gf_action_send
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->g()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0116    # com.google.android.gms.R.id.gf_issue_description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Ljp;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const v0, 0x7f0a0116    # com.google.android.gms.R.id.gf_issue_description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    new-instance v0, Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/feedback/FeedbackSession;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/os/Bundle;)V

    sput-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->c()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Ljp;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    const v0, 0x7f0a0116    # com.google.android.gms.R.id.gf_issue_description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocusFromTouch()Z

    iget v2, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v1, 0xf

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Ljp;->onSaveInstanceState(Landroid/os/Bundle;)V

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/feedback/FeedbackSession;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Ljp;->onStart()V

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->c()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Ljp;->onStop()V

    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->o:Lcom/google/android/gms/feedback/FeedbackSession;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->d()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
