.class public Lcom/google/android/gms/feedback/ShowTextActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    const v5, 0x7f0a02e1    # com.google.android.gms.R.id.text_in_list_view

    const v4, 0x7f0a024c    # com.google.android.gms.R.id.text_view

    const v3, 0x7f0a014f    # com.google.android.gms.R.id.empty_view

    const/4 v2, 0x0

    const/16 v1, 0x8

    if-nez p1, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v5}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0a0162    # com.google.android.gms.R.id.text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v5}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a([Ljava/lang/String;)V
    .locals 6

    const v5, 0x7f0a02e1    # com.google.android.gms.R.id.text_in_list_view

    const v4, 0x7f0a024c    # com.google.android.gms.R.id.text_view

    const v2, 0x7f0a014f    # com.google.android.gms.R.id.empty_view

    const/4 v3, 0x0

    const/16 v1, 0x8

    array-length v0, p1

    if-nez v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v5}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v5}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x1090003    # android.R.layout.simple_list_item_1

    invoke-direct {v1, p0, v2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04011a    # com.google.android.gms.R.layout.show_text_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/ShowTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feedback.FIELD_NAME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->g()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/ShowTextActivity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/ShowTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "feedback.FIELD_VALUE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "running applications"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a([Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v2, "system logs"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a([Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "event logs"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a([Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v2, "stack trace"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v2, "anr info"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$AnrInfo;->info:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string v2, "anr stack trace"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    const-string v2, "battery usage details"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->usageDetails:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_9
    const-string v2, "battery checkin details"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->checkinDetails:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v2, "running service details"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
