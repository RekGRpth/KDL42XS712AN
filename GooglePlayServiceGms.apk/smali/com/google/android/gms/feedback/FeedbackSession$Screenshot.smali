.class public Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcsm;

    invoke-direct {v0}, Lcsm;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BZ)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v0, 0x1

    const/4 v6, 0x0

    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    array-length v1, p0

    invoke-static {p0, v6, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v3, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/lit8 v3, v3, 0x2

    if-eqz p1, :cond_1

    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gt v4, v3, :cond_0

    if-le v5, v1, :cond_1

    :cond_0
    int-to-float v0, v4

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v3, v5

    int-to-float v1, v1

    div-float v1, v3, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    if-le v0, v1, :cond_2

    :cond_1
    :goto_0
    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iput-boolean v6, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    array-length v0, p0

    invoke-static {p0, v6, v0, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;
    .locals 3

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x46

    invoke-virtual {p0, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a([BII)Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Parcel;)Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->c:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    iput v2, v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a:I

    iput v2, v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->b:I

    goto :goto_0
.end method

.method public static a([BII)Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;
    .locals 2

    new-instance v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;-><init>()V

    iput p1, v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a:I

    iput p2, v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->b:I

    const/4 v1, 0x0

    invoke-static {p0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
