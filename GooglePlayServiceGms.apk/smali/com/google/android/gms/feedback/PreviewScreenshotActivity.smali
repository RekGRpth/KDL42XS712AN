.class public Lcom/google/android/gms/feedback/PreviewScreenshotActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/high16 v8, 0x3fc00000    # 1.5f

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v7}, Lcom/google/android/gms/feedback/PreviewScreenshotActivity;->requestWindowFeature(I)Z

    const v0, 0x7f040115    # com.google.android.gms.R.layout.preview_ui

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/PreviewScreenshotActivity;->setContentView(I)V

    const v0, 0x7f0a02dc    # com.google.android.gms.R.id.gf_screenshot_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/PreviewScreenshotActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/PreviewScreenshotActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_2

    array-length v2, v1

    if-eqz v2, :cond_2

    array-length v2, v1

    invoke-static {v1, v6, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/PreviewScreenshotActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xd

    if-lt v4, v5, :cond_1

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v3, v4, Landroid/graphics/Point;->x:I

    aput v3, v2, v6

    iget v3, v4, Landroid/graphics/Point;->y:I

    aput v3, v2, v7

    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v8

    float-to-int v3, v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v8

    float-to-int v4, v4

    aget v5, v2, v6

    if-ge v3, v5, :cond_0

    aget v2, v2, v7

    if-ge v4, v2, :cond_0

    invoke-static {v1, v3, v4, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v4

    aput v4, v2, v6

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v3

    aput v3, v2, v7

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/PreviewScreenshotActivity;->finish()V

    goto :goto_1
.end method
