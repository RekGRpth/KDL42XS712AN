.class public final Lcom/google/android/gms/feedback/FeedbackSession;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private a:Lcom/google/android/gms/feedback/ErrorReport;

.field private b:[Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:[Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

.field private h:Lcom/google/android/gms/feedback/FeedbackActivity;

.field private i:Landroid/os/Handler;

.field private j:Z

.field private k:Z

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const-string v0, "feedback.REPORT"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    const-string v0, "feedback.SYSTEM_LOG"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->b:[Ljava/lang/String;

    const-string v0, "feedback.EVENT_LOG"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->c:[Ljava/lang/String;

    const-string v0, "feedback.RUNNING_APPS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->d:[Ljava/lang/String;

    const-string v0, "feedback.GET_SYSTEM_LOG_THREAD_DONE"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->j:Z

    const-string v0, "feedback.SEND_PRIVATE_DATA"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    const-string v0, "feedback.ANR_STACK_TRACES_KEY"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->f:Ljava/lang/String;

    const-string v0, "feedback.SCREENSHOT_KEY"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->g:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    const-string v0, "feedback.FOUND_ACCOUNTS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->e:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->g()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/feedback/FeedbackSession;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    new-instance v0, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0, p2}, Lcom/google/android/gms/feedback/ErrorReport;-><init>(Lcom/google/android/gms/feedback/ErrorReport;)V

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->h()V

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->g()V

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->f:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->g:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->h:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->i:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->j:Ljava/lang/String;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->l:I

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->m:Ljava/lang/String;

    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->n:Ljava/lang/String;

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->o:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->BOARD:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->p:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->q:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->k:Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->d:I

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->e:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/feedback/ErrorReport;->z:I

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->A:I

    iput-object p3, p0, Lcom/google/android/gms/feedback/FeedbackSession;->g:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->i:Landroid/os/Handler;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget v0, p1, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->b:I

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    iget v0, p1, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a:I

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    :goto_0
    return-void

    :cond_0
    iput v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    iput v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    goto :goto_0
.end method

.method private a(Z)V
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackSession;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Z)V

    goto :goto_0
.end method

.method private f()[Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const v2, 0x7f0b00ac    # com.google.android.gms.R.string.gf_anonymous

    invoke-virtual {v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-le v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const-string v4, "user"

    invoke-virtual {v0, v4}, Lcom/google/android/gms/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserRestrictions()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "no_modify_accounts"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v4, "com.google"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/VerifyError; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object v2, v4, v1

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    aput-object v0, v4, v1

    goto :goto_3

    :cond_2
    return-object v4

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private g()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, v2, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v3, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->n:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->m:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->m:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a8    # com.google.android.gms.R.string.gf_unknown_app

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->m:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->n:Ljava/lang/String;

    const-string v1, "v"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->m:Ljava/lang/String;

    :goto_2
    return-void

    :cond_2
    :try_start_1
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->n:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "unknown"

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->n:Ljava/lang/String;

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " v"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->m:Ljava/lang/String;

    goto :goto_2
.end method

.method private h()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->d:[Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v4, p0, Lcom/google/android/gms/feedback/FeedbackSession;->d:[Ljava/lang/String;

    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    aput-object v0, v4, v1

    move v1, v2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v0, v0, Landroid/app/ApplicationErrorReport;->type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackSession;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->f:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private static i()Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileReader;

    const-string v2, "/data/anr/traces.txt"

    invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v2

    :goto_1
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    :cond_0
    :goto_2
    return-object v0

    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    move-object v1, v0

    :goto_3
    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_2

    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_4
    if-eqz v1, :cond_2

    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_2
    :goto_5
    throw v0

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v2

    goto :goto_3

    :catch_7
    move-exception v1

    move-object v1, v0

    goto :goto_1
.end method

.method private j()V
    .locals 4

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackSession;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/ApplicationErrorReport;->time:J

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->D:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const v2, 0x7f0a0116    # com.google.android.gms.R.id.gf_issue_description

    invoke-virtual {v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const v2, 0x7f0a0117    # com.google.android.gms.R.id.gf_include_pii

    invoke-virtual {v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->g:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    invoke-static {v0, v2}, Lcom/google/android/gms/feedback/FeedbackSession;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->b:[Ljava/lang/String;

    :goto_2
    iput-object v0, v2, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->c:[Ljava/lang/String;

    :goto_3
    iput-object v0, v2, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->d:[Ljava/lang/String;

    :goto_4
    iput-object v0, v2, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->f:Ljava/lang/String;

    :cond_1
    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {v0, v1}, Lcom/google/android/gms/feedback/FeedbackSession;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_4
.end method


# virtual methods
.method final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->j()V

    const-string v0, "feedback.REPORT"

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "feedback.SYSTEM_LOG"

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->b:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "feedback.EVENT_LOG"

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->c:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "feedback.RUNNING_APPS"

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->d:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "feedback.GET_SYSTEM_LOG_THREAD_DONE"

    iget-boolean v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "feedback.SEND_PRIVATE_DATA"

    iget-boolean v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "feedback.ANR_STACK_TRACES_KEY"

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "feedback.SCREENSHOT_KEY"

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->g:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "feedback.FOUND_ACCOUNTS"

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->e:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->b:[Ljava/lang/String;

    :cond_0
    if-eqz p2, :cond_1

    iput-object p2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->c:[Ljava/lang/String;

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->j:Z

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->j()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/FeedbackSession;->a(Z)V

    return-void
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->e:[Ljava/lang/String;

    return-object v0
.end method

.method public final b()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->j()V

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/ErrorReport;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->f()V

    return-void
.end method

.method public final c()V
    .locals 4

    invoke-static {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackSession;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const v2, 0x7f0a0116    # com.google.android.gms.R.id.gf_issue_description

    invoke-virtual {v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, v2, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const v2, 0x7f0a0117    # com.google.android.gms.R.id.gf_include_pii

    invoke-virtual {v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-boolean v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->k:Z

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, p0, Lcom/google/android/gms/feedback/FeedbackSession;->g:Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->h:Lcom/google/android/gms/feedback/FeedbackActivity;

    const v2, 0x7f0b0094    # com.google.android.gms.R.string.gf_report_feedback

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/feedback/FeedbackSession;->m:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/feedback/FeedbackSession;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->f()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->e:[Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->j:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/FeedbackSession;->a(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->j:Z

    if-nez v0, :cond_1

    invoke-static {}, Lcst;->a()V

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->i:Landroid/os/Handler;

    new-instance v1, Lcsl;

    invoke-direct {v1, p0}, Lcsl;-><init>(Lcom/google/android/gms/feedback/FeedbackSession;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->j()V

    invoke-static {}, Lcst;->b()V

    return-void
.end method

.method public final e()Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    return-object v0
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackSession;->j()V

    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackSession;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackSession;->e:[Ljava/lang/String;

    aget-object v1, v1, p2

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    goto :goto_0
.end method
