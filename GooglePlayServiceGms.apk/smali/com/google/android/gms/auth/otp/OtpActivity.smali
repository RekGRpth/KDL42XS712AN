.class public Lcom/google/android/gms/auth/otp/OtpActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lavn;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/Spinner;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/Button;

.field private j:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/auth/otp/OtpActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->j:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/auth/otp/OtpActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->j:Landroid/view/View;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->b:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->i:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->b:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->c:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method public final a([Landroid/accounts/Account;Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 4

    const/4 v0, 0x0

    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/otp/OtpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b05a8    # com.google.android.gms.R.string.auth_otp_select_account_message

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    add-int/lit8 v2, v0, 0x1

    aget-object v3, p1, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lavb;

    invoke-direct {v0, p0, p0, v1}, Lavb;-><init>(Lcom/google/android/gms/auth/otp/OtpActivity;Landroid/content/Context;[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->f:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->f:Landroid/widget/Spinner;

    new-instance v1, Lavc;

    invoke-direct {v1, p0, p2}, Lavc;-><init>(Lcom/google/android/gms/auth/otp/OtpActivity;Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->f:Landroid/widget/Spinner;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->e:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0b05a6    # com.google.android.gms.R.string.auth_otp_dialog_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->setTitle(I)V

    const v0, 0x7f0400b5    # com.google.android.gms.R.layout.otp_dialog

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->setContentView(I)V

    const v0, 0x7f0a0240    # com.google.android.gms.R.id.otp_account_picker_spinner

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->f:Landroid/widget/Spinner;

    const v0, 0x7f0a0241    # com.google.android.gms.R.id.otp_wheel

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->b:Landroid/view/View;

    const v0, 0x7f0a0243    # com.google.android.gms.R.id.otp_results

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->c:Landroid/view/View;

    const v0, 0x7f0a0242    # com.google.android.gms.R.id.otp_error

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->e:Landroid/view/View;

    const v0, 0x7f0a0247    # com.google.android.gms.R.id.otp_no_accounts_available

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->d:Landroid/view/View;

    const v0, 0x7f0a0244    # com.google.android.gms.R.id.otp_account_name

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->g:Landroid/widget/TextView;

    const v0, 0x7f0a0245    # com.google.android.gms.R.id.otp_code

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->h:Landroid/widget/TextView;

    const v0, 0x7f0a0248    # com.google.android.gms.R.id.otp_done_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/otp/OtpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->i:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/otp/OtpActivity;->i:Landroid/widget/Button;

    new-instance v1, Lava;

    invoke-direct {v1, p0}, Lava;-><init>(Lcom/google/android/gms/auth/otp/OtpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lavd;

    new-instance v1, Lavm;

    new-instance v2, Lauy;

    invoke-direct {v2, p0}, Lauy;-><init>(Landroid/content/Context;)V

    new-instance v3, Lavk;

    invoke-direct {v3, p0}, Lavk;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v2, v3}, Lavm;-><init>(Lavj;Lauz;)V

    invoke-direct {v0, p0, p0, v1}, Lavd;-><init>(Landroid/content/Context;Lavn;Lavh;)V

    iget-object v1, v0, Lavd;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    if-nez v2, :cond_0

    iget-object v0, v0, Lavd;->b:Lavn;

    invoke-interface {v0}, Lavn;->c()V

    :goto_0
    return-void

    :cond_0
    array-length v2, v1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lavd;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lavd;->b:Lavn;

    new-instance v3, Lave;

    invoke-direct {v3, v0, v1}, Lave;-><init>(Lavd;[Landroid/accounts/Account;)V

    invoke-interface {v2, v1, v3}, Lavn;->a([Landroid/accounts/Account;Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0
.end method
