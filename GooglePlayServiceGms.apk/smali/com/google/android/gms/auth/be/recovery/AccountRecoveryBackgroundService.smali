.class public Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "AccountRecoveryBackgroundService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private static a()J
    .locals 7

    invoke-static {}, Lapz;->a()Lapz;

    move-result-object v0

    iget-object v1, v0, Lapz;->a:Landroid/content/Context;

    const-string v2, "auth_recovery_state"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    iget-object v1, v0, Lapz;->a:Landroid/content/Context;

    const-string v2, "account_recovery_silence_default_ms"

    const-wide/32 v3, 0x240c8400

    invoke-static {v1, v2, v3, v4}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v1

    iget-object v0, v0, Lapz;->a:Landroid/content/Context;

    const-string v3, "account_recovery_silence_randomize_ms"

    const-wide/32 v4, 0x48190800

    invoke-static {v0, v3, v4, v5}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Laqb;

    invoke-direct {v0, v1, v2, v3, v4}, Laqb;-><init>(JJ)V

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, v0, Laqb;->a:J

    add-long/2addr v3, v5

    iget-wide v5, v0, Laqb;->b:J

    long-to-double v5, v5

    mul-double v0, v5, v1

    double-to-long v0, v0

    add-long/2addr v0, v3

    return-wide v0
.end method

.method private a(J)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.recovery.WAKEUP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9

    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x2

    const-string v0, "Recovery"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Recovery"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Action: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lapz;->a()Lapz;

    move-result-object v3

    new-instance v0, Lbhv;

    invoke-direct {v0, p0}, Lbhv;-><init>(Landroid/content/Context;)V

    const-string v1, "account_recovery_enabled"

    invoke-virtual {v0, v1}, Lbhv;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v1, "Recovery"

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Recovery"

    const-string v4, "Account recovery is disabled"

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    new-instance v2, Lbhv;

    invoke-direct {v2, p0}, Lbhv;-><init>(Landroid/content/Context;)V

    const-string v4, "account_recovery_disabled_alarm_period_ms"

    invoke-virtual {v2, v4}, Lbhv;->b(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-double v4, v4

    mul-double/2addr v0, v4

    double-to-long v0, v0

    add-long/2addr v0, v6

    invoke-virtual {v3, v0, v1}, Lapz;->a(J)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;->a(J)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Recovery"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Recovery"

    const-string v1, "Account recovery is disabled for Restricted User profile."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v0, "com.google.android.gms.recovery.WAKEUP"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lapz;->a()Lapz;

    move-result-object v4

    invoke-virtual {v4}, Lapz;->b()J

    move-result-wide v0

    cmp-long v5, v0, v6

    if-gez v5, :cond_5

    invoke-static {}, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;->a()J

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Lapz;->a(J)V

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v4, v0

    if-ltz v0, :cond_7

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_8

    invoke-virtual {v3}, Lapz;->b()J

    move-result-wide v0

    cmp-long v2, v0, v6

    if-gez v2, :cond_6

    const-string v2, "Recovery"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Illegal next alarm time: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;->a()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Lapz;->a(J)V

    :cond_6
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;->a(J)V

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    invoke-static {}, Lapr;->a()Lapr;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    :try_start_0
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lapz;->a(Ljava/lang/String;)Laqa;

    move-result-object v1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v7, "background_service"

    const/4 v8, 0x0

    invoke-virtual {v2, v6, v7, v8, v4}, Lapr;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lapw;

    move-result-object v6

    iget-object v7, v6, Lapw;->j:Ljava/lang/String;

    if-eqz v7, :cond_9

    const-string v1, "Recovery"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Error while talking to server: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v6, Lapw;->j:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    const-string v6, "Recovery"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Error while checking recovery info for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_9
    :try_start_1
    iget-boolean v7, v6, Lapw;->a:Z

    iput-boolean v7, v1, Laqa;->c:Z

    iget-boolean v7, v6, Lapw;->c:Z

    iput-boolean v7, v1, Laqa;->d:Z

    iget-boolean v6, v6, Lapw;->b:Z

    iput-boolean v6, v1, Laqa;->b:Z

    invoke-static {}, Lapz;->a()Lapz;

    move-result-object v6

    invoke-virtual {v6, v1}, Lapz;->a(Laqa;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_a
    invoke-static {}, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;->a()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Lapz;->a(J)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;->a(J)V

    goto/16 :goto_0
.end method
