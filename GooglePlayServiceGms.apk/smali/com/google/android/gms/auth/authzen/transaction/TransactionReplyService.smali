.class public Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "TransactionReplyService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 3

    invoke-static {p0}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "reply"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 12

    const-string v0, "AuthZen"

    const-string v1, "Sending authzen reply..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-class v2, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    const-wide/16 v0, 0xbb8

    :try_start_0
    invoke-virtual {v8, v0, v1}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v0, "reply"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v10

    new-instance v11, Lasp;

    new-instance v0, Lbmi;

    invoke-static {v1}, Lanh;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cryptauth/v1/"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v11, v0}, Lasp;-><init>(Lbmi;)V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    int-to-long v3, v2

    const-wide/16 v5, 0x3

    cmp-long v0, v3, v5

    if-gez v0, :cond_2

    if-eqz v2, :cond_0

    const-string v0, "AuthZen"

    const-string v3, "attempting send operation again"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :try_start_1
    new-instance v3, Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v9, v9, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1}, Lanh;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v10}, Lbpd;->c([B)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Latb;

    invoke-direct {v4}, Latb;-><init>()V

    iput-object v0, v4, Latb;->a:Ljava/lang/String;

    iget-object v0, v4, Latb;->b:Ljava/util/Set;

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ReplyTxRequestEntity;

    iget-object v5, v4, Latb;->b:Ljava/util/Set;

    iget-object v4, v4, Latb;->a:Ljava/lang/String;

    invoke-direct {v0, v5, v4}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ReplyTxRequestEntity;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ReplyTxRequestEntity;

    const-string v4, "authzen/replytx"

    iget-object v5, v11, Lasp;->a:Lbmi;

    const/4 v6, 0x1

    invoke-virtual {v5, v3, v6, v4, v0}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    const-string v0, "AuthZen"

    const-string v3, "reply sent."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_1
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v3, "AuthZen"

    const-string v4, "Error"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    const-wide/16 v3, 0x1f4

    :try_start_3
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v3, "AuthZen"

    const-string v4, "Error"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    :cond_2
    int-to-long v0, v2

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const v0, 0x7f0b0588    # com.google.android.gms.R.string.auth_authzen_sending_reply_failed_text

    const/4 v1, 0x1

    :try_start_5
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_3
.end method
