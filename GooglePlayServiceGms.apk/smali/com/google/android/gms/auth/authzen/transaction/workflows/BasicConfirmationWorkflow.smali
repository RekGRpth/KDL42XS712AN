.class public Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;
.super Lanz;
.source "SourceFile"


# instance fields
.field private t:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lanz;-><init>()V

    return-void
.end method

.method public static a(Ljby;)Z
    .locals 5

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljby;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljby;->c:Ljbx;

    iget-boolean v1, v1, Ljbx;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljby;->c:Ljbx;

    iget-object v1, v1, Ljbx;->d:Ljbn;

    iget-boolean v1, v1, Ljbn;->b:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    invoke-static {p0}, Laos;->a(Ljby;)Laor;

    move-result-object v1

    invoke-interface {v1}, Laor;->b()Landroid/os/Bundle;
    :try_end_0
    .catch Laot; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v1, p0, Ljby;->c:Ljbx;

    iget-object v1, v1, Ljbx;->d:Ljbn;

    iget v1, v1, Ljbn;->c:I

    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error while creating TextProvider for UseCase: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static b(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 3

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->a(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public final a(Laoj;I)V
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->a(I)V

    :goto_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->finish()V

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->a(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lanz;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->g()Ljby;

    move-result-object v0

    invoke-static {v0}, Laos;->a(Ljby;)Laor;

    move-result-object v0

    invoke-interface {v0}, Laor;->b()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->t:Landroid/os/Bundle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->t:Landroid/os/Bundle;

    invoke-static {v1}, Laoh;->c(Landroid/os/Bundle;)Laoh;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->a(Laoj;Laoj;)V

    :cond_0
    return-void
.end method
