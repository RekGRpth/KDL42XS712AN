.class public Lcom/google/android/gms/auth/login/LoginActivity;
.super Latw;
.source "SourceFile"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

.field private C:Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

.field private D:Z

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

.field private J:Landroid/os/Bundle;

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Landroid/content/Intent;

.field private O:Z

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Laui;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Latw;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    if-eqz p0, :cond_0

    invoke-static {p0}, Lapc;->a(Landroid/content/Intent;)Laso;

    move-result-object v1

    invoke-static {v1}, Lapc;->a(Laso;)Lapc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lapc;->b(Landroid/content/Intent;)V

    const-string v1, "authtoken"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "authtoken"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/auth/login/LoginActivity;)Laui;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->R:Laui;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "calling_app"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->I:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    const-string v0, "suppress_progress_screen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->D:Z

    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    const-string v0, "service"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->E:Ljava/lang/String;

    const-string v0, "options"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->J:Landroid/os/Bundle;

    const-string v0, "pacl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->C:Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    const-string v0, "facl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->B:Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    const-string v0, "backup"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->O:Z

    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->P:Ljava/lang/String;

    const-string v0, "title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->Q:Ljava/lang/String;

    const-string v0, "confirmCredentials"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->K:Z

    const-string v0, "response_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->N:Landroid/content/Intent;

    return-void
.end method

.method private a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Laro;)V
    .locals 7

    const-string v0, "Calling app cannot be null!"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->I:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->E:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->B:Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->C:Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->J:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->L:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->M:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->I:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v2

    if-eqz p2, :cond_0

    invoke-virtual {v2, p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Laro;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->D:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->R:Laui;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->R:Laui;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laui;->cancel(Z)Z

    :cond_1
    new-instance v0, Laui;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->G:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->H:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/auth/login/LoginActivity;->O:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Laui;-><init>(Lcom/google/android/gms/auth/login/LoginActivity;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->R:Laui;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->R:Laui;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Laui;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->G:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->H:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/auth/login/LoginActivity;->O:Z

    iget-object v6, p0, Lcom/google/android/gms/auth/login/LoginActivity;->Q:Ljava/lang/String;

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/google/android/gms/auth/login/LoginActivityTask;->a(Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Laso;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->L:Z

    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->M:Z

    invoke-static {v0, p1, p2, v1, v2}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/String;Ljava/lang/String;Laso;ZZ)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3f1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 5

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->N:Landroid/content/Intent;

    invoke-static {p1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Landroid/content/Intent;)Laug;

    move-result-object v2

    iget-object v0, v2, Laug;->a:Laso;

    sget-object v3, Laso;->a:Laso;

    if-ne v0, v3, :cond_2

    iget-object v0, v2, Laug;->c:Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->B:Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    iget-object v0, v2, Laug;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->C:Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->C:Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v3, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    iget-object v4, v2, Laug;->b:Ljava/lang/String;

    invoke-direct {v3, v0, v4}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->C:Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    :goto_1
    iget-object v0, v2, Laug;->d:Laro;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Laro;)V

    :goto_2
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iput-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->C:Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    goto :goto_1

    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "errorCode"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "errorMessage"

    invoke-virtual {v0}, Laso;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0}, Lapc;->a(Laso;)Lapc;

    move-result-object v0

    invoke-virtual {v0, v1}, Lapc;->b(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-static {v1}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto :goto_2
.end method

.method private i()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->P:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->j()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lauw;

    invoke-direct {v0, p0}, Lauw;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    iget-object v2, v0, Lauw;->a:Landroid/content/Intent;

    const-string v3, "account_name"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->K:Z

    iget-object v2, v0, Lauw;->a:Landroid/content/Intent;

    const-string v3, "is_confirming_credentials"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->M:Z

    iget-object v2, v0, Lauw;->a:Landroid/content/Intent;

    const-string v3, "is_adding_account"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v1, Landroid/content/Intent;

    iget-object v0, v0, Lauw;->a:Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/16 v0, 0x402

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private j()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->P:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->H:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->M:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3ec

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    const/4 v9, 0x6

    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v7, -0x1

    const/4 v2, 0x1

    invoke-super {p0, p1, p2, p3}, Latw;->onActivityResult(IILandroid/content/Intent;)V

    const-string v0, "GLSActivity"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    const-string v0, "NO_INTENT"

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->x:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " #onActivityResult(requestCode: %s, resultCode %s, )"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GLSActivity"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    aput-object v0, v5, v8

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-ne p2, v2, :cond_3

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "NO_EXTRAS"

    goto :goto_0

    :cond_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-nez p2, :cond_5

    invoke-static {p3}, Lapc;->a(Landroid/content/Intent;)Laso;

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->M:Z

    if-eqz v0, :cond_4

    packed-switch p1, :pswitch_data_0

    :cond_4
    :pswitch_0
    invoke-static {p3}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->i()V

    goto :goto_1

    :cond_5
    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3}, Latw;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1

    :sswitch_0
    if-ne p2, v9, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->j()V

    goto :goto_1

    :cond_6
    invoke-static {p3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/content/Intent;)Laux;

    move-result-object v0

    iget-object v1, v0, Laux;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    iget-object v0, v0, Laux;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->G:Ljava/lang/String;

    :sswitch_1
    if-ne p2, v7, :cond_7

    invoke-static {p3}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->H:Ljava/lang/String;

    :cond_7
    :sswitch_2
    if-ne p2, v7, :cond_9

    invoke-static {p3}, Lcom/google/android/gms/auth/login/CaptchaActivity;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->A:Ljava/lang/String;

    if-eqz v2, :cond_8

    if-eqz v1, :cond_8

    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->A:Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Laro;)V

    goto :goto_1

    :cond_9
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto :goto_1

    :sswitch_3
    invoke-direct {p0, p3}, Lcom/google/android/gms/auth/login/LoginActivity;->b(Landroid/content/Intent;)V

    goto :goto_1

    :sswitch_4
    invoke-static {p3}, Laut;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    sget-object v3, Laso;->a:Laso;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v4

    if-ne v3, v4, :cond_c

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    if-nez v4, :cond_a

    sget-object v0, Laso;->a:Laso;

    if-ne v3, v0, :cond_b

    :cond_a
    move v0, v2

    :goto_2
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {v3}, Lapc;->a(Laso;)Lapc;

    move-result-object v0

    invoke-virtual {v0, v6}, Lapc;->b(Landroid/content/Intent;)V

    const-string v0, "authtoken"

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "accountType"

    const-string v1, "com.google"

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "authAccount"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v7, v6}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto/16 :goto_1

    :cond_b
    move v0, v1

    goto :goto_2

    :cond_c
    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->P:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v1

    sget-object v2, Laso;->b:Laso;

    if-ne v1, v2, :cond_d

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Ljava/lang/String;Laso;)V

    goto/16 :goto_1

    :cond_d
    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v1

    const-string v2, "GLSActivity"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->x:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " #handleTokenResponse - status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    sget-object v2, Lauh;->a:[I

    invoke-virtual {v1}, Laso;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    :cond_f
    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Ljava/lang/String;Laso;)V

    goto/16 :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->i()V

    goto/16 :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->I:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->I:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->E:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->p()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x403

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->o()Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->A:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->o()Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->a(Landroid/graphics/Bitmap;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    :sswitch_5
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->i()V

    goto/16 :goto_1

    :sswitch_6
    if-ne p2, v9, :cond_10

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->j()V

    goto/16 :goto_1

    :cond_10
    const/4 v0, 0x5

    if-ne p2, v0, :cond_11

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->i()V

    goto/16 :goto_1

    :cond_11
    if-ne p2, v7, :cond_12

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->j()V

    goto/16 :goto_1

    :cond_12
    invoke-virtual {p0, p2}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_2
        0x3ea -> :sswitch_4
        0x3ec -> :sswitch_1
        0x3f1 -> :sswitch_6
        0x402 -> :sswitch_0
        0x403 -> :sswitch_3
        0x409 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x2

    const/4 v3, 0x0

    invoke-super {p0, p1}, Latw;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GLSActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " #onCreate - isFinishing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LoginActivity requires extra data"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Landroid/os/Bundle;)V

    invoke-static {v0}, Lapc;->a(Landroid/content/Intent;)Laso;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Laso;->a:Laso;

    :cond_3
    sget-object v1, Lauh;->a:[I

    invoke-virtual {v0}, Laso;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Ljava/lang/String;Laso;)V

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, v3, v3}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Laro;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->i()V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->N:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->N:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->b(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->R:Laui;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->R:Laui;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laui;->cancel(Z)Z

    :cond_0
    invoke-super {p0}, Latw;->onDestroy()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "calling_app"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->I:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "suppress_progress_screen"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "service"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "options"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->J:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v0, "pacl"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->C:Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "facl"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->B:Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "backup"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "url"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->P:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "title"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "confirmCredentials"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->K:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->N:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v0, "response_status"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->N:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method
