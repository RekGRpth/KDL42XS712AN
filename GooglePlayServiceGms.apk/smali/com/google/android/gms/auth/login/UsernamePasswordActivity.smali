.class public Lcom/google/android/gms/auth/login/UsernamePasswordActivity;
.super Latw;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field protected A:Landroid/widget/EditText;

.field protected B:Landroid/widget/EditText;

.field protected C:Ljava/lang/String;

.field protected D:Ljava/lang/String;

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private G:Landroid/widget/TextView;

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:J

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Z

.field private O:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Latw;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->D:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Laux;
    .locals 4

    new-instance v0, Laux;

    const-string v1, "account_name"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "password"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Laux;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->H:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->I:Z

    return p1
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {v1}, Latw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x40

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    move-object v1, v2

    :cond_1
    return-object v1

    :cond_2
    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v1, p1

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v0, 0x7f0b048d    # com.google.android.gms.R.string.auth_gmail_host_name

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Laox;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "device_country"

    invoke-static {p0, v4, v2}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "de"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const v0, 0x7f0b048e    # com.google.android.gms.R.string.auth_googlemail_host_name

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t find gmail_host_name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_6
    const-string v0, "@"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    aget-object v3, v0, v6

    invoke-static {v3}, Latw;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    aget-object v0, v0, v6

    goto :goto_1

    :cond_7
    const-string v0, ""

    goto :goto_1
.end method


# virtual methods
.method public final e()V
    .locals 3

    invoke-super {p0}, Latw;->e()V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->N:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->M:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "password"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->M:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->finish()V

    return-void
.end method

.method protected final f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final h()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0}, Latw;->h()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->H:Z

    if-nez v4, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v4, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->N:Z

    if-nez v4, :cond_0

    move v0, v1

    :cond_0
    iget-boolean v4, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->I:Z

    if-nez v4, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Latw;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    :cond_0
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    const-string v0, "password"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->M:Ljava/lang/String;

    const-string v0, "is_adding_account"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->N:Z

    const-string v0, "is_confirming_credentials"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->O:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Ljava/lang/String;

    :cond_1
    const v0, 0x7f04002d    # com.google.android.gms.R.layout.auth_login_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setContentView(I)V

    const v0, 0x7f0a00a5    # com.google.android.gms.R.id.next_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->E:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->E:Landroid/view/View;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/view/View;Z)V

    const v0, 0x7f0a00aa    # com.google.android.gms.R.id.back_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->F:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->F:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/view/View;)V

    const v0, 0x7f0a00a7    # com.google.android.gms.R.id.username_edit

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    const v0, 0x7f0a00a8    # com.google.android.gms.R.id.password_edit

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->N:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->O:Z

    if-nez v0, :cond_2

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b04db    # com.google.android.gms.R.string.auth_relogin_activity_title

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    const v0, 0x7f0a00a6    # com.google.android.gms.R.id.username_fixed

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    new-instance v2, Lauv;

    invoke-direct {v2, p0}, Lauv;-><init>(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;)V

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/view/View;Z)V

    const v0, 0x7f0a00a9    # com.google.android.gms.R.id.sign_in_agreement

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->G:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->G:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->h()V

    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    new-instance v2, Lauu;

    invoke-direct {v2, p0}, Lauu;-><init>(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;)V

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iput-boolean v4, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->J:Z

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Latw;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const v0, 0x7f0b04c1    # com.google.android.gms.R.string.auth_sign_in_browser

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02006b    # com.google.android.gms.R.drawable.auth_ic_menu_account

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-static {v0}, Laox;->a(Landroid/view/MenuItem;)V

    return v2
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    const v2, 0x7f0b049a    # com.google.android.gms.R.string.auth_field_cant_be_blank

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    if-ne p1, v0, :cond_4

    if-nez p2, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->N:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->H:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    const v1, 0x7f0b0498    # com.google.android.gms.R.string.auth_invalid_login_character

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    const v1, 0x7f0b04a9    # com.google.android.gms.R.string.auth_invalid_username

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->I:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    const v1, 0x7f0b0499    # com.google.android.gms.R.string.auth_invalid_password_character

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Latw;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Latw;->onPause()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->K:J

    return-void
.end method

.method protected onResume()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Latw;->onResume()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->h()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->K:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->J:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->D:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->I:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Latw;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "password"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "is_adding_account"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "is_confirming_credentials"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
