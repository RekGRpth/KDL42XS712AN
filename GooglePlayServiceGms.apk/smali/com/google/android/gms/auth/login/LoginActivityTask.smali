.class public Lcom/google/android/gms/auth/login/LoginActivityTask;
.super Latw;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final B:Ljava/lang/String;

.field private static final C:Ljava/lang/String;

.field private static final D:Ljava/lang/String;

.field private static final E:Ljava/lang/String;

.field private static final F:Ljava/lang/String;


# instance fields
.field A:Z

.field private G:Landroid/widget/Button;

.field private H:Landroid/widget/TextView;

.field private I:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Z

.field private M:Ljava/lang/String;

.field private N:Laut;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/gms/auth/login/LoginActivityTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->B:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "auth_code"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->C:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".token_request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->D:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".backup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->E:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->F:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Latw;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->A:Z

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/LoginActivityTask;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->D:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "password"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->I:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    const-string v0, "password"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->K:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->C:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->J:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->E:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->L:Z

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->M:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final e()V
    .locals 7

    invoke-super {p0}, Latw;->e()V

    new-instance v0, Lauk;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->I:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->K:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->J:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->L:Z

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lauk;-><init>(Lcom/google/android/gms/auth/login/LoginActivityTask;Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->N:Laut;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->N:Laut;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Laut;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->N:Laut;

    invoke-virtual {v0, v3}, Laut;->cancel(Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GLSActivity"

    const-string v1, "LoginActivityTask.BackgroudTask failed to cancel."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "errorCode"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Latw;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->a(Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/LoginActivityTask;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Latw;->onPause()V

    sget-boolean v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->w:Z

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPause(), class="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Latw;->onSaveInstanceState(Landroid/os/Bundle;)V

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->D:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->I:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "password"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->K:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->C:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->J:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->E:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->L:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->F:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Latw;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->g()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f040034    # com.google.android.gms.R.layout.auth_task_progress_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setContentView(I)V

    const v0, 0x7f0a007b    # com.google.android.gms.R.id.cancel_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->G:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->G:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00c4    # com.google.android.gms.R.id.verbose_message

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->H:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->H:Landroid/widget/TextView;

    const v1, 0x7f0b049b    # com.google.android.gms.R.string.auth_plus_name_check_text

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->M:Ljava/lang/String;

    if-nez v0, :cond_1

    const v0, 0x7f0b04df    # com.google.android.gms.R.string.auth_login_activity_task_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->M:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->M:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
