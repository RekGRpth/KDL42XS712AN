.class public Lcom/google/android/gms/auth/TokenActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Landroid/os/Bundle;

.field private f:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

.field private g:Ljava/lang/String;

.field private h:Lamu;

.field private i:Ljava/util/ArrayList;

.field private j:Laoy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/gms/auth/TokenActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/TokenActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/TokenActivity;->b:Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->g:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/TokenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "callerExtras"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "account:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "scope:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "extrashash:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/auth/TokenActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/auth/TokenActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;ZLandroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6

    invoke-static {}, Lamt;->a()Lamt;

    move-result-object v2

    invoke-static {}, Lamt;->a()Lamt;

    move-result-object v0

    if-eqz p6, :cond_5

    invoke-virtual {v0, p1}, Lamt;->a(Ljava/lang/String;)Lamu;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_8

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lamv;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lamv;-><init>(Lamt;B)V

    iget-object v3, v1, Lamv;->a:Lamu;

    iput-object v0, v3, Lamu;->a:Ljava/lang/String;

    iget-object v0, v1, Lamv;->a:Lamu;

    iput-object p1, v0, Lamu;->c:Ljava/lang/String;

    if-eqz p6, :cond_6

    :try_start_0
    sget-object v0, Lamx;->a:Lamx;

    :goto_1
    invoke-virtual {v1, v0}, Lamv;->a(Lamx;)Lamu;
    :try_end_0
    .catch Lamw; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    :goto_2
    invoke-virtual {v1, p2, p4}, Lamu;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v0, "authAccount"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "service"

    invoke-virtual {v3, v0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "uid_key"

    invoke-virtual {v3, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "package_key"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "callback_intent"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "callback_intent"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    const-string v4, "callback_intent"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "authority"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "sync_extras"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "authority"

    const-string v4, "authority"

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sync_extras"

    const-string v4, "sync_extras"

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    invoke-virtual {v1, v3}, Lamu;->a(Landroid/os/Bundle;)V

    :cond_2
    const-string v0, "notification_data_key"

    iget-object v3, v1, Lamu;->a:Ljava/lang/String;

    invoke-virtual {p7, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "notification_request_list_key"

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, v1, Lamu;->e:Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p7, v0, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/4 v0, 0x0

    const/high16 v3, 0x10000000

    invoke-static {p0, v0, p7, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    if-eqz p6, :cond_3

    const/4 p2, 0x0

    :cond_3
    if-eqz p2, :cond_7

    const v0, 0x7f0b04e9    # com.google.android.gms.R.string.app_level_title

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-ge v4, v5, :cond_4

    const v0, 0x7f0b04ea    # com.google.android.gms.R.string.app_level_title_secondary

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p9, v4, v5

    invoke-virtual {p0, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    :goto_3
    new-instance v4, Lax;

    invoke-direct {v4, p0}, Lax;-><init>(Landroid/content/Context;)V

    iput-object v0, v4, Lax;->b:Ljava/lang/CharSequence;

    iput-object p1, v4, Lax;->c:Ljava/lang/CharSequence;

    const v0, 0x108008a    # android.R.drawable.stat_sys_warning

    invoke-virtual {v4, v0}, Lax;->a(I)Lax;

    move-result-object v0

    iput-object p8, v0, Lax;->g:Landroid/graphics/Bitmap;

    iput-object v3, v0, Lax;->d:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Lax;->b()Lax;

    move-result-object v0

    const v3, 0x7f0b04ec    # com.google.android.gms.R.string.notification_ticker

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lax;->a(Ljava/lang/CharSequence;)Lax;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v5, v0, Lax;->r:Landroid/app/Notification;

    iput-wide v3, v5, Landroid/app/Notification;->when:J

    invoke-virtual {v0}, Lax;->d()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v2, v1}, Lamt;->a(Lamu;)V

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iget v1, v1, Lamu;->b:I

    invoke-virtual {v0, v1, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    invoke-static {}, Lamt;->a()Lamt;

    move-result-object v0

    invoke-virtual {v0}, Lamt;->b()Z

    :goto_4
    return-void

    :cond_5
    invoke-virtual {v0, p1, p4, p2}, Lamt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lamu;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    :try_start_1
    sget-object v0, Lamx;->b:Lamx;
    :try_end_1
    .catch Lamw; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/google/android/gms/auth/TokenActivity;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Error: Malformed NotificationData. This is a Bug."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :cond_7
    const v0, 0x7f0b04eb    # com.google.android.gms.R.string.account_level_title

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020133    # com.google.android.gms.R.drawable.ic_google_selected

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p8

    goto :goto_3

    :cond_8
    move-object v1, v0

    goto/16 :goto_2
.end method

.method private a(Landroid/content/Intent;Z)V
    .locals 5

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "authAccount"

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "service"

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "callerExtras"

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    if-eqz p2, :cond_1

    sget-object v2, Lapc;->d:Lapc;

    invoke-virtual {v2, v1}, Lapc;->b(Landroid/content/Intent;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Lcom/google/android/gms/auth/TokenActivity;->setResult(ILandroid/content/Intent;)V

    const-string v1, "response"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    const-string v2, "canceled"

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lapc;->a(Landroid/content/Intent;)Laso;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v2}, Lapc;->a(Laso;)Lapc;

    move-result-object v2

    invoke-virtual {v2, v1}, Lapc;->b(Landroid/content/Intent;)V

    :cond_2
    if-eqz p1, :cond_3

    const-string v2, "authtoken"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-boolean v3, p0, Lcom/google/android/gms/auth/TokenActivity;->b:Z

    if-eqz v3, :cond_3

    const-string v3, "authtoken"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string v2, "response"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_4

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "retry"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v2}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    :cond_4
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/TokenActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->finish()V

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "service"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->d:Ljava/lang/String;

    const-string v0, "callerExtras"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    const-string v0, "authAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->c:Ljava/lang/String;

    const-string v0, "is_for_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/TokenActivity;->b:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    const-string v1, "request_visible_actions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->g:Ljava/lang/String;

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/auth/TokenActivity;)Lcom/google/android/gms/auth/firstparty/shared/AppDescription;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->f:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/auth/TokenActivity;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/auth/TokenActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/auth/TokenActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/gms/auth/TokenActivity;)Laoy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->j:Laoy;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p2, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, p3, v0}, Lcom/google/android/gms/auth/TokenActivity;->a(Landroid/content/Intent;Z)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, p3, v0}, Lcom/google/android/gms/auth/TokenActivity;->a(Landroid/content/Intent;Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0}, Lamu;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/Window;->clearFlags(I)V

    invoke-static {}, Lamt;->a()Lamt;

    move-result-object v0

    new-instance v3, Laoy;

    invoke-direct {v3, p0}, Laoy;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->j:Laoy;

    if-eqz p1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/TokenActivity;->a(Landroid/os/Bundle;)V

    const-string v1, "notification_data_key"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lamt;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->h:Lamu;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/TokenActivity;->a(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    sget-object v4, Lamr;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "notification_request_list_key"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->i:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->j:Laoy;

    invoke-virtual {v0, v3}, Laoy;->a(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    const-string v5, "clientPackageName"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    const-string v5, "clientPackageName"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/gms/auth/TokenActivity;->b:Z

    :goto_2
    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    const-string v6, "clientPackageName"

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->j:Laoy;

    invoke-virtual {v5, v0}, Laoy;->a(Ljava/lang/String;)I

    move-result v0

    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->j:Laoy;

    invoke-static {v0, v3, v4, v5}, Laow;->a(ILjava/lang/String;ILaoy;)Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->f:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    const-string v0, "GLSActivity"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gms/auth/TokenActivity;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Handling token request from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/auth/TokenActivity;->f:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/TokenActivity;->f:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v3

    if-nez v3, :cond_6

    :cond_3
    move v0, v2

    :goto_3
    if-nez v0, :cond_8

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/TokenActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->finish()V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->j:Laoy;

    iget-object v0, v0, Laoy;->c:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iput-boolean v1, p0, Lcom/google/android/gms/auth/TokenActivity;->b:Z

    goto :goto_2

    :cond_6
    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Laoz;->a(Ljava/lang/String;Ljava/lang/String;)Laoz;

    move-result-object v0

    if-eqz v0, :cond_7

    move v0, v1

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    new-instance v0, Lanc;

    invoke-direct {v0, p0}, Lanc;-><init>(Lcom/google/android/gms/auth/TokenActivity;)V

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lanc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "service"

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "callerExtras"

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->e:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v0, "is_for_result"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/TokenActivity;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->h:Lamu;

    if-eqz v0, :cond_0

    const-string v0, "notification_data_key"

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->h:Lamu;

    iget-object v1, v1, Lamu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
