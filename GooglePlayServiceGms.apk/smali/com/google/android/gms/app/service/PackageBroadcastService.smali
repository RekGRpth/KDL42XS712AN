.class public final Lcom/google/android/gms/app/service/PackageBroadcastService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "PackageBroadcastService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/app/service/PackageBroadcastService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "broadcastIntent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbov;->b(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "package:com.google.android.gms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "package:com.google.android.gms."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "PackageBroadcastService"

    const-string v1, "Null package name or gms related package.  Ignoreing."

    invoke-static {v0, v1}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/gms/icing/service/SystemEventReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 8

    const/4 v7, 0x0

    const-string v0, "broadcastIntent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, ""

    :goto_0
    const-string v3, "PackageBroadcastService"

    const-string v4, "Received broadcast action=%s and uri=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v7

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0, v0}, Liju;->b(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->b(Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->c(Landroid/content/Intent;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p0, v0}, Liju;->b(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->b(Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->c(Landroid/content/Intent;)V

    goto :goto_1

    :cond_4
    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->a(Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->c(Landroid/content/Intent;)V

    goto :goto_1

    :cond_5
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "android.intent.extra.REPLACING"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->a(Landroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p0, v0}, Liju;->b(Landroid/content/Context;Landroid/content/Intent;)V

    const-string v2, "com.google.android.gms"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "PackageBroadcastService"

    const-string v3, "Received PLAY_SERVICES_REMOVED broadcast"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/google/android/gms/wallet/service/DataRemovalService;->a(Landroid/content/Context;)V

    const-string v2, "PackageBroadcastService"

    const-string v3, "Removed Wallet data."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-static {p0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_7
    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->b(Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->c(Landroid/content/Intent;)V

    goto :goto_1

    :cond_8
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, v0}, Lcom/google/android/gms/gcm/GmsAutoStarter;->a(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->b(Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/service/PackageBroadcastService;->c(Landroid/content/Intent;)V

    goto :goto_1
.end method
