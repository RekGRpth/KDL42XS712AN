.class public Lcom/google/android/gms/app/settings/ManageSpaceActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private o:Landroid/view/View;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/Button;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/Button;

.field private v:Ljava/lang/CharSequence;

.field private w:Lahg;

.field private x:Lahf;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->v:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public static synthetic a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->clearApplicationUserData()Z

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->p:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->t:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->r:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->q:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->s:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->u:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    new-instance v0, Lahe;

    invoke-direct {v0}, Lahe;-><init>()V

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "clearDataDialog"

    invoke-virtual {v0, v1, v2}, Lahe;->a(Lu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0400ad    # com.google.android.gms.R.layout.manage_space_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->setContentView(I)V

    const v0, 0x7f0b0479    # com.google.android.gms.R.string.storage_managment_computing_size

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->v:Ljava/lang/CharSequence;

    const v0, 0x7f0a022a    # com.google.android.gms.R.id.icing_storage_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->p:Landroid/widget/TextView;

    const v0, 0x7f0a022b    # com.google.android.gms.R.id.manage_icing_storage

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->q:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->q:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a022e    # com.google.android.gms.R.id.drive_storage_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->r:Landroid/widget/TextView;

    const v0, 0x7f0a022f    # com.google.android.gms.R.id.manage_drive_storage

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->s:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->s:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0230    # com.google.android.gms.R.id.clear_all_data_section

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->o:Landroid/view/View;

    const/16 v0, 0x13

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0232    # com.google.android.gms.R.id.total_storage_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->t:Landroid/widget/TextView;

    const v0, 0x7f0a0233    # com.google.android.gms.R.id.clear_all_data

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->u:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->u:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->w:Lahg;

    invoke-virtual {v0, v1}, Lahg;->cancel(Z)Z

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->x:Lahf;

    invoke-virtual {v0, v1}, Lahf;->cancel(Z)Z

    iput-object v2, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->w:Lahg;

    iput-object v2, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->x:Lahf;

    invoke-super {p0}, Ljp;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Ljp;->onResume()V

    new-instance v0, Lahg;

    invoke-direct {v0, p0}, Lahg;-><init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->w:Lahg;

    new-instance v0, Lahf;

    invoke-direct {v0, p0, v3}, Lahf;-><init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->x:Lahf;

    const/16 v0, 0xb

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->w:Lahg;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lahg;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->x:Lahf;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lahf;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->w:Lahg;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lahg;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->x:Lahf;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lahf;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
