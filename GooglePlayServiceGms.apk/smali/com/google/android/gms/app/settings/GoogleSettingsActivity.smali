.class public Lcom/google/android/gms/app/settings/GoogleSettingsActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private o:Lamd;

.field private p:Z

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/TextView;

.field private s:I

.field private t:Landroid/graphics/drawable/Drawable;

.field private u:Liiw;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Intent;I)Landroid/widget/TextView;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x1090003    # android.R.layout.simple_list_item_1

    iget-object v2, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->q:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/widget/TextView;Landroid/content/Intent;)Landroid/widget/TextView;

    return-object v0
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;)Landroid/widget/TextView;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x1090003    # android.R.layout.simple_list_item_1

    iget-object v2, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->q:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/widget/TextView;Landroid/content/Intent;)Landroid/widget/TextView;

    return-object v0
.end method

.method private a(Landroid/widget/TextView;Landroid/content/Intent;)Landroid/widget/TextView;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/high16 v0, 0x41900000    # 18.0f

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    const v0, 0x7f02009d    # com.google.android.gms.R.drawable.common_settings_button_bg

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->s:I

    iget v1, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->s:I

    invoke-virtual {p1, v0, v2, v1, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, p2, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object p1

    :cond_1
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setMinimumHeight(I)V

    iget-object v1, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private f()Z
    .locals 6

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v5, "@google.com"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x3

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "GoogleSettingsActivity"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GoogleSettingsActivity"

    const-string v2, "App hidden after item was displayed?"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    const-string v0, "GoogleSettingsActivity"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleSettingsActivity"

    const-string v1, "App action was never defined?"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/16 v7, 0xe

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040040    # com.google.android.gms.R.layout.common_settings

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->setContentView(I)V

    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleSettingsActivity"

    const-string v1, "Cannot run for restricted users."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v0}, Lbox;->a(Landroid/content/Context;Landroid/content/ComponentName;)V

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    const v3, 0x7f02009a    # com.google.android.gms.R.drawable.common_red_banner_settings_icon

    invoke-virtual {v0, v3}, Ljj;->b(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_f

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->p:Z

    const v0, 0x7f0a00d3    # com.google.android.gms.R.id.settings_container

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->q:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d01e4    # com.google.android.gms.R.dimen.common_settings_item_left_right_padding

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->s:I

    const/16 v0, 0xb

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    new-array v0, v1, [I

    const v3, 0x1010214    # android.R.attr.listDivider

    aput v3, v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_3
    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->p:Z

    if-nez v0, :cond_10

    :cond_4
    :goto_2
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.plus.PRIVACY_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "account"

    new-instance v4, Landroid/accounts/Account;

    const-string v5, "fake"

    const-string v6, "com.google"

    invoke-direct {v4, v5, v6}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const v3, 0x7f0b0467    # com.google.android.gms.R.string.common_plus_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Ldvi;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_11

    invoke-static {p0}, Leee;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_11

    :cond_5
    :goto_3
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.location.settings.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0b0464    # com.google.android.gms.R.string.common_location_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->r:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->r:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0b0463    # com.google.android.gms.R.string.common_maps_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    const/16 v0, 0x10

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_12

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.googlequicksearchbox.action.PRIVACY_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0b0466    # com.google.android.gms.R.string.common_search_and_now_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    :goto_4
    invoke-direct {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.google.android.gms.lockbox.settings.DeviceUsageSettingsActivity"

    invoke-virtual {v0, p0, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "lockbox_device_usage_settings_title"

    const-string v5, "string"

    const-string v6, "com.google.android.gms"

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_6

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.settings.ADS_PRIVACY"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0b0468    # com.google.android.gms.R.string.common_ads_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    invoke-static {p0}, Lgqm;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_13

    move v0, v2

    :goto_5
    if-eqz v0, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v3, 0x7f0b046b    # com.google.android.gms.R.string.common_security_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    :cond_7
    sget-object v0, Lexh;->m:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p0}, Lbov;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    invoke-static {p0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const v3, 0x7f0b0475    # com.google.android.gms.R.string.common_mdm_feature_name

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    :cond_9
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.settings.DRIVE_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0b0062    # com.google.android.gms.R.string.drive_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    invoke-static {v7}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.auth.setup.CONFIG_REMOTE_SETUP"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const-string v5, "x_auth_setup_configuration_title"

    invoke-virtual {v3, v5, v4, v1}, Landroid/content/res/Resources;->getValue(Ljava/lang/String;Landroid/util/TypedValue;Z)V

    invoke-virtual {v4}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;Ljava/lang/String;)Landroid/widget/TextView;

    :cond_a
    invoke-static {v7}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_b

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.auth.setup.PULL_ACCOUNT_SETUP"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    const-string v4, "x_auth_setup_pull_account_title"

    invoke-virtual {v2, v4, v3, v1}, Landroid/content/res/Resources;->getValue(Ljava/lang/String;Landroid/util/TypedValue;Z)V

    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;Ljava/lang/String;)Landroid/widget/TextView;

    :cond_b
    invoke-direct {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->f()Z

    move-result v0

    if-nez v0, :cond_c

    sget-object v0, Lbfx;->c:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_c
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "People debug"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;Ljava/lang/String;)Landroid/widget/TextView;

    :cond_d
    invoke-direct {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->f()Z

    move-result v0

    if-nez v0, :cond_e

    sget-object v0, Lbfx;->d:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_e
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "Download Service debug"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;Ljava/lang/String;)Landroid/widget/TextView;

    goto/16 :goto_0

    :cond_f
    move v0, v2

    goto/16 :goto_1

    :cond_10
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.plus.action.MANAGE_APPS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.gms.extras.ALL_APPS"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const v3, 0x7f0b0469    # com.google.android.gms.R.string.common_connected_apps_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    goto/16 :goto_2

    :cond_11
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.games.SHOW_GOOGLE_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0b046a    # com.google.android.gms.R.string.common_games_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    goto/16 :goto_3

    :cond_12
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.googlequicksearchbox.action.PRIVACY_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0b0465    # com.google.android.gms.R.string.common_search_settings_title

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Landroid/content/Intent;I)Landroid/widget/TextView;

    goto/16 :goto_4

    :cond_13
    const-string v0, "show_security_settings"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_14

    move v0, v2

    goto/16 :goto_5

    :cond_14
    move v0, v1

    goto/16 :goto_5
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->p:Z

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f120017    # com.google.android.gms.R.menu.settings_menu

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    new-instance v2, Lbhv;

    invoke-direct {v2, p0}, Lbhv;-><init>(Landroid/content/Context;)V

    const-string v3, "enable_offline_otp_v2"

    invoke-virtual {v2, v3}, Lbhv;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    const v0, 0x7f0a0392    # com.google.android.gms.R.id.get_verification_code

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lanh;->c(Landroid/content/Context;)Z

    move-result v0

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Ljp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lo;->b:Lw;

    new-instance v2, Lamd;

    invoke-direct {v2}, Lamd;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->o:Lamd;

    iget-object v2, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->o:Lamd;

    const-string v3, "clear_app_state_dialog"

    invoke-virtual {v2, v0, v3}, Lamd;->a(Lu;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    sget-object v0, Lahd;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static {p0, v2}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v2, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/auth/otp/OtpActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a0391
        :pswitch_0    # com.google.android.gms.R.id.clear_app_data
        :pswitch_2    # com.google.android.gms.R.id.get_verification_code
        :pswitch_1    # com.google.android.gms.R.id.settings_help
    .end packed-switch
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    const/4 v2, 0x2

    const-string v0, "GoogleSettingsActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleSettingsActivity"

    const-string v1, "GoogleSettingsActivity.onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p2}, Liix;->a(Landroid/os/IBinder;)Liiw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->u:Liiw;

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->r:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v0, "GoogleSettingsActivity"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleSettingsActivity"

    const-string v1, "Location setting item not set up yet."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->u:Liiw;

    if-nez v0, :cond_3

    const-string v0, "GoogleSettingsActivity"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleSettingsActivity"

    const-string v1, "Not connected, will add location settings when connect."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->u:Liiw;

    invoke-interface {v0}, Liiw;->a()Lcom/google/android/location/reporting/service/ReportingConfig;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Landroid/content/Context;Lcom/google/android/location/reporting/service/ReportingConfig;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->r:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GoogleSettingsActivity"

    const-string v2, "Exception while fetching reporting config"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    const-string v0, "GoogleSettingsActivity"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleSettingsActivity"

    const-string v1, "GoogleSettingsActivity.onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->u:Liiw;

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Ljp;->onStart()V

    invoke-static {p0, p0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Ljp;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->u:Liiw;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->u:Liiw;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->o:Lamd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->o:Lamd;

    invoke-virtual {v0}, Lamd;->J()V

    :cond_1
    return-void
.end method
