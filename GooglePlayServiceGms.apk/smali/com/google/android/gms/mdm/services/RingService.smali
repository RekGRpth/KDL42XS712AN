.class public Lcom/google/android/gms/mdm/services/RingService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lsj;
.implements Lsk;


# static fields
.field private static final a:Landroid/content/IntentFilter;

.field private static final b:[I


# instance fields
.field private c:Landroid/media/AudioManager;

.field private d:Landroid/media/MediaPlayer;

.field private e:Lexu;

.field private f:Lext;

.field private g:Landroid/view/WindowManager;

.field private h:Landroid/view/View;

.field private final i:Landroid/os/Handler;

.field private final j:Ljava/lang/Runnable;

.field private final k:Landroid/content/BroadcastReceiver;

.field private l:I

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/mdm/services/RingService$1;

    invoke-direct {v0}, Lcom/google/android/gms/mdm/services/RingService$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/mdm/services/RingService;->a:Landroid/content/IntentFilter;

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/mdm/services/RingService;->b:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x4
        0x2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->i:Landroid/os/Handler;

    new-instance v0, Lexr;

    invoke-direct {v0, p0}, Lexr;-><init>(Lcom/google/android/gms/mdm/services/RingService;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->j:Ljava/lang/Runnable;

    new-instance v0, Lexs;

    invoke-direct {v0, p0}, Lexs;-><init>(Lcom/google/android/gms/mdm/services/RingService;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->k:Landroid/content/BroadcastReceiver;

    iput v1, p0, Lcom/google/android/gms/mdm/services/RingService;->l:I

    iput v1, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/mdm/services/RingService;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->j:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/services/RingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/services/RingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "echoServerToken"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/mdm/services/RingService;Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    iget v0, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/mdm/services/RingService;->l:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/media/AudioManager;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    invoke-virtual {v0, v2, v1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lexu;

    invoke-direct {v0, p0, v3}, Lexu;-><init>(Lcom/google/android/gms/mdm/services/RingService;B)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Lexu;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Lexu;

    invoke-virtual {v0}, Lexu;->start()V

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "MDM"

    const-string v3, "Failed to play requested ringtone"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/gms/mdm/services/RingService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->i:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    const-string v1, "Successfully sent the payload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public final a(Lsp;)V
    .locals 2

    const-string v0, "MDM"

    const-string v1, "Failed to send the payload"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public final a()Z
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v4, Landroid/media/RingtoneManager;

    invoke-direct {v4, p0}, Landroid/media/RingtoneManager;-><init>(Landroid/content/Context;)V

    sget-object v0, Lexh;->k:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4}, Landroid/media/RingtoneManager;->getCursor()Landroid/database/Cursor;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ge v1, v6, :cond_2

    invoke-interface {v5, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4, v1}, Landroid/media/RingtoneManager;->getRingtoneUri(I)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gms/mdm/services/RingService;->a(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v2, v3

    :cond_0
    :goto_1
    return v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/gms/mdm/services/RingService;->b:[I

    array-length v4, v1

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_0

    aget v5, v1, v0

    invoke-static {p0, v5}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/gms/mdm/services/RingService;->a(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v3

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    const/4 v1, -0x1

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/RingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/media/AudioManager;

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/RingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->g:Landroid/view/WindowManager;

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7da

    const v4, 0x800c0

    const/4 v5, -0x2

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->h:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->g:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/RingService;->h:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->k:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/gms/mdm/services/RingService;->a:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/mdm/services/RingService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lax;

    invoke-direct {v0, p0}, Lax;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0201cc    # com.google.android.gms.R.drawable.mdm_ic_notification

    invoke-virtual {v0, v1}, Lax;->a(I)Lax;

    move-result-object v0

    const v1, 0x7f0b0475    # com.google.android.gms.R.string.common_mdm_feature_name

    invoke-virtual {p0, v1}, Lcom/google/android/gms/mdm/services/RingService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lax;->b:Ljava/lang/CharSequence;

    const v1, 0x7f0b01d9    # com.google.android.gms.R.string.mdm_ringing_notification_text

    invoke-virtual {p0, v1}, Lcom/google/android/gms/mdm/services/RingService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lax;->c:Ljava/lang/CharSequence;

    invoke-static {p0}, Lexm;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Lax;->d:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Lax;->b()Lax;

    move-result-object v0

    invoke-virtual {v0}, Lax;->d()Landroid/app/Notification;

    move-result-object v0

    sget v1, Lexw;->b:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/mdm/services/RingService;->startForeground(ILandroid/app/Notification;)V

    return-void
.end method

.method public onDestroy()V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lext;

    invoke-virtual {v0, v5}, Lext;->cancel(Z)Z

    iput-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lext;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Lexu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Lexu;

    invoke-virtual {v0}, Lexu;->a()V

    :cond_1
    iget v0, p0, Lcom/google/android/gms/mdm/services/RingService;->l:I

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/media/AudioManager;

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/mdm/services/RingService;->l:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_2
    iget v0, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    if-eq v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/RingService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->g:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->h:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    invoke-virtual {p0, v5}, Lcom/google/android/gms/mdm/services/RingService;->stopForeground(Z)V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3

    const-string v0, "MDM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error playing ringtone, what: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " extra: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/RingService;->stopSelf()V

    const/4 v0, 0x1

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lext;

    if-nez v2, :cond_0

    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/MediaPlayer;

    new-instance v2, Lext;

    invoke-direct {v2, p0, v0}, Lext;-><init>(Lcom/google/android/gms/mdm/services/RingService;B)V

    iput-object v2, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lext;

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lext;

    new-array v3, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lext;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    const-string v2, "echoServerToken"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v1

    move-object v4, p0

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lexf;->a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V

    const/4 v0, 0x2

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    const-string v1, "[RingService] onTouch()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/RingService;->stopSelf()V

    const/4 v0, 0x1

    return v0
.end method
