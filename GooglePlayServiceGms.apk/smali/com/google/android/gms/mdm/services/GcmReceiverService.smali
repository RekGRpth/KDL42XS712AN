.class public Lcom/google/android/gms/mdm/services/GcmReceiverService;
.super Landroid/app/IntentService;
.source "SourceFile"

# interfaces
.implements Lbdx;
.implements Lbdy;
.implements Leqg;
.implements Lsj;


# instance fields
.field protected a:Ljava/lang/String;

.field private b:Ljava/util/concurrent/Semaphore;

.field private c:Landroid/os/HandlerThread;

.field private d:Lepy;

.field private e:Lbdu;

.field private f:Z

.field private g:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/mdm/services/GcmReceiverService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->f:Z

    return-void
.end method

.method private static a(Landroid/content/Intent;)Ljdl;
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Ljdl;

    invoke-direct {v0}, Ljdl;-><init>()V

    :try_start_0
    const-string v2, "rp"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "rp"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v2, v3, v4}, Lizm;->a([BII)Lizm;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljdl;->b(Lizm;)Ljdl;

    :goto_0
    return-object v0

    :cond_0
    const-string v2, "payload"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "payload"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v2, v3, v4}, Lizm;->a([BII)Lizm;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljdl;->b(Lizm;)Ljdl;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "MDM"

    const-string v2, "Invalid remote policy proto. Ignoring"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v2, "action"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Ljdl;->a:I

    const-string v2, "token"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "token"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iput-object v2, v0, Ljdl;->b:Ljava/lang/String;

    const-string v2, "email"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "email"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    const-string v3, "SHA-256"

    invoke-static {v2, v3}, Lbox;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, v0, Ljdl;->c:[B

    const-string v2, "locate"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Ljdl;->d:Z

    const-string v2, "new_password"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "new_password"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    iput-object v2, v0, Ljdl;->e:Ljava/lang/String;

    const-string v2, "lock_message"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "lock_message"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_4
    iput-object v2, v0, Ljdl;->f:Ljava/lang/String;
    :try_end_1
    .catch Lizr; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "MDM"

    const-string v2, "IOException parsing remote policy proto. Ignoring"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto/16 :goto_0

    :cond_2
    :try_start_2
    const-string v2, ""

    goto :goto_1

    :cond_3
    const-string v2, ""

    goto :goto_2

    :cond_4
    const-string v2, ""

    goto :goto_3

    :cond_5
    const-string v2, ""
    :try_end_2
    .catch Lizr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lbdu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d:Lepy;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lbdu;

    invoke-interface {v0, v1, p0}, Lepy;->a(Lbdu;Leqg;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lbdu;

    invoke-interface {v0}, Lbdu;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lbdu;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZI)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method private static a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lexf;->a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V

    return-void
.end method

.method private a(J)V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    const-string v1, "locate()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Lbpl;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lexl;->d:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;

    goto :goto_0

    :cond_3
    sget-object v0, Leqi;->b:Lepy;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d:Lepy;

    new-instance v0, Lbdw;

    invoke-direct {v0, p0}, Lbdw;-><init>(Landroid/content/Context;)V

    sget-object v1, Leqi;->a:Lbdm;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbdw;->a(Lbdm;Lbdv;)Lbdw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbdw;->a(Lbdx;)Lbdw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbdw;->a(Lbdy;)Lbdw;

    move-result-object v0

    invoke-virtual {v0}, Lbdw;->a()Lbdu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lbdu;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lbdu;

    invoke-interface {v0}, Lbdu;->a()V

    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b:Ljava/util/concurrent/Semaphore;

    iput-boolean v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->f:Z

    new-instance v0, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v0}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    sget-object v0, Lexh;->e:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->c(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    sget-object v0, Lexh;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/location/LocationRequest;->b(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d:Lepy;

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lbdu;

    iget-object v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-interface {v1, v2, v0, p0, v3}, Lepy;->a(Lbdu;Lcom/google/android/gms/location/LocationRequest;Leqg;Landroid/os/Looper;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b:Ljava/util/concurrent/Semaphore;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->f:Z

    if-nez v0, :cond_4

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a()V

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "MDM"

    const-string v1, "Done looking for updates"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a()V

    goto/16 :goto_0
.end method

.method private a(Z)V
    .locals 3

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "autoUpdateDeviceAdmin("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lexv;->b(Landroid/content/Context;Z)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    return-void
.end method

.method private static a(Ljava/lang/String;[B)Z
    .locals 2

    const-string v0, "SHA-256"

    invoke-static {p0, v0}, Lbox;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)Ljdn;
    .locals 6

    const/4 v1, 0x0

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v0, v2, :cond_0

    const-string v0, "MDM"

    const-string v2, "Don\'t call on the main thread"

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    invoke-static {}, Lth;->a()Lth;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    move v0, p1

    move-object v3, v1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V

    :try_start_0
    invoke-virtual {v4}, Lth;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdn;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "MDM"

    const-string v2, "Unable to send response"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Ljdn;

    invoke-direct {v0}, Ljdn;-><init>()V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "MDM"

    const-string v2, "Unable to send response"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Ljdn;

    invoke-direct {v0}, Ljdn;-><init>()V

    goto :goto_0
.end method

.method private d(I)V
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    move v0, p1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->f:Z

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    sget-object v0, Lexh;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    const-string v1, "We have a good enough location, stopping collection."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a()V

    :cond_1
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 3

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to connect: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbbo;->d()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a()V

    return-void
.end method

.method public final a(Lsp;)V
    .locals 2

    const-string v0, "MDM"

    const-string v1, "Unable to send response"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public final c(I)V
    .locals 3

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GCore connection suspended: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d:Lepy;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lbdu;

    invoke-interface {v0, v1}, Lepy;->a(Lbdu;)Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    move-object v4, v3

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V

    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    sget-object v0, Lexh;->p:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->g:[Ljava/lang/String;

    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " callbacks"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    :cond_0
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Wakelock"
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v2, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    :try_start_1
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->acquire()V

    invoke-static {p1}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Landroid/content/Intent;)Ljdl;

    move-result-object v2

    if-nez v2, :cond_2

    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/gcm/GcmReceiver;->b(Landroid/content/Intent;)V

    return-void

    :cond_2
    :try_start_2
    iget-object v0, v2, Ljdl;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    sget-object v0, Lexh;->m:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lbov;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/gcm/GcmReceiver;->b(Landroid/content/Intent;)V

    throw v0

    :cond_4
    :try_start_3
    iget-object v3, v2, Ljdl;->c:[B

    if-eqz v3, :cond_5

    array-length v0, v3

    if-nez v0, :cond_6

    :cond_5
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_c

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;

    goto :goto_0

    :cond_6
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v5, :cond_b

    aget-object v0, v4, v1

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {v7, v3}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Ljava/lang/String;[B)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v8, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->g:[Ljava/lang/String;

    array-length v9, v8

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v9, :cond_9

    aget-object v10, v8, v0

    invoke-virtual {v7, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    const-string v11, "@gmail.com"

    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v3}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Ljava/lang/String;[B)Z

    move-result v10

    if-eqz v10, :cond_8

    const/4 v0, 0x1

    goto :goto_4

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_b
    const/4 v0, 0x0

    goto :goto_2

    :cond_c
    iget-boolean v7, v2, Ljdl;->d:Z

    iget v0, v2, Ljdl;->a:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "MDM"

    const-string v1, "Unrecognized action passed with tickle. Ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;

    goto/16 :goto_0

    :pswitch_0
    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_d

    const-string v0, "MDM"

    const-string v1, "wipe()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    invoke-static {p0}, Lexv;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_e

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(I)V

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    goto/16 :goto_0

    :cond_e
    if-eqz v7, :cond_f

    sget-object v0, Lexh;->f:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(J)V

    sget-object v0, Lexh;->f:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    :cond_f
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;

    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->wipeData(I)V

    goto/16 :goto_0

    :pswitch_1
    sget-object v0, Lexh;->e:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(J)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/mdm/services/RingService;->a(Landroid/content/Context;Ljava/lang/String;)V

    if-eqz v7, :cond_0

    sget-object v0, Lexh;->e:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(J)V

    goto/16 :goto_0

    :pswitch_3
    iget-boolean v0, v2, Ljdl;->g:Z

    invoke-static {p0}, Lexv;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_10

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(I)V

    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    const-string v0, "MDM"

    const-string v1, "Device administrator is already enabled; not showing notification."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_10
    if-eqz v0, :cond_12

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_11

    const-string v0, "MDM"

    const-string v1, "strongRemind()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    const-class v0, Lcom/google/android/gms/mdm/receivers/ActivateDeviceAdminUponUnlockReceiver;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    goto/16 :goto_0

    :cond_12
    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_13

    const-string v0, "MDM"

    const-string v1, "weakRemind()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    invoke-static {p0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Lax;

    invoke-direct {v1, p0}, Lax;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0201cc    # com.google.android.gms.R.drawable.mdm_ic_notification

    invoke-virtual {v1, v2}, Lax;->a(I)Lax;

    move-result-object v1

    const v2, 0x7f0b0475    # com.google.android.gms.R.string.common_mdm_feature_name

    invoke-virtual {p0, v2}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lax;->b:Ljava/lang/CharSequence;

    const v2, 0x7f0b01d8    # com.google.android.gms.R.string.mdm_reminder_notification_text

    invoke-virtual {p0, v2}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lax;->c:Ljava/lang/CharSequence;

    iput-object v0, v1, Lax;->d:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Lax;->b()Lax;

    move-result-object v0

    invoke-virtual {v0}, Lax;->d()Landroid/app/Notification;

    move-result-object v1

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string v2, "mdm.notification_reminder"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, v2, Ljdl;->e:Ljava/lang/String;

    iget-object v8, v2, Ljdl;->f:Ljava/lang/String;

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_14

    const-string v0, "MDM"

    const-string v2, "lockAndResetPassword()"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    invoke-static {p0}, Lexv;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_15

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(I)V

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    goto/16 :goto_0

    :cond_15
    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->lockNow()V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->resetPassword(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_19

    sget-boolean v1, Lexw;->a:Z

    if-eqz v1, :cond_16

    const-string v1, "MDM"

    const-string v2, "Unable to reset. Password was not strong enough"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_16
    new-instance v3, Ljdk;

    invoke-direct {v3}, Ljdk;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;)I

    move-result v1

    iput v1, v3, Ljdk;->a:I

    iget v1, v3, Ljdk;->a:I

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMaximumLength(I)I

    move-result v1

    iput v1, v3, Ljdk;->b:I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;)I

    move-result v1

    iput v1, v3, Ljdk;->c:I

    const/16 v1, 0xb

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_17

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLetters(Landroid/content/ComponentName;)I

    move-result v1

    iput v1, v3, Ljdk;->d:I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLowerCase(Landroid/content/ComponentName;)I

    move-result v1

    iput v1, v3, Ljdk;->e:I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;)I

    move-result v1

    iput v1, v3, Ljdk;->f:I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumNumeric(Landroid/content/ComponentName;)I

    move-result v1

    iput v1, v3, Ljdk;->g:I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumSymbols(Landroid/content/ComponentName;)I

    move-result v1

    iput v1, v3, Ljdk;->h:I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumUpperCase(Landroid/content/ComponentName;)I

    move-result v0

    iput v0, v3, Ljdk;->i:I

    :cond_17
    :goto_6
    if-nez v3, :cond_1a

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    :goto_7
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    invoke-static {p0, v8}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_18
    if-eqz v7, :cond_0

    sget-object v0, Lexh;->e:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(J)V

    goto/16 :goto_0

    :cond_19
    const/4 v3, 0x0

    goto :goto_6

    :cond_1a
    const/16 v0, 0x9

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Ljava/lang/String;Ljdk;Lsk;Lsj;)V

    goto :goto_7

    :pswitch_5
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Z)V

    goto/16 :goto_0

    :pswitch_6
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Z)V

    goto/16 :goto_0

    :pswitch_7
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;

    goto/16 :goto_0

    :pswitch_8
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)Ljdn;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
