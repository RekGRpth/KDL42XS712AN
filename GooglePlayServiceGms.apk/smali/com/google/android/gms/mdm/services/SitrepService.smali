.class public Lcom/google/android/gms/mdm/services/SitrepService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/mdm/services/SitrepService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private static a(JLjava/lang/String;)J
    .locals 6

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-static {p2}, Landroid/net/http/AndroidHttpClient;->parseDate(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :cond_0
    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    :try_start_1
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    :goto_1
    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    add-long v0, p0, v2

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v2, "MDM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot parse retry time: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ZI)Landroid/content/Intent;
    .locals 1

    invoke-static {p0}, Lexv;->c(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZIZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;ZIIZ)Landroid/content/Intent;
    .locals 8

    const/4 v6, 0x0

    const/4 v3, 0x1

    sget-object v0, Lexl;->a:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget-object v0, Lexl;->b:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lexl;->c:Lbgm;

    invoke-virtual {v1}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "MDM"

    const-string v1, "No Google accounts; deferring server state update and enabling accounts changed receiver."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-class v0, Lcom/google/android/gms/mdm/receivers/AccountsChangedReceiver;

    invoke-static {p0, v0, v3}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    const-class v0, Lcom/google/android/gms/mdm/receivers/ConnectivityReceiver;

    invoke-static {p0, v0, v6}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const-class v2, Lcom/google/android/gms/mdm/receivers/AccountsChangedReceiver;

    invoke-static {p0, v2, v6}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    new-instance v2, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gms/mdm/services/SitrepService;

    invoke-direct {v2, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "reason"

    invoke-virtual {v2, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "retry_reason"

    invoke-virtual {v2, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0}, Lbox;->e(Landroid/content/Context;)I

    move-result v6

    const-string v7, "gms_core_version"

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-nez p1, :cond_2

    if-eq v6, v4, :cond_7

    :cond_2
    move v4, v3

    :goto_1
    if-nez p1, :cond_3

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_3
    const-string v0, "gcm_registration_id"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move v0, v3

    :goto_2
    if-nez p1, :cond_4

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v1, p4, :cond_5

    :cond_4
    const-string v0, "is_device_admin"

    invoke-virtual {v2, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move v0, v3

    :cond_5
    if-eqz v0, :cond_0

    move-object v0, v2

    goto :goto_0

    :cond_6
    move v0, v4

    goto :goto_2

    :cond_7
    move v4, p1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ZIZ)Landroid/content/Intent;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZIIZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 8

    const-wide/16 v2, 0x0

    const-string v0, "MDM"

    const-string v1, "Error sending sitrep."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lsp;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lsp;

    move-object v1, v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    if-eqz v1, :cond_1

    instance-of v0, v1, Lexe;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lexe;

    invoke-virtual {v0}, Lexe;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    if-eqz v1, :cond_4

    iget-object v0, v1, Lsp;->a:Lrz;

    if-eqz v0, :cond_4

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "MDM"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "network response: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v1, Lsp;->a:Lrz;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, v1, Lsp;->a:Lrz;

    iget v0, v0, Lrz;->a:I

    const/16 v6, 0x1f7

    if-ne v0, v6, :cond_5

    iget-object v0, v1, Lsp;->a:Lrz;

    iget-object v0, v0, Lrz;->c:Ljava/util/Map;

    const-string v6, "Retry-After"

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lsp;->a:Lrz;

    iget-object v0, v0, Lrz;->c:Ljava/util/Map;

    const-string v1, "Retry-After"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v5, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(JLjava/lang/String;)J

    move-result-wide v0

    :goto_0
    sget-object v4, Lexl;->f:Lbgm;

    iget v5, p0, Lcom/google/android/gms/mdm/services/SitrepService;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lbgm;->a(Ljava/lang/Object;)V

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/mdm/receivers/RetryAfterAlarmReceiver;->a(Landroid/content/Context;J)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_5

    const-string v0, "MDM"

    const-string v1, "errorResponse or networkResponse is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move-wide v0, v2

    goto :goto_0

    :cond_6
    const-class v0, Lcom/google/android/gms/mdm/receivers/ConnectivityReceiver;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;ZI)Landroid/content/Intent;
    .locals 2

    sget-object v0, Lexl;->f:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0}, Lexv;->c(Landroid/content/Context;)Z

    move-result v1

    invoke-static {p0, p1, v0, p2, v1}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZIIZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v10, 0x0

    invoke-static {p1}, Lcr;->a(Landroid/content/Intent;)Z

    const-string v0, "reason"

    invoke-virtual {p1, v0, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->d:I

    const-string v0, "gms_core_version"

    invoke-virtual {p1, v0, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    const-string v0, "gcm_registration_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    const-string v0, "is_device_admin"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "is_device_admin"

    invoke-virtual {p1, v0, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    :cond_0
    const-string v0, "retry_reason"

    invoke-virtual {p1, v0, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {}, Lth;->a()Lth;

    move-result-object v3

    iget v1, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    iget v5, p0, Lcom/google/android/gms/mdm/services/SitrepService;->d:I

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lsf;

    move-result-object v7

    new-instance v6, Ljdr;

    invoke-direct {v6}, Ljdr;-><init>()V

    iput v5, v6, Ljdr;->g:I

    iput v0, v6, Ljdr;->h:I

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    invoke-static {}, Lbox;->a()J

    move-result-wide v8

    cmp-long v0, v8, v11

    if-eqz v0, :cond_9

    iput-wide v8, v6, Ljdr;->a:J

    :cond_1
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, v6, Ljdr;->c:I

    if-lez v1, :cond_2

    iput v1, v6, Ljdr;->b:I

    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iput-object v2, v6, Ljdr;->e:Ljava/lang/String;

    :cond_3
    if-eqz v4, :cond_4

    new-instance v0, Ljdp;

    invoke-direct {v0}, Ljdp;-><init>()V

    iput-object v0, v6, Ljdr;->f:Ljdp;

    iget-object v0, v6, Ljdr;->f:Ljdp;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Ljdp;->a:Z

    iget-object v0, v6, Ljdr;->f:Ljdp;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Ljdp;->b:Z

    iget-object v0, v6, Ljdr;->f:Ljdp;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Ljdp;->c:Z

    :cond_4
    const-string v0, "sitrep"

    new-instance v1, Lsg;

    invoke-direct {v1, v7, v0}, Lsg;-><init>(Lsf;Ljava/lang/Object;)V

    invoke-virtual {v7, v1}, Lsf;->a(Lsh;)V

    new-instance v0, Lexg;

    sget-object v1, Lexh;->c:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    const-class v5, Ljds;

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lexg;-><init>(Ljava/lang/String;ZLsk;Lsj;Ljava/lang/Class;Lizs;)V

    const-string v1, "sitrep"

    invoke-virtual {v0, v1}, Lsc;->a(Ljava/lang/Object;)Lsc;

    invoke-virtual {v0, v10}, Lsc;->a(Z)Lsc;

    invoke-virtual {v7, v0}, Lsf;->a(Lsc;)Lsc;

    :try_start_0
    invoke-virtual {v3}, Lth;->get()Ljava/lang/Object;

    const-string v0, "MDM"

    const-string v1, "Sitrep successful"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_5

    const-string v0, "MDM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "version = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", regId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isDeviceAdmin = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    if-lez v0, :cond_6

    sget-object v0, Lexl;->a:Lbgm;

    iget v1, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbgm;->a(Ljava/lang/Object;)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    sget-object v0, Lexl;->b:Lbgm;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbgm;->a(Ljava/lang/Object;)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    sget-object v0, Lexl;->c:Lbgm;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lbgm;->a(Ljava/lang/Object;)V

    :cond_8
    invoke-static {p0}, Lcom/google/android/gms/mdm/receivers/RetryAfterAlarmReceiver;->b(Landroid/content/Context;)V

    sget-object v0, Lexl;->f:Lbgm;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbgm;->a(Ljava/lang/Object;)V

    sget-object v0, Lexl;->g:Lbgm;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbgm;->a(Ljava/lang/Object;)V

    const-class v0, Lcom/google/android/gms/mdm/receivers/ConnectivityReceiver;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    return-void

    :cond_9
    const-string v0, "MDM"

    const-string v5, "Missing android_id"

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lexh;->o:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "MDM"

    const-string v5, "Including debug info"

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljdh;

    invoke-direct {v0}, Ljdh;-><init>()V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v5

    invoke-static {v5}, Lbox;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    iput-object v5, v0, Ljdh;->a:Ljava/lang/String;

    :cond_a
    sget-object v5, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    sget-object v5, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    iput-object v5, v0, Ljdh;->b:Ljava/lang/String;

    :cond_b
    sget-object v5, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    sget-object v5, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v5, v0, Ljdh;->c:Ljava/lang/String;

    :cond_c
    iput-object v0, v6, Ljdr;->d:Ljdh;

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Ljava/lang/Exception;)V

    goto :goto_1
.end method
