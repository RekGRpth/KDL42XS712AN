.class public Lcom/google/android/gms/mdm/services/LockscreenMessageService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/IntentFilter;


# instance fields
.field private final b:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lexn;

    invoke-direct {v0, p0}, Lexn;-><init>(Lcom/google/android/gms/mdm/services/LockscreenMessageService;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->b:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lexl;->e:Lbgm;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lbgm;->a(Ljava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/services/LockscreenMessageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/services/LockscreenMessageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "lock_message"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->b:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/mdm/receivers/LockscreenUnlockedReceiver;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-class v0, Lcom/google/android/gms/mdm/receivers/LockscreenUnlockedReceiver;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->stopForeground(Z)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    const-string v0, "lock_message"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->stopSelf()V

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lexl;->e:Lbgm;

    invoke-virtual {v1, v0}, Lbgm;->a(Ljava/lang/Object;)V

    new-instance v1, Lax;

    invoke-direct {v1, p0}, Lax;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0201cc    # com.google.android.gms.R.drawable.mdm_ic_notification

    invoke-virtual {v1, v2}, Lax;->a(I)Lax;

    move-result-object v1

    const v2, 0x7f0b0475    # com.google.android.gms.R.string.common_mdm_feature_name

    invoke-virtual {p0, v2}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lax;->b:Ljava/lang/CharSequence;

    iput-object v0, v1, Lax;->c:Ljava/lang/CharSequence;

    invoke-static {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v1, Lax;->d:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Lax;->d()Landroid/app/Notification;

    move-result-object v1

    sget v2, Lexw;->c:I

    invoke-virtual {p0, v2, v1}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->startForeground(ILandroid/app/Notification;)V

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/mdm/LockscreenActivity;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    goto :goto_0
.end method
