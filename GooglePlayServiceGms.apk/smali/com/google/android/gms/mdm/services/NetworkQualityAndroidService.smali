.class public final Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;


# instance fields
.field private a:Lesi;

.field private b:Z

.field private c:Z

.field private d:Landroid/accounts/AccountManager;

.field private e:Lbdu;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->b:Z

    return p1
.end method

.method private b()Z
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->b:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->d:Landroid/accounts/AccountManager;

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v0, v2

    new-array v3, v0, [Lcom/google/android/gms/location/reporting/ReportingState;

    move v0, v1

    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_2

    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a:Lesi;

    aget-object v5, v2, v0

    iget-object v4, v4, Lesi;->a:Less;

    invoke-virtual {v4, v5}, Less;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v4

    aput-object v4, v3, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "Herrevad"

    const-string v2, "ULR reporting client disconnected between connection check and use - won\'t upload location"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    array-length v0, v3

    if-eqz v0, :cond_0

    array-length v2, v3

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_3

    aget-object v4, v3, v0

    invoke-virtual {v4}, Lcom/google/android/gms/location/reporting/ReportingState;->f()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/location/reporting/ReportingState;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final P_()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c:Z

    return-void
.end method

.method public final a()Landroid/location/Location;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Leqi;->b:Lepy;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->e:Lbdu;

    invoke-interface {v0, v1}, Lepy;->a(Lbdu;)Landroid/location/Location;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lbbo;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c:Z

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c:Z

    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.gms.mdm.services.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lexq;

    invoke-direct {v0, p0}, Lexq;-><init>(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;)V

    invoke-virtual {v0}, Lexq;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 3

    const-string v0, "account"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->d:Landroid/accounts/AccountManager;

    new-instance v0, Lesi;

    invoke-direct {v0, p0, p0, p0}, Lesi;-><init>(Landroid/content/Context;Lbbr;Lbbs;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a:Lesi;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a:Lesi;

    invoke-virtual {v0}, Lesi;->a()V

    new-instance v0, Lbdw;

    invoke-direct {v0, p0}, Lbdw;-><init>(Landroid/content/Context;)V

    sget-object v1, Leqi;->a:Lbdm;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbdw;->a(Lbdm;Lbdv;)Lbdw;

    move-result-object v0

    new-instance v1, Lexp;

    invoke-direct {v1, p0}, Lexp;-><init>(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;)V

    invoke-virtual {v0, v1}, Lbdw;->a(Lbdx;)Lbdw;

    move-result-object v0

    new-instance v1, Lexo;

    invoke-direct {v1, p0}, Lexo;-><init>(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;)V

    invoke-virtual {v0, v1}, Lbdw;->a(Lbdy;)Lbdw;

    move-result-object v0

    invoke-virtual {v0}, Lbdw;->a()Lbdu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->e:Lbdu;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->e:Lbdu;

    invoke-interface {v0}, Lbdu;->a()V

    return-void
.end method

.method public final onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a:Lesi;

    invoke-virtual {v0}, Lesi;->b()V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->e:Lbdu;

    invoke-interface {v0}, Lbdu;->b()V

    return-void
.end method
