.class public Lcom/google/android/gms/mdm/MdmSettingsActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final o:Landroid/content/IntentFilter;


# instance fields
.field private p:Landroid/view/View;

.field private q:Landroid/widget/CheckBox;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/CheckBox;

.field private t:Z

.field private u:Lce;

.field private final v:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.mdm.DEVICE_ADMIN_CHANGE_INTENT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->o:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljp;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->t:Z

    new-instance v0, Lexd;

    invoke-direct {v0, p0}, Lexd;-><init>(Lcom/google/android/gms/mdm/MdmSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->v:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/MdmSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/mdm/MdmSettingsActivity;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->s:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    invoke-static {p0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_device_admin"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_modal_request"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->p:Landroid/view/View;

    if-ne p1, v1, :cond_3

    invoke-static {p0}, Lbpl;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->q:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->q:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    sget-object v0, Lexl;->d:Lbgm;

    iget-object v1, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->q:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbgm;->a(Ljava/lang/Object;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->startActivity(Landroid/content/Intent;)V

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->t:Z

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->r:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p0}, Lexv;->a(Landroid/content/Context;)V

    goto :goto_1

    :cond_4
    invoke-static {p0}, Lexv;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lexv;->b(Landroid/content/Context;)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const v7, 0x7f0a0082    # com.google.android.gms.R.id.summary

    const v6, 0x7f0a0060    # com.google.android.gms.R.id.checkbox

    const v5, 0x7f0a005e    # com.google.android.gms.R.id.title

    const/4 v4, 0x0

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0400af    # com.google.android.gms.R.layout.mdm_settings

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->setContentView(I)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljj;->a(Z)V

    const v0, 0x7f0a0237    # com.google.android.gms.R.id.locate_item

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->p:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->p:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->p:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->p:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->q:Landroid/widget/CheckBox;

    const v2, 0x7f0b01d0    # com.google.android.gms.R.string.mdm_settings_locate_title

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-static {p0}, Lbpl;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x13

    invoke-static {v2}, Lbpz;->a(I)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b01d1    # com.google.android.gms.R.string.mdm_settings_locate_summary

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const v0, 0x7f0a0238    # com.google.android.gms.R.id.wipe_item

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->r:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->r:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->r:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b01d3    # com.google.android.gms.R.string.mdm_settings_wipe_title

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->r:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b01d4    # com.google.android.gms.R.string.mdm_settings_wipe_summary

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->r:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->s:Landroid/widget/CheckBox;

    invoke-static {p0}, Lce;->a(Landroid/content/Context;)Lce;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->u:Lce;

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_device_admin"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lexv;->a(Landroid/content/Context;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_modal_request"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    new-instance v0, Lexi;

    invoke-direct {v0}, Lexi;-><init>()V

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "activate_device_admin_dialog"

    invoke-virtual {v0, v1, v2}, Lexi;->a(Lu;Ljava/lang/String;)V

    :cond_2
    if-eqz p1, :cond_3

    const-string v0, "verify_google_location"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->t:Z

    :cond_3
    return-void

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->q:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->q:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    const v2, 0x7f0b01d2    # com.google.android.gms.R.string.mdm_settings_locate_disabled_summary

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120014    # com.google.android.gms.R.menu.mdm_settings

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Ljp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0a038c    # com.google.android.gms.R.id.mdm_help

    if-ne v0, v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v0, Lexh;->l:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lhid;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {p0, v1}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/mdm/MdmSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Ljp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->u:Lce;

    iget-object v1, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->v:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lce;->a(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Ljp;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Ljp;->onResume()V

    invoke-static {p0}, Lbpl;->a(Landroid/content/Context;)Z

    move-result v0

    iget-boolean v2, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->t:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    sget-object v2, Lexl;->d:Lbgm;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbgm;->a(Ljava/lang/Object;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->s:Landroid/widget/CheckBox;

    invoke-static {p0}, Lexv;->c(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v2, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->q:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    sget-object v0, Lexl;->d:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->u:Lce;

    iget-object v1, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->v:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/gms/mdm/MdmSettingsActivity;->o:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Lce;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "verify_google_location"

    iget-boolean v1, p0, Lcom/google/android/gms/mdm/MdmSettingsActivity;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Ljp;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
