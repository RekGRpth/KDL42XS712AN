.class public Lcom/google/android/gms/security/snet/SnetService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Boolean;

.field private static final c:Ljava/lang/Boolean;

.field private static final d:Ljava/lang/Boolean;


# instance fields
.field private e:Z

.field private f:Lgqn;

.field private g:Lgqr;

.field private h:I

.field private i:Lgqp;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/google/android/gms/security/snet/SnetService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/snet/SnetService;->a:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/snet/SnetService;->b:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/snet/SnetService;->c:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/snet/SnetService;->d:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/SnetService;->e:Z

    return-void
.end method

.method private a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/SnetService;->e:Z

    invoke-virtual {p0}, Lcom/google/android/gms/security/snet/SnetService;->stopSelf()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/security/snet/SnetService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/security/snet/SnetService;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lgqw;

    invoke-direct {v0, p0, p1}, Lgqw;-><init>(Lcom/google/android/gms/security/snet/SnetService;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lgqw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/security/snet/SnetService;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "snet_log_all_runs"

    sget-object v3, Lcom/google/android/gms/security/snet/SnetService;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v0, v3}, Lhhz;->a(Ljava/lang/String;Z)Lhhz;

    move-result-object v0

    invoke-virtual {v0}, Lhhz;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->i:Lgqp;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lgqp;->a(I[Ljava/lang/Object;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/gms/security/snet/SnetService;)Lgqn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->f:Lgqn;

    return-object v0
.end method

.method private b()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Lgqm;->a(Landroid/content/Context;)Z

    move-result v3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v0, v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/security/snet/SnetService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "package_verifier_user_consent"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    :try_start_0
    const-string v0, "com.android.vending"

    const/4 v4, 0x2

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/security/snet/SnetService;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    const-string v4, "package_verifer_public_preferences"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v4, "accepted-anti-malware-consent"

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private c()I
    .locals 4

    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/google/android/gms/security/snet/SnetService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/security/snet/SnetService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/gms/security/snet/SnetService;)Lgqp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->i:Lgqp;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/security/snet/SnetService;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/security/snet/SnetService;->h:I

    return v0
.end method

.method public static synthetic e(Lcom/google/android/gms/security/snet/SnetService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/security/snet/SnetService;->a()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {p0}, Lhhz;->a(Landroid/content/Context;)V

    invoke-static {}, Lgqn;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgqv;

    invoke-direct {v1, p0, p0, v0}, Lgqv;-><init>(Lcom/google/android/gms/security/snet/SnetService;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/security/snet/SnetService;->g:Lgqr;

    new-instance v0, Lgqn;

    invoke-direct {v0, p0}, Lgqn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->f:Lgqn;

    invoke-direct {p0}, Lcom/google/android/gms/security/snet/SnetService;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/security/snet/SnetService;->h:I

    new-instance v0, Lgqp;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/SnetService;->f:Lgqn;

    invoke-virtual {v1}, Lgqn;->f()J

    move-result-wide v1

    iget v3, p0, Lcom/google/android/gms/security/snet/SnetService;->h:I

    invoke-direct {v0, v1, v2, v3}, Lgqp;-><init>(JI)V

    iput-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->i:Lgqp;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->g:Lgqr;

    iget-object v0, v0, Lgqr;->a:Lsf;

    invoke-virtual {v0}, Lsf;->b()V

    sget-object v0, Lcom/google/android/gms/security/snet/SnetService;->a:Ljava/lang/String;

    const-string v1, "snet destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/security/snet/SnetService;->e:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/SnetService;->e:Z

    invoke-direct {p0}, Lcom/google/android/gms/security/snet/SnetService;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "snet_force_run"

    sget-object v1, Lcom/google/android/gms/security/snet/SnetService;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lhhz;->a(Ljava/lang/String;Z)Lhhz;

    move-result-object v0

    invoke-virtual {v0}, Lhhz;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "snet_service_remote_enable"

    sget-object v1, Lcom/google/android/gms/security/snet/SnetService;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lhhz;->a(Ljava/lang/String;Z)Lhhz;

    move-result-object v0

    invoke-virtual {v0}, Lhhz;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lgqq;->a(Landroid/content/Context;J)V

    invoke-static {}, Lgqn;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/security/snet/SnetService;->f:Lgqn;

    invoke-virtual {v1, v0}, Lgqn;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->g:Lgqr;

    invoke-virtual {v0}, Lgqr;->a()V

    :goto_0
    const/4 v0, 0x2

    return v0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/security/snet/SnetService;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/security/snet/SnetService;->a()V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/gms/security/snet/SnetService;->a:Ljava/lang/String;

    const-string v1, "snet re-entered."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
