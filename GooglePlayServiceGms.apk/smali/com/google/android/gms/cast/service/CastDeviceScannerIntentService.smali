.class public Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "CastDeviceScannerIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Laxb;)V
    .locals 1

    new-instance v0, Lbbf;

    invoke-direct {v0, p1}, Lbbf;-><init>(Laxb;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a(Landroid/content/Context;Lbaj;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lbaj;)V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    sget-object v0, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    const-string v0, "com.google.android.gms.cast.service.DEVICE_SCANNER_INTENT"

    invoke-static {v0}, Lbox;->f(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static b(Landroid/content/Context;Laxb;)V
    .locals 1

    new-instance v0, Lbbh;

    invoke-direct {v0, p1}, Lbbh;-><init>(Laxb;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a(Landroid/content/Context;Lbaj;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbaj;

    if-nez v0, :cond_0

    const-string v0, "CastDeviceScannerIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lbaj;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    throw v0
.end method
