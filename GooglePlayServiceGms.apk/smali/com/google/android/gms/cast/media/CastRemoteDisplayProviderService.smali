.class public final Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lazi;


# instance fields
.field private a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->stopForeground(Z)V

    return-void
.end method

.method public final a(ILandroid/app/Notification;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->startForeground(ILandroid/app/Notification;)V

    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.media.remotedisplay.RemoteDisplayProvider"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lazi;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lazi;)V

    iput-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method
