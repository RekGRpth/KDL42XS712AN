.class public Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;
.super Lcom/android/media/remotedisplay/RemoteDisplayProvider;
.source "SourceFile"


# static fields
.field private static final a:Laye;

.field private static b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;


# instance fields
.field private final c:Layl;

.field private final d:Ldf;

.field private e:Lazi;

.field private f:Lcom/android/media/remotedisplay/RemoteDisplay;

.field private g:Z

.field private h:Landroid/app/PendingIntent;

.field private final i:Lazg;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lbbj;

    const-string v1, "CastRemoteDisplayProvider"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Laye;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Layl;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/media/remotedisplay/RemoteDisplayProvider;-><init>(Landroid/content/Context;)V

    new-instance v0, Ldf;

    invoke-direct {v0}, Ldf;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d:Ldf;

    new-instance v0, Lazh;

    invoke-direct {v0, p0}, Lazh;-><init>(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->i:Lazg;

    iput-object p2, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Layl;

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Layl;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->i:Lazg;

    invoke-virtual {v0, v1}, Layl;->a(Lazg;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;
    .locals 4

    const-class v1, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0}, Layl;->a(Landroid/content/Context;)Layl;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;-><init>(Landroid/content/Context;Layl;)V

    sput-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d()V

    return-void
.end method

.method public static synthetic b()Laye;
    .locals 1

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Laye;

    return-object v0
.end method

.method static synthetic c()Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;
    .locals 1

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    return-object v0
.end method

.method private d()V
    .locals 9

    const v8, 0x7f0b0305    # com.google.android.gms.R.string.cast_display_notification_connecting_title

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Lazi;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/android/media/remotedisplay/RemoteDisplay;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getDisplays()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/media/remotedisplay/RemoteDisplay;

    invoke-virtual {v0}, Lcom/android/media/remotedisplay/RemoteDisplay;->getStatus()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_3

    if-ne v4, v6, :cond_2

    :cond_3
    move-object v3, v0

    :goto_1
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/android/media/remotedisplay/RemoteDisplay;->getStatus()I

    move-result v0

    if-ne v0, v6, :cond_7

    move v0, v1

    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/android/media/remotedisplay/RemoteDisplay;

    if-ne v4, v3, :cond_4

    iget-boolean v4, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->g:Z

    if-eq v4, v0, :cond_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/android/media/remotedisplay/RemoteDisplay;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Lazi;

    invoke-interface {v4}, Lazi;->a()V

    :cond_5
    iput-object v3, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/android/media/remotedisplay/RemoteDisplay;

    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->g:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v0, :cond_8

    new-instance v0, Lax;

    invoke-direct {v0, v4}, Lax;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lax;->b:Ljava/lang/CharSequence;

    const v4, 0x7f0b0306    # com.google.android.gms.R.string.cast_display_notification_connecting_message

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/android/media/remotedisplay/RemoteDisplay;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lax;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getSettingsPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v0, Lax;->d:Landroid/app/PendingIntent;

    const v2, 0x7f02013f    # com.google.android.gms.R.drawable.ic_notification_cast_connecting

    invoke-virtual {v0, v2}, Lax;->a(I)Lax;

    move-result-object v0

    invoke-virtual {v0, v7, v1}, Lax;->a(IZ)V

    const v1, 0x7f0b0309    # com.google.android.gms.R.string.cast_display_notification_disconnect

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lax;->a(Ljava/lang/CharSequence;Landroid/app/PendingIntent;)Lax;

    move-result-object v0

    invoke-virtual {v0}, Lax;->d()Landroid/app/Notification;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Lazi;

    invoke-interface {v1, v8, v0}, Lazi;->a(ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    new-instance v0, Lax;

    invoke-direct {v0, v4}, Lax;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b0307    # com.google.android.gms.R.string.cast_display_notification_connected_title

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lax;->b:Ljava/lang/CharSequence;

    const v4, 0x7f0b0308    # com.google.android.gms.R.string.cast_display_notification_connected_message

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/android/media/remotedisplay/RemoteDisplay;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lax;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getSettingsPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v0, Lax;->d:Landroid/app/PendingIntent;

    const v2, 0x7f020140    # com.google.android.gms.R.drawable.ic_notification_cast_on

    invoke-virtual {v0, v2}, Lax;->a(I)Lax;

    move-result-object v0

    invoke-virtual {v0, v7, v1}, Lax;->a(IZ)V

    const v1, 0x7f0b0309    # com.google.android.gms.R.string.cast_display_notification_disconnect

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lax;->a(Ljava/lang/CharSequence;Landroid/app/PendingIntent;)Lax;

    move-result-object v0

    invoke-virtual {v0}, Lax;->d()Landroid/app/Notification;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Lazi;

    const v2, 0x7f0b0307    # com.google.android.gms.R.string.cast_display_notification_connected_title

    invoke-interface {v1, v2, v0}, Lazi;->a(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method private e()Landroid/app/PendingIntent;
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->h:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.cast.media.ACTION_DISCONNECT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider$Receiver;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->h:Landroid/app/PendingIntent;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->h:Landroid/app/PendingIntent;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/android/media/remotedisplay/RemoteDisplay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/android/media/remotedisplay/RemoteDisplay;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->onDisconnect(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    :cond_0
    return-void
.end method

.method public final a(Lazi;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Lazi;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Lazi;

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d()V

    :cond_0
    return-void
.end method

.method public onAdjustVolume(Lcom/android/media/remotedisplay/RemoteDisplay;I)V
    .locals 3

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Laye;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAdjustVolume, display="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", delta="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d:Ldf;

    invoke-virtual {v0, p1}, Ldf;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layx;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Layx;->b(I)V

    :cond_0
    return-void
.end method

.method public onConnect(Lcom/android/media/remotedisplay/RemoteDisplay;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Laye;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConnect, display="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/android/media/remotedisplay/RemoteDisplay;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Layl;

    invoke-virtual {v1, v0}, Layl;->a(Ljava/lang/String;)Lod;

    move-result-object v0

    check-cast v0, Layx;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d:Ldf;

    invoke-virtual {v1, p1, v0}, Ldf;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Layx;->k:Laye;

    const-string v2, "Starting mirroring on device %s"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, v0, Layx;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v5, v0, Layx;->i:Z

    iput v5, v0, Layx;->f:I

    const/4 v1, 0x0

    iput-object v1, v0, Layx;->g:Ljava/lang/String;

    invoke-virtual {v0}, Layx;->d()V

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d()V

    :cond_0
    return-void
.end method

.method public onDisconnect(Lcom/android/media/remotedisplay/RemoteDisplay;)V
    .locals 5

    const/4 v4, 0x0

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Laye;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDisconnect, display="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d:Ldf;

    invoke-virtual {v0, p1}, Ldf;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layx;

    if-eqz v0, :cond_0

    iget-object v1, v0, Layx;->k:Laye;

    const-string v2, "Stopping mirroring on device"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Layx;->e()V

    :cond_0
    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d()V

    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Laye;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "not updating display status because it\'s already lost, display="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onDiscoveryModeChanged(I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Laye;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDiscoveryModeChanged, mode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eq p1, v4, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Layl;

    invoke-virtual {v0, v4}, Layl;->a(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Layl;

    invoke-virtual {v0, v3}, Layl;->a(Z)V

    goto :goto_0
.end method

.method public onSetVolume(Lcom/android/media/remotedisplay/RemoteDisplay;I)V
    .locals 3

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Laye;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSetVolume, display="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", volume="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d:Ldf;

    invoke-virtual {v0, p1}, Ldf;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layx;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Layx;->a(I)V

    :cond_0
    return-void
.end method
