.class public final Lcom/google/android/gms/identity/accounts/service/AccountDataService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Leoz;


# instance fields
.field private final b:Leoz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leoy;

    invoke-direct {v0}, Leoy;-><init>()V

    sput-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataService;->a:Leoz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataService;->a:Leoz;

    invoke-direct {p0, v0}, Lcom/google/android/gms/identity/accounts/service/AccountDataService;-><init>(Leoz;)V

    return-void
.end method

.method public constructor <init>(Leoz;)V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/identity/accounts/service/AccountDataService;->b:Leoz;

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.gms.accounts.ACCOUNT_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lepa;

    iget-object v1, p0, Lcom/google/android/gms/identity/accounts/service/AccountDataService;->b:Leoz;

    invoke-interface {v1, p0}, Leoz;->a(Landroid/content/Context;)Lepb;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lepa;-><init>(Landroid/content/Context;Lepb;)V

    invoke-virtual {v0}, Lepa;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
