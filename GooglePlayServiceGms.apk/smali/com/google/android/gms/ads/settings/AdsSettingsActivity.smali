.class public Lcom/google/android/gms/ads/settings/AdsSettingsActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field private o:Landroid/view/ViewGroup;

.field private final p:Ladk;

.field private final q:Ladn;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/view/ViewGroup;

.field private t:Landroid/view/ViewGroup;

.field private u:Landroid/view/ViewGroup;

.field private v:Landroid/widget/CheckBox;

.field private w:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljp;-><init>()V

    new-instance v0, Ladk;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Ladk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->p:Ladk;

    new-instance v0, Ladn;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Ladn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->q:Ladn;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->v:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup;Z)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Landroid/view/ViewGroup;Z)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Lbbu;)V
    .locals 3

    invoke-virtual {p1}, Lbbu;->a()I

    move-result v0

    invoke-static {v0, p0}, Lbbv;->a(ILandroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-static {v0, p0}, Lbbw;->a(Landroid/app/Dialog;Landroid/content/DialogInterface$OnCancelListener;)Lbbw;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Lbbw;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object v3, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    if-nez p1, :cond_2

    :goto_2
    invoke-static {v0, v1}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Landroid/view/ViewGroup;Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "?vv=5"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Ladr;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "&sig="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {p0, v1}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    const-class v0, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->finish()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    new-instance v0, Lada;

    invoke-direct {v0, p0, v1}, Lada;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;B)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lada;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->q:Ladn;

    invoke-virtual {v0}, Ladn;->d()V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->toggle()V

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    new-instance v3, Ladc;

    invoke-direct {v3, p0, v1}, Ladc;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;B)V

    new-array v4, v0, [Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v3, v4}, Ladc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v3, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->p:Ladk;

    if-nez v2, :cond_1

    :goto_0
    invoke-virtual {v3, v0}, Ladk;->a(Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->s:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->s:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ladq;

    invoke-direct {v0}, Ladq;-><init>()V

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "reset_dialog"

    invoke-virtual {v0, v1, v2}, Ladq;->a(Lu;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->u:Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->u:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lade;->d:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lade;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbfy;->a(Landroid/content/Context;)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Ljj;->a(Z)V

    invoke-virtual {v0, v2}, Ljj;->b(Z)V

    :goto_0
    const v0, 0x7f04001f    # com.google.android.gms.R.layout.ads_settings

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->setContentView(I)V

    const v0, 0x7f0a0083    # com.google.android.gms.R.id.settings_root

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->o:Landroid/view/ViewGroup;

    const v0, 0x7f0a0086    # com.google.android.gms.R.id.interest_based_ads

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    const v1, 0x7f0a0060    # com.google.android.gms.R.id.checkbox

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->v:Landroid/widget/CheckBox;

    const v0, 0x7f0a0084    # com.google.android.gms.R.id.advertising_id

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0a0089    # com.google.android.gms.R.id.adid

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->r:Landroid/widget/TextView;

    const v0, 0x7f0a0085    # com.google.android.gms.R.id.reset_advertising_id

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->s:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f0a0087    # com.google.android.gms.R.id.interest_based_ads_link

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->u:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f0a0088    # com.google.android.gms.R.id.ads_by_google_link

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljj;->a(Z)V

    goto/16 :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/16 v1, 0x42

    if-eq p2, v1, :cond_1

    const/16 v1, 0x17

    if-eq p2, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->performClick()Z

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->s:Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->s:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->performClick()Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->u:Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->u:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->performClick()Z

    move-result v0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->performClick()Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Ljp;->onResume()V

    new-instance v0, Ladb;

    invoke-direct {v0, p0, v1}, Ladb;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;B)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ladb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Ljp;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->p:Ladk;

    invoke-virtual {v0}, Ladk;->a()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->q:Ladn;

    invoke-virtual {v0}, Ladn;->a()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Ljp;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->p:Ladk;

    invoke-virtual {v0}, Ladk;->b()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->q:Ladn;

    invoke-virtual {v0}, Ladn;->b()V

    return-void
.end method
