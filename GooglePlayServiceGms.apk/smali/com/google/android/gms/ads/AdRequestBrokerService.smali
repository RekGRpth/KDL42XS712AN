.class public final Lcom/google/android/gms/ads/AdRequestBrokerService;
.super Landroid/app/Service;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "Binding to the ad request service."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.ads.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lxh;

    invoke-direct {v0, p0}, Lxh;-><init>(Lcom/google/android/gms/ads/AdRequestBrokerService;)V

    invoke-virtual {v0}, Lxh;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
