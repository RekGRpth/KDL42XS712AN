.class public Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;
.super Ljp;
.source "SourceFile"


# static fields
.field private static final o:Lbhl;

.field private static final p:[Ljava/lang/String;


# instance fields
.field private final q:Ljava/lang/Runnable;

.field private r:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lbhl;

    const-string v1, "__cats.jpg"

    const-string v2, "https://i.imgur.com/oNmqoJU.jpg"

    const-wide/32 v3, 0x11892

    const-string v5, "54cb88b9af7c5f2bd967165fe0c4c9b93a1089eb"

    const-string v6, "cats"

    invoke-direct/range {v0 .. v6}, Lbhl;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->o:Lbhl;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "text1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "text2"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->p:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljp;-><init>()V

    new-instance v0, Lbhm;

    invoke-direct {v0, p0}, Lbhm;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->q:Ljava/lang/Runnable;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->q:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic a(I)Ljava/lang/String;
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const-string v0, "Unknown"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "Failed"

    goto :goto_0

    :sswitch_1
    const-string v0, "Paused"

    goto :goto_0

    :sswitch_2
    const-string v0, "Pending"

    goto :goto_0

    :sswitch_3
    const-string v0, "Running"

    goto :goto_0

    :sswitch_4
    const-string v0, "Successful"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x4 -> :sswitch_1
        0x8 -> :sswitch_4
        0x10 -> :sswitch_0
    .end sparse-switch
.end method

.method public static synthetic b(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->r:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic f()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->p:[Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic g()Lbhl;
    .locals 1

    sget-object v0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->o:Lbhl;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    const v1, 0x102000a    # android.R.id.list

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setId(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->setContentView(Landroid/view/View;)V

    const-string v0, "Download Service debug"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->r:Landroid/os/Handler;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    const-string v0, "Start DownloadService Now"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lbhn;

    invoke-direct {v1, p0, p0}, Lbhn;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v0, "Enable Kitty"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lbho;

    invoke-direct {v1, p0, p0}, Lbho;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v0, "Disable Kitty"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lbhp;

    invoke-direct {v1, p0, p0}, Lbhp;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v0, "View Kitty"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lbhq;

    invoke-direct {v1, p0, p0}, Lbhq;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    invoke-super {p0, p1}, Ljp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Ljp;->onStart()V

    new-instance v0, Lbhs;

    invoke-direct {v0, p0, v1}, Lbhs;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;B)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbhs;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Ljp;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->r:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
