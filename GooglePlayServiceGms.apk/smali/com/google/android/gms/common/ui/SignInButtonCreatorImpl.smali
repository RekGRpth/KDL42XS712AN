.class public final Lcom/google/android/gms/common/ui/SignInButtonCreatorImpl;
.super Lbkc;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lbkc;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/content/res/Resources;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "com.google.android.gms"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final newSignInButton(Lcrv;II)Lcrv;
    .locals 3

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/ui/SignInButtonCreatorImpl;->a(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "Could not load GMS resources!"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v2, Lbko;

    invoke-direct {v2, v0}, Lbko;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1, p2, p3}, Lbko;->a(Landroid/content/res/Resources;II)V

    invoke-static {v2}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method
