.class public final Lcom/google/android/gms/common/images/internal/LoadingImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/common/images/ImageManager;


# instance fields
.field private b:Landroid/net/Uri;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d:Z

    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->e:Z

    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d:Z

    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->e:Z

    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d:Z

    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->e:Z

    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    return-void
.end method

.method public final a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    return-void
.end method

.method public final a(Landroid/net/Uri;I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a:Lcom/google/android/gms/common/images/ImageManager;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.google.android.play.games"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/gms/common/images/ImageManager;->a(Landroid/content/Context;Z)Lcom/google/android/gms/common/images/ImageManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a:Lcom/google/android/gms/common/images/ImageManager;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->e:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    if-eqz v0, :cond_4

    :cond_2
    move v0, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    new-instance v1, Lbig;

    invoke-direct {v1, p1}, Lbig;-><init>(Landroid/net/Uri;)V

    iput p2, v1, Lbig;->c:I

    iget-boolean v3, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d:Z

    iput-boolean v3, v1, Lbig;->j:Z

    iput-boolean v0, v1, Lbig;->k:Z

    if-eqz v0, :cond_3

    iput-boolean v2, v1, Lbig;->j:Z

    :cond_3
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    iput v0, v1, Lbig;->l:I

    invoke-static {p0}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object v4, v1, Lbig;->f:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, v1, Lbig;->g:Ljava/lang/ref/WeakReference;

    iput-object v4, v1, Lbig;->h:Ljava/lang/ref/WeakReference;

    const/4 v0, -0x1

    iput v0, v1, Lbig;->i:I

    const/4 v0, 0x2

    iput v0, v1, Lbig;->d:I

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, v1, Lbig;->e:I

    sget-object v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/ImageManager;->a(Lbig;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    return v0
.end method

.method public final b(I)V
    .locals 2

    const/4 v0, 0x0

    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_0
    iput v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    if-eqz v0, :cond_1

    sget-object v0, Lbij;->a:Landroid/graphics/ColorFilter;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->invalidate()V

    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b:Landroid/net/Uri;

    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d:Z

    return-void
.end method

.method public final d()V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    :cond_0
    return-void
.end method
