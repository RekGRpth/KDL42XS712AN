.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lgjq;


# static fields
.field public static final CREATOR:Lgjs;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final e:Ljava/util/Set;

.field private final f:I

.field private g:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

.field private h:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

.field private i:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lgjs;

    invoke-direct {v0}, Lgjs;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->CREATOR:Lgjs;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a:Ljava/util/HashMap;

    const-string v1, "actionType"

    const-string v2, "actionType"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a:Ljava/util/HashMap;

    const-string v1, "endView"

    const-string v2, "endView"

    const/4 v3, 0x4

    const-class v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a:Ljava/util/HashMap;

    const-string v1, "startView"

    const-string v2, "startView"

    const/16 v3, 0x14

    const-class v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a:Ljava/util/HashMap;

    const-string v1, "totalTimeMs"

    const-string v2, "totalTimeMs"

    const/16 v3, 0x18

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->f:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->e:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;ILcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->e:Ljava/util/Set;

    iput p2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->f:I

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object p4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->h:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object p5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput p6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->j:I

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->e:Ljava/util/Set;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->f:I

    iput-object p2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->h:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object p4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput p5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->j:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;I)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an int."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iput p3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->j:I

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->e:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->e:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void

    :sswitch_1
    check-cast p3, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->h:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    goto :goto_0

    :sswitch_2
    check-cast p3, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    :goto_0
    return-object v0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->h:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->j:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x14 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final b()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->e:Ljava/util/Set;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->f:I

    return v0
.end method

.method protected final c(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    return-object v0
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->CREATOR:Lgjs;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v0, p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;

    sget-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method public final bridge synthetic f()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public final g()Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->h:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->j:I

    return v0
.end method

.method public final q_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;->CREATOR:Lgjs;

    invoke-static {p0, p1, p2}, Lgjs;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;Landroid/os/Parcel;I)V

    return-void
.end method
