.class public final Lcom/google/android/gms/plus/service/DefaultIntentService;
.super Lgar;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/Intent;

.field private static b:Lgat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.service.default.INTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->a:Landroid/content/Intent;

    new-instance v0, Lgat;

    invoke-direct {v0}, Lgat;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->b:Lgat;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string v0, "DefaultIntentService"

    sget-object v1, Lcom/google/android/gms/plus/service/DefaultIntentService;->b:Lgat;

    invoke-direct {p0, v0, v1}, Lgar;-><init>(Ljava/lang/String;Lgat;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lgas;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->b:Lgat;

    invoke-virtual {v0, p1}, Lgat;->a(Lgas;)V

    sget-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "isLoggingIntent"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lfnq;

    invoke-direct {v0, p0, p1}, Lfnq;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Lgas;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lgar;->onHandleIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method
