.class public Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;
.super Lfyv;
.source "SourceFile"

# interfaces
.implements Lfyf;
.implements Lfyu;
.implements Lfyy;
.implements Lfza;
.implements Lfzf;


# instance fields
.field private q:Lfze;

.field private r:Lk;

.field private s:Landroid/support/v4/app/Fragment;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private x:Landroid/view/View;

.field private y:Landroid/graphics/drawable/Drawable;

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfyv;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.oob.EXTRAS_CLIENT_CALLING_APP_PACKAGE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.OVERRIDE_THEME"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->e()V

    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;->j()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v0

    invoke-static {v0, p2}, Lfyz;->a(ZLcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)Lfyz;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lfyz;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lfyz;)V
    .locals 2

    invoke-super {p0, p1}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-super {p0}, Lfyv;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    sget-object v1, Lbcr;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v0, v1}, Lfyv;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-direct {p0, p2}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lfyz;)V

    return-void
.end method

.method private a(Lfyz;)V
    .locals 4

    const v3, 0x7f0a02ae    # com.google.android.gms.R.id.content_layout

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "content_fragment"

    invoke-virtual {v0, v2}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "content_fragment"

    invoke-virtual {v1, v3, p1, v0}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :goto_0
    invoke-virtual {v1}, Lag;->d()I

    iput-object p1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    return-void

    :cond_0
    const-string v0, "content_fragment"

    invoke-virtual {v1, v3, p1, v0}, Lag;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Lfys;->a(Ljava/lang/String;Ljava/lang/String;)Lfys;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->r:Lk;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->r:Lk;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "progress_dialog_fragment"

    invoke-virtual {v0, v1, v2}, Lk;->a(Lu;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p1}, Lgqe;->a(Ljava/lang/CharSequence;)Lgqe;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->r:Lk;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->r:Lk;

    const/4 v1, 0x1

    const v2, 0x7f100188    # com.google.android.gms.R.style.common_Activity_Light_Dialog

    invoke-virtual {v0, v1, v2}, Lk;->a(II)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 5

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfo;

    invoke-interface {v0}, Lgfo;->l()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Lgfo;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    sget-object v0, Lbcq;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_2
    return-void

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    sget-object v0, Lbcq;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v1

    invoke-static {v1, p1}, Lfyz;->a(ZLjava/util/ArrayList;)Lfyz;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lfyz;)V

    goto :goto_2
.end method

.method private c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v0

    const v1, 0x7f0b03fc    # com.google.android.gms.R.string.plus_oob_failure_text

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1, v2}, Lfyz;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfyz;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lfyz;)V

    return-void
.end method

.method private j()V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->z:I

    iget v1, p0, Lfyv;->p:I

    if-nez v1, :cond_3

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->y:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->y:Landroid/graphics/drawable/Drawable;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x106000d    # android.R.color.transparent

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0b03f4    # com.google.android.gms.R.string.plus_oob_interstitial_loading

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b03f5    # com.google.android.gms.R.string.plus_oob_interstitial_loading_body

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const v0, 0x7f0b03f3    # com.google.android.gms.R.string.plus_oob_loading

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private k()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->z:I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0b03f7    # com.google.android.gms.R.string.plus_oob_interstitial_sending

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b03f8    # com.google.android.gms.R.string.plus_oob_interstitial_sending_body

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0b03f6    # com.google.android.gms.R.string.plus_oob_sending

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private l()Z
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "progress_dialog_fragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->z:I

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->n()V

    return-void
.end method

.method private n()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->r:Lk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->r:Lk;

    invoke-virtual {v0}, Lk;->b()V

    iput-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->r:Lk;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->y:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_2
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "progress_dialog_fragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lk;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lk;->b()V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;Lgfp;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2}, Lfyv;->a(Ljava/lang/String;Lgfp;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lgfp;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lfyv;->a(Ljava/lang/String;Lgfp;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lgfr;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2}, Lfyv;->a(Ljava/lang/String;Lgfr;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lgfr;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lfyv;->a(Ljava/lang/String;Lgfr;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lglo;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2}, Lfyv;->a(Ljava/lang/String;Lglo;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lglo;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lfyv;->a(Ljava/lang/String;Lglo;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lfyw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    check-cast v0, Lfyw;

    invoke-virtual {v0}, Lfyw;->J()V

    :cond_0
    return-void
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->m()V

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_2

    :cond_0
    sget-object v0, Lbcq;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {p2}, Lfzd;->b(Lgll;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v1, Lbcq;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->j()Lgln;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)V

    goto :goto_0

    :cond_3
    invoke-static {p2}, Lfzd;->a(Lgll;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "UpgradeAccount"

    const-string v1, "Account is already upgraded to G+"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lbcq;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v0}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->h()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lfyw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    check-cast v0, Lfyw;

    invoke-virtual {v0, p2}, Lfyw;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    invoke-super {p0, p1}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->k()V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->q:Lfze;

    invoke-interface {v0, p1}, Lfze;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    return-void
.end method

.method public final bridge synthetic b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    invoke-super {p0}, Lfyv;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbbo;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->m()V

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    sget-object v0, Lbcq;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Lfzd;->a(Lgll;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lbcq;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v0}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->h()V

    goto :goto_0

    :cond_2
    invoke-static {p2}, Lfzd;->b(Lgll;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v1, Lbcq;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->j()Lgln;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)V

    goto :goto_0

    :cond_3
    invoke-static {p2}, Lfzd;->d(Lgll;)Lgfm;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_a

    invoke-static {p2}, Lfzd;->d(Lgll;)Lgfm;

    move-result-object v0

    invoke-interface {v0}, Lgfm;->d()Ljava/util/List;

    move-result-object v3

    invoke-static {v0}, Lfzd;->a(Lgfm;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_8

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfo;

    invoke-interface {v0}, Lgfo;->g()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v0}, Lgfo;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "invalidNameHardFail"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v1, Lbcq;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v1}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_3
    invoke-interface {v0}, Lgfo;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbcr;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v1}, Lfyv;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v1

    invoke-static {v1, p2, v0}, Lfyt;->a(ZLcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;Ljava/lang/String;)Lfyt;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const v2, 0x7f0a02ae    # com.google.android.gms.R.id.content_layout

    const-string v3, "content_fragment"

    invoke-virtual {v1, v2, v0, v3}, Lag;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lag;->a(Ljava/lang/String;)Lag;

    invoke-virtual {v1}, Lag;->c()I

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    const-string v2, "invalidNameAppealable"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lbcq;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v1}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_3

    :cond_6
    sget-object v1, Lbcq;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v1}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_3

    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_8
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    :goto_4
    if-ge v1, v4, :cond_9

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ErrorsEntity;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_9
    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    :cond_a
    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "UpgradeAccount"

    const-string v1, "Unhandled error result"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    sget-object v0, Lbcq;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_0
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    invoke-super {p0, p1}, Lfyv;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lfyv;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final e()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->i()V

    return-void
.end method

.method public final f()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->onBackPressed()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "content_fragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lfyw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    check-cast v0, Lfyw;

    invoke-virtual {v0}, Lfyw;->J()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lfyv;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "UpgradeAccountActivity.dialogStateKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->z:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.oob.EXTRAS_PROMO_APP_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->t:Ljava/lang/String;

    const-string v1, "com.google.android.gms.common.oob.EXTRAS_PROMO_APP_TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->u:Ljava/lang/String;

    const-string v1, "com.google.android.gms.common.oob.EXTRA_BACK_BUTTON_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->v:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->w:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->w:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {p0, v0}, Lfmr;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->x:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    const v0, 0x7f0400f8    # com.google.android.gms.R.layout.plus_oob_upgrade_account_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->setContentView(I)V

    invoke-static {p0}, Lbpu;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "debug_no_connection"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v0

    const v1, 0x7f0b03e5    # com.google.android.gms.R.string.plus_oob_no_connection_title

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b03e6    # com.google.android.gms.R.string.plus_oob_no_connection_message

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a    # android.R.string.ok

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lfyz;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfyz;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lfyz;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "content_fragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->t:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->u:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->o:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->v:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v5}, Lfyw;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfyw;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a02ae    # com.google.android.gms.R.id.content_layout

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->s:Landroid/support/v4/app/Fragment;

    const-string v3, "content_fragment"

    invoke-virtual {v0, v1, v2, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->j()V

    :cond_4
    iget-object v1, p0, Lo;->b:Lw;

    const-string v0, "upgrade_account_fragment"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lfzg;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->o:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->w:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {v0, v2, v3}, Lfzg;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)Lfzg;

    move-result-object v0

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "upgrade_account_fragment"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v1

    invoke-virtual {v1}, Lag;->c()I

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->q:Lfze;

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lfzg;->a()Lfze;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->q:Lfze;

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lfyv;->onPause()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->n()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lfyv;->onResume()V

    iget v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->z:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->j()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->k()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lfyv;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "UpgradeAccountActivity.dialogStateKey"

    iget v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
