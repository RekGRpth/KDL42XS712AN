.class public final Lcom/google/android/gms/plus/plusone/PlusOneActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private n:Lfzx;

.field private o:Landroid/os/Bundle;

.field private p:Ljava/lang/String;

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method private e()V
    .locals 4

    const v3, 0x7f0a0293    # com.google.android.gms.R.id.plus_one_container

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v2, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0x0

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    const v0, 0x7f0400dd    # com.google.android.gms.R.layout.plus_one_activity_constrained

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setContentView(I)V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "PlusOneActivity#Fragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lfzx;

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    if-nez v0, :cond_1

    new-instance v0, Lfzx;

    invoke-direct {v0}, Lfzx;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->o:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lfzx;->g(Landroid/os/Bundle;)V

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    const-string v2, "PlusOneActivity#Fragment"

    invoke-virtual {v0, v3, v1, v2}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    const v0, 0x7f0a0292    # com.google.android.gms.R.id.frame_container

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_2
    iget v2, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0x3

    if-eqz v2, :cond_3

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const v0, 0x7f0400dc    # com.google.android.gms.R.layout.plus_one_activity

    goto :goto_1
.end method


# virtual methods
.method protected final h_()V
    .locals 1

    invoke-super {p0}, Lo;->h_()V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->e()V

    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lo;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    invoke-virtual {v0, p2}, Lfzx;->c(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->q:Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Lo;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    if-eqz v0, :cond_0

    const v0, 0x7f0a0194    # com.google.android.gms.R.id.progress_spinner

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    invoke-virtual {v1, v0}, Lfzx;->a(Landroid/widget/ProgressBar;)V

    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    invoke-virtual {v0}, Lfzx;->a()V

    :cond_0
    invoke-super {p0}, Lo;->onBackPressed()V

    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0292    # com.google.android.gms.R.id.frame_container

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->n:Lfzx;

    invoke-virtual {v0}, Lfzx;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    :cond_1
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 11

    const/4 v10, 0x2

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "com.google.android.gms.plus.intent.extra.TOKEN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.gms.plus.intent.extra.URL"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.google.android.gms.plus.intent.extra.EXTRA_SIGNED_UP"

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v7, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->p:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    const-string v0, "PlusOneActivity"

    const-string v1, "Intent missing required arguments"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->p:Ljava/lang/String;

    if-nez v7, :cond_3

    const-string v7, "<<default account>>"

    iput-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->p:Ljava/lang/String;

    :cond_3
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iput-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->o:Landroid/os/Bundle;

    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->o:Landroid/os/Bundle;

    const-string v8, "PlusOneFragment#mCallingPackage"

    invoke-virtual {v7, v8, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->o:Landroid/os/Bundle;

    const-string v8, "PlusOneFragment#mAccount"

    iget-object v9, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->p:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->o:Landroid/os/Bundle;

    const-string v8, "PlusOneFragment#mUrl"

    invoke-virtual {v7, v8, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->o:Landroid/os/Bundle;

    const-string v7, "PlusOneFragment#mApplyPlusOne"

    const-string v8, "com.google.android.gms.plus.action.PLUS_ONE"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v3, v7, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->o:Landroid/os/Bundle;

    const-string v3, "PlusOneFragment#mToken"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    invoke-static {p0, v0}, Lfmr;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    if-nez p1, :cond_5

    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    if-nez v5, :cond_4

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->q:Z

    :goto_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->q:Z

    if-eqz v0, :cond_7

    if-nez p1, :cond_1

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->p:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->k()Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->d()V

    :goto_3
    sget-object v1, Lfzx;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ge v6, v1, :cond_6

    sget-object v1, Lfzx;->a:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_4
    move v0, v6

    goto :goto_1

    :cond_5
    const-string v0, "needs_sign_in"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->q:Z

    goto :goto_2

    :cond_6
    new-instance v1, Lfmu;

    invoke-direct {v1, p0, v0}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput v10, v1, Lfmu;->b:I

    invoke-virtual {v1}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v10}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->e()V

    goto/16 :goto_0
.end method

.method public final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b034d    # com.google.android.gms.R.string.plus_one_error

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a    # android.R.string.ok

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "needs_sign_in"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
