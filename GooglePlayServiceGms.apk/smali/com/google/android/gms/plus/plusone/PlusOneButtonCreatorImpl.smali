.class public final Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;
.super Lfti;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfti;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/content/res/Resources;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "com.google.android.gms"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0, p0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "PlusOneButtonCreatorImpl"

    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final a(Lcrv;IILjava/lang/String;)Lcrv;
    .locals 2

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;->a(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "Could not load GMS resources!"

    invoke-static {v1}, Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;->a(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lfzk;

    invoke-direct {v1, v0, p2, p3, p4}, Lfzk;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    invoke-static {v1}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcrv;IILjava/lang/String;Ljava/lang/String;)Lcrv;
    .locals 6

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;->a(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Could not load GMS resources!"

    invoke-static {v0}, Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;->a(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lfzm;

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lfzm;-><init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method
