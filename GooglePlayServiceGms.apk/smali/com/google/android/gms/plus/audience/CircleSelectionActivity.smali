.class public Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.super Lfqd;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfqd;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lfqy;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->m()Lfqq;

    move-result-object v0

    return-object v0
.end method

.method protected e()I
    .locals 1

    const v0, 0x7f0b0357    # com.google.android.gms.R.string.plus_audience_selection_title_update_circles

    return v0
.end method

.method protected h()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    sget-object v0, Lfny;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method protected m()Lfqq;
    .locals 4

    iget-object v0, p0, Lfqd;->n:Ljava/lang/String;

    iget-object v1, p0, Lfqd;->o:Ljava/lang/String;

    iget-object v2, p0, Lfqd;->q:Ljava/lang/String;

    iget-object v3, p0, Lfqd;->p:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lfqq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfqq;

    move-result-object v0

    return-object v0
.end method
