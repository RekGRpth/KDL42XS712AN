.class public final Lcom/google/android/gms/plus/audience/AclSelectionActivity;
.super Lfqd;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private t:Lfpt;

.field private u:Landroid/view/View;

.field private v:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfqd;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lfqy;
    .locals 17

    const-string v2, "EXTRA_SEARCH_DEVICE"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->v:Z

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    instance-of v2, v0, Lfpt;

    if-eqz v2, :cond_0

    check-cast p2, Lfpt;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lfpt;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lfpt;

    return-object v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lfqd;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfqd;->o:Ljava/lang/String;

    const-string v4, "SHOULD_LOAD_SUGGESTED"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "SHOULD_LOAD_GROUPS"

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "LOAD_CIRCLES"

    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v7, "LOAD_PEOPLE"

    const/4 v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string v8, "DESCRIPTION_TEXT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.google.android.gms.common.acl.EXTRA_CLIENT_APPLICATION_ID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lfqd;->p:Ljava/lang/String;

    const-string v11, "com.google.android.gms.common.acl.EXTRA_DOMAIN_RESTRICTED"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    const-string v12, "EXTRA_INCLUDE_SUGGESTIONS_WITH_PEOPLE"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    const-string v13, "EXTRA_MAX_SUGGESTED_IMAGES"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const-string v14, "EXTRA_MAX_SUGGESTED_LIST_ITEMS"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    const-string v15, "EXTRA_MAX_SUGGESTED_DEVICE"

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Lfpt;->a(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIIILjava/util/List;)Lfpt;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lfpt;

    goto :goto_0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lfqd;->a(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400cd    # com.google.android.gms.R.layout.plus_audience_selection_title_search

    const v0, 0x7f0a026f    # com.google.android.gms.R.id.title_frame

    invoke-virtual {p0, v0}, Lfqd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->u:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->u:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lfpt;

    iget-boolean v1, p0, Lfqd;->r:Z

    invoke-virtual {v0, v1}, Lfpt;->a(Z)V

    return-void
.end method

.method protected final a(Lfec;)V
    .locals 1

    invoke-super {p0, p1}, Lfqd;->a(Lfec;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lfpt;

    invoke-virtual {v0, p1}, Lfpt;->a(Lfec;)V

    return-void
.end method

.method protected final e()I
    .locals 1

    const v0, 0x7f0b0355    # com.google.android.gms.R.string.plus_audience_selection_title_acl

    return v0
.end method

.method protected final f()V
    .locals 2

    sget-object v0, Lbcz;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->l()Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    invoke-super {p0}, Lfqd;->f()V

    return-void
.end method

.method protected final g()V
    .locals 2

    sget-object v0, Lbcz;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    invoke-super {p0}, Lfqd;->g()V

    return-void
.end method

.method protected final h()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    sget-object v0, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method protected final i()Lbew;
    .locals 2

    invoke-super {p0}, Lfqd;->i()Lbew;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lfpt;

    invoke-virtual {v1}, Lfpt;->K()I

    move-result v1

    invoke-virtual {v0, v1}, Lbew;->a(I)Lbew;

    move-result-object v0

    return-object v0
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    sget-object v0, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lbda;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-static {p3}, Lbew;->b(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v1, p0, Lfqd;->s:Lfrb;

    iget-object v2, p0, Lfqd;->s:Lfrb;

    iget-object v2, v2, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v2, v0}, Lblc;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    invoke-super {p0, p1}, Lfqd;->onClick(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->u:Landroid/view/View;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->onSearchRequested()Z

    :cond_0
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 4

    new-instance v0, Lbew;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Lbew;-><init>(Landroid/content/Intent;)V

    iget-object v1, p0, Lfqd;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbew;->e(Ljava/lang/String;)Lbew;

    move-result-object v0

    iget-object v1, p0, Lfqd;->o:Ljava/lang/String;

    iget-object v2, v0, Lbew;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms.common.acl.EXTRA_PLUS_PAGE_ID"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lfqd;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbew;->f(Ljava/lang/String;)Lbew;

    move-result-object v0

    const v1, 0x7f0b0358    # com.google.android.gms.R.string.plus_audience_selection_title_search

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbew;->g(Ljava/lang/String;)Lbew;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->v:Z

    iget-object v2, v0, Lbew;->a:Landroid/content/Intent;

    const-string v3, "EXTRA_SEARCH_DEVICE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, v0, Lbew;->a:Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->startActivityForResult(Landroid/content/Intent;I)V

    sget-object v0, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lbda;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const/4 v0, 0x0

    return v0
.end method
