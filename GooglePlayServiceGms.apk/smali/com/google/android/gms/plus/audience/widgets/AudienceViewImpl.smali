.class public final Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Ljava/util/LinkedHashMap;

.field private final f:Lbkt;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Lbfg;

.field private j:Lcom/google/android/gms/common/people/data/Audience;

.field private k:I

.field private l:I

.field private final m:Ljava/lang/String;

.field private final n:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Context;Lbfg;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    new-instance v0, Lbkt;

    const/high16 v1, 0x200000

    invoke-direct {v0, v1}, Lbkt;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->f:Lbkt;

    iput-object p3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->i:Lbfg;

    iput-object p2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->n:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f04003c    # com.google.android.gms.R.layout.common_audience_view

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a00cb    # com.google.android.gms.R.id.audience_view_list

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    const v0, 0x7f0a00ca    # com.google.android.gms.R.id.audience_view_editable

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/widget/ImageView;

    const v0, 0x7f0a00cc    # com.google.android.gms.R.id.audience_view_text

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    const v0, 0x7f0b03d2    # com.google.android.gms.R.string.plus_sharebox_audience_view_name_separator

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->m:Ljava/lang/String;

    invoke-virtual {p0, p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setSaveEnabled(Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;)Lbkt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->f:Lbkt;

    return-object v0
.end method

.method private a()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iput v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->setVisibility(I)V

    :goto_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->invalidate()V

    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->k:I

    iput v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->setVisibility(I)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setClickable(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b(Z)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setClickable(Z)V

    invoke-virtual {p0, p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b(Z)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setClickable(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b(Z)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    return-void
.end method

.method private static b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xff

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    return-void
.end method

.method private b(Z)V
    .locals 7

    const/4 v5, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f0a00d1    # com.google.android.gms.R.id.chip_close

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "checkboxEnabled"

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v5

    :goto_1
    if-nez v2, :cond_1

    move v4, v3

    :goto_2
    if-eqz v4, :cond_2

    move v2, v3

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v4, :cond_3

    move-object v1, p0

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    :cond_0
    move v2, v3

    goto :goto_1

    :cond_1
    move v4, p1

    goto :goto_2

    :cond_2
    const/16 v2, 0x8

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    goto :goto_4

    :cond_4
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->k:I

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 14

    const v5, 0x7f02008c    # com.google.android.gms.R.drawable.common_ic_acl_circles

    const v7, 0x7f020086    # com.google.android.gms.R.drawable.common_acl_chip_green

    const v8, 0x7f020083    # com.google.android.gms.R.drawable.common_acl_chip_blue

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AudienceMember can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f04003d    # com.google.android.gms.R.layout.common_audience_view_chip

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    const v1, 0x7f0a00ce    # com.google.android.gms.R.id.chip_background

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    const v1, 0x7f0a00d0    # com.google.android.gms.R.id.chip_text

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00cf    # com.google.android.gms.R.id.chip_icon

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v10, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const/16 v3, 0x66

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setAlpha(I)V

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->n:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown audience member type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    goto :goto_1

    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown circle type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_2
    const v4, 0x7f0b045d    # com.google.android.gms.R.string.common_chips_label_public

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020091    # com.google.android.gms.R.drawable.common_ic_acl_public

    move v6, v7

    :goto_2
    new-instance v12, Lfrv;

    const/4 v13, 0x0

    invoke-direct {v12, v6, v3, v4, v13}, Lfrv;-><init>(ILjava/lang/String;IB)V

    iget v3, v12, Lfrv;->a:I

    invoke-virtual {v11, v3}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v3, v12, Lfrv;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, v12, Lfrv;->c:I

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->f:Lbkt;

    invoke-virtual {v1, v3}, Lbkt;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->i:Lbfg;

    new-instance v4, Lfrt;

    invoke-direct {v4, p0, v3, v2}, Lfrt;-><init>(Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;Ljava/lang/String;Landroid/widget/ImageView;)V

    const/4 v2, 0x1

    const/4 v6, 0x1

    invoke-interface {v1, v4, v3, v2, v6}, Lbfg;->a(Lbfj;Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_3
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v10}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->invalidate()V

    goto/16 :goto_0

    :pswitch_3
    const v4, 0x7f0b045f    # com.google.android.gms.R.string.common_chips_label_extended_circles

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02008e    # com.google.android.gms.R.drawable.common_ic_acl_extended

    move v6, v7

    goto :goto_2

    :pswitch_4
    const v4, 0x7f0b045e    # com.google.android.gms.R.string.common_chips_label_your_circles

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move v4, v5

    move v6, v8

    goto :goto_2

    :pswitch_5
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02008d    # com.google.android.gms.R.drawable.common_ic_acl_domain

    move v6, v7

    goto :goto_2

    :pswitch_6
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    move v4, v5

    move v6, v8

    goto :goto_2

    :pswitch_7
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020090    # com.google.android.gms.R.drawable.common_ic_acl_person

    move v6, v8

    goto :goto_2

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    const v1, 0x7f0b0461    # com.google.android.gms.R.string.common_chips_label_empty_circles

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a()V

    return-void

    :catch_0
    move-exception v1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    const v1, 0x7f0b0461    # com.google.android.gms.R.string.common_chips_label_empty_circles

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->i:Lbfg;

    invoke-interface {v0}, Lbfg;->a()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->i:Lbfg;

    invoke-interface {v1, v0}, Lbfg;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v0, "parent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "showEmptyText"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "parent"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "showEmptyText"

    iget-boolean v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "audience"

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method
