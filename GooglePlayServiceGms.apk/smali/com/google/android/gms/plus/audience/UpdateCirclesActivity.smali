.class public final Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;
.super Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfao;
.implements Lfri;
.implements Lgqf;


# instance fields
.field private final t:Lftz;

.field private u:Ljava/lang/String;

.field private v:Z

.field private w:Lfaj;

.field private x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

.field private y:Lbbo;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;-><init>(Lftz;)V

    return-void
.end method

.method constructor <init>(Lftz;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->t:Lftz;

    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3

    iget-object v0, p0, Lfqd;->s:Lfrb;

    iget-object v0, v0, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lbew;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private o()I
    .locals 2

    iget-object v0, p0, Lfqd;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private p()Z
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    invoke-virtual {v0}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbew;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-direct {v0, v1}, Lbew;-><init>(Landroid/content/Intent;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lbew;->a(Ljava/util/ArrayList;)Lbew;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lbew;->b(Ljava/util/ArrayList;)Lbew;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lbew;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lbew;->c(Ljava/util/List;)Lbew;

    move-result-object v0

    iget-object v0, v0, Lbew;->a:Landroid/content/Intent;

    const v1, 0x7f0b036a    # com.google.android.gms.R.string.plus_update_circles_failed_message

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lfrg;->a(Ljava/lang/String;Landroid/content/Intent;)Lfrg;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "errorDialog"

    invoke-virtual {v0, v1, v2}, Lfrg;->a(Lu;Ljava/lang/String;)V

    move v0, v6

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfqd;->n:Ljava/lang/String;

    iget-object v1, p0, Lfqd;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->o()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lfsm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v6

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 7

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lfqd;->n:Ljava/lang/String;

    iget-object v1, p0, Lfqd;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Ljava/lang/String;

    iget-object v5, p0, Lfqd;->q:Ljava/lang/String;

    iget-object v6, p0, Lfqd;->p:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lfrh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Lfrh;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "updateCircles"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v1

    invoke-virtual {v1}, Lag;->d()I

    invoke-virtual {v0}, Lfrh;->a()V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->w:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    :cond_0
    return-void
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 6

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpdateCirclesActivity"

    const-string v1, "Update person ID must not be null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->j()V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_2

    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->t:Lftz;

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->o()I

    move-result v4

    iget-object v5, p0, Lfqd;->p:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-interface/range {v0 .. v5}, Lftz;->a(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)Lfaj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->w:Lfaj;

    goto :goto_0

    :cond_2
    const-string v0, "hasLoggedCircleLoad"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    const-string v0, "addToCircleConsentData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    const-string v0, "addToCircleConsentDataResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "addToCircleConsentDataResultIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Lbbo;

    const-string v0, "addToCircleConsentDataResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "addToCircleConsentDataResultIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v0}, Lbbo;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    goto :goto_1
.end method

.method public final a(Lbbo;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    return-void
.end method

.method public final a(Lbbo;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "progressDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgqe;

    invoke-virtual {v0}, Lgqe;->b()V

    new-instance v0, Lbew;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-direct {v0, v1}, Lbew;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, p2}, Lbew;->a(Ljava/util/ArrayList;)Lbew;

    move-result-object v0

    invoke-virtual {v0, p3}, Lbew;->b(Ljava/util/ArrayList;)Lbew;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lbew;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lbew;->c(Ljava/util/List;)Lbew;

    move-result-object v0

    iget-object v0, v0, Lbew;->a:Landroid/content/Intent;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0b036a    # com.google.android.gms.R.string.plus_update_circles_failed_message

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lfrg;->a(Ljava/lang/String;Landroid/content/Intent;)Lfrg;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "errorDialog"

    invoke-virtual {v0, v1, v2}, Lfrg;->a(Lu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbbo;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->p()Z

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcb;Lfed;)V
    .locals 5

    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->a(Lcb;Lfed;)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfqd;->n:Ljava/lang/String;

    sget-object v2, Lfnx;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lfny;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfqd;->p:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Lcb;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lfed;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Lcb;Lfed;)V

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->w:Lfaj;

    iget-object v1, p0, Lfqd;->n:Ljava/lang/String;

    iget-object v2, p0, Lfqd;->o:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2}, Lfaj;->a(Lfao;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final f()V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfqd;->n:Ljava/lang/String;

    sget-object v2, Lfnx;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lfny;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfqd;->p:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-gtz v0, :cond_1

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0b0369    # com.google.android.gms.R.string.plus_update_circles_progress

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, Lgqe;->a(Ljava/lang/CharSequence;Z)Lgqe;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "progressDialog"

    invoke-virtual {v0, v1, v2}, Lgqe;->a(Lu;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->p()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->q()V

    goto :goto_0

    :cond_3
    iput-boolean v5, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Z

    goto :goto_0
.end method

.method public final g()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfqd;->n:Ljava/lang/String;

    sget-object v2, Lfnx;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lfny;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfqd;->p:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->g()V

    return-void
.end method

.method protected final h()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    sget-object v0, Lfny;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final n()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->g()V

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v1, 0x1

    const/16 v0, 0x7d0

    if-ne p1, v0, :cond_2

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    if-eq p2, v1, :cond_0

    if-ne p2, v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->q()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->g()V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "hasLoggedCircleLoad"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "addToCircleConsentData"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    if-eqz v0, :cond_0

    const-string v0, "addToCircleConsentDataResultCode"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    invoke-virtual {v1}, Lbbo;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "addToCircleConsentDataResultIntent"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lbbo;

    invoke-virtual {v1}, Lbbo;->d()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->w:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method public final onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->w:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onStop()V

    return-void
.end method
