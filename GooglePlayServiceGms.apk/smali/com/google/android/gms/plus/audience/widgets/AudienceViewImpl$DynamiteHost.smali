.class public Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;
.super Lbfe;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lbfe;-><init>()V

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "call initialize() first"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcrv;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(I)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    const-string v1, "state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    return-void
.end method

.method public final a(Lcrv;Lcrv;Lbfg;)V
    .locals 3

    new-instance v2, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p2}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1, p3}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;-><init>(Landroid/content/Context;Landroid/content/Context;Lbfg;)V

    iput-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Z)V

    return-void
.end method

.method public final b()Landroid/os/Bundle;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "state"

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method
