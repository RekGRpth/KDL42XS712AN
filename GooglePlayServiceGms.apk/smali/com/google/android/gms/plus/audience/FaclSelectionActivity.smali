.class public final Lcom/google/android/gms/plus/audience/FaclSelectionActivity;
.super Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.source "SourceFile"


# static fields
.field private static final t:Ljava/util/Comparator;


# instance fields
.field private u:Z

.field private v:Z

.field private w:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfrd;

    invoke-direct {v0}, Lfrd;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->t:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;-><init>()V

    return-void
.end method

.method public static synthetic q()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->t:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lfqy;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->m()Lfqq;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez p1, :cond_1

    const-string v1, "EVERYONE_CHECKED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    :goto_0
    const-string v1, "DESCRIPTION_TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b0394    # com.google.android.gms.R.string.plus_facl_default_description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    const-string v1, "FaclSelectionActivity.Everyone"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    const-string v1, "FaclSelectionActivity.CirclesHidden"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    return-void
.end method

.method public final c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    return-void
.end method

.method protected final e()I
    .locals 1

    const v0, 0x7f0b0391    # com.google.android.gms.R.string.plus_choose_facl_title

    return v0
.end method

.method protected final h()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    sget-object v0, Lbck;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method protected final i()Lbew;
    .locals 2

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->i()Lbew;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    invoke-virtual {v0, v1}, Lbew;->a(Z)Lbew;

    move-result-object v0

    return-object v0
.end method

.method protected final m()Lfqq;
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lfqd;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfqd;->o:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lfqd;->q:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lfqd;->p:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v1 .. v16}, Lfqq;->a(Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    new-instance v2, Lfre;

    invoke-direct {v2}, Lfre;-><init>()V

    invoke-virtual {v2, v1}, Lfre;->g(Landroid/os/Bundle;)V

    return-object v2
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    return v0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "FaclSelectionActivity.Everyone"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "FaclSelectionActivity.CirclesHidden"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Ljava/lang/String;

    return-object v0
.end method
