.class public Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lfxh;


# static fields
.field public static final CREATOR:Lfwn;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/pm/ApplicationInfo;

.field private final f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

.field private final g:Z

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfwn;

    invoke-direct {v0}, Lfwn;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->CREATOR:Lfwn;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->e:Landroid/content/pm/ApplicationInfo;

    iput-object p6, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-boolean p7, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    iput-object p8, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lfxh;)V
    .locals 8

    invoke-interface {p1}, Lfxh;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lfxh;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lfxh;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lfxh;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    invoke-interface {p1}, Lfxh;->i()Z

    move-result v5

    invoke-interface {p1}, Lfxh;->j()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1}, Lfxh;->k()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 10

    const/4 v1, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;B)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lfxh;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    :goto_0
    return-object p0

    :cond_0
    instance-of v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Lfxh;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic f()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public final g()Landroid/content/pm/ApplicationInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->e:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final q_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lfwn;->a(Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Landroid/os/Parcel;I)V

    return-void
.end method
