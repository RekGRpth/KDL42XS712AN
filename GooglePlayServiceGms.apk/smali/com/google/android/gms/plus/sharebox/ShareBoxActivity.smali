.class public Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lfqr;
.implements Lfrc;
.implements Lgnx;
.implements Lgok;
.implements Lgon;
.implements Lgoq;
.implements Lgpp;
.implements Lgpw;
.implements Lgpz;


# instance fields
.field protected n:Lgpn;

.field protected o:Lgpi;

.field p:Lgpx;

.field protected final q:Landroid/os/Handler;

.field private r:Lgpr;

.field private s:Lgpy;

.field private t:Lcom/google/android/gms/common/people/data/Audience;

.field private u:Lfrb;

.field private v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

.field private w:Ljava/lang/String;

.field private x:Lcom/google/android/gms/plus/internal/PlusCommonExtras;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lo;-><init>()V

    new-instance v0, Lgpj;

    invoke-direct {v0, p0}, Lgpj;-><init>(Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->q:Landroid/os/Handler;

    return-void
.end method

.method private static a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lgik;

    invoke-direct {v0}, Lgik;-><init>()V

    invoke-virtual {v0, p0}, Lgik;->a(Ljava/lang/String;)Lgik;

    move-result-object v0

    invoke-virtual {v0}, Lgik;->a()Lgij;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v4, p1, v1

    invoke-static {v4}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(Ljava/lang/String;)Lgim;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    array-length v0, p1

    goto :goto_0

    :cond_2
    new-instance v0, Lgit;

    invoke-direct {v0}, Lgit;-><init>()V

    iput-object v2, v0, Lgit;->a:Ljava/util/List;

    iget-object v1, v0, Lgit;->b:Ljava/util/Set;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0, v3}, Lgit;->a(Ljava/util/List;)Lgit;

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lgit;->a()Lgis;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgih;

    invoke-direct {v0}, Lgih;-><init>()V

    new-instance v2, Lgiq;

    invoke-direct {v2}, Lgiq;-><init>()V

    invoke-virtual {v2, v1}, Lgiq;->a(Ljava/util/List;)Lgiq;

    move-result-object v1

    invoke-virtual {v1}, Lgiq;->a()Lgip;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgih;->a(Lgip;)Lgih;

    move-result-object v0

    invoke-virtual {v0}, Lgih;->a()Lgig;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    return-object v0
.end method

.method private static a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 3

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lgit;

    invoke-direct {v0}, Lgit;-><init>()V

    invoke-virtual {v0, p0}, Lgit;->a(Ljava/util/List;)Lgit;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lgit;->a()Lgis;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgih;

    invoke-direct {v0}, Lgih;-><init>()V

    new-instance v2, Lgiq;

    invoke-direct {v2}, Lgiq;-><init>()V

    invoke-virtual {v2, v1}, Lgiq;->a(Ljava/util/List;)Lgiq;

    move-result-object v1

    invoke-virtual {v1}, Lgiq;->a()Lgip;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgih;->a(Lgip;)Lgih;

    move-result-object v0

    invoke-virtual {v0}, Lgih;->a()Lgig;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;)Lgoj;
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "confirm_action_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f0b03d8    # com.google.android.gms.R.string.plus_sharebox_confirm_cancel_dialog_message

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgoj;->a(Ljava/lang/String;)Lgoj;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lgoj;

    goto :goto_0
.end method

.method private a(II)Ljava/util/List;
    .locals 7

    const/4 v6, 0x1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "selectionSource"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    packed-switch p1, :pswitch_data_0

    const-string v5, "contactType"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ne p2, v6, :cond_0

    if-ne v4, v6, :cond_0

    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_0
    if-ne v5, v6, :cond_0

    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_1

    :pswitch_1
    const/4 v4, 0x2

    if-ne v5, v4, :cond_0

    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_1

    :pswitch_2
    if-nez v5, :cond_0

    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_1

    :pswitch_3
    const/4 v4, 0x3

    if-ne v5, v4, :cond_0

    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_1

    :cond_1
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private a(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    return-void
.end method

.method private static a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(Ljava/lang/String;)Lgim;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private static b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;
    .locals 2

    new-instance v0, Lggh;

    invoke-direct {v0}, Lggh;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lggh;->a(I)Lggh;

    new-instance v1, Lggl;

    invoke-direct {v1}, Lggl;-><init>()V

    invoke-virtual {v0}, Lggh;->a()Lggg;

    move-result-object v0

    invoke-virtual {v1, v0}, Lggl;->a(Lggg;)Lggl;

    move-result-object v0

    invoke-virtual {v0}, Lggl;->a()Lggk;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private b(Z)V
    .locals 2

    const-string v0, "pref_com.google.android.gms.plus.sharebox"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_dont_ask_again"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private c(I)I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v0}, Lgpi;->N()I

    move-result v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v0}, Lgpi;->L()I

    move-result v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v0}, Lgpi;->M()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static c(Ljava/lang/String;)Lgim;
    .locals 4

    invoke-static {p0}, Lfdl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lfdl;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgin;

    invoke-direct {v2}, Lgin;-><init>()V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v0}, Lgin;->b(Ljava/lang/String;)Lgin;

    :goto_0
    invoke-virtual {v2}, Lgin;->a()Lgim;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2, v1}, Lgin;->a(Ljava/lang/String;)Lgin;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private t()V
    .locals 3

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "post_error_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lbof;

    if-nez v0, :cond_0

    const v0, 0x7f0b03cd    # com.google.android.gms.R.string.plus_sharebox_post_error_message

    invoke-static {v0}, Lbof;->c(I)Lbof;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "post_error_dialog"

    invoke-virtual {v0, v1, v2}, Lbof;->a(Lu;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private u()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final a(Landroid/content/Intent;Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-direct {p0, p2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Z)V

    :cond_0
    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbda;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0, p1}, Lgpn;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public final a(Lbbo;)V
    .locals 3

    const v2, 0x7f0b03ca    # com.google.android.gms.R.string.plus_sharebox_internal_error

    if-nez p1, :cond_1

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lbbo;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, p0, v0}, Lbbo;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "ShareBox"

    const-string v1, "Failed to start connection resolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    invoke-static {v0, p0}, Lbbv;->b(ILandroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    const-string v0, "ShareBox"

    const-string v1, "Failed to get GooglePlayServices dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 14

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgqe;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgqe;->a()V

    :cond_0
    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    const v0, 0x7f0b03d0    # com.google.android.gms.R.string.plus_sharebox_post_success

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(I)V

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(I)I

    move-result v2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v4, Lbcz;->H:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v5, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v6

    new-instance v0, Lggh;

    invoke-direct {v0}, Lggh;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v0, v7}, Lggh;->a(I)Lggh;

    if-lez v2, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    int-to-float v7, v7

    int-to-float v8, v2

    div-float/2addr v7, v8

    iput v7, v0, Lggh;->d:F

    iget-object v7, v0, Lggh;->i:Ljava/util/Set;

    const/16 v8, 0x9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v7, Lggl;

    invoke-direct {v7}, Lggl;-><init>()V

    invoke-virtual {v0}, Lggh;->a()Lggg;

    move-result-object v0

    invoke-virtual {v7, v0}, Lggl;->a(Lggg;)Lggl;

    move-result-object v0

    invoke-virtual {v0}, Lggl;->a()Lggk;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v3, v4, v5, v6, v0}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_2
    const/4 v0, 0x2

    const/4 v3, -0x1

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(I)I

    move-result v4

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v6, Lbcz;->I:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v7, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v8

    new-instance v0, Lggh;

    invoke-direct {v0}, Lggh;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v0, v9}, Lggh;->a(I)Lggh;

    if-lez v4, :cond_3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    int-to-float v9, v9

    int-to-float v10, v4

    div-float/2addr v9, v10

    iput v9, v0, Lggh;->f:F

    iget-object v9, v0, Lggh;->i:Ljava/util/Set;

    const/16 v10, 0xb

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v9, Lggl;

    invoke-direct {v9}, Lggl;-><init>()V

    invoke-virtual {v0}, Lggh;->a()Lggg;

    move-result-object v0

    invoke-virtual {v9, v0}, Lggl;->a(Lggg;)Lggl;

    move-result-object v0

    invoke-virtual {v0}, Lggl;->a()Lggk;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v5, v6, v7, v8, v0}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_4
    const/4 v0, 0x0

    const/4 v5, -0x1

    invoke-direct {p0, v0, v5}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v5

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(I)I

    move-result v6

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v8, Lbcz;->J:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v9, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v5}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v10

    new-instance v0, Lggh;

    invoke-direct {v0}, Lggh;-><init>()V

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v0, v11}, Lggh;->a(I)Lggh;

    if-lez v6, :cond_5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v11

    int-to-float v11, v11

    int-to-float v12, v6

    div-float/2addr v11, v12

    iput v11, v0, Lggh;->b:F

    iget-object v11, v0, Lggh;->i:Ljava/util/Set;

    const/4 v12, 0x7

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    new-instance v11, Lggl;

    invoke-direct {v11}, Lggl;-><init>()V

    invoke-virtual {v0}, Lggh;->a()Lggg;

    move-result-object v0

    invoke-virtual {v11, v0}, Lggl;->a(Lggg;)Lggl;

    move-result-object v0

    invoke-virtual {v0}, Lggl;->a()Lggk;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v7, v8, v9, v10, v0}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_6
    const/4 v0, 0x3

    const/4 v7, -0x1

    invoke-direct {p0, v0, v7}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v8, Lbcz;->K:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v9, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v10

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    move-result-object v0

    invoke-virtual {v7, v8, v9, v10, v0}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_7
    const/4 v0, -0x1

    const/4 v7, 0x1

    invoke-direct {p0, v0, v7}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v8, Lbcz;->L:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v9, Lbda;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v10

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    move-result-object v0

    invoke-virtual {v7, v8, v9, v10, v0}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_8
    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v8, Lbcz;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lbcl;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v10

    if-nez p2, :cond_9

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int v11, v1, v3

    add-int/2addr v11, v5

    new-instance v12, Lggl;

    invoke-direct {v12}, Lggl;-><init>()V

    iput-object v0, v12, Lggl;->a:Ljava/lang/String;

    iget-object v0, v12, Lggl;->c:Ljava/util/Set;

    const/4 v13, 0x5

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v0, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v6}, Lggl;->a(I)Lggl;

    move-result-object v0

    new-instance v6, Lggh;

    invoke-direct {v6}, Lggh;-><init>()V

    iput v5, v6, Lggh;->a:I

    iget-object v5, v6, Lggh;->i:Ljava/util/Set;

    const/4 v12, 0x6

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v5, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput v1, v6, Lggh;->c:I

    iget-object v1, v6, Lggh;->i:Ljava/util/Set;

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput v3, v6, Lggh;->e:I

    iget-object v1, v6, Lggh;->i:Ljava/util/Set;

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput v2, v6, Lggh;->g:I

    iget-object v1, v6, Lggh;->i:Ljava/util/Set;

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput v4, v6, Lggh;->h:I

    iget-object v1, v6, Lggh;->i:Ljava/util/Set;

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, v11}, Lggh;->a(I)Lggh;

    move-result-object v1

    invoke-virtual {v1}, Lggh;->a()Lggg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lggl;->a(Lggg;)Lggl;

    move-result-object v0

    invoke-virtual {v0}, Lggl;->a()Lggk;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v7, v8, v9, v10, v0}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    const-string v0, "pref_com.google.android.gms.plus.sharebox"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "pref_dont_ask_again"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "com.google.android.apps.plus"

    invoke-static {p0, v0}, Lbox;->e(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_b

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    :goto_2
    return-void

    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Post;->j()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto :goto_1

    :cond_b
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "install_app_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_c

    const-string v0, "com.google.android.apps.plus"

    invoke-static {p0, v0}, Lgop;->c(Landroid/content/Context;Ljava/lang/String;)Lgop;

    move-result-object v0

    :goto_3
    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "install_app_dialog"

    invoke-virtual {v0, v1, v2}, Lgop;->a(Lu;Ljava/lang/String;)V

    goto :goto_2

    :cond_c
    check-cast v0, Lgop;

    goto :goto_3

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t()V

    goto :goto_2
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/model/posts/Settings;)V
    .locals 3

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_3

    :cond_0
    const-string v0, "ShareBox"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ShareBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load settings: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const v0, 0x7f0b03ca    # com.google.android.gms.R.string.plus_sharebox_internal_error

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const-string v0, "ShareBox"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ShareBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loaded settings: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const v0, 0x7f0a01b0    # com.google.android.gms.R.id.loading

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0, p2}, Lgpn;->a(Lcom/google/android/gms/plus/model/posts/Settings;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v0}, Lgpi;->b()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    iget v0, v0, Lgpx;->i:I

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p2}, Lcom/google/android/gms/plus/model/posts/Settings;->f()Z

    move-result v0

    :goto_1
    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v2

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    :goto_2
    invoke-virtual {v2, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v2}, Lag;->d()I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->G:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V
    .locals 2

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lgpn;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ShareBox"

    const-string v1, "Failed to load add-to-circle data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->L()V

    goto :goto_0
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/sharebox/Circle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0, p2}, Lgpn;->a(Lcom/google/android/gms/plus/sharebox/Circle;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v2, Lbdf;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/sharebox/Circle;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lgik;

    invoke-direct {v4}, Lgik;-><init>()V

    invoke-virtual {v4, v0}, Lgik;->a(Ljava/lang/String;)Lgik;

    move-result-object v0

    invoke-virtual {v0}, Lgik;->a()Lgij;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgih;

    invoke-direct {v0}, Lgih;-><init>()V

    new-instance v4, Lgie;

    invoke-direct {v4}, Lgie;-><init>()V

    invoke-virtual {v4, v3}, Lgie;->a(Ljava/util/List;)Lgie;

    move-result-object v3

    invoke-virtual {v3}, Lgie;->a()Lgid;

    move-result-object v3

    invoke-virtual {v0, v3}, Lgih;->a(Lgid;)Lgih;

    move-result-object v0

    invoke-virtual {v0}, Lgih;->a()Lgig;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    invoke-virtual {v1, v2, v5, v0, v5}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->M()V

    const v0, 0x7f0b03ba    # com.google.android.gms.R.string.plus_sharebox_circles_create_failed

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(Lbbo;Lfst;)V
    .locals 3

    const-string v0, "ShareBox"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ShareBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loaded preview: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-virtual {v0, p2}, Lgpn;->a(Lfst;)V

    return-void

    :cond_1
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public final a(Lbbo;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v4, Lbdf;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lgik;

    invoke-direct {v0}, Lgik;-><init>()V

    invoke-virtual {v0, p2}, Lgik;->a(Ljava/lang/String;)Lgik;

    move-result-object v0

    invoke-virtual {v0}, Lgik;->a()Lgij;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-nez p3, :cond_1

    move v0, v1

    :goto_0
    move v2, v1

    :goto_1
    if-ge v2, v0, :cond_2

    aget-object v7, p3, v2

    invoke-static {v7}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(Ljava/lang/String;)Lgim;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    array-length v0, p3

    goto :goto_0

    :cond_2
    new-instance v0, Lgih;

    invoke-direct {v0}, Lgih;-><init>()V

    invoke-virtual {v0, v5}, Lgih;->a(Ljava/util/List;)Lgih;

    move-result-object v0

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    iput-object v6, v0, Lgih;->a:Ljava/util/List;

    iget-object v2, v0, Lgih;->c:Ljava/util/Set;

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v0}, Lgih;->a()Lgig;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    new-instance v2, Lggl;

    invoke-direct {v2}, Lggl;-><init>()V

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(I)I

    move-result v5

    invoke-virtual {v2, v5}, Lggl;->a(I)Lggl;

    move-result-object v2

    new-instance v5, Lggh;

    invoke-direct {v5}, Lggh;-><init>()V

    if-nez p3, :cond_4

    :goto_2
    invoke-virtual {v5, v1}, Lggh;->a(I)Lggh;

    move-result-object v1

    invoke-virtual {v1}, Lggh;->a()Lggg;

    move-result-object v1

    invoke-virtual {v2, v1}, Lggl;->a(Lggg;)Lggl;

    move-result-object v1

    invoke-virtual {v1}, Lggl;->a()Lggk;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v3, v4, v8, v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcx;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p2, p3}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v2

    invoke-virtual {v0, v1, v8, v2, v8}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :goto_3
    return-void

    :cond_4
    array-length v1, p3

    goto :goto_2

    :cond_5
    const v0, 0x7f0b03bd    # com.google.android.gms.R.string.plus_sharebox_circles_add_to_circle_failed

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(I)V

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u:Lfrb;

    invoke-virtual {v0, p1, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u:Lfrb;

    iget-object v0, v0, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1}, Lgpn;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Lcom/google/android/gms/common/people/data/Audience;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    invoke-virtual {v0, p1}, Lgpr;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->s:Lgpy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->s:Lgpy;

    invoke-virtual {v0, p1, p2}, Lgpy;->a([Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final a_(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->q:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbda;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbdd;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    return-void
.end method

.method public final b(Lbbo;)V
    .locals 1

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->L()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p()V

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbbo;->a:Lbbo;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Lbbo;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    const v5, 0x7f0a02b5    # com.google.android.gms.R.id.post_container

    const/4 v4, 0x0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    new-instance v2, Lgpn;

    invoke-direct {v2}, Lgpn;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    const-string v3, "share_fragment"

    invoke-virtual {v1, v5, v2, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v1, v2}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    new-instance v2, Lgpi;

    invoke-direct {v2}, Lgpi;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    const-string v3, "acl_fragment"

    invoke-virtual {v1, v5, v2, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v1, v2}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    invoke-virtual {v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    invoke-static {p1}, Lgpr;->a(Ljava/lang/String;)Lgpr;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    const-string v3, "share_worker_fragment"

    invoke-virtual {v1, v2, v3}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->w:Ljava/lang/String;

    invoke-static {p0, p1, v2}, Lgpq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {v1}, Lag;->c()I

    :goto_0
    invoke-virtual {v0}, Lu;->d()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0}, Lu;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lu;->c(I)Lv;

    move-result-object v1

    invoke-interface {v1}, Lv;->e()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.sharebox.show_compose"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lu;->c()Z

    goto :goto_0

    :cond_1
    const v0, 0x7f0a01b0    # com.google.android.gms.R.id.loading

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->s:Lgpy;

    invoke-virtual {v0, v4}, Lgpy;->c(I)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->M()V

    return-void
.end method

.method public final e()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    return-object v0
.end method

.method public final g()Lgpr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    return-object v0
.end method

.method public getCallingPackage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lgpy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->s:Lgpy;

    return-object v0
.end method

.method public final i()Lcom/google/android/gms/plus/internal/PlusCommonExtras;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->x:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    return-object v0
.end method

.method public final j()Lgpx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    return-object v0
.end method

.method public final k()Lfrb;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u:Lfrb;

    return-object v0
.end method

.method public final l()V
    .locals 3

    iget-object v1, p0, Lo;->b:Lw;

    const-string v0, "create_circle_fragment"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgol;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lgol;->a(Ljava/util/ArrayList;)Lgol;

    move-result-object v0

    :cond_0
    const-string v2, "create_circle_fragment"

    invoke-virtual {v0, v1, v2}, Lgol;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public final m()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "add_to_circle_data"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v2}, Lgpn;->P()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "calling_package_name"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_application_id"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    iget-object v2, v2, Lgpx;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final n()V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    const-string v1, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-virtual {v0, v1}, Lag;->a(Ljava/lang/String;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    return-void
.end method

.method public final o()V
    .locals 3

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "underage_warning_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgnw;

    if-nez v0, :cond_0

    const v0, 0x7f0b03ce    # com.google.android.gms.R.string.plus_sharebox_under_age_warning_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b03cf    # com.google.android.gms.R.string.plus_sharebox_under_age_warning_message

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgnw;->a(Ljava/lang/String;Ljava/lang/String;)Lgnw;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "underage_warning_dialog"

    invoke-virtual {v0, v1, v2}, Lgnw;->a(Lu;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbda;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lo;->onActivityResult(IILandroid/content/Intent;)V

    :goto_1
    return-void

    :pswitch_0
    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    invoke-virtual {v0}, Lgpr;->U()V

    goto :goto_1

    :cond_1
    const-string v0, "ShareBox"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ShareBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to resolve connection/account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    goto :goto_1

    :pswitch_1
    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->s:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbda;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbdd;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->t:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbda;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lgpr;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_2

    :pswitch_2
    if-eq p2, v0, :cond_4

    if-eq p2, v2, :cond_4

    if-ne p2, v2, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbbo;->a:Lbbo;

    const/4 v2, 0x0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lgpr;->a(Lbbo;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->q()V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->N()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->R()V

    goto :goto_1

    :pswitch_3
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "add_to_circle_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0, v1, v2}, Lgpn;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v0}, Lgpi;->p()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v0}, Lgpi;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->d()I

    move-result v2

    if-lez v2, :cond_1

    add-int/lit8 v0, v2, -0x1

    invoke-virtual {v1, v0}, Lu;->c(I)Lv;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "com.google.android.gms.plus.sharebox.show_compose"

    invoke-interface {v0}, Lv;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-interface {v0}, Lv;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->q:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    const-string v1, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-interface {v0}, Lv;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u:Lfrb;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    :cond_4
    invoke-super {p0}, Lo;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    const v8, 0x7f0a02b5    # com.google.android.gms.R.id.post_container

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lgpq;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->w:Ljava/lang/String;

    invoke-static {p0}, Lbpu;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0b03de    # com.google.android.gms.R.string.plus_sharebox_no_network_connection

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->x:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->x:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {p0, v0}, Lfmr;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    new-instance v0, Lgpx;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lgpx;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lgpq;->c(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lgpq;->a(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ShareBox"

    const-string v1, "Invalid share action"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lgpx;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lgpq;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lgpq;->c(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "ShareBox"

    const-string v1, "Invalid deep link"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lgpq;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p0}, Lgpq;->b(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ShareBox"

    const-string v1, "Invalid interactive post"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    goto :goto_0

    :cond_5
    const v0, 0x7f040109    # com.google.android.gms.R.layout.plus_sharebox_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setContentView(I)V

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0d017a    # com.google.android.gms.R.dimen.plus_sharebox_container_margin

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110013    # com.google.android.gms.R.bool.plus_is_tablet

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-static {v0, v1, v3}, Lgpq;->a(Landroid/view/View;IZ)V

    if-eqz p1, :cond_c

    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    const-string v0, "addToCircleData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    const-string v0, "share_box_hidden"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v0, "acl_hidden"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    move v3, v1

    move v1, v0

    :goto_1
    new-instance v0, Lfrb;

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-direct {v0, v4}, Lfrb;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u:Lfrb;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u:Lfrb;

    invoke-virtual {v0, p0}, Lfrb;->a(Lfrc;)V

    iget-object v4, p0, Lo;->b:Lw;

    invoke-virtual {v4}, Lu;->a()Lag;

    move-result-object v5

    const-string v0, "share_worker_fragment"

    invoke-virtual {v4, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgpr;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    iget-object v0, v0, Lgpx;->a:Ljava/lang/String;

    invoke-static {v0}, Lgpr;->a(Ljava/lang/String;)Lgpr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    const-string v6, "share_worker_fragment"

    invoke-virtual {v5, v0, v6}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_6
    const-string v0, "title_fragment"

    invoke-virtual {v4, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgpy;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->s:Lgpy;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->s:Lgpy;

    if-nez v0, :cond_7

    new-instance v0, Lgpy;

    invoke-direct {v0}, Lgpy;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->s:Lgpy;

    const v0, 0x7f0a02c2    # com.google.android.gms.R.id.title_container

    iget-object v6, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->s:Lgpy;

    const-string v7, "title_fragment"

    invoke-virtual {v5, v0, v6, v7}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_7
    const-string v0, "share_fragment"

    invoke-virtual {v4, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgpn;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    if-nez v0, :cond_8

    new-instance v0, Lgpn;

    invoke-direct {v0}, Lgpn;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    const-string v6, "share_fragment"

    invoke-virtual {v5, v8, v0, v6}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_8
    const-string v0, "acl_fragment"

    invoke-virtual {v4, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgpi;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    if-nez v0, :cond_9

    new-instance v0, Lgpi;

    invoke-direct {v0}, Lgpi;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    const-string v4, "acl_fragment"

    invoke-virtual {v5, v8, v0, v4}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_9
    if-eqz v3, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v5, v0}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    :goto_2
    if-eqz v1, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v5, v0}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    :goto_3
    invoke-virtual {v5}, Lag;->c()I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    invoke-virtual {v0}, Lgpx;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    iget-object v0, v0, Lgpx;->o:Lgqb;

    invoke-virtual {v0}, Lgqb;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    invoke-virtual {v0}, Lgpx;->g()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    iget-object v1, v0, Lgpx;->q:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v1, :cond_f

    iget-object v0, v0, Lgpx;->q:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lblc;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-nez v0, :cond_f

    :goto_4
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    sget-object v1, Lbcz;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_0

    :cond_c
    sget-object v0, Lbkw;->a:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    new-instance v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-static {p0}, Lgof;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lgof;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move v1, v2

    move v3, v2

    goto/16 :goto_1

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v5, v0}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    goto :goto_2

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v5, v0}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    goto :goto_3

    :cond_f
    const/4 v2, 0x0

    goto :goto_4
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lo;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "confirm_action_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgoj;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lgoj;->a(Lgok;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    invoke-virtual {v0}, Lgpr;->M()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a01b0    # com.google.android.gms.R.id.loading

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "audience"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "addToCircleData"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->v:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "share_box_hidden"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v1}, Lgpn;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "acl_hidden"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v1}, Lgpi;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const v0, 0x7f0a02b5    # com.google.android.gms.R.id.post_container

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lbow;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->onBackPressed()V

    invoke-static {p0, v0}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lo;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final p()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    invoke-virtual {v0}, Lgpr;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->R()V

    :cond_1
    return-void
.end method

.method public final q()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->Q()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    invoke-virtual {v0}, Lgpr;->O()Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    invoke-virtual {v0}, Lgpr;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p:Lgpx;

    invoke-virtual {v1}, Lgpx;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->d()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x50

    invoke-static/range {v0 .. v5}, Lfsm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->O()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lblc;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->J()Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v0

    if-eqz v0, :cond_3

    const v1, 0x7f0b03d1    # com.google.android.gms.R.string.plus_sharebox_post_pending

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgqe;->a(Ljava/lang/CharSequence;)Lgqe;

    move-result-object v1

    iget-object v2, p0, Lo;->b:Lw;

    invoke-virtual {v2}, Lu;->a()Lag;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v2, v1, v3}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v2}, Lag;->d()I

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->r:Lgpr;

    invoke-virtual {v1, v0}, Lgpr;->a(Lcom/google/android/gms/plus/model/posts/Post;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t()V

    goto :goto_0
.end method

.method public final r()V
    .locals 4

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v1, v2}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v1, v2}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v0}, Lu;->d()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-virtual {v0}, Lu;->d()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Lu;->c(I)Lv;

    move-result-object v3

    invoke-interface {v3}, Lv;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-virtual {v0, v2}, Lu;->b(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1}, Lag;->c()I

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u:Lfrb;

    iget-object v0, v0, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n:Lgpn;

    invoke-virtual {v0}, Lgpn;->a()V

    return-void

    :cond_0
    const-string v0, "com.google.android.gms.plus.sharebox.show_compose"

    invoke-virtual {v1, v0}, Lag;->a(Ljava/lang/String;)Lag;

    goto :goto_0
.end method

.method public final s()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->o:Lgpi;

    invoke-virtual {v0}, Lgpi;->K()V

    return-void
.end method
