.class public Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;
.super Landroid/app/ListActivity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lfaj;

.field private b:Lbfs;

.field private final c:Lbkt;

.field private d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    new-instance v0, Lbkt;

    const/high16 v1, 0x200000

    invoke-direct {v0, v1}, Lbkt;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lbkt;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/plus/sharebox/AddToCircleData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "ShareBox"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->finish()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lbkt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lbkt;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lbfs;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->b:Lbfs;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a024e    # com.google.android.gms.R.id.done_button

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "add_to_circle_data"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lgob;

    if-eqz v1, :cond_0

    check-cast v0, Lgob;

    iget-object v0, v0, Lgob;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "add_to_circle_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    if-nez v0, :cond_1

    const-string v0, "Add to circle data not specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "add_to_circle_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, "No un-circled audience members specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    new-instance v0, Lfaj;

    const-class v1, Lbbr;

    invoke-static {v1}, Lbpw;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbbr;

    const-class v1, Lbbs;

    invoke-static {v1}, Lbpw;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbbs;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "client_application_id"

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v5, "calling_package_name"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    new-instance v0, Lbfs;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    invoke-direct {v0, v1}, Lbfs;-><init>(Lfaj;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->b:Lbfs;

    const v0, 0x7f0b03c0    # com.google.android.gms.R.string.plus_sharebox_circles_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setTitle(I)V

    const v0, 0x7f0400bb    # com.google.android.gms.R.layout.plus_add_to_circle_list_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setClickable(Z)V

    const v0, 0x7f0a024e    # com.google.android.gms.R.id.done_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lgny;

    invoke-direct {v0, p0, v6}, Lgny;-><init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;B)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lbkt;

    invoke-virtual {v0}, Lbkt;->a()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "add_to_circle_data"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->d:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    :cond_1
    return-void
.end method
