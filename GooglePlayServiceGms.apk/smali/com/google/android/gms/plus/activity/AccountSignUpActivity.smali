.class public Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;


# instance fields
.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:I

.field private r:Ljava/lang/String;

.field private s:[Ljava/lang/String;

.field private t:Landroid/app/PendingIntent;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Lfni;

.field private x:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private final y:[Lfng;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lo;-><init>()V

    const/4 v0, 0x7

    new-array v0, v0, [Lfng;

    new-instance v1, Lfnc;

    invoke-direct {v1, p0, v3}, Lfnc;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v2, Lfnb;

    invoke-direct {v2, p0, v3}, Lfnb;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lfna;

    invoke-direct {v2, p0, v3}, Lfna;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lfmy;

    invoke-direct {v2, p0, v3}, Lfmy;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lfmz;

    invoke-direct {v2, p0, v3}, Lfmz;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lfnf;

    invoke-direct {v2, p0, v3}, Lfnf;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lfnd;

    invoke-direct {v2, p0, v3}, Lfnd;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->q:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->t:Landroid/app/PendingIntent;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lfni;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->w:Lfni;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    return-object p1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    sget-object v0, Lbdc;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lbdd;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, p1, v0, v1, p2}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    sget-object v0, Lbdd;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lbdd;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, p1, v0, v1, p2}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    sget-object v0, Lbdd;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, p1, p3, v0, p2}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    sget-object v0, Lbdd;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lbdd;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, p1, v0, v1, p2}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 1

    const-string v0, "authAccount"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {p0, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.plus.intent.extra.SIGN_UP_STATE"

    invoke-virtual {p0, v0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz p6, :cond_0

    const-string v0, "request_visible_actions"

    invoke-virtual {p0, v0, p6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    const-string v0, "com.google.android.gms.plus.intent.extra.AUTH_SCOPE_STRING"

    invoke-virtual {p0, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.plus.intent.extra.AUTH_TOKEN_INTENT"

    invoke-virtual {p0, v0, p7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->q:I

    return v0
.end method

.method public static synthetic f(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "AccountSignUpActivity"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AccountSignUpActivity"

    const-string v1, "Resolution intents must be called with startIntentSenderForResult"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    sget-object v2, Lbdc;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->x:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    return-object v0
.end method

.method private g()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    aget-object v0, v1, v0

    iget-boolean v1, v0, Lfng;->c:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lfng;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    sget-object v2, Lbdc;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    goto :goto_0
.end method

.method private h()I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lfng;->c()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    array-length v0, v0

    goto :goto_1
.end method

.method public static synthetic h(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->r:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Landroid/app/PendingIntent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->t:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->v:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->r:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->p:Z

    return v0
.end method

.method public static synthetic m(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->p:Z

    return v0
.end method


# virtual methods
.method public final P_()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lfng;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lfng;->c:Z

    :cond_0
    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f()V

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    if-ne p2, v6, :cond_0

    const-string v0, "authAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    sget-object v2, Lbdc;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbdd;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lbdd;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iput-boolean v7, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->p:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    if-ne p2, v6, :cond_1

    sget-object v2, Lbdc;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    sget-object v3, Lbcr;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lbdd;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    if-ne p2, v6, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->q:I

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g()V

    goto :goto_0

    :cond_1
    sget-object v2, Lbdc;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    if-ne p2, v6, :cond_4

    sget-object v2, Lbdc;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_2
    sget-object v3, Lbdd;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lbdd;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_3
    if-ne p2, v6, :cond_5

    iput-object v8, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->t:Landroid/app/PendingIntent;

    iput-object v8, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->r:Ljava/lang/String;

    iput-boolean v7, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->p:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g()V

    goto :goto_0

    :cond_4
    sget-object v2, Lbdc;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->requestWindowFeature(I)Z

    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AccountSignUpActivity"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz p1, :cond_3

    const-string v0, "stateIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lfng;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    aget-object v0, v1, v0

    iput-boolean v2, v0, Lfng;->c:Z

    :cond_1
    const-string v0, "shouldSetDefaultAccount"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->p:Z

    move-object v0, p1

    :cond_2
    const-string v1, "com.google.android.gms.common.oob.OOB_SIGN_UP"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    invoke-static {v1, v2}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "com.google.android.gms.common.oob.OOB_SIGN_UP is only available to first-party clients"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez v0, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_4
    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    if-nez v1, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f()V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_7
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Calling/Auth package may only be set by GmsCore"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.common.oob.EXTRA_BACK_BUTTON_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->o:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->v:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->x:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->x:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {p0, v1}, Lfmr;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->v:Ljava/lang/String;

    if-nez v1, :cond_d

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f()V

    :goto_2
    if-nez p1, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    new-instance v0, Lfni;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->s:[Ljava/lang/String;

    invoke-direct {v0, p0, p0, p0, v1}, Lfni;-><init>(Landroid/content/Context;Lbbr;Lbbs;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->w:Lfni;

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g()V

    goto/16 :goto_0

    :cond_a
    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    :cond_b
    const-string v1, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->v:Ljava/lang/String;

    :goto_3
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    goto :goto_1

    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->v:Ljava/lang/String;

    goto :goto_3

    :cond_d
    const-string v1, "com.google.android.gms.plus.intent.extra.SIGN_UP_STATE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "com.google.android.gms.plus.intent.extra.SIGN_UP_STATE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->q:I

    :goto_4
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->s:[Ljava/lang/String;

    const-string v1, "request_visible_actions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "request_visible_actions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->s:[Ljava/lang/String;

    :cond_e
    const-string v1, "com.google.android.gms.plus.intent.extra.AUTH_SCOPE_STRING"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->r:Ljava/lang/String;

    const-string v1, "com.google.android.gms.plus.intent.extra.AUTH_TOKEN_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->t:Landroid/app/PendingIntent;

    goto :goto_2

    :cond_f
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->q:I

    goto :goto_4
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lo;->onPause()V

    iget-object v1, p0, Lo;->b:Lw;

    const-string v0, "progress_dialog"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgqe;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lag;->d(Landroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v1}, Lag;->c()I

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Lo;->onResume()V

    const v0, 0x7f0b0382    # com.google.android.gms.R.string.plus_loading

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lo;->b:Lw;

    const-string v0, "progress_dialog"

    invoke-virtual {v2, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgqe;

    if-nez v0, :cond_0

    invoke-static {v1}, Lgqe;->a(Ljava/lang/CharSequence;)Lgqe;

    move-result-object v0

    const/4 v1, 0x1

    const v3, 0x7f100188    # com.google.android.gms.R.style.common_Activity_Light_Dialog

    invoke-virtual {v0, v1, v3}, Lgqe;->a(II)V

    invoke-virtual {v2}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v1}, Lag;->c()I

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->u:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->v:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->q:I

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->r:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->s:[Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->t:Landroid/app/PendingIntent;

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h()I

    move-result v0

    const-string v1, "stateIndex"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->y:[Lfng;

    aget-object v0, v1, v0

    iget-boolean v0, v0, Lfng;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    const-string v0, "shouldSetDefaultAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lo;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->w:Lfni;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->w:Lfni;

    invoke-virtual {v0}, Lfni;->a()V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lo;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->w:Lfni;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->w:Lfni;

    invoke-virtual {v0}, Lfni;->b()V

    :cond_0
    return-void
.end method
