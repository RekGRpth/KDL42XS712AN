.class public final Lcom/google/android/gms/plus/apps/ListAppsActivity;
.super Ljp;
.source "SourceFile"


# instance fields
.field private o:Lfob;

.field private final p:Ljava/util/ArrayList;

.field private q:Lfpp;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljp;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    return-void
.end method

.method private a(Lfpp;)V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    invoke-virtual {v0, p1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    :cond_0
    invoke-virtual {v0}, Lag;->a()Lag;

    invoke-virtual {v0}, Lag;->c()I

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {p1, v0}, Lfpp;->b(Ljj;)V

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    return-void
.end method


# virtual methods
.method public final I_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Ljp;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v0, :cond_0

    const-string v0, "com.google.android.gms.plus.DISCONNECTED_APP_ID"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->o:Lfob;

    invoke-virtual {v1, v0}, Lfob;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    iget-object v1, p0, Ljp;->n:Ljq;

    invoke-virtual {v1}, Ljq;->b()Ljj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfpp;->b(Ljj;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->o:Lfob;

    invoke-virtual {v0, p2}, Lfob;->c(I)V

    goto :goto_0

    :pswitch_2
    if-ne p2, v0, :cond_0

    const-string v0, "deleted_moment_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lo;->b:Lw;

    const-string v2, "activity_log"

    invoke-virtual {v0, v2}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lfpn;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lfpn;->b(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 16

    invoke-super/range {p0 .. p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-static/range {p0 .. p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "AppSettings"

    const-string v2, "This activity is not available for restricted profile."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setResult(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v1, "com.google.android.gms.plus.action.VIEW_ACTIVITY_LOG"

    invoke-virtual {v13}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    const-string v1, "com.google.android.gms.extras.ACCOUNT_NAME"

    invoke-virtual {v13, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    if-eqz v14, :cond_2

    const-string v2, "com.google.android.gms.extras.APP_ID"

    invoke-virtual {v13, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "application"

    invoke-virtual {v13, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-static/range {p0 .. p0}, Lfob;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    const-string v1, "AppSettings"

    const-string v2, "This activity is requires an account name."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setResult(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    goto :goto_0

    :cond_4
    move-object v10, v1

    const v1, 0x7f0400d6    # com.google.android.gms.R.layout.plus_list_apps_activity

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setContentView(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Ljp;->n:Ljq;

    invoke-virtual {v1}, Ljq;->b()Ljj;

    move-result-object v1

    const/16 v2, 0x14

    const/16 v3, 0x1c

    invoke-virtual {v1, v2, v3}, Ljj;->a(II)V

    invoke-static/range {p0 .. p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lo;->b:Lw;

    const-string v1, "apps_util"

    invoke-virtual {v2, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lfob;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->o:Lfob;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->o:Lfob;

    if-nez v1, :cond_5

    invoke-static {v10, v11}, Lfob;->a(Ljava/lang/String;Ljava/lang/String;)Lfob;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->o:Lfob;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->o:Lfob;

    invoke-virtual {v1}, Lfob;->q()V

    invoke-virtual {v2}, Lu;->a()Lag;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->o:Lfob;

    const-string v3, "apps_util"

    invoke-virtual {v1, v2, v3}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v1

    invoke-virtual {v1}, Lag;->c()I

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, v11}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lo;->b:Lw;

    invoke-virtual {v3}, Lu;->a()Lag;

    move-result-object v15

    invoke-virtual {v15}, Lag;->a()Lag;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v1, "com.google.android.gms.extras.APP_ID"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "application"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    const/4 v2, 0x0

    if-eqz v5, :cond_15

    const-string v2, "com.google.android.gms.extras.COLLECTION_FILTER"

    invoke-virtual {v6, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v4, :cond_9

    if-eqz v2, :cond_9

    const-string v1, "AppSettings"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "AppSettings"

    const-string v2, "EXTRA_APP_ID and EXTRA_COLLECTION_FILTER filters for Activity Log are mutually exclusive."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setResult(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    :goto_2
    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz p1, :cond_10

    const-string v1, "selected_page_tag"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfpp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfpp;

    invoke-virtual {v2}, Lfpp;->h()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    move-object v7, v2

    :goto_4
    if-eqz v4, :cond_8

    sget-object v3, Lbcj;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object/from16 v1, p0

    move-object v2, v10

    move-object v6, v11

    invoke-static/range {v1 .. v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a(Lfpp;)V

    goto/16 :goto_0

    :cond_9
    move-object v12, v2

    :goto_5
    if-eqz v14, :cond_a

    if-nez v4, :cond_c

    if-nez v1, :cond_c

    if-nez v12, :cond_c

    :cond_a
    const-string v2, "connected_apps"

    invoke-virtual {v3, v2}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lfpp;

    if-nez v2, :cond_b

    const-string v2, "com.google.android.gms.extras.ALL_APPS"

    const/4 v7, 0x0

    invoke-virtual {v6, v2, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Lfoe;->a(Z)Lfoe;

    move-result-object v2

    const v7, 0x7f0a01a7    # com.google.android.gms.R.id.fragment_container

    const-string v8, "connected_apps"

    invoke-virtual {v15, v7, v2, v8}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_b
    invoke-virtual {v15, v2}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    const-string v2, "activity_log"

    invoke-virtual {v3, v2}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lfpp;

    if-nez v2, :cond_f

    if-nez v1, :cond_e

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz v5, :cond_d

    const-string v2, "com.google.android.gms.extras.APP_NAME"

    invoke-virtual {v6, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.gms.extras.APP_ICON_URL"

    invoke-virtual {v6, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_d
    if-eqz v4, :cond_e

    new-instance v1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;B)V

    :cond_e
    invoke-static {v1, v12}, Lfpn;->a(Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/lang/String;)Lfpn;

    move-result-object v2

    const v1, 0x7f0a01a7    # com.google.android.gms.R.id.fragment_container

    const-string v3, "activity_log"

    invoke-virtual {v15, v1, v2, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_f
    invoke-virtual {v15, v2}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v15}, Lag;->c()I

    goto/16 :goto_2

    :cond_10
    if-eqz v14, :cond_11

    const-string v1, "activity_log"

    sget-object v5, Lbck;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    sget-object v4, Lbco;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v3, v1

    goto/16 :goto_3

    :cond_11
    const-string v1, "connected_apps"

    const-string v2, "com.google.android.gms.extras.ALL_APPS"

    const/4 v3, 0x0

    invoke-virtual {v13, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_12

    sget-object v4, Lbck;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v5, Lbck;->q:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v3, v1

    goto/16 :goto_3

    :cond_12
    sget-object v4, Lfnp;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v5, Lbck;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v3, v1

    goto/16 :goto_3

    :cond_13
    move-object v7, v1

    goto/16 :goto_4

    :cond_14
    move-object v3, v1

    goto/16 :goto_3

    :cond_15
    move-object v12, v2

    goto/16 :goto_5

    :cond_16
    move-object v11, v1

    goto/16 :goto_1
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    invoke-virtual {v0, p1}, Lfpp;->a(Landroid/view/Menu;)V

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpp;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    if-eq v0, v3, :cond_0

    const/4 v3, 0x2

    invoke-virtual {v0}, Lfpp;->J()I

    move-result v0

    invoke-interface {p1, v2, v1, v3, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Ljp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->p:Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpp;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    if-eq v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->o:Lfob;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    invoke-virtual {v3}, Lfpp;->K()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-virtual {v0}, Lfpp;->K()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lfob;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a(Lfpp;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->i_()V

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    invoke-virtual {v0, p1}, Lfpp;->a_(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-super {p0, p1}, Ljp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "selected_page_tag"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->q:Lfpp;

    invoke-virtual {v1}, Lfpp;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
