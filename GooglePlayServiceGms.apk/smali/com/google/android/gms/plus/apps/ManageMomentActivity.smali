.class public Lcom/google/android/gms/plus/apps/ManageMomentActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Lbbr;
.implements Lbbs;
.implements Lfoo;
.implements Lfoq;
.implements Lfpf;
.implements Lfpm;
.implements Lfps;
.implements Lfuf;


# instance fields
.field private A:Lftx;

.field private B:Lfpq;

.field private o:Landroid/accounts/Account;

.field private p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

.field private q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Z

.field private u:Z

.field private v:Lfop;

.field private w:Lfpk;

.field private x:Lfpd;

.field private y:Landroid/app/AlertDialog;

.field private final z:Lftz;


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;-><init>(Lftz;)V

    return-void
.end method

.method constructor <init>(Lftz;)V
    .locals 1

    invoke-direct {p0}, Ljp;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->t:Z

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->z:Lftz;

    return-void
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljj;->a(Ljava/lang/CharSequence;)V

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljj;->a(Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v1, :cond_0

    invoke-static {p0}, Lfnz;->a(Landroid/content/Context;)Lfnz;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1, v2}, Lfnz;->a(Lfxh;)Lfoa;

    move-result-object v1

    iget-object v2, v1, Lfoa;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Ljj;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, v1, Lfoa;->c:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lfpd;->a(Landroid/content/Context;)Lfpd;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->x:Lfpd;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->x:Lfpd;

    invoke-virtual {v1, p0}, Lfpd;->a(Lfpf;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->x:Lfpd;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1, v2, v0}, Lfpd;->a(Lfxh;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f0b03a0    # com.google.android.gms.R.string.plus_manage_moment_label

    invoke-virtual {v0, v1}, Ljj;->d(I)V

    goto :goto_0
.end method

.method private g()V
    .locals 5

    const v0, 0x7f0a0218    # com.google.android.gms.R.id.acl

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b03a4    # com.google.android.gms.R.string.plus_manage_moment_acl

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->r:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private h()V
    .locals 2

    const v0, 0x7f0b03a6    # com.google.android.gms.R.string.plus_manage_moment_acl_error

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public final I_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method public final P_()V
    .locals 0

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->h()V

    return-void
.end method

.method public final a(Lfpr;)V
    .locals 9

    const/4 v4, 0x0

    iget-object v0, p1, Lfpr;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v1, p1, Lfpr;->b:Ljava/lang/String;

    iget-object v2, p1, Lfpr;->c:Ljava/lang/String;

    iget-object v3, p1, Lfpr;->a:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v8, 0x0

    move-object v6, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;B)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f()V

    :cond_0
    return-void
.end method

.method public final a(Lfxh;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lfxh;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljj;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p0}, Lfnz;->a(Landroid/content/Context;)Lfnz;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lfnz;->a(Lfxh;Landroid/graphics/drawable/Drawable;)Lfoa;

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    const v0, 0x7f0a005a    # com.google.android.gms.R.id.image

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p0}, Lfpj;->a(Landroid/content/Context;)Lfpj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lfpj;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgqe;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgqe;->b()V

    :cond_0
    if-eqz p2, :cond_1

    const v0, 0x7f0b03a8    # com.google.android.gms.R.string.plus_delete_moment_failed_dialog_message

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgqd;->a(Ljava/lang/String;)Lgqd;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "error_dialog"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v1}, Lag;->d()I

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "deleted_moment_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 5

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b0396    # com.google.android.gms.R.string.plus_manage_app_only_you_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->r:Ljava/lang/String;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->g()V

    :goto_1
    return-void

    :cond_0
    const v0, 0x7f0b03a5    # com.google.android.gms.R.string.plus_manage_moment_acl_separator

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->r:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->h()V

    goto :goto_1
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lftx;->a(Lfuf;Ljava/lang/String;)V

    return-void
.end method

.method public final f_(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->v:Lfop;

    invoke-virtual {v0, p1}, Lfop;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b03a7    # com.google.android.gms.R.string.plus_delete_moment_progress_dialog_message

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgqe;->a(Ljava/lang/CharSequence;)Lgqe;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v1}, Lag;->d()I

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0b03a8    # com.google.android.gms.R.string.plus_delete_moment_failed_dialog_message

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgqd;->a(Ljava/lang/String;)Lgqd;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Lgqd;->a(Lu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final h_()V
    .locals 3

    invoke-super {p0}, Ljp;->h_()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    invoke-static {p0}, Lfpq;->a(Landroid/content/Context;)Lfpq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->B:Lfpq;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->B:Lfpq;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2}, Lfpq;->a(Lfps;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->u:Z

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->t:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->y:Landroid/app/AlertDialog;

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->t:Z

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->y:Landroid/app/AlertDialog;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lfon;->a(Landroid/accounts/Account;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;Ljava/lang/String;)Lfon;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "delete_moment_dialog"

    invoke-virtual {v0, v1, v2}, Lfon;->a(Lu;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->j()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->j()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0, p0, p0}, Lfoy;->a(Ljava/lang/String;Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lbcj;->x:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lbck;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v1, v0, v2, v3}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    sget-object v0, Lbcj;->y:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a005a -> :sswitch_1    # com.google.android.gms.R.id.image
        0x7f0a024a -> :sswitch_0    # com.google.android.gms.R.id.delete
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x0

    const v6, 0x7f0a005a    # com.google.android.gms.R.id.image

    const/16 v3, 0x8

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AppSettings"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    const-string v0, "moment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    const-string v0, "application"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    const-string v0, "moment_acl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->r:Ljava/lang/String;

    const-string v0, "manage_error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->t:Z

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    if-nez v0, :cond_5

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "app_activity"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "application"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    if-nez v0, :cond_8

    :cond_6
    const-string v0, "AppSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "AppSettings"

    const-string v1, "Missing required extra(s): account=%s moment=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    goto/16 :goto_0

    :cond_8
    const v0, 0x7f0400db    # com.google.android.gms.R.layout.plus_manage_moment_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f()V

    const v0, 0x7f0a028e    # com.google.android.gms.R.id.action

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f0a028f    # com.google.android.gms.R.id.target

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->i()Z

    move-result v0

    if-eqz v0, :cond_9

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfol;->a(Ljava/lang/String;)Lfol;

    move-result-object v0

    invoke-virtual {v0}, Lfol;->a()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lfom;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    const v0, 0x7f0a0290    # com.google.android.gms.R.id.time

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_9
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->o()Z

    move-result v0

    if-eqz v0, :cond_c

    const v0, 0x7f0a024a    # com.google.android.gms.R.id.delete

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b03a1    # com.google.android.gms.R.string.plus_manage_moment_delete_label

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_a
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->r:Ljava/lang/String;

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->z:Lftz;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p0, p0, p0, v1}, Lfob;->a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    :goto_3
    iget-object v1, p0, Lo;->b:Lw;

    const-string v0, "delete_moment_fragment"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lfop;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->v:Lfop;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->v:Lfop;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    invoke-static {v0}, Lfop;->a(Landroid/accounts/Account;)Lfop;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->v:Lfop;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->v:Lfop;

    const-string v3, "delete_moment_fragment"

    invoke-virtual {v0, v1, v3}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->j()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->j()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;->d()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_4
    invoke-static {p0}, Lfpj;->a(Landroid/content/Context;)Lfpj;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfpj;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    if-eqz v2, :cond_f

    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_c
    const v0, 0x7f0a028d    # com.google.android.gms.R.id.manage_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0291    # com.google.android.gms.R.id.manage_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->t:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400d2    # com.google.android.gms.R.layout.plus_delete_moment_dialog_contents

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a0071    # com.google.android.gms.R.id.message

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b03a2    # com.google.android.gms.R.string.plus_manage_moment_no_in_app_delete

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    sget-object v1, Lfsr;->D:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, v1}, Lfoy;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b03ea    # com.google.android.gms.R.string.plus_done

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->y:Landroid/app/AlertDialog;

    goto/16 :goto_2

    :cond_d
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->g()V

    goto/16 :goto_3

    :cond_e
    move-object v1, v2

    goto/16 :goto_4

    :cond_f
    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f020207    # com.google.android.gms.R.drawable.plus_icon_red_32

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    if-eqz v1, :cond_0

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->s:Ljava/lang/String;

    invoke-static {p0}, Lfpk;->a(Landroid/content/Context;)Lfpk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->w:Lfpk;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->w:Lfpk;

    invoke-virtual {v0, p0}, Lfpk;->a(Lfpm;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->w:Lfpk;

    invoke-virtual {v0, v1}, Lfpk;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Ljp;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->x:Lfpd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->x:Lfpd;

    invoke-virtual {v0, p0}, Lfpd;->b(Lfpf;)V

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->x:Lfpd;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->w:Lfpk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->w:Lfpk;

    invoke-virtual {v0, p0}, Lfpk;->b(Lfpm;)V

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->w:Lfpk;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->y:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->y:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->y:Landroid/app/AlertDialog;

    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Ljp;->onPause()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->A:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->B:Lfpq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->B:Lfpq;

    invoke-virtual {v0, p0}, Lfpq;->a(Lfps;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->B:Lfpq;

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->u:Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->o:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "moment"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->p:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "application"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->q:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "moment_acl"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "manage_error"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
