.class public Lcom/google/android/gms/plus/apps/ManageAppActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;
.implements Landroid/view/View$OnClickListener;
.implements Lbbr;
.implements Lbbs;
.implements Lfos;
.implements Lfov;
.implements Lfox;
.implements Lfpf;
.implements Lfua;
.implements Lfub;
.implements Lfuc;


# static fields
.field private static final o:Ljava/util/ArrayList;


# instance fields
.field private A:Z

.field private B:Landroid/view/View;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/view/View;

.field private E:Landroid/widget/TextView;

.field private F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

.field private G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

.field private H:Lfow;

.field private final I:Lftz;

.field private J:Lftx;

.field private K:Ljava/lang/String;

.field private p:Landroid/accounts/Account;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

.field private v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->o:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;-><init>(Lftz;)V

    return-void
.end method

.method constructor <init>(Lftz;)V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->I:Lftz;

    return-void
.end method

.method private static a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Boolean;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
    .locals 2

    new-instance v1, Lfwk;

    invoke-direct {v1}, Lfwk;-><init>()V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object p1

    :cond_0
    iput-object p1, v1, Lfwk;->b:Lcom/google/android/gms/common/people/data/Audience;

    if-nez p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object p2

    :cond_1
    iput-object p2, v1, Lfwk;->c:Ljava/util/ArrayList;

    if-nez p3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v0

    :goto_0
    iput-boolean v0, v1, Lfwk;->d:Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lfwk;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lfwk;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V
    .locals 2

    new-instance v0, Lbmt;

    invoke-direct {v0, p0}, Lbmt;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lbck;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    invoke-virtual {v1, v0}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lbmt;

    :cond_0
    invoke-static {p0, v0}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    return-void

    :cond_1
    sget-object v0, Lbck;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 12

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v10, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->C:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const v0, 0x7f0b0396    # com.google.android.gms.R.string.plus_manage_app_only_you_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v10, :cond_4

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    const v0, 0x7f0b0397    # com.google.android.gms.R.string.plus_manage_app_limited_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v4, v0

    move-object v2, v1

    move-object v3, v1

    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_7

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v8

    if-ne v8, v10, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v8

    if-ne v8, v10, :cond_5

    move-object v11, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v11

    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v8

    if-ne v8, v10, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v8

    const/4 v9, 0x4

    if-ne v8, v9, :cond_6

    move-object v2, v3

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_3

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v8

    if-ne v8, v10, :cond_b

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_b

    move-object v1, v2

    move-object v2, v3

    goto :goto_3

    :cond_7
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b0398    # com.google.android.gms.R.string.plus_manage_app_public_label

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_8
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b0399    # com.google.android.gms.R.string.plus_manage_app_extended_circles_label

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v6}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v0

    if-ne v0, v10, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b039a    # com.google.android.gms.R.string.plus_manage_app_domain_label

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->C:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_b
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_3
.end method

.method private b(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->E:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0b039b    # com.google.android.gms.R.string.plus_manage_app_all_circles_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const v0, 0x7f0b039c    # com.google.android.gms.R.string.plus_manage_app_no_circles_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    const v0, 0x7f0b0397    # com.google.android.gms.R.string.plus_manage_app_limited_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->C:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Lgqe;->a(Ljava/lang/CharSequence;)Lgqe;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v1}, Lag;->d()I

    return-void
.end method

.method private b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->B:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->D:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->D:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private f()Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;
    .locals 11

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_IS_ASPEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;B)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "AppSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AppSettings"

    const-string v1, "Missing required EXTRA_APP_NAME"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move-object v0, v9

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.plus.APP_ICON_URL"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Ljava/lang/String;

    move-object v4, v9

    move v8, v10

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;B)V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgqe;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgqe;->b()V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    const v1, 0x7f020207    # com.google.android.gms.R.drawable.plus_icon_red_32

    invoke-virtual {v0, v1}, Ljj;->b(I)V

    const v0, 0x7f0a0281    # com.google.android.gms.R.id.acl_description_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00b6    # com.google.android.gms.R.id.pacl_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->B:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->B:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0284    # com.google.android.gms.R.id.pacl_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->C:Landroid/widget/TextView;

    const v0, 0x7f0a00b2    # com.google.android.gms.R.id.facl_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->D:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->D:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0286    # com.google.android.gms.R.id.facl_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->E:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k()V

    return-void
.end method

.method private i()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0a0280    # com.google.android.gms.R.id.acl_description_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0281    # com.google.android.gms.R.id.acl_description_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0282    # com.google.android.gms.R.id.acl_description_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a0283    # com.google.android.gms.R.id.pacl_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->B:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0285    # com.google.android.gms.R.id.facl_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    goto :goto_0

    :cond_3
    const v1, 0x7f0b038c    # com.google.android.gms.R.string.plus_manage_app_no_acl_description

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method private j()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f0a0288    # com.google.android.gms.R.id.scopes_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a007e    # com.google.android.gms.R.id.scopes_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0287    # com.google.android.gms.R.id.scopes_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f0a0289    # com.google.android.gms.R.id.disconnect_divider

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a028a    # com.google.android.gms.R.id.disconnect_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a028b    # com.google.android.gms.R.id.disconnect_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b0395    # com.google.android.gms.R.string.plus_manage_app_disconnect_label

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final I_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method public final P_()V
    .locals 0

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l()V

    return-void
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->z:Z

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "AppSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load application ACLs: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lbbo;Lfxi;Ljava/lang/String;)V
    .locals 5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->A:Z

    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lfxi;->a()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p2, v0}, Lfxi;->b(I)Lfxh;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    invoke-interface {v2}, Lfxh;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Lfxh;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Ljava/lang/String;

    invoke-interface {v2}, Lfxh;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f()Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Ljava/lang/String;

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->A:Z

    iput-object p3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->K:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    sget-object v0, Lfsr;->E:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, p0, v0, p3}, Lftx;->a(Lfuc;ILjava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Lfxh;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-eqz p2, :cond_0

    const v0, 0x7f0a0080    # com.google.android.gms.R.id.app_icon

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p0}, Lfnz;->a(Landroid/content/Context;)Lfnz;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lfnz;->a(Lfxh;Landroid/graphics/drawable/Drawable;)Lfoa;

    :cond_0
    return-void
.end method

.method public final a(Lfxh;Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->H:Lfow;

    invoke-virtual {v0, p1, p2}, Lfow;->a(Lfxh;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b037f    # com.google.android.gms.R.string.plus_disconnect_source_progress_dialog_message

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Lfnz;->a(Landroid/content/Context;)Lfnz;

    move-result-object v2

    invoke-virtual {v2, p1}, Lfnz;->a(Lfxh;)Lfoa;

    move-result-object v2

    iget-object v2, v2, Lfoa;->a:Ljava/lang/CharSequence;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0b0380    # com.google.android.gms.R.string.plus_disconnect_source_failed_dialog_message

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {p1}, Lfxh;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgqd;->a(Ljava/lang/String;)Lgqd;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Lgqd;->a(Lu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbbo;Lfxh;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Z

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g()V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    const v2, 0x7f0b0380    # com.google.android.gms.R.string.plus_disconnect_source_failed_dialog_message

    new-array v3, v1, [Ljava/lang/Object;

    invoke-interface {p2}, Lfxh;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lgqd;->a(Ljava/lang/String;)Lgqd;

    move-result-object v2

    :try_start_0
    iget-object v3, p0, Lo;->b:Lw;

    invoke-virtual {v3}, Lu;->a()Lag;

    move-result-object v3

    const-string v4, "error_dialog"

    invoke-virtual {v3, v2, v4}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v3}, Lag;->c()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.plus.DISCONNECTED_APP_ID"

    invoke-interface {p2}, Lfxh;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    return-void
.end method

.method public final b(Lbbo;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const v0, 0x7f0b039e    # com.google.android.gms.R.string.plus_manage_app_acl_error

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iput-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-static {v0, v1, v3, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Boolean;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    const-string v0, "AppSettings"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mAppACLs: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    sget-object v0, Lbcj;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-static {v1}, Lbcl;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    iput-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v3, v1, v2}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Boolean;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    const-string v0, "AppSettings"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mAppAcls: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    sget-object v0, Lbcj;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v2

    invoke-static {v1, v2}, Lbcl;->a(Ljava/util/List;Z)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    iput-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    goto/16 :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    invoke-static {}, Lfot;->a()Lfot;

    const/4 v0, 0x3

    invoke-static {p0, v0}, Lfot;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->z:Z

    if-nez v0, :cond_2

    iput-boolean v4, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    invoke-interface {v0, p0, v1}, Lftx;->a(Lfub;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->A:Z

    if-nez v0, :cond_1

    iput-boolean v4, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->A:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    sget-object v0, Lfsr;->E:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->K:Ljava/lang/String;

    invoke-interface {v1, p0, v0, v2}, Lftx;->a(Lfuc;ILjava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-interface {v0, p0, v1, v2}, Lftx;->a(Lfua;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v3

    invoke-interface {v0, p0, v1, v2, v3}, Lftx;->a(Lfua;Ljava/lang/String;Ljava/util/List;Z)V

    goto :goto_0
.end method

.method public final h_()V
    .locals 1

    invoke-super {p0}, Ljp;->h_()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->H:Lfow;

    invoke-virtual {v0}, Lfow;->a()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const v4, 0x7f0b039d    # com.google.android.gms.R.string.plus_manage_app_updating_acl

    const/4 v3, 0x2

    const/4 v1, -0x1

    const/4 v0, 0x1

    if-nez p1, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Z)V

    if-ne p2, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Lbkw;

    invoke-direct {v0}, Lbkw;-><init>()V

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    :cond_0
    invoke-static {p3}, Lbew;->b(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Lbkw;

    invoke-direct {v2, v0}, Lbkw;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v2, v1}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v0

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Ljava/lang/String;)V

    new-instance v1, Lfwk;

    invoke-direct {v1}, Lfwk;-><init>()V

    iput-object v0, v1, Lfwk;->b:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lfwk;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    const-string v0, "AppSettings"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mPaclToWrite: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-ne p1, v0, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c(Z)V

    if-ne p2, v1, :cond_1

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Ljava/lang/String;)V

    invoke-static {p3}, Lbfa;->a(Landroid/content/Intent;)Lbfc;

    move-result-object v0

    new-instance v1, Lfwk;

    invoke-direct {v1}, Lfwk;-><init>()V

    invoke-interface {v0}, Lbfc;->b()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lfwk;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Lbfc;->c()Z

    move-result v0

    iput-boolean v0, v1, Lfwk;->d:Z

    invoke-virtual {v1}, Lfwk;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    const-string v0, "AppSettings"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mFaclToWrite: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0281    # com.google.android.gms.R.id.acl_description_layout

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.VIEW_ACTIVITY_LOG"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.extras.ACCOUNT_NAME"

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "application"

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v1, Lbck;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbck;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->r:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2, v3}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0a00b6    # com.google.android.gms.R.id.pacl_layout

    if-ne v0, v1, :cond_3

    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->o:Ljava/util/ArrayList;

    :cond_2
    invoke-static {}, Lbet;->a()Lbeu;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Lbeu;->a(Ljava/lang/String;)Lbeu;

    move-result-object v1

    invoke-interface {v1, v0}, Lbeu;->a(Ljava/util/List;)Lbeu;

    move-result-object v0

    const v1, 0x7f0b038e    # com.google.android.gms.R.string.plus_manage_app_pacl_sub_label

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbeu;->d(Ljava/lang/String;)Lbeu;

    move-result-object v0

    const-string v1, "81"

    invoke-interface {v0, v1}, Lbeu;->b(Ljava/lang/String;)Lbeu;

    move-result-object v0

    invoke-interface {v0}, Lbeu;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivityForResult(Landroid/content/Intent;I)V

    sget-object v0, Lbcj;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    goto :goto_0

    :cond_3
    const v1, 0x7f0a00b2    # com.google.android.gms.R.id.facl_layout

    if-ne v0, v1, :cond_5

    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v1

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->o:Ljava/util/ArrayList;

    :cond_4
    invoke-static {}, Lbfa;->a()Lbfb;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v3}, Lbfb;->l(Ljava/lang/String;)Lbfb;

    move-result-object v2

    invoke-interface {v2, v0}, Lbfb;->e(Ljava/util/List;)Lbfb;

    move-result-object v0

    invoke-interface {v0, v1}, Lbfb;->b(Z)Lbfb;

    move-result-object v0

    const-string v1, "81"

    invoke-interface {v0, v1}, Lbfb;->f(Ljava/lang/String;)Lbew;

    move-result-object v0

    iget-object v0, v0, Lbew;->a:Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivityForResult(Landroid/content/Intent;I)V

    sget-object v0, Lbcj;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    goto/16 :goto_0

    :cond_5
    const v1, 0x7f0a028a    # com.google.android.gms.R.id.disconnect_layout

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Z

    invoke-static {v0, v1, v2, v3}, Lfou;->a(Landroid/accounts/Account;Lfxh;Ljava/lang/String;Z)Lfou;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "disconnect_source_dialog"

    invoke-virtual {v0, v1, v2}, Lfou;->a(Lu;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AppSettings"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->r:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->r:Ljava/lang/String;

    invoke-static {v0, v1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    const-string v0, "app_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    const-string v0, "app_entity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    const-string v0, "app_acls"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    const-string v0, "app_acls_loading"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->z:Z

    const-string v0, "pacl_to_write"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    const-string v0, "facl_to_write"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    const-string v0, "scopes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Ljava/lang/String;

    const-string v0, "revoke_handle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Ljava/lang/String;

    const-string v0, "is_signed_up"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "is_signed_up"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Z

    iput-boolean v5, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->x:Z

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    if-nez v0, :cond_6

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.ACCOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_SCOPES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_REVOKE_HANDLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Ljava/lang/String;

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    if-nez v0, :cond_9

    :cond_7
    const-string v0, "AppSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "AppSettings"

    const-string v1, "Missing required extra(s): account=%s appId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l()V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "service_googleme"

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1, p0}, Lbkv;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;)Landroid/accounts/AccountManagerFuture;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v0, "disconnect_source_fragment"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lfow;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->H:Lfow;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->H:Lfow;

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    invoke-static {v0}, Lfow;->a(Landroid/accounts/Account;)Lfow;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->H:Lfow;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->H:Lfow;

    const-string v2, "disconnect_source_fragment"

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    :cond_a
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f()Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v0, :cond_b

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l()V

    goto/16 :goto_0

    :cond_b
    invoke-static {p0}, Lfnz;->a(Landroid/content/Context;)Lfnz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0, v1}, Lfnz;->a(Lfxh;)Lfoa;

    move-result-object v1

    iget-boolean v0, v1, Lfoa;->c:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-static {p0}, Lfpd;->a(Landroid/content/Context;)Lfpd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfpd;->a(Lfpf;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lfpd;->a(Lfxh;Ljava/lang/String;)V

    :cond_c
    const v0, 0x7f0400da    # com.google.android.gms.R.layout.plus_manage_app_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setContentView(I)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v2

    if-eqz v2, :cond_d

    const v2, 0x7f0b038a    # com.google.android.gms.R.string.plus_manage_aspen_app_label

    invoke-virtual {v0, v2}, Ljj;->d(I)V

    :goto_1
    invoke-virtual {v0, v5}, Ljj;->a(Z)V

    const v0, 0x7f0a0172    # com.google.android.gms.R.id.app_name

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v1, Lfoa;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0080    # com.google.android.gms.R.id.app_icon

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, v1, Lfoa;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h()V

    invoke-static {}, Lfot;->a()Lfot;

    const-string v0, "disabled_dialog"

    invoke-static {p0, v6}, Lfot;->a(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {p0, v6}, Lfot;->b(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Lfor;->a(Ljava/lang/CharSequence;)Lfor;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lfor;->a(Lu;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const v2, 0x7f0b0389    # com.google.android.gms.R.string.plus_manage_app_label

    invoke-virtual {v0, v2}, Ljj;->d(I)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    const v1, 0x7f0b0473    # com.google.android.gms.R.string.common_list_apps_menu_help

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    invoke-super {p0, p1}, Ljp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Ljp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v0, Lfsr;->A:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    const-class v1, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Ljp;->onPause()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "app_id"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "app_entity"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "app_acls"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "app_acls_loading"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "pacl_to_write"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->F:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "facl_to_write"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->G:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->x:Z

    if-eqz v0, :cond_0

    const-string v0, "is_signed_up"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->x:Z

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h()V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->I:Lftz;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p0, p0, p0, v1}, Lfob;->a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->I:Lftz;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p0, p0, p0, v1}, Lfob;->b(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->J:Lftx;

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method
