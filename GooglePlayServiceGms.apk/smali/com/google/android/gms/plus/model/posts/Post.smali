.class public Lcom/google/android/gms/plus/model/posts/Post;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lfxz;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/List;

.field private final d:Landroid/net/Uri;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Landroid/os/Bundle;

.field private final i:Landroid/os/Bundle;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/Boolean;

.field private final l:Ljava/lang/String;

.field private final m:Lcom/google/android/gms/common/people/data/Audience;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfxz;

    invoke-direct {v0}, Lfxz;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/model/posts/Post;->CREATOR:Lfxz;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/plus/model/posts/Post;->a:I

    iput-object p2, p0, Lcom/google/android/gms/plus/model/posts/Post;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/plus/model/posts/Post;->c:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/gms/plus/model/posts/Post;->d:Landroid/net/Uri;

    iput-object p5, p0, Lcom/google/android/gms/plus/model/posts/Post;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/plus/model/posts/Post;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/plus/model/posts/Post;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/plus/model/posts/Post;->h:Landroid/os/Bundle;

    iput-object p9, p0, Lcom/google/android/gms/plus/model/posts/Post;->i:Landroid/os/Bundle;

    iput-object p10, p0, Lcom/google/android/gms/plus/model/posts/Post;->j:Ljava/lang/String;

    invoke-static {p11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->k:Ljava/lang/Boolean;

    iput-object p12, p0, Lcom/google/android/gms/plus/model/posts/Post;->l:Ljava/lang/String;

    iput-object p13, p0, Lcom/google/android/gms/plus/model/posts/Post;->m:Lcom/google/android/gms/common/people/data/Audience;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 14

    const/4 v1, 0x1

    invoke-virtual/range {p10 .. p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/google/android/gms/plus/model/posts/Post;-><init>(ILjava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    return-void
.end method

.method public static a(Landroid/net/Uri;Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/String;)Lcom/google/android/gms/plus/model/posts/Post;
    .locals 14

    new-instance v0, Lcom/google/android/gms/plus/model/posts/Post;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x1

    move-object v4, p0

    move-object v8, p1

    move-object/from16 v10, p4

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    invoke-direct/range {v0 .. v13}, Lcom/google/android/gms/plus/model/posts/Post;-><init>(ILjava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->m:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->c:Ljava/util/List;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    instance-of v0, p1, Lcom/google/android/gms/plus/model/posts/Post;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/model/posts/Post;

    iget v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->a:I

    iget v3, p1, Lcom/google/android/gms/plus/model/posts/Post;->a:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->m:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v3, p1, Lcom/google/android/gms/plus/model/posts/Post;->m:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/gms/plus/model/posts/Post;->c:Ljava/util/List;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->d:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/gms/plus/model/posts/Post;->d:Landroid/net/Uri;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/plus/model/posts/Post;->e:Ljava/lang/String;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/plus/model/posts/Post;->f:Ljava/lang/String;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/plus/model/posts/Post;->g:Ljava/lang/String;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/plus/model/posts/Post;->j:Ljava/lang/String;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/model/posts/Post;->k:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/android/gms/plus/model/posts/Post;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->l:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/plus/model/posts/Post;->l:Ljava/lang/String;

    invoke-static {v0, v4}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3, v0}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->f:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->m:Lcom/google/android/gms/common/people/data/Audience;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->c:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->d:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->k:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Post;->l:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->h:Landroid/os/Bundle;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->h:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->i:Landroid/os/Bundle;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->i:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->i:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final r()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/model/posts/Post;->a:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lfxz;->a(Lcom/google/android/gms/plus/model/posts/Post;Landroid/os/Parcel;I)V

    return-void
.end method
