.class public Lcom/google/android/gms/appdatasearch/ResultClickInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lain;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:[Lcom/google/android/gms/appdatasearch/DocumentId;

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lain;

    invoke-direct {v0}, Lain;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->CREATOR:Lain;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->c:[Lcom/google/android/gms/appdatasearch/DocumentId;

    iput p4, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->d:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[Lcom/google/android/gms/appdatasearch/DocumentId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->c:[Lcom/google/android/gms/appdatasearch/DocumentId;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->CREATOR:Lain;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->CREATOR:Lain;

    invoke-static {p0, p1, p2}, Lain;->a(Lcom/google/android/gms/appdatasearch/ResultClickInfo;Landroid/os/Parcel;I)V

    return-void
.end method
