.class public Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Laie;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:[Lcom/google/android/gms/appdatasearch/CorpusId;

.field public final d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Laie;

    invoke-direct {v0}, Laie;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Laie;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/CorpusId;[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->c:[Lcom/google/android/gms/appdatasearch/CorpusId;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->d:[I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, v1, v1}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;-><init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/CorpusId;[I)V

    return-void
.end method

.method public constructor <init>([Lcom/google/android/gms/appdatasearch/CorpusId;[I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;-><init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/CorpusId;[I)V

    array-length v1, p1

    if-eqz v1, :cond_0

    array-length v1, p2

    array-length v2, p1

    rem-int/2addr v1, v2

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Laie;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Laie;

    invoke-static {p0, p1, p2}, Laie;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;Landroid/os/Parcel;I)V

    return-void
.end method
