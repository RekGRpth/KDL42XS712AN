.class public Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Laif;


# instance fields
.field public final a:I

.field public final b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Laif;

    invoke-direct {v0}, Laif;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Laif;

    return-void
.end method

.method public constructor <init>(I[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    return-void
.end method


# virtual methods
.method public final a()[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    invoke-virtual {v0}, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Laif;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Laif;

    invoke-static {p0, p1, p2}, Laif;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;Landroid/os/Parcel;I)V

    return-void
.end method
