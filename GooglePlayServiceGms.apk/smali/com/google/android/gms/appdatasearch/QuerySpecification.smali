.class public Lcom/google/android/gms/appdatasearch/QuerySpecification;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Laig;


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Ljava/util/List;

.field public final d:Ljava/util/List;

.field public final e:Z

.field public final f:I

.field public final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Laig;

    invoke-direct {v0}, Laig;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Laig;

    return-void
.end method

.method public constructor <init>(IZLjava/util/List;Ljava/util/List;ZII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->a:I

    iput-boolean p2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->b:Z

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    iput-boolean p5, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->e:Z

    iput p6, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->f:I

    iput p7, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->g:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Laig;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Laig;

    invoke-static {p0, p1}, Laig;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/Parcel;)V

    return-void
.end method
