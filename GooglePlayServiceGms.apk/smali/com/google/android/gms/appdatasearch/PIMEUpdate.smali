.class public Lcom/google/android/gms/appdatasearch/PIMEUpdate;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Laib;


# instance fields
.field public final a:I

.field public final b:[B

.field public final c:[B

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:Landroid/os/Bundle;

.field public final i:J

.field public final j:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Laib;

    invoke-direct {v0}, Laib;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Laib;

    return-void
.end method

.method public constructor <init>(I[B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->b:[B

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->c:[B

    iput p4, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->d:I

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->f:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->g:Z

    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->h:Landroid/os/Bundle;

    iput-wide p9, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->i:J

    iput-wide p11, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->j:J

    return-void
.end method

.method public constructor <init>([B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJ)V
    .locals 13

    const/4 v1, 0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-wide/from16 v9, p8

    move-wide/from16 v11, p10

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/appdatasearch/PIMEUpdate;-><init>(I[B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJ)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Laib;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Laib;

    invoke-static {p0, p1}, Laib;->a(Lcom/google/android/gms/appdatasearch/PIMEUpdate;Landroid/os/Parcel;)V

    return-void
.end method
