.class public Lcom/google/android/gms/people/service/PeopleRequestProcessor;
.super Lbeo;
.source "SourceFile"


# static fields
.field public static final b:Lffd;

.field private static final c:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lfgx;

    invoke-direct {v0}, Lfgx;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->b:Lffd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lfbd;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lbeo;-><init>(I)V

    return-void
.end method

.method public static synthetic a()Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    sget-object v0, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffb;

    if-nez v0, :cond_0

    const-string v0, "PeopleRP"

    const-string v1, "No operation found when processing!"

    invoke-static {v0, v1}, Lfdk;->f(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lffb;->b()V

    goto :goto_0
.end method
