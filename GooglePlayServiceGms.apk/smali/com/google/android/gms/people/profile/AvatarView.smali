.class public Lcom/google/android/gms/people/profile/AvatarView;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# static fields
.field private static b:Z

.field private static c:I

.field private static d:Z

.field private static e:Landroid/graphics/Paint;

.field private static f:Landroid/graphics/Paint;

.field private static g:I


# instance fields
.field private A:Lfem;

.field private B:F

.field private C:F

.field private D:Z

.field private E:F

.field private F:J

.field private G:Lfeo;

.field private H:Lfen;

.field private I:Lfel;

.field private J:F

.field private K:Landroid/graphics/RectF;

.field private L:Landroid/graphics/RectF;

.field private M:Landroid/graphics/RectF;

.field private N:[F

.field a:F

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/Matrix;

.field private j:Landroid/graphics/Matrix;

.field private k:Landroid/graphics/Matrix;

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Landroid/graphics/Rect;

.field private r:I

.field private s:I

.field private t:Landroid/view/GestureDetector;

.field private u:Landroid/view/ScaleGestureDetector;

.field private v:Landroid/view/View$OnClickListener;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->x:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->f()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->x:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->f()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->x:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->f()V

    return-void
.end method

.method private a(FFF)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->J:F

    neg-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->C:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->d()F

    move-result v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->e()V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->J:F

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->e()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/people/profile/AvatarView;F)V
    .locals 3

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->J:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->J:F

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/people/profile/AvatarView;FFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/profile/AvatarView;->a(FFF)V

    return-void
.end method

.method private a(Z)V
    .locals 9

    const/4 v3, 0x0

    const/high16 v8, 0x41000000    # 8.0f

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->m:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    if-nez p1, :cond_2

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    cmpl-float v0, v0, v7

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->m:Z

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    int-to-float v5, v0

    int-to-float v6, v1

    invoke-virtual {v4, v7, v7, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->a:F

    int-to-float v0, v3

    int-to-float v1, v2

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->a:F

    cmpl-float v0, v1, v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v1, v2

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->a:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    sub-int v4, v0, v1

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->d()F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    mul-float/2addr v1, v8

    invoke-static {v1, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->C:F

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v1, v3

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->a:F

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    sub-int v3, v0, v1

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1
.end method

.method private a(FF)Z
    .locals 8

    const/high16 v7, 0x40000000    # 2.0f

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-boolean v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    if-eqz v4, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget-boolean v5, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    if-eqz v5, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    invoke-static {v2, p2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    cmpl-float v0, v0, p1

    if-nez v0, :cond_4

    cmpl-float v0, v1, p2

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    sub-float v4, v3, v2

    sub-float v5, v1, v0

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    sub-float/2addr v1, v0

    add-float/2addr v2, v3

    sub-float/2addr v1, v2

    div-float/2addr v1, v7

    add-float/2addr v0, v1

    goto :goto_0

    :cond_1
    sub-float/2addr v1, v3

    sub-float/2addr v0, v2

    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0

    :cond_2
    sub-float v5, v4, v3

    sub-float v6, v2, v1

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    sub-float/2addr v2, v1

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    div-float/2addr v2, v7

    add-float/2addr v1, v2

    goto :goto_1

    :cond_3
    sub-float/2addr v2, v4

    sub-float/2addr v1, v3

    invoke-static {v1, p2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/gms/people/profile/AvatarView;FF)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/profile/AvatarView;->a(FF)Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    iput-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    return-void
.end method

.method private d()F
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private e()V
    .locals 10

    const/high16 v9, 0x41a00000    # 20.0f

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    sub-float v5, v4, v3

    sub-float v6, v2, v0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    sub-float/2addr v2, v0

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    div-float/2addr v2, v8

    add-float/2addr v0, v2

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float v6, v5, v4

    sub-float v7, v3, v2

    cmpg-float v6, v6, v7

    if-gez v6, :cond_5

    sub-float v1, v3, v2

    add-float v3, v5, v4

    sub-float/2addr v1, v3

    div-float/2addr v1, v8

    add-float/2addr v1, v2

    :cond_0
    :goto_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v9

    if-gtz v2, :cond_1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v9

    if-lez v2, :cond_7

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->H:Lfen;

    invoke-virtual {v2, v0, v1}, Lfen;->a(FF)Z

    :goto_2
    return-void

    :cond_2
    cmpl-float v5, v3, v0

    if-lez v5, :cond_3

    sub-float/2addr v0, v3

    goto :goto_0

    :cond_3
    cmpg-float v0, v4, v2

    if-gez v0, :cond_4

    sub-float v0, v2, v4

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    cmpl-float v6, v4, v2

    if-lez v6, :cond_6

    sub-float v1, v2, v4

    goto :goto_1

    :cond_6
    cmpg-float v2, v5, v3

    if-gez v2, :cond_0

    sub-float v1, v3, v5

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    goto :goto_2
.end method

.method private f()V
    .locals 5

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-boolean v2, Lcom/google/android/gms/people/profile/AvatarView;->b:Z

    if-nez v2, :cond_0

    sput-boolean v0, Lcom/google/android/gms/people/profile/AvatarView;->b:Z

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c003b    # com.google.android.gms.R.color.people_avatar_preview_background

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/people/profile/AvatarView;->g:I

    const v3, 0x7f0d003b    # com.google.android.gms.R.dimen.people_avatar_preview_profile_width

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/people/profile/AvatarView;->c:I

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/gms/people/profile/AvatarView;->e:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->e:Landroid/graphics/Paint;

    const v4, 0x7f0c003c    # com.google.android.gms.R.color.people_avatar_preview_crop_dim

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->e:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    const v4, 0x7f0c003d    # com.google.android.gms.R.color.people_avatar_preview_crop_highlight

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    const v4, 0x7f0d003c    # com.google.android.gms.R.dimen.people_avatar_preview_stroke_width

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/google/android/gms/people/profile/AvatarView;->d:Z

    :cond_0
    new-instance v2, Landroid/view/GestureDetector;

    const/4 v3, 0x0

    sget-boolean v4, Lcom/google/android/gms/people/profile/AvatarView;->d:Z

    if-nez v4, :cond_1

    :goto_0
    invoke-direct {v2, v1, p0, v3, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->t:Landroid/view/GestureDetector;

    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, v1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->u:Landroid/view/ScaleGestureDetector;

    new-instance v0, Lfem;

    invoke-direct {v0, p0}, Lfem;-><init>(Lcom/google/android/gms/people/profile/AvatarView;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->A:Lfem;

    new-instance v0, Lfeo;

    invoke-direct {v0, p0}, Lfeo;-><init>(Lcom/google/android/gms/people/profile/AvatarView;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->G:Lfeo;

    new-instance v0, Lfen;

    invoke-direct {v0, p0}, Lfen;-><init>(Lcom/google/android/gms/people/profile/AvatarView;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->H:Lfen;

    new-instance v0, Lfel;

    invoke-direct {v0, p0}, Lfel;-><init>(Lcom/google/android/gms/people/profile/AvatarView;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->I:Lfel;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->c()V

    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 7

    const/16 v4, 0x100

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    const/high16 v1, 0x43800000    # 256.0f

    int-to-float v0, v0

    div-float v0, v1, v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v4, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c003b    # com.google.android.gms.R.color.people_avatar_preview_background

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    new-instance v5, Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    invoke-direct {v5, v6}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    add-int v6, v1, v2

    if-eqz v6, :cond_0

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_0
    add-float v1, v0, v0

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    :cond_1
    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, Lfbd;->N:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v3, v2, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v1}, Lbpm;->a(Ljava/io/Closeable;)V

    return-void

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-static {v1}, Lbpm;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->b()V

    sget-object v0, Lfbd;->M:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, v0, v0}, Lbpn;->a(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No bitmap loaded from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eq v2, v0, :cond_4

    :cond_1
    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v0, v3, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->b()V

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    if-eqz v2, :cond_3

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->x:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->y:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->d()F

    move-result v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->C:F

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->A:Lfem;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v0, v1, v3, v4}, Lfem;->a(FFFF)Z

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->y:Z

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->G:Lfeo;

    invoke-virtual {v0}, Lfeo;->a()V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->H:Lfen;

    invoke-virtual {v0}, Lfen;->a()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    sget v0, Lcom/google/android/gms/people/profile/AvatarView;->g:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/gms/people/profile/AvatarView;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->o:Z

    if-eqz v0, :cond_0

    const/4 p3, 0x0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->D:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->G:Lfeo;

    invoke-virtual {v0, p3, p4}, Lfeo;->a(FF)Z

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->D:Z

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->m:Z

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    sget v3, Lcom/google/android/gms/people/profile/AvatarView;->c:I

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->r:I

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->r:I

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->s:I

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->r:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->s:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->r:I

    add-int/2addr v2, v0

    iget v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->s:I

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/profile/AvatarView;->a(Z)V

    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->p:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    cmpg-float v1, v0, v2

    if-gez v1, :cond_2

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_3

    :cond_2
    cmpl-float v1, v0, v2

    if-lez v1, :cond_4

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    :cond_3
    iput v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    :cond_4
    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3d23d70a    # 0.04f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->d()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/people/profile/AvatarView;->a(FFF)V

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->A:Lfem;

    invoke-virtual {v0}, Lfem;->a()V

    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    :cond_0
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->y:Z

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->c()V

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->D:Z

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->F:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->o:Z

    if-eqz v2, :cond_0

    const/4 p3, 0x0

    :cond_0
    const-wide/16 v2, 0x190

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_1

    neg-float v0, p3

    neg-float v1, p4

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarView;->a(FF)Z

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->v:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->v:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->u:Landroid/view/ScaleGestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->t:Landroid/view/GestureDetector;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->u:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->t:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->G:Lfeo;

    invoke-static {v0}, Lfeo;->a(Lfeo;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->e()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->F:J

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v2, :cond_2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->F:J

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/people/profile/AvatarView;->v:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setVisibility(I)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
