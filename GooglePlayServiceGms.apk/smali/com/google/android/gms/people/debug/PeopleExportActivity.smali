.class public Lcom/google/android/gms/people/debug/PeopleExportActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "PeopleExportActivity"

    sput-object v0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/people/debug/PeopleExportActivity;Landroid/net/Uri;)V
    .locals 3

    sget-object v0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->a:Ljava/lang/String;

    const-string v1, "Drafting email"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.SUBJECT"

    const v2, 0x7f0b01bf    # com.google.android.gms.R.string.people_export_email_subject

    invoke-virtual {p0, v2}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    const v2, 0x7f0b01c0    # com.google.android.gms.R.string.people_export_email_body

    invoke-virtual {p0, v2}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "application/zip"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const v1, 0x7f0b01be    # com.google.android.gms.R.string.people_export_email_sender_picker

    invoke-virtual {p0, v1}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->d:Landroid/widget/Button;

    invoke-static {p0}, Lfbs;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b()V

    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v0, Lfbt;

    invoke-direct {v0, p0, v1}, Lfbt;-><init>(Lcom/google/android/gms/people/debug/PeopleExportActivity;B)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lfbt;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :sswitch_1
    invoke-static {p0}, Lfbs;->c(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a009d -> :sswitch_2    # com.google.android.gms.R.id.cancel
        0x7f0a0249 -> :sswitch_0    # com.google.android.gms.R.id.confirm
        0x7f0a024a -> :sswitch_1    # com.google.android.gms.R.id.delete
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbox;->c(Landroid/app/Activity;)Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->requestWindowFeature(I)Z

    const v0, 0x7f0400b6    # com.google.android.gms.R.layout.people_export_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x1080027    # android.R.drawable.ic_dialog_alert

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    const v0, 0x7f0a0249    # com.google.android.gms.R.id.confirm

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b:Landroid/widget/Button;

    const v0, 0x7f0a009d    # com.google.android.gms.R.id.cancel

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->c:Landroid/widget/Button;

    const v0, 0x7f0a024a    # com.google.android.gms.R.id.delete

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->d:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b()V

    return-void
.end method
