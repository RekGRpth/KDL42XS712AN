.class public Lcom/google/android/gms/people/settings/PeopleSettingsActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lbbr;
.implements Lbbs;
.implements Lfau;


# instance fields
.field private o:Lfaj;

.field private p:Landroid/widget/Spinner;

.field private q:Landroid/webkit/WebView;

.field private r:Landroid/widget/ArrayAdapter;

.field private final s:Ljava/lang/Runnable;

.field private final t:Landroid/os/Handler;

.field private final u:Lfat;

.field private final v:Lfat;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljp;-><init>()V

    new-instance v0, Lfhj;

    invoke-direct {v0, p0}, Lfhj;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->s:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->t:Landroid/os/Handler;

    new-instance v0, Lfhl;

    invoke-direct {v0, p0}, Lfhl;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->u:Lfat;

    new-instance v0, Lfhm;

    invoke-direct {v0, p0}, Lfhm;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->v:Lfat;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->f()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->s:Ljava/lang/Runnable;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->q:Landroid/webkit/WebView;

    const-string v1, ""

    const-string v3, "text/plain"

    const-string v4, "UTF-8"

    const-string v5, ""

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->t:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)Landroid/widget/Spinner;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->p:Landroid/widget/Spinner;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->q:Landroid/webkit/WebView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->pageDown(Z)Z

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 2

    const-string v0, "PeopleSettingsActivity"

    const-string v1, "Disconnected from GMS-core."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to connect.  result="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lbbo;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to load log.  result="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lfch;->a(Lfau;Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "internal_call_method"

    const-string v2, "GET_SHOW_SYNC_NOTIFICATION_ERROR"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    iget-object v2, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->u:Lfat;

    invoke-virtual {v1, v2, v0}, Lfaj;->a(Lfat;Landroid/os/Bundle;)V

    const-string v0, "Loading log..."

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0400b8    # com.google.android.gms.R.layout.people_settings

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->setContentView(I)V

    const-string v0, "People debug"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljj;->a(Z)V

    const v0, 0x7f0a024b    # com.google.android.gms.R.id.show_notification_spinner

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->p:Landroid/widget/Spinner;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f0400b7    # com.google.android.gms.R.layout.people_setting_spinner_view

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->r:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->r:Landroid/widget/ArrayAdapter;

    const-string v1, "GServices default"

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->r:Landroid/widget/ArrayAdapter;

    const-string v1, "Disable"

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->r:Landroid/widget/ArrayAdapter;

    const-string v1, "Enable"

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->p:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->r:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->p:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->p:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    const v0, 0x7f0a024c    # com.google.android.gms.R.id.text_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->q:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->q:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->q:Landroid/webkit/WebView;

    new-instance v1, Lfhk;

    invoke-direct {v1, p0}, Lfhk;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    new-instance v0, Lfaj;

    invoke-direct {v0, p0, p0, p0}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;)V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120016    # com.google.android.gms.R.menu.people_settings_menu

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Ljp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "internal_call_method"

    const-string v2, "SET_SHOW_SYNC_NOTIFICATION_ERROR"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "internal_call_arg_1"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    iget-object v2, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->v:Lfat;

    invoke-virtual {v1, v2, v0}, Lfaj;->a(Lfat;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Ljp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->q:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->pageUp(Z)Z

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->f()V

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.people.DUMP_DATABASE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a038e
        :pswitch_0    # com.google.android.gms.R.id.scroll_to_top
        :pswitch_1    # com.google.android.gms.R.id.scroll_to_end
        :pswitch_2    # com.google.android.gms.R.id.export_database
    .end packed-switch
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Ljp;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->o:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->s:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Ljp;->onStop()V

    return-void
.end method
