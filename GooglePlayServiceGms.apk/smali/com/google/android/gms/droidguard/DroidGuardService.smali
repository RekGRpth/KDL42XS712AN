.class public Lcom/google/android/gms/droidguard/DroidGuardService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Lcqs;

.field private final c:Lcpk;

.field private final d:Ljava/lang/ThreadLocal;

.field private final e:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcpg;

    invoke-direct {v0, p0}, Lcpg;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;)V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->a:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lcpj;

    invoke-direct {v1, p0, v2}, Lcpj;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;B)V

    invoke-static {p0, v0, v1}, Lcqv;->a(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/Object;)Lcqs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->b:Lcqs;

    new-instance v0, Lcpk;

    invoke-direct {v0, p0, v2}, Lcpk;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;B)V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->c:Lcpk;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->d:Ljava/lang/ThreadLocal;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->e:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/droidguard/DroidGuardService;)Ljava/lang/ThreadLocal;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->d:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/droidguard/DroidGuardService;Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpl;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcpl;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/droidguard/DroidGuardService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/droidguard/DroidGuardService;)Lcqs;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->b:Lcqs;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.gms.droidguard.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->c:Lcpk;

    invoke-virtual {v0}, Lcpk;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
