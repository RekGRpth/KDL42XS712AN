.class public Lcom/google/android/gms/maps/internal/CreatorImpl;
.super Leti;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Leti;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Google Play services package version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/i/f;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public init(Lcrv;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/maps/internal/CreatorImpl;->initV2(Lcrv;I)V

    return-void
.end method

.method public initV2(Lcrv;I)V
    .locals 3

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Google Play services client version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/i/f;->a(ILjava/lang/String;)V

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {v0}, Lmaps/e/be;->a(Landroid/content/res/Resources;)V

    invoke-static {p2}, Lmaps/e/ci;->a(I)V

    invoke-static {p2}, Levn;->a(I)V

    return-void
.end method

.method public newBitmapDescriptorFactoryDelegate()Lewf;
    .locals 1

    new-instance v0, Lmaps/e/e;

    invoke-direct {v0}, Lmaps/e/e;-><init>()V

    return-object v0
.end method

.method public newCameraUpdateFactoryDelegate()Letb;
    .locals 1

    new-instance v0, Lmaps/e/s;

    invoke-direct {v0}, Lmaps/e/s;-><init>()V

    return-object v0
.end method

.method public newMapFragmentDelegate(Lcrv;)Lett;
    .locals 1

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/CreatorImpl;->a(Landroid/content/Context;)V

    invoke-static {v0}, Lmaps/e/bg;->a(Landroid/app/Activity;)Lmaps/e/bg;

    move-result-object v0

    return-object v0
.end method

.method public newMapViewDelegate(Lcrv;Lcom/google/android/gms/maps/GoogleMapOptions;)Letw;
    .locals 2

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/CreatorImpl;->a(Landroid/content/Context;)V

    new-instance v1, Lmaps/e/bk;

    invoke-direct {v1, v0, p2}, Lmaps/e/bk;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    return-object v1
.end method
