.class public Lcom/google/android/gms/gcm/GcmRegisterService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field a:Lefr;

.field b:Lcom/google/android/gms/gcm/GcmProvisioning;

.field c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "GcmRegisterService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a()V
    .locals 3

    invoke-static {p0}, Leet;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/GcmProvisioning;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->b:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-static {p0}, Lefr;->a(Landroid/content/Context;)Lefr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->a:Lefr;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->b:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->a:Lefr;

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmService;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lefr;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/gcm/GcmRegisterService;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/gcm/GcmRegisterService;->a()V

    :cond_0
    const-string v0, "com.google.android.c2dm.intent.REGISTER"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->a:Lefr;

    invoke-virtual {v0, p1}, Lefr;->a(Landroid/content/Intent;)Landroid/content/Intent;

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmRegisterService;->a:Lefr;

    invoke-virtual {v0, p1}, Lefr;->b(Landroid/content/Intent;)Landroid/content/Intent;

    goto :goto_0
.end method
