.class public Lcom/google/android/gms/gcm/GcmConnectionTestReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const-wide/16 v0, 0x3e8

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/gcm/GcmService;->a(Landroid/content/Context;J)Lcom/google/android/gms/gcm/GcmService;

    move-result-object v0

    const-string v1, "cmd"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "connect"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    invoke-virtual {v1}, Leer;->j()V

    :cond_0
    :goto_0
    iget-object v0, v0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    const-string v1, "d2cFallback"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Leeu;->h:Z

    const-string v0, "messenger"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    if-eqz v0, :cond_1

    sput-object v0, Leer;->b:Landroid/os/Messenger;

    :cond_1
    return-void

    :cond_2
    const-string v2, "disconnect"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, v0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Leer;->b(I)V

    goto :goto_0

    :cond_3
    const-string v2, "idle"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    iget-object v1, v1, Leeu;->f:Lega;

    const-wide/16 v2, 0x1f4

    iput-wide v2, v1, Lega;->g:J

    iget-object v1, v0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    iget-object v1, v1, Leeu;->f:Lega;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lega;->e:Z

    iget-object v1, v0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    iget-object v1, v1, Leeu;->f:Lega;

    invoke-virtual {v1}, Lega;->d()V

    goto :goto_0
.end method
