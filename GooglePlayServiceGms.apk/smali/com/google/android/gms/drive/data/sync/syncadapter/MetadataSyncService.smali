.class public final Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lbwc;

.field private b:Lbtd;

.field private final c:Lbvw;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lbvw;

    invoke-direct {v0, p0}, Lbvw;-><init>(Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->c:Lbvw;

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "MetadataSyncService"

    invoke-static {v0, p1, p0}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 13

    const/4 v6, 0x0

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    invoke-virtual {v0}, Lbtd;->a()V

    :try_start_0
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Landroid/content/SyncStats;->numEntries:J

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a:Lbwc;

    iget-object v1, v0, Lbwc;->a:Lcfz;

    invoke-interface {v1, p1}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v2

    iget-object v1, v0, Lbwc;->a:Lcfz;

    invoke-interface {v1, p1}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v5

    invoke-virtual {v5}, Lcfe;->g()Ljava/util/Date;

    move-result-object v3

    sget-object v1, Lbqs;->E:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v3, :cond_3

    move v1, v7

    :goto_0
    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lbwc;->a(ZLcfc;Landroid/content/SyncResult;ILcfe;)Z

    iget-object v1, v0, Lbwc;->d:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    sget-object v1, Lcoo;->a:Lcoo;

    invoke-virtual {v1}, Lcoo;->a()J

    move-result-wide v3

    sget-object v1, Lbqs;->a:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v1, v0, Lbwc;->a:Lcfz;

    invoke-interface {v1, p1}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v1

    invoke-virtual {v1}, Lcfe;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbwc;->a:Lcfz;

    invoke-interface {v1, p1}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v1

    invoke-virtual {v1}, Lcfe;->d()J

    move-result-wide v10

    sub-long/2addr v3, v10

    cmp-long v1, v3, v8

    if-lez v1, :cond_9

    :cond_0
    move v1, v7

    :goto_1
    iget-object v3, v0, Lbwc;->c:Lcby;

    invoke-interface {v3}, Lcby;->f()Lcbz;

    move-result-object v3

    iget-object v4, v0, Lbwc;->b:Lcos;

    invoke-interface {v4}, Lcos;->c()Lcot;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcbz;->a(Lcot;)Z

    move-result v3

    if-nez v2, :cond_1

    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    :cond_1
    sget-object v1, Lcoo;->a:Lcoo;

    invoke-virtual {v1}, Lcoo;->a()J

    move-result-wide v1

    iget-object v3, v0, Lbwc;->a:Lcfz;

    invoke-interface {v3}, Lcfz;->c()V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbue; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljdw; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljdu; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, v0, Lbwc;->a:Lcfz;

    invoke-interface {v3, p1}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcfe;->a(J)V

    invoke-virtual {v3}, Lcfe;->k()V

    iget-object v1, v0, Lbwc;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, v0, Lbwc;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    const-string v1, "sync"

    const-string v2, "entriesChanged"

    const/4 v3, 0x0

    iget-object v4, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v4, Landroid/content/SyncStats;->numEntries:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lbue; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljdw; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljdu; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    invoke-virtual {v0}, Lbtd;->b()V

    return-void

    :cond_3
    :try_start_3
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const-wide v10, 0x7fffffffffffffffL

    cmp-long v1, v8, v10

    if-eqz v1, :cond_8

    iget-object v1, v0, Lbwc;->a:Lcfz;

    invoke-static {v1, v2}, Lbwx;->a(Lcfz;Lcfc;)Lbvj;

    move-result-object v8

    iget-object v1, v8, Lbvj;->c:Ljava/lang/Object;

    check-cast v1, Lcgb;

    iget-object v1, v1, Lcgb;->a:Lbuw;

    iget-wide v9, v1, Lbuw;->b:J

    int-to-long v11, v4

    cmp-long v1, v9, v11

    if-gez v1, :cond_4

    iget-object v1, v8, Lbvj;->c:Ljava/lang/Object;

    check-cast v1, Lcgb;

    iget-object v1, v1, Lcgb;->a:Lbuw;

    invoke-virtual {v1}, Lbuw;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_4
    move v3, v7

    :goto_3
    iget-object v1, v8, Lbvj;->d:Ljava/lang/Object;

    check-cast v1, Lcgb;

    iget-object v1, v1, Lcgb;->a:Lbuw;

    iget-wide v9, v1, Lbuw;->b:J

    int-to-long v11, v4

    cmp-long v1, v9, v11

    if-gez v1, :cond_5

    iget-object v1, v8, Lbvj;->d:Ljava/lang/Object;

    check-cast v1, Lcgb;

    iget-object v1, v1, Lcgb;->a:Lbuw;

    invoke-virtual {v1}, Lbuw;->c()Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_5
    move v1, v7

    :goto_4
    if-eqz v3, :cond_8

    if-eqz v1, :cond_8

    move v1, v7

    goto/16 :goto_0

    :cond_6
    move v3, v6

    goto :goto_3

    :cond_7
    move v1, v6

    goto :goto_4

    :cond_8
    move v1, v6

    goto/16 :goto_0

    :cond_9
    move v1, v6

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    iget-object v0, v0, Lbwc;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    throw v1
    :try_end_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lbue; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljdw; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljdu; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "Cannot obtain required authentication."

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "AuthenicationCredentialException"

    invoke-virtual {v0, v1, v2, v3}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    invoke-virtual {v1}, Lbtd;->b()V

    throw v0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Lbue;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "SyncException"

    invoke-virtual {v0, v1, v2, v3}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_2
    move-exception v0

    const-string v1, "The sync was interrupted."

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "InterruptedException"

    invoke-virtual {v0, v1, v2, v3}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_3
    move-exception v0

    const-string v1, "There was a network error."

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "HttpException"

    invoke-virtual {v0, v1, v2, v3}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_4
    move-exception v0

    const-string v1, "There was a network error."

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "IOException"

    invoke-virtual {v0, v1, v2, v3}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_5
    move-exception v0

    const-string v1, "There was an error authenticating."

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "AuthenticationException"

    invoke-virtual {v0, v1, v2, v3}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_2
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->c:Lbvw;

    return-object v0
.end method

.method public final onCreate()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    new-instance v1, Lbwc;

    invoke-direct {v1, v0}, Lbwc;-><init>(Lcoy;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a:Lbwc;

    invoke-virtual {v0}, Lcoy;->m()Lbtd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->b:Lbtd;

    return-void
.end method
