.class public Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;
.super Ljp;
.source "SourceFile"


# instance fields
.field private o:Landroid/view/View;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/Button;

.field private u:Landroid/widget/Button;

.field private v:Lbxq;

.field private w:Lbxj;

.field private x:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Lbxj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;Lbxj;)Lbxj;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;Lbxq;)Lbxq;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->v:Lbxq;

    return-object p1
.end method

.method public static synthetic b(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Lbxq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->v:Lbxq;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->t:Landroid/widget/Button;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->x:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->o:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->u:Landroid/widget/Button;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->p:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->s:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040051    # com.google.android.gms.R.layout.drive_manage_space_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->setContentView(I)V

    if-eqz p1, :cond_0

    const-string v0, "clearing_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->x:Z

    :cond_0
    const v0, 0x7f0a0101    # com.google.android.gms.R.id.drive_progress_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->o:Landroid/view/View;

    const v0, 0x7f0a0107    # com.google.android.gms.R.id.drive_metadata_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->p:Landroid/widget/TextView;

    const v0, 0x7f0a0109    # com.google.android.gms.R.id.drive_cached_contents_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->q:Landroid/widget/TextView;

    const v0, 0x7f0a010b    # com.google.android.gms.R.id.drive_pinned_items_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->r:Landroid/widget/TextView;

    const v0, 0x7f0a0105    # com.google.android.gms.R.id.drive_total_size_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->s:Landroid/widget/TextView;

    const v0, 0x7f0a010c    # com.google.android.gms.R.id.drive_reclaim_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->t:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->t:Landroid/widget/Button;

    new-instance v1, Lbxh;

    invoke-direct {v1, p0}, Lbxh;-><init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a010d    # com.google.android.gms.R.id.drive_unpin_all_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->u:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->u:Landroid/widget/Button;

    new-instance v1, Lbxi;

    invoke-direct {v1, p0}, Lbxi;-><init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Ljp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0    # android.R.id.home
    .end packed-switch
.end method

.method protected onPause()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    invoke-virtual {v0}, Lbxj;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    invoke-virtual {v0}, Lbxj;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->x:Z

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    invoke-virtual {v0, v2}, Lbxj;->cancel(Z)Z

    iput-object v3, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->v:Lbxq;

    invoke-virtual {v0, v2}, Lbxq;->cancel(Z)Z

    iput-object v3, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->v:Lbxq;

    invoke-super {p0}, Ljp;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Ljp;->onResume()V

    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->x:Z

    if-eqz v0, :cond_0

    new-instance v0, Lbxj;

    invoke-direct {v0, p0, v1}, Lbxj;-><init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->w:Lbxj;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbxj;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lbxq;

    invoke-direct {v0, p0, v1}, Lbxq;-><init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->v:Lbxq;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->v:Lbxq;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbxq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "clearing_data"

    iget-boolean v1, p0, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
