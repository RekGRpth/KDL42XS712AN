.class public Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;
.super Lbxd;
.source "SourceFile"


# instance fields
.field private p:Lbyg;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lbxd;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbsp;)Lbyf;
    .locals 1

    new-instance v0, Lbyf;

    invoke-direct {v0, p0, p1}, Lbyf;-><init>(Landroid/content/Context;Lbsp;)V

    return-object v0
.end method

.method private g()V
    .locals 10

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcbl;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x10102

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    const v5, 0x7f0d0008    # com.google.android.gms.R.dimen.drive_floating_window_default_margin

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const v6, 0x7f0d0007    # com.google.android.gms.R.dimen.drive_file_picker_floating_max_width

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const-wide v6, 0x3fb999999999999aL    # 0.1

    int-to-double v8, v4

    mul-double/2addr v6, v8

    double-to-int v6, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    sub-int/2addr v4, v6

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    mul-int/lit8 v4, v5, 0x2

    sub-int/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->alpha:F

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    invoke-virtual {v1, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->p:Lbyg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->p:Lbyg;

    invoke-virtual {v0}, Lbyg;->a()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lbxd;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->g()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lbxd;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lo;->b:Lw;

    const-string v0, "OpenFileActivityDelegate"

    invoke-virtual {v2, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lbyg;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->p:Lbyg;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->p:Lbyg;

    if-nez v0, :cond_1

    new-instance v3, Lbyg;

    invoke-direct {v3}, Lbyg;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_0
    invoke-virtual {v3, v0}, Lbyg;->g(Landroid/os/Bundle;)V

    iput-object v3, p0, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->p:Lbyg;

    invoke-virtual {v2}, Lu;->a()Lag;

    move-result-object v0

    const v2, 0x1020002    # android.R.id.content

    iget-object v3, p0, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->p:Lbyg;

    const-string v4, "OpenFileActivityDelegate"

    invoke-virtual {v0, v2, v3, v4}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->p:Lbyg;

    invoke-virtual {v0}, Lbyg;->r()V

    const-string v0, "dialogTitle"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const v0, 0x7f0b0054    # com.google.android.gms.R.string.drive_pick_entry_dialog_title_pick_an_item

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->setTitle(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->setResult(I)V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->g()V

    return-void
.end method
