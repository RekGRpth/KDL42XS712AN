.class public Lcom/google/android/gms/drive/data/ui/create/CreateFileActivityDelegate;
.super Lbxe;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lbxe;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbsp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;ILcom/google/android/gms/drive/DriveId;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    const-string v0, "app"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/google/android/gms/drive/data/ui/create/CreateFileActivityDelegate;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "accountName"

    iget-object v2, p1, Lbsp;->a:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "callerIdentity"

    iget-object v2, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "metadata"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "requestId"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "selectedCollectionDriveId"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lbxe;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "CreateDocumentActivity"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lbxs;

    if-nez v0, :cond_3

    new-instance v0, Lbxs;

    invoke-direct {v0}, Lbxs;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/create/CreateFileActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "accountName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/gms/drive/data/ui/create/CreateFileActivityDelegate;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/create/CreateFileActivityDelegate;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0, v1}, Lbxs;->g(Landroid/os/Bundle;)V

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "CreateDocumentActivity"

    invoke-virtual {v0, v1, v2}, Lbxs;->a(Lu;Ljava/lang/String;)V

    :cond_3
    if-nez p1, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/gms/drive/data/ui/create/CreateFileActivityDelegate;->setResult(I)V

    goto :goto_0
.end method
