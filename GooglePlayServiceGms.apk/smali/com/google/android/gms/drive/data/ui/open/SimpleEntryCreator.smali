.class public Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbyq;

    invoke-direct {v0}, Lbyq;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->a:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->c:I

    iput p3, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->b:I

    iput p4, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->d:I

    iput p5, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->e:I

    iput p6, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->f:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->a:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
