.class public Lcom/google/android/gms/drive/api/DriveAsyncService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# instance fields
.field private b:Lbbn;

.field private c:Lbri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "DriveAsyncService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbqz;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    const-string v0, "com.google.android.gms.drive.EXECUTE"

    invoke-static {v0}, Lbox;->f(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private static a(Lbqz;Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    instance-of v0, p0, Lbro;

    if-eqz v0, :cond_0

    check-cast p0, Lbro;

    invoke-virtual {p0}, Lbro;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lchq;

    invoke-interface {v0, p1}, Lchq;->a(Lcom/google/android/gms/common/api/Status;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lbri;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lbbn;

    if-nez v0, :cond_0

    new-instance v0, Lbbn;

    invoke-direct {v0}, Lbbn;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lbbn;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/api/AccountService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lbbn;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/drive/api/DriveAsyncService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lbbn;

    invoke-virtual {v0}, Lbbn;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lbrj;->a(Landroid/os/IBinder;)Lbri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lbri;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lbri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lbbn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lbbn;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/api/DriveAsyncService;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lbbn;

    iput-object v1, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lbri;

    :cond_0
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5

    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqz;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v0, p0}, Lbqz;->a(Lcom/google/android/gms/drive/api/DriveAsyncService;)V
    :try_end_1
    .catch Lbrm; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lbqx; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "DriveAsyncService"

    invoke-virtual {v1}, Lbrm;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    invoke-virtual {v1}, Lbrm;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Lbqz;Lcom/google/android/gms/common/api/Status;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "DriveAsyncService"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v1

    :try_start_3
    const-string v2, "DriveAsyncService"

    invoke-virtual {v1}, Lbqx;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    invoke-virtual {v1}, Lbqx;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Lbqz;Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :catch_3
    move-exception v1

    const-string v2, "DriveAsyncService"

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Lbqz;Lcom/google/android/gms/common/api/Status;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method
