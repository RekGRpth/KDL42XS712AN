.class public Lcom/google/android/gms/drive/api/ApiService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field public a:Lbrb;

.field public b:Lbrf;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.gms.drive.ApiService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbqy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbqy;-><init>(Lcom/google/android/gms/drive/api/ApiService;B)V

    invoke-virtual {v0}, Lbqy;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    invoke-static {p0}, Lbtb;->a(Landroid/content/Context;)V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhhz;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    new-instance v1, Lbrb;

    invoke-direct {v1}, Lbrb;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/drive/api/ApiService;->a:Lbrb;

    new-instance v1, Lbrf;

    invoke-virtual {v0}, Lcoy;->p()Lbra;

    move-result-object v2

    invoke-virtual {v0}, Lcoy;->g()Lbrl;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lbrf;-><init>(Lbra;Lbrl;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/api/ApiService;->b:Lbrf;

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method
