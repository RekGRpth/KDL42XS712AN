.class public Lcom/google/android/gms/drive/query/internal/ComparisonFilter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcnj;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcnp;


# instance fields
.field public final a:Lcom/google/android/gms/drive/query/internal/Operator;

.field public final b:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field public final c:I

.field final d:Lcje;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcnp;

    invoke-direct {v0}, Lcnp;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->CREATOR:Lcnp;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/drive/query/internal/Operator;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->c:I

    iput-object p2, p0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->a:Lcom/google/android/gms/drive/query/internal/Operator;

    iput-object p3, p0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->b:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {p3}, Lcns;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcje;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->d:Lcje;

    return-void
.end method


# virtual methods
.method public final a(Lcnt;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->a:Lcom/google/android/gms/drive/query/internal/Operator;

    iget-object v1, p0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->d:Lcje;

    iget-object v2, p0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->b:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p0, Lcom/google/android/gms/drive/query/internal/ComparisonFilter;->d:Lcje;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcnt;->a(Lcom/google/android/gms/drive/query/internal/Operator;Lcje;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcnp;->a(Lcom/google/android/gms/drive/query/internal/ComparisonFilter;Landroid/os/Parcel;I)V

    return-void
.end method
