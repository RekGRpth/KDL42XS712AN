.class public Lcom/google/android/gms/drive/query/internal/InFilter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcnj;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcnu;


# instance fields
.field public final a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field public final b:I

.field private final c:Lcja;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcnu;

    invoke-direct {v0}, Lcnu;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/query/internal/InFilter;->CREATOR:Lcnu;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/drive/query/internal/InFilter;->b:I

    iput-object p2, p0, Lcom/google/android/gms/drive/query/internal/InFilter;->a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {p2}, Lcns;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcje;

    move-result-object v0

    check-cast v0, Lcja;

    iput-object v0, p0, Lcom/google/android/gms/drive/query/internal/InFilter;->c:Lcja;

    return-void
.end method


# virtual methods
.method public final a(Lcnt;)Ljava/lang/Object;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/drive/query/internal/InFilter;->c:Lcja;

    iget-object v0, p0, Lcom/google/android/gms/drive/query/internal/InFilter;->a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v2, p0, Lcom/google/android/gms/drive/query/internal/InFilter;->c:Lcja;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcnt;->a(Lcja;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcnu;->a(Lcom/google/android/gms/drive/query/internal/InFilter;Landroid/os/Parcel;I)V

    return-void
.end method
