.class public final Lcom/google/android/gms/appstate/service/AppStateIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "AppStateIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7

    sget-object v6, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lamb;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lamb;-><init>(Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lamc;

    invoke-direct {v1, p1, p2}, Lamc;-><init>(Lcom/google/android/gms/common/server/ClientContext;Laku;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lalx;

    invoke-direct {v1, p1, p2, p3}, Lalx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lalw;

    invoke-direct {v1, p1, p2, p3, p4}, Lalw;-><init>(Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;ILjava/lang/String;[B)V
    .locals 8

    sget-object v7, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lalz;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lalz;-><init>(Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;ILjava/lang/String;[B)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I[B)V
    .locals 7

    sget-object v6, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lama;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lama;-><init>(Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I[B)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lalv;

    invoke-direct {v1, p1}, Lalv;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    invoke-virtual {p1, p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    const-string v0, "com.google.android.gms.appstate.service.INTENT"

    invoke-static {v0}, Lbox;->f(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Laly;

    invoke-direct {v1, p1, p2, p3, p4}, Laly;-><init>(Lcom/google/android/gms/common/server/ClientContext;Laku;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lalr;)V

    return-void
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalr;

    if-nez v0, :cond_0

    const-string v0, "AppStateIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "operation missing"

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lakk;->a(Landroid/content/Context;)Lakk;

    move-result-object v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    invoke-interface {v0, p0, v1}, Lalr;->a(Landroid/content/Context;Lakk;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lakk;->a()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "AppStateIntentService"

    const-string v3, "Auth error executing an operation: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lakk;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lakk;->a()V

    throw v0
.end method
