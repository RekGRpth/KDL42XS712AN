.class public final Lcom/google/android/gms/appstate/service/AppStateSyncService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lals;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/service/AppStateSyncService;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/appstate/service/AppStateSyncService;->b:Lals;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateSyncService;->b:Lals;

    invoke-virtual {v0}, Lals;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/AppStateSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbfy;->a(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/appstate/service/AppStateSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateSyncService;->b:Lals;

    if-nez v0, :cond_0

    new-instance v0, Lals;

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/AppStateSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lals;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/appstate/service/AppStateSyncService;->b:Lals;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
