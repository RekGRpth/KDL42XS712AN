.class public Lcom/google/android/location/reporting/DetectedActivityStore;
.super Lihn;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lihq;

.field private final d:Liuo;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE TABLE ActivityDetection (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, " \'%s\' STRING,"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "Account"

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, " \'%s\' BLOB,"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "ActivityType"

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, " \'%s\' INTEGER"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "Time"

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/reporting/DetectedActivityStore;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lihq;)V
    .locals 1

    new-instance v0, Liup;

    invoke-direct {v0}, Liup;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/reporting/DetectedActivityStore;-><init>(Landroid/content/Context;Lihq;Liuo;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lihq;Liuo;)V
    .locals 9

    const-string v2, "ActivityDetection"

    const-string v3, "id"

    const-string v4, "Account"

    const-string v5, "Time"

    sget-wide v6, Lcom/google/android/location/reporting/DetectedActivityStore;->a:J

    sget-object v8, Lcom/google/android/location/reporting/DetectedActivityStore;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lihn;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/location/reporting/DetectedActivityStore;->c:Lihq;

    iput-object p3, p0, Lcom/google/android/location/reporting/DetectedActivityStore;->d:Liuo;

    return-void
.end method

.method private a([B)[B
    .locals 3

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/reporting/DetectedActivityStore;->c:Lihq;

    invoke-virtual {v0, p1}, Lihq;->a([B)[B
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "GCoreUlr"

    const-string v2, "Error encrypting activity type"

    invoke-static {v1, v2, v0}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v1, Liho;

    invoke-direct {v1, v0}, Liho;-><init>(Ljava/lang/Exception;)V

    throw v1

    :cond_0
    new-instance v0, Liho;

    const-string v1, "Can\'t encrypt null data"

    invoke-direct {v0, v1}, Liho;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Landroid/database/Cursor;)Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 3

    const-string v0, "ActivityType"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ActivityType"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/reporting/DetectedActivityStore;->b([B)[B

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Liho;

    const-string v1, "Error decrypting activity detection result"

    invoke-direct {v0, v1}, Liho;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    new-instance v1, Lihh;

    invoke-direct {v1}, Lihh;-><init>()V

    array-length v2, v0

    invoke-virtual {v1, v0, v2}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Lihh;

    invoke-static {v0}, Lhxz;->a(Lihh;)Lcom/google/android/gms/location/ActivityRecognitionResult;
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Liho;

    invoke-direct {v1, v0}, Liho;-><init>(Ljava/lang/Exception;)V

    throw v1

    :cond_1
    new-instance v0, Liho;

    const-string v1, "Null activity detection column"

    invoke-direct {v0, v1}, Liho;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b([B)[B
    .locals 3

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/reporting/DetectedActivityStore;->c:Lihq;

    invoke-virtual {v0, p1}, Lihq;->b([B)[B
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "GCoreUlr"

    const-string v2, "Error decrypting activity type"

    invoke-static {v1, v2, v0}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v1, Liho;

    invoke-direct {v1, v0}, Liho;-><init>(Ljava/lang/Exception;)V

    throw v1

    :cond_0
    new-instance v0, Liho;

    const-string v1, "Can\'t decrypt empty array"

    invoke-direct {v0, v1}, Liho;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Landroid/content/ContentValues;
    .locals 4

    check-cast p1, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "Time"

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {p1}, Lhxz;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)Lihh;

    move-result-object v1

    invoke-virtual {v1}, Lihh;->d()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/location/reporting/DetectedActivityStore;->a([B)[B

    move-result-object v1

    const-string v2, "ActivityType"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    return-object v0
.end method

.method public final synthetic a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/location/reporting/DetectedActivityStore;->b(Landroid/database/Cursor;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    return-object v0
.end method
