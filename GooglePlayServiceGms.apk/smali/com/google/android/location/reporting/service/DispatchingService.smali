.class public Lcom/google/android/location/reporting/service/DispatchingService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Z


# instance fields
.field private b:Lijt;

.field private c:Lihy;

.field private d:Lijb;

.field private e:Landroid/os/Looper;

.field private f:Landroid/os/Handler;

.field private g:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/location/reporting/service/DispatchingService;)Lihy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->c:Lihy;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "com.google.android.location.reporting.ACTION_UPDATE_ACTIVE_STATE"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Likh;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/location/reporting/service/DispatchingService;->a:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/location/reporting/service/DispatchingService;)Lijb;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->d:Lijb;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    const-string v0, "com.google.android.location.reporting.ACTION_APPLY_UPLOAD_REQUESTS"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Likh;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    return-void
.end method

.method public static synthetic c(Lcom/google/android/location/reporting/service/DispatchingService;)Lijt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->b:Lijt;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/location/reporting/service/DispatchingService;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "DispatchingService dumping...."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "DispatchingService ULR dump...."

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->d:Lijb;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-static {p0}, Lijd;->a(Landroid/content/Context;)Lijd;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->b:Lijt;

    iget-object v2, p0, Lcom/google/android/location/reporting/service/DispatchingService;->c:Lihy;

    invoke-static {p2, v1, v2, v0}, Likh;->a(Ljava/io/PrintWriter;Lijt;Lihy;Lijd;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {p0}, Likh;->a(Landroid/content/Context;)V

    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "DispatchingService.onCreate()"

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/location/reporting/service/DispatchingService;->a:Z

    new-instance v0, Lijt;

    invoke-direct {v0, p0}, Lijt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->b:Lijt;

    invoke-static {p0}, Lihy;->a(Landroid/content/Context;)Lihy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->c:Lihy;

    new-instance v0, Lijb;

    invoke-direct {v0}, Lijb;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->d:Lijb;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "UlrDispatchingService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->e:Landroid/os/Looper;

    new-instance v0, Liiu;

    iget-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->e:Landroid/os/Looper;

    invoke-direct {v0, p0, p0, v1}, Liiu;-><init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->f:Landroid/os/Handler;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_ENABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_DISABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Liit;

    invoke-direct {v1}, Liit;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->g:Landroid/content/BroadcastReceiver;

    invoke-static {p0}, Lce;->a(Landroid/content/Context;)Lce;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/reporting/service/DispatchingService;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lce;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/reporting/service/DispatchingService;->a:Z

    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "DispatchingService.onDestroy()"

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0}, Lce;->a(Landroid/content/Context;)Lce;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lce;->a(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->e:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2

    const-string v0, "GCoreUlr"

    const-string v1, "We don\'t support Froyo, this shouldn\'t be called"

    invoke-static {v0, v1}, Lijy;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    const/4 v2, 0x1

    if-nez p1, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->f:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p3, v0, Landroid/os/Message;->arg1:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->f:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
