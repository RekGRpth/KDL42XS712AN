.class public Lcom/google/android/location/reporting/service/PreferenceService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private final a:Lijg;

.field private b:Lijt;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lijg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lijg;-><init>(Lcom/google/android/location/reporting/service/PreferenceService;B)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/PreferenceService;->a:Lijg;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/location/reporting/service/PreferenceService;)Lijt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/PreferenceService;->b:Lijt;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/reporting/service/PreferenceService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x81

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PreferenceService.bindTo() bound: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "PreferenceService dumping...."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "PreferenceService ULR dump...."

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/location/reporting/service/DispatchingService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PreferenceService deferring to DispatchingService for dump"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v1, "GCoreUlr"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lihy;->a(Landroid/content/Context;)Lihy;

    move-result-object v0

    invoke-static {p0}, Lijd;->a(Landroid/content/Context;)Lijd;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/reporting/service/PreferenceService;->b:Lijt;

    invoke-static {p2, v2, v0, v1}, Likh;->a(Ljava/io/PrintWriter;Lijt;Lihy;Lijd;)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "GCoreUlr"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "PreferenceService.onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/service/PreferenceService;->a:Lijg;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {p0}, Likh;->a(Landroid/content/Context;)V

    new-instance v0, Lijt;

    invoke-direct {v0, p0}, Lijt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/PreferenceService;->b:Lijt;

    return-void
.end method
