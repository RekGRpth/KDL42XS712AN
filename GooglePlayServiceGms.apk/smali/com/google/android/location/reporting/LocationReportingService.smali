.class public Lcom/google/android/location/reporting/LocationReportingService;
.super Likg;
.source "SourceFile"


# instance fields
.field private a:Landroid/net/ConnectivityManager;

.field private b:Lcom/google/android/location/reporting/StateReporter;

.field private c:Liid;

.field private d:Lihy;

.field private e:Lcom/google/android/location/reporting/LocationRecordStore;

.field private f:Lcom/google/android/location/reporting/DetectedActivityStore;

.field private g:Lcom/google/android/location/reporting/ApiMetadataStore;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "GCoreUlr-LocationReportingService"

    invoke-direct {p0, v0}, Likg;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lihn;Landroid/accounts/Account;)Ljava/util/ArrayList;
    .locals 5

    :try_start_0
    invoke-virtual {p0, p1}, Lihn;->retrieveEntities(Landroid/accounts/Account;)Ljava/util/ArrayList;
    :try_end_0
    .catch Liho; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DB error retrieving entities for account "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    invoke-static {v0}, Likf;->e(Ljava/lang/Exception;)V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private static a(Landroid/accounts/Account;Ljava/lang/Exception;)V
    .locals 3

    invoke-static {p1}, Likf;->c(Ljava/lang/Exception;)V

    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Batch Location Update failed for account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static a(Lihn;Landroid/accounts/Account;J)V
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lihn;->a(Landroid/accounts/Account;J)V
    :try_end_0
    .catch Liho; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DB error deleting entities for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    invoke-static {v0}, Likf;->d(Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 23

    const-string v2, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GCoreUlr"

    const-string v3, "LocationReportingService has started."

    invoke-static {v2, v3}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->b:Lcom/google/android/location/reporting/StateReporter;

    invoke-interface {v2}, Lcom/google/android/location/reporting/StateReporter;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/location/reporting/service/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingService;->e:Lcom/google/android/location/reporting/LocationRecordStore;

    invoke-static {v3, v2}, Likh;->a(Lihn;Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingService;->f:Lcom/google/android/location/reporting/DetectedActivityStore;

    invoke-static {v3, v2}, Likh;->a(Lihn;Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingService;->g:Lcom/google/android/location/reporting/ApiMetadataStore;

    invoke-static {v3, v2}, Likh;->a(Lihn;Ljava/util/List;)V

    const-string v3, "attempt"

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    int-to-long v5, v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v3, v5}, Likf;->a(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingService;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "GCoreUlr"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "GCoreUlr"

    const-string v4, "Batch Location Update aborted because backgroundDataEnabled false"

    invoke-static {v3, v4}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v3, "bg_data_disabled"

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v3, v2}, Likf;->a(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static/range {p0 .. p0}, Lbpu;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "GCoreUlr"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "GCoreUlr"

    const-string v4, "Batch Location Update aborted because not connected"

    invoke-static {v3, v4}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v3, "no_connection"

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v3, v2}, Likf;->a(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->d:Lihy;

    invoke-virtual {v2}, Lihy;->c()Ljava/util/Collection;

    move-result-object v13

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {v4}, Lcom/google/android/location/reporting/service/ReportingConfig;->getActiveAccountConfigs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move v9, v2

    move v10, v3

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v15

    :try_start_0
    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-static {v3}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/AccountConfig;->k()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "GCoreUlr"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Settings dirty, skipping upload for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string v2, "LocationReportingService"

    invoke-static {v3, v2}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    const-string v2, "dirty"

    const-wide/16 v3, 0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Likf;->a(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x0

    :goto_2
    or-int/2addr v2, v10

    move v10, v2

    goto :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->e:Lcom/google/android/location/reporting/LocationRecordStore;

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/LocationReportingService;->a(Lihn;Landroid/accounts/Account;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->f:Lcom/google/android/location/reporting/DetectedActivityStore;

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/LocationReportingService;->a(Lihn;Landroid/accounts/Account;)Ljava/util/ArrayList;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->g:Lcom/google/android/location/reporting/ApiMetadataStore;

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/LocationReportingService;->a(Lihn;Landroid/accounts/Account;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Skipping account with no locations: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const-string v2, "empty"

    const-wide/16 v3, 0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Likf;->a(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x0

    goto :goto_2

    :cond_9
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liif;

    iget-wide v0, v2, Liif;->f:J

    move-wide/from16 v17, v0

    const-string v2, "GCoreUlr"

    const/4 v8, 0x3

    invoke-static {v2, v8}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "GCoreUlr"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v19, "LocationReportingService sending %d locations, %d activities, and %d metadatas for account %s; requests: %s"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    aput-object v16, v20, v21

    const/16 v21, 0x4

    aput-object v13, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v8, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    const-string v2, "GCoreUlr"

    const/4 v8, 0x2

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "GCoreUlr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v19, "Locations: "

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Likc;->wrap(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "GCoreUlr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v19, "Activities: "

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "GCoreUlr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v19, "Metadatas: "

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/AccountConfig;->h()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->c:Liid;

    invoke-interface/range {v2 .. v8}, Liid;->a(Landroid/accounts/Account;Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;J)Lihk;

    move-result-object v2

    if-eqz v2, :cond_d

    const-string v16, "GCoreUlr"

    const/16 v17, 0x4

    invoke-static/range {v16 .. v17}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v16

    if-eqz v16, :cond_c

    const-string v16, "GCoreUlr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Server reports setting change occurred after "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", requesting sync: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-static {v0, v7}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const-string v7, "LocationReportingService"

    invoke-static {v3, v7}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-static {v2, v4, v5, v6}, Likf;->a(Lihk;Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;)V

    const/4 v2, 0x1

    goto/16 :goto_2

    :cond_d
    const-string v2, "GCoreUlr"

    const/4 v7, 0x4

    invoke-static {v2, v7}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "GCoreUlr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Batch Location Update succeeded for account "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    invoke-static {v4, v5, v6}, Likf;->a(Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->e:Lcom/google/android/location/reporting/LocationRecordStore;

    move-wide/from16 v0, v17

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/location/reporting/LocationReportingService;->a(Lihn;Landroid/accounts/Account;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->f:Lcom/google/android/location/reporting/DetectedActivityStore;

    move-wide/from16 v0, v17

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/location/reporting/LocationReportingService;->a(Lihn;Landroid/accounts/Account;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->g:Lcom/google/android/location/reporting/ApiMetadataStore;

    move-wide/from16 v0, v17

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/location/reporting/LocationReportingService;->a(Lihn;Landroid/accounts/Account;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v2, 0x1

    goto/16 :goto_2

    :catch_0
    move-exception v2

    move-object v3, v2

    const/4 v2, 0x1

    invoke-static {v15, v3}, Lcom/google/android/location/reporting/LocationReportingService;->a(Landroid/accounts/Account;Ljava/lang/Exception;)V

    move v9, v2

    goto/16 :goto_1

    :catch_1
    move-exception v2

    move-object v3, v2

    const/4 v2, 0x1

    invoke-static {v15, v3}, Lcom/google/android/location/reporting/LocationReportingService;->a(Landroid/accounts/Account;Ljava/lang/Exception;)V

    move v9, v2

    goto/16 :goto_1

    :catch_2
    move-exception v2

    move-object v3, v2

    const/4 v2, 0x1

    invoke-static {v3}, Likf;->a(Ljava/lang/RuntimeException;)V

    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Batch Location Update failed for account "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v15}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v9, v2

    goto/16 :goto_1

    :cond_f
    if-nez v10, :cond_10

    if-eqz v9, :cond_2

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingService;->d:Lihy;

    invoke-virtual {v2, v11, v12}, Lihy;->c(J)Z

    goto/16 :goto_0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Likg;->onCreate()V

    invoke-static {p0}, Likh;->a(Landroid/content/Context;)V

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/location/reporting/LocationReportingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/location/reporting/LocationReportingService;->a:Landroid/net/ConnectivityManager;

    new-instance v0, Lijt;

    invoke-direct {v0, p0}, Lijt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/LocationReportingService;->b:Lcom/google/android/location/reporting/StateReporter;

    new-instance v0, Liib;

    invoke-direct {v0, p0}, Liib;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/LocationReportingService;->c:Liid;

    invoke-static {p0}, Lihy;->a(Landroid/content/Context;)Lihy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/LocationReportingService;->d:Lihy;

    new-instance v0, Lihq;

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingService;->d:Lihy;

    invoke-direct {v0, v1}, Lihq;-><init>(Lihy;)V

    new-instance v1, Lcom/google/android/location/reporting/LocationRecordStore;

    invoke-virtual {p0}, Lcom/google/android/location/reporting/LocationReportingService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/location/reporting/LocationRecordStore;-><init>(Landroid/content/Context;Lihq;)V

    iput-object v1, p0, Lcom/google/android/location/reporting/LocationReportingService;->e:Lcom/google/android/location/reporting/LocationRecordStore;

    new-instance v1, Lcom/google/android/location/reporting/DetectedActivityStore;

    invoke-virtual {p0}, Lcom/google/android/location/reporting/LocationReportingService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/location/reporting/DetectedActivityStore;-><init>(Landroid/content/Context;Lihq;)V

    iput-object v1, p0, Lcom/google/android/location/reporting/LocationReportingService;->f:Lcom/google/android/location/reporting/DetectedActivityStore;

    new-instance v1, Lcom/google/android/location/reporting/ApiMetadataStore;

    invoke-virtual {p0}, Lcom/google/android/location/reporting/LocationReportingService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/location/reporting/ApiMetadataStore;-><init>(Landroid/content/Context;Lihq;)V

    iput-object v1, p0, Lcom/google/android/location/reporting/LocationReportingService;->g:Lcom/google/android/location/reporting/ApiMetadataStore;

    return-void
.end method
