.class public Lcom/google/android/location/internal/server/GoogleLocationService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field public final a:Lidp;

.field private b:Licp;

.field private final c:Landroid/os/HandlerThread;

.field private d:Lhon;

.field private e:Liam;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Liag;

    invoke-direct {v0, p0}, Liag;-><init>(Lcom/google/android/location/internal/server/GoogleLocationService;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->a:Lidp;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "GoogleLocationService"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->c:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->f:Z

    return-void
.end method

.method private static a(Landroid/content/Intent;Ljava/lang/String;I)J
    .locals 5

    const-wide/16 v3, -0x1

    invoke-virtual {p0, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    int-to-long v0, p2

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0, p1, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v3

    if-nez v2, :cond_1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    :cond_1
    cmp-long v2, v0, v3

    if-nez v2, :cond_2

    int-to-long v0, p2

    :cond_2
    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Z)V
    .locals 14

    const-string v0, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    const-string v0, "com.google.android.location.internal.EXTRA_LOCATION_LOW_POWER"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v0, "com.google.android.location.internal.EXTRA_LOCATION_REMOVE"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "com.google.android.location.internal.EXTRA_DEBUG_INFO"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v2, "com.google.android.location.internal.EXTRA_PERIOD_MILLIS"

    const/4 v3, -0x1

    invoke-static {p1, v2, v3}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Ljava/lang/String;I)J

    move-result-wide v2

    const-string v5, "com.google.android.location.internal.EXTRA_BATCH_DURATION_MILLIS"

    const/4 v7, 0x0

    invoke-static {p1, v5, v7}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Ljava/lang/String;I)J

    move-result-wide v7

    const-string v5, "com.google.android.location.internal.EXTRA_LOCATION_FORCE_NOW"

    const/4 v9, 0x1

    invoke-virtual {p1, v5, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v1, :cond_1

    if-eqz v0, :cond_6

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client canceled location update "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    invoke-virtual {v0, v1}, Liam;->a(Landroid/app/PendingIntent;)V

    :cond_1
    :goto_0
    const-string v0, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    sget-boolean v2, Licj;->c:Z

    if-eqz v2, :cond_2

    const-string v2, "GoogleLocationService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "received activity pending intent from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-boolean v2, Licj;->f:Z

    if-nez v2, :cond_3

    invoke-static {v0}, Liaf;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_3
    const-string v2, "com.google.android.location.internal.EXTRA_ACTIVITY_REMOVE"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_e

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_4

    const-string v0, "GoogleLocationService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client canceled activity detection "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    invoke-virtual {v0, v1}, Liam;->b(Landroid/app/PendingIntent;)V

    if-eqz p2, :cond_5

    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lhon;

    invoke-virtual {v0, p1}, Lhon;->b(Landroid/os/Parcelable;)V

    :cond_5
    :goto_1
    return-void

    :cond_6
    const-wide/16 v9, 0x0

    cmp-long v0, v2, v9

    if-ltz v0, :cond_1

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    if-eqz v4, :cond_8

    const-string v0, "android.permission.ACCESS_WIFI_STATE"

    invoke-virtual {v10, v0, v9}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_2
    sget-boolean v11, Licj;->c:Z

    if-eqz v11, :cond_7

    const-string v11, "GoogleLocationService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "package "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " hasWifi? "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    if-eqz v4, :cond_d

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_3
    move v4, v0

    :cond_8
    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_9

    const-string v0, "GoogleLocationService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "received pending intent from "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0, v11}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const-wide/16 v11, 0x3e8

    div-long/2addr v2, v11

    long-to-int v2, v2

    const-wide/16 v11, 0x3e8

    div-long/2addr v7, v11

    long-to-int v3, v7

    const-string v0, "com.google.android.location.internal.EXTRA_LOCATION_WORK_SOURCE"

    invoke-static {p1, v0}, Lilx;->a(Landroid/content/Intent;Ljava/lang/String;)Lilx;

    move-result-object v7

    if-eqz v7, :cond_b

    const-string v0, "android.permission.UPDATE_DEVICE_STATS"

    invoke-virtual {v10, v0, v9}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_b

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_a

    const-string v0, "GoogleLocationService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "package "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " doesn\'t have permission for WorkSource"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    const/4 v7, 0x0

    :cond_b
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    invoke-virtual/range {v0 .. v7}, Liam;->a(Landroid/app/PendingIntent;IIZZZLilx;)V

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_d
    const/4 v0, 0x0

    goto :goto_3

    :cond_e
    const-string v2, "com.google.android.location.internal.EXTRA_ACTIVITY_PERIOD_MILLIS"

    const v3, 0x2bf20

    invoke-static {p1, v2, v3}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Ljava/lang/String;I)J

    move-result-wide v6

    const-string v2, "com.google.android.location.internal.EXTRA_ACTIVITY_FORCE_DETECTION_NOW"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    sget-boolean v2, Licj;->c:Z

    if-eqz v2, :cond_f

    const-string v2, "GoogleLocationService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Adding/updating activity detection client: periodMillis: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " force: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    const-string v2, "com.google.android.location.internal.EXTRA_IS_FROM_FIRST_PARTY"

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v2, "com.google.android.location.internal.EXTRA_LOCATION_WORK_SOURCE"

    invoke-static {p1, v2}, Lilx;->a(Landroid/content/Intent;Ljava/lang/String;)Lilx;

    move-result-object v5

    if-eqz v5, :cond_11

    invoke-virtual {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v8, "android.permission.UPDATE_DEVICE_STATS"

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_11

    sget-boolean v2, Licj;->c:Z

    if-eqz v2, :cond_10

    const-string v2, "GoogleLocationService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "package "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " doesn\'t have permission for WorkSource"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    const/4 v5, 0x0

    :cond_11
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v2, v6

    invoke-virtual/range {v0 .. v5}, Liam;->a(Landroid/app/PendingIntent;IZZLilx;)V

    if-eqz p2, :cond_5

    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lhon;

    invoke-virtual {v0, p1}, Lhon;->a(Landroid/os/Parcelable;)V

    goto/16 :goto_1

    :cond_12
    sget-boolean v1, Licj;->c:Z

    if-eqz v1, :cond_5

    const-string v1, "GoogleLocationService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Rejecting request for activity detection. Application not in whitelist. Please contact lbs-team. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private declared-synchronized b()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lhon;

    invoke-virtual {v0}, Lhon;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->stopSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v0, "MM-dd HH:mm:ss.SSS"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v0, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string v6, "elapsedRealtime "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v4, v5}, Ljava/io/PrintWriter;->print(J)V

    const-string v6, " is time "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    invoke-virtual {v0, p2}, Liam;->a(Ljava/io/PrintWriter;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    invoke-virtual {v0, v1, p2}, Liam;->a(Ljava/text/Format;Ljava/io/PrintWriter;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    invoke-virtual {v0, p2}, Liam;->b(Ljava/io/PrintWriter;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Licp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Licp;

    sub-long/2addr v2, v4

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Licp;->a(Ljava/text/Format;JJLjava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized onCreate()V
    .locals 5

    const/4 v0, 0x0

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Licp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v1

    invoke-virtual {v1}, Lifs;->c()V

    invoke-static {p0}, Lbfy;->a(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lhon;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, Liah;

    invoke-direct {v4, p0}, Liah;-><init>(Lcom/google/android/location/internal/server/GoogleLocationService;)V

    invoke-direct {v1, p0, v3, v4}, Lhon;-><init>(Landroid/content/Context;Ljava/lang/Class;Lhor;)V

    iput-object v1, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lhon;

    new-instance v1, Liam;

    iget-object v3, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lhon;

    invoke-direct {v1, p0, v3, v4}, Liam;-><init>(Lcom/google/android/location/internal/server/GoogleLocationService;Landroid/os/Looper;Lhon;)V

    iput-object v1, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v1, "nlp_debug_log"

    invoke-virtual {p0, v1}, Lcom/google/android/location/internal/server/GoogleLocationService;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    const/4 v1, 0x1

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    if-eqz v1, :cond_2

    :try_start_3
    new-instance v2, Ljava/io/BufferedOutputStream;

    const-string v1, "nlp_debug_log"

    const v3, 0x8000

    invoke-virtual {p0, v1, v3}, Lcom/google/android/location/internal/server/GoogleLocationService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    :try_start_4
    new-instance v2, Licp;

    iget-object v3, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->a:Lidp;

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v4

    invoke-virtual {v4}, Lifs;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_3
    invoke-direct {v2, v3, v0, v1}, Licp;-><init>(Lidp;Lima;Ljava/io/PrintWriter;)V

    iput-object v2, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Licp;

    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    iget-object v1, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Licp;

    invoke-virtual {v0, v1}, Liam;->a(Licp;)V

    new-instance v0, Lieb;

    iget-object v1, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Licp;

    invoke-direct {v0, v1}, Lieb;-><init>(Licp;)V

    invoke-static {v0}, Lilz;->a(Lima;)V

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "GoogleLocationService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    invoke-virtual {v0}, Liam;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v1

    move v1, v2

    goto :goto_1

    :catch_1
    move-exception v1

    move v1, v2

    goto :goto_1

    :catch_2
    move-exception v1

    :try_start_5
    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_2

    const-string v1, "GoogleLocationService"

    const-string v2, "debug log file missing for output!?"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v1, v0

    goto :goto_2

    :cond_3
    new-instance v0, Lieb;

    invoke-direct {v0}, Lieb;-><init>()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method

.method public declared-synchronized onDestroy()V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Liam;

    invoke-virtual {v0}, Liam;->c()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "GoogleLocationService"

    const-string v1, "unregistering logger"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lilz;->a(Lima;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onStart(Landroid/content/Intent;I)V
    .locals 4

    monitor-enter p0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_0
    invoke-static {p1}, Lhon;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_2

    const-string v0, "GoogleLocationService"

    const-string v1, "Received SystemMemoryCache init intent."

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lhon;

    invoke-virtual {v0, p1}, Lhon;->c(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lhon;

    invoke-virtual {v0}, Lhon;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    sget-boolean v2, Licj;->c:Z

    if-eqz v2, :cond_4

    const-string v2, "GoogleLocationService"

    const-string v3, "Re-adding cached client."

    invoke-static {v2, v3}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Z)V

    goto :goto_1

    :cond_5
    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationService"

    const-string v1, "Finished initializing clients from pending intent cache."

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
