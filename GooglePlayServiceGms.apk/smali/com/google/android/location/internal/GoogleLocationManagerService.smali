.class public Lcom/google/android/location/internal/GoogleLocationManagerService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lhzy;

.field private b:Lhzx;

.field private c:Landroid/content/pm/PackageManager;

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/location/internal/GoogleLocationManagerService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->d:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Landroid/content/Context;II)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Activity detection usage requires the com.google.android.gms.permission.ACTIVITY_RECOGNITION permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private a(Lbjv;)V
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lhzx;

    invoke-virtual {v1}, Lhzx;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GoogleLocationManagerSe"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationManagerSe"

    const-string v1, "client died while brokering service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/location/internal/GoogleLocationManagerService;Lbjv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Lbjv;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/location/internal/GoogleLocationManagerService;Lbjv;Ljava/lang/String;II)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3, p4}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Landroid/content/Context;II)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "GoogleLocationManagerSe"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationManagerSe"

    const-string v2, "App was not granted the activity recognition permisison"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->c:Landroid/content/pm/PackageManager;

    invoke-static {p2, v0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GoogleLocationManagerSe"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleLocationManagerSe"

    const-string v2, "App should show the activity recognition permisison dialog."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.location.settings.ACTIVITY_RECOGNITION_PERMISSION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v0, 0x6

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v0, v2, v1}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    return-void

    :cond_3
    const-string v0, "GoogleLocationManagerSe"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GoogleLocationManagerSe"

    const-string v2, "App did not request activity recognition in its manifest"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GoogleLocationManagerSe"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GoogleLocationManagerSe"

    const-string v1, "client died while brokering service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Lbjv;)V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;II)Z
    .locals 12

    const/4 v11, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "com.google.android.gms.permission.ACTIVITY_RECOGNITION"

    invoke-virtual {p0, v2, p1, p2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "activity_recognition_permission_whitelist"

    invoke-static {}, Lhzw;->a()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_4

    aget-object v7, v5, v2

    invoke-interface {v3, v7, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-static {v7, v4}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v1, "GoogleLocationManagerSe"

    invoke-static {v1, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GoogleLocationManagerSe"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Found package in activity recognition white-list: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v8, "GoogleLocationManagerSe"

    invoke-static {v8, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "GoogleLocationManagerSe"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Found whitelisted package that doesn\'t define the permission in its manifest: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static synthetic a(Landroid/os/Bundle;)Z
    .locals 2

    const-string v0, "client_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "activity_recognition"

    const-string v1, "client_name"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z
    .locals 6

    const/4 v0, 0x0

    const/16 v1, 0x1000

    :try_start_0
    invoke-virtual {p1, p0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    const-string v5, "com.google.android.gms.permission.ACTIVITY_RECOGNITION"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_1
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "GoogleLocationManagerSe"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "GoogleLocationManagerSe"

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    const-string v1, "GoogleLocationManagerSe"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GoogleLocationManagerSe"

    const-string v2, "Did not find the activity recognition permission in the app\'s manifest"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lhzx;

    invoke-virtual {v0, p1, p2, p3}, Lhzx;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleLocationManagerSe"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationManagerSe"

    const-string v1, "Location ServiceBroker created."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lhzx;

    invoke-static {v0, p1}, Lhzx;->b(Lhzx;Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->a:Lhzy;

    invoke-virtual {v0}, Lhzy;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->c:Landroid/content/pm/PackageManager;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v0, Lhzy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhzy;-><init>(Lcom/google/android/location/internal/GoogleLocationManagerService;B)V

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->a:Lhzy;

    new-instance v0, Lhzx;

    invoke-virtual {p0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhzx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lhzx;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "LocationServiceBroker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->d:Landroid/os/Handler;

    :goto_0
    return-void

    :cond_0
    const-string v0, "GoogleLocationManagerSe"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleLocationManagerSe"

    const-string v1, "Unable to create looper. Running handler on main thread."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->d:Landroid/os/Handler;

    goto :goto_0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lhzx;

    invoke-static {v0, p1}, Lhzx;->b(Lhzx;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    invoke-static {p1}, Lhon;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lhzx;

    invoke-static {v0, p1}, Lhzx;->a(Lhzx;Landroid/content/Intent;)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, "com.google.android.location.geofencer.service.ACTION_ACTIVITY_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-static {p0}, Lce;->a(Landroid/content/Context;)Lce;

    move-result-object v1

    invoke-virtual {v1, v0}, Lce;->a(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lhzx;

    invoke-static {v0, p1}, Lhzx;->c(Lhzx;Landroid/content/Intent;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
