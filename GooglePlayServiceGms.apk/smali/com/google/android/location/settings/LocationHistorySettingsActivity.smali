.class public Lcom/google/android/location/settings/LocationHistorySettingsActivity;
.super Liki;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private t:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Liki;-><init>()V

    return-void
.end method

.method private a(I)Landroid/app/AlertDialog;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a    # android.R.string.ok

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/location/settings/LocationHistorySettingsActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->t:Landroid/widget/Button;

    const v1, 0x7f0b0449    # com.google.android.gms.R.string.location_settings_delete_location_history_in_progress

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->t:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v0, Liks;

    invoke-direct {v0, p0, p0}, Liks;-><init>(Lcom/google/android/location/settings/LocationHistorySettingsActivity;Lcom/google/android/location/settings/LocationHistorySettingsActivity;)V

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->s:Landroid/accounts/Account;

    invoke-static {p0, v0, v1}, Lcom/google/android/location/reporting/service/DeleteHistoryService;->a(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Messenger;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/location/settings/LocationHistorySettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->f()V

    return-void
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->t:Landroid/widget/Button;

    const v1, 0x7f0b0448    # com.google.android.gms.R.string.location_settings_delete_location_history

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->t:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/location/reporting/service/AccountConfig;)I
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/AccountConfig;->o()I

    move-result v0

    return v0
.end method

.method protected final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->q:Liiw;

    iget-object v1, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->s:Landroid/accounts/Account;

    invoke-interface {v0, v1, p1}, Liiw;->b(Landroid/accounts/Account;Z)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Liki;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0400a4    # com.google.android.gms.R.layout.location_history_settings

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->setContentView(I)V

    const v0, 0x7f0a020f    # com.google.android.gms.R.id.delete_location_history

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->t:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->f()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Liki;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/16 v1, 0xb

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x1010355    # android.R.attr.alertDialogIcon

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    :cond_0
    const v1, 0x7f0b044a    # com.google.android.gms.R.string.location_settings_confirm_deleting_location_history_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b044b    # com.google.android.gms.R.string.location_settings_confirm_deleting_location_history_body

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b044d    # com.google.android.gms.R.string.location_settings_confirm_deleting_location_history_confirm

    new-instance v2, Liko;

    invoke-direct {v2, p0}, Liko;-><init>(Lcom/google/android/location/settings/LocationHistorySettingsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v1, 0x1040000    # android.R.string.cancel

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040044    # com.google.android.gms.R.layout.delete_location_history_dialog

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    const v2, 0x7f0a00da    # com.google.android.gms.R.id.i_understand_delete_consequences

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    new-instance v2, Likp;

    invoke-direct {v2, p0, v1}, Likp;-><init>(Lcom/google/android/location/settings/LocationHistorySettingsActivity;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v2, Likq;

    invoke-direct {v2, p0, v1, v0}, Likq;-><init>(Lcom/google/android/location/settings/LocationHistorySettingsActivity;Landroid/app/AlertDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    new-instance v2, Likr;

    invoke-direct {v2, p0, v0}, Likr;-><init>(Lcom/google/android/location/settings/LocationHistorySettingsActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object v0, v1

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b044e    # com.google.android.gms.R.string.location_settings_delete_auth_error

    invoke-direct {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->a(I)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b044f    # com.google.android.gms.R.string.location_settings_delete_offline_error

    invoke-direct {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->a(I)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0b0450    # com.google.android.gms.R.string.location_settings_delete_io_error

    invoke-direct {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->a(I)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onDeleteLocationHistoryClicked(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->showDialog(I)V

    return-void
.end method
