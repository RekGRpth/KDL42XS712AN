.class public Lcom/google/android/location/settings/GoogleLocationSettingsActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final o:Landroid/content/IntentFilter;


# instance fields
.field private p:Liiw;

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Likn;

.field private u:Ljava/lang/String;

.field private v:Landroid/widget/CheckBox;

.field private w:Landroid/view/ViewGroup;

.field private final x:Ljava/util/List;

.field private y:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.location.reporting.SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->o:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljp;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->x:Ljava/util/List;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I
    .locals 1

    iget v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->q:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->q:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->u:Ljava/lang/String;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/location/reporting/service/ReportingConfig;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/16 v2, 0x13

    invoke-static {v2}, Lbpz;->a(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Lbpl;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-instance v2, Likz;

    invoke-direct {v2, p0}, Likz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Likz;->a()[Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/ReportingConfig;->g()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/ReportingConfig;->c()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/Conditions;->isIneligibleDueToGeoOnly()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    array-length v2, v3

    if-gtz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->u:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->g()V

    return-void
.end method

.method private f()Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->s:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Lbpl;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lbpl;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x89

    invoke-virtual {p0, v1, v2}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    iput-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->s:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->p:Liiw;

    if-nez v0, :cond_1

    const-string v0, "GCoreLocationSettings"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreLocationSettings"

    const-string v1, "Service not connected, skipping UI refresh"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->p:Liiw;

    invoke-interface {v0}, Liiw;->a()Lcom/google/android/location/reporting/service/ReportingConfig;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/ReportingConfig;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iput v10, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->q:I

    :cond_2
    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/ReportingConfig;->c()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->e()Z

    move-result v1

    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/ReportingConfig;->g()Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->isIneligibleDueToGeoOnly()Z

    move-result v3

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->v:Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->t:Likn;

    sget-object v1, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->o:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {p0, v7}, Lcom/google/android/location/reporting/service/InitializerService;->a(Landroid/content/Context;Lcom/google/android/location/reporting/service/ReportingConfig;)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->q:I

    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "refreshUlrSettings("

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "), initialization: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->q:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->r:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->q:I

    if-eq v0, v5, :cond_6

    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/ReportingConfig;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/ReportingConfig;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v0

    const-string v4, "GoogleLocationSettingsActivity"

    invoke-static {p0, v4, v0, v6, v6}, Lijj;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Likf;->a(Ljava/lang/Throwable;)V

    const-string v1, "GCoreLocationSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :cond_5
    iput-boolean v9, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->r:Z

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likl;

    iget-object v4, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->y:Landroid/view/ViewGroup;

    iget-object v0, v0, Likl;->b:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-nez v2, :cond_8

    if-eqz v3, :cond_9

    :cond_8
    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/ReportingConfig;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/location/reporting/service/AccountConfig;

    new-instance v0, Likl;

    invoke-virtual {v6}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Likl;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;Landroid/accounts/Account;)V

    iget-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->y:Landroid/view/ViewGroup;

    iget-object v2, v0, Likl;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v6}, Lcom/google/android/location/reporting/service/AccountConfig;->t()Z

    move-result v2

    invoke-virtual {v6}, Lcom/google/android/location/reporting/service/AccountConfig;->n()I

    move-result v1

    invoke-virtual {v0, v1, v6, v10}, Likl;->a(ILcom/google/android/location/reporting/service/AccountConfig;Z)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v0, Likl;->c:Landroid/view/ViewGroup;

    const v3, 0x7f0b0444    # com.google.android.gms.R.string.location_settings_location_reporting_activity_title

    const-class v5, Lcom/google/android/location/settings/LocationReportingSettingsActivity;

    invoke-virtual/range {v0 .. v5}, Likl;->a(Landroid/view/ViewGroup;ZILjava/lang/String;Ljava/lang/Class;)Landroid/view/ViewGroup;

    invoke-virtual {v6}, Lcom/google/android/location/reporting/service/AccountConfig;->o()I

    move-result v1

    invoke-virtual {v0, v1, v6, v9}, Likl;->a(ILcom/google/android/location/reporting/service/AccountConfig;Z)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v0, Likl;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0b0446    # com.google.android.gms.R.string.location_settings_location_history_activity_title

    const-class v5, Lcom/google/android/location/settings/LocationHistorySettingsActivity;

    invoke-virtual/range {v0 .. v5}, Likl;->a(Landroid/view/ViewGroup;ZILjava/lang/String;Ljava/lang/Class;)Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->x:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->w:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->y:Landroid/view/ViewGroup;

    invoke-static {v0}, Likt;->a(Landroid/view/ViewGroup;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v7}, Lcom/google/android/location/reporting/service/ReportingConfig;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    new-instance v0, Likk;

    invoke-direct {v0, p0}, Likk;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V

    iget-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->w:Landroid/view/ViewGroup;

    const v2, 0x7f0a023e    # com.google.android.gms.R.id.add_account

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch p1, :sswitch_data_0

    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->s:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x89 -> :sswitch_1
    .end sparse-switch
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gsf.action.SET_USE_LOCATION_FOR_SERVICES"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v4, "disable"

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "GCoreLocationSettings"

    const-string v1, "Problem while starting GSF location activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->g()V

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Likh;->a(Landroid/content/Context;)V

    if-eqz p1, :cond_0

    const-string v0, "showingLgaayl"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->s:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "com.google.android.gms.location.settings.LOCATION_HISTORY"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v3

    :goto_0
    const/16 v4, 0x13

    invoke-static {v4}, Lbpz;->a(I)Z

    move-result v4

    if-eqz v4, :cond_4

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f()Z

    move-result v2

    if-nez v2, :cond_8

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->startActivity(Landroid/content/Intent;)V

    :goto_1
    const-string v1, "GCoreLocationSettings"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "GCoreLocationSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Redirecting: lgaayl: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->finish()V

    :goto_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GCoreLocationSettings"

    const-string v1, "Can\'t run for restricted users."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->finish()V

    goto :goto_2

    :cond_5
    const v0, 0x7f0400a6    # com.google.android.gms.R.layout.location_settings

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->setContentView(I)V

    const v0, 0x7f0a0083    # com.google.android.gms.R.id.settings_root

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->y:Landroid/view/ViewGroup;

    const v0, 0x7f0a0210    # com.google.android.gms.R.id.lgaayl_pref

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p0}, Lbpl;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {p0}, Lbpl;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v3

    :goto_3
    if-eqz v1, :cond_7

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v1, 0x1020001    # android.R.id.checkbox

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->v:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    new-instance v4, Likj;

    invoke-direct {v4, p0}, Likj;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V

    const v1, 0x7f0b0431    # com.google.android.gms.R.string.location_settings_allow_access_title

    invoke-static {v0, v1}, Likt;->a(Landroid/view/ViewGroup;I)V

    const v1, 0x1020010    # android.R.id.summary

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v5, 0x7f0b0432    # com.google.android.gms.R.string.location_settings_allow_access_summary

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    const v0, 0x7f0a0213    # com.google.android.gms.R.id.no_settings_shown

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->w:Landroid/view/ViewGroup;

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljj;->a(Z)V

    iput v2, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->q:I

    new-instance v0, Likn;

    invoke-direct {v0, p0, v2}, Likn;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;B)V

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->t:Likn;

    goto/16 :goto_2

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_4

    :cond_8
    move-object v0, v1

    goto/16 :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Ljp;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b0433    # com.google.android.gms.R.string.location_settings_see_global_settings_dialog_message

    invoke-virtual {p0, v1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a    # android.R.string.ok

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v1, 0x1040000    # android.R.string.cancel

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const v0, 0x7f0b0430    # com.google.android.gms.R.string.location_settings_menu_help

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    return v2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c    # android.R.id.home

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->finish()V

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v2, v0, :cond_2

    const-string v2, "ulr_googlelocation"

    invoke-static {p0, v2}, Lhid;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {p0, v3}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    const-class v2, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v3, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3, v1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onPause()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->t:Likn;

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-super {p0}, Ljp;->onPause()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "GCoreLocationSettings"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreLocationSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Swallowing "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Ljp;->onResume()V

    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->g()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ljp;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "showingLgaayl"

    iget-boolean v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->s:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreLocationSettings"

    const-string v1, "GoogleLocationSettingsActivity.onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p2}, Liix;->a(Landroid/os/IBinder;)Liiw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->p:Liiw;

    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->g()V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreLocationSettings"

    const-string v1, "GoogleLocationSettingsActivity.onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->p:Liiw;

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Ljp;->onStart()V

    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f()Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->r:Z

    invoke-static {p0, p0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Ljp;->onStop()V

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->p:Liiw;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->p:Liiw;

    :cond_0
    return-void
.end method
