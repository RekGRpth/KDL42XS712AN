.class public Lcom/google/android/location/network/NetworkLocationService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static b:Lcom/google/android/location/network/NetworkLocationService;


# instance fields
.field private a:Lcom/google/android/location/network/NetworkLocationProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "GmsNetworkLocationService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    sput-object p0, Lcom/google/android/location/network/NetworkLocationService;->b:Lcom/google/android/location/network/NetworkLocationService;

    return-void
.end method

.method static a(Landroid/content/Context;)V
    .locals 8

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lhhv;->a:Landroid/net/Uri;

    const-string v3, "(name=?)"

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "network_location_opt_in"

    aput-object v5, v4, v7

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "GmsNetworkLocationService"

    const-string v1, "applySettings(): provider not available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network_location_opt_in"

    invoke-static {v0, v1, v7}, Lhhv;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1, v7}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    invoke-static {p0}, Lbpl;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lbpl;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v6

    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/location/network/ConfirmAlertActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "confirmLgaayl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_1

    :cond_3
    invoke-static {p0, v7}, Lcom/google/android/location/network/NetworkLocationService;->b(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Z)V
    .locals 2

    invoke-static {p0, p1}, Lcom/google/android/location/network/NetworkLocationService;->b(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    return-void
.end method

.method private static b(Landroid/content/Context;Z)V
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network_location_opt_in"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-static {v1, v2, v0}, Lhhv;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-virtual {p0}, Lcom/google/android/location/network/NetworkLocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/network/NetworkLocationProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/network/NetworkLocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/network/NetworkLocationService;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/location/network/NetworkLocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "location"

    sget-object v2, Lhjh;->a:Lbfy;

    invoke-static {v0, v1, v2}, Lhnj;->a(Landroid/content/Context;Ljava/lang/String;Lbfy;)V

    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/location/network/NetworkLocationProvider;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    invoke-static {p1}, Lhpg;->a(Landroid/content/Intent;)Lcom/google/android/location/clientlib/NlpLocation;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/location/clientlib/NlpLocation;->a:Landroid/location/Location;

    if-eqz v0, :cond_0

    const-string v1, "noGPSLocation"

    new-instance v2, Landroid/location/Location;

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-static {v0, v1, v2}, Lilk;->a(Landroid/location/Location;Ljava/lang/String;Landroid/location/Location;)V

    iget-object v1, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/location/network/NetworkLocationProvider;->reportLocation(Landroid/location/Location;)V

    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/location/network/NetworkLocationProvider;->onDisable()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
