.class public Lcom/google/android/location/network/LocationProviderChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Liae;->e:Liae;

    invoke-static {v0, p1}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v0

    sget-object v1, Liae;->a:Liae;

    invoke-static {v1, p1}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v1

    if-eqz v0, :cond_2

    iget v0, v0, Liad;->d:I

    const/16 v2, 0x7d3

    if-lt v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    sget-object v2, Liae;->f:Liae;

    iget-object v3, v1, Liad;->a:Liae;

    if-ne v2, v3, :cond_3

    :cond_1
    const/16 v2, 0x13

    invoke-static {v2}, Lbpz;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "ProvidersChangedReceiver"

    const-string v1, "calling applySettings"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/google/android/location/network/NetworkLocationService;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const-string v2, "ProvidersChangedReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Not calling apply-settings: debugClause is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "ProvidersChangedReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "and Android NLP is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "ProvidersChangedReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "and build Version is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
