.class public Lcom/google/android/location/network/NetworkLocationProvider;
.super Lcom/android/location/provider/LocationProviderBase;
.source "SourceFile"


# static fields
.field private static a:Lcom/android/location/provider/ProviderPropertiesUnbundled;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v8, 0x2

    move v1, v0

    move v2, v0

    move v3, v0

    move v5, v4

    move v6, v4

    move v7, v4

    invoke-static/range {v0 .. v8}, Lcom/android/location/provider/ProviderPropertiesUnbundled;->create(ZZZZZZZII)Lcom/android/location/provider/ProviderPropertiesUnbundled;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/network/NetworkLocationProvider;->a:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "GmsNetworkLocationProvi"

    sget-object v1, Lcom/google/android/location/network/NetworkLocationProvider;->a:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    invoke-direct {p0, v0, v1}, Lcom/android/location/provider/LocationProviderBase;-><init>(Ljava/lang/String;Lcom/android/location/provider/ProviderPropertiesUnbundled;)V

    new-instance v0, Lich;

    invoke-direct {v0, p0}, Lich;-><init>(Lcom/google/android/location/network/NetworkLocationProvider;)V

    iput-object v0, p0, Lcom/google/android/location/network/NetworkLocationProvider;->c:Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/location/network/NetworkLocationProvider;->b:Landroid/content/Context;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/location/network/NetworkLocationProvider;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationProvider;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onDisable()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationProvider;->c:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onEnable()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationProvider;->c:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onGetStatus(Landroid/os/Bundle;)I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public onGetStatusUpdateTime()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onSetRequest(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V
    .locals 4

    const-string v0, "GmsNetworkLocationProvi"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSetRequest: ProviderRequestUnbundled, reportLocation is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/location/provider/ProviderRequestUnbundled;->getReportLocation()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and interval is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/location/provider/ProviderRequestUnbundled;->getInterval()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationProvider;->c:Landroid/os/Handler;

    const/4 v1, 0x3

    new-instance v2, Lici;

    invoke-direct {v2, p1, p2}, Lici;-><init>(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
