.class public Lcom/google/android/location/fused/FusedLocationService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lhva;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/fused/FusedLocationService;->a:Lhva;

    if-nez v0, :cond_0

    new-instance v0, Lhva;

    invoke-virtual {p0}, Lcom/google/android/location/fused/FusedLocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhva;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/fused/FusedLocationService;->a:Lhva;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/FusedLocationService;->a:Lhva;

    invoke-virtual {v0}, Lhva;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/fused/FusedLocationService;->a:Lhva;

    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/fused/FusedLocationService;->a:Lhva;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/fused/FusedLocationService;->a:Lhva;

    invoke-virtual {v0}, Lhva;->onDisable()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
