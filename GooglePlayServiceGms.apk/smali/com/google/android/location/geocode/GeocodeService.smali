.class public Lcom/google/android/location/geocode/GeocodeService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lhxi;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/geocode/GeocodeService;->a:Lhxi;

    if-nez v0, :cond_0

    new-instance v0, Lhxi;

    invoke-direct {v0}, Lhxi;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/geocode/GeocodeService;->a:Lhxi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/geocode/GeocodeService;->a:Lhxi;

    invoke-virtual {v0}, Lhxi;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/location/geocode/GeocodeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Life;->a(Landroid/content/Context;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/location/geocode/GeocodeService;->a:Lhxi;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
