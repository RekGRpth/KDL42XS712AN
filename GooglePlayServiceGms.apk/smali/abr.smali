.class public final Labr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

.field public final b:Lacl;

.field public final c:Ljava/util/List;

.field public final d:I

.field public final e:Ljava/util/List;

.field public final f:Ljava/util/List;

.field public final g:I

.field public final h:J

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Lzg;

.field public final l:Lzs;

.field public final m:Ljava/lang/String;

.field public final n:Lzh;

.field public final o:Lzj;

.field public final p:J

.field public final q:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field public final r:J

.field public final s:J

.field public final t:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lacl;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLzg;Lzs;Ljava/lang/String;Lzh;Lzj;JLcom/google/android/gms/ads/internal/client/AdSizeParcel;JJJ)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Labr;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iput-object p2, p0, Labr;->b:Lacl;

    if-eqz p3, :cond_0

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Labr;->c:Ljava/util/List;

    iput p4, p0, Labr;->d:I

    if-eqz p5, :cond_1

    invoke-static {p5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Labr;->e:Ljava/util/List;

    if-eqz p6, :cond_2

    invoke-static {p6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_2
    iput-object v2, p0, Labr;->f:Ljava/util/List;

    iput p7, p0, Labr;->g:I

    iput-wide p8, p0, Labr;->h:J

    iput-object p10, p0, Labr;->i:Ljava/lang/String;

    iput-boolean p11, p0, Labr;->j:Z

    iput-object p12, p0, Labr;->k:Lzg;

    move-object/from16 v0, p13

    iput-object v0, p0, Labr;->l:Lzs;

    move-object/from16 v0, p14

    iput-object v0, p0, Labr;->m:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Labr;->n:Lzh;

    move-object/from16 v0, p16

    iput-object v0, p0, Labr;->o:Lzj;

    move-wide/from16 v0, p17

    iput-wide v0, p0, Labr;->p:J

    move-object/from16 v0, p19

    iput-object v0, p0, Labr;->q:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-wide/from16 v0, p20

    iput-wide v0, p0, Labr;->r:J

    move-wide/from16 v0, p22

    iput-wide v0, p0, Labr;->s:J

    move-wide/from16 v0, p24

    iput-wide v0, p0, Labr;->t:J

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method
