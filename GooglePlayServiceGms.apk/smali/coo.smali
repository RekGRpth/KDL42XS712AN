.class public abstract enum Lcoo;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcon;


# static fields
.field public static final enum a:Lcoo;

.field public static final enum b:Lcoo;

.field public static final enum c:Lcoo;

.field private static final synthetic d:[Lcoo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcop;

    const-string v1, "WALL"

    invoke-direct {v0, v1}, Lcop;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcoo;->a:Lcoo;

    new-instance v0, Lcoq;

    const-string v1, "UPTIME"

    invoke-direct {v0, v1}, Lcoq;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcoo;->b:Lcoo;

    new-instance v0, Lcor;

    const-string v1, "REALTIME"

    invoke-direct {v0, v1}, Lcor;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcoo;->c:Lcoo;

    const/4 v0, 0x3

    new-array v0, v0, [Lcoo;

    const/4 v1, 0x0

    sget-object v2, Lcoo;->a:Lcoo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcoo;->b:Lcoo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcoo;->c:Lcoo;

    aput-object v2, v0, v1

    sput-object v0, Lcoo;->d:[Lcoo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcoo;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcoo;
    .locals 1

    const-class v0, Lcoo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcoo;

    return-object v0
.end method

.method public static values()[Lcoo;
    .locals 1

    sget-object v0, Lcoo;->d:[Lcoo;

    invoke-virtual {v0}, [Lcoo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcoo;

    return-object v0
.end method


# virtual methods
.method public abstract a()J
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Clock[type=%s, time=%d]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcoo;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcoo;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
