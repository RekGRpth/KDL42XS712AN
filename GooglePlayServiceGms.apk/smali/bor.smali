.class public abstract Lbor;
.super Lbon;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbol;

.field private final c:Lbon;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbol;ILbon;)V
    .locals 2

    invoke-static {p1, p2}, Lbol;->a(Lbol;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lbol;->a(Lbol;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbol;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1, p3}, Lbor;-><init>(Lbol;Ljava/lang/String;Ljava/lang/String;Lbon;)V

    return-void
.end method

.method public constructor <init>(Lbol;ILjava/lang/String;Lbon;)V
    .locals 1

    invoke-static {p1, p2}, Lbol;->a(Lbol;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3, p4}, Lbor;-><init>(Lbol;Ljava/lang/String;Ljava/lang/String;Lbon;)V

    return-void
.end method

.method public constructor <init>(Lbol;Ljava/lang/String;Lbon;)V
    .locals 1

    invoke-static {p2}, Lbol;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, Lbor;-><init>(Lbol;Ljava/lang/String;Ljava/lang/String;Lbon;)V

    return-void
.end method

.method private constructor <init>(Lbol;Ljava/lang/String;Ljava/lang/String;Lbon;)V
    .locals 0

    iput-object p1, p0, Lbor;->a:Lbol;

    invoke-direct {p0, p1}, Lbon;-><init>(Lbol;)V

    iput-object p4, p0, Lbor;->c:Lbon;

    iput-object p2, p0, Lbor;->d:Ljava/lang/String;

    iput-object p3, p0, Lbor;->e:Ljava/lang/String;

    return-void
.end method

.method private c()Z
    .locals 1

    iget-object v0, p0, Lbor;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Z
    .locals 1

    invoke-direct {p0}, Lbor;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)I
    .locals 1

    invoke-direct {p0}, Lbor;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    return p1
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lbor;->c:Lbon;

    invoke-virtual {v0}, Lbon;->a()I

    move-result v1

    invoke-direct {p0}, Lbor;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Lbor;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lbor;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, p2}, Lbor;->a(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, Lbor;->c:Lbon;

    invoke-direct {p0, p1}, Lbor;->d(I)I

    move-result v4

    if-eqz p4, :cond_2

    invoke-direct {p0}, Lbor;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    if-ne p1, v0, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_2

    :goto_2
    invoke-virtual {v3, v4, p2, p3, v0}, Lbon;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method protected abstract a(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    invoke-direct {p0, p1}, Lbor;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbor;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbor;->c:Lbon;

    invoke-direct {p0, p1}, Lbor;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbon;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ljava/util/ArrayList;I)V
    .locals 2

    iget-object v0, p0, Lbor;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbor;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbot;

    iget-object v1, p0, Lbor;->e:Ljava/lang/String;

    invoke-direct {v0, v1, p2}, Lbot;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lbor;->c:Lbon;

    invoke-direct {p0}, Lbor;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, p2

    invoke-virtual {v1, p1, v0}, Lbon;->a(Ljava/util/ArrayList;I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)I
    .locals 2

    invoke-direct {p0, p1}, Lbor;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbor;->a:Lbol;

    iget v0, v0, Lbol;->c:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbor;->c:Lbon;

    invoke-direct {p0, p1}, Lbor;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbon;->b(I)I

    move-result v0

    goto :goto_0
.end method
