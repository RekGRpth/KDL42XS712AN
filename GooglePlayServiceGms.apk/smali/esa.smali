.class public Lesa;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lerd;

.field private final d:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field private final e:Ljava/util/Locale;

.field private final f:Lesb;

.field private final g:Ljava/util/LinkedHashSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lesa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lesa;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Locale;Lerd;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lesa;->b:Landroid/content/Context;

    iput-object p3, p0, Lesa;->c:Lerd;

    iput-object p2, p0, Lesa;->e:Ljava/util/Locale;

    new-instance v0, Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/location/places/internal/PlacesParams;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    iput-object v0, p0, Lesa;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lesa;->g:Ljava/util/LinkedHashSet;

    iget-object v0, p0, Lesa;->g:Ljava/util/LinkedHashSet;

    sget v1, Lxf;->bo:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lesa;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lesa;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v2, Lesb;

    iget-object v3, p0, Lesa;->c:Lerd;

    invoke-direct {v2, v3, v1, v0}, Lesb;-><init>(Lerd;Ljava/lang/String;I)V

    iput-object v2, p0, Lesa;->f:Lesb;

    return-void

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method
