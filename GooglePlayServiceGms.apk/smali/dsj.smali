.class public final Ldsj;
.super Ldrw;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ldax;

.field private final g:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)V
    .locals 0

    invoke-direct {p0, p1}, Ldrw;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldsj;->b:Ldad;

    iput p3, p0, Ldsj;->g:I

    iput-object p4, p0, Ldsj;->c:Ljava/lang/String;

    iput-object p5, p0, Ldsj;->d:Ljava/lang/String;

    iput-object p6, p0, Ldsj;->e:Ljava/lang/String;

    iput-object p7, p0, Ldsj;->f:Ldax;

    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    iget-object v0, p0, Ldsj;->b:Ldad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldsj;->b:Ldad;

    iget-object v1, p0, Ldsj;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Ldad;->b(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)I
    .locals 7

    iget v0, p0, Ldsj;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v2, p0, Ldsj;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsj;->c:Ljava/lang/String;

    iget-object v4, p0, Ldsj;->d:Ljava/lang/String;

    iget-object v5, p0, Ldsj;->e:Ljava/lang/String;

    iget-object v6, p0, Ldsj;->f:Ldax;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Ldsj;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v2, p0, Ldsj;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsj;->c:Ljava/lang/String;

    iget-object v4, p0, Ldsj;->d:Ljava/lang/String;

    iget-object v5, p0, Ldsj;->e:Ljava/lang/String;

    iget-object v6, p0, Ldsj;->f:Ldax;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)I

    move-result v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown opetation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldsj;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
