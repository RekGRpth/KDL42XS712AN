.class public final Lbxs;
.super Lbxf;
.source "SourceFile"


# instance fields
.field private Y:Lcfz;

.field private Z:Lbuc;

.field private aa:Lcom/google/android/gms/drive/DriveId;

.field private ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private ac:I

.field private ad:Lcfc;

.field private ae:Landroid/view/View;

.field private af:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private ag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lbxf;-><init>()V

    return-void
.end method

.method private J()Landroid/widget/EditText;
    .locals 2

    iget-object v0, p0, Lbxs;->ae:Landroid/view/View;

    const v1, 0x7f0a00dc    # com.google.android.gms.R.id.create_file_dialog_edittext_document_title

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lbxs;)V
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0}, Lbxs;->J()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcle;->b:Lcje;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    iget-object v0, p0, Lbxs;->Y:Lcfz;

    iget-object v1, p0, Lbxs;->ad:Lcfc;

    iget-object v2, p0, Lbxs;->af:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v0, v1, v2}, Lcfz;->a(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lbsp;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lbxs;->Z:Lbuc;

    iget-object v2, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget v3, p0, Lbxs;->ac:I

    iget-object v4, p0, Lbxs;->aa:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1, v0, v2, v3, v4}, Lbuc;->a(Lbsp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;ILcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    :try_end_0
    .catch Lbrm; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "response_drive_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lo;->setResult(ILandroid/content/Intent;)V

    :cond_0
    invoke-virtual {p0, v5}, Lk;->a(Z)V

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "CreateFileDialogFragment"

    const-string v2, "Error creating new file"

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0, v5}, Lo;->setResult(I)V

    :cond_2
    invoke-virtual {p0, v5}, Lk;->a(Z)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/DriveId;)V
    .locals 8

    const/4 v6, 0x0

    iget-object v0, p0, Lbxs;->ae:Landroid/view/View;

    const v1, 0x7f0a00de    # com.google.android.gms.R.id.create_file_dialog_folder_selector

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lbxs;->ad:Lcfc;

    invoke-static {v1}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v2

    if-eqz p1, :cond_1

    iget-object v1, p0, Lbxs;->Y:Lcfz;

    iget-object v3, p0, Lbxs;->ad:Lcfc;

    iget-object v3, v3, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcfz;->a(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v1

    :goto_0
    iget-object v3, p0, Lbxs;->Y:Lcfz;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v4}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b004b    # com.google.android.gms.R.string.drive_menu_my_drive

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcfz;->b(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v2

    invoke-virtual {v2}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    if-eqz v1, :cond_0

    invoke-virtual {v2, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    iput-object v2, p0, Lbxs;->aa:Lcom/google/android/gms/drive/DriveId;

    sget-object v1, Lbyp;->a:Lbyp;

    invoke-virtual {v1}, Lbyp;->a()I

    move-result v1

    invoke-virtual {p0, v1}, Lbxs;->b(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020106    # com.google.android.gms.R.drawable.ic_drive_my_drive

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2, v6, v6, v6}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lbxs;->aa:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcfp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcfp;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcfp;->s()Z

    move-result v1

    invoke-static {v3, v1}, Lcfp;->a(Ljava/lang/String;Z)I

    move-result v1

    move-object v7, v2

    move v2, v1

    move-object v1, v7

    goto :goto_1
.end method

.method static synthetic b(Lbxs;)Lcfc;
    .locals 1

    iget-object v0, p0, Lbxs;->ad:Lcfc;

    return-object v0
.end method

.method static synthetic c(Lbxs;)Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    iget-object v0, p0, Lbxs;->af:Lcom/google/android/gms/drive/auth/AppIdentity;

    return-object v0
.end method

.method static synthetic d(Lbxs;)Lcfz;
    .locals 1

    iget-object v0, p0, Lbxs;->Y:Lcfz;

    return-object v0
.end method

.method static synthetic e(Lbxs;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    iget-object v0, p0, Lbxs;->aa:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    if-nez p1, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const-string v0, "response_drive_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lbxs;->a(Lcom/google/android/gms/drive/DriveId;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lbxf;->a(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lbxf;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    invoke-virtual {v0}, Lcoy;->f()Lcfz;

    move-result-object v1

    iput-object v1, p0, Lbxs;->Y:Lcfz;

    new-instance v1, Lbuc;

    invoke-direct {v1, v0}, Lbuc;-><init>(Lcoy;)V

    iput-object v1, p0, Lbxs;->Z:Lbuc;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "callerIdentity"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lbxs;->af:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v0, p0, Lbxs;->Y:Lcfz;

    invoke-interface {v0, v1}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    iput-object v0, p0, Lbxs;->ad:Lcfc;

    if-eqz p1, :cond_1

    :goto_0
    const-string v0, "selectedCollectionDriveId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lbxs;->aa:Lcom/google/android/gms/drive/DriveId;

    const-string v0, "metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object v0, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    const-string v0, "requestId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lbxs;->ac:I

    const-string v0, "dialogTitle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbxs;->ag:Ljava/lang/String;

    iget-object v0, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v0, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcle;->b:Lcje;

    invoke-virtual {p0}, Lbxs;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b001d    # com.google.android.gms.R.string.drive_create_file_default_title

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    iget-object v0, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcle;->c:Lcje;

    const-string v2, "application/octet-stream"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    iget-object p1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lcaa;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    const-string v0, "layout_inflater"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x7f040047    # com.google.android.gms.R.layout.drive_create_new_file_fragment

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbxs;->ae:Landroid/view/View;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lbxs;->ae:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f020180    # com.google.android.gms.R.drawable.launcher_drive_icon

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a    # android.R.string.ok

    new-instance v2, Lbxt;

    invoke-direct {v2, p0}, Lbxt;-><init>(Lbxs;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v1, 0x1040000    # android.R.string.cancel

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lbxs;->ag:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbxs;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v0

    :cond_0
    const v1, 0x7f0b0021    # com.google.android.gms.R.string.drive_create_file_dialog_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lbxf;->e(Landroid/os/Bundle;)V

    const-string v0, "selectedCollectionDriveId"

    iget-object v1, p0, Lbxs;->aa:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "metadata"

    iget-object v1, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "requestId"

    iget v1, p0, Lbxs;->ac:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "dialogTitle"

    iget-object v1, p0, Lbxs;->ag:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    invoke-super {p0, p1}, Lbxf;->onDismiss(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public final w()V
    .locals 3

    invoke-super {p0}, Lbxf;->w()V

    invoke-direct {p0}, Lbxs;->J()Landroid/widget/EditText;

    move-result-object v1

    iget-object v0, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcle;->b:Lcje;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbxs;->ae:Landroid/view/View;

    const v1, 0x7f0a00de    # com.google.android.gms.R.id.create_file_dialog_folder_selector

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lbxu;

    invoke-direct {v1, p0}, Lbxu;-><init>(Lbxs;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbxs;->aa:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lbxs;->a(Lcom/google/android/gms/drive/DriveId;)V

    iget-object v0, p0, Lbxs;->ae:Landroid/view/View;

    const v1, 0x7f0a00dd    # com.google.android.gms.R.id.drive_create_file_dialog_account_name

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lbxs;->ad:Lcfc;

    iget-object v1, v1, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final x()V
    .locals 3

    invoke-super {p0}, Lbxf;->x()V

    invoke-direct {p0}, Lbxs;->J()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lbxs;->ab:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcle;->b:Lcje;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    return-void
.end method
