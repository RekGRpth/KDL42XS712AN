.class public final Ldsz;
.super Ldrv;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;IIIIZ)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Ldrv;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    iput-object p2, p0, Ldsz;->b:Ldad;

    iput-object p3, p0, Ldsz;->c:Ljava/lang/String;

    iput-object p4, p0, Ldsz;->d:Ljava/lang/String;

    iput p5, p0, Ldsz;->e:I

    iput p6, p0, Ldsz;->f:I

    iput p7, p0, Ldsz;->g:I

    iput p8, p0, Ldsz;->h:I

    iput-boolean p9, p0, Ldsz;->i:Z

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lcun;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v5, 0x1

    if-nez p3, :cond_2

    iget v0, p0, Ldsz;->h:I

    if-ne v0, v5, :cond_0

    iget-object v2, p0, Ldsz;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsz;->c:Ljava/lang/String;

    iget-object v4, p0, Ldsz;->d:Ljava/lang/String;

    iget v5, p0, Ldsz;->e:I

    iget v6, p0, Ldsz;->f:I

    iget v7, p0, Ldsz;->g:I

    iget-boolean v8, p0, Ldsz;->i:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Ldsz;->h:I

    if-nez v0, :cond_1

    iget-object v2, p0, Ldsz;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsz;->c:Ljava/lang/String;

    iget-object v4, p0, Ldsz;->d:Ljava/lang/String;

    iget v5, p0, Ldsz;->e:I

    iget v6, p0, Ldsz;->f:I

    iget v7, p0, Ldsz;->g:I

    iget-boolean v8, p0, Ldsz;->i:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown page type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldsz;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-ne p3, v5, :cond_3

    iget-object v2, p0, Ldsz;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsz;->c:Ljava/lang/String;

    iget-object v4, p0, Ldsz;->d:Ljava/lang/String;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incorrect number of data holders requested!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    array-length v0, p1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldsz;->b:Ldad;

    aget-object v1, p1, v1

    aget-object v2, p1, v2

    invoke-interface {v0, v1, v2}, Ldad;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method
