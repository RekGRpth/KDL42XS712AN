.class public final Ldev;
.super Lbgq;
.source "SourceFile"

# interfaces
.implements Ldet;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbgq;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    const-string v0, "_id"

    invoke-virtual {p0, v0}, Ldev;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "notification_id"

    invoke-virtual {p0, v0}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "external_game_id"

    invoke-virtual {p0, v0}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 1

    const-string v0, "type"

    invoke-virtual {p0, v0}, Ldev;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    const-string v0, "image_uri"

    invoke-virtual {p0, v0}, Ldev;->g(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    const-string v0, "ticker"

    invoke-virtual {p0, v0}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    const-string v0, "title"

    invoke-virtual {p0, v0}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    const-string v0, "text"

    invoke-virtual {p0, v0}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    const-string v0, "coalesced_text"

    invoke-virtual {p0, v0}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Z
    .locals 1

    const-string v0, "acknowledged"

    invoke-virtual {p0, v0}, Ldev;->c(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    const-string v0, "alert_level"

    invoke-virtual {p0, v0}, Ldev;->c(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lbkj;->a(Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "Id"

    const-string v2, "_id"

    invoke-virtual {p0, v2}, Ldev;->b(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "NotificationId"

    const-string v2, "notification_id"

    invoke-virtual {p0, v2}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "Type"

    const-string v2, "type"

    invoke-virtual {p0, v2}, Ldev;->c(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "Title"

    const-string v2, "title"

    invoke-virtual {p0, v2}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "Ticker"

    const-string v2, "ticker"

    invoke-virtual {p0, v2}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "Text"

    const-string v2, "text"

    invoke-virtual {p0, v2}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "CoalescedText"

    const-string v2, "coalesced_text"

    invoke-virtual {p0, v2}, Ldev;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "isAcknowledged"

    invoke-virtual {p0}, Ldev;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "isSilent"

    invoke-virtual {p0}, Ldev;->k()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    invoke-virtual {v0}, Lbkk;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
