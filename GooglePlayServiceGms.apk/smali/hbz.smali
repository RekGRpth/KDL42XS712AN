.class public final Lhbz;
.super Ljava/util/TimerTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/service/PaymentService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/service/PaymentService;)V
    .locals 0

    iput-object p1, p0, Lhbz;->a:Lcom/google/android/gms/wallet/service/PaymentService;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    new-instance v0, Lhhe;

    iget-object v1, p0, Lhbz;->a:Lcom/google/android/gms/wallet/service/PaymentService;

    invoke-direct {v0, v1}, Lhhe;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lhhe;->a()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lhhf;

    iget-object v2, p0, Lhbz;->a:Lcom/google/android/gms/wallet/service/PaymentService;

    iget-object v3, p0, Lhbz;->a:Lcom/google/android/gms/wallet/service/PaymentService;

    invoke-static {v3}, Lcom/google/android/gms/wallet/service/PaymentService;->a(Lcom/google/android/gms/wallet/service/PaymentService;)Lorg/apache/http/client/HttpClient;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhhf;-><init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhd;

    const-string v3, "PaymentService"

    const-string v4, "Sending crash to server"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "https://checkout.google.com/inapp/frontend/app/android_exception"

    iget-object v4, v1, Lhhf;->a:Landroid/content/Context;

    invoke-static {v4, v0}, Lhhf;->a(Landroid/content/Context;Lhhd;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v4, v3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    :try_start_0
    const-string v0, "CrashLogger"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Sending post to url="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lhhf;->b:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    const-string v3, "CrashLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "HttpResponse sc="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "CrashLogger"

    const-string v4, "Unable to send crash log"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v3, "CrashLogger"

    const-string v4, "Unable to send crash log"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    return-void
.end method
