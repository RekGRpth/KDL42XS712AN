.class public abstract Ldvj;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Ldvh;


# instance fields
.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldvj;->d:Z

    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 1

    iget-boolean v0, p0, Ldvj;->d:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Ldvj;->d:Z

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Ldvj;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Ldvj;->a(ZZ)V

    return-void
.end method

.method public final j()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v0}, Ldvj;->a(ZZ)V

    return-void
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Ldvj;->d:Z

    return v0
.end method
