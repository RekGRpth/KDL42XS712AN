.class public final Laqc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lbfy;

.field public static b:Lbfy;

.field public static c:Lbfy;

.field public static d:Lbfy;

.field public static e:Lbfy;

.field public static f:Lbfy;

.field public static g:Lbfy;

.field public static h:Lbfy;

.field public static i:Lbfy;

.field public static j:Lbfy;

.field public static k:Lbfy;

.field public static l:Lbfy;

.field public static m:Lbfy;

.field public static n:Lbfy;

.field public static o:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string v0, "auth_servlet_path"

    const-string v1, "https://android.clients.google.com/auth"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->a:Lbfy;

    const-string v0, "setup_servlet_path"

    const-string v1, "https://android.clients.google.com/setup"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->b:Lbfy;

    const-string v0, "checkname_servlet_path"

    const-string v1, "https://android.clients.google.com/setup/checkname"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->c:Lbfy;

    const-string v0, "create_profile_path"

    const-string v1, "https://android.clients.google.com/setup/createprofile"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->d:Lbfy;

    const-string v0, "enable_dumpsys"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->e:Lbfy;

    const-string v0, "enable_playlog"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->f:Lbfy;

    const-string v0, "enable_clearcut"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->g:Lbfy;

    const-string v0, "enable_auth_generic_account_recovery"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->h:Lbfy;

    const-string v0, "enable_auth_generic_login_challange"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->i:Lbfy;

    const-string v0, "enable_auth_generic_two_step_verification"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->j:Lbfy;

    const-string v0, "enable_auth_generic_reauth"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->k:Lbfy;

    const-string v0, "get_token_sample_percentage"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->l:Lbfy;

    const-string v0, "grant_credential_screen_sample_percentage"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->m:Lbfy;

    const-string v0, "enable_droidguard"

    invoke-static {v0, v2}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->n:Lbfy;

    const-string v0, "auth_enable_droidguard_token_on_request_probability"

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Double;)Lbfy;

    move-result-object v0

    sput-object v0, Laqc;->o:Lbfy;

    return-void
.end method
