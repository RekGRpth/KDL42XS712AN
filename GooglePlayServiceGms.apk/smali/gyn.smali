.class public final Lgyn;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgxf;
.implements Lgyq;


# instance fields
.field private Y:Z

.field private Z:Lhgu;

.field a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private aa:Ljava/lang/String;

.field private ab:Lgwx;

.field private ac:Lgwy;

.field private ad:Lgwn;

.field b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field d:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

.field e:Lgvc;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/ImageView;

.field h:Landroid/widget/EditText;

.field private i:Lioj;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Lioj;ZZLjava/util/Collection;Lipv;)Lgyn;
    .locals 3

    new-instance v0, Lgyn;

    invoke-direct {v0}, Lgyn;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "com.google.android.gms.wallet.instrument"

    invoke-static {v1, v2, p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    const-string v2, "com.google.android.gms.wallet.requireAddressUpgrade"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "com.google.android.gms.wallet.addressHints"

    invoke-static {v1, v2, p3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v2, "com.google.android.gms.wallet.baseAddress"

    invoke-static {v1, v2, p4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    invoke-virtual {v0, v1}, Lgyn;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private b(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x4

    new-array v4, v0, [Lgyo;

    iget-object v0, p0, Lgyn;->e:Lgvc;

    aput-object v0, v4, v2

    iget-object v0, p0, Lgyn;->ab:Lgwx;

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lgyn;->ac:Lgwy;

    aput-object v3, v4, v0

    const/4 v0, 0x3

    iget-object v3, p0, Lgyn;->ad:Lgwn;

    aput-object v3, v4, v0

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    if-eqz v6, :cond_0

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final J()V
    .locals 4

    iget-object v0, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    invoke-virtual {p0}, Lgyn;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b014d    # com.google.android.gms.R.string.wallet_error_creditcard_expiry_date_invalid

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    iget-object v0, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    return-void
.end method

.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgyn;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgyn;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    const v0, 0x7f040137    # com.google.android.gms.R.layout.wallet_fragment_update_instrument

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a031c    # com.google.android.gms.R.id.card_description

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgyn;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lgyn;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lgyn;->aa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0340    # com.google.android.gms.R.id.card_nickname

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgyn;->h:Landroid/widget/EditText;

    iget-object v0, p0, Lgyn;->i:Lioj;

    iget-object v0, v0, Lioj;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgyn;->aa:Ljava/lang/String;

    iget-object v1, p0, Lgyn;->i:Lioj;

    iget-object v1, v1, Lioj;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgyn;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lgyn;->i:Lioj;

    iget-object v1, v1, Lioj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f0a031b    # com.google.android.gms.R.id.card_image

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lgyn;->g:Landroid/widget/ImageView;

    iget-object v0, p0, Lgyn;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lgyn;->i:Lioj;

    iget-object v3, p0, Lgyn;->Z:Lhgu;

    invoke-static {v0, v1, v3}, Lgth;->a(Landroid/widget/ImageView;Lioj;Lhgu;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgyn;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lgyn;->i:Lioj;

    invoke-static {v0}, Lgth;->f(Lioj;)Z

    move-result v0

    if-nez v0, :cond_5

    const v0, 0x7f0a032e    # com.google.android.gms.R.id.exp_date_and_cvc

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-boolean v0, p0, Lgyn;->Y:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lgyn;->i:Lioj;

    iget-object v0, v0, Lioj;->e:Lipv;

    iget-boolean v0, v0, Lipv;->f:Z

    if-nez v0, :cond_6

    :cond_1
    const/4 v0, 0x1

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lgyn;->k()Lu;

    move-result-object v0

    const v3, 0x7f0a0315    # com.google.android.gms.R.id.address_fragment_holder

    invoke-virtual {v0, v3}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgvc;

    iput-object v0, p0, Lgyn;->e:Lgvc;

    iget-object v0, p0, Lgyn;->e:Lgvc;

    if-nez v0, :cond_3

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    iget-object v0, p0, Lgyn;->i:Lioj;

    iget-object v4, v0, Lioj;->e:Lipv;

    iget-object v0, v4, Lipv;->a:Lixo;

    iget-object v5, v0, Lixo;->a:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    const-string v0, "com.google.android.gms.wallet.addressHints"

    const-class v7, Lipv;

    invoke-static {v3, v0, v7}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v7}, Lgty;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v8

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lgvq;

    move-result-object v9

    if-nez v1, :cond_7

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v9, v0}, Lgvq;->a(Z)Lgvq;

    move-result-object v0

    invoke-virtual {v0, v6}, Lgvq;->a(Ljava/util/List;)Lgvq;

    move-result-object v0

    invoke-virtual {v0, v5}, Lgvq;->a(Ljava/lang/String;)Lgvq;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [C

    fill-array-data v1, :array_0

    iget-object v5, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-object v1, v5, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    invoke-virtual {v0}, Lgvq;->a()Lgvq;

    move-result-object v1

    iget-object v0, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lgvq;->a(Ljava/util/ArrayList;)Lgvq;

    move-result-object v0

    iget-object v1, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v0, p0, Lgyn;->i:Lioj;

    invoke-static {v0}, Lgth;->f(Lioj;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {v4}, Lgth;->c(Lipv;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {v4}, Lgth;->a(Lipv;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_4
    invoke-static {v1, v7, v5, v0}, Lgvc;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;Ljava/util/Collection;ZZ)Lgvc;

    move-result-object v0

    iput-object v0, p0, Lgyn;->e:Lgvc;

    const-string v0, "com.google.android.gms.wallet.baseAddress"

    const-class v1, Lipv;

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    if-eqz v0, :cond_9

    iget-object v1, p0, Lgyn;->e:Lgvc;

    iget-object v3, v0, Lipv;->a:Lixo;

    invoke-virtual {v1, v3}, Lgvc;->a(Lixo;)V

    :goto_5
    if-eqz v5, :cond_2

    iget-object v1, v4, Lipv;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v0, p0, Lgyn;->e:Lgvc;

    iget-object v1, v4, Lipv;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgvc;->a(Ljava/lang/String;)V

    :cond_2
    :goto_6
    invoke-virtual {p0}, Lgyn;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a0315    # com.google.android.gms.R.id.address_fragment_holder

    iget-object v3, p0, Lgyn;->e:Lgvc;

    invoke-virtual {v0, v1, v3}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_3
    return-object v2

    :cond_4
    iget-object v0, p0, Lgyn;->g:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    const v0, 0x7f0a0332    # com.google.android.gms.R.id.exp_month

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const v0, 0x7f0a0333    # com.google.android.gms.R.id.exp_year

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const v0, 0x7f0a031d    # com.google.android.gms.R.id.cvc

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const v0, 0x7f0a031e    # com.google.android.gms.R.id.cvc_hint

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    iput-object v0, p0, Lgyn;->d:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    iget-object v0, p0, Lgyn;->i:Lioj;

    iget v0, v0, Lioj;->c:I

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-static {v0}, Lgth;->d(I)I

    move-result v3

    invoke-direct {v1, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iget-object v3, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/text/InputFilter;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v1, p0, Lgyn;->d:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a(I)V

    new-instance v1, Lgwn;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v4, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, p0, Lgyn;->i:Lioj;

    iget v0, v0, Lioj;->c:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_7
    invoke-direct {v1, v3, v4, v0}, Lgwn;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;I)V

    iput-object v1, p0, Lgyn;->ad:Lgwn;

    new-instance v0, Lgww;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v1, v3, v4}, Lgww;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    new-instance v1, Lgwx;

    iget-object v3, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v1, v3, v4, v0}, Lgwx;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V

    iput-object v1, p0, Lgyn;->ab:Lgwx;

    new-instance v1, Lgwy;

    iget-object v3, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v1, v3, v0}, Lgwy;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V

    iput-object v1, p0, Lgyn;->ac:Lgwy;

    iget-object v0, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ad:Lgwn;

    iget-object v3, p0, Lgyn;->ad:Lgwn;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ab:Lgwx;

    iget-object v3, p0, Lgyn;->ab:Lgwx;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ac:Lgwy;

    iget-object v3, p0, Lgyn;->ac:Lgwy;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ad:Lgwn;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ab:Lgwx;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ac:Lgwy;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ad:Lgwn;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ab:Lgwx;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgyn;->ac:Lgwy;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const v1, 0x7f0a0333    # com.google.android.gms.R.id.exp_year

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const v1, 0x7f0a031d    # com.google.android.gms.R.id.cvc

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const v1, 0x7f0a0340    # com.google.android.gms.R.id.card_nickname

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    goto/16 :goto_1

    :pswitch_0
    const/4 v0, 0x3

    goto/16 :goto_7

    :pswitch_1
    const/4 v0, 0x4

    goto/16 :goto_7

    :pswitch_2
    const/4 v0, 0x2

    goto/16 :goto_7

    :pswitch_3
    const/4 v0, 0x1

    goto/16 :goto_7

    :cond_6
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_2

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_9
    iget-object v1, p0, Lgyn;->e:Lgvc;

    iget-object v3, v4, Lipv;->a:Lixo;

    invoke-virtual {v1, v3}, Lgvc;->a(Lixo;)V

    goto/16 :goto_5

    :cond_a
    if-eqz v0, :cond_2

    iget-object v1, p0, Lgyn;->e:Lgvc;

    iget-object v0, v0, Lipv;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgvc;->a(Ljava/lang/String;)V

    goto/16 :goto_6

    :array_0
    .array-data 2
        0x52s
        0x4es
    .end array-data

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Linv;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Lint;

    invoke-direct {v2}, Lint;-><init>()V

    iget-object v0, p0, Lgyn;->e:Lgvc;

    invoke-virtual {v0}, Lgvc;->a()Lixo;

    move-result-object v0

    iput-object v0, v2, Lint;->d:Lixo;

    iget-object v0, p0, Lgyn;->e:Lgvc;

    invoke-virtual {v0}, Lgvc;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgyn;->e:Lgvc;

    invoke-virtual {v0}, Lgvc;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lint;->g:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lgyn;->i:Lioj;

    invoke-static {v0}, Lgth;->f(Lioj;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    iget-object v0, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    :try_start_1
    iget-object v4, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit16 v4, v4, 0x7d0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Lint;->b:I

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Lint;->c:I

    :cond_2
    new-instance v0, Linu;

    invoke-direct {v0}, Linu;-><init>()V

    iput-object v0, v2, Lint;->a:Linu;

    iget-object v0, v2, Lint;->a:Linu;

    iput-object v3, v0, Linu;->b:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lgyn;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lgyn;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lint;->h:Ljava/lang/String;

    :cond_4
    new-instance v0, Linv;

    invoke-direct {v0}, Linv;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Linv;->a:I

    iput-object v2, v0, Linv;->b:Lint;

    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lgyn;->e:Lgvc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyn;->e:Lgvc;

    invoke-virtual {v0, p1}, Lgvc;->a(Z)V

    :cond_0
    iget-object v0, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lgyn;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgyn;->h:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    :cond_2
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Fragment requires instrument extra!"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v2, Lioj;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, p0, Lgyn;->i:Lioj;

    const-string v0, "com.google.android.gms.wallet.requireAddressUpgrade"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgyn;->Y:Z

    new-instance v0, Lhgu;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v1}, Lhgu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgyn;->Z:Lhgu;

    iget-object v0, p0, Lgyn;->Z:Lhgu;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Lhgs;->a(Landroid/content/Context;)Lhgs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhgu;->a(Lhgs;)V

    iget-object v0, p0, Lgyn;->i:Lioj;

    invoke-static {v0}, Lgth;->b(Lioj;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyn;->aa:Ljava/lang/String;

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lgyn;->e:Lgvc;

    invoke-virtual {v0}, Lgvc;->J()V

    return-void
.end method

.method public final i()Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v3, v2, [Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgyn;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v1

    iget-object v2, p0, Lgyn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lgyn;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lgyn;->e:Lgvc;

    invoke-virtual {v2}, Lgvc;->S_()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v1, p0, Lgyn;->e:Lgvc;

    invoke-virtual {v1}, Lgvc;->i()Z

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method
