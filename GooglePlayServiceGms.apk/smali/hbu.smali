.class public final Lhbu;
.super Lhbr;
.source "SourceFile"


# instance fields
.field j:Lhcs;

.field k:Lhfa;

.field final l:I

.field m:Landroid/os/Handler;

.field final n:Ljava/util/concurrent/CountDownLatch;

.field private final o:Landroid/content/ServiceConnection;

.field private final p:Landroid/content/ServiceConnection;

.field private q:Z

.field private final r:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p2, p3, p4}, Lhbr;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput p1, p0, Lhbu;->l:I

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lhbu;->n:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Lhbv;

    invoke-direct {v0, p0}, Lhbv;-><init>(Lhbu;)V

    iput-object v0, p0, Lhbu;->o:Landroid/content/ServiceConnection;

    new-instance v0, Lhbw;

    invoke-direct {v0, p0}, Lhbw;-><init>(Lhbu;)V

    iput-object v0, p0, Lhbu;->p:Landroid/content/ServiceConnection;

    new-instance v0, Lhbx;

    invoke-direct {v0, p0}, Lhbx;-><init>(Lhbu;)V

    iput-object v0, p0, Lhbu;->r:Ljava/lang/Thread;

    iget-object v0, p0, Lhbu;->r:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :try_start_0
    iget-object v0, p0, Lhbu;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    const-string v0, "NetworkPaymentServiceConnection"

    const-string v1, "Service thread initialization complete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "NetworkPaymentServiceConnection"

    const-string v1, "Unable to initialize PaymentService background thread."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    iget v0, p0, Lhbu;->l:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    iget v0, p0, Lhbu;->l:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lhbu;->q:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lhbu;->f:Landroid/content/Context;

    iget-object v2, p0, Lhbu;->o:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_0
    invoke-direct {p0}, Lhbu;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.service.ow.IOwInternalService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lhbu;->f:Landroid/content/Context;

    iget-object v2, p0, Lhbu;->p:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_1
    iput-boolean v3, p0, Lhbu;->q:Z

    :cond_2
    return-void
.end method

.method public final a(Lios;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;-><init>(Landroid/accounts/Account;Lios;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0xf

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Liou;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;-><init>(Landroid/accounts/Account;Liou;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0x11

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Liow;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;-><init>(Landroid/accounts/Account;Liow;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0x10

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lioz;Z)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;-><init>(Landroid/accounts/Account;Lioz;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lipa;Z)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;-><init>(Landroid/accounts/Account;Lipa;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lipb;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;-><init>(Landroid/accounts/Account;Lipb;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lipc;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;-><init>(Landroid/accounts/Account;Lipc;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lipf;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;-><init>(Landroid/accounts/Account;Lipf;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0xd

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lipk;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;-><init>(Landroid/accounts/Account;Lipk;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lipm;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;-><init>(Landroid/accounts/Account;Lipm;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lipo;Z)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;-><init>(Landroid/accounts/Account;Lipo;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Lipp;Z)V
    .locals 3

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;-><init>(Landroid/accounts/Account;Lipp;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Ljas;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;-><init>(Landroid/accounts/Account;Ljas;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0x12

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Ljau;Z)V
    .locals 3

    invoke-direct {p0}, Lhbu;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    iget-object v2, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;-><init>(Landroid/accounts/Account;Ljau;Ljava/lang/String;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 5

    invoke-direct {p0}, Lhbu;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v0, Ljaq;

    invoke-direct {v0}, Ljaq;-><init>()V

    iput p2, v0, Ljaq;->b:I

    new-instance v1, Ljbc;

    invoke-direct {v1}, Ljbc;-><init>()V

    iput-object v1, v0, Ljaq;->a:Ljbc;

    iget-object v1, v0, Ljaq;->a:Ljbc;

    iput-object p1, v1, Ljbc;->a:Ljava/lang/String;

    iget-object v1, v0, Ljaq;->a:Ljbc;

    invoke-static {}, Lhfx;->a()Ljbf;

    move-result-object v2

    iput-object v2, v1, Ljbc;->b:Ljbf;

    invoke-virtual {p0, v0}, Lhbu;->a(Lizs;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0xb

    new-instance v3, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

    iget-object v4, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;-><init>(Landroid/accounts/Account;Ljaq;)V

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljao;)V
    .locals 3

    invoke-direct {p0}, Lhbu;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p2}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;-><init>(Landroid/accounts/Account;Ljava/lang/String;Ljao;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Ljaw;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Z)V
    .locals 3

    invoke-direct {p0}, Lhbu;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lhbu;->a(Lizs;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;-><init>(Landroid/accounts/Account;Ljaw;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final a(Ljba;Z)V
    .locals 3

    invoke-direct {p0}, Lhbu;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lhbu;->a(Lizs;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;

    iget-object v1, p0, Lhbu;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;-><init>(Landroid/accounts/Account;Ljba;Z)V

    iget-object v1, p0, Lhbu;->m:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    const/4 v1, 0x0

    const-string v0, "NetworkPaymentServiceConnection"

    const-string v2, "destroy"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lhbu;->q:Z

    if-eqz v0, :cond_1

    const-string v0, "NetworkPaymentServiceConnection"

    const-string v2, "disconnect"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lhbu;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhbu;->f:Landroid/content/Context;

    iget-object v2, p0, Lhbu;->o:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0}, Lhbu;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhbu;->f:Landroid/content/Context;

    iget-object v3, p0, Lhbu;->p:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v2, p0, Lhbu;->m:Landroid/os/Handler;

    const v3, 0x8000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, p0, Lhbu;->m:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    iput-boolean v1, p0, Lhbu;->q:Z

    :cond_1
    iget-object v0, p0, Lhbu;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lhbu;->m:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected final c(ILjava/lang/Object;)Landroid/util/Pair;
    .locals 6

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    :try_start_0
    const-string v3, "NetworkPaymentServiceConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown message type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " passed to handler."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :pswitch_1
    :try_start_1
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    iget-object v3, p0, Lhbu;->k:Lhfa;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

    invoke-interface {v3, v4, p2}, Lhfa;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    iget-object v3, p0, Lhbu;->k:Lhfa;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;

    invoke-interface {v3, v4, p2}, Lhfa;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    iget-object v3, p0, Lhbu;->k:Lhfa;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-interface {v3, v4, p2}, Lhfa;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->a()J

    move-result-wide v1

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    iget-object v3, p0, Lhbu;->k:Lhfa;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    invoke-interface {v3, v4, p2}, Lhfa;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_9
    iget-object v3, p0, Lhbu;->k:Lhfa;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

    invoke-interface {v3, v4, p2}, Lhfa;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_a
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    :pswitch_b
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_c
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_d
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_e
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_f
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_10
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_11
    iget-object v3, p0, Lhbu;->j:Lhcs;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    invoke-interface {v3, v4, p2}, Lhcs;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_12
    iget-object v3, p0, Lhbu;->k:Lhfa;

    iget-object v4, p0, Lhbu;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

    invoke-interface {v3, v4, p2}, Lhfa;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    move-exception v3

    const-string v4, "NetworkPaymentServiceConnection"

    const-string v5, "Failed to contact PaymentService:"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method
