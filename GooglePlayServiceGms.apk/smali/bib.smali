.class public final Lbib;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/images/ImageManager;

.field private final b:Landroid/net/Uri;

.field private final c:Landroid/graphics/Bitmap;

.field private final d:Ljava/util/concurrent/CountDownLatch;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/images/ImageManager;Landroid/net/Uri;Landroid/graphics/Bitmap;ZLjava/util/concurrent/CountDownLatch;)V
    .locals 0

    iput-object p1, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lbib;->b:Landroid/net/Uri;

    iput-object p3, p0, Lbib;->c:Landroid/graphics/Bitmap;

    iput-boolean p4, p0, Lbib;->e:Z

    iput-object p5, p0, Lbib;->d:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v0, "OnBitmapLoadedRunnable must be executed in the main thread"

    invoke-static {v0}, Lbiq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lbib;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    move v2, v3

    :goto_0
    iget-object v0, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-static {v0}, Lcom/google/android/gms/common/images/ImageManager;->f(Lcom/google/android/gms/common/images/ImageManager;)Lbhx;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lbib;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-static {v0}, Lcom/google/android/gms/common/images/ImageManager;->f(Lcom/google/android/gms/common/images/ImageManager;)Lbhx;

    move-result-object v0

    invoke-virtual {v0}, Lbhx;->a()V

    invoke-static {}, Ljava/lang/System;->gc()V

    iput-boolean v4, p0, Lbib;->e:Z

    iget-object v0, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-static {v0}, Lcom/google/android/gms/common/images/ImageManager;->e(Lcom/google/android/gms/common/images/ImageManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    return-void

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    iget-object v0, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-static {v0}, Lcom/google/android/gms/common/images/ImageManager;->f(Lcom/google/android/gms/common/images/ImageManager;)Lbhx;

    move-result-object v0

    new-instance v1, Lbih;

    iget-object v5, p0, Lbib;->b:Landroid/net/Uri;

    invoke-direct {v1, v5}, Lbih;-><init>(Landroid/net/Uri;)V

    iget-object v5, p0, Lbib;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v5}, Lbhx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v0, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-static {v0}, Lcom/google/android/gms/common/images/ImageManager;->b(Lcom/google/android/gms/common/images/ImageManager;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lbib;->b:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;

    if-eqz v0, :cond_6

    iput-boolean v3, v0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->a:Z

    invoke-static {v0}, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->a(Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v5, v4

    :goto_2
    if-ge v5, v7, :cond_5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbig;

    if-eqz v2, :cond_4

    iget-object v8, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-static {v8}, Lcom/google/android/gms/common/images/ImageManager;->a(Lcom/google/android/gms/common/images/ImageManager;)Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lbib;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v8, v9, v4}, Lbig;->a(Landroid/content/Context;Landroid/graphics/Bitmap;Z)V

    :goto_3
    iget v8, v1, Lbig;->d:I

    if-eq v8, v3, :cond_3

    iget-object v8, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-static {v8}, Lcom/google/android/gms/common/images/ImageManager;->c(Lcom/google/android/gms/common/images/ImageManager;)Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    :cond_4
    iget-object v8, p0, Lbib;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-static {v8}, Lcom/google/android/gms/common/images/ImageManager;->a(Lcom/google/android/gms/common/images/ImageManager;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v1, v8, v4}, Lbig;->a(Landroid/content/Context;Z)V

    goto :goto_3

    :cond_5
    iput-boolean v4, v0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->a:Z

    :cond_6
    iget-object v0, p0, Lbib;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    invoke-static {}, Lcom/google/android/gms/common/images/ImageManager;->a()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/images/ImageManager;->b()Ljava/util/HashSet;

    move-result-object v0

    iget-object v2, p0, Lbib;->b:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
