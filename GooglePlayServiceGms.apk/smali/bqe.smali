.class public final Lbqe;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static b:Ljava/lang/String;

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "\\(\\d+-"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbqe;->a:Ljava/util/regex/Pattern;

    const/4 v0, -0x1

    sput v0, Lbqe;->c:I

    return-void
.end method

.method public static a()I
    .locals 3

    invoke-static {}, Lbqe;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "version_code"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static a(I)V
    .locals 2

    invoke-static {}, Lbqe;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "version_code"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public static b()J
    .locals 3

    sget-object v0, Lbqe;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lbox;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbqe;->b:Ljava/lang/String;

    :cond_0
    sget-object v0, Lbqe;->b:Ljava/lang/String;

    sget-object v1, Lbqe;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static c()I
    .locals 1

    invoke-static {}, Lbqe;->f()I

    move-result v0

    rem-int/lit16 v0, v0, 0x3e8

    div-int/lit8 v0, v0, 0x64

    add-int/lit8 v0, v0, 0x5

    return v0
.end method

.method public static d()I
    .locals 1

    invoke-static {}, Lbqe;->f()I

    move-result v0

    rem-int/lit8 v0, v0, 0x64

    div-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public static e()I
    .locals 1

    invoke-static {}, Lbqe;->f()I

    move-result v0

    rem-int/lit8 v0, v0, 0xa

    return v0
.end method

.method private static f()I
    .locals 2

    sget v0, Lbqe;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lbox;->e(Landroid/content/Context;)I

    move-result v0

    sput v0, Lbqe;->c:I

    :cond_0
    sget v0, Lbqe;->c:I

    return v0
.end method

.method private static g()Landroid/content/SharedPreferences;
    .locals 3

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "init.initialized_version"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/app/GmsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
