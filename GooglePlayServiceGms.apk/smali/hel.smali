.class public final Lhel;
.super Lhfb;
.source "SourceFile"


# instance fields
.field private final a:Lhfb;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhfb;)V
    .locals 1

    invoke-direct {p0}, Lhfb;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhel;->b:Landroid/content/Context;

    iput-object p2, p0, Lhel;->a:Lhfb;

    return-void
.end method

.method private a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    iget-object v0, p0, Lhel;->b:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lhhe;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/Throwable;)V

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lhel;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v0

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    invoke-direct {p0, v1, p1}, Lhel;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lhel;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0, p1}, Lhel;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lhel;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0, p1}, Lhel;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lhel;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0, p1}, Lhel;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lhel;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0, p1}, Lhel;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lhel;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0, p1}, Lhel;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
