.class public final Lxy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/widget/ViewSwitcher;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/content/Context;

.field public final d:Luf;

.field public final e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field public f:Lyd;

.field public g:Laby;

.field public h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field public i:Labr;

.field public j:Labs;

.field public k:Lyl;

.field public l:Labw;

.field public m:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lxy;->l:Labw;

    iput-object v1, p0, Lxy;->m:Ljava/util/HashSet;

    iget-boolean v0, p2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_0

    iput-object v1, p0, Lxy;->a:Landroid/widget/ViewSwitcher;

    :goto_0
    iput-object p2, p0, Lxy;->h:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iput-object p3, p0, Lxy;->b:Ljava/lang/String;

    iput-object p1, p0, Lxy;->c:Landroid/content/Context;

    new-instance v0, Luf;

    iget-object v1, p4, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    invoke-static {v1, p1}, Lud;->a(Ljava/lang/String;Landroid/content/Context;)Lud;

    move-result-object v1

    invoke-direct {v0, v1}, Luf;-><init>(Ltz;)V

    iput-object v0, p0, Lxy;->d:Luf;

    iput-object p4, p0, Lxy;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    return-void

    :cond_0
    new-instance v0, Landroid/widget/ViewSwitcher;

    invoke-direct {v0, p1}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget-object v0, p0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget v1, p2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setMinimumWidth(I)V

    iget-object v0, p0, Lxy;->a:Landroid/widget/ViewSwitcher;

    iget v1, p2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setMinimumHeight(I)V

    iget-object v0, p0, Lxy;->a:Landroid/widget/ViewSwitcher;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setVisibility(I)V

    goto :goto_0
.end method
