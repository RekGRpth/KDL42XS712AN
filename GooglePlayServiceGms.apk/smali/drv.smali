.class public abstract Ldrv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldrb;


# instance fields
.field protected final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:[Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Ldrv;->a:Lcom/google/android/gms/common/server/ClientContext;

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    new-array v0, p2, [Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;Lcun;I)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public final a(Landroid/content/Context;Lcun;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v1, 0x0

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v4, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    :try_start_0
    iget-object v4, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {p0, p1, p2, v0}, Ldrv;->a(Landroid/content/Context;Lcun;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v5

    aput-object v5, v4, v0
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v5, "MultiDataOperation"

    const-string v6, "Auth exception while performing operation, requesting reconnect"

    invoke-static {v5, v6, v4}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v4, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    const/4 v5, 0x2

    invoke-static {v5}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v5

    aput-object v5, v4, v0

    goto :goto_1

    :catch_1
    move-exception v4

    const-string v5, "MultiDataOperation"

    invoke-virtual {v4}, Ldqq;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v5, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v4}, Ldqq;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v4}, Ldqq;->a()I

    move-result v4

    const/16 v5, 0x5dc

    if-ne v4, v5, :cond_1

    move v2, v3

    goto :goto_1

    :cond_1
    const/16 v5, 0x3eb

    if-ne v4, v5, :cond_0

    invoke-virtual {p2, p1}, Lcun;->c(Landroid/content/Context;)V

    goto :goto_1

    :catch_2
    move-exception v4

    const-string v5, "MultiDataOperation"

    const-string v6, "Runtime exception while performing operation"

    invoke-static {v5, v6, v4}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v5, "MultiDataOperation"

    const-string v6, "Killing (on development devices) due to RuntimeException"

    invoke-static {v5, v6, v4}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v4, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v5

    aput-object v5, v4, v0

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    iget-object v0, p0, Ldrv;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0, p2}, Leep;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcun;)V

    iget-object v0, p0, Ldrv;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lduj;->e(Landroid/content/Context;Ljava/lang/String;)V

    :cond_3
    :try_start_1
    iget-object v0, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {p0, v0}, Ldrv;->a([Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    iget-object v0, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :goto_3
    iget-object v0, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :goto_4
    iget-object v2, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    iget-object v2, p0, Ldrv;->b:[Lcom/google/android/gms/common/data/DataHolder;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    throw v0

    :cond_5
    return-void

    :catchall_0
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_3
.end method

.method protected abstract a([Lcom/google/android/gms/common/data/DataHolder;)V
.end method
