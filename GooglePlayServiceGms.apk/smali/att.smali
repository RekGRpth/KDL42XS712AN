.class public Latt;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/ArrayList;

.field private static final c:Ljava/util/Map;


# instance fields
.field private Y:Ljava/util/ArrayList;

.field private Z:Landroid/widget/TextView;

.field private aa:Landroid/widget/TextView;

.field private ab:Latv;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/gms/common/acl/ScopeData;

.field private i:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "<placeholder\\s*id=[\'\"]app_name[\'\"]\\s*/?>(.*</placeholder>)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Latt;->a:Ljava/util/regex/Pattern;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Latt;->b:Ljava/util/ArrayList;

    invoke-static {}, Lirv;->e()Lirw;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/plus.me"

    const v2, 0x7f02020f    # com.google.android.gms.R.drawable.plus_iconic_ic_gplus_color_24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lirw;->a(Ljava/lang/Object;Ljava/lang/Object;)Lirw;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/userinfo.email"

    const v2, 0x7f02020e    # com.google.android.gms.R.drawable.plus_iconic_ic_gmail_red_24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lirw;->a(Ljava/lang/Object;Ljava/lang/Object;)Lirw;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/youtube"

    const v2, 0x7f020219    # com.google.android.gms.R.drawable.plus_iconic_ic_video_red_24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lirw;->a(Ljava/lang/Object;Ljava/lang/Object;)Lirw;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/games"

    const v2, 0x7f020214    # com.google.android.gms.R.drawable.plus_iconic_ic_play_games_color_24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lirw;->a(Ljava/lang/Object;Ljava/lang/Object;)Lirw;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/payments.make_payments"

    const v2, 0x7f02021a    # com.google.android.gms.R.drawable.plus_iconic_ic_wallet_color_24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lirw;->a(Ljava/lang/Object;Ljava/lang/Object;)Lirw;

    move-result-object v0

    iget-object v0, v0, Lirw;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/util/Map$Entry;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/Map$Entry;

    new-instance v1, Lisq;

    invoke-direct {v1, v0}, Lisq;-><init>([Ljava/util/Map$Entry;)V

    move-object v0, v1

    :goto_0
    sput-object v0, Latt;->c:Ljava/util/Map;

    return-void

    :pswitch_0
    invoke-static {}, Lirv;->d()Lirv;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v1, Lisz;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lisg;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-direct {v1, v0}, Lisz;-><init>(Ljava/util/Map$Entry;)V

    move-object v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private P()Ljava/util/ArrayList;
    .locals 5

    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->e()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {v1}, Lbpd;->b(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbky;->a([B)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "AuthScopeFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to parse audience from roster: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Latt;->j()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "myCircles"

    const v3, 0x7f0b045e    # com.google.android.gms.R.string.common_chips_label_your_circles

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/acl/ScopeData;)Latt;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "scope_index"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "app_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "calling_package"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "scope_data"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Latt;

    invoke-direct {v1}, Latt;-><init>()V

    invoke-virtual {v1, v0}, Latt;->g(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Latt;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 9

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    const v0, 0x7f0b0406    # com.google.android.gms.R.string.plus_scope_fragment_only_you_label

    invoke-virtual {p0, v0}, Latt;->b(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    move v4, v5

    move-object v2, v1

    move-object v3, v1

    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v6

    if-ne v6, v7, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_2
    :pswitch_0
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_1

    :pswitch_1
    move-object v8, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v8

    goto :goto_2

    :pswitch_2
    move-object v2, v3

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_2

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->i()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :cond_3
    if-eqz v3, :cond_4

    const v0, 0x7f0b0402    # com.google.android.gms.R.string.plus_scope_fragment_public_label

    invoke-virtual {p0, v0}, Latt;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_5

    const v0, 0x7f0b0403    # com.google.android.gms.R.string.plus_scope_fragment_extended_circles_label

    invoke-virtual {p0, v0}, Latt;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    if-eqz v1, :cond_6

    const v0, 0x7f0b0404    # com.google.android.gms.R.string.plus_scope_fragment_your_circles_label

    invoke-virtual {p0, v0}, Latt;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v7, :cond_7

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    const v0, 0x7f0b0405    # com.google.android.gms.R.string.plus_scope_fragment_mixed_label

    invoke-virtual {p0, v0}, Latt;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbpx;->a(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v2, 0x1

    if-ge v0, v4, :cond_2

    const-string v0, " "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "AuthScopeFragment"

    const-string v3, "Failed to convert audience to circle ID list"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object p2

    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_1

    aget-object v3, v2, v1

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_2

    move-object p2, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "AuthScopeFragment"

    const-string v2, "Failed to parse audience from circle ID list."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/acl/ScopeData;->a(Z)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 4

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Latt;->f:Ljava/lang/String;

    sget-object v2, Lbck;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v3, p0, Latt;->g:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, v3}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Latt;->f:Ljava/lang/String;

    iget-object v2, p0, Latt;->g:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, v2}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p0}, Latt;->j()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, 0x7f0c012d    # com.google.android.gms.R.color.plus_auth_scope_fragment_circles_text

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Latt;->aa:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private b(Ljava/util/List;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lbpd;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lblf;->a([B)Lblf;

    move-result-object v2

    invoke-virtual {v2}, Lblf;->c()I
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    :try_start_1
    invoke-virtual {v2, v1}, Lblf;->a(I)Lblg;

    move-result-object v4

    iget-boolean v5, v4, Lblg;->a:Z

    if-eqz v5, :cond_3

    iget-object v4, v4, Lblg;->b:Lbli;

    invoke-virtual {v4}, Lbli;->d()[B

    move-result-object v4

    invoke-static {v4}, Lbli;->a([B)Lbli;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lbli;->a:Ljava/util/List;
    :try_end_1
    .catch Lizj; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_1
    if-nez v0, :cond_1

    new-instance v0, Lbli;

    invoke-direct {v0}, Lbli;-><init>()V

    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0, p1}, Lbky;->a(Lbli;Ljava/util/List;)V

    :cond_2
    invoke-virtual {v0}, Lbli;->d()[B

    move-result-object v0

    invoke-static {v0}, Lbpd;->c([B)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    const-string v2, "AuthScopeFragment"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "AuthScopeFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get SharingRoster from RenderedSharingRoster: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lizj;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move-object v0, v1

    goto :goto_1

    :catch_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2
.end method


# virtual methods
.method public final J()Z
    .locals 1

    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->j()Z

    move-result v0

    goto :goto_0
.end method

.method public final K()Z
    .locals 1

    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final L()Z
    .locals 1

    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    goto :goto_0
.end method

.method public final M()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public final N()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Latt;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget v1, p0, Latt;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-class v1, Latt;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Latt;->K()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "\n   SharingRoster: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "No one"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Latt;->L()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "\n   Visible edges: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Latt;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, "\n   All circles: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Latt;->J()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    iget-object v1, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v7, 0x7f0a027a    # com.google.android.gms.R.id.scope_details_icon

    const v6, 0x7f0a00b1    # com.google.android.gms.R.id.warning

    const v5, 0x7f0a00ac    # com.google.android.gms.R.id.scope_icon

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    const v0, 0x7f0400d1    # com.google.android.gms.R.layout.plus_auth_scope_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a0279    # com.google.android.gms.R.id.scope_description_label

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latt;->Z:Landroid/widget/TextView;

    const v0, 0x7f0a027b    # com.google.android.gms.R.id.pacl_visible_circles

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latt;->aa:Landroid/widget/TextView;

    iget-object v0, p0, Latt;->Z:Landroid/widget/TextView;

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0278    # com.google.android.gms.R.id.scope_fragment_layout

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Latt;->j()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02020f    # com.google.android.gms.R.drawable.plus_iconic_ic_gplus_color_24

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Latt;->aa:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    invoke-direct {p0}, Latt;->P()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Latt;->a(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Latt;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    sget-object v0, Latt;->c:Ljava/util/Map;

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-object v1, Latt;->c:Ljava/util/Map;

    iget-object v4, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    return-object v2

    :cond_3
    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020205    # com.google.android.gms.R.drawable.plus_icn_scope_icon_facl

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Latt;->i:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Latt;->b:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Latt;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Latt;->i:Ljava/util/ArrayList;

    :cond_4
    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Latt;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v2, -0x1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    if-eqz p3, :cond_0

    const-string v0, "pacl_audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Latt;->Y:Ljava/util/ArrayList;

    iget-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Latt;->a(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Latt;->a(Ljava/lang/String;)V

    if-ne p2, v2, :cond_1

    sget-object v0, Lbcz;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Latt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    sget-object v0, Lbcz;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Latt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    if-ne p2, v2, :cond_3

    invoke-static {p3}, Lbfa;->a(Landroid/content/Intent;)Lbfc;

    move-result-object v0

    invoke-interface {v0}, Lbfc;->b()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Latt;->i:Ljava/util/ArrayList;

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-interface {v0}, Lbfc;->c()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/acl/ScopeData;->a(Z)V

    sget-object v0, Lbcj;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Latt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_3
    if-nez p2, :cond_0

    sget-object v0, Lbcj;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Latt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    if-ne p2, v2, :cond_0

    iget-object v0, p0, Latt;->ab:Latv;

    const-string v1, "detail_end_time"

    invoke-virtual {p3, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v3, "detail_start_time"

    invoke-virtual {p3, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-interface {v0, v1, v2}, Latv;->a(J)V

    iget-object v0, p0, Latt;->ab:Latv;

    const-string v1, "detail_screen_scrollable"

    invoke-virtual {p3, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "scroll_screen_end"

    invoke-virtual {p3, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Latv;->a(ZZ)V

    goto :goto_0

    :cond_5
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v1, p1, Latu;

    invoke-static {v1}, Lbkm;->a(Z)V

    :try_start_0
    move-object v0, p1

    check-cast v0, Latv;

    move-object v1, v0

    iput-object v1, p0, Latt;->ab:Latv;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement OnScopeDetailsSelectedListener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "app_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Latt;->e:Ljava/lang/String;

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Latt;->f:Ljava/lang/String;

    const-string v1, "scope_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Latt;->d:I

    const-string v1, "calling_package"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Latt;->g:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "facl_audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Latt;->i:Ljava/util/ArrayList;

    const-string v0, "pacl_audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Latt;->Y:Ljava/util/ArrayList;

    const-string v0, "scope_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    :goto_0
    return-void

    :cond_0
    const-string v1, "scope_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Latt;->i:Ljava/util/ArrayList;

    invoke-static {v0}, Latt;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "app_name"

    iget-object v1, p0, Latt;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "account_name"

    iget-object v1, p0, Latt;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "scope_data"

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "pacl_audience"

    iget-object v1, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "facl_audience"

    iget-object v1, p0, Latt;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Latt;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Latt;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Latt;->f:Ljava/lang/String;

    iget-object v2, p0, Latt;->g:Ljava/lang/String;

    iget-object v3, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    new-instance v4, Lbkw;

    invoke-direct {v4}, Lbkw;-><init>()V

    iget-object v5, p0, Latt;->Y:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v4

    invoke-virtual {v4}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/acl/ScopeData;Lcom/google/android/gms/common/people/data/Audience;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Latt;->a(Landroid/content/Intent;I)V

    sget-object v0, Lbck;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lbck;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0, v1}, Latt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lbfa;->a()Lbfb;

    move-result-object v0

    iget-object v1, p0, Latt;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbfb;->l(Ljava/lang/String;)Lbfb;

    move-result-object v0

    iget-object v1, p0, Latt;->i:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lbfb;->e(Ljava/util/List;)Lbfb;

    move-result-object v0

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->h()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Latt;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Latt;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbfb;->k(Ljava/lang/String;)Lbfb;

    move-result-object v0

    iget-object v1, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/ScopeData;->j()Z

    move-result v1

    invoke-interface {v0, v1}, Lbfb;->b(Z)Lbfb;

    move-result-object v0

    sget-object v1, Lbch;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbfb;->f(Ljava/lang/String;)Lbew;

    move-result-object v0

    iget-object v0, v0, Lbew;->a:Landroid/content/Intent;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Latt;->a(Landroid/content/Intent;I)V

    sget-object v0, Lbcj;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Latt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    sget-object v0, Lbck;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lbck;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0, v1}, Latt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Latt;->h:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Latu;

    invoke-interface {v0}, Latu;->b()Z

    move-result v0

    iget-object v2, p0, Latt;->g:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->a(Ljava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Latt;->a(Landroid/content/Intent;I)V

    goto :goto_0
.end method
